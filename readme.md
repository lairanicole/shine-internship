## SHINEOS+ EMR

The First Health Care Exchange System
A digital health solution that will enhance the workflow of the health community.

There is no better way to an optimized healthcare workflow.

With ShineOS+, whether you are a private or government healthcare clinician, a health IT professional, a health educator or a healthcare beneficiary, you are able to do more or get more. Secured information is fully accessible anytime and anywhere in all your devices. At the core of SHINE OS+ is user-centric design by incorporating user’s workflow in the system so that patient information is captured in an accurate, easy and secure manner.

Full Feature Application

ShineOS+ is packed with features an eMR should have. And we added more to provide you a system that extends and expands your healthcare workflow.

User-centric Design

Designed for the user, we used only the latest UI/UX design patterns and technologies to give you an application you will call you own.

Application for Everyone

We know not everyone works and have everything. So created ShineOS+ Editions to cater to various needs, situations and available support.


## Official Documentation

Documentation for the platform can be found on the [Shine.ph website](http://www.shine.ph/support).

## Contributing

Thank you for considering contributing to the Shine Core Development! The contribution guide can be found in the [Shine.ph documentation](http://www.shine.ph/do-more/).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
