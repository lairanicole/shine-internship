/**
* ShineOS+ Form Validation
*
* ShineOS+ V3.0
**/

"use strict";

$(function () {
    var parser = new UAParser();
    var result = parser.getResult();

    var browser = "N/A";
    var browserversion = "N/A";
    var device = "N/A";
    var devicemodel = "N/A";
    var devicevendor = "N/A";
    var os = "N/A";
    var osversion = "N/A";
    var osengine = "N/A";
    var osarchitecture = "N/A";

    if(result.browser.name) browser = result.browser.name;
    if(result.browser.version) browserversion = result.browser.version;
    if(result.device.type) device = result.device.type;
    if(result.device.model) devicemodel = result.device.model;
    if(result.device.vendor) devicevendor = result.device.vendor;
    if(result.os.name) os = result.os.name;
    if(result.os.version) osversion = result.os.version;
    if(result.engine.name) osengine = result.engine.name;
    if(result.cpu.architecture) osarchitecture = result.cpu.architecture;

    $('input[type=text]').on('click', function(e) {
       var myName = this.name;
       var myLabel = $(this).prop('placeholder');
       $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "input",
                type: $(this).prop('type'),
                ID: $(this).prop('id'),
                name: myName,
                curvalue: this.value,
                label: myLabel,
                action: "focus"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('input[type=text]').on('blur', function(e) {
       var myName = this.name;
       var myLabel = $(this).prop('placeholder');
       $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "input",
                type: $(this).prop('type'),
                ID: $(this).prop('id'),
                name: myName,
                curvalue: this.value,
                label: myLabel,
                action: "blur"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('select').on('click', function(e) {
       var myName = this.name;
       $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "select",
                type: $(this).prop('type'),
                ID: $(this).prop('id'),
                name: myName,
                curvalue: this.options[this.selectedIndex].text,
                label: "",
                action: "click"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('select').on('change', function(e) {
       var myName = this.name;
       $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "select",
                type: $(this).prop('type'),
                ID: $(this).prop('id'),
                name: myName,
                curvalue: this.options[this.selectedIndex].text,
                label: "",
                action: "change"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('textarea').on('click', function(e) {
       var myName = this.name;
       $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "textarea",
                type: $(this).prop('type'),
                ID: $(this).prop('id'),
                name: myName,
                curvalue: this.value,
                label: "",
                action: "click"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('textarea').on('blur', function(e) {
       var myName = this.name;
       $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "textarea",
                type: $(this).prop('type'),
                ID: $(this).prop('id'),
                name: myName,
                curvalue: this.value,
                label: "",
                action: "blur"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('label.btn').on('click', function(){
        var myName = $(this).find("input").attr('name');
        var myValue = $(this).find("input").attr('value');
        var myLabel = $(this).text().trim();
        $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "radio",
                type: "",
                ID: $(this).find("input").prop('id'),
                name: myName,
                curvalue: myValue,
                label: myLabel,
                action: "click"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('ins').on('click', function(){
        var myName = $(this).siblings('input').attr('name');
        var myValue = $(this).siblings('input').prop('checked');
        var myLabel = $(this).parents('label').text().trim();
        var myID = $(this).siblings('input').prop('id');
        $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "checkbox",
                type: "",
                ID: myID,
                name: myName,
                curvalue: myValue,
                label: myLabel,
                action: "click"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('a.btn').on('click', function(){
        var myLabel = $(this).text().trim();
        var myID = $(this).prop('id');
        var myName = $(this).prop('title');
        var myHref = $(this).prop('href');
        $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "link",
                type: "button",
                ID: myID,
                name: myName,
                curvalue: myHref,
                label: myLabel,
                action: "click"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('input.btn[type=submit]').on('click', function(){
        var myLabel = $(this).value();
        var myID = $(this).prop('id');
        var myName = $(this).prop('name');
        $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "button",
                type: "button",
                ID: myID,
                name: myName,
                curvalue: "",
                label: myLabel,
                action: "click"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('#dataTable_patients a').on('click', function(){
        var myLabel = $(this).text().trim();
        var myID = $(this).prop('id');
        var myName = $(this).prop('title');
        var myHref = $(this).prop('href');
        $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "link",
                type: "button",
                ID: myID,
                name: myName,
                curvalue: myHref,
                label: myLabel,
                action: "click"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('.sidebar-menu a').on('click', function(){
        var myLabel = $(this).text().trim();
        var myID = $(this).prop('id');
        var myName = $(this).prop('name');
        var myHref = $(this).prop('href');
        $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "link",
                type: "navigation",
                ID: myID,
                name: myName,
                curvalue: myHref,
                label: myLabel,
                action: "click"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('.nav-tabs a').on('mousedown', function(){
        var myLabel = $(this).text().trim();
        var myID = $(this).prop('id');
        var myName = $(this).prop('name');
        var myHref = $(this).prop('href');
        $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "link",
                type: "page-tab",
                ID: myID,
                name: myName,
                curvalue: myHref,
                label: myLabel,
                action: "click"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('.breadcrumb a').on('click', function(){
        var myLabel = $(this).text().trim();
        var myID = $(this).prop('id');
        var myName = $(this).prop('name');
        var myHref = $(this).prop('href');
        $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "link",
                type: "breadcrumb",
                ID: myID,
                name: myName,
                curvalue: myHref,
                label: myLabel,
                action: "click"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
    $('button').on('click', function(){
        var myLabel = $(this).text().trim();
        var myType = $(this).prop('type');
        var myHref = $(this).prop('onclick');
        var myID = $(this).prop('id');
        $.ajax({
            url : baseurl+"default/track",
            async : true,
            dataType : "json",
            data : {
                browser: browser,
                browserversion: browserversion,
                device: device,
                devicemodel: devicemodel,
                devicevendor: devicevendor,
                os: os,
                osversion: osversion,
                osengine: osengine,
                osarchitecture: osarchitecture,
                url: window.location.href,
                element: "button",
                type: myType,
                ID: myID,
                name: "",
                curvalue: "",
                label: myLabel,
                action: "click"
            },
            headers: {
              'X-CSRF-Token': $('span[name="_token"]').attr('content')
            },
            method: 'post',
            success : function( jsn ) {

            }
        });
    });
});

$(document).ready(function() {
    var parser = new UAParser();
    var result = parser.getResult();

    var browser = "N/A";
    var browserversion = "N/A";
    var device = "N/A";
    var devicemodel = "N/A";
    var devicevendor = "N/A";
    var os = "N/A";
    var osversion = "N/A";
    var osengine = "N/A";
    var osarchitecture = "N/A";
    var thisid = "";
    var thisaction = "";
    var thiscategory = "";

    if(result.browser.name) browser = result.browser.name;
    if(result.browser.version) browserversion = result.browser.version;
    if(result.device.type) device = result.device.type;
    if(result.device.model) devicemodel = result.device.model;
    if(result.device.vendor) devicevendor = result.device.vendor;
    if(result.os.name) os = result.os.name;
    if(result.os.version) osversion = result.os.version;
    if(result.engine.name) osengine = result.engine.name;
    if(result.cpu.architecture) osarchitecture = result.cpu.architecture;

    var url = window.location.href;
    var pieces = url.split('/');

    if(pieces.length == 7) {
        thisid = pieces[ pieces.length - 1 ];
        thisaction = pieces[ pieces.length - 2 ];
        thiscategory = pieces[ pieces.length - 3 ];
    }
    if(pieces.length == 6) {
        thisid = pieces[ pieces.length - 1 ];
        thisaction = pieces[ pieces.length - 2 ];
        thiscategory = pieces[ pieces.length - 2 ];
    }
    if(pieces.length == 5) {
        thisid = pieces[ pieces.length - 1 ];
        thisaction = pieces[ pieces.length - 1 ];
        thiscategory = pieces[ pieces.length - 1 ];
    }

    $.ajax({
        url : baseurl+"default/track",
        async : true,
        dataType : "json",
        data : {
            browser: browser,
            browserversion: browserversion,
            device: device,
            devicemodel: devicemodel,
            devicevendor: devicevendor,
            os: os,
            osversion: osversion,
            osengine: osengine,
            osarchitecture: osarchitecture,
            url: url,
            element: "page",
            type: "pageload",
            ID: thisid,
            name: thiscategory,
            curvalue: "",
            label: "",
            action: thisaction,
        },
        headers: {
          'X-CSRF-Token': $('span[name="_token"]').attr('content')
        },
        method: 'post',
        success : function( jsn ) {

        }
    });
});
