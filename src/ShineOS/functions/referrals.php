<?php
/* Functions for Referrals */

use ShineOS\Core\Referrals\Entities\Referrals;
use ShineOS\Core\Referrals\Entities\Referralmessages as referralmessages;
use ShineOS\Core\Facilities\Entities\FacilityUser as facilityUser;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser as facilityPatientUser;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;

/**
* Count inbound referrals
* @param string $facility_id Facility ID
* @return int Count of referrals
* @package Referrals
*/
function countInboundReferrals($facility_id)
{
    $referrals = DB::table('referrals_view')
        ->where('facility_id', $facility_id)
        ->get();

    return count($referrals);
}

/**
* Count outbound referrals
* @param string $facility_id Facility ID
* @return int Count of referrals
* @package Referrals
*/
function countOutboundReferrals($facility_id)
{
    $o = 0;
    //get all my healthcare records
    $hcs = getAllHealthcareByFacilityID($facility_id);

    if($hcs) {
        foreach($hcs as $h){
            $referrals = DB::table('referrals_view')
            ->where('healthcareservice_id', $h->healthcareservice_id)
            ->get();

            if($referrals) {
                $o += count($referrals);
            }
        }
    }

    return $o;
}

/**
* Count referral message
* @param string $facility_id Facility ID
* @return int Count of referrals
* @package Referrals
*/
function countReferralMessages($facility_id)
{
    $ref_facility_id = Session::get('facility_details')->facility_id;

    $data['facilityUser'] = facilityUser::where('facility_id',$ref_facility_id)->lists('facilityuser_id');
    $data['facilityPatientUser'] = facilityPatientUser::whereIn('facilityuser_id',$data['facilityUser'])->lists('facilitypatientuser_id');
    $data['healthcareservices'] = Healthcareservices::whereIn('facilitypatientuser_id',$data['facilityPatientUser'])->lists('healthcareservice_id');
    $allreferrals = Referrals::WhereIn('healthcareservice_id',$data['healthcareservices'])->orWhere('facility_id',$ref_facility_id)->lists('referral_id');
    $refMessages = referralmessages::whereIn('referral_id',$allreferrals)->orderBy('updated_at', 'DESC')->groupBy('referral_id')->lists('referral_id');
    return $refMessages->count();

}

/**
* Count draft referrals
* @param string $facility_id Facility ID
* @return int Count of referrals
* @package Referrals
*/
function countDraftReferrals($facility_id)
{
    $o = 0;

    $referrals = DB::table('referrals_view')
        ->where('referral_status', 6)
        ->where('facility_id', $facility_id)
        ->where('deleted_at', NULL)
        ->get();
    $o = count($referrals);

    return $o;
}
