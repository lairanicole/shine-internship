<?php

/**
 * Load Extensions JS scripts file
 *
 * @param null
 * @return null
 *
 * @package Extensions
 *
 */
function js_modules()
{
    //Inserts Module JS script at the end of the page
    $modules = Session::get('roles');
    if(isset($modules['external_modules'])) {
        foreach($modules['external_modules'] as $module) {
            if(file_exists(modules_path().$module.'/Assets/functions/modulejs.php')) {
                include modules_path().$module.'/Assets/functions/modulejs.php';
            }
        }
    }
}

/**
 * Load module PHP Functions
 */
function php_modules()
{
    $modules = Session::get('roles');
    if(isset($modules['external_modules'])) {
        foreach($modules['external_modules'] as $module) {
            if(file_exists(modules_path().$module.'/Assets/functions/modulephp.php')) {
                include modules_path().$module.'/Assets/functions/modulephp.php';
            }
        }
    }
}

/**
 * Load Plugins scripts
 * Make sure only activated plugins are loaded.
 * This is only applicable to facilities.
 * Patient Portal skips this.
 */

function php_plugins()
{
    $facility = Session::get('facility_details');
    if($facility) {
        $enabled_plugins = $facility->enabled_plugins;
        //get installed plugins
        $patientPluginDir = plugins_path()."/";
        $plugins = directoryFiles($patientPluginDir);
        asort($plugins);

        if($enabled_plugins) {
            foreach($plugins as $k=>$plugin) {
                if(file_exists(plugins_path().$plugin.'/functions.php')){
                    include plugins_path().$plugin.'/functions.php';
                }
            }
        }
    }
}

/**
 * Collects and inserts all script for fnDrawCallback
 * inside the Healthcare dataTable function
 * after data is rendered
 */
function dataTableHCJsFuncs() {
    $modules = Session::get('roles');
    if(isset($modules['external_modules'])) {
        foreach($modules['external_modules'] as $module) {
            if(file_exists(modules_path().$module.'/Assets/functions/datatableHCInsert.php')) {
                include modules_path().$module.'/Assets/functions/datatableHCInsert.php';
            }
        }
    }
}

/**
 * Collects and inserts all script for fnDrawCallback
 * inside the Patient dataTable function
 * after data is rendered
 */
function dataTablePatJsFuncs() {
    $modules = Session::get('roles');
    if(isset($modules['external_modules'])) {
        foreach($modules['external_modules'] as $module) {
            if(file_exists(modules_path().$module.'/Assets/functions/datatablePatInsert.php')) {
                include modules_path().$module.'/Assets/functions/datatablePatInsert.php';
            }
        }
    }
}