<?php

/**
 * Get the user file path
 * @param null
 * @return string
 *          User File path
 * @package Paths
 */
function userfiles_path()
{
    static $folder;
    if (!$folder) {
        $folder = normalize_path(base_path() . DIRECTORY_SEPARATOR . SHINEOS_USERFILES_FOLDER_NAME . DIRECTORY_SEPARATOR);
    }
    return $folder;
}

/**
 * Get the user file URL
 * @param null
 * @return string
 *          User File URL
 * @package Paths
 */
function userfiles_url()
{
    static $folder;
    if (!$folder) {
        $folder = site_url() . SHINEOS_USERFILES_FOLDER_NAME . DIRECTORY_SEPARATOR;
    }
    return $folder;
}

/**
 * Get the Media base url
 * @param null
 * @return string
 *          Media base url
 * @package Paths
 */
function media_base_url()
{
    static $folder;
    if (!$folder) {
        $folder = userfiles_url() . (SHINEOS_MEDIA_FOLDER_NAME . '/');
    }
    return $folder;
}

/**
 * Get the Media base path
 * @param null
 * @return string
 *          Media base path
 * @package Paths
 */
function media_base_path()
{
    static $folder;
    if (!$folder) {
        $folder = userfiles_path() . (SHINEOS_MEDIA_FOLDER_NAME . DIRECTORY_SEPARATOR);
    }
    return $folder;
}

/**
 * Get the upload base path
 * @param null
 * @return string
 *          Upload base path
 * @package Paths
 */
function upload_base_path()
{
    static $folder;
    if (!$folder) {
        $folder = userfiles_path() . (SHINEOS_UPLOADS_FOLDER_NAME . DIRECTORY_SEPARATOR);
    }
    return $folder;
}

/**
 * Get the Core path
 * @param null
 * @return string
 *          Core path
 * @package Paths
 */
function core_path()
{
    static $folder;
    if (!$folder) {
        $folder = normalize_path(base_path() . DIRECTORY_SEPARATOR . SHINEOS_SYSTEM_MODULE_FOLDER . DIRECTORY_SEPARATOR . SHINEOS_SYSTEM_CORE_FOLDER . DIRECTORY_SEPARATOR);
    }
    return $folder;
}

/**
 * Get the Modules path
 * @param null
 * @return string
 *          Modules path
 * @package Paths
 */
function modules_path()
{
    static $folder;
    if (!$folder) {
        $folder = normalize_path(base_path() . DIRECTORY_SEPARATOR . SHINEOS_MODULES_FOLDER_NAME . DIRECTORY_SEPARATOR);
    }
    return $folder;
}

/**
 * Get the Plugins path
 * @param null
 * @return string
 *          Plugins path
 * @package Paths
 */
function plugins_path()
{
    static $folder;
    if (!$folder) {
        $folder = normalize_path(base_path() . DIRECTORY_SEPARATOR . SHINEOS_PLUGINS_FOLDER_NAME . DIRECTORY_SEPARATOR);
    }
    return $folder;
}

/**
 * Get the Widgets Path
 * @param null
 * @return string
 *          Widgets Path
 * @package Paths
 */
function widgets_path()
{
    static $folder;
    if (!$folder) {
        $folder = normalize_path(base_path() . DIRECTORY_SEPARATOR . SHINEOS_WIDGETS_FOLDER_NAME . DIRECTORY_SEPARATOR);
    }
    return $folder;
}

/**
 * Get the Elements Path
 * @param null
 * @return string
 *          Elements Path
 * @package Paths
 */
function elements_path()
{
    static $folder;
    if (!$folder) {
        $folder = (userfiles_path() . SHINEOS_ELEMENTS_FOLDER_NAME . DIRECTORY_SEPARATOR);
    }
    return $folder;
}

/**
 * Get the Uploads URL
 * @param null
 * @return string
 *          Uploads url
 * @package Paths
 */
function uploads_url()
{
    static $folder;
    if (!$folder) {
        $folder = url(SHINEOS_USERFILES_FOLDER_NAME. DIRECTORY_SEPARATOR .SHINEOS_UPLOADS_FOLDER_NAME . DIRECTORY_SEPARATOR);

    }
    return $folder;
}

/**
 * Get the Modules url
 * @param null
 * @return string
 *          Modules url
 * @package Paths
 */
function modules_url()
{
    static $folder;
    if (!$folder) {
        $folder = site_url(SHINEOS_USERFILES_FOLDER_NAME . '/' . SHINEOS_MODULES_FOLDER_NAME . '/');
    }
    return $folder;
}

/**
 * Get the Plugins url
 * @param null
 * @return string
 *          Plugins url
 * @package Paths
 */
function plugins_url()
{
    static $folder;
    if (!$folder) {
        $folder = site_url(SHINEOS_USERFILES_FOLDER_NAME . '/' . SHINEOS_PLUGINS_FOLDER_NAME . '/');
    }
    return $folder;
}

/**
 * Get the Widgets url
 * @param null
 * @return string
 *          Widgets url
 * @package Paths
 */
function widgets_url()
{
    static $folder;
    if (!$folder) {
        $folder = site_url(SHINEOS_USERFILES_FOLDER_NAME . '/' . SHINEOS_WIDGETS_FOLDER_NAME . '/');
    }
    return $folder;
}

/**
 * Get the Templates path
 * @param null
 * @return string
 *          Templates path
 * @package Paths
 */
function templates_path()
{
    static $folder;
    if (!$folder) {
        $folder = (userfiles_path() . SHINEOS_TEMPLATES_FOLDER_NAME . DIRECTORY_SEPARATOR);
    }
    return $folder;
}

/**
 * Get the Templates url
 * @param null
 * @return string
 *          Templates url
 * @package Paths
 */
function templates_url()
{
    static $folder;
    if (!$folder) {
        $folder = site_url(SHINEOS_USERFILES_FOLDER_NAME . '/' . SHINEOS_TEMPLATES_FOLDER_NAME . '/');
    }
    return $folder;
}

/**
 * Get the Admin url
 * @param null
 * @return string
 *          Admin url
 * @package Paths
 */
function admin_url($add_string = false)
{
    return site_url() . '/' . $add_string;
}

/**
 * Get the Includes url
 * @param null
 * @return string
 *          Includes url
 * @package Paths
 */
function shineos_includes_url()
{
    static $folder;
    if (!$folder) {
        $folder = site_url() . 'public/dist/';
    }
    return $folder;
}

/**
 * Get the Includes path
 * @param null
 * @return string
 *          Includes path
 * @package Paths
 */
function shineos_includes_path()
{
    static $folder;
    if (!$folder) {
        $folder = modules_path() . SHINEOS_SYSTEM_MODULE_FOLDER . '/assets/';
    }
    return $folder;
}

/**
 * Get the root path
 * @param null
 * @return string
 *          root path
 * @package Paths
 */
function shineos_root_path()
{
    static $folder;
    if (!$folder) {
        $folder = public_path();
    }
    return $folder;
}
