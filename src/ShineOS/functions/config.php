<?php

/**
 * Check if ShineOS is installed
 *
 * @param null
 * @return boolean status of installation
 *
 * @package Config
 *
 */
function shineos_is_installed()
{
    static $is = false;

    if ($is == false) {
        $is = Config::get('shineos.is_installed');
    }
    return (bool)$is;
}

/**
 * Check if ShineOS is in maintenance mode
 *
 * @param null
 * @return boolean status of maintenance
 *
 * @package Config
 *
 */
function shineos_is_onmaintenance()
{
    static $is = false;

    if ($is == false) {
        $is = Config::get('shineos.on_maintenance');
    }
    return (bool)$is;
}
