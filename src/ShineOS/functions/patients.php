<?php

/* Functions for Patients */

use Shine\Libraries\FacilityHelper;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\FacilityPatientUser;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;

    /**
     * Get a patient record using Patient ID
     * @param  string $id        patient_id
     * @param  string $attribute modelObject name
     * @return mixed
     * @package Patients
     */
    function findPatientByPatientID($id)
    {
        return patients::where('patient_id', '=', $id)->first();
    }

    /**
     * Get complete patient record including all related information
     * Alerts; Allergies; Disabilities; Contact; DeathInfo
     * using Patient ID
     * @param  string $id Patient ID
     * @return [array] Complete Patient record
     * @package Patients
     */
    function getCompletePatientByPatientID($id)
    {
        return Patients::with('patientAlert','patientAllergies','patientMedicalHistory','patientDisabilities','patientContact','patientDeathInfo','patientEmergencyInfo','patientPhilhealthInfo')->where('patient_id','=', $id)->first();
    }

    /**
     * Get complete patient record including all related information
     * Alerts; Allergies; Disabilities; Contact; DeathInfo
     * using Facility ID
     * @param  string $id Facility ID
     * @return [array] Complete Patient record
     * @package Patients
     */
    function getAllCompletePatientsByFacility($order = NULL, $dir = 'asc')
    {
        $facilityInfo = FacilityHelper::facilityInfo();
        $user = Session::get('user_details');
        $bhs = NULL;
        if($user->facilityUser){
            foreach($user->facilityUser as $fuser) {
                if($fuser->facility_id == $facilityInfo->facility_id AND $fuser->catchment_area_id != NULL) {
                    //this is a BHS user
                    $bhs = $fuser->catchment_area_id;
                }
            }
        }
        if($bhs) {
            $fus = FacilityUser::where('catchment_area_id', $bhs)->lists('facilityuser_id')->toArray();
            if($fus!=""){
                $sql = "patients.deleted_at IS NULL AND a.facilityuser_id IN (" . implode(',', $fus) . ") ";
            } else {
                $sql = NULL;
            }
        } else {
            $sql = "patients.deleted_at IS NULL";
        }
        
        if($sql) {
            if($order) {
                $sql .= ' order by patients.'.$order.' '.$dir;
            }

            $patients = Patients::with('patientContact','facilityUser','patientDeathInfo')
                ->whereHas('facilityUser', function($query) use ($facilityInfo) {
                    $query->where('facility_id', '=', $facilityInfo->facility_id);
                })
                ->whereRaw( $sql )
                ->get();

            return $patients;
        } else {
            return NULL;
        }
    }

    /**
     * Get all patients of the Facility of current user
     * @return [array] Array of patient records
     * @package Patients
     */

    function getAllPatientsByFacility($order = NULL, $search = NULL, $dir = NULL, $limit = NULL, $offset = NULL)
    {
        $facilityInfo = FacilityHelper::facilityInfo();
        $user = Session::get('user_details');
        $bhs = NULL;
        if($user->facilityUser){
            foreach($user->facilityUser as $fuser) {
                if($fuser->facility_id == $facilityInfo->facility_id AND $fuser->catchment_area_id != NULL) {
                    //this is a BHS user
                    $bhs = $fuser->catchment_area_id;
                }
            }
        }
        if($bhs) {
            $fus = FacilityUser::where('catchment_area_id', $bhs)->lists('facilityuser_id')->toArray();
            if($fus) {
                $sql = " `deleted_at` IS NULL AND `facilityuser_id` IN (" . implode(',', $fus) . ") ";
            } else {
                $sql = NULL;
            }
        } else {
            $sql = " `deleted_at` IS NULL AND `facility_id` = '".$facilityInfo->facility_id."'";
        }
        if($sql){
            if($search) {
                $sql .= ' AND (`first_name` LIKE "%'.$search.'%" OR `last_name` LIKE "%'.$search.'%" OR `middle_name` LIKE "%'.$search.'%" OR `birthdate` LIKE "%'.$search.'%" OR `family_folder_name` LIKE "%'.$search.'%" ';

                //try to get barangay
                $brgy = getBrgyCode($search);
                if($brgy) {
                    $sql .= ' OR `barangay` = "'.$brgy.'" ';
                }
                $sql .= ')';
            }

            $sql .= ' GROUP BY `patient_id` ';

            if($order){
                if($order == 'barangay'){
                    $sql .= ' order by `barangay` '.$dir;
                } elseif($order == 'family_folder_name'){
                    $sql .= ' order by `'.$order.'` '.$dir;
                } else {
                    $sql .= ' order by `'.$order.'` '.$dir;
                }
            }
            if($limit) {
                $sql .= ' limit '.$limit;
                if($offset) {
                    $sql .= ' offset '.$offset;
                }
            }

            $patients = DB::table('patients_view')
                ->whereRaw( $sql )
                ->get();

            return $patients;
        } else {
            return NULL;
        }
    }

    /**
     * Count all patients of the Facility of current user
     * @return [int] Count of patient records
     * @package Patients
     */
    function countAllPatientsByFacility()
    {
        $bhs = NULL;
        $facilityInfo = FacilityHelper::facilityInfo();
        $user = Session::get('user_details');
        $bhs = NULL;
        if($user->facilityUser){
            foreach($user->facilityUser as $fuser) {
                if($fuser->facility_id == $facilityInfo->facility_id AND $fuser->catchment_area_id != NULL) {
                    //this is a BHS user
                    $bhs = $fuser->catchment_area_id;
                }
            }
        }

        if($bhs) {
            //if BHS user, let us get all users of the BHS and list them
            $fus = FacilityUser::where('catchment_area_id', $bhs)->lists('facilityuser_id')->toArray();
            if($fus){
                $sql = "SELECT count(DISTINCT(`patient_id`)) as c FROM `patients_view` a
WHERE a.facilityuser_id IN (" . implode(',', $fus) . ")
AND a.deleted_at IS NULL";
            } else {
                $sql = NULL;
            }
        } else {
            //this is an RHU account, get all
            $sql = "SELECT count(DISTINCT(`patient_id`)) as c FROM `patients_view` a
WHERE a.facility_id = '".$facilityInfo->facility_id."'
AND a.deleted_at IS NULL";
        }

        if($sql) {
            $patients = DB::select( DB::raw( $sql ));
        return $patients[0]->c;
        } else {
            return 0;
        }
    }

    /**
     * Get Patient ID
     * using Healthcareservice ID
     * @param  string $id Healthcareservice ID
     * @return string $patient_id Patient ID
     * @package Patients
     */
    function getPatientIDByHealthcareserviceID($id)
    {
        $hc_service = Healthcareservices::where('healthcareservice_id','=',$id)->first();
        $facilitypatientuser_id = $hc_service->facilitypatientuser_id;
        $facility_patient_user = FacilityPatientUser::where('facilitypatientuser_id','=',$facilitypatientuser_id)->first();
        $patient_id = $facility_patient_user->patient_id;
        return $patient_id;
    }

    /**
     * Get Philheatlh Category ID
     * @param  string $category Category Name
     * @return string Category ID
     * @package Patients
     */
    function getPhicCategory($category){
        switch($category) {
            case $category ="01" : return "FE - Private - Permanent Regular"; break;
            case $category ="02" : return "FE - Private - Casual"; break;
            case $category ="03" : return "FE - Private - Contract/Project Based"; break;
            case $category ="04" : return "FE - Govt - Permanent Regular"; break;
            case $category ="05" : return "FE - Govt - Casual"; break;
            case $category ="06" : return "FE - Govt - Contract/Project Based"; break;
            case $category ="07" : return "FE - Enterprise Owner"; break;
            case $category ="08" : return "FE -Household Help/Kasambahay"; break;
            case $category ="09" : return "FE - Family Driver"; break;
            case $category ="10" : return "IE - Migrant Worker - Land Based"; break;
            case $category ="11" : return "IE - Migrant Worker - Sea Based"; break;
            case $category ="12" : return "IE - Informal Sector"; break;
            case $category ="13" : return "IE - Self Earning Individual"; break;
            case $category ="14" : return "IE - Filipino with Dual Citizenship"; break;
            case $category ="15" : return "IE - Naturalized Filipino Citizen"; break;
            case $category ="16" : return "IE - Citizen of other countries working/residing/studying in the Philippines"; break;
            case $category ="17" : return "IE - Organized Group"; break;
            case $category ="18" : return "Indigent - NHTS-PR"; break;
            case $category ="19" : return "Sponsored - LGU"; break;
            case $category ="20" : return "Sponsored - NGA"; break;
            case $category ="21" : return "Sponsored - Others"; break;
            case $category ="22" : return "Lifetime Member - Retiree/Pensioner"; break;
            case $category ="23" : return "Lifetime Member - With 120 months contribution and has reached retirement age"; break;
        }
    }

    /**
     * Get a Barangays within an RHU
     * @param  null
     * @return array Array of Barangays
     * @package Patients
     */
    function getPatientBrgys() { //revised
        $facilityInfo = FacilityHelper::facilityInfo();
        $brgys = DB::table('lov_barangays')
                ->select('barangay_code','barangay_name')
                ->where('city_code',$facilityInfo->facility_contact->city)
                ->orderBy('barangay_name')
                ->get();
        return $brgys;
    }

    /**
     * Get a patient Full Name using Patient ID
     * @param  string $id        patient_id
     * @param  string $attribute modelObject name
     * @return mixed
     * @package Patients
     */
    function returnFullNameOfPatientID($id)
    {
        $patient = patients::where('patient_id', '=', $id)->first();
        if($patient) {
            return $patient->first_name." ".$patient->last_name;
        } else {
            return NULL;
        }
    }
