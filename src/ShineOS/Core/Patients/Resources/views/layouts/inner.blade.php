<?php
    $pid = NULL;
    if(isset($patient))
    {
        $pid = $patient->patient_id;
    }
    $user = Session::get('user_details');
    $patientfacilityuser = ShineOS\Core\Patients\Entities\FacilityPatientUser::where('patient_id', $pid)->where('facilityuser_id', $user->facilityUser[0]->facilityuser_id)->first();
    $hidden = '';
    if($patientfacilityuser AND $patientfacilityuser->owner == '0') {
        $hidden = 'disabled';
    }

?>
@extends('layout.master')

@section('page-header')
    <section class="content-header">
      <h1 class="text-overflow">
          <i class="fa fa-fw fa-user"></i> @yield('header-content')
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url() }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{ url('records') }}">Records</a></li>
        <li class="active"><a href="{{ url('patients/'.$pid) }}">Patient Dashboard</a></li>
      </ol>
    </section>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">@yield('patient-title')</h3>
            <div class="box-tools pull-right">
            @if( isset($action) AND $action == 'view' )
                  <button class="btn bg-maroon btn-sm" href="" onclick="location.href='{{ url('healthcareservices/add', [$pid]) }}'" >Add Healthcare <i class="fa fa-stethoscope fa-lg"></i></button>
                  <button class="btn btn-warning btn-sm deadPatient-button deathModal {{ $hidden }}" data-id="{{ $pid }}" data-toggle="modal" data-target="#deathModal">Declare Dead</button>
                  <button class="btn btn-primary btn-sm" href="" onclick="location.href='{{ url('patients', [$pid]) }}'" >Patient Dashboard</button>
            @elseif( isset($action) AND $action == 'add' )
            @else
                <button class="btn btn-primary btn-arrow-right" href="" onclick="location.href='{{ url('patients/view', [$pid]) }}'">Back to Patient Profile</button>
            @endif

            </div>

            @yield('patient-info')
        </div>

        <div class="box-body patient-form">
            @yield('patient-content')
        </div><!-- /.box-body -->

        <div class="box-footer">
            @yield('patient-footer')
        </div><!-- /.box-footer-->
    </div>
@stop

@section('before_validation_scripts')

@stop

@section('scripts')
    <script>
        var patientId = "{{ $patient->patient_id or NULL }}";
    </script>
    {!! HTML::script('public/dist/js/pages/patients/patients.js') !!}
@stop
