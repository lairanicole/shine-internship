<?php if($action == "view") { ?>
    <ul class="nav nav-tabs nav-tabs-responsive hidden"  id="step_visualization" role="tablist">
         <li class="active"><a href="#basic" data-toggle="tab" role="tab" aria-expanded="true"><span class="text">Basic</span></a></li>
         <li role="presentation" class="next"><a href="#location" data-toggle="tab" role="tab"><span class="text">Location</span></a></li>
         <li role="presentation" class="next"><a href="#health" data-toggle="tab" role="tab"><span class="text">Health Info</span></a></li>
         <li role="presentation" class="next" id="plugin-insert"><a href="#history" data-toggle="tab" role="tab"><span class="text">Past Medical History</span></a></li>
         @if($plugs)
              @foreach($plugs as $plug)
                @if($plug['plugin_location'] == 'tab')
                    <li role="presentation" class="next"><a href="#{{ strtolower($plug['plugin']) }}" data-toggle="tab" role="tab"><span class="text">{{ $plug['title'] }}</span></a></li>
                @endif
              @endforeach
         @endif
         <li role="presentation" class="next"><a href="#notifications" data-toggle="tab" role="tab"><span class="text">Record Settings</span></a></li>
         <li role="presentation" class="next"><a href="#photos" class="text-lg" data-toggle="tab" role="tab"><span class="text">Photo</span></a></li>

         <li role="presentation" class="dropdown special">
            <a href="#" id="myTabDrop1" class="dropdown-toggle {{ $hidden }}" data-toggle="dropdown" aria-controls="myTabDrop1-contents"><span class="text"><i class="fa fa-plus"></i> Additional Patient Data  </span></a>
            <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                @if($plugs)
                  @foreach($plugs as $plug)
                    @if($plug['plugin_location'] == 'newtab' AND !$plug['pdata'])
                        <li><a href="{{ url('plugin/call/'.$plug['folder'].'/'.$plug['plugin'].'/view/'.$patient->patient_id) }}" role="tab"> + Add {{ $plug['title'] }} </a></li>
                    @endif
                    @if($plug['plugin_location'] == 'newdata')
                        <li><a href="{{ url('plugin/call/'.$plug['folder'].'/'.$plug['plugin'].'/view/'.$patient->patient_id) }}" role="tab">@if(!$plug['pdata']) + Add {{ $plug['title'] }} @else - View {{ $plug['title'] }} @endif</a></li>
                    @endif
                  @endforeach
                @endif
            </ul>
        </li>
    </ul>
<?php } ?>
<?php if($action == "add") { ?>
    <ul id="step_visualization" class="nav nav-tabs new-record">
         <li id="basictab" class="active"><a class="disabled" data-toggle="tab" role="tab" aria-expanded="true">Basic</a></li>
         <li id="personaltab"><a class="disabled" data-toggle="tab" role="tab"><span class="text">Personal</span></a></li>
         <li id="locationtab"><a class="disabled" data-toggle="tab" role="tab"><span class="text">Location</span></a></li>
         <li id="healthtab"><a class="disabled" data-toggle="tab" role="tab"><span class="text">Health Info</span></a></li>
         <li id="historytab"><a class="disabled" data-toggle="tab" role="tab"><span class="text">Medical History</span></a></li>
         @if($plugs)
              @foreach($plugs as $plug)
                @if($plug['plugin_location'] == 'tab')
                    <li id="{{ strtolower($plug['title']) }}tab"><a class="disabled" data-toggle="tab" role="tab"><span class="text"> {{ $plug['title'] }} </span></a></li>
                @endif
              @endforeach
         @endif
         <li id="notificationstab"><a class="disabled" data-toggle="tab" role="tab"><span class="text">Record Settings</span></a></li>
         <li id="photostab"><a class="disabled" data-toggle="tab" role="tab"><span class="text">Photo</span></a></li>
    </ul>
<?php } ?>
