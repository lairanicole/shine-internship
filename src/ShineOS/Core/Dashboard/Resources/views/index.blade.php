@extends('layout.master')
@section('title') SHINE OS+ @stop

@section('content')
    @if (Session::has('warning'))
        <div class="alert alert-dismissible alert-warning">
            <p>{{ Session::get('warning') }}</p>
        </div>
    @endif
    <div class="jumbotron"><!--Jumbotron / Welcome Message-->
        <div class="welcome-widget">
            @if($facilityInfo->customizations != NULL)
            <?php
                $customizations = json_decode($facilityInfo->customizations);
            ?>
            <h1 class="welcome"><b>{{ $customizations->appTitle }}</b></h1>
            @else
            <h1 class="welcome">Welcome to <b>SHINE</b><sup class="text-shine-green">OS+</sup></h1>
            @endif

            <div class="col-xs-12 col-sm-6 col-md-6 pull-left">
                <a href="{{ url('/patients/add')}}" class="ajax-link btn btn-primary">Create New Patient</a>
                <a href="{{ url('/records/search')}}" class="ajax-link btn btn-warning"><i class="fa fa-search"></i> | Advanced Search</a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 text-right pull-right">
                <a href="http://www.shine.ph/support/" class="btn btn-danger marginr-10" target="new">Get Support</a>
                <a href="http://www.shine.ph/shineos/test-drive/" class="btn btn-info" target="new">Take a Tour</a>
            </div>
        </div>
    </div><!--./End Jumbotron-->
    
    {!! AsyncWidget::run('\ShineOS\Core\Dashboard\Widgets\profileCompleteness\ProfileCompleteness') !!}

    <div class="row">
        <div class="col-sm-6">
            {!! AsyncWidget::run('\ShineOS\Core\Dashboard\Widgets\TotalCount\TotalCountBox') !!}
            {!! AsyncWidget::run('\ShineOS\Core\Dashboard\Widgets\Analytics\analytics') !!}
            {!! AsyncWidget::run('\ShineOS\Core\Dashboard\Widgets\Tugon\TugonWidget') !!}
        </div><!--./col-md-6-->
        <div class="col-sm-6 padleft0">
            {!! AsyncWidget::run('\ShineOS\Core\Dashboard\Widgets\VisitList\VisitList') !!}
            {!! AsyncWidget::run('\ShineOS\Core\Dashboard\Widgets\News\newsWidget') !!}
        </div><!--./col-md-6-->
    </div><!--./row-->
@stop



