<?php
namespace ShineOS\Core\Dashboard\Widgets\Tugon;

use Arrilot\Widgets\AbstractWidget;
use Shine\Repositories\Eloquent\UserRepository as BaseRepository;
use Shine\Repositories\Contracts\UserRepositoryInterface;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\UserHelper;
use View, Config, Session;

/**
 * Widget for the total number of records per module
 */
class TugonWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */

    protected $config = [];

    public function placeholder()
    {
        $loading = '<div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-cog fa-spin fa-fw"></i> Loading SMS Stats widget...</h3>
                </div>
            </div>';


        return $loading;
    }

    /**
     * The repository object.
     *
     * @var object
     */
    private $baseRepository;

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(BaseRepository $baseRepository)
    {

        $this->baseRepository = $baseRepository;

        $id = UserHelper::getUserInfo();
        $facility = Session::get('facility_details');
        $fac = FacilityHelper::facilityInfo();

        $province = $facility->facilityContact->province."00000"; //'063000000';
        //$data['city_code'] = $facility->facilityContact->city."000";
        $year = date('Y');

        // Test Voucher: 201-110001-18I12
        $url = "https://fassster.tugon.ph/api/reports";
        $result = NULL;
        
        $result = @file_get_contents('https://fassster.tugon.ph/api/reports?year='.$year.'&province_code='.$province);
        
        if($result!=NULL) {
            $res = json_decode($result);
        } else {
            $res = 'None';
        }
        View::addNamespace('tugon_box', 'src/ShineOS/Core/Dashboard/Widgets/Tugon');
        return view("tugon_box::index", [
            'config' => $this->config,
            'tugon_count' => $res,
            'tugon_month' => date('m'),
            'tugon_month_title' => date('F'),
            'tugon_year' => date('Y')
        ]);
    }
/*
    function _sendCurl($url, $data)
    {
        if(!$data) {
            return NULL;
        }

        $curl = curl_init();

        $postvars = '';
        foreach($data as $key=>$value) {
            if(is_array($value)) {
                $postvars .= $key . "=" . http_build_query($value) . "&";
            } else {
                $postvars .= $key . "=" . $value . "&";
            }
        }

        curl_setopt($curl,CURLOPT_URL,$url);
        curl_setopt($curl,CURLOPT_POST, 0);                //0 for a get request
        curl_setopt($curl,CURLOPT_POSTFIELDS,$postvars);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($curl,CURLOPT_TIMEOUT, 20);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            return $err;
        } else {
            return json_decode($response, true);
        }
    }*/
}
