<?php
    $tugonDis = array(
        "AP"=>"Abdominal pain",
        "CP"=>"Chest pain",
        "FP"=>"Foot or ankle pain",
        "HP"=>"Hip pain",
        "JP"=>"Joint or muscle pain",
        "KP"=>"Knee pain",
        "LP"=>"Lower back pain",
        "NP"=>"Neck pain",
        "PP"=>"Pelvis pain",
        "SP"=>"Shoulder pain",
        "CG"=>"Cough",
        "BR"=>"Difficulty breathing and wheezing",
        "SW"=>"Difficulty swallowing",
        "NC"=>"Nasal congestion and cold",
        "SB"=>"Shortness of breath",
        "ST"=>"Sore throat",
        "BS"=>"Blood in stool",
        "CS"=>"Constipation",
        "DI"=>"Diarrhea and loose stool",
        "UR"=>"Urinary problems",
        "DH"=>"Decreased hearing",
        "DZ"=>"Dizziness",
        "ER"=>"Earache",
        "EY"=>"Eye discomfort and redness",
        "HD"=>"Headache",
        "NV"=>"Nausea or vomiting",
        "VI"=>"Vision Problem",
        "FV"=>"Fever",
        "FS"=>"Foot or leg swelling",
        "PA"=>"Heart palpitations",
        "NU"=>"Numbness or tingling in hands",
        "SR"=>"Skin rashes"
    );

    if($tugon_count != 'None' AND $tugon_count->data){
        foreach($tugon_count->data as $k => $smss) {
            if( count($smss->Diseases) > 0 ) {
                $tots = count($smss->Diseases);
                foreach($smss->Diseases as $d => $disease) {
                    $mdisease[$d] = $disease->Disease;
                    $mcount[$d] = $disease->Count[$tugon_month-1];
                }
            }
        }
    }
?>
<style>
    .smsItem {
        border-bottom: 1px dotted #BBB;
        padding: 5px 0;
        margin: 0 10px;
    }
    .smsTotal {
        font-size: 60px;
        float: left;
        width: 30%;
        line-height: 1;
        text-align: right;
        border-right: 2px solid #BBB;
        padding-right:10px;
    }
    .smsLabel {
        width: 70%;
        float: left;
        font-size: 18px;
        text-align: left;
        padding-left:10px;
    }
    span.small {
        color: #DEDEDE;
    }
</style>
<div class="box box-primary bg-blue" id="tugon_stats">
    <div class="box-header ui-sortable-handle">
        <i class="fa fa-mobile text-white"></i>
        <h3 class="box-title text-white">SMS Surveillance Stats</h3>
    </div>

    <div class="box-body">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12 topGroup">
                @if(isset($mdisease))
                    <h4 id="topTitleLabel">Top Symptoms | <span class="small">Reported {{ $tugon_month_title }} {{ $tugon_year }}</span></h4>
                    <br />
                    @foreach($mdisease as $r => $disea)
                    <div class="col-md-6 smsItem">
                        <p class="smsTotal">{{ $mcount[$r] }}</p>
                        <p class="smsLabel">{{ ucfirst($tugonDis[$disea]) }}</p>
                    </div>
                    @endforeach
                @else
                    <h4>Your location has no reported SMS Surveillance Reports</h4>
                @endif
            </div>
        </div><!-- /.row -->
    </div>
</div><!--./box-->
