<div class="box box-primary">
    <div class="box-header">
        <i class="fa fa-newspaper-o"></i><h3 class="box-title text-shine-blue boxTitle">News &amp; Updates</h3>
    </div>

    <div class="box-body">

        <!-- https://www.philhealth.gov.ph/advisories/2018/adv2018-0024.pdf -->
        <div class="col-md-12">
<!--             <div class="bg-danger pad20">
                <h3 class="smartblue"> Special Announcement<small class="text-muted pull-right text-small">Jan. 28, 2019</small></h3> 
                <p> 
                <strong>Electronic Claims & PCB Engagement Form</strong>
                <br><br>
                We are pleased to announce to everyone that SHINE OS+ is now certified for Electronic Claims submission. Should you wish to activate your eClaims module, please fill-out and sign the attached forms and submit it to us through e-mail at support@medixserve.com, with Subject <strong>ECLAIMS-APPLICATION</strong>.
                <br><br>
                <a href="https://www.shine.ph/shineph/docs/eclaims-engagement.zip"> eClaims Certification Documents </a>
                <br><br>
                For RHUs who need to renew their Engagement forms, please download the form here:
                <a href="https://www.shine.ph/shineph/docs/HITP_EMR_Engagement_Form.pdf"> PCB Engagement Form </a>
                </p>   
            </div> -->

<!--             <h4 class="smartblue"> Attention to SHINE OS+ RHUs <small class="text-muted pull-right text-small">Nov. 09, 2018</small></h4> 
            <p> 
            Adjustments are being made to serve you better. For concerns and inquiries, pls send email at support@medixserve.com, join the SHINEOS+ FB page at <a href="https://www.facebook.com/SHINEOSPLUS/" target="blank">@SHINEOSPLUS</a> and our new medixServe FB page at <a href="https://www.facebook.com/MediXserve/" target="blank">@mediXserve</a> and send us a message there, or send us an SMS at 09175148292.
            </p>    -->


<!--             <h4 class="smartblue"> eClaims Certification <small class="text-muted pull-right text-small">July 3, 2018</small></h4> 
            <p> 
                Please be advised that SHINE OS+ is currently undergoing software certification for the electronic submission of claims. Should you wish to get your facility certified for eClaims, you may accomplish the required documents and submit it to your respective PhilHealth Regional Offices. The template documents can be downloaded through this link: <a href="https://www.shine.ph/shineph/docs/eClaimsDocs.zip"> eClaims Documents </a>

                <br><br>
                Upon submission, please send us a notification and a copy of the documents at admin@shine.ph.
            </p>   
            <hr> -->


            <h4 class="smartblue"> PhilHealth Online Services <small class="text-muted pull-right text-small">June 12, 2018</small></h4> 
            <p> For online services of PhilHealth, including access your <strong>electronic SAP</strong>, you may visit the their site at <a href="https://www.philhealth.gov.ph/services/">https://www.philhealth.gov.ph/services/</a></p>
            <p> To check the availability of PhilHealth eSAP servers, you may refer to following link: <a href="https://checkecws.philhealth.gov.ph/checkws/">https://checkecws.philhealth.gov.ph/checkws/</a></p>   
            <hr>

                    </div>
    </div><!-- /.box-body -->
    @if(!empty($visit_list))
    <div class="box-footer text-center">
        
    </div><!--/.box-footer-->
    @endif

</div><!--/. end consultations-->

<script>

</script>
