<?php
namespace ShineOS\Core\Dashboard\Widgets\News;

use Arrilot\Widgets\AbstractWidget;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\FacilityRepositoryInterface;
use Shine\Libraries\HealthcareservicesHelper;
use Shine\Libraries\FacilityHelper;

use View;

class newsWidget extends AbstractWidget
{
    private $UserRepository;
    private $healthcareRepository;

    /**
     * The configuration array.
     *
     * @var array
     */
    public $reloadTimeout = 60;
    
    protected $config = [];

    public function placeholder()
    {
        $loading = '<div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-cog fa-spin fa-fw"></i> Loading Queue widget...</h3>
                </div>
            </div>';

        return $loading;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(UserRepository $UserRepository, HealthcareRepository $healthcareRepository)
    {
        $news = NULL; //getNews('today');
        View::addNamespace('visit_list', 'src/ShineOS/Core/Dashboard/Widgets/News');
        return view("visit_list::index", [
            'config' => $this->config,
            'news' => $news
        ]);
    }
}
