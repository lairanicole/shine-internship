<?php namespace ShineOS\Core\Dashboard\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\Users\Entities\Users;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\Utils;
use Shine\Libraries\UserHelper;
use View, Config, Session, Auth, Redirect;

class DashboardController extends Controller {


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $modules =  Utils::getModules();

        # variables to share to all view
        View::share('modules', $modules);
    }

    public function index()
    {
        $user = UserHelper::getUserInfo();
        $facilityInfo = FacilityHelper::facilityInfo(); // get user id
        $userFacilities = FacilityHelper::getFacilities($user);

        if($facilityInfo AND $user) {
            //go to dashboard
            return view('dashboard::index', compact('user', 'facilityInfo','userFacilities'));
        } else {
            //for some reason thi session do no exist, force logout and ask to login again.
            Session::flush();
            // clear cache
            Session::forget('roles');
            Session::forget('user_details');
            Session::forget('facility_details');
            Session::forget('facilityuser_details');
            // logout
            Auth::logout();
            // clear session
            Session::flush();
            return Redirect::to('login');
        }
    }

}
