<?php namespace ShineOS\Core\Users\Libraries;

Use Mail, Session, Redirect;

class UserActivation {

    function __construct(){
    }


    /**
     * Generate random password
     *
     * @return string
     */
    public static function generateActivationCode ( $email = '' )
    {
        return $generatedKey = sha1(mt_rand(10000,99999).time().$email);
    }

    /**
     * Generate user account activation link
     *
     * @return string
     */
    public static function sendUserActivationCode ( $user, $temporary_password = '' ) {
        $contactName = 'SHINE OS+ Administrator';
        $contactEmail = 'support@shineos.com';
        $activation_link = url("activateaccount/user/{$user->activation_code}");

        $data = array(
            'name' => "{$user->first_name} {$user->last_name}",
            'email' => $user->email,
            'activation_link' => $activation_link,
            'temporary_password' => $temporary_password
        );
        Mail::send('users::emails.activation_code', $data, function($message) use ($contactEmail, $contactName, $user, $temporary_password)
        {
            $message->from($contactEmail, $contactName);
            $message->to($user->email, "{$user->first_name} {$user->last_name}")->subject('SHINE OS+ Account Activation');
        });
    }


    /**
     * Generate user account activation link
     *
     * @return string
     */
    public static function sendAdminActivationCode ( $user, $type = NULL, $ownership_type = NULL) {
        $contactName = 'SHINE OS+ Administrator';
        $contactEmail = 'support@shineos.com';
        $activation_link = NULL;
        $activated = true;
        $login_link = url('login');

        $data = array(
            'name' => "{$user->first_name} {$user->last_name}",
            'email' => $user->email,
            'activation_link' => $activation_link,
            'owner' => $ownership_type,
            'activated' => $activated,
            'login_link' => $login_link
        );
        $status = Mail::send('users::emails.admin_activation', $data, function($message) use ($contactEmail, $contactName, $user, $ownership_type, $activated, $login_link)
        {
            $message->from($contactEmail, $contactName);
            $message->to($user->email, "{$user->first_name} {$user->last_name}")->subject('SHINE OS+ Account Activated');
            //add if statement here for government only
            if($ownership_type == 'government') {
                $message->attach('resources/attachment/SHINEOS+_FILES.zip');
            }
        });
    }

    /**
     * Generate user account activation link
     *
     * @return string
     */
    public static function sendAdminCEActivationCode ( $user, $file ) {
        $contactName = 'SHINE OS+ Administrator';
        $contactEmail = 'support@shine.ph';

        $data = array(
            'name' => "{$user->first_name} {$user->last_name}",
            'email' => $user->email,
            'code' => $file.'.txt'
        );

        Mail::send('users::emails.admin_ce_activation', $data, function($message) use ($contactEmail, $contactName, $user, $file)
        {
            $message->from($contactEmail, $contactName);
            $message->to($user->email, "{$user->first_name} {$user->last_name}")->subject('Welcome to SHINE OS+ Community Edition');
            $message->attach( 'public/uploads/'.$file.'.txt' );
        });
    }


    /**
     * Notify registration to **___ admin SHINE OS+ __**
     */
    public static function sendRegistrationNotification ($post) {
        $contactName = 'SHINE OS+ Administrator';
        $contactEmail = 'support@shine.ph';

        $contactName_SHINE = 'SHINE OS+ Administrator';
        $contactEmail_SHINE = 'register@shine.ph';
        $activation_link = url("activateaccount/admin/cloud/{$post['_token']}/{$post['id']}");

        $data = array(
            'registration' => $post,
            'activation_link' => $activation_link
        );

        Mail::send('users::emails.registration_notification', $data, function($message) use ($post, $contactEmail_SHINE, $contactName_SHINE, $contactEmail, $contactName)
        {
            $message->from($contactEmail, $contactName);
            $message->to($contactEmail_SHINE, $contactName_SHINE)->subject('SHINE OS+ Registration Notification ('.$post['ownership_type'].')');
            if(array_key_exists('ptr_file_url', $post)) {
                $message->attach('public/uploads/facility_ptr/'.$post['ptr_file_url']);
            }
        });
    }

    /**
     * Notify registration to Registrant
     */
    public static function sendRegistrationConfirmation ($post) {
        $contactName = 'SHINE OS+ Administrator';
        $contactEmail = 'support@shine.ph';
        $activated = false;
        $data = array(
            'name' => $post['first_name']." ".$post['last_name'],
            'email' => $post['email'],
            'activation_link' => NULL,
            'owner' => $post['ownership_type'],
            'activated' => $activated
        );

        Mail::send('users::emails.admin_activation', $data, function($message) use ($contactEmail, $contactName, $post, $activated)
        {
            $message->from($contactEmail, $contactName);
            $message->to($post['email'], $post['first_name']." ".$post['last_name'])->subject('Welcome to SHINE OS+');
            if($post['ownership_type'] == 'government') {
                $message->attach('resources/attachment/SHINEOS+_FILES.zip');
            }

        });
    }

    private static function print_this( $object = array(), $title = '' ) {
        echo "<hr><h2>{$title}</h2><pre>";
        print_r($object);
        echo "</pre>";
    }
}
