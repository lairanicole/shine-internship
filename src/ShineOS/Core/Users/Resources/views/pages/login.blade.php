<?php
    /** Customizing the login page via customizations
     * 
     * {
     *  "appTitle":"Municipal Health Information System",
     *  "companyName":"Amellar Solutions",
     *  "byLine":"",
     *  "appTopLogo":"amellar/amellar-top-bar.jpg",
     *  "appLogo":"amellar/amellar.png",
     *  "appBlurb":"A  gateway to promote technology base management to reach out and record the medical services rendered by rural health units.  From profiling down to statistical reports to help in the analysis, for the improvement of welfare and health programs.",
     *  "loginstyle":{
     *      "introbox":{
     *          "width":"col-md-7",
     *          "background":"amellar/amellar-bk.jpg",
     *          "opacity":1
     *      },
     *      "logmebox":{
     *          "width":"col-md-5",
     *          "background":"#005E7D",
     *          "opacity":1
     *      }
     *  }
     * }
     */
?>
@extends('users::layouts.masterlogin')
@section('title') SHINE OS+ | Login @stop

@section('heads')
    <style>
        #loginstyle {
            display:none;
        }
        body {
            color: #333;
            background: #285377;
            /*background: -moz-linear-gradient(top, #444444 1%, #111111 100%);
            background: -webkit-linear-gradient(top, #444444 1%,#111111 100%);
            background: linear-gradient(to bottom, #444444 1%,#111111 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#444444', endColorstr='#111111',GradientType=0 );*/
        }
        .background_wrapper {
            background: url({{ asset('public/dist/img/saas-bak.jpg') }}) no-repeat center center;
            background-size: cover;
            left: 0;
            height: 100%;
            overflow: hidden;
            position: absolute;
            top: 0;
            width: 100%;
            z-index: 2;
        }
        .overlay {
            background-color: #000;
            left: 0;
            top: 0;
            position: absolute;
            width: 100%;
            height: 100%;
            z-index: 3;
            opacity: .55;
        }
        .overlay2 {
            background-color: #285377;
            left: 0;
            top: 0;
            position: absolute;
            height: 100%;
            z-index: 3;
            opacity: 1;
        }
        h2.smartlitblue {
            font-size: 2em;
        }
        .modal-content {
            border-radius: 12px;
            overflow:hidden;
        }
        .modal-body {
            padding:0 !important;
        }
        .form-box {
            margin: 25% 17% 0px !important;
        }
        .intro-box {
            margin-top:10% !important;
        }
    </style>
@stop

@if(Config::get('config.mode') == 'cloud' OR Config::get('config.mode') == 'demo' OR Config::get('config.mode') == 'training')
<?php $title = "The First Health Care Exchange System"; ?>
@endif
@if(Config::get('config.mode') == 'ce' OR Config::get('config.mode') == 'stand-alone')
<?php $title = "Community Edition"; ?>
@endif
@if(Config::get('config.mode') == 'developer')
<?php $title = "Developer Edition"; ?>
@endif
<?php
    $version = Config::get('config.version');
?>

@section('content')
    <div class="background_wrapper hidden-xs"></div>
    <div class="overlay hidden-xs"></div>
    <div class="overlay2 hidden-xs col-md-6"></div>

    <div id="loginstyle" class="row">
        <div class="intro-box col-md-6">
            <div class="col-md-10 col-md-offset-1">
            <h1 id="welcomeH1">Welcome to<br /><span class="smartblue">SHINE</span><span class="smartolive">OS+</span> v{{ $version }}</h1>
            <h2 class="smartlitblue" id="subtitleH2">{{ $title }}</h2>
            <p class="lead" id="pLead">SHINE&trade; stands for <strong>Secured Health Information Network and Exchange</strong>. A web and mobile-based system that addresses the data management needs of doctors, nurses, midwives, and other allied health professionals in the Philippines.</p>
            <h3 id="buttonGroup">
                @if(Config::get('config.mode') == 'cloud' OR Config::get('config.mode') == 'training' OR Config::get('config.mode') == 'stand-alone' )
                    <a href="{{ url('registration') }}" class="btn btn-primary text-center" id="registerButton">Register for FREE!</a>
                @endif
                <a href="http://www.shine.ph/" class="btn btn-danger text-center" target="_blank">Learn more</a>
                <a href="http://www.shine.ph/developer" class="btn bg-shine-green text-center" target="_blank">Developer</a>
            </h3>
            </div>
        </div>
        <div class="logme-box col-md-6">
            <div class="col-md-8 col-md-offset-2 form-box" id="login-box">
                <div class="header"><i>SHINE OS+</i></div>
                @if((Config::get('config.mode') == 'ce' OR Config::get('config.mode') == 'stand-alone') AND Config::get('shineos.is_activated') != 1)
                    <div class="body">
                        <h4>Activate Community Edition</h4>
                        <p>Congratulations! One more step and your SHINE OS+ Community Edition is ready for your use. Click the Activate button and upload the activation file you received after your registration. Check your email and download the file.</p>
                        <div class="footer centered">
                            <a href="{{ url('activateaccount/ce') }}" class="btn btn-danger btn-block text-center" data-toggle="modal" data-target="#activateModal">Activate CE</a>
                        </div>
                    </div>
                @else
                    {!! Form::open(array( 'url'=>'login/verify', 'id'=>'loginForm', 'name'=>'loginForm' )) !!}
                    <div class="body">
                        <h4>Account Login</h4>
                        @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <p>{{ Session::get('message') }}</p>
                            </div>
                        @endif

                        @if (Session::has('warning'))
                            <div class="alert alert-dismissible alert-warning">
                                <p>{{ Session::get('warning') }}</p>
                            </div>
                        @endif

                        <p>Please login with your email/username and password below.</p>

                        <div class="form-group">
                            <input type="text" name="identity" id="identity" class="form-control" placeholder="Username or Email" />
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" />
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="remember_me" id="remember_me" value="1" /> Remember me
                        </div>
                    </div>
                    <div class="footer">
                        <p><button type="submit" class="btn btn-warning btn-block">Sign me in</button></p>
                        <p><a href="{{ url('forgotpassword') }}">I forgot my password</a></p>
                    </div>
                {!! Form::close() !!}
                @endif
            </div>
        </div>

        <div class="loginfooter">
            <span class="col-md-1"></span>
            <span class="col-md-5 text-right" id="submenu">
                <a href="http://www.shine.ph/" target="_blank">About</a>
                <a href="http://www.shine.ph/support" target="_blank">Support</a>
                <a href="http://www.shine.ph/developers" target="_blank">Developers</a>
                <a href="http://www.shine.ph/about/privacy-policy/" target="_blank">Privacy</a>
                <a href="http://www.shine.ph/terms-of-use" target="_blank">Terms</a>
                <a href="http://www.shine.ph/about" target="_blank">SHINELabs</a>
                <span>©2014 - 2018</span>
            </span>
        </div>
    </div>

    <div class="modal fade" id="activateModal" tabindex="-1" role="dialog" aria-labelledby="activateModal">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@stop
<?php /*
<!--begin modal window-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">

<div class="modal-body">

  <!--CAROUSEL CODE GOES HERE-->
    <!--begin carousel-->
    <div id="myGallery" class="carousel slide" data-interval="false">
    <div class="carousel-inner">

    <div class="item active">
    <img src="http://lorempixel.com/600/400/nature/1" alt="item1">
    <div class="carousel-caption">
    <h3>Heading 3</h3>
    <p>Slide 0  description.</p>
    </div>
    </div>

    <div class="item">
    <img src="http://lorempixel.com/600/400/nature/2" alt="item2">
    <div class="carousel-caption">
    <h3>Heading 3</h3>
    <p>Slide 1 description.</p>
    </div>
    </div>

    <div class="item">
    <img src="http://lorempixel.com/600/400/nature/3" alt="item3">
    <div class="carousel-caption">
    <h3>Heading 3</h3>
    <p>Slide 2  description.</p>
    </div>
    </div>

    <div class="item">
    <img src="http://lorempixel.com/600/400/nature/4" alt="item4">
    <div class="carousel-caption">
    <h3>Heading 3</h3>
    <p>Slide 3 description.</p>
    </div>
    </div>

    <div class="item">
    <img src="http://lorempixel.com/600/400/nature/5" alt="item5">
    <div class="carousel-caption">
    <h3>Heading 3</h3>
    <p>Slide 4 description.</p>
    </div>
    </div>

    <div class="item">
    <img src="http://lorempixel.com/600/400/nature/6" alt="item6">
    <div class="carousel-caption">
    <h3>Heading 3</h3>
    <p>Slide 5 description.</p>
    </div>
    </div>
    <!--end carousel-inner--></div>

    <!--Begin Previous and Next buttons-->
    <a class="left carousel-control" href="#myGallery" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span></a>
    <a class="right carousel-control" href="#myGallery" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span></a>
    <!--end myGallery--></div>


<!--end modal-content--></div>
<!--end modal-dialoge--></div>
<!--end myModal--></div>
@stop

@section('scripts')
    <!--Latest stable release of jQuery Core Library-->
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

    <!--Bootstrap JS-->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    </script>

    <script>
        $(document).ready(function() {
            $('#myModal').modal('show');
        });
    </script>
@stop
*/ ?>

@section('scripts')
    <script type="text/javascript">
        var customizations = $.cookie('customizations');
        if(customizations) {
            var customs = JSON.parse(customizations);
            console.log(customs);
            if(customs.appTitle != '') {
                $('#welcomeH1').html(customs.appTitle);
                $('#buttonGroup').hide();
                if(customs.byLine) {
                    if(customs.byLine != "none") {
                        $('#subtitleH2').html("By "+customs.byLine);
                    } else {
                        $('#subtitleH2').html("");
                    }
                }
                if(customs.appBlurb) {
                    $('#pLead').html(customs.appBlurb);
                }
                $('div.header').attr('style','background:#FFFFFF url(\'public/uploads/customizations/'+customs.appLogo+'\') no-repeat center center;background-size: auto 65px !important;');
                $('#submenu').html("Powered by<a href='http://www.medixserve.com' target='new'>MediXserve</a> <span>&copy;2014 - <?php echo date("Y"); ?></span>").removeClass('text-right').addClass('text-center');
                $('#loginstyle').css('display','block'); 
                if(customs.loginstyle) {
                    $('.intro-box').removeClass('col-md-6').addClass(customs.loginstyle.introbox.width);
                    $('.logme-box').removeClass('col-md-6').addClass(customs.loginstyle.logmebox.width);
                    
                    $('.overlay2').attr('style','background:#FFFFFF url("public/uploads/customizations/'+customs.loginstyle.introbox.background+'") no-repeat center center; background-size: cover;').removeClass('col-md-6').addClass(customs.loginstyle.introbox.width).css('opacity',customs.loginstyle.introbox.opacity);
                    
                    $('.overlay').attr('style','background-color:'+customs.loginstyle.logmebox.background).css('opacity',customs.loginstyle.logmebox.opacity);
                }
            }
        } else {
            $('#loginstyle').css('display','block'); 
        }
        $(document).ready(function() {
            //$('#myModal').modal('show');
        });

        $("#activateModal").on("show.bs.modal", function(e) {
            $(this).find(".modal-content").html("");
            var link = $(e.relatedTarget);
            console.log(link.attr("href"));
            $(this).find(".modal-content").load(link.attr("href"));
        });
    </script>
@stop
