<?php //dd($userRole);?>
<h4>Change User Role</h4>

 {!! Form::open(array( 'url'=>$modulePath.'/save_role/'.$userInfo->user_id, 'class'=>'form-horizontal' )) !!}
  <div class="box-body">
        <select id="role" name="role" class="form-control">
          @foreach($role as $k => $v)
            <option value="{{ $v['role_id'] }}" @if($userRole!=NULL) @if($userRole->role_name==$v['role_name']) selected @endif @endif> {{ $v['role_name'] }}</option>
            }
          @endforeach 
        </select>
  </div>

  <div class="box-footer">
    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-success pull-right" value="Assign Role" />
  </div><!-- /.box-footer -->
{!! Form::close() !!}