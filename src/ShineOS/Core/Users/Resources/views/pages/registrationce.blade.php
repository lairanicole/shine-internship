@extends('users::layouts.master')
@section('title') SHINE OS+ | Registration @stop
@section('content')

<?php
    //RJBS solution for form data
    //for clean-up
    $enteredData = Session::get('enteredData');
    if($enteredData) {
        $DOH_facility_code = $enteredData['DOH_facility_code'];
        $facility_name = $enteredData['facility_name'];
        $provider_type = $enteredData['provider_type'];
        $ownership_type = $enteredData['ownership_type'];
        $facility_type = $enteredData['facility_type'];
        $phic_accr_id = $enteredData['phic_accr_id'];
        $first_name = $enteredData['first_name'];
        $last_name = $enteredData['last_name'];
        $password = $enteredData['password'];
        $email = $enteredData['email'];
        $phone = $enteredData['phone'];
        $mobile = $enteredData['mobile'];
        $region = $enteredData['region'];
        $province = $enteredData['province'];
        $city = $enteredData['city'];
        $barangay = $enteredData['barangay'];
    } else {
        $DOH_facility_code = '';
        $facility_name = '';
        $provider_type = '';
        $ownership_type = '';
        $facility_type = '';
        $phic_accr_id = '';
        $first_name = '';
        $last_name = '';
        $email = '';
        $phone = '';
        $mobile = '';
        $password = '';
        $region = NULL;
        $province = NULL;
        $city = NULL;
        $barangay = NULL;
    }
?>
<style>
    body {
        color: #333;
        background: #FFF;
        /*background: -moz-linear-gradient(top, #d2d6de 1%, #A9ABB6 100%);
        background: -webkit-linear-gradient(top, #d2d6de 1%,#A9ABB6 100%);
        background: linear-gradient(to bottom, #d2d6de 1%,#A9ABB6 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d2d6de', endColorstr='#A9ABB6',GradientType=0 );*/
    }
    img.shinelogo {
        height: 50px !important;
        margin: auto;
    }
    .form-control {
        background-color: #FFF; !important;
        border-color: #AAA !important;
        color: #333;
    }
    .input-group .input-group-addon {
        background-color: #DDD !important;
        border-color: #AAA !important;
        color: #222;
        cursor: pointer;
    }
    i.fa {
        color:#fff !important;
        font-size: 18px;
        width: 14px;
    }
    #captcha-image {
        opacity: 0.8 !important;
        height: 34px;
    }

    .popover {
        color: #000 !important;
    }
    .fa-refresh {
        margin-left: 10px;
    }
    select option {
        color: #000;
    }
    .help-block {
        position: relative !important;
        text-align: right !important;
        margin-top:0px;
        display: inline-block;
        margin-left: 25px;
    }
    .has-error .form-control {
        margin-bottom:0px;
    }

    .regbtn {
        padding: 8px 10px;
        border-color: #AAA !important;
        height: 36px !important;
    }
    .toggler {
        margin-bottom: 0;
        display: block;
    }
    .has-error .toggler {
        margin-bottom: 13px;
    }
    .btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
        /*border-top-left-radius: 0;
        border-bottom-left-radius: 0;*/
    }
    .input-group div .form-control:first-child {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
    }
    .jumbotron {
        background-color:#FFF;
        color:#333;
        padding:20px !important;
        text-align: center;
        margin:10px;
        box-shadow: 0px 0px 4px rgba(0,0,0,.2);
    }
    .jumbotron h3 {
        font-size: 20px;
    }
    .jumbotron p {
        font-size: 13px !important;
    }
    .reg-jumbotron {
        padding:20px !important;
    }
    .top20 {
        margin-top:20px;
    }
    .bottom20 {
        margin-bottom:20px;
    }
    .for_government, .for_private {
        display: none;
    }
    hr {
        margin-top:3px;
        margin-bottom:3px;
    }
    .input-group .form-control {
        width:50%;
    }
    .filebrowse {
        width: 100%;
        height: 34px;
        padding: 0px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        border: none;
        border-radius: 4px;
        margin:14px 0;
    }
</style>
<div class="container">
    <div class="row top20">
        <div class="reg-jumbotron text-center">
            <img src="{{ asset('public/dist/img/logos/shinelogo-x-big.png') }}" class="shinelogo img-responsive" />
            <h3>Community Edition Registration</h3>
            <p>Registration is subject to validation, review and approval. User accounts will be sent by email once it has been approved within <b>48 hours</b> of registration.<br />
            All <b>fields</b> are <b>required</b> except PhilHealth Accreditation Number for Private providers.</p>
        </div>
    </div>

    <div class="row bottom20">
        <div class="col-md-4 col-md-offset-4 text-center">
            @if (Session::has('message'))
                <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <p>{{ Session::get('message') }}</p>
                </div>
            @endif
            @if (Session::has('warning'))
                <div class="alert alert-dismissible alert-warning">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <p>{{ Session::get('warning') }}</p>
                </div>
            @endif

            <p>Choose what account you have in SHINE OS+.</p>

            <div class="btn-group btn-group-justified toggler" role="group" data-toggle="buttons">
              <label class="btn btn-default acctbtn required">
                <i class="fa fa-check"></i> <input type="radio" class="required account_type" name="account_type" id="" autocomplete="off" value="existing" required="required"> Existing User
              </label>
              <label class="btn btn-default acctbtn required">
                <i class="fa fa-check"></i> <input type="radio" class="required account_type" name="account_type" id="" autocomplete="off" value="new" required="required"> New User
              </label>
            </div>
        </div>

        <fieldset class="col-md-4 col-md-offset-4 text-center" id="oldUser" style="display:none;">
                {!! Form::open(array( 'url'=>$modulePath.'/getactivation', 'id'=>'oldUserForm', 'name'=>'oldUserForm', 'class'=>'form-horizontal' )) !!}
                <h4>Existing User</h4>
                <p>Welcome! Please enter your username(email) and password then click on Download Activation Code. An email will be sent to your email address with your activation code. You will be directed to the download page once your account is confirmed.</p>

                <div class="form-group">
                        <input type="text" name="identity" id="identity" class="form-control" placeholder="Username or Email" />
                </div>
                <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" />
                </div>
                <div class="form-group pull-right">
                    <input type="submit" name="Download" id="btnDownload" class="btn btn-success" value="Download Activation Code" />
                    <a href="{{ url('/') }}" class="btn btn-warning">Cancel</a>
                </div>
                {!! Form::close() !!}
        </fieldset>

        <div class="clearfix">&nbsp;</div>

        <fieldset id="newUser" style="display:none;">
            {!! Form::open(array( 'url'=>$modulePath.'/registerce', 'id'=>'newUserForm', 'name'=>'newUserForm', 'class'=>'form-horizontal' )) !!}
            <div class="row top20">
                <div class="col-md-4 bottom40">
                    <div class="jumbotron">
                        @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <p>{{ Session::get('message') }}</p>
                            </div>
                        @endif
                        @if (Session::has('warning'))
                            <div class="alert alert-dismissible alert-warning">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <p>{{ Session::get('warning') }}</p>
                            </div>
                        @endif

                        <h4>REGISTRATION INFORMATION</h4>

                        <div class="form-group">
                            <div class="btn-group btn-group-justified toggler" data-toggle="buttons">
                              <label class="btn btn-default regbtn required @if($ownership_type == 'government') active @endif">
                                <i class="fa fa-check"></i> <input type="radio" class="required ownership_type" name="ownership_type" id="" autocomplete="off" value="government" required="required" @if($ownership_type == 'government') checked="checked" @endif> Government
                              </label>
                              <label class="btn btn-default regbtn required @if($ownership_type == 'private') active @endif">
                                <i class="fa fa-check"></i> <input type="radio" class="required ownership_type" name="ownership_type" id="" autocomplete="off" value="private" required="required"  @if($ownership_type == 'private') checked="checked" @endif> Private
                              </label>
                            </div>
                        </div>

                        <div class="for_government">
                            <div class="form-group">
                                <input type="text" data-mask="" data-inputmask="'mask': 'DOH000000000099999'" placeholder="DOH Facility Code. Last 5 digits only." class="form-control" id="DOH_facility_code" name="DOH_facility_code" value="{{ $DOH_facility_code }}" />
                            </div>
                            <small>If you do not know your DOH Facility Code, you can check it from this site: <a href="http://nhfr.doh.gov.ph/" target="_blank">NHFR</a></small>
                            <hr />
                            <p class="text-left">You are required to engage us thru a Memorandum of Agreement (MOA), Engagement Form and Service Level Agreement. After registration, you will receive by email all necesssary documents and forms you need to have them sign by your Mayor or Health Officer.</p>
                            <p class="text-left"> You will also need to receive instructions how register to PHIE so you can submit to Philhealth and DOH.</p>
                        </div>

                        <div class="for_private">
                            <p class="text-left">As a private facility or practitioner, we will require you to attached a scanned image of your PTR License during this registration.</p>
                            {!! Form::file('private_provider_ptr', array('id'=>'ptrImage', 'class'=>'filebrowse ptrImage')) !!}
                            <p class="text-left">If you are part of an educational institution or student who want to use SHINE OS+ for your courses, please contact us at inquiry@shine.ph.</p>
                            <p class="text-left">You may also check our demo at <a href="http://www.shine.ph/demo" target="_blank">http://www.shine.ph/demo</a> before registering.</p>
                        </div>

                        <div class="instruct">
                            <p class="text-left">Please choose the type of facility you want to register. There will be specific requirements depending on your chosen type.</p>
                            <p class="text-left">If you are part of an educational institution or student who want to use SHINE OS+ for your courses, please contact us at inquiry@shine.ph.</p>
                            <p class="text-left">You may also check our demo at <a href="http://www.shine.ph/demo" target="_blank">http://www.shine.ph/demo</a> before registering.</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-4 bottom20">
                    <div class="jumbotron">
                        <h4>PROVIDER INFO</h4>
                        <div class="form-group">
                            <input type="text" placeholder="Facility Name" class="form-control required" id="facility_name" value="{{ $facility_name }}" name="facility_name" required />
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="PhilHealth Accreditation Number" class="form-control" id="phic_accr_id" value="{{ $phic_accr_id }}" name="phic_accr_id" />
                        </div>

                        <div class="form-group">
                            <select name="provider_type" id="provider_type" class="required form-control" required>
                                <option value="">Select Provider Type</option>
                                <option value="facility" @if($provider_type == 'facility') selected @endif>Facility</option>
                                <option value="individual" @if($provider_type == 'individual') selected @endif>Individual</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <select name="facility_type" id="facility_type" class="populate placeholder required form-control">
                                <option value="">Select Facility Type</option>
                                <option class="ot_government" value="Barangay Health Station" @if($facility_type == 'Barangay Health Station') selected @endif>Barangay Health Station</option>
                                <option class="ot_private" value="Birthing Home" @if($facility_type == 'Birthing Home') selected @endif>Birthing Home</option>
                                <option class="ot_government" value="City Health Office" @if($facility_type == 'City Health Office') selected @endif>City Health Office</option>
                                <option class="ot_government" value="District Health Office" @if($facility_type == 'District Health Office') selected @endif>District Health Office</option>
                                <option class="ot_private" value="Hospital" @if($facility_type == 'Hospital') selected @endif>Hospital</option>
                                <option class="ot_government" value="Main Health Center" @if($facility_type == 'Main Health Center') selected @endif>Main Health Center</option>
                                <option class="ot_government" value="Municipal Health Office" @if($facility_type == 'Municipal Health Office') selected @endif>Municipal Health Office</option>
                                <option class="ot_government" value="Provincial Health Office" @if($facility_type == 'Provincial Health Office') selected @endif>Provincial Health Office</option>
                                <option class="ot_government" value="Rural Health Unit" @if($facility_type == 'Rural Health Unit') selected @endif>Rural Health Unit</option>
                                <option class="ot_private" value="Private Clinic" @if($facility_type == 'Private Clinic') selected @endif>Private Clinic</option>
                            </select>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6" style="float:left; padding-right:7px !important;">
                                <select name="region" id="region" class="required form-control" required>
                                    {{ Shine\Libraries\Utils::get_regions($region) }}
                                </select>
                            </div>
                            <div class="form-group col-md-6" style="float:left; padding-left:7px !important;">
                                <select name="province" id="province" class="required form-control" required>
                                    {{ Shine\Libraries\Utils::get_provinces($region, $province) }}
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6" style="float:left; padding-right:7px !important;">
                                <select name="city" id="city" class="required form-control" required>
                                    {{ Shine\Libraries\Utils::get_cities($region, $province, $city) }}
                                </select>
                            </div>

                            <div class="form-group col-md-6" style="float:left; padding-left:7px !important;">
                                <select name="barangay" id="brgy" class="required form-control" required>
                                    {{ Shine\Libraries\Utils::get_brgys($region, $province, $city, $barangay) }}
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-4 bottom20">
                    <div class="jumbotron">
                        <h4>ADMINISTRATOR ACCOUNT</h4>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" placeholder="First Name" class="form-control required" id="first_name" value="{{ $first_name }}" name="first_name" />
                                <input type="text" placeholder="Last Name" class="form-control required" id="last_name" value="{{ $last_name }}" name="last_name" />
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="text" placeholder="Administrator Email Address" class="form-control required" id="email" value="{{ $email }}" name="email" />
                        </div>

                        <div class="form-group">
                            <input type="text" name="phone" id="phone" data-mask="" data-inputmask="&quot;mask&quot;: &quot;99-9999999&quot;" class="form-control" placeholder="Telephone 02-5772886" value="{{ $phone }}" />
                        </div>

                        <div class="form-group">
                            <input type="text" name="mobile" id="mobile" data-mask="" data-inputmask="&quot;mask&quot;: &quot;0999-9999999&quot;" class="form-control required" placeholder="Mobile 0917-1234567" value="{{ $mobile }}" required />
                        </div>

                        <div class="form-group">
                            <input type="password" placeholder="Administrator Password" class="form-control required password" id="password" value="" name="password" />
                        </div>

                        <div class="form-group">
                            <input type="password" placeholder="Confirm Administrator Password" class="form-control required confirmPassword" id="password_confirm" value="" name="password_confirm" />
                        </div>
                        <div class="form-group">
                            <p> {!! Form::checkbox('user_doctor', 1, false, ['class' => 'field']) !!} I am a Doctor</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 form-group text-center">
                <div class="col-md-12" style="padding-bottom: 10px">
                    By clicking <strong class="label label-success">Register</strong>, you agree with the <a href="http://www.shine.ph/shineph/terms-of-use" target="_blank"> Terms and Conditions of Use </a> set out by this site.
                </div>
                <div style="display:inline-block;">
                    <div class="has-succes col-md-6">
                        <img src="{{ url('registration/captcha') }}" alt="Please verify if you are a human" id="captcha-image" />
                        <a href="#" class="fa fa-refresh captcha-refresh">&nbsp;</a>
                    </div>
                    <div class="col-md-6">
                        <input id="test_captcha" placeholder="Copy the code." name="test_captcha" value="" type="text" class="form-control required captcha" />
                    </div>
                </div>
                <div style="display:inline-block;top: -20px;position: relative;">
                    <input type="submit" name="Register" id="btnRegister" class="btn btn-success" value="Register" />
                    <a href="{{ url('/') }}" class="btn btn-warning">Cancel</a>
                </div>
            </div>
            {!! Form::close() !!}
        </fieldset>
    </div>
</div>
@stop


@section('footer')
    <script src="{{ asset('public/dist/plugins/input-mask/inputmask.js') }}"></script>
    <script src="{{ asset('public/dist/plugins/input-mask/inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('public/dist/plugins/input-mask/inputmask.extensions.js') }}"></script>
    <script src="{{ asset('public/dist/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('public/dist/js/pages/registration.js') }}"></script>
@stop

