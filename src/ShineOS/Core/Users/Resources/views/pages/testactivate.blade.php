@extends('users::layouts.masterlogin')
@section('title') SHINE OS+ | Login @stop

@section('heads')
    <style>
        body {
            color: #333;
            background: #285377;
            /*background: -moz-linear-gradient(top, #444444 1%, #111111 100%);
            background: -webkit-linear-gradient(top, #444444 1%,#111111 100%);
            background: linear-gradient(to bottom, #444444 1%,#111111 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#444444', endColorstr='#111111',GradientType=0 );*/
        }
        .background_wrapper {
            background: url({{ asset('public/dist/img/saas-bak.jpg') }}) no-repeat center center;
            background-size: cover;
            left: 0;
            height: 100%;
            overflow: hidden;
            position: absolute;
            top: 0;
            width: 100%;
            z-index: 2;
        }
        .overlay {
            background-color: #000;
            left: 0;
            top: 0;
            position: absolute;
            width: 100%;
            height: 100%;
            z-index: 3;
            opacity: .55;
        }
        .overlay2 {
            background-color: #285377;
            left: 0;
            top: 0;
            position: absolute;
            width: 53%;
            height: 100%;
            z-index: 3;
            opacity: 1;
        }
        h2.smartlitblue {
            font-size: 2em;
        }
        .modal-content {
            border-radius: 12px;
            overflow:hidden;
        }
        .modal-body {
            padding:0 !important;
        }
    </style>
@stop

@if(Config::get('config.mode') == 'cloud' OR Config::get('config.mode') == 'demo' OR Config::get('config.mode') == 'training')
<?php $title = "The First Health Care Exchange System"; ?>
@endif
@if(Config::get('config.mode') == 'ce' OR Config::get('config.mode') == 'stand-alone')
<?php $title = "Community Edition"; ?>
@endif
@if(Config::get('config.mode') == 'developer')
<?php $title = "Developer Edition"; ?>
@endif
<?php
    $version = Config::get('config.version');
?>

@section('content')
    <div class="background_wrapper hidden-xs"></div>
    <div class="overlay hidden-xs"></div>
    <div class="overlay2 hidden-xs"></div>
    <div id="loginstyle" class="row">
        <div class="col-md-1 leftrightfill hidden-xs"></div>
        <div class="intro-box col-md-5 hidden-xs">
            <h1>Welcome to<br /><span class="smartblue">SHINE</span><span class="smartolive">OS+</span> v{{ $version }}</h1>
            <h2 class="smartlitblue">{{ $title }}</h2>
            <p class="lead">SHINE&trade; stands for <strong>Secured Health Information Network and Exchange</strong>. A web and mobile-based system that addresses the data management needs of doctors, nurses, midwives, and other allied health professionals in the Philippines.</p>
            <h3>
                @if(Config::get('config.mode') == 'cloud' OR Config::get('config.mode') == 'training' OR Config::get('config.mode') == 'stand-alone' )
                    <a href="{{ url('registration') }}" class="btn btn-primary text-center">Register for FREE!</a>
                @endif
                <a href="http://www.shine.ph/" class="btn btn-danger text-center" target="_blank">Learn more</a>
                <a href="http://www.shine.ph/developer" class="btn bg-shine-green text-center" target="_blank">Developer</a>
            </h3>

        </div>
        <div class="col-md-1 hidden-xs"></div>
        <div class="form-box col-md-3 col-xs-12" id="login-box">
            <div class="header"><i>SHINE OS+</i></div>
            <div class="body">
                <h4>Activate Community Edition</h4>
                <p>Congratulations! One more step and your SHINE OS+ Community Edition is ready for your use. Click the Activate button and upload the activation file you received after your registration. Check your email and download the file.</p>
                <div class="footer centered">
                    <a href="{{ url('activateaccount/ce/test') }}" class="btn btn-danger btn-block text-center" data-toggle="modal" data-target="#activateModal">Activate CE</a>
                </div>
            </div>
        </div>
        <div class="col-md-1 leftrightfill hidden-xs"></div>

        <br clear="all" />
    </div>

    <div class="modal fade" id="activateModal" tabindex="-1" role="dialog" aria-labelledby="activateModal">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@stop

@section('scripts')
{!! HTML::script('public/dist/plugins/jQuery/jQuery-2.1.4.min.js') !!}
{!! HTML::script('public/dist/js/bootstrap.min.js') !!}
{!! HTML::script('public/dist/plugins/jQueryUI/jquery-ui.min.js') !!}

    <script>

        $("#activateModal").on("show.bs.modal", function(e) {
            $(this).find(".modal-content").html("");
            var link = $(e.relatedTarget);
            console.log(link.attr("href"));
            $(this).find(".modal-content").load(link.attr("href"));
        });
    </script>
@stop
