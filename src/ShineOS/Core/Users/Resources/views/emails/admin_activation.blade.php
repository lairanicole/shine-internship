    <html>
    <head>
        <title> SHINE OS+ </title>

        <style>
            body {
                font-family: Arial;
                background: #eceff0 !important;
                color:#888;
            }
            a {
                color:#999;
                text-decoration: none;
            }
            hr {
                height: 1px;
                color: #CCC;
                border: 1px solid #CCC;
            }
            h2, h4 {
                font-weight: 100;
            }
        </style>
    </head>
    <body>
    <div style="background: #eceff0;">
        <table class="" cellspacing="0" border="0" cellpadding="20" align="center" width="600" style="background: #eceff0;">
            <tbody>
                <tr>
                    <td valign="top" colspan="2">
                        <h2 style="text-align:center;">
                            <img src="{{ asset('http://www.shine.ph/emr/public/dist/img/shine-logo-big.png') }}">
                        </h2>
                    </td>
                </tr>
                <tr style="background-color: #FFFFFF; border-radius: 9px;">
                    <td valign="top" colspan="2">
                        @if(isset($activated) AND $activated!=true)
                            <h2>SHINE OS+ Manual Activation</h2>
                        @else
                            <h2>SHINE OS+ Account Activated</h2>
                        @endif
                        <h4> <strong> Name: </strong> {{ $name }} </h4>
                        <h4> <strong> Email: </strong> {{ $email }}  </h4>
                        <hr />
                        <h4> Hi {{ $name }}, </h4>
                        @if(isset($activated) AND $activated!=true)
                            <p>Your SHINE OS+ Account Registration is received and will be reviewed. Please take note of the password you registered with. <b> And wait up to 48 working hours, for the activation email for your account. </b> </p>
                            <p>Always remember and keep your password in a safe place. Giving away your password is a security flaw that can put your records in authorized access.</p>
                            @if($owner == 'government')
                                <p>Attached here is the ZIP file containing all documents you need to accomplish and submit to ShineLabs as soon as possible. These are requirements for PhilHealth and PHIE submission. Failure to submit these documents within 30 days of this registration will be subject to cancellation of your account.</p>
                            @endif
                        @else
                            <p>Welcome to SHINE OS+. Your account registration has been activated. You may now login your account. </p>
                            @if(isset($login_link))
                            <p style="padding:15px;font-family:monospace;"><a href="{{ $login_link }}" target="_blank"> LOGIN </a></p>
                            @endif
                        @endif 
                        <p>Thank you!</p>
                        <p>SHINE OS+ Team</p>
                        </td>
                </tr>
                <tr>
                    <td style="text-align:center;font-size:36px;padding:35px 0;">
                        <a href="https://www.facebook.com/SHINEOSPLUS" style="padding:0 15px;"><img src="{{ asset('http://www.shine.ph/emr/public/dist/img/fb.png') }}" width='26' height='26'></a>
                        <a href="https://twitter.com/shineOSplusEMR" style="padding:0 15px;"><img src="{{ asset('http://www.shine.ph/emr/public/dist/img/twitter.png') }}" width='26' height='26'></a>
                    </td>
                </tr>
                <tr style="font-size:11px;">
                    <td style="text-align:center;">
                        &copy; 2011-{{ date('Y') }}. ShineLabs. All rights reserved<br>
                        SHINE OS+ is a product of ShineLabs<br>
                        in partnerships with Smart Communications.<br>
                        AJWCC, Ateneo de Manila University, Katipunan Ave., Q.C., Philippines<br>
                        <a href="http://www.shine.ph">Shine.ph</a> | <a href="http://www.shine.ph/developer">SHINE Developer</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </body>
</html>
