<?php
$chunked = $qdrugs_chunk;
// dd(count($chunked));
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>SHINE OS+ {{ "v".Config::get('config.version') }} :: Prescription</title> <!--Dynamic title-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
        rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        {!! HTML::style('public/dist/css/print_bootstrap.css') !!}
        <style>
            html, body {
                font-size: 13px;
                height: 100%;
            }
            body, .container {
                width: 100%;
                max-width: 700px;
                margin:auto;
            }
            h1 {
                font-size: 27px;
                margin:5px 0;
            }
            hr {
                margin:8px 0;
            }
            
            .letterhead {
                margin-bottom: 0px;
                padding-bottom: 10px;
            }
            #logos {
                
            }
            #logos img {
                margin:15px 10px 0;
                height:  35px;
            }
            <?php if(strtolower($provider->ownership_type) != "government") { ?>
            #logos h1 {
                position: relative;
                  top: 50%;
                  transform: translateY(-50%);
            }
            <?php } ?>
            #onepage {
                background: url("{{ asset('public/dist/img/rx_symbol_gry.png') }}") center 200px no-repeat;
                min-height:660px;
                overflow: hidden;
                page-break-before: always;
            }
            #footlogo img {
                margin:5px 30px 0;
                height: 25px;
            }
            #section-header {
                border-bottom: 2px solid #CCC;
                margin-bottom: 10px;
                padding-bottom: 10px;
            }
            #section-body {
                min-height: 23%;
            }
            #section-footer {
                bottom:0;
            }
            #section-footer img {
                margin:0 9px;
            }
            kbd {
                font-size:9px;
                vertical-align:top;
            }
            .bname {
                font-size: 14px;
            }
            .section td, .section td p {
                font-size: 12px;
            }
            .sectionn td, .sectionn td p {
                font-size: 10px;
            }
            #prescriptions {
                float: right;
                width: 85%;
            }
            .img-circle {
                margin: 0;
                padding: 0;
            }
            
            @media print {
                #logos img {
                    margin:10px 5px 0;
                    height: 20px;
                }  
                #section-footer {
                    position: fixed;
                } 
            }
        </style>
    </head>

    <body>
        <?php if($consultation->seen_by == NULL OR $consultation->seen_by =="") { ?>
            <div class="container" style="margin-top:100px;">
            <div class="jumbotron col-md-12">
                <h2>Oops! Not allowed.</h2>
                <p class="lead">This consultation has not been seen/checked by a physician. Please check with the doctor before printing this prescription.</p>
                <p><a class="btn btn-primary btn-lg" href="javascript:window.close();" role="button">Close</a></p>
            </div>
            </div>
        <?php } else { ?>
            <?php
            // $page_chunked = count($chunked);
            // for($page_chunked = 1; $page_chunked <= $pages; $page_chunked++)
            foreach($chunked as $k_c => $v_c) { ?>
            <div id="onepage">
                <div class="letterhead clearfix">
                    <?php if(isset($user->prescription_header) AND $user->prescription_header!=NULL) { 
                        echo $user->prescription_header;
                    } else { ?>
                    <div class="row">
                        <?php if(strtolower($provider->ownership_type) == "government") { ?>
                        <div class="col-md-12" id="logos">
                        <p class="small text-center"><img src="{{ asset('public/dist/img/doh.png') }}" /> <img src="{{ asset('public/dist/img/UHCNew.png') }}" /> <img src="{{ asset('public/dist/img/tsekap.jpg') }}" /> <img src="{{ asset('public/dist/img/philhealth.png') }}" /></p>
                        </div>
                        <?php } ?>

                        <h1 class="text-left col-md-12">
                        <?php
                            $userPhoto = $provider->facility_logo;
                        ?>
                        @if ( $userPhoto != '' )
                            <img src="{{ url( 'public/uploads/profile_picture/'.$userPhoto ) }}" class="img-circle" height="75" />
                        @endif
                        <?php echo $provider->facility_name; ?>
                        </h1>
                        
                    </div>
                    <?php } ?>
                </div>
                <div class="section" id="section-header">
                    <div class="container">
                        <div class="row">
                            <table width="100%">
                                <tr>
                                    <td colspan="3"><strong>Patient Information</strong></td>
                                    <td colspan="2" align="right"><?php echo date("F d, Y"); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="4">Name: <?php echo $patient->first_name." ".$patient->last_name; ?></td>
                                    <td rowspan="4" align="right">
                                        <?php if(isset($user->qrcode) AND $user->qrcode == "1") { ?>
                                        {!! QrCode::margin(2)->size(150)->generate($patqrcode); !!}
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">Encounter Date and Time: <?php echo date("F d, Y h:i:s",strtotime($consultation->encounter_datetime)); ?></td>  
                                </tr>
                                <tr>
                                    <td colspan="5">Address: <?php echo getBrgyName($patient->patientContact->barangay); ?>, <?php echo getCityName($patient->patientContact->city); ?></td>
                                </tr>
                                <tr>
                                    <td width="25%" valign="top">Contact:<br /><?php echo $patient->patientContact->mobile; ?></td>
                                    <td width="25%" valign="top">
                                            PhilHealth#:<br /><?php if(isset($phic->philhealth_id)) echo $phic->philhealth_id; ?><br>
                                            <?php if(isset($phic->philhealth_id)) echo "Member"; ?>
                                            </td>
                                    <td width="25%" valign="top">Sex:<br /><?php echo $patient->gender; ?></td>
                                    <td width="25%" valign="top">Birth:<br /><?php echo date("F d, Y", strtotime($patient->birthdate)); ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                
                <div class="section" id="section-body">
                    <div class="container">
                        <div class="row">
                            <img src="{{ asset('public/dist/img/rx_symbol.png') }}" style="float:left;margin-right: 15px;height:55px;" />
                            <table id="prescriptions">
                                <?php $dc = 0; $dp = 1;
                                foreach($v_c as $q => $drug) {
                                    $dcode = $freq = NULL;
                                    // if($q >= $dc AND $q <= $totaldrugnum AND $dp <= 6) 
                                    // {
                                        switch($drug->dosage_regimen)
                                        {
                                            case 'OD': $regimen = 'Once a day'; break;
                                            case 'BID': $regimen = '2 x a day - Every 12 hours'; break;
                                            case 'TID': $regimen = '3 x a day - Every 8 hours'; break;
                                            case 'QID': $regimen = '4 x a day - Every 6 hours'; break;
                                            case 'QOD': $regimen = 'Every other day'; break;
                                            case 'QHS': $regimen = 'Every bedtime'; break;
                                            case 'OTH': $regimen = 'Others'; break;
                                            default: $regimen = 'Not given';
                                        }
                                        $intake = explode(" ",$drug->duration_of_intake);
                                        if(isset($intake[1])) {
                                            switch($intake[1])
                                            {
                                                case 'D': $freq = 'Days'; break;
                                                case 'W': $freq = 'Weeks'; break;
                                                case 'M': $freq = 'Months'; break;
                                                case 'Q': $freq = 'Quarters'; break;
                                                case 'Y': $freq = 'Years'; break;
                                                case 'O': $freq = 'Others'; break;
                                                case 'C': $freq = 'Maintenance'; break;
                                                default: $freq = 'Not given';
                                            }
                                            $dcode = NULL;
                                        }

                                        if(isset($intake[0])) {
                                            $drugin = $intake[0];
                                        } else {
                                            $drugin = "none";
                                        }
                                        $dosage = explode(" ",$drug->dose_quantity);
                                        $total = explode(" ",$drug->total_quantity);

                                        $tq = $total[0];
                                        if($drug->dosage_regimen == 'OTH') {
                                            $regimen = $drug->dosage_regimen_others;
                                        }
                                        $t1 = $regimen." for ".$intake[0]." ".$freq;
                                        if(isset($total[1])) {
                                            $tq .= " ".$total[1];
                                            $t1 = " - ".$regimen." for ".$intake[0]." ".$freq;
                                        }
                                        if($dcode) {
                                            $drugline = $dcode->hprodid." #".$tq;
                                        } else {
                                            $drugline = $drug->generic_name."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#".$drug->total_quantity;
                                        }
                                    ?>
                                    <tr>
                                        <td width="100%" valign="top">
                                            <p>
                                                <strong>
                                                    <?php if(isset($dcode->source)) { ?><span style="
                                                        font-size: 9px;
                                                        vertical-align: top;
                                                        padding: 3px 5px;
                                                        color: #FFF;
                                                        background-color: #333;
                                                        border-radius: 3px;
                                                        box-shadow: 0px -1px 0px rgba(0, 0, 0, 0.25) inset;"><?php echo $dcode->source; ?></span> <?php } ?>
                                                    <span class='bname'><?php echo $drugline; ?></span></strong>
                                                    <?php if($drug->brand_name != "") echo "<br /><strong>(".$drug->brand_name.")</strong>"; ?>
                                                    <?php if($regimen != '') echo "<br />".$drug->dose_quantity." ".$t1; ?>
                                                    <?php if(isset($intake[1]) AND $intake[1] != 'C') { ?>
                                                    <?php if( $drug->regimen_startdate == '1970-01-01' OR $drug->regimen_startdate == '0000-00-00' ) { } else { ?>
                                                        <br />From: <?php echo date("M. d, Y", strtotime($drug->regimen_startdate)); ?> - <?php echo date("M. d, Y", strtotime($drug->regimen_enddate)); ?>
                                                    <?php } } ?>
                                                    <?php if($drug->prescription_remarks) echo "<br />".$drug->prescription_remarks; ?>
                                            </p>
                                        </td>
                                        <?php if(isset($user->qrcode) AND $user->qrcode == "1" AND $dp == 1) { ?>
                                        <td valign="top" rowspan="5" align="center">
                                            {!! QrCode::margin(2)->size(250)->generate(json_encode($qrdata_v2[$k_c])); !!}
                                        </td>
                                        <?php } ?>
                                    </tr>
                                <?php unset($drugs[$dc]); $dc++; $dp++; 
                                    // } else {
                                    //     $dp == 1;
                                    // } //end of if-else ?> 
                                <?php } //end of foreach ?>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="section" id="section-footer">
                    <div class="container">
                        <div class="row">
                            <table width="100%">
                                <tr>
                                    <td colspan="2"><hr /></td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <?php if($consultation->seen_by) {
                                            //try facilityuser_id
                                            $doctor = findUserByUserFacID($consultation->seen_by);
                                            if(!$doctor) {
                                                //try user_id
                                                $doctor = findUserByUserID($consultation->seen_by);
                                            }
                                            if($doctor) { ?>
                                        <p><strong><?php echo $doctor->first_name; ?> <?php echo $doctor->last_name; ?>, M.D.<?php if(isset($doctor->professional_titles)) echo ", ".$doctor->professional_titles; ?></strong>
                                            @if(isset($doctor->professional_license_number))<br>License#: <?php echo $doctor->professional_license_number; ?>@endif
                                            @if(isset($doctor->s2))<br>S2#: <?php echo $doctor->s2; ?>@endif
                                            @if(isset($doctor->ptr))<br>PTR#: <?php echo $doctor->ptr; ?>@endif
                                            @if($provider->phic_accr_id)<br>PHIC Accr#: <?php echo $provider->phic_accr_id; ?>@endif
                                        </p>
                                        <?php } } ?>
                                    </td>
                                    <td width="50%" valign="top">
                                        <p><strong><?php echo $provider->facility_name; ?></strong>
                                            <br>
                                            {{ isset($provider->facility_contact->building_name) ? $provider->facility_contact->building_name.", " : "" }}
                                            {{ isset($provider->facility_contact->house_no) ? $provider->facility_contact->house_no." " : "" }}
                                            {{ isset($provider->facility_contact->street_name) ? $provider->facility_contact->street_name.", " : "" }}
                                            <br>
                                            {{ isset($provider->facility_contact->barangay) ? getBrgyName($provider->facility_contact->barangay) : "" }}
                                            {{ isset($provider->facility_contact->municipality) ? ", ".getCityName($provider->facility_contact->municipality) : "" }}
                                            <br>
                                            @if(isset($provider->facility_contact->province))
                                                {{ getProvinceName($provider->facility_contact->province).", " }}
                                            @endif
                                            @if(isset($provider->facility_contact->zip))
                                                {{ $provider->facility_contact->zip.", " }}
                                            @endif
                                            @if(isset($provider->facility_contact->region))
                                                {{ getRegionName($provider->facility_contact->region) }}
                                            @endif
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12" id="footlogo">
                        <p class="small text-center">Powered by <img src="{{ asset('public/dist/img/shine-logo-big.png') }}" /></p>
                    </div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
    </body>
</html>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
{!! HTML::script('public/dist/plugins/bootbox.min.js') !!}
<script type="text/javascript">

    $(document).load(function() {
        /*$.ajax({
            url : "http://medrxapistaging.azurewebsites.net/api/Prescription",
            method: "post",
            data : <?php //echo $medrx; ?>,
            async : false,
            dataType : "json",
            success : function( data ){
                alert("Sent to MedRX");
            }
        });*/
    });

    bootbox.alert({
        message: "In order to print this prescription properly, we suggest that you turn off the Headings and Footers settings on the print dialog box.<br><br><b>This prescription can be printed on any size of paper in portrait standard prescription pad size. Make the necessary adjustments on your browser's printing command.</b>",
        title: 'Prescription Printing'
    });
</script>
