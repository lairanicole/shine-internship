<?php namespace ShineOS\Core\Healthcareservices\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrder; //model
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderPrescription; //model
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderLabExam; //model
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderProcedure; //model
use ShineOS\Core\Healthcareservices\Http\Requests\MedicalOrderFormRequest;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use ShineOS\Core\LOV\Entities\LovLaboratories;
use Shine\Libraries\IdGenerator;
use Shine\Libraries\Utils\Lovs;

use ShineOS\Core\Patients\Models\Patients;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Shine\Libraries\UserHelper;
use View, Response, Validator, Input, Mail, Session, Redirect, Hash, Auth, DB, Datetime, Request;

class MedicalOrderController extends Controller {

    protected $facility_name = "samplefacility name";
    protected $unique_id = "";
    protected $current_timestamp;

    protected $default_tbl = "medicalorder";
    protected $tbl_prescription = "medicalorder_prescription";
    protected $tbl_laboratory = "medicalorder_laboratoryexam";
    protected $tbl_procedure = "medicalorder_procedure";

    protected $txt_hservices_id;

    private $txt_action;
    private $params;

    public function __construct(HealthcareRepository $healthcareRepository) {
        /** User Session or Authenticaion  */
        $this->middleware('auth');

        $this->HealthcareRepository = $healthcareRepository;
        $this->user = UserHelper::getUserInfo();
        $this->roles = Session::get('roles');
        $date = new Datetime('now');
        $this->current_timestamp = strtotime($date->format('His'));
        $this->unique_id =  IdGenerator::generateId();
        $this->txt_hservices_id = Input::has('hservices_id') ? Input::get('hservices_id')  : false;

        View::addNamespace('healthcareservices', 'src/ShineOS/Core/Healthcareservices/Resources/Views');
    }

    public function UpdateCreate(MedicalOrderFormRequest $request) {
        $input = Request::all();
        

        if (array_key_exists('insert', $input)) {
            foreach ($input['insert']['type'] as $key => $value) {
                if( $value != 'MO_NONE' ) {

                    if( isset($input['insert']['medicalorder_id'][$key]) AND ($input['insert']['medicalorder_id'][$key] != NULL AND $input['insert']['medicalorder_id'][$key] != "undefined")  ){
                        $medorderID = $input['insert']['medicalorder_id'][$key];

                    } else {

                        $med_query = new MedicalOrder;
                        $med_query->medicalorder_id = $medorderID = $this->unique_id.$key;
                        $med_query->healthcareservice_id = $this->txt_hservices_id;
                        $med_query->medicalorder_type = $value;
                    }

                    if($value == 'MO_MED_PRESCRIPTION') {
                        $pres_query = new MedicalOrderPrescription;
                        $pres_query->medicalorderprescription_id = IdGenerator::generateId().$key;
                        $pres_query->medicalorder_id = $medorderID;
                        $pres_query->generic_name = $input['insert'][$value]['Drug_Code'][$key];
                        $pres_query->brand_name = $input['insert'][$value]['Drug_Brand_Name'][$key];
                        $pres_query->dose_quantity = $input['insert'][$value]['Dose_Qty'][$key].' '.$input['insert'][$value]['Dose_UOM'][$key];
                        $pres_query->total_quantity = $input['insert'][$value]['Total_Quantity'][$key].' '.$input['insert'][$value]['Total_Quantity_UOM'][$key];
                        $pres_query->dosage_regimen = $input['insert'][$value]['dosage'][$key];
                        $pres_query->dosage_regimen_others = isset($input['update'][$value]['Specify'][$key]) ? $input['update'][$value]['Specify'][$key] : NULL;
                        $pres_query->duration_of_intake = $input['insert'][$value]['Duration_Intake'][$key].' '.$input['insert'][$value]['Duration_Intake_Freq'][$key];

                        $regimen_startend_date = explode(" - ", $input['insert'][$value]['regimen_startend_date'][$key]);
                        $start_date = new Datetime($regimen_startend_date[0]);
                        $pres_query->regimen_startdate = $start_date;

                        if(isset($regimen_startend_date[1])) {
                            $end_date = new Datetime($regimen_startend_date[1]);
                            $pres_query->regimen_enddate = $end_date;
                        }

                        $pres_query->prescription_remarks = $input['insert'][$value]['Remarks'][$key];

                        if(isset($med_query) AND $med_query->medicalorder_type = "MO_MED_PRESCRIPTION") {
                            if(isset($input['insert']['prescription_instructions'][$key])){
                                $med_query->user_instructions = $input['insert']['prescription_instructions'][$key];
                            }
                            $MedicalOrder_insert = $med_query->save();
                        }
                        if(isset($input['update']['prescription_instructions'][$key])):
                            $med_query_u['user_instructions'] = $input['update']['prescription_instructions'][$key];
                            if($med_query_u['user_instructions']):
                                $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $medorderID)->update($med_query_u);
                            endif;
                        endif;

                        $pres_query->save();

                    }
                    else if($value == 'MO_LAB_TEST') {

                        if(isset($input['insert'][$value]['Examination_Code'])) {
                            $Examination_Code = $input['insert'][$value]['Examination_Code'];

                            foreach ($Examination_Code as $keyExamination_Code => $valueExamination_Code) {
                                $lab_query = new MedicalOrderLabExam;
                                $lab_query->medicalorderlaboratoryexam_id = IdGenerator::generateId().$keyExamination_Code;
                                $lab_query->medicalorder_id = $medorderID;
                                $lab_query->laboratory_test_type = $valueExamination_Code;
                                if($valueExamination_Code == 'OT' AND isset($input['insert'][$value]['others'][0]) AND $input['insert'][$value]['others'][0] != '') {
                                        $lab_query->laboratory_test_type_others = $input['insert'][$value]['others'][0];
                                    }

                                if(isset($med_query) AND $med_query->medicalorder_type = "MO_LAB_TEST") {
                                    if(isset($input['insert']['laboratory_instructions'][$key])){
                                        $med_query->user_instructions = $input['insert']['laboratory_instructions'][$key];
                                    }
                                    $MedicalOrder_insert = $med_query->save();
                                }
                                if(isset($input['update']['laboratory_instructions'][0])):
                                    $med_query_u['user_instructions'] = $input['update']['laboratory_instructions'][0];
                                    if($med_query_u['user_instructions']):
                                        $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $medorderID)->update($med_query_u);
                                    endif;
                                endif;

                                $lab_query->save();
                            }
                        }
                    }
                    else if($value == 'MO_PROCEDURE') {

                        foreach($input['insert'][$value]['medicalorderprocedure_id'] as $pkey => $procedure) {

                            $prod_query = new MedicalOrderProcedure;
                            $prod_query->medicalorderprocedure_id = IdGenerator::generateId().$key;
                            $prod_query->medicalorder_id = $medorderID;
                            $prod_query->procedure_order = $input['insert'][$value]['Procedure_Order'][$pkey];
                            $prod_query->procedure_date = new Datetime($input['insert'][$value]['Date_of_Procedure'][$pkey]);
                            $prod_query->procedure_instructions = $input['insert'][$value]['Procedure_Remarks'][$pkey];
                            $prod_query->save();

                            if(isset($med_query) AND $med_query->medicalorder_type = "MO_PROCEDURE") {
                                if(isset($input['insert']['procedure_user_instructions'][$key])) {
                                    $med_query->user_instructions = $input['insert']['procedure_user_instructions'][$key];
                                }
                                $MedicalOrder_insert = $med_query->save();
                            }
                            if(isset($input['update']['procedure_user_instructions'][$key])):
                                $med_query_u['user_instructions'] = $input['update']['procedure_user_instructions'][$key];
                                if($med_query_u['user_instructions']):
                                    $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $medorderID)->update($med_query_u);
                                endif;
                            endif;
                        }

                    }
                    else if($value == 'MO_OTHERS') {

                        if(!empty($input['insert'][$value]['order_type_others'])) {
                            foreach($input['insert'][$value]['order_type_others'] as $okey => $others) {
                                if($others){
                                    if(isset($input['insert'][$value]['order_type_others'][$okey])) {
                                        $med_query->medicalorder_others = $input['insert'][$value]['order_type_others'][$okey];
                                    }
                                    if(isset($med_query) AND $med_query->medicalorder_type = "MO_OTHERS") {
                                        if(isset($input['insert']['other_instructions'][$okey])) {
                                            $med_query->user_instructions = $input['insert']['other_instructions'][$okey];
                                        }
                                    $MedicalOrder_insert = $med_query->save();
                                    }
                                    if(isset($input['update']['other_instructions'][$okey])):
                                        $med_query_u['user_instructions'] = $input['update']['other_instructions'][$okey];
                                        if($med_query_u['user_instructions']):
                                            $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $medorderID)->update($med_query_u);
                                        endif;
                                    endif;
                                }
                            }
                        }
                    }
                    else {

                    }
                } else {
                    $MedicalOrder_insert = FALSE;
                }
            }
        }

        if (array_key_exists('update', $input)) {

            foreach ($input['update']['type'] as $key => $value) {
                if( $value != 'MO_NONE' ) {



                    if($value == 'MO_MED_PRESCRIPTION') {

                        $pres_query_u['generic_name'] = $input['update'][$value]['Drug_Code'][$key];
                        $pres_query_u['brand_name'] = $input['update'][$value]['Drug_Brand_Name'][$key];
                        $pres_query_u['dose_quantity'] = $input['update'][$value]['Dose_Qty'][$key].' '.$input['update'][$value]['Dose_UOM'][$key];
                        $pres_query_u['total_quantity'] = $input['update'][$value]['Total_Quantity'][$key].' '.$input['update'][$value]['Total_Quantity_UOM'][$key];
                        $pres_query_u['dosage_regimen'] = $input['update'][$value]['dosage'][$key];
                        $pres_query_u['dosage_regimen_others'] = isset($input['update'][$value]['Specify'][$key]) ? $input['update'][$value]['Specify'][$key] : NULL;
                        $pres_query_u['duration_of_intake'] = $input['update'][$value]['Duration_Intake'][$key].' '.$input['update'][$value]['Duration_Intake_Freq'][$key];

                        $regimen_startend_date  = explode(" - ", $input['update'][$value]['regimen_startend_date'][$key]);
                        $start_date  =  new Datetime($regimen_startend_date[0]);
                        $end_date  = new Datetime($regimen_startend_date[1]);

                        $pres_query_u['regimen_startdate'] = $start_date;
                        $pres_query_u['regimen_enddate'] = $end_date;

                        $pres_query_u['prescription_remarks'] = $input['update'][$value]['Remarks'][$key];

                        $pres_query_u_update = MedicalOrderPrescription::where('medicalorderprescription_id', $input['update'][$value]['medicalorderprescription_id'][$key])->update($pres_query_u);

                        $med_query_u['user_instructions'] = isset($input['update']['prescription_instructions'][$key]) ? $input['update']['prescription_instructions'][$key] : NULL;

                        $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->update($med_query_u);
                    }
                    else if($value == 'MO_LAB_TEST') {
                        //reset all lab exams by force deleting all so that unchecked items will be removed
                        $deleteAllExam = MedicalOrderLabExam::where('medicalorder_id',$input['update']['medicalorder_id'][$key])->forceDelete();
                        if(isset($input['update'][$value]['Examination_Code'])){
                            $Examination_Code = $input['update'][$value]['Examination_Code']; //dd($key, $Examination_Code, $input['update']);
                            foreach ($Examination_Code as $key_u_e_code => $value_u_e_code) {
                                    $lab_query = new MedicalOrderLabExam;
                                    $lab_query->medicalorderlaboratoryexam_id = IdGenerator::generateId().$key;
                                    $lab_query->medicalorder_id = $input['update']['medicalorder_id'][$key];
                                    $lab_query->laboratory_test_type = $value_u_e_code;
                                    if($value_u_e_code == 'OT' AND isset($input['update'][$value]['others'][0]) AND $input['update'][$value]['others'][0] != '') {
                                        $lab_query->laboratory_test_type_others = $input['update'][$value]['others'][0];
                                    }
                                    $lab_query->save();
                            }
                        } else {
                            //there is no checked lab
                            //delete this medical order ID
                            $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->forceDelete();
                        }
                        $med_query_u['user_instructions'] = isset($input['update']['laboratory_instructions'][0]) ? $input['update']['laboratory_instructions'][0] : NULL;

                        $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->update($med_query_u);

                    }
                    else if($value == 'MO_PROCEDURE') {
                        foreach($input['update'][$value]['Procedure_Order'] as $key => $v){
                            $prod_query_u['procedure_order'] = $input['update'][$value]['Procedure_Order'][$key];
                            $prod_query_u['procedure_date'] =  new Datetime($input['update'][$value]['Date_of_Procedure'][$key]);
                            $prod_query_u['procedure_instructions'] =  $input['update'][$value]['Procedure_Remarks'][$key];

                            $prod_query_u_update = MedicalOrderProcedure::where('medicalorderprocedure_id', $input['update'][$value]['medicalorderprocedure_id'][$key])->update($prod_query_u);
                        }
                        $med_query_u['user_instructions'] = isset($input['update']['procedure_user_instructions'][$key]) ? $input['update']['procedure_user_instructions'][$key] : NULL;

                        $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->update($med_query_u);


                    }
                    else if($value == 'MO_OTHERS') {

                        $other_query_u['medicalorder_others'] = isset($input['update'][$value]['order_type_others'][$key]) ? $input['update'][$value]['order_type_others'][$key] : NULL;

                        $med_query_u['user_instructions'] = isset($input['update']['other_instructions'][$key]) ? $input['update']['other_instructions'][$key] : NULL;

                        //if others entry to empty - delete medical order others
                        if($other_query_u['medicalorder_others'] == "") {
                            MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->forceDelete();
                        //else update the entry
                        } else {
                            $other_query_u_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->update($other_query_u);
                            $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->update($med_query_u);
                        }


                    }
                    else {

                    }

                }
            }
        }

        if (array_key_exists('delete', $input)) {

            foreach ($input['delete']['type'] as $key => $value) {

                if($value == 'MO_MED_PRESCRIPTION') {
                       $prescripDelete = MedicalOrderPrescription::where('medicalorderprescription_id', $input['delete'][$value]['medicalorderprescription_id'][$key])->delete();

                        if(isset($input['delete'][$value]['medicalorder_id'][$key])) {
                            $medprescDelete = MedicalOrder::where('medicalorder_id', $input['delete'][$value]['medicalorder_id'][$key])->forceDelete();
                        }
                }
                if($value == 'MO_PROCEDURE') {
                    //delete the item for deletion
                    $procDelete = MedicalOrderProcedure::where('medicalorderprocedure_id', $input['delete'][$value]['medicalorderprocedure_id'][$key])->forceDelete();

                    //check if there still orders here
                    $mayronpa = 0;
                    $mayprodcedurespa = MedicalOrder::where('healthcareservice_id', $input['hservices_id'])->get();

                    foreach($mayprodcedurespa as $proc){
                        $proced = MedicalOrderProcedure::where('medicalorder_id', $proc->medicalorder_id)->get();
                        if(empty($proced)) {
                            $mayronpa++;
                        }
                    }

                    //if the order is empty, delete medicalorder item
                    if($mayronpa == 0){
                        $procDelete = MedicalOrder::where('healthcareservice_id', $input['hservices_id'])->forceDelete();
                    }
                    //if the whole order form is deleted, delete medicalorder item
                    if(isset($input['delete'][$value]['medicalorder_id'][$key])) {
                        $procDelete = MedicalOrder::where('medicalorder_id', $input['delete'][$value]['medicalorder_id'][$key])->forceDelete();
                    }
                }
            }
        }

        $flash_message = 'Well done with your medical order!';
        $flash_type = 'alert-success alert-dismissible';

        return Redirect::back()->with('flash_message', $flash_message)
                                ->with('flash_type', $flash_type)
                                    ->with('flash_tab', 'medicalorders');

    }

    public function printLaboratory($hservice_id)
    {
        $data['consultation'] = $consultation = findHealthRecordByServiceID($hservice_id);

        $data['order'] = $medicalorder_record = getMedicalOrdersByHealthServiceID($hservice_id);

        foreach($medicalorder_record as $order)
        {
            if($order->medical_order_lab_exam) {
                $data['labs'] = $glabs = $order->medical_order_lab_exam;
                $data['instructions'] = $order->user_instructions;
            }
        }

        $data['consultation_id'] = $hservice_id;
        $phic = "NP";

        //get provider
        $data['provider'] = $provider = getFacilityByFacilityUserID($consultation->facilityuser_id);
        $data['user'] = $doctor = getUserDetailsByUserID($provider->facility_user->user_id);

        //get patient data
        $data['patient'] = $patient = getCompletePatientByPatientID($consultation->patient_id);

        //get patient phic data
        $data['phic'] = $patphic = $patient->patientPhilhealthInfo;

        //diagnosis
        $diagnosis = getDiagnosisDetailsByHealthServiceID($hservice_id);

        $data['lovlaboratories'] = LovLaboratories::orderBy('laboratorydescription')->get();

        //generate patient data for QRCode
        $pat['id'] = $patient->id;
        $pat['firstname'] = $patient->first_name;
        $pat['lastname'] = $patient->last_name;
        if(isset($patient->patientPhilhealthInfo->philhealth_id)) $patient->MEMID_NO = $phic = $patient->patientPhilhealthInfo->philhealth_id;
        $pat['barangay'] = $patient->patientContact->barangay;
        $pat['city'] = $patient->patientContact->city;
        $pat['province'] = $patient->patientContact->province;
        $pat['sex'] = $patient->gender;
        $pat['birthdate'] = $patient->birthdate;
        $data['patqrcode'] = json_encode($pat);


        return view('healthcareservices::laboratory')->with($data);
    }

    public function printPrescription($hservice_id)
    {
        $data['consultation'] = $consultation = findHealthRecordByServiceID($hservice_id);

        $medicalorder_record = getMedicalOrdersByHealthServiceID($hservice_id);

        $k = 0;
        foreach($medicalorder_record as $order)
        {
            if($order->medical_order_prescription) {
                foreach($order->medical_order_prescription as $prescription) {

                    // Get product code of drugs from LOV drugs
                    $product_code = getProductCodeByProductName($prescription->generic_name);
                    if($product_code){ $prescription->product_code = $product_code; }
                    else{ $prescription->product_code = $prescription->generic_name; }

                    $data['drugs'][$k] = $qdrugs[$k] = $prescription;
                    $k++;
                }
            }
        }


        $data['consultation_id'] = $hservice_id;

        //get provider
        $data['provider'] = $provider = getFacilityByFacilityUserID($consultation->facilityuser_id);
        $data['user'] = $doctor = getUserDetailsByUserID($provider->facility_user->user_id);

        $phic = "NP";

        //get patient data
        $data['patient'] = $patient = getCompletePatientByPatientID($consultation->patient_id);

        //get patient phic data
        $data['phic'] = $patphic = $patient->patientPhilhealthInfo;

        //diagnosis
        $diagnosis = getDiagnosisDetailsByHealthServiceID($hservice_id);

        //generate patient data for QRCode
        $pat['id'] = $patient->id;
        $pat['firstname'] = $patient->first_name;
        $pat['lastname'] = $patient->last_name;
        if(isset($patient->patientPhilhealthInfo->philhealth_id)) $patient->MEMID_NO = $phic = $patient->patientPhilhealthInfo->philhealth_id;
        $pat['barangay'] = $patient->patientContact->barangay;
        $pat['city'] = $patient->patientContact->city;
        $pat['province'] = $patient->patientContact->province;
        $pat['sex'] = $patient->gender;
        $pat['birthdate'] = $patient->birthdate;
        $data['patqrcode'] = json_encode($pat);

        //count all prescribed drugs
        $drugcount = count($qdrugs);

        //count number of drugs
        $dcount = $drugcount/6;
        $data['pages'] = ceil($dcount); //divide drugs into 3 each page

        $age = getAge($patient->birthdate);

        //setup MedRX data
        $totaldrugnum = $data['totaldrugnum'] = $drugcount;
        $qcnt = 1;

        $data['qdrugs_chunk'] = $chunks = array_chunk($qdrugs, 6, true);

        // dd($chunks,$qrdata_v2);

        //-------------------------- FOR QR CODE -----------------------//
        // for($page = 0; $page <= ceil($dcount); $page++) {

        //     $dc = 0;

        //     foreach($qdrugs as $q => $drug){
        //         $dcode = $freq = NULL;
        //         if($q >= $dc AND $q <= $totaldrugnum){
        //             $qrdata[$page][$dc] = '{
        //                 "PatientId": "'.$patient->id.'",
        //                 "DrugstoreId": "1",
        //                 "HealthUnitId": "1",
        //                 "PrescriptionList": [';

        //                 switch($drug->dosage_regimen)
        //                 {
        //                     case 'OD': $regimen = 'Once a day'; break;
        //                     case 'BID': $regimen = '2 x a day - Every 12 hours'; break;
        //                     case 'TID': $regimen = '3 x a day - Every 8 hours'; break;
        //                     case 'QID': $regimen = '4 x a day - Every 6 hours'; break;
        //                     case 'QOD': $regimen = 'Every other day'; break;
        //                     case 'QHS': $regimen = 'Every bedtime'; break;
        //                     case 'OTH': $regimen = 'Others'; break;
        //                     default: $regimen = 'Not given';
        //                 }
        //                 $intake = explode(" ",$drug->duration_of_intake);

        //                 if(isset($intake[1])){
        //                 switch($intake[1])
        //                 {
        //                     case 'D': $freq = 'Days'; break;
        //                     case 'W': $freq = 'Weeks'; break;
        //                     case 'M': $freq = 'Months'; break;
        //                     case 'Q': $freq = 'Quarters'; break;
        //                     case 'Y': $freq = 'Years'; break;
        //                     case 'O': $freq = 'Others'; break;
        //                     default: $freq = 'Not given';
        //                 }
        //                 //let us get the drug_code
        //                 $dcode = Lovs::getValueOfFieldBy('drugs', 'drug_specification', 'product_id', NULL);
        //                 }

        //                 if($dcode) {
        //                     $drugcode = $dcode->hprodid;
        //                 } else {
        //                     $drugcode = $drug->generic_name;
        //                 }
        //                 if(isset($intake[0])) {
        //                     $drugin = $intake[0];
        //                 } else {
        //                     $drugin = "none";
        //                 }
        //                 $dosage = explode(" ",$drug->dose_quantity);
        //                 $total = explode(" ",$drug->total_quantity);
        //                 $tq = $total[0];
        //                 if(isset($total[1])) {
        //                     $tq .= "|".$total[1];
        //                 }
        //                 $qrdata[$page][$dc] .= '{
        //                     "genericnameid": "'.$drug->generic_name.'",
        //                     "brandname": "'.$drug->brand_name.'",
        //                     "doseqty": "'.$dosage[0].' '.$dosage[1].'",
        //                     "totalquantity": "'.$tq.'",
        //                     "dosageregimen": "'.$regimen.'",
        //                     "intakefrequency": "'.$intake[0].'|'.$freq.'",
        //                     "regimenstartdate": "'.$drug->regimen_startdate.'|'.$drug->regimen_enddate.'",
        //                     "remarks": "'.$drug->prescription_remarks.'"
        //                 },';

        //                 $qrdata[$page][$dc] .= ']
        //             }';
        //         }
        //         $dc++;
        //     }
        // }
         //-------------------------- END QR CODE -----------------------//
         // dd($data);

        //-------------------------- FOR QR V2 CODE -----------------------//
        
        foreach ($chunks as $page => $drugs_per_chunk) {
            $qrdata_v2[$page]['PatientName'] = $patient->first_name . ' ' . $patient->last_name;

            foreach ($drugs_per_chunk as $key => $drug_in_chunk) {

                switch($drug_in_chunk->dosage_regimen)
                {
                    case 'OD': $regimen = 'Once a day'; break;
                    case 'BID': $regimen = '2 x a day - Every 12 hours'; break;
                    case 'TID': $regimen = '3 x a day - Every 8 hours'; break;
                    case 'QID': $regimen = '4 x a day - Every 6 hours'; break;
                    case 'QOD': $regimen = 'Every other day'; break;
                    case 'QHS': $regimen = 'Every bedtime'; break;
                    case 'OTH': $regimen = 'Others'; break;
                    default: $regimen = 'Not given';
                }

                $intake = explode(" ",$drug_in_chunk->duration_of_intake);

                if(isset($intake[1]))
                {
                    switch($intake[1])
                    {
                        case 'D': $freq = 'Days'; break;
                        case 'W': $freq = 'Weeks'; break;
                        case 'M': $freq = 'Months'; break;
                        case 'Q': $freq = 'Quarters'; break;
                        case 'Y': $freq = 'Years'; break;
                        case 'O': $freq = 'Others'; break;
                        default: $freq = 'Not given';
                    }
                }

                $drug_array =array(
                    'code' => $drug_in_chunk->product_code,
                    // 'DoseQuantity' => $drug_in_chunk->dose_quantity,
                    'qty' => $drug_in_chunk->total_quantity,
                    // 'DosageRegimen' => $regimen,
                    // 'DurationofIntake' => $intake[0] . " " . $freq,
                    // 'RegimenDates' => date("M. d, Y", strtotime($drug_in_chunk->regimen_startdate)) . "-" . date("M. d, Y", strtotime($drug_in_chunk->regimen_enddate)),
                    // 'PrescriptionRemarks' => $drug_in_chunk->prescription_remarks,
                    // 'Maintenance' => $drug_in_chunk->maintenance
                );

                $qrdata_v2[$page]['Prescriptions'][$key+1] = $drug_array;
            }
        }
         //-------------------------- END QR V2 CODE -----------------------//


        return view('healthcareservices::prescription', compact('qrdata_v2'))->with($data);
    }

    public function save($data)
    {
        $input = $data;

        if ($input AND array_key_exists('insert', $input)) {

            foreach ($input['insert']['type'] as $key => $value) {
                if( $value != 'MO_NONE' ) {

                    if( isset($input['insert']['medicalorder_id'][$key]) AND ($input['insert']['medicalorder_id'][$key] != NULL AND $input['insert']['medicalorder_id'][$key] != "undefined")  ){
                        $medorderID = $input['insert']['medicalorder_id'][$key];

                    } else {

                        $med_query = new MedicalOrder;
                        $med_query->medicalorder_id = $medorderID = $this->unique_id.$key;
                        $med_query->healthcareservice_id = $this->txt_hservices_id;
                        $med_query->medicalorder_type = $value;
                    }

                    if($value == 'MO_MED_PRESCRIPTION') {
                        $pres_query = new MedicalOrderPrescription;
                        $pres_query->medicalorderprescription_id = IdGenerator::generateId().$key;
                        $pres_query->medicalorder_id = $medorderID;
                        $pres_query->generic_name = $input['insert'][$value]['Drug_Code'][$key];
                        $pres_query->brand_name = $input['insert'][$value]['Drug_Brand_Name'][$key];
                        $pres_query->dose_quantity = $input['insert'][$value]['Dose_Qty'][$key].' '.$input['insert'][$value]['Dose_UOM'][$key];
                        $pres_query->total_quantity = $input['insert'][$value]['Total_Quantity'][$key].' '.$input['insert'][$value]['Total_Quantity_UOM'][$key];
                        $pres_query->dosage_regimen = $input['insert'][$value]['dosage'][$key];

                        $pres_query->dosage_regimen_others = isset($input['insert'][$value]['Specify'][$key]) ? $input['insert'][$value]['Specify'][$key] : NULL;
                        $pres_query->duration_of_intake = $input['insert'][$value]['Duration_Intake'][$key].' '.$input['insert'][$value]['Duration_Intake_Freq'][$key];

                            $regimen_startend_date = explode(" - ", $input['insert'][$value]['regimen_startend_date'][$key]);
                            $start_date = new Datetime($regimen_startend_date[0]);
                            if(isset($regimen_startend_date[1])) {
                                $end_date = new Datetime($regimen_startend_date[1]);
                                $pres_query->regimen_enddate = $end_date;
                            }

                        $pres_query->regimen_startdate = $start_date;
                        $pres_query->prescription_remarks = $input['insert'][$value]['Remarks'][$key];

                        if(isset($med_query) AND $med_query->medicalorder_type = "MO_MED_PRESCRIPTION") {
                            if(isset($input['insert']['prescription_instructions'][$key])){
                                $med_query->user_instructions = $input['insert']['prescription_instructions'][$key];
                            }
                            $MedicalOrder_insert = $med_query->save();
                        }
                        if(isset($input['update']['prescription_instructions'][$key])):
                            $med_query_u['user_instructions'] = $input['update']['prescription_instructions'][$key];
                            if($med_query_u['user_instructions']):
                                $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $medorderID)->update($med_query_u);
                            endif;
                        endif;

                        $pres_query->save();

                    }
                    else if($value == 'MO_LAB_TEST') {

                        if(isset($input['insert'][$value]['Examination_Code'])) {
                            $Examination_Code = $input['insert'][$value]['Examination_Code'];

                            foreach ($Examination_Code as $keyExamination_Code => $valueExamination_Code) {
                                $lab_query = new MedicalOrderLabExam;
                                $lab_query->medicalorderlaboratoryexam_id = IdGenerator::generateId().$keyExamination_Code;
                                $lab_query->medicalorder_id = $medorderID;
                                $lab_query->laboratory_test_type = $valueExamination_Code;
                                if($valueExamination_Code == 'OT' AND isset($input['insert'][$value]['others'][0]) AND $input['insert'][$value]['others'][0] != '') {
                                        $lab_query->laboratory_test_type_others = $input['insert'][$value]['others'][0];
                                }

                                if(isset($med_query) AND $med_query->medicalorder_type = "MO_LAB_TEST") {
                                    if(isset($input['insert']['laboratory_instructions'][$key])){
                                        $med_query->user_instructions = $input['insert']['laboratory_instructions'][$key];
                                    }
                                    $MedicalOrder_insert = $med_query->save();
                                }
                                if(isset($input['update']['laboratory_instructions'][0])):
                                    $med_query_u['user_instructions'] = $input['update']['laboratory_instructions'][0];
                                    if($med_query_u['user_instructions']):
                                        $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $medorderID)->update($med_query_u);
                                    endif;
                                endif;
                                $lab_query->save();
                            }
                        }
                    }
                    else if($value == 'MO_PROCEDURE') {

                        foreach($input['insert'][$value]['medicalorderprocedure_id'] as $pkey => $procedure) {

                            $prod_query = new MedicalOrderProcedure;
                            $prod_query->medicalorderprocedure_id = IdGenerator::generateId().$key;
                            $prod_query->medicalorder_id = $medorderID;
                            $prod_query->procedure_order = $input['insert'][$value]['Procedure_Order'][$pkey];
                            $prod_query->procedure_date = new Datetime($input['insert'][$value]['Date_of_Procedure'][$pkey]);
                            $prod_query->procedure_instructions = $input['insert'][$value]['Procedure_Remarks'][$pkey];
                            $prod_query->save();

                            if(isset($med_query) AND $med_query->medicalorder_type = "MO_PROCEDURE") {
                                if(isset($input['insert']['procedure_user_instructions'][$key])) {
                                    $med_query->user_instructions = $input['insert']['procedure_user_instructions'][$key];
                                }
                                $MedicalOrder_insert = $med_query->save();
                            }
                            if(isset($input['update']['procedure_user_instructions'][$key])):
                                $med_query_u['user_instructions'] = $input['update']['procedure_user_instructions'][$key];
                                if($med_query_u['user_instructions']):
                                    $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $medorderID)->update($med_query_u);
                                endif;
                            endif;
                        }

                    }
                    else if($value == 'MO_OTHERS') {

                        if(!empty($input['insert'][$value]['order_type_others'])) {
                            foreach($input['insert'][$value]['order_type_others'] as $okey => $others) {
                                if($others){
                                    if(isset($input['insert'][$value]['order_type_others'][$okey])) {
                                        $med_query->medicalorder_others = $input['insert'][$value]['order_type_others'][$okey];
                                    }
                                    if(isset($med_query) AND $med_query->medicalorder_type = "MO_OTHERS") {
                                        if(isset($input['insert']['other_instructions'][$okey])) {
                                            $med_query->user_instructions = $input['insert']['other_instructions'][$okey];
                                        }
                                    $MedicalOrder_insert = $med_query->save();
                                    }
                                    if(isset($input['update']['other_instructions'][$okey])):
                                        $med_query_u['user_instructions'] = $input['update']['other_instructions'][$okey];
                                        if($med_query_u['user_instructions']):
                                            $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $medorderID)->update($med_query_u);
                                        endif;
                                    endif;
                                }
                            }
                        }
                    }
                    else {

                    }
                } else {
                    $MedicalOrder_insert = FALSE;
                }
            }
        }

        if ($input AND array_key_exists('update', $input)) {
            if(isset($input['update']['type'])){
            foreach ($input['update']['type'] as $key => $value) {
                if( $value != 'MO_NONE' ) {
                    if($value == 'MO_MED_PRESCRIPTION') {

                        $pres_query_u['generic_name'] = $input['update'][$value]['Drug_Code'][$key];
                        $pres_query_u['brand_name'] = $input['update'][$value]['Drug_Brand_Name'][$key];
                        $pres_query_u['dose_quantity'] = $input['update'][$value]['Dose_Qty'][$key].' '.$input['update'][$value]['Dose_UOM'][$key];
                        $pres_query_u['total_quantity'] = $input['update'][$value]['Total_Quantity'][$key].' '.$input['update'][$value]['Total_Quantity_UOM'][$key];
                        $pres_query_u['dosage_regimen'] = $input['update'][$value]['dosage'][$key];
                        $pres_query_u['dosage_regimen_others'] = isset($input['update'][$value]['Specify'][$key]) ? $input['update'][$value]['Specify'][$key] : NULL;
                        $pres_query_u['duration_of_intake'] = $input['update'][$value]['Duration_Intake'][$key].' '.$input['update'][$value]['Duration_Intake_Freq'][$key];

                        if($input['update'][$value]['regimen_startend_date'][$key] AND $input['update'][$value]['Duration_Intake_Freq'][$key] != "C"){
                            $regimen_startend_date  = explode(" - ", $input['update'][$value]['regimen_startend_date'][$key]);
                            $start_date  =  new Datetime($regimen_startend_date[0]);
                            $end_date  = new Datetime($regimen_startend_date[1]);

                            $pres_query_u['regimen_startdate'] = $start_date;
                            $pres_query_u['regimen_enddate'] = $end_date;
                        } else {
                            $pres_query_u['regimen_startdate'] = NULL;
                            $pres_query_u['regimen_enddate'] = NULL;
                        }

                        $pres_query_u['prescription_remarks'] = $input['update'][$value]['Remarks'][$key];

                        $pres_query_u_update = MedicalOrderPrescription::where('medicalorderprescription_id', $input['update'][$value]['medicalorderprescription_id'][$key])->update($pres_query_u);

                        $med_query_u['user_instructions'] = isset($input['update']['prescription_instructions'][$key]) ? $input['update']['prescription_instructions'][$key] : NULL;

                        $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->update($med_query_u);
                    }
                    else if($value == 'MO_LAB_TEST') {

                        if(isset($input['update'][$value]['Examination_Code'])){
                            //get all current labs
                            $getlabs = MedicalOrderLabExam::where('medicalorder_id',$input['update']['medicalorder_id'][$key])->lists('laboratory_test_type','medicalorderlaboratoryexam_id')->toArray();

                            //this is the submitted labs
                            $Examination_Code = $input['update'][$value]['Examination_Code'];

                            //let us process labs for deletion
                            foreach($getlabs as $labid => $olabs){
                                //if this lab is included in the submitted labs, no change
                                if(in_array($olabs, $Examination_Code)) {

                                //if not, delete it
                                } else {
                                    MedicalOrderLabExam::where('medicalorderlaboratoryexam_id',$labid)->forceDelete();
                                    //delete also lab results
                                    DB::table('laboratory_result')->where('medicalorderlaboratoryexam_id',$labid)->delete();
                                }
                            }

                            //let us process labs for inclusion
                            foreach ($Examination_Code as $key_u_e_code => $value_u_e_code) {
                                //if this submitted is existing already, no change
                                if(in_array($value_u_e_code, $getlabs)) {
                                    if($value_u_e_code == 'OT'){
                                        $lab_query = MedicalOrderLabExam::where('medicalorderlaboratoryexam_id',$labid)->first();
                                        $lab_query->laboratory_test_type_others = $input['update'][$value]['others'][0];
                                        $lab_query->save();
                                    }
                                //if not, create new
                                } else {
                                    $lab_query = new MedicalOrderLabExam;
                                    $lab_query->medicalorderlaboratoryexam_id = IdGenerator::generateId().$key;
                                    $lab_query->medicalorder_id = $input['update']['medicalorder_id'][$key];
                                    $lab_query->laboratory_test_type = $value_u_e_code;
                                    if($value_u_e_code == 'OT' AND isset($input['update'][$value]['others'][0]) AND $input['update'][$value]['others'][0] != '') {
                                        $lab_query->laboratory_test_type_others = $input['update'][$value]['others'][0];
                                    }
                                    $lab_query->save();
                                }
                            }
                        } else {
                            //there is no checked lab
                            //delete this medical order ID
                            $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->forceDelete();
                        }
                        $med_query_u['user_instructions'] = isset($input['update']['laboratory_instructions'][0]) ? $input['update']['laboratory_instructions'][0] : NULL;

                        $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->update($med_query_u);

                    }
                    else if($value == 'MO_PROCEDURE') {
                        foreach($input['update'][$value]['Procedure_Order'] as $key => $v){
                            $prod_query_u['procedure_order'] = $input['update'][$value]['Procedure_Order'][$key];
                            $prod_query_u['procedure_date'] =  new Datetime($input['update'][$value]['Date_of_Procedure'][$key]);
                            $prod_query_u['procedure_instructions'] =  $input['update'][$value]['Procedure_Remarks'][$key];

                            $prod_query_u_update = MedicalOrderProcedure::where('medicalorderprocedure_id', $input['update'][$value]['medicalorderprocedure_id'][$key])->update($prod_query_u);
                        }
                        $med_query_u['user_instructions'] = isset($input['update']['procedure_user_instructions'][$key]) ? $input['update']['procedure_user_instructions'][$key] : NULL;

                        $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->update($med_query_u);


                    }
                    else if($value == 'MO_OTHERS') {

                        $other_query_u['medicalorder_others'] = isset($input['update'][$value]['order_type_others'][$key]) ? $input['update'][$value]['order_type_others'][$key] : NULL;

                        $med_query_u['user_instructions'] = isset($input['update']['other_instructions'][$key]) ? $input['update']['other_instructions'][$key] : NULL;

                        //if others entry to empty - delete medical order others
                        if($other_query_u['medicalorder_others'] == "") {
                            MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->forceDelete();
                        //else update the entry
                        } else {
                            $other_query_u_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->update($other_query_u);
                            $MedicalOrder_update = MedicalOrder::where('medicalorder_id', $input['update']['medicalorder_id'][$key])->update($med_query_u);
                        }


                    }
                    else {

                    }

                }
            }
            }
        }

        if ($input AND array_key_exists('delete', $input)) {

            foreach ($input['delete']['type'] as $key => $value) {

                if($value == 'MO_MED_PRESCRIPTION') {
                       $prescripDelete = MedicalOrderPrescription::where('medicalorderprescription_id', $input['delete'][$value]['medicalorderprescription_id'][$key])->delete();

                        if(isset($input['delete'][$value]['medicalorder_id'][$key])) {
                            $medprescDelete = MedicalOrder::where('medicalorder_id', $input['delete'][$value]['medicalorder_id'][$key])->forceDelete();
                        }
                }
                if($value == 'MO_PROCEDURE') {
                    //delete the item for deletion
                    $procDelete = MedicalOrderProcedure::where('medicalorderprocedure_id', $input['delete'][$value]['medicalorderprocedure_id'][$key])->forceDelete();

                    //check if there still orders here
                    $mayronpa = 0;
                    $mayprodcedurespa = MedicalOrder::where('healthcareservice_id', $input['hservices_id'])->get();

                    foreach($mayprodcedurespa as $proc){
                        $proced = MedicalOrderProcedure::where('medicalorder_id', $proc->medicalorder_id)->get();
                        if(empty($proced)) {
                            $mayronpa++;
                        }
                    }

                    //if the order is empty, delete medicalorder item
                    if($mayronpa == 0){
                        $procDelete = MedicalOrder::where('healthcareservice_id', $input['hservices_id'])->forceDelete();
                    }
                    //if the whole order form is deleted, delete medicalorder item
                    if(isset($input['delete'][$value]['medicalorder_id'][$key])) {
                        $procDelete = MedicalOrder::where('medicalorder_id', $input['delete'][$value]['medicalorder_id'][$key])->forceDelete();
                    }
                }
            }
        }

        //if this is an existing health record
        if($this->txt_hservices_id){
            //if this is a doctor set the seen_by (attending physician)
            $hc =  Healthcareservices::where('healthcareservice_id', $this->txt_hservices_id)->first();
            if($hc AND $hc->seen_by == NULL AND strtolower($this->roles['role_name']) == 'physician' OR strtolower($this->roles['role_name']) == 'doctor') {
                $hc->seen_by = $this->user->user_id;
                $hc->update(['seen_by'=>$this->user->user_id]);
            }
        }

        return "ok";
    }

}
