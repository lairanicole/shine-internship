<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use ShineOS\Core\Facilities\Entities\Facilities;
use Request, Validator;

class APIFacilitiesController extends Controller {
	
	public function index() {
		return response(json_encode(array('message'=>'No data')), 200);
	}

	public function facilityData_get() {
		$data = Request::all();
		$validator = Validator::make($data, [
			'facility_id' => 'required'
		]);

		if ($validator->fails()==false) { 
			$data = Facilities::with('facilityContact')->where('facility_id',$data['facility_id'])->first();
			return new JsonResponse((array('status'=>'success', 'data'=>$data)), 200);
		} else {
			 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}

	public function facilityList_get() {
		$data = Facilities::select('facilities.facility_name',
									// 'facility_contact.house_no',
									// 'facility_contact.building_name',
									// 'facility_contact.street_name',
									// 'facility_contact.village',
									'facility_contact.barangay',
									'facility_contact.city',
									'facility_contact.province',
									'facility_contact.region',
									'facility_contact.country',
									// 'facility_contact.zip',
									'facility_contact.phone',
									'facility_contact.mobile')
							->join('facility_contact','facilities.facility_id','=','facility_contact.facility_id')
							->paginate(10);
		foreach ($data as $key => $value) {
			$value->barangay = getBrgyName($value->barangay);
			$value->city = getCityNameReturn($value->city);
			$value->province = getProvinceNameReturn($value->province);
			$value->region = getRegionNameReturn($value->region);			
		}		
		return new JsonResponse((array('status'=>'success', 'data'=>json_decode($data->toJson()))), 200);
	}
}