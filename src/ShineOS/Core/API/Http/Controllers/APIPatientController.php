<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Users\Entities\Users;
use ShineOS\Core\Patients\Entities\PatientMonitoring;
use ShineOS\Core\LOV\Http\Controllers\LOVController as LOVController;
use Shine\Repositories\Eloquent\PatientRepository as PatientRepository;
use Shine\Repositories\Eloquent\FacilityRepository as FacilityRepository;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Patients\Entities\FacilityPatientUser;
use ShineOS\Core\Patients\Entities\PatientContacts;
use ShineOS\Core\Patients\Entities\PatientDeathInfo;
use ShineOS\Core\Patients\Entities\PatientEmergencyInfo;
use ShineOS\Core\Reminders\Entities\Reminders;
use ShineOS\Core\Reminders\Entities\ReminderMessage;
use ShineOS\Core\Referrals\Entities\Referrals;
use ShineOS\Core\LOV\Entities\LovReferralReasons;
use Shine\Libraries\FacilityHelper;
use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderPrescription;
use Request,Validator,DB,Auth,Hash;

class APIPatientController extends Controller {
	
	private $PatientRepository;

    public function __construct(PatientRepository $PatientRepository, FacilityRepository $FacilityRepository, HealthcareRepository $HealthcareRepository) {
        $this->PatientRepository = $PatientRepository;
        $this->FacilityRepository = $FacilityRepository;
        $this->HealthcareRepository = $HealthcareRepository;
    }

	public function index() {
		return response(json_encode(array('message'=>'No data')), 200);
	}

	//patient's login
	public function login_post() {
		$data = Request::all();
		$patient = Patients::where('email',$data['email'])->first();
        if($patient) {
            $checkPass = Hash::check($data['password'], $patient->password);
            if($checkPass) {
                return new JsonResponse(array('status'=>'success','message'=> 'Successfully logged in.','data'=>$patient), 200);
            }
        }
        return new JsonResponse(array('status'=>'error', 'message'=>'Incorrect Login Credentials'), 401);
	}

	//patient's registration
	public function registration_post() {

	}
	
	public function monitoring_post() {
		$data = Request::all();
		
		if(array_key_exists('patient_id',$data)) {
			$patientIdexists = Patients::where('patient_id',$data['patient_id'])->count();
			if($patientIdexists > 0) {

				$validator = Validator::make($data, [
		            'bloodpressure_systolic' => 'required_with:bloodpressure_diastolic|numeric|between:50,300',
		            'bloodpressure_diastolic' => 'required_with:bloodpressure_systolic|numeric|between:30,200',
		        ]); 

		        if ($validator->fails()) {
					return new JsonResponse((array('message'=>'Invalid data received', 'data'=>$data)), 400);
		        } else {
		        	/**
		        	 * Blood pressure assessment
		        	 * @var LOVController
		        	 */
		        	$LOVController = new LOVController();
		        	$bloodpressure_assessment = $LOVController->bloodpressure_assessment($data['bloodpressure_systolic'], $data['bloodpressure_diastolic']);

		        	/**
		        	 * Data to Patient Monitoring
		        	 * @var PatientMonitoring
		        	 */
		        	$PatientMonitoring = new PatientMonitoring;
					$PatientMonitoring->monitoring_id = IdGenerator::generateId();
					$PatientMonitoring->patient_id = $data['patient_id'];
					$PatientMonitoring->bloodpressure_systolic = (array_key_exists('bloodpressure_systolic',$data)? $data['bloodpressure_systolic']: '');
					$PatientMonitoring->bloodpressure_diastolic = (array_key_exists('bloodpressure_diastolic',$data)? $data['bloodpressure_diastolic']: '');
					$PatientMonitoring->bloodpressure_assessment = $bloodpressure_assessment;
					$PatientMonitoringSave = $PatientMonitoring->save();

					if($PatientMonitoringSave) {
						return new JsonResponse((array('message'=>'Success!', 'data'=>$PatientMonitoring)), 200);
					}
		        }
			} else { 
				return new JsonResponse((array('message'=>'Invalid patient id.', 'data'=>$data)), 400);
			}
		}
		return new JsonResponse((array('message'=>'Patient id does not exist.', 'data'=>$data)), 400);
	}

	/**
	 * get patient's list by facility id
	 * @return [type] [description]
	 */
	public function patientsListByFacilityId_get() {
		$data = Request::all();
		if(array_key_exists('facility_id',$data)) { 
			$id = $data['facility_id'];
			$facilities = json_decode($this->FacilityRepository->findByFacilityID($id));

			if($facilities) {
				$result = $this->patientsResult($id);
				return new JsonResponse((array('status'=>'success','data'=>json_decode($result->toJson()))), 200);
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=>'Incorrect facility id'), 400);
			} 
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Facility id required'), 400);
		}
	}

	public function patientsResult($facility_id) {
		$result = Patients::with('patientContact','facilityUser')
		                	->whereHas('facilityUser',
		                		function($query) use ($facility_id) {
		                    		$query->where('facility_id', '=', $facility_id);
		                	})
		                	->orderBy('updated_at', 'DESC')
		                	->paginate(10);

		if($result) {
			foreach ($result as $value) {
				$value->age = getAge($value->birthdate);
				$value->patientContact->barangay = getBrgyName($value->patientContact->barangay);
				$value->patientContact->city = getCityNameReturn($value->patientContact->city);
				$value->patientContact->province = getProvinceNameReturn($value->patientContact->province);
				$value->patientContact->region = getRegionNameReturn($value->patientContact->region);
			}
		}
				
        return $result;		
	}

	/**
	 * get patient's details by patient id
	 * @return [type] [description]
	 */
	public function patientDetailsByPatientId_get() {
		$data = Request::all();
		if(array_key_exists('patient_id',$data)) {
			$patient = Patients::with('patientContact','patientAlert','patientDeathInfo','patientEmergencyInfo')
				                ->where('patient_id', '=', $data['patient_id'])
				                ->paginate(1);
			if($patient->isEmpty() == 0) { 
				foreach ($patient as $key => $value) {
					$value->age = getAge($value->birthdate);
					// $value->patientContact->barangay = getBrgyName($value->patientContact->barangay);
					// $value->patientContact->city = getCityNameReturn($value->patientContact->city);
					// $value->patientContact->province = getProvinceNameReturn($value->patientContact->province);
					// $value->patientContact->region = getRegionNameReturn($value->patientContact->region);
				}
				return new JsonResponse((array('status'=>'success','data'=>json_decode($patient->toJson()))), 200);
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=>'Incorrect patient id'), 400);
			}
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Patient id required'), 400);
		}
	}

	/**
	 * get patient's visits by patient id
	 * @return [type] [description]
	 */
	public function patientVisitsByPatientId_get() { 
		$data = Request::all();
		if(array_key_exists('patient_id',$data)) {
			$patient = json_decode($this->HealthcareRepository->findHealthcareByPatientID($data['patient_id']));
			if($patient) {
				return new JsonResponse((array('status'=>'success','data'=>$patient)), 200);
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=>'Incorrect patient id'), 400);
			}
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>'Patient id required'), 400);
		}
	}

	public function test()
	{
		$data = Request::all();
		//dd($data);
	}

	public function insertPatientsRecord_post() {		
		$data = Request::all();
		$validator = Validator::make($data, [
			'facility_id' => 'required', 
			'user_id' => 'required', 
			'first_name' => 'required|string',
			'last_name' => 'required|string',
			'middle_name' => 'string',
			'maiden_lastname' => 'string',
			'maiden_middlename' => 'string',
			// 'name_suffix' => 'required',
			'gender' => 'required|in:F,M,U,f,m,u',
			'birthdate' => 'required|date|date_format:Y-m-d',
			'birthtime' => 'date_format:H:i:s',
			'birthplace' => 'required|string',
			'civil_status' => 'required|in:A,C,D,M,S,W,U,X,9,a,c,d,m,s,w,u,x',
			'blood_type' => 'required',
			'referral_notif' => 'required',
			'broadcast_notif' => 'required',
			'nonreferral_notif' => 'required',
			'patient_consent' => 'required',
			'myshine_acct' => 'required',
			'street_address' => 'required',
			'barangay' => 'required|integer',
			'city' => 'required|integer',
			'province' => 'required|integer',
			'region' => 'required|integer',
			'country' => 'required',
			'zip' => 'required|integer',
			'phone' => 'required|integer',
			'mobile' => 'required|integer',
			'email' => 'required|email',
        ]);

		if ($validator->fails()==false) { 
			$patientExists = Patients::where('first_name',$data['first_name'])->where('last_name',$data['last_name'])->where('middle_name',$data['middle_name'])->where('birthdate',$data['birthdate'])->pluck('patient_id');
			// dd($patientExists);
			if(!$patientExists) {			
				/**
	 			 * patient
	 			 */
	 			
	 			$facilityuser_id = DB::table('facility_user')->where('user_id','=',$data['user_id'])->where('facility_id','=',$data['facility_id'])->first(['facilityuser_id']);
	        	if($facilityuser_id!=NULL) {

					$newId = IdGenerator::generateId();
					$patient = new Patients;
					$patient->patient_id = $newId;
					$id = $patient->patient_id;
					$patient->first_name = $data['first_name'];
					$patient->last_name = $data['last_name'];
					$patient->middle_name = (array_key_exists('middle_name',$data)) ? $data['middle_name'] : '';
					$patient->maiden_lastname = (array_key_exists('maiden_lastname',$data)) ? $data['maiden_lastname'] : '';
					$patient->maiden_middlename = (array_key_exists('maiden_middlename',$data)) ? $data['maiden_middlename'] : '';
					$patient->name_suffix = $data['name_suffix'];
					$patient->gender = $data['gender'];
					$patient->birthdate = $data['birthdate'];
					$patient->birthtime = (array_key_exists('birthtime',$data)) ? $data['birthtime'] : '00:00:00';
					$patient->birthplace = $data['birthplace'];
					$patient->civil_status = $data['civil_status'];
					$patient->blood_type = $data['blood_type'];
					$patient->referral_notif = $data['referral_notif'];
					$patient->broadcast_notif = $data['broadcast_notif'];
					$patient->nonreferral_notif = $data['nonreferral_notif'];
					$patient->patient_consent = $data['patient_consent'];
					$patient->myshine_acct = $data['myshine_acct'];
		 			$insert_patient = $patient->save();
	 			
		        	$FacilityPatientUser = new FacilityPatientUser;
		        	$FacilityPatientUser->facilitypatientuser_id = IdGenerator::generateId();
		        	$FacilityPatientUser->patient_id = $id;
		        	$FacilityPatientUser->facilityuser_id = $facilityuser_id->facilityuser_id;
		        	$insert_FacilityPatientUser = $FacilityPatientUser->save();

		 			/**
		 			 * patient contact
		 			 */
		 			$patient_contact = new PatientContacts;
		 			$patient_contact->patient_contact_id = $id;
		 			$patient_contact->patient_id = $id;

					$patient_contact->street_address = $data['street_address'];
					$patient_contact->barangay = $data['barangay'];
					$patient_contact->city = $data['city'];
					$patient_contact->province = $data['province'];
					$patient_contact->region = $data['region'];
					$patient_contact->country = $data['country'];
					$patient_contact->zip = $data['zip'];
					$patient_contact->phone = $data['phone'];
					$patient_contact->mobile = $data['mobile'];
					$patient_contact->email = $data['email'];
					$insert_patient_contact = $patient_contact->save();

					if($insert_patient AND $insert_FacilityPatientUser AND $insert_patient_contact) {
						return new JsonResponse(array('status'=>'success', 'message'=> 'Successfully added patients data.'), 200);
					} else {
						return new JsonResponse(array('status'=>'error', 'message'=> 'Failed to add data.'), 400);
					}
				} else {
					return new JsonResponse(array('status'=>'error', 'message'=> 'Invalid facility or user id.'), 400);
				}
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=> 'Failed to add data. Patient info already exists', 'patient_id' => $patientExists), 400);
			}
			
		} else {
			 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
		
	}

	public function insertDeclareDead_post(){
		$data = Request::all();
		$validator = Validator::make($data, [
			'patient_id' => 'required',
			// 'DeathCertificateNo' => 'required',
			'datetime_death' => 'required|date|date_format:Y-m-d',
			'PlaceDeath' => 'required',
			'PlaceDeath_FacilityBased' => 'required_if:PlaceDeath,FB',
			'PlaceDeath_NID' => 'required_if:PlaceDeath,NID',
			'PlaceDeath_NID_Others_Specify' => 'required_if:PlaceDeath_NID,oth',
			// 'mStageDeath' => 'required',
			'Immediate_Cause_of_Death' => 'required',
			// 'Antecedent_Cause_of_Death' => 'required',
			// 'Underlying_Cause_of_Death' => 'required',
			// 'Type_of_Death' => 'required',
			'Remarks' => 'required',
			
        ]);

		if ($validator->fails()==false) {
			$patientId = Patients::where('patient_id',$data['patient_id'])->pluck('patient_id');
			if($patientId) {
				$patientDeathInfoExists = PatientDeathInfo::where('patient_id',$patientId)->pluck('patient_id');
				if(!$patientDeathInfoExists) { 
					/**
					 * patient death info
					 */
					$patient_death_info = new PatientDeathInfo;
					$patient_death_info->patient_deathinfo_id = $patientId;
					$patient_death_info->patient_id = $patientId;
					$patient_death_info->DeathCertificateNo = (array_key_exists('DeathCertificateNo',$data)) ? $data['DeathCertificateNo'] : '';
					$patient_death_info->datetime_death = $data['datetime_death'];
					$patient_death_info->PlaceDeath = $data['PlaceDeath'];
					$patient_death_info->PlaceDeath_FacilityBased = (array_key_exists('DeathCertificateNo',$data)) ? $data['DeathCertificateNo'] : '';
					$patient_death_info->PlaceDeath_NID = (array_key_exists('PlaceDeath_NID',$data)) ? $data['PlaceDeath_NID'] : '';
					$patient_death_info->PlaceDeath_NID_Others_Specify = (array_key_exists('PlaceDeath_NID_Others_Specify',$data)) ? $data['PlaceDeath_NID_Others_Specify'] : '';
					$patient_death_info->mStageDeath = (array_key_exists('mStageDeath',$data)) ? $data['mStageDeath'] : '';
					$patient_death_info->Immediate_Cause_of_Death = (array_key_exists('Immediate_Cause_of_Death',$data)) ? $data['Immediate_Cause_of_Death'] : '';
					$patient_death_info->Antecedent_Cause_of_Death = (array_key_exists('Antecedent_Cause_of_Death',$data)) ? $data['Antecedent_Cause_of_Death'] : '';
					$patient_death_info->Underlying_Cause_of_Death = (array_key_exists('Underlying_Cause_of_Death',$data)) ? $data['Underlying_Cause_of_Death'] : '';
					$patient_death_info->Type_of_Death = (array_key_exists('Type_of_Death',$data)) ? $data['Type_of_Death'] : '';
					$patient_death_info->Remarks = (array_key_exists('Remarks',$data)) ? $data['Remarks'] : '';
					$insert_patient_death_info = $patient_death_info->save();
					
					if($insert_patient_death_info) {				
						return new JsonResponse(array('status'=>'success', 'message'=> 'Successfully updated patients data.'), 200);
					} else {
						return new JsonResponse(array('status'=>'error', 'message'=> 'Failed to add data. Server problem.'), 400);
					}
				} else {
					return new JsonResponse(array('status'=>'error', 'message'=> 'Failed to insert data. Patient already updated.'), 400);
				}
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=> 'Patient id not exists.'), 400);	
			}
		} else {
			 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}


	public function updatePatientsRecord_post() {		
		$data = Request::all();
		$validator = Validator::make($data, [
			'patient_id' => 'required'
        ]);

		if ($validator->fails()==false) {
			$patientId = Patients::where('patient_id',$data['patient_id'])->pluck('patient_id');
			if($patientId) {
				
				$patient_contact = PatientContacts::where('patient_id',$patientId)->first();
				if(!$patient_contact) {
					$patient_contact = new PatientContacts();
					$patient_contact->patient_contact_id = IdGenerator::generateId();
					$patient_contact->patient_id = $patientId;
				}
				$patient_contact->street_address = (array_key_exists('street_address',$data)) ? $data['street_address'] : '';
				$patient_contact->barangay = (array_key_exists('barangay',$data)) ? $data['barangay'] : '';
				$patient_contact->city = (array_key_exists('city',$data)) ? $data['city'] : '';
				$patient_contact->province = (array_key_exists('province',$data)) ? $data['province'] : '';
				$patient_contact->region = (array_key_exists('region',$data)) ? $data['region'] : '';
				$patient_contact->country = (array_key_exists('country',$data)) ? $data['country'] : '';
				$patient_contact->zip = (array_key_exists('zip',$data)) ? $data['zip'] : '';
				$patient_contact->phone = (array_key_exists('phone',$data)) ? $data['phone'] : '';
				$patient_contact->phone_ext = (array_key_exists('phone_ext',$data)) ? $data['phone_ext'] : '';
				$patient_contact->mobile = (array_key_exists('mobile',$data)) ? $data['mobile'] : '';
				$patient_contact->save();

				$patient_emergencyinfo = PatientEmergencyInfo::where('patient_id',$patientId)->first();

				if(!$patient_emergencyinfo) {
					$patient_emergencyinfo = new PatientEmergencyInfo();
					$patient_emergencyinfo->patient_emergencyinfo_id = IdGenerator::generateId();
					$patient_emergencyinfo->patient_id = $patientId;
				}
                $patient_emergencyinfo->emergency_name = (array_key_exists('emergency_name',$data)) ? $data['emergency_name'] : '';
                $patient_emergencyinfo->emergency_relationship = (array_key_exists('emergency_relationship',$data)) ? $data['emergency_relationship'] : '';
                $patient_emergencyinfo->emergency_phone = (array_key_exists('emergency_phone',$data)) ? $data['emergency_phone'] : '';
                $patient_emergencyinfo->emergency_mobile = (array_key_exists('emergency_mobile',$data)) ? $data['emergency_mobile'] : '';
                $patient_emergencyinfo->emergency_address = (array_key_exists('emergency_address',$data)) ? $data['emergency_address'] : '';
                $patient_emergencyinfo->save();

				return new JsonResponse(array('status'=>'success', 'message'=> 'Successfully updated patients data.', 'data'=>$patientId), 200);
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=> 'Patient id not exists.'), 400);
			}
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}

	public function patientsAppointments_get() {
		$data = Request::all();
		$validator = Validator::make($data, [
			'patient_id' => 'required'
        ]);

		if ($validator->fails()==false) {
			$data = Reminders::with('ReminderMessage')
							->where('patient_id',$data['patient_id'])
							->paginate(10);
			if(!empty($data)) {
				foreach ($data as $key => $value) {
					$FacilityUser = FacilityUser::where('facilityuser_id',$value->facilityuser_id)->first();
					if(!empty($FacilityUser)) {
						$Facilities = Facilities::where('facility_id',$FacilityUser->facility_id)->first();
						$Users = Users::where('user_id',$FacilityUser->user_id)->first();

						$data[$key]['facility_name'] = $Facilities->facility_name;
						$data[$key]['users_first_name'] = $Users->first_name;
						$data[$key]['users_middle_name'] = $Users->middle_name;
						$data[$key]['users_last_name'] = $Users->last_name;
						$data[$key]['users_suffix'] = $Users->suffix;
					}
					else {
						return new JsonResponse(array('status'=>'error', 'message'=> 'Facility User does not exists.'), 400);
					}
				}
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=> 'Empty Data'), 400);
			}
			return new JsonResponse(array('status'=>'success', 'data'=>json_decode($data->toJson())), 200);
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}

	public function patientsReferrals_get() {
		$data = Request::all();
		$p_referrals = NULL;
		$validator = Validator::make($data, [
			'patient_id' => 'required'
        ]);

		if ($validator->fails()==false) {
			//get facilitypatientuser of the patient
			$p_fpu = FacilityPatientUser::where('patient_id',$data['patient_id'])->lists('facilitypatientuser_id');
			$p_fu = FacilityPatientUser::where('patient_id',$data['patient_id'])->lists('facilityuser_id');
			if(!empty($p_fpu) AND !empty($p_fu)) {
				//get all healthcareservice_id of the patient_id
 				$p_healthcare = Healthcareservices::whereIn('facilitypatientuser_id',$p_fpu)->lists('healthcareservice_id');
 				$referrer_facility_id = FacilityUser::whereIn('facilityuser_id',$p_fu)->lists('facility_id');
 				
 				if(!empty($p_healthcare)) {
 					$p_referrals = new Referrals();
 					$p_referrals = $p_referrals->with('referralmessages','referralReasons')->whereIn('healthcareservice_id',$p_healthcare)->paginate(10);
 					if(!empty($p_referrals)) { 
 						foreach ($p_referrals as $key => $value) {
 							$facility = Facilities::where('facility_id',$value->facility_id)->first();
 							$referrer_facility = Facilities::where('facility_id',$referrer_facility_id[0])->first();
 							$healthcare = Healthcareservices::where('healthcareservice_id',$value->healthcareservice_id)->select('healthcareservicetype_id')->first();
 							$value->healthcareservicename = $healthcare['healthcareservicetype_id'];
 							$value->facility_name = $facility['facility_name'];
 							$value->referrer_facility = $referrer_facility->facility_name;

 							if(!empty($value->referralReasons)) { 
 								foreach ($value->referralReasons as $key_1 => $value_1) {
 									$LovReferralReasons = LovReferralReasons::lists('referral_reason', 'lovreferralreason_id')->toArray();
 									$value_1->lovreferralreason_id = $LovReferralReasons[$value_1->lovreferralreason_id];
 								}
 							}
 						}
 					}
 					return new JsonResponse(array('status'=>'success', 'data'=>json_decode($p_referrals->toJson())), 200);
 				}
			}
 			return new JsonResponse(array('status'=>'error', 'message'=>"No data found."), 400);
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}
	
	public function patientsPrescriptions_get() {
		$data = Request::all();
		$result = NULL;
		$validator = Validator::make($data, [
			'patient_id' => 'required'
        ]);

		if ($validator->fails()==false) {
			//get facilitypatientuser of the patient
			$p_fpu = FacilityPatientUser::where('patient_id',$data['patient_id'])->lists('facilitypatientuser_id');
			if(!empty($p_fpu)) {
				//get all healthcareservice_id of the patient_id
 				$p_healthcare = Healthcareservices::with('MedicalOrder')->has('MedicalOrder')->whereIn('facilitypatientuser_id',$p_fpu)->paginate(10);
 				foreach ($p_healthcare as $key => $value) {
 					$facilitypatientuser_id = FacilityPatientUser::where('facilitypatientuser_id',$value->facilitypatientuser_id)->select('facilityuser_id')->first();
 					$facilityuser_id = FacilityUser::where('facilityuser_id',$facilitypatientuser_id->facilityuser_id)->select('facility_id')->first();
 					$facility = Facilities::where('facility_id',$facilityuser_id->facility_id)->first();
 					// dd($facilitypatientuser_id,$facilityuser_id);
					$healthcare = Healthcareservices::where('healthcareservice_id',$value->healthcareservice_id)->select('healthcareservicetype_id')->first();
					$value->healthcareservicename = $healthcare['healthcareservicetype_id'];
					$value->facility_name = $facility['facility_name'];

 					if($value->MedicalOrder->isEmpty() == false) {
 						foreach ($value->MedicalOrder as $key_1 => $value_1) {
 							if($value_1->medicalorder_type == "MO_MED_PRESCRIPTION") {
 								$value_1['MedicalOrderPrescription'] = MedicalOrderPrescription::where('medicalorder_id',$value_1->medicalorder_id)->first();
 							}
 						}
 					}
 				}
 				return new JsonResponse(array('status'=>'success', 'data'=>json_decode($p_healthcare->toJson())), 200);
			} else {
				return new JsonResponse(array('status'=>'error', 'message'=>"No data found."), 400);
			} 			
		} else {
			return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}


	public function patientsRegistration_post() {		
		$data = Request::all();
		$validator = Validator::make($data, [
			'first_name' => 'required|string',
			'last_name' => 'required|string',
			'middle_name' => 'required|string',
			'gender' => 'in:F,M,U,f,m,u',
			'birthdate' => 'required|date|date_format:Y-m-d',
			'birthtime' => 'date_format:H:i:s',
			'birthplace' => 'required|string',
			'civil_status' => 'required|in:A,C,D,M,S,W,U,X,9,a,c,d,m,s,w,u,x',
			
			'street_address' => 'required',
			'barangay' => 'required|integer',
			'city' => 'required|integer',
			'province' => 'required|integer',
			'region' => 'required|integer',
			'country' => 'required',
			'zip' => 'required|integer',
			'phone' => 'required|integer',
			'mobile' => 'required|integer',
			'email' => 'required|email',
			'password' => 'required',			
        ]);

		if ($validator->fails()==false) {
			$patientExists = Patients::where('first_name',$data['first_name'])->where('last_name',$data['last_name'])->where('middle_name',$data['middle_name'])->where('birthdate',$data['birthdate'])->pluck('patient_id');
			if(!$patientExists) {
				$emailExists = Patients::where('email',$data['email'])->pluck('patient_id');
				if(!$emailExists) {
					$newId = IdGenerator::generateId();
					$patient = new Patients;
					$patient->patient_id = $newId;
					$id = $patient->patient_id;
					$patient->first_name = (array_key_exists('first_name',$data)) ? $data['first_name'] : '';
					$patient->last_name = (array_key_exists('last_name',$data)) ? $data['last_name'] : '';
					$patient->middle_name = (array_key_exists('middle_name',$data)) ? $data['middle_name'] : '';
					$patient->maiden_lastname = (array_key_exists('maiden_lastname',$data)) ? $data['maiden_lastname'] : '';
					$patient->maiden_middlename = (array_key_exists('maiden_middlename',$data)) ? $data['maiden_middlename'] : '';
					$patient->name_suffix = (array_key_exists('name_suffix',$data)) ? $data['name_suffix'] : '';
					$patient->gender = (array_key_exists('gender',$data)) ? $data['gender'] : 'U';
					$patient->birthdate = (array_key_exists('birthdate',$data)) ? $data['birthdate'] : '0000-00-00';
					$patient->birthtime = (array_key_exists('birthtime',$data)) ? $data['birthtime'] : '00:00:00';
					$patient->birthplace = (array_key_exists('birthplace',$data)) ? $data['birthplace'] : '';
					$patient->civil_status = (array_key_exists('civil_status',$data)) ? $data['civil_status'] : '';
					$patient->blood_type = (array_key_exists('blood_type',$data)) ? $data['blood_type'] : 'U';
					$patient->referral_notif = (array_key_exists('referral_notif',$data)) ? $data['referral_notif'] : 0;
					$patient->broadcast_notif = (array_key_exists('broadcast_notif',$data)) ? $data['broadcast_notif'] : 0;
					$patient->nonreferral_notif = (array_key_exists('nonreferral_notif',$data)) ? $data['nonreferral_notif'] : 0;
					$patient->patient_consent = (array_key_exists('patient_consent',$data)) ? $data['patient_consent'] : 0;
					$patient->myshine_acct = (array_key_exists('myshine_acct',$data)) ? $data['myshine_acct'] : 0;
		 			$patient->password = (array_key_exists('password',$data)) ? Hash::make($data['password']) : '';
		 			$patient->email = (array_key_exists('email',$data)) ? $data['email'] : '';
		 			$insert_patient = $patient->save();
	 			
		 			/** patient contact */
		 			$patient_contact = new PatientContacts;
		 			$patient_contact->patient_contact_id = $id;
		 			$patient_contact->patient_id = $id;

					$patient_contact->street_address = (array_key_exists('street_address',$data)) ? $data['street_address'] : '';
					$patient_contact->barangay = (array_key_exists('barangay',$data)) ? $data['barangay'] : '';
					$patient_contact->city = (array_key_exists('city',$data)) ? $data['city'] : '';
					$patient_contact->province = (array_key_exists('province',$data)) ? $data['province'] : '';
					$patient_contact->region = (array_key_exists('region',$data)) ? $data['region'] : '';
					$patient_contact->country = (array_key_exists('country',$data)) ? $data['country'] : '';
					$patient_contact->zip = (array_key_exists('zip',$data)) ? $data['zip'] : '';
					$patient_contact->phone = (array_key_exists('phone',$data)) ? $data['phone'] : '';
					$patient_contact->mobile = (array_key_exists('mobile',$data)) ? $data['mobile'] : '';
					$patient_contact->email = (array_key_exists('email',$data)) ? $data['email'] : '';
					$insert_patient_contact = $patient_contact->save();

					if($insert_patient AND $insert_patient_contact) {
						return new JsonResponse(array('status'=>'success', 'message'=> 'Successfully registered.'), 200);
					} else {
						return new JsonResponse(array('status'=>'error', 'message'=> 'Failed to register.'), 400);
					}
				} else {
					return new JsonResponse(array('status'=>'error', 'message'=> '001:Failed to register data. Email already exists!', 'patient_id' => $emailExists), 400);
				}				
			} else {
				//patient's data exists
				//check if email is the same with the other patient's record
				$emailExists_2 = Patients::where('email',$data['email'])->pluck('patient_id');
				$patient_1 = Patients::where('patient_id',$patientExists)->first();
				if(!$emailExists_2) {
					if($patient_1->email!=NULL AND $patient_1->password!=NULL) {
						return new JsonResponse(array('status'=>'error', 'message'=> 'Failed to register data. Information or Email already exists.', 'patient_id' => $patientExists), 400);					
					} else {
						$patient_1->password = (array_key_exists('password',$data)) ? Hash::make($data['password']) : '';
			 			$patient_1->email = (array_key_exists('email',$data)) ? $data['email'] : '';
			 			$patient_1->save();
			 			return new JsonResponse(array('status'=>'success', 'message'=> 'Successfully registered.'), 200);
					}
				} else {
					if($emailExists_2 == $patientExists) {
						$patient_1->password = (array_key_exists('password',$data)) ? Hash::make($data['password']) : '';
			 			$patient_1->email = (array_key_exists('email',$data)) ? $data['email'] : '';
			 			$patient_1->save();
			 			return new JsonResponse(array('status'=>'success', 'message'=> 'Successfully registered.'), 200);
					} else {
						return new JsonResponse(array('status'=>'error', 'message'=> '002: Failed to register data. Email already exists!', 'patient_id' => $emailExists_2), 400);
					}
				}
			}
		} else {
			 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
		
	}
}
