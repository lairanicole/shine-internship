<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;
use Request;

class APIController extends Controller {
	
	public function index() {
		return response(json_encode(array('message'=>'No data')), 200);
	}
	
	/**
	 * PENDING PENDING PENDING
	 * API Developers Registration
	 * @return json
	 */
	public function register_post() {
		$data = Request::all();
		$array_keys = array_keys($data);
		$required_keys = array('first_name','middle_name','last_name','gender','email','api_purpose','username','password');
		
		$diff = array_diff($required_keys,$array_keys);

		if(empty($diff)) {
			$data['api_key'] = sha1(mt_rand(90000,99999).time().$data['email']);
			$data['api_secret'] = sha1(mt_rand(9999,10000).$data['email'].time());

			return new JsonResponse((array('message'=>'User successfully registered', 'data'=>$data)), 200);
		} else {
			return new JsonResponse(array('Incomplete data'=>array_values($diff)), 400);
		}
	}
}