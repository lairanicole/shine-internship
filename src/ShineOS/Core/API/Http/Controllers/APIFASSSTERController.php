<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Shine\Libraries\IdGenerator;
use Request,Validator,DB;

use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;

class APIFASSSTERController extends Controller {
	
    public function __construct() {}

	public function index() {
		return response(json_encode(array('message'=>'No data')), 200);
	}

    public function getCompleteIndividual()
    {
        DB::enableQueryLog();

        $data = Request::all();

        $month = NULL;
        $year = date('Y');
        $region = NULL;
        if(isset($data['month'])) {
            $month = $data['month']; }
        if(isset($data['year'])) {
            $year = $data['year']; }
        if(isset($data['region'])) {
            $region = $data['region']; }


        $patients = DB::table('healthcare_services')
                        ->join('facility_patient_user','facility_patient_user.facilitypatientuser_id','=','healthcare_services.facilitypatientuser_id')
                        ->join('facility_user','facility_patient_user.facilityuser_id','=','facility_user.facilityuser_id')
                        ->join('facility_contact','facility_user.facility_id','=','facility_contact.facility_id')
                        ->join('patients','patients.patient_id','=','facility_patient_user.patient_id')
                        ->join('patient_contact','patient_contact.patient_id','=','patients.patient_id')
                        ->join('general_consultation','general_consultation.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                        ->join('vital_physical','vital_physical.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                        ->join('diagnosis','diagnosis.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                        ->leftJoin('diagnosis_icd10','diagnosis.diagnosis_id','=','diagnosis_icd10.diagnosis_id')
                        ->leftJoin('medicalorder','medicalorder.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                        ->leftJoin('medicalorder_prescription','medicalorder.medicalorder_id','=','medicalorder_prescription.medicalorder_id')
                        ->select(
                                'patients.patient_id',
                                'healthcare_services.healthcareservice_id',
                                'patients.gender',
                                DB::raw("TIMESTAMPDIFF(YEAR,patients.birthdate,CURDATE()) as current_age"),
                                DB::raw("TIMESTAMPDIFF(YEAR,patients.birthdate,healthcare_services.created_at) as age_consulted"),
                                'patients.blood_type',
                                'vital_physical.temperature',
                                'vital_physical.height',
                                'vital_physical.weight',
                                'vital_physical.bloodpressure_systolic',
                                'vital_physical.bloodpressure_diastolic',
                                'vital_physical.heart_rate',
                                'general_consultation.complaint',
                                'diagnosis.diagnosislist_id',
                                'diagnosis_icd10.icd10_code',
                                DB::raw("medicalorder_prescription.generic_name AS prescription"),
                                DB::raw("patient_contact.barangay AS patient_barangay"),
                                DB::raw("patient_contact.city AS patient_city"),
                                DB::raw("patient_contact.province AS patient_province"),
                                DB::raw("patient_contact.region AS patient_region"),
                                DB::raw("facility_contact.barangay AS facility_barangay"),
                                DB::raw("facility_contact.city AS facility_city"),
                                DB::raw("facility_contact.province AS facility_province"),
                                DB::raw("facility_contact.region AS facility_region"),
                                DB::raw("healthcare_services.encounter_datetime as created_at" )
                            )
                        ->where(function($query) use ($month,$year,$region)
                            {
                                if($month != NULL)
                                {
                                    $query->whereMonth('healthcare_services.encounter_datetime', '=',$month)
                                            ->whereYear('healthcare_services.encounter_datetime', '=',$year);
                                }
                                else
                                {
                                    $query->whereYear('healthcare_services.encounter_datetime', '=',$year);   
                                }

                                if($region != NULL)
                                {
                                    $query->where('patient_contact.region','=',$region);
                                }
                            })
                        ->get();

        $the_query = DB::getQueryLog();

        if(count($patients) > 0) { 
            return new JsonResponse((array('data'=>$patients)), 200); }
        else {
            return new JsonResponse((array('data'=>'No data')), 200); }
    }

    public function getAllIndividual()
    {
        DB::enableQueryLog();

        $data = Request::all();

        $month = NULL;
        $year = date('Y');
        $region = NULL;
        if(isset($data['month'])) {
            $month = $data['month']; }
        if(isset($data['year'])) {
            $year = $data['year']; }
        if(isset($data['region'])) {
            $region = $data['region']; }


        $patients = DB::table('healthcare_services')
                        ->join('facility_patient_user','facility_patient_user.facilitypatientuser_id','=','healthcare_services.facilitypatientuser_id')
                        ->join('patients','patients.patient_id','=','facility_patient_user.patient_id')
                        ->join('patient_contact','patient_contact.patient_id','=','patients.patient_id')
                        ->join('general_consultation','general_consultation.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                        ->join('vital_physical','vital_physical.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                        ->join('diagnosis','diagnosis.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                        ->select(
                                    'patients.patient_id',
                                    'patients.gender',
                                    DB::raw("TIMESTAMPDIFF(YEAR,patients.birthdate,CURDATE()) as current_age"),
                                    DB::raw("TIMESTAMPDIFF(YEAR,patients.birthdate,healthcare_services.created_at) as age_consulted"),
                                    'patients.blood_type',
                                    'vital_physical.temperature',
                                    'vital_physical.height',
                                    'vital_physical.weight',
                                    'vital_physical.bloodpressure_systolic',
                                    'vital_physical.bloodpressure_diastolic',
                                    'vital_physical.heart_rate',
                                    'general_consultation.complaint',
                                    'diagnosis.diagnosislist_id',
                                    'patient_contact.barangay',
                                    'patient_contact.city',
                                    'patient_contact.province',
                                    'patient_contact.region',
                                    DB::raw("healthcare_services.encounter_datetime as created_at" )
                                )
                        ->where(function($query) use ($month,$year,$region)
                            {
                                if($month != NULL)
                                {
                                    $query->whereMonth('healthcare_services.encounter_datetime', '=',$month)
                                            ->whereYear('healthcare_services.encounter_datetime', '=',$year);
                                }
                                else
                                {
                                    $query->whereYear('healthcare_services.encounter_datetime', '=',$year);   
                                }

                                if($region != NULL)
                                {
                                    $query->where('patient_contact.region','=',$region);
                                }
                            })
                        ->get();

        $the_query = DB::getQueryLog();

        if(count($patients) > 0) { 
            return new JsonResponse((array('data'=>$patients)), 200); }
        else {
            return new JsonResponse((array('data'=>'No data')), 200); }
    }

    public function getPatients(){
        $gender = NULL;
        $age = NULL;
        $blood_type = NULL;

        $data = Request::all();
        if(isset($data['gender'])){
            $gender = $data['gender']; }
        if(isset($data['age'])){
            $age = $data['age']; }
        if(isset($data['blood_type'])){
            $blood_type = $data['blood_type']; }

        DB::enableQueryLog();
        $patients = Patients::join('patient_contact','patient_contact.patient_id','=','patients.patient_id')
                                ->select(
                                        'gender',
                                        DB::raw("TIMESTAMPDIFF(YEAR, birthdate, CURDATE()) as age"),
                                        'blood_type','patient_contact.barangay',
                                        'patient_contact.city',
                                        'patient_contact.province',
                                        'patient_contact.region',
                                        DB::raw("COUNT(*) as counter")
                                    )
                                ->gender($gender)
                                ->age($age)
                                ->bloodtype($blood_type)
                                ->groupBy(
                                        'gender',
                                        'age',
                                        'blood_type',
                                        'barangay',
                                        'city',
                                        'province',
                                        'region'
                                    )
                                ->orderBy('age')
                                ->get();
        // dd(DB::getQueryLog());
        if(count($patients) > 0) { 
            return new JsonResponse((array('data'=>$patients)), 200); }
        else {
            return new JsonResponse((array('data'=>'No data')), 200); }
    }

    public function getVitals(){
        $bpsystolic = NULL;
        $temperature = NULL;
        $weight = NULL;

        $data = Request::all();
        if(isset($data['bpsystolic'])){
            $bpsystolic = $data['bpsystolic']; }
        if(isset($data['temperature'])){
            $temperature = $data['temperature']; }
        if(isset($data['weight'])){
            $weight = $data['weight']; }

        $month = date('n');
        $year = date('Y');
        if(isset($data['month'])) {
            $month = $data['month']; }
        if(isset($data['year'])) {
            $year = $data['year']; }

        DB::enableQueryLog();
        $patients = Healthcareservices::join('facility_patient_user','facility_patient_user.facilitypatientuser_id','=','healthcare_services.facilitypatientuser_id')
                                            ->join('patients', 'patients.patient_id','=','facility_patient_user.patient_id')
                                            ->join('patient_contact','patient_contact.patient_id','=','patients.patient_id')
                                            ->join('vital_physical', 'vital_physical.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                                            ->select(
                                                        DB::raw("TIMESTAMPDIFF(YEAR, patients.birthdate, CURDATE()) as current_age"),
                                                        DB::raw("TIMESTAMPDIFF(YEAR, patients.birthdate, healthcare_services.created_at) as age_consulted"),
                                                        'vital_physical.bloodpressure_systolic',
                                                        // 'vital_physical.temperature',
                                                        // 'vital_physical.weight',
                                                        'patient_contact.barangay',
                                                        'patient_contact.city',
                                                        'patient_contact.province',
                                                        'patient_contact.region',
                                                        DB::raw("COUNT(*) as counter")
                                                    )
                                            ->bpsystolic($bpsystolic)
                                            // ->temperature($temperature)
                                            // ->weight($weight)
                                            ->whereMonth('healthcare_services.created_at', '=',$month)
                                            ->whereYear('healthcare_services.created_at', '=',$year)
                                            ->groupBy(
                                                        'current_age',
                                                        'age_consulted',
                                                        'bloodpressure_systolic',
                                                        // 'temperature',
                                                        // 'weight',
                                                        'barangay',
                                                        'city',
                                                        'province',
                                                        'region'
                                                    )
                                            ->get();

        if(count($patients) > 0) { 
            return new JsonResponse((array('data'=>$patients)), 200); }
        else {
            return new JsonResponse((array('data'=>'No data')), 200); }
    }

	public function getDemographyIndividual() 
    {
        $patients = DB::table('patients')
            ->join('patient_contact','patient_contact.patient_id','=','patients.patient_id')
            ->rightJoin('patient_alert','patient_alert.patient_id','=','patients.patient_id')
            ->select(
                  'patients.patient_id',
                  'patients.birthdate',
                  'patients.gender',
                  DB::raw("TIMESTAMPDIFF(YEAR,patients.birthdate,CURDATE()) as current_age"),
                  'patients.blood_type',
                  'patient_contact.barangay',
                  'patient_contact.city',
                  'patient_contact.province',
                  'patient_contact.region'
                )
            ->get();
        if(count($patients) > 0) { 
            return new JsonResponse((array('data'=>$patients)), 200); }
        else {
            return new JsonResponse((array('data'=>'No data')), 200); }
	}

	public function getAllergiesIndividual()
    {
        $patients = DB::table('patients')
            ->join('patient_contact','patient_contact.patient_id','=','patients.patient_id')
            ->rightJoin('patient_alert','patient_alert.patient_id','=','patients.patient_id')
            ->leftJoin('allergy_patient', 'allergy_patient.patient_alert_id','=','patient_alert.patient_alert_id')
            ->select(
                  'patients.patient_id',
                  'patients.birthdate',
                  'patients.gender',
                  DB::raw("TIMESTAMPDIFF(YEAR,patients.birthdate,CURDATE()) as current_age"),
                  'patients.blood_type',
                  'allergy_patient.allergy_id',
                  'allergy_patient.allergy_reaction_id',
                  'allergy_patient.allergy_severity',
                  'patient_contact.barangay',
                  'patient_contact.city',
                  'patient_contact.province',
                  'patient_contact.region'
                )
            ->get();
        if(count($patients) > 0) { 
            return new JsonResponse((array('data'=>$patients)), 200); }
        else {
            return new JsonResponse((array('data'=>'No data')), 200); }
	}

	public function getVitalsIndividual()
	{
        $data = Request::all();
        $month = date('n');
        $year = date('Y');
        if(isset($data['month'])) {
            $month = $data['month']; }
        if(isset($data['year'])) {
            $year = $data['year']; }

        $patients = DB::table('healthcare_services')
                    ->join('facility_patient_user','facility_patient_user.facilitypatientuser_id','=','healthcare_services.facilitypatientuser_id')
                    ->join('patients','patients.patient_id','=','facility_patient_user.patient_id')
                    ->join('patient_contact','patient_contact.patient_id','=','patients.patient_id')
                    ->join('general_consultation','general_consultation.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                    ->join('vital_physical','vital_physical.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                    ->select(
                                'patients.patient_id',
                                'patients.gender',
                                DB::raw("TIMESTAMPDIFF(YEAR,patients.birthdate,CURDATE()) as current_age"),
                                DB::raw("TIMESTAMPDIFF(YEAR,patients.birthdate,healthcare_services.created_at) as age_consulted"),
                                'patients.blood_type',
                                'vital_physical.temperature',
                                'vital_physical.height',
                                'vital_physical.weight',
                                'vital_physical.bloodpressure_systolic',
                                'vital_physical.bloodpressure_diastolic',
                                'vital_physical.heart_rate',
                                'patient_contact.barangay',
                                'patient_contact.city',
                                'patient_contact.province',
                                'patient_contact.region',
                                DB::raw("healthcare_services.created_at as created_at" )
                            )
                    ->whereMonth('healthcare_services.created_at', '=',$month)
                    ->whereYear('healthcare_services.created_at', '=',$year)
                    ->get();
        if(count($patients) > 0) { 
            return new JsonResponse((array('data'=>$patients)), 200); }
        else {
            return new JsonResponse((array('data'=>'No data')), 200); }
	}



    public function getConsultationsIndividual()
    {
        $data = Request::all();
        $month = date('n');
        $year = date('Y');
        if(isset($data['month'])) {
            $month = $data['month']; }
        if(isset($data['year'])) {
            $year = $data['year']; }

        $patients = DB::table('healthcare_services')
                        ->join('facility_patient_user','facility_patient_user.facilitypatientuser_id','=','healthcare_services.facilitypatientuser_id')
                        ->join('patients','patients.patient_id','=','facility_patient_user.patient_id')
                        ->join('patient_contact','patient_contact.patient_id','=','patients.patient_id')
                        ->join('general_consultation','general_consultation.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                        ->join('vital_physical','vital_physical.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                        ->join('diagnosis','diagnosis.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                        ->select(
                                    'patients.patient_id',
                                    'patients.gender',
                                    DB::raw("TIMESTAMPDIFF(YEAR,patients.birthdate,CURDATE()) as current_age"),
                                    DB::raw("TIMESTAMPDIFF(YEAR,patients.birthdate,healthcare_services.created_at) as age_consulted"),
                                    'patients.blood_type',
                                    'vital_physical.temperature',
                                    'vital_physical.height',
                                    'vital_physical.weight',
                                    'vital_physical.bloodpressure_systolic',
                                    'vital_physical.bloodpressure_diastolic',
                                    'vital_physical.heart_rate',
                                    'general_consultation.complaint',
                                    'diagnosis.diagnosislist_id',
                                    'patient_contact.barangay',
                                    'patient_contact.city',
                                    'patient_contact.province',
                                    'patient_contact.region',
                                    DB::raw("healthcare_services.created_at as created_at" )
                                )
                        ->whereMonth('healthcare_services.created_at', '=',$month)
                        ->whereYear('healthcare_services.created_at', '=',$year)
                        ->get();
        if(count($patients) > 0) { 
            return new JsonResponse((array('data'=>$patients)), 200); }
        else {
            return new JsonResponse((array('data'=>'No data')), 200); }
	}
}