<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use ShineOS\Core\Users\Entities\Users;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Roles\Entities\RolesAccess;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use Request, Auth, Validator;

class APIUsersController extends Controller { 
	private $UserRepository;

    public function __construct(UserRepository $UserRepository) {
        $this->UserRepository = $UserRepository;
    }

	public function index() {
		return response(json_encode(array('message'=>'No data')), 200);
	}
	
	public function login_post() {
		$data = Request::all();
		$array_keys = array_keys($data);
		$required_keys = array('email','password');
		
		$diff = array_diff($required_keys,$array_keys);

		if(empty($diff)) {
			$user = Users::where('email', $data['email'])->first();
			if($user) { 
				if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'].$user->salt])) {
					$result = json_decode($this->UserRepository->findUserAndFacilityUserByUserID($user->user_id));
	                return new JsonResponse((array('status'=>'success','data'=>$result)), 200);
	            } else {
	                return new JsonResponse(array('status'=>'error', 'message'=>'Incorrect Login Credentials'), 400);
	            } 
	        } else {
				return new JsonResponse(array('status'=>'error', 'message'=>'Incorrect Login Credentials'), 400);
			} 
		} else {
			return new JsonResponse(array('status'=>'error', 'Incomplete data'=>array_values($diff)), 400);
		}
	}

	public function userProfile_get() {
		$data = Request::all();
		$validator = Validator::make($data, [
			'user_id' => 'required'
		]);

		if ($validator->fails()==false) { 
			$data = Users::with('contact', 'mdUsers')->where('user_id',$data['user_id'])->first();

			return new JsonResponse((array('status'=>'success', 'data'=>$data)), 200);
		} else {
			 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
	}

	public function usersList_get() {
		// $roles = RolesAccess::where('role_id',2)->orWhere('role_id',1)->paginate(10); // get all doctors account
		// foreach ($roles as $key => $value) {
		// 	$FacilityUser = FacilityUser::where('facilityuser_id',$value->facilityuser_id)->first();
		// 	$value->users = Users::where('user_id',$FacilityUser->user_id)
		// 				->where('status','Active')
		// 				->with('mdUsers','contact')->first();

		// 	$Facilities = Facilities::where('facility_id',$FacilityUser->facility_id)->first();
		// 	$value->facility_name = $Facilities->facility_name;
		// 	if($value->users == null) {
		// 		unset($roles[$key]);
		// 	}
		// }
		// return new JsonResponse((array('status'=>'success', 'data'=>json_decode($roles->toJson()))), 200);
	}
}