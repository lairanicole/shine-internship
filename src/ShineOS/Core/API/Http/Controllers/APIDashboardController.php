<?php namespace ShineOS\Core\API\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\API\Http\Controllers\APIPatientController;
use ShineOS\Core\API\Http\Controllers\APIReferralsController;
use Shine\Repositories\Eloquent\PatientRepository as PatientRepository;
use Shine\Repositories\Eloquent\FacilityRepository as FacilityRepository;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use ShineOS\Core\Referrals\Http\Controllers\ReferralsController as ReferralsController;
use Illuminate\Http\JsonResponse;
use Request, Validator; 

class APIDashboardController extends Controller {
	
    public function __construct(PatientRepository $PatientRepository, FacilityRepository $FacilityRepository, HealthcareRepository $HealthcareRepository) {
        $this->PatientRepository = $PatientRepository;
        $this->FacilityRepository = $FacilityRepository;
        $this->HealthcareRepository = $HealthcareRepository;
    }

	public function index() {
		return response(json_encode(array('message'=>'No data')), 200);
	}

	public function dashboardTotalCount_get() {
		$data = Request::all();
		$validator = Validator::make($data, [
			'facility_id' => 'required', 
		]);

		if ($validator->fails()==false) { 
			$patientsResult = new APIPatientController($this->PatientRepository, $this->FacilityRepository, $this->HealthcareRepository);
			$patientsResult = $patientsResult->patientsResult($data['facility_id'])->total();

			$referralsInboundResult = new APIReferralsController($this->HealthcareRepository);
			$referralsInboundResult = $referralsInboundResult->referralsInbound($data['facility_id']);
			$referralsInboundResult = $referralsInboundResult['referrals']->total();

			$referralsOutboundResult = new APIReferralsController($this->HealthcareRepository);
			$referralsOutboundResult = $referralsOutboundResult->referralsDraftsOutbound($data['facility_id'], 'outbound');
			$referralsOutboundResult = $referralsOutboundResult['referrals']->total();

			$referralsDraftsResult = new APIReferralsController($this->HealthcareRepository);
			$referralsDraftsResult = $referralsDraftsResult->referralsDraftsOutbound($data['facility_id'], 'drafts');
			$referralsDraftsResult = $referralsDraftsResult['referrals']->total();


			$referralMessagesResult = new ReferralsController($this->HealthcareRepository);
			$referralMessagesResult = $referralMessagesResult->referralM(NULL, $data['facility_id']);
			$referralMessagesResult = $referralMessagesResult['referrals']->total();

			return new JsonResponse((array('status'=>'success','patients_count'=>$patientsResult, 'referrals_inbound'=>$referralsInboundResult, 'referrals_outbound'=> $referralsOutboundResult, 'referrals_drafts'=>$referralsDraftsResult, 'referral_messages'=>$referralMessagesResult)), 200);
		} else {
			 return new JsonResponse(array('status'=>'error', 'message'=>$validator->errors()->all()), 400);
		}
		
	}
	
}