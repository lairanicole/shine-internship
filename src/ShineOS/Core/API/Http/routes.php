<?php

/**
 * General API Routes-Controller
 */
Route::group(['prefix' => 'api',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::get('/', 'APIController@index');
	Route::post('/', 'APIController@index');
	Route::post('/register', 'APIController@register_post');

});

/**
 * Dashboard API Routes-Controller
 */
Route::group(['prefix' => 'api/dashboard',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::get('/dashboardTotalCount', 'APIDashboardController@dashboardTotalCount_get');

});

/**
 * Healthcare services API Routes-Controller
 */
Route::group(['prefix' => 'api/healthcareservices',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::post('/', 'APIHealthcareservicesController@index');
	Route::get('/visitsListByFacilityId', 'APIHealthcareservicesController@visitsListByFacilityId_get'); 
	Route::get('/healthcaretypeByHealthcareserviceid', 'APIHealthcareservicesController@healthcaretypeByHealthcareserviceid_get'); 
	Route::post('/insertHealthcareData', 'APIHealthcareservicesController@insertHealthcareData_post'); 

});

/**
 * Patients API Routes-Controller
 */
Route::group(['prefix' => 'api/patients',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::post('/monitoring', 'APIPatientController@monitoring_post');
	Route::get('/patientsListByFacilityId', 'APIPatientController@patientsListByFacilityId_get');
	Route::get('/patientDetailsByPatientId', 'APIPatientController@patientDetailsByPatientId_get');
	Route::get('/patientVisitsByPatientId', 'APIPatientController@patientVisitsByPatientId_get');

	Route::post('/insertPatientsRecord', 'APIPatientController@insertPatientsRecord_post');
	Route::post('/patientDeclareDead', 'APIPatientController@insertDeclareDead_post');
	Route::post('/test', 'APIPatientController@test');

	Route::post('/login', 'APIPatientController@login_post');
	Route::post('/updatePatientsRecord', 'APIPatientController@updatePatientsRecord_post');
	
	Route::get('/patientsAppointments', 'APIPatientController@patientsAppointments_get');
	Route::get('/patientsReferrals', 'APIPatientController@patientsReferrals_get');
	Route::get('/patientsPrescriptions', 'APIPatientController@patientsPrescriptions_get');

	Route::post('/register', 'APIPatientController@patientsRegistration_post');
	
});

/**
 * Users API Routes-Controller
 */
Route::group(['prefix' => 'api/users',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::post('/login', 'APIUsersController@login_post');
	Route::get('/userProfile', 'APIUsersController@userProfile_get');
	Route::get('/usersList', 'APIUsersController@usersList_get');
});

/**
 * Facilities API Routes-Controller
 */
Route::group(['prefix' => 'api/facilities',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::get('/facilityData', 'APIFacilitiesController@facilityData_get');
	Route::get('/facilityList', 'APIFacilitiesController@facilityList_get');
});

/**
 * Reminders API Routes-Controller
 */
Route::group(['prefix' => 'api/reminders',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::get('/allByFacilityId', 'APIRemindersController@reminders_get');
	Route::get('/createReminder', 'APIRemindersController@createReminder_get');
	Route::post('/insertReminder', 'APIRemindersController@insertReminder_post');

});

/**
 * Broadcast API Routes-Controller
 */
Route::group(['prefix' => 'api/broadcast',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::get('/allByFacilityId', 'APIRemindersController@broadcast_get');
});

/**
 * Referrals API Routes-Controller
 */
Route::group(['prefix' => 'api/referrals',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::get('/inboundByFacility', 'APIReferralsController@referralsInbound_get');
	Route::get('/outboundByFacility', 'APIReferralsController@referralsOutbound_get');
	Route::get('/draftsByFacility', 'APIReferralsController@referralsDrafts_get');
	Route::get('/messagesByFacility', 'APIReferralsController@referralMessages_get');
	Route::get('/createReferral', 'APIReferralsController@createReferral_get');
	Route::get('/viewReferralByReferralId', 'APIReferralsController@viewReferralByReferralId_get');
	
	Route::post('/insertReferral', 'APIReferralsController@insertReferral_post');
	Route::post('/updateReferralStatus', 'APIReferralsController@updateReferralStatus_post');
	Route::post('/addfollowup', 'APIReferralsController@addfollowup_post');
	Route::post('/replyToFollowUp', 'APIReferralsController@replyToFollowUp_post');

});

/**
 * Analytics API Routes-Controller
 */
Route::group(['prefix' => 'api/analytics',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::get('/analyticsdata', 'APIAnalyticsController@analyticsdata_get');
});


/**
 * Sync API Routes-Controller
 */
Route::group(['prefix' => 'api/sync',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::get('/getSyncDateTime', 'APISyncController@getSyncDateTime_get');
	Route::post('/manageRecords', 'APISyncController@manageRecords_post');
	Route::post('/cloudRecords', 'APISyncController@cloudRecords_post');
	Route::get('/checkFacilityID', 'APISyncController@checkFacilityID_get');

	Route::get('/compareTables_CE_SAAS', 'APISyncController@compareTables_CE_SAAS_get');
	Route::post('/insertLastSync', 'APISyncController@insertLastSync_post');


	Route::get('/checkTable', 'APISyncController@checkTable_get');
	Route::post('/userCreateUpdate', 'APISyncController@userCreateUpdate');
	Route::post('/syndata', 'APISyncController@syndata_get');
	Route::get('/getAllTables', 'APISyncController@getAllTables_get');

});

/**
 * APIs for FASSSTER
 */
Route::group(['prefix' => 'api/fassster',
				'middleware' => 'authapi',
				'namespace' => 'ShineOS\Core\API\Http\Controllers'], function()
{
	Route::get('/patients', 'APIFASSSTERController@getPatients');
	Route::get('/vitals', 'APIFASSSTERController@getVitals');

	Route::get('/getdemography', 'APIFASSSTERController@getDemographyIndividual');
	Route::get('/getvitals', 'APIFASSSTERController@getVitalsIndividual');
	Route::get('/getallergies', 'APIFASSSTERController@getAllergiesIndividual');
	Route::get('/getconsultations', 'APIFASSSTERController@getConsultationsIndividual');

	Route::get('/getall', 'APIFASSSTERController@getAllIndividual');
	Route::get('/getcomplete', 'APIFASSSTERController@getCompleteIndividual');
});