<?php

namespace ShineOS\Core\API\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class APITrail extends Model {
    use SoftDeletes;  
    protected $fillable = [];
    protected $dates = array('deleted_at','created_at','updated_at');
    protected $table = 'api_trail'; 
    protected $primaryKey = 'api_trail';
}