<?php

return [
    'name' => 'PHIE LITE',
    'icon' => 'fa-database',
    'roles' => '["Admin"]',
    'version' => '1.0',
    'title' => 'PHIE LITE',
    'folder' => 'PHIE',
    'table' => 'phie_sync',
    'description' => 'Module for PHIE Submission',
    'developer' => 'Ateneo ShineLabs',
    'copy' => '2016',
    'url' => 'www.shine.ph'
];
