<?php
  $TPD = 0;
  $TED = 0;
  $TXPD = 0;
  $TXED = 0;
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4 class="modal-title" id="myModalLabel"><i class="fa fa-database"></i> PHIE Submission for {{ $facilityname }} </h4>
</div>

<div class="modal-body" id="modal-body">
  <p>Below are the number of patient and encounter records submitted for PHIE and PCB. The <kbd class="bg-red">Errors</kbd> buttons indicate the number records which were rejected by PHIE for several reasons. To view the errors, click on the Error button.</p>
  <table class="table table-bordered table-striped dataTable">
    <tr><th width="20%">Year / Quarter submission</th><th>Patient Records</th><th>Encounter Records</th></tr>
    @foreach($PHIESync_all as $a => $b)
      @foreach($b as $c => $d)
        <tr>
          <td>{{ $a }} : {{ $c }}Q</td>
          <td>
            @if(isset($d['PD'])){{ $d['PD'] }}@else 0 @endif
            @if(isset($d['XPD']) AND $d['XPD'] > 0)<a href="{{ url('phie/showErrorDetails/'.$fid.'/'.$a.'/'.$c.'/PatientDataa') }}" class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#error_box">{{ $d['XPD'] }} Errors</a>@endif
          </td>
          <td>
            @if(isset($d['ED'])){{ $d['ED'] }}@else 0 @endif
            @if(isset($d['XED']) AND $d['XED'] > 0)<a href="{{ url('phie/showErrorDetails/'.$fid.'/'.$a.'/'.$c.'/EncounterDataa') }}" class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#error_box">{{ $d['XED'] }} Errors</a>@endif
          </td>
        </tr>
        <?php
          if(isset($d['PD'])) {
            $TPD += $d['PD'];
          }
          if(isset($d['ED'])) {
            $TED += $d['ED'];
          }
          if(isset($d['XPD'])) {
            $TXPD += $d['XPD'];
          }
          if(isset($d['XED'])) {
            $TXED += $d['XED'];
          }
        ?>
      @endforeach
      <tr><th>{{ $a }} Totals</th><th>{{ $TPD }}</th><th>{{ $TED }}</th></tr>
      <?php
        $TPD = 0;
        $TED = 0;
        $TXPD = 0;
        $TXED = 0;
      ?>
    @endforeach
  </table>
</div>
<div class="modal-footer" id="modal-footer">
  <p class="text-justify">You can verify and review these numbers by accessing PHIE Lite Portal using your account and find out the data that was successfully submitted and accepted by PHIE. It will also show the number of data successfully transferred to Philhealth for SAP processing.</p>
  <p><button class="btn btn-info btn-sml" data-dismiss="modal">Close</button></p>
</div>
