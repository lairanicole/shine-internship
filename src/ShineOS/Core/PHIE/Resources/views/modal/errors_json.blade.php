<?php
  $TPD = 0;
  $TED = 0;
  $TXPD = 0;
  $TXED = 0;
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4 class="modal-title" id="myModalLabel"><i class="fa fa-database"></i> Errors Details</h4>
</div>

<div class="modal-body" id="modal-body">
  <p>Below are the details of errors from PHIE</p>
  <table class="table table-bordered table-striped dataTable">
    @if($errors AND $errors[0]['id'] == 'PatientData')
      <tr>
          <th width="15%">Record Details</th>
          <th width="15%">Patient Name</th>
          <th width="35%">Description</th>
          <th width="35%">Error Details</th>
      </tr>
      @foreach($errors as $key=>$error)
        <tr>
          <td>Pat ID: {{$error['Patient ID']}}</td>
          <td>{{$error['Patient Name']}}</td>
          <td>{{$error['Description']}}</td>
          <td><b>{{ key($error['Error Details']) }}</b><br><em>{{ $error['Error Details'][key($error['Error Details'])] }}</em></td>
        </tr>
      @endforeach
    @endif

    @if($errors AND $errors[0]['id'] == 'EncounterData')
      <tr>
          <th width="15%">Record Details</th>
          <th width="15%">Patient Name</th>
          <th width="35%">Description</th>
          <th width="35%">Error Details</th>
      </tr>
      @foreach($errors as $key=>$error)
        <tr>
          <td>Pat ID: {{$error['Patient ID']}}<br/>Enc ID: {{$error['Encounter ID']}}</td>
          <td>{{$error['Patient Name']}}</td>
          <td>{{$error['Description']}}</td>
          <td><b>{{ key($error['Error Details']) }}</b><br><em>{{ $error['Error Details'][key($error['Error Details'])] }}</em></td>
        </tr>
      @endforeach
    @endif

    @if(!$errors)
    <tr><td><h3>No errors found</h3></td></tr>
    @endif
  </table>
</div>
<div class="modal-footer" id="modal-footer">
  <p><button class="btn btn-info btn-sml" data-dismiss="modal">Close</button></p>
</div>
