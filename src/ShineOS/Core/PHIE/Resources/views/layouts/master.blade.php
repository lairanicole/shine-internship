@extends('layout.master')
@section('title') SHINE OS+ | PHIE LITE @stop

@section('page-header')
  <section class="content-header">
    <h1>
      <i class="fa fa-database"></i>
      PHIE Lite
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">PHIE</li>
    </ol>
  </section>
@stop

@section('content')
    <div class="row">
        <!-- Main content -->
        @yield('content')
    </div><!-- /.row -->
@stop

