<?php

namespace ShineOS\Core\PHIE\Http\Controllers;

use Illuminate\Routing\Controller;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\UserHelper;
use ShineOS\Core\PHIE\Entities\PHIESync;
use ShineOS\Core\Users\Entities\Users;
use ShineOS\Core\PHIE\Http\Controllers\PHIEPatientsController;
use ShineOS\Core\PHIE\Http\Controllers\PHIEExaminationController;
use ShineOS\Core\PHIE\Http\Controllers\PHIEMedicalOrderController;
use ShineOS\Core\PHIE\Http\Controllers\PHIEMaternalCareController;
use ShineOS\Core\PHIE\Http\Controllers\PHIEHistoryController;
use ShineOS\Core\PHIE\Http\Controllers\PHIEHealthcareController;
use Shine\Libraries\IdGenerator;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\PHIE\Http\Middleware\PHIE_Soap_Connect;
use DB, Session;

class PHIEController extends Controller {
    public function __construct() {
        $this->middleware('auth.access:phie');

        $this->userInfo = UserHelper::getUserInfo();
        $this->facility = FacilityHelper::facilityInfo();
        $this->facilityInfo = $this->facility->facility_id;

        $this->wskey = $this->facility->wskey; // Web Service Key
        $this->ekey = $this->facility->ekey; // Encrption Key

        // $phielitetest = new PHIE_Soap_Connect;
        // $phielitetest = $phielitetest->phielitetest();
        // dd($phielitetest);
        // $this->index();

        // response code - response_code
        // 100	Web Service is Running
        // 101	Web Service is Under Maintenance
        // 102	Web Service Authentication Failed!
        // 103	Error : Invalid Parameter
        // 104	Success
        // 105	JSON File Converted Result
        // 106	PHIE Data Successfully Uploaded
        // 107	PHIE Data Failed to Upload
        // 108	Phil Health Number Successfully Retrieved!
        // 109	Phil Health Number Doesn't Exists!
        // 110	Phil Health PMRF Data Successfully Submitted
        // 111	Phil Health PMRF Data Submission Failed!
        // 112	Phil Health PMRF Data already Exists!
        // 113	Phil Health UPCM Billing Successfully Retrieved
        // 114	Phil Health UPCM Billing Doesn't Exists!
        // 115	Phil Health PMRF Dependent Data Successfully Submitted
        // 116	Phil Health PMRF Dependent Data Submission Failed!
        // 117	Phil Health PMRF Dependent Data already Submitted!
        // 118	Phil Health PMRF Data doesn't Exists!
    }

    public function indexxx() {
        Session::forget('_global_phie_type');
        //reset logs
        $this->install_log("Ready");
        $this->install_logg("ok");
        //let us get facilities for submission
        $data['facilities'] = Facilities::whereNotNull('ekey')->whereNotNull('wskey')->get();
        return view('phie::batch')->with($data);
    }

    public function index() {
        $dat = [];
        //let us process all patientdata
        $patsubmits = PHIESync::select('year','timeval','facility_id','user_id','response','excludeFile')
            ->where('facility_id',$this->facilityInfo)
            ->where('param_type','PatientDataa')
            ->orderBy('year','asc')
            ->orderBy('timeval','asc')
            ->get();
        $pjs = 0;
        $xpjs = 0;
        if(count($patsubmits)>0) {
            $pyear = $patsubmits[0]->year;
            $ptimeval = $patsubmits[0]->timeval;
            
            foreach($patsubmits as $patient) {
                $js = json_decode($patient->response, TRUE);
                $pstripped = str_replace('\"', '"', $patient->excludeFile);
                $xfs = json_decode(trim($pstripped,'"'), TRUE);
                if($patient->timeval != $ptimeval) {
                    $dat[$pyear][$ptimeval]['PD'] = $pjs - $xpjs;
                    $dat[$pyear][$ptimeval]['XPD'] = $xpjs;
                    $pjs = 0; //reset counter
                    $xpjs = 0; //reset counter
                    $pyear = $patient->year;
                    $ptimeval = $patient->timeval;
                }
                $pjs += count( $js['PatientData'] );
                if($xfs) {
                    $xpjs += count( $xfs );
                }
            }
            $dat[$patient->year][$patient->timeval]['PD'] = $pjs - $xpjs;
            $dat[$patient->year][$patient->timeval]['XPD'] = $xpjs;

            //let us process all encounterdata
            $consubmits = PHIESync::select('year','timeval','facility_id','user_id','response','excludeFile')
                ->where('facility_id',$this->facilityInfo)
                ->where('param_type','EncounterDataa')
                ->orderBy('year','asc')
                ->orderBy('timeval','asc')
                ->get();
            $cjs = 0;
            $xcjs = 0;
            $cyear = $consubmits[0]->year;
            $ctimeval = $consubmits[0]->timeval;
            
            foreach($consubmits as $consult) {
                $js = json_decode($consult->response, TRUE);
                $cstripped = str_replace('\"', '"', $consult->excludeFile);
                $xjs = json_decode(trim($cstripped,'"'), TRUE);
                if($consult->timeval != $ctimeval) {
                    $dat[$cyear][$ctimeval]['ED'] = $cjs - $xcjs;
                    $dat[$cyear][$ctimeval]['XED'] = $xcjs;
                    $cjs = 0; //reset counter
                    $xcjs = 0; //reset counter
                    $cyear = $consult->year;
                    $ctimeval = $consult->timeval;
                }
                $cjs += count( $js['EncounterData'] );
                if($xjs) {
                    $xcjs += count( $xjs );
                }
            }
            $dat[$consult->year][$consult->timeval]['ED'] = $cjs - $xcjs;
            $dat[$consult->year][$consult->timeval]['XED'] = $xcjs;
        }

        $data['PHIESync_all'] = $dat;
        $data['fid'] = $this->facilityInfo;
        $facility = Facilities::where('facility_id', $this->facilityInfo)->first();
        $data['facilityname'] = $facility->facility_name;
        return view('phie::index')->with($data);
    }

    public function showErrorDetails($fid,$year,$timeval,$type) {
        $errors = PHIESync::select('year','timeval','facility_id','response','excludeFile')
            ->where('facility_id',$fid)
            ->where('param_type',$type)
            ->where('year',$year)
            ->where('timeval',$timeval)
            ->get();
        
        $allerr = array();
        $a = 0;
        foreach($errors as $error) {
            /*if($error->excludeFile != '"null"') {
                $stripped = str_replace('\"', '"', $error->excludeFile);
                $err = json_decode( trim($stripped,'"'), TRUE );
                $allerr = array_merge($allerr, $err);
            }*/
            //dd($error->response);
            $responses = json_decode($error->response, TRUE);
            
            foreach($responses as $type=>$response) {
                foreach($response as $j) {
                    if($j['response_code'] == '103') {
                        $fullname = returnFullNameOfPatientID($j['sentdata']['Pat_Facility_No']);
                        $allerr[$a][ 'id' ] = $type;
                        $allerr[$a][ 'Patient ID' ] = $j['sentdata']['Pat_Facility_No'];
                        if($type == 'EncounterData') {
                            $allerr[$a][ 'Encounter ID' ] = $j['sentdata']['Encounter_ID'];
                        }
                        $allerr[$a][ 'Patient Name' ] = $fullname;
                        $allerr[$a][ 'Response_Code' ] = $j['response_code'];
                        $allerr[$a][ 'Description' ] = $j['soap_result']['PHIE']['Description'];
                        if($j['soap_result']['PHIE']['Invalid_Content_Count'] <> "0") {
                            $allerr[$a][ 'Error Details' ] = $j['soap_result']['PHIE']['InvalidDetails'];
                        }
                        if($j['soap_result']['PHIE']['Missing_Content_Count'] <> "0") {
                            $allerr[$a][ 'Error Details' ] = $j['soap_result']['PHIE']['MissingDetails'];
                        }
                        if($j['soap_result']['PHIE']['Unknown_Content_Count'] <> "0") {
                            $allerr[$a][ 'Error Details' ] = $j['soap_result']['PHIE']['UnknownDetails'];
                        }
                        $a++;
                    }
                    if($j['response_code'] == '107') {
                        $fullname = returnFullNameOfPatientID($j['sentdata']['Pat_Facility_No']);
                        $allerr[$a][ 'id' ] = $type;
                        $allerr[$a][ 'Patient ID' ] = $j['sentdata']['Pat_Facility_No'];
                        if($type == 'EncounterData') {
                            $allerr[$a][ 'Encounter ID' ] = $j['sentdata']['Encounter_ID'];
                        }
                        $allerr[$a][ 'Patient Name' ] = $fullname;
                        $allerr[$a][ 'Response_Code' ] = $j['response_code'];
                        $allerr[$a][ 'Description' ] = $j['soap_result']['PHIE'];
                        $allerr[$a][ 'Error Details' ] = array( "HTTP Error" => $j['response_desc']);
                        $a++;
                    }
                    if($j['response_code'] == 'HTTP') {
                        $fullname = returnFullNameOfPatientID($j['sentdata']['Pat_Facility_No']);
                        $allerr[$a][ 'id' ] = $type;
                        $allerr[$a][ 'Patient ID' ] = $j['sentdata']['Pat_Facility_No'];
                        if($type == 'EncounterData') {
                            $allerr[$a][ 'Encounter ID' ] = $j['sentdata']['Encounter_ID'];
                        }
                        $allerr[$a][ 'Patient Name' ] = $fullname;
                        $allerr[$a][ 'Response_Code' ] = $j['response_code'];
                        $allerr[$a][ 'Description' ] = $j['soap_result']['PHIE']['Description'];
                        $allerr[$a][ 'Error Details' ] = array( "HTTP Error" => $j['response_desc']);
                        $a++;
                    }
                }
            }
        }
        
        $data['errors'] = $allerr;
        return view('phie::modal.errors_json')->with($data);
    }

    public function checkLastSync($facility_id, $cdate, $param_type=NULL) {
        /** Check last sync */
        $data['PHIESync_all'] = $this->PHIESync_list($facility_id, $cdate, NULL, $param_type);
        //dd($data['PHIESync_all'][0]->updated_at);
        if($data['PHIESync_all']->count() > 0) {
            $data['updated_at'] = $data['PHIESync_all'][0]->updated_at;
        } else {
            $data['updated_at'] = "1970-01-01 00:00:00";
        }
        return $data;
    }

    public function PHIESync_list($facility_id, $cdate, $limit=NULL, $param_type=NULL) {

        $PHIESync = PHIESync::with('Facility')->where('facility_id',$facility_id)
                    ->where('param_type','LIKE',$param_type)
                    ->where( DB::raw( "DATE_FORMAT(`created_at`, '%Y-%m-%d')"), $cdate)
                    ->orderBy('updated_at', 'desc')->take($limit)->get();
        return $PHIESync;
    }

    public function showDetails($fid, $date) {
        $sql = "SELECT * FROM `phie_sync` WHERE DATE_FORMAT(`created_at`, '%Y-%m-%d') = '".$date."' AND `facility_id` = '".$fid."'";
        $data['PHIESync_all'] = DB::select( DB::raw($sql) );
        $data['fid'] = $fid;
        return view('phie::modal.modal_json_response')->with($data);
    }

    public function checkVPN() {
        //let us check server VPN
        $str = exec("ping -c2 210.4.103.175", $input, $result);
        if ($result == 0){
          $this->install_logg("VPN is Connected ...");
            flush();
            sleep(1);
        } else {
          echo shell_exec('sudo -S /home/user/connect-philhealth.sh');
          $this->install_logg("VPN Reconnected!");
            flush();
            sleep(1);
            $result == 0;
        }

        //let us check if the account is setup properly
        $this->install_logg("Checking account...");
        flush();
        sleep(1);

        if(!$this->wskey) {
            $this->install_logg("Error: Account wskey not configured.");
            flush();
            sleep(1);
            return 'error';
        }
        if(!$this->ekey) {
            $this->install_logg("Error: Account ekey  not configured.");
            flush();
            sleep(1);
            return 'error';
        }
        //if both keys are present - continue
        if($this->wskey AND $this->ekey AND $result = 0) {
            $this->install_logg("Account confirmed.");
            flush();
            sleep(1);
            return 'ok';
        }
    }

    public function getMaster($year) {
        $this->install_log("Processing...");
        $this->install_logg("Checking VPN connectivity...");
        flush();
        sleep(1);
        
        Session::put('_global_phie_type', 'batch');
        
        $facility = Facilities::where('facility_id', $this->facilityInfo)->whereNotNull('ekey')->whereNotNull('wskey')->whereNotNull('phic_accr_id')->first();

        Session::put('_batch_facility', $this->facility);

        if( $this->checkVPN() == 'error') {
            return 'VPN Connection Error. Please again later.';
            $this->install_log("done");
            $this->install_logg("ok");
        } else {

            //let us check connection first
            $this->install_log("Checking PHIE Webservice...");
            $PHIE_Soap_Connect = new PHIE_Soap_Connect;
            $resp = $PHIE_Soap_Connect->phielitetest();

            if($resp['response_code'] == 100){
                if(!is_dir(userfiles_path() .  'masterlists/'.$this->facilityInfo)) {
                    mkdir(userfiles_path() .  'masterlists/'.$this->facilityInfo);
                }
                $this->install_log("PHIE Webservice okay.<br />Retrieving Masterlist...");
                $getResp = $PHIE_Soap_Connect->phieGetMaster($year);
                if($getResp['response_code'] == 119) {
                    $this->install_log($getResp['response_desc']."<br />");
                    $this->install_log("Processing masterlist...");
                    $master = json_decode($getResp['soap_result']);
                    if($master){
                        //create the file
                        $masterfile = userfiles_path() .  'masterlists/'.$this->facilityInfo.'/'.$year.'_masterlist.csv';
                        if (!is_file($masterfile)) {
                            touch($masterfile);
                        }
                        if (is_file($masterfile)) {
                            @file_put_contents($masterfile, "FACILITY NAME: ".$facility->facility_name . "\n", FILE_APPEND);
                            @file_put_contents($masterfile, "EFFECTIVITY YEAR: ".$master->PHIE->Effectivity_Year . "\n", FILE_APPEND);
                            @file_put_contents($masterfile, "ACCREDICATION NO: ".$master->PHIE->Accredication_No . "\n", FILE_APPEND);
                            @file_put_contents($masterfile, "ASSIGNMENT LIST COUNT: ".$master->PHIE->Assignment_List_Count . "\n", FILE_APPEND);
                            @file_put_contents($masterfile, "#,Category ID,Members Pin,Last Name,First Name,Middle Name,Maiden Name,Suffix,Sex,Birthday,Assign Date" . "\n", FILE_APPEND);
                            foreach($master->PHIE->Assignment_List_Data as $c=>$data) {
                                @file_put_contents($masterfile, $c.",".$data->Category_ID.",".$data->Members_Pin.",".$data->Last_Name.",".$data->First_Name.",".$data->Middle_Name.",".$data->Maiden_Name.",".$data->Suffix.",".$data->Sex.",".$data->Birthday.",".$data->Assign_Date . "\n", FILE_APPEND);
                            }
                        }
                    } 
                    $this->install_log("Master List saved.");
                    $this->install_log("done");
                    $this->install_logg("ok");
                    //reload page
                    //return Redirect::to('phie');
                }
                if($getResp['response_code'] == 120) {
                    $this->install_log("done");
                    $this->install_logg("ok");
                    return 'No Assignment List Retrieved!';
                }
            } else {
                $this->install_log("done");
                $this->install_logg("ok");
                return 'PHIE Lite is under Maintenance';
            }
        }
    }

    public function getSAP($qyear) {
        $t = explode(':', $qyear);
        $qrt = $t[0];
        $year = $t[1];
        $this->install_log("Processing...");
        $this->install_logg("Checking VPN connectivity...");
        flush();
        sleep(1);
        if( $this->checkVPN() == 'error') {
            return 'VPN Account error';
            $this->install_log("done");
            $this->install_logg("ok");
        } else {

            //let us check connection first
            $this->install_log("Checking PHIE Webservice...");
            $PHIE_Soap_Connect = new PHIE_Soap_Connect;
            $resp = $PHIE_Soap_Connect->phielitetest();

            if($resp['response_code'] == 100){
                $this->install_log("PHIE Webservice okay.");

                $getResp = $PHIE_Soap_Connect->phieGetSAP($qrt, $year);
                $this->install_log("Starting to download...");
                if($getResp['response_code'] == 103) {
                    $this->install_log($getResp['response_desc']);
                    return 'error';
                }
                if($getResp['response_code'] == 113) {
                    $this->install_log("Retrieving SAP");
                }
                if($getResp['response_code'] == 114) {
                    $this->install_log("done_w_err");
                    $this->install_logg("Phil Health UPCM Billing Doesn&apos;t Exists!");
                    return 'fail';
                }
            } else {
                $this->install_log($getResp['response_desc']);
                return 'error';
            }
        }
    }

    public function batchPHIE($facid=NULL) {
        Session::forget('_global_phie_type');
        Session::forget('from');
        Session::forget('to');
        
        Session::put('_global_phie_type', 'batch');

        /*if( $this->checkVPN() == 'error') {
            $this->install_log("VPN Account error. Please check account.");
            exit;
        } else {*/
                //get all facilities registered to PHIE
                if($facid!=NULL) {
                    $facilities = Facilities::where('facility_id', $facid)->whereNotNull('ekey')->whereNotNull('wskey')->whereNull('deleted_at')->get();
                } else {
                    $facilities = Facilities::whereNotNull('ekey')->whereNotNull('wskey')->where('facility_name','<>','SHINE Lab')->whereNull('deleted_at')->get();
                }
                
                $this->install_log(count($facilities)." facilities found for submission.");

                foreach($facilities as $facility) {
                    Session::put('_batch_facility', $facility);
                    $this->install_log("Checking PHIE Webservice for ".$facility->facility_name);
                    $PHIE_Soap_Connect = new PHIE_Soap_Connect;
                    $resp = $PHIE_Soap_Connect->phielitetest();

                    //if service is running, continue
                    if($resp['response_code'] == 100) {
                        $this->install_log("PHIE Webservice okay. Starting...");
                        $this->install_log("<br />Submitting records for ".$facility->facility_name);
                        
                        //submit for this facility
                        $data = $EncounterData = array();
                        $PHIEPatientsController = new PHIEPatientsController();
                        $PHIEHealthcareController = new PHIEHealthcareController();
                        $PHIEExaminationController = new PHIEExaminationController();
                        $PHIEMedicalOrderController = new PHIEMedicalOrderController();
                        $PHIEMaternalCareController = new PHIEMaternalCareController();
                        $PHIEHistoryController = new PHIEHistoryController();

                        $PatientData = $PHIEPatientsController->PHIE_patientsData($facility->facility_id);
                        $data['PatientData'] = $PatientData['result'];

                        $EncounterData = $PHIEHealthcareController->PHIE_encounterLogData($facility->facility_id, $PatientData['PHIE_patientsData']);
                        $data['EncounterData'] = $EncounterData['result'];
        /*
                        $data['DeathData'] = $PHIEPatientsController->PHIE_Patients_DeathInfo($facility->facility_id);

                        $data['VitalSignData'] = $PHIEExaminationController->PHIE_PE_VitalSigns($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['SkinData'] = $PHIEExaminationController->PHIE_PE_Skin($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['HeentData'] = $PHIEExaminationController->PHIE_PE_HEENT($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['ChestLungsData'] = $PHIEExaminationController->PHIE_PE_ChestLungs($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['HeartData'] = $PHIEExaminationController->PHIE_PE_Heart($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['AbdomenData'] = $PHIEExaminationController->PHIE_PE_Abdomen($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['ExtremitiesData'] = $PHIEExaminationController->PHIE_PE_Extremities($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['ExaminationData'] = $PHIEMedicalOrderController->PHIE_MO_Examinations($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['ProcedureData'] = $PHIEMedicalOrderController->PHIE_MO_Procedures($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['DrugMedicinePrescriptionData'] = $PHIEMedicalOrderController->PHIE_MO_Prescription($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['PregnancyData'] = $PHIEMaternalCareController->PHIE_Maternal_Pregnancy($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['DeliveryData'] = $PHIEMaternalCareController->PHIE_Maternal_Delivery($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        //History Data
                        $data['PastMedicalHistoryData'] = $PHIEHistoryController->PHIE_Medical_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['PastSurgicalHistoryData'] = $PHIEHistoryController->PHIE_Surgical_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['FamilyHistoryData'] = $PHIEHistoryController->PHIE_Family_Medical_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['PersonalSocialHistoryData'] = $PHIEHistoryController->PHIE_Personal_Social_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['ImmunizationHistoryData_Children'] = $PHIEHistoryController->PHIE_Immunization_Children_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['ImmunizationHistoryData_YoungWomen'] = $PHIEHistoryController->PHIE_Immunization_Young_Women_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['ImmunizationHistoryData_Pregnancy'] = $PHIEHistoryController->PHIE_Immunization_Pregnancy_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['MenstrualHistoryData'] = $PHIEHistoryController->PHIE_Menstrual_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['PregnancyHistoryData'] = $PHIEHistoryController->PHIE_Pregnancy_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['FamilyAccessData'] = $PHIEHistoryController->PHIE_FamilyPlanning_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['DrugMedicineIntakeData'] = $PHIEHistoryController->PHIE_Drugs_Medicine_Intake_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        //PMRF Data
                        //$data['PMRF'] = $PHIEPMRFController->PHIE_PMRF($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
        */                        
                        Session::forget('_batch_facility');
                        $this->install_log("<br />Submission Done");

                    } else {
                        $this->install_log("PHIE Webservice not available");
                    }
                } 
        //}

        // clear cache
        Session::forget('_global_phie_type');
    }

    public function checkPHIE($facid=NULL) {
        Session::forget('_global_phie_type');
        Session::forget('from');
        Session::forget('to');

        Session::put('_global_phie_type', 'check');
        /*if( $this->checkVPN() == 'error') {
            $this->install_log("VPN Account error. Please check account.");
            exit;
        } else {*/
                //get all facilities registered to PHIE
                if($facid!=NULL) {
                    $facilities = Facilities::where('facility_id', $facid)->whereNotNull('ekey')->whereNotNull('wskey')->whereNull('deleted_at')->get();
                } else {
                    $facilities = Facilities::whereNotNull('ekey')->whereNotNull('wskey')->where('facility_name','<>','SHINE Lab')->whereNull('deleted_at')->get();
                }
                
                $this->install_log(count($facilities)." facilities found for submission.<br />");
                $c= 0;
                foreach($facilities as $facility) {
                    $c++;
                    Session::put('_batch_facility', $facility);
                    $this->install_log("Checking PHIE Webservice for ".$facility->facility_name);
                    //$PHIE_Soap_Connect = new PHIE_Soap_Connect;
                    //$resp = $PHIE_Soap_Connect->phielitetest();
                    
                    //if service is running, continue
                    //if($resp['response_code'] == 100){
                        $this->install_log("PHIE Webservice okay. Starting...");
                        
                        $this->install_log("<br />Submitting records for ".$facility->facility_name);
                        
                        //submit for this facility
                        $data = $EncounterData = array();
                        $PHIEPatientsController = new PHIEPatientsController();
                        $PHIEHealthcareController = new PHIEHealthcareController();
                        $PHIEExaminationController = new PHIEExaminationController();
                        $PHIEMedicalOrderController = new PHIEMedicalOrderController();
                        $PHIEMaternalCareController = new PHIEMaternalCareController();
                        $PHIEHistoryController = new PHIEHistoryController();

                        $PatientData = $PHIEPatientsController->PHIE_patientsData($facility->facility_id);
                        $data['PatientData'] = $PatientData['result'];

                        $EncounterData = $PHIEHealthcareController->PHIE_encounterLogData($facility->facility_id, $PatientData['PHIE_patientsData']);
                        $data['EncounterData'] = $EncounterData['result'];
        /*
                        $data['DeathData'] = $PHIEPatientsController->PHIE_Patients_DeathInfo($facility->facility_id);

                        $data['VitalSignData'] = $PHIEExaminationController->PHIE_PE_VitalSigns($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['SkinData'] = $PHIEExaminationController->PHIE_PE_Skin($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['HeentData'] = $PHIEExaminationController->PHIE_PE_HEENT($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['ChestLungsData'] = $PHIEExaminationController->PHIE_PE_ChestLungs($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['HeartData'] = $PHIEExaminationController->PHIE_PE_Heart($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['AbdomenData'] = $PHIEExaminationController->PHIE_PE_Abdomen($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['ExtremitiesData'] = $PHIEExaminationController->PHIE_PE_Extremities($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['ExaminationData'] = $PHIEMedicalOrderController->PHIE_MO_Examinations($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['ProcedureData'] = $PHIEMedicalOrderController->PHIE_MO_Procedures($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['DrugMedicinePrescriptionData'] = $PHIEMedicalOrderController->PHIE_MO_Prescription($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['PregnancyData'] = $PHIEMaternalCareController->PHIE_Maternal_Pregnancy($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        $data['DeliveryData'] = $PHIEMaternalCareController->PHIE_Maternal_Delivery($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        //History Data
                        $data['PastMedicalHistoryData'] = $PHIEHistoryController->PHIE_Medical_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['PastSurgicalHistoryData'] = $PHIEHistoryController->PHIE_Surgical_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['FamilyHistoryData'] = $PHIEHistoryController->PHIE_Family_Medical_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['PersonalSocialHistoryData'] = $PHIEHistoryController->PHIE_Personal_Social_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['ImmunizationHistoryData_Children'] = $PHIEHistoryController->PHIE_Immunization_Children_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['ImmunizationHistoryData_YoungWomen'] = $PHIEHistoryController->PHIE_Immunization_Young_Women_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['ImmunizationHistoryData_Pregnancy'] = $PHIEHistoryController->PHIE_Immunization_Pregnancy_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['MenstrualHistoryData'] = $PHIEHistoryController->PHIE_Menstrual_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['PregnancyHistoryData'] = $PHIEHistoryController->PHIE_Pregnancy_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['FamilyAccessData'] = $PHIEHistoryController->PHIE_FamilyPlanning_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        $data['DrugMedicineIntakeData'] = $PHIEHistoryController->PHIE_Drugs_Medicine_Intake_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);

                        //PMRF Data
                        //$data['PMRF'] = $PHIEPMRFController->PHIE_PMRF($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
        */                        
                        Session::forget('_batch_facility');

                        $this->install_log("Submission Done<br />----------<br />");

                    /*} else {
                        $this->install_log("PHIE Webservice not available");
                    }*/
                }
        //}

        // clear cache
        Session::forget('_global_phie_type');
    }

    public function singlePHIE($facid, $param, $from=NULL, $to=NULL) {
        Session::forget('_global_phie_type');
        Session::forget('from');
        Session::forget('to');

        if($param != 'PatientData' AND $param != 'EncounterData') {
            Session::put('_global_phie_type', 'single');
        } else if($param != 'PatientData' AND $param == 'EncounterData') {
            Session::put('_global_phie_type', 'singlex');
        } else {
            Session::put('_global_phie_type', 'singles');
        }
        Session::put('from', $from);
        Session::put('to', $to);
        $this->install_log("Starting submission for ".$param."<br />");
        /*if( $this->checkVPN() == 'error') {
            $this->install_log("VPN Account error. Please check account.");
            exit;
        } else {*/
                //get all facilities registered to PHIE
                $facilities = Facilities::where('facility_id', $facid)->whereNotNull('ekey')->whereNotNull('wskey')->whereNull('deleted_at')->get();
                
                $this->install_log(count($facilities)." facility found for submission.<br />");

                foreach($facilities as $facility) {
                    Session::put('_batch_facility', $facility);
                    $this->install_log("Checking PHIE Webservice for ".$facility->facility_name);
                    $PHIE_Soap_Connect = new PHIE_Soap_Connect;
                    $resp = $PHIE_Soap_Connect->phielitetest();

                    //if service is running, continue
                    if($resp['response_code'] == 100){
                        $this->install_log("PHIE Webservice okay. Starting...");
                        
                        $this->install_log("<br />Submitting records for ".$facility->facility_name);
                        
                        //submit for this facility
                        $data = $EncounterData = array();
                        $PHIEPatientsController = new PHIEPatientsController();
                        $PHIEHealthcareController = new PHIEHealthcareController();
                        $PHIEExaminationController = new PHIEExaminationController();
                        $PHIEMedicalOrderController = new PHIEMedicalOrderController();
                        $PHIEMaternalCareController = new PHIEMaternalCareController();
                        $PHIEHistoryController = new PHIEHistoryController();

                        $PatientData = $PHIEPatientsController->PHIE_patientsData($facility->facility_id);
                        $data['PatientData'] = $PatientData['result'];

                        if($param == 'EncounterData' OR ($param != 'PatientData' AND $param != 'EncounterData')) {
                        $EncounterData = $PHIEHealthcareController->PHIE_encounterLogData($facility->facility_id, $PatientData['PHIE_patientsData']);
                        $data['EncounterData'] = $EncounterData['result'];
                        }

                        if($param == 'DeathData') {
                        $data['DeathData'] = $PHIEPatientsController->PHIE_Patients_DeathInfo($facility->facility_id);
                        }

                        if($param == 'VitalSignData') {
                        $data['VitalSignData'] = $PHIEExaminationController->PHIE_PE_VitalSigns($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'SkinData') {
                        $data['SkinData'] = $PHIEExaminationController->PHIE_PE_Skin($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'HeentData') {
                        $data['HeentData'] = $PHIEExaminationController->PHIE_PE_HEENT($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'ChestLungsData') {
                        $data['ChestLungsData'] = $PHIEExaminationController->PHIE_PE_ChestLungs($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'HeartData') {
                        $data['HeartData'] = $PHIEExaminationController->PHIE_PE_Heart($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'AbdomenData') {
                        $data['AbdomenData'] = $PHIEExaminationController->PHIE_PE_Abdomen($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'ExtremitiesData') {
                        $data['ExtremitiesData'] = $PHIEExaminationController->PHIE_PE_Extremities($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'ExaminationData') {
                        $data['ExaminationData'] = $PHIEMedicalOrderController->PHIE_MO_Examinations($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'ProcedureData') {
                        $data['ProcedureData'] = $PHIEMedicalOrderController->PHIE_MO_Procedures($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'DrugMedicinePrescriptionData') {
                        $data['DrugMedicinePrescriptionData'] = $PHIEMedicalOrderController->PHIE_MO_Prescription($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'PregnancyData') {
                        $data['PregnancyData'] = $PHIEMaternalCareController->PHIE_Maternal_Pregnancy($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'DeliveryData') {
                        $data['DeliveryData'] = $PHIEMaternalCareController->PHIE_Maternal_Delivery($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'PastMedicalHistoryData') {
                        //History Data
                        $data['PastMedicalHistoryData'] = $PHIEHistoryController->PHIE_Medical_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'PastSurgicalHistoryData') {
                        $data['PastSurgicalHistoryData'] = $PHIEHistoryController->PHIE_Surgical_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'FamilyHistoryData') {
                        $data['FamilyHistoryData'] = $PHIEHistoryController->PHIE_Family_Medical_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'PersonalSocialHistoryData') {
                        $data['PersonalSocialHistoryData'] = $PHIEHistoryController->PHIE_Personal_Social_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'ImmunizationHistoryData_Children') {
                        $data['ImmunizationHistoryData_Children'] = $PHIEHistoryController->PHIE_Immunization_Children_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'ImmunizationHistoryData_YoungWomen') {
                        $data['ImmunizationHistoryData_YoungWomen'] = $PHIEHistoryController->PHIE_Immunization_Young_Women_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'ImmunizationHistoryData_Pregnancy') {
                        $data['ImmunizationHistoryData_Pregnancy'] = $PHIEHistoryController->PHIE_Immunization_Pregnancy_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'MenstrualHistoryData') {
                        $data['MenstrualHistoryData'] = $PHIEHistoryController->PHIE_Menstrual_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'PregnancyHistoryData') {
                        $data['PregnancyHistoryData'] = $PHIEHistoryController->PHIE_Pregnancy_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'FamilyAccessData') {
                        $data['FamilyAccessData'] = $PHIEHistoryController->PHIE_FamilyPlanning_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        if($param == 'DrugMedicineIntakeData') {
                        $data['DrugMedicineIntakeData'] = $PHIEHistoryController->PHIE_Drugs_Medicine_Intake_History($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        }

                        //PMRF Data
                        //$data['PMRF'] = $PHIEPMRFController->PHIE_PMRF($facility->facility_id, $EncounterData['PHIE_EncounterLog']);
                        
                        Session::forget('_batch_facility');

                        $this->install_log("<br />Submission Done");

                    } else {
                        $this->install_log("PHIE Webservice not available");
                    }
                }
        //}

        // clear cache
        Session::forget('_global_phie_type');
        Session::forget('from');
        Session::forget('to');
    }

    public function submitDatatoPHIE($facid) {

        Session::forget('_global_phie_type');
        Session::forget('from');
        Session::forget('to');

        $this->install_log("Processing...");
        /*$this->install_logg("Checking VPN connectivity...");
        flush();
        sleep(1);
        if( $this->checkVPN() == 'error') {
            return 'VPN Account error';
            $this->install_log("done");
            $this->install_logg("ok");
        } else {*/

            $facility = Facilities::where('facility_id', $facid)->whereNotNull('ekey')->whereNotNull('wskey')->whereNull('deleted_at')->first();

            Session::put('_batch_facility', $facility);
            $this->install_log("Checking PHIE Webservice for ".$facility->facility_name);

            //let us check connection first
            $this->install_logg("Checking PHIE Webservice...");
            $PHIE_Soap_Connect = new PHIE_Soap_Connect;
            $resp = $PHIE_Soap_Connect->phielitetest();

            //if service is running, continue
            if($resp['response_code'] == 100) {
                $this->install_logg("PHIE Webservice okay. Starting...");

                $data = $EncounterData = array();
                $PHIEPatientsController = new PHIEPatientsController();
                $PHIEHealthcareController = new PHIEHealthcareController();
                $PHIEExaminationController = new PHIEExaminationController();
                $PHIEMedicalOrderController = new PHIEMedicalOrderController();
                $PHIEMaternalCareController = new PHIEMaternalCareController();
                $PHIEHistoryController = new PHIEHistoryController();

                $PatientData = $PHIEPatientsController->PHIE_patientsData($this->facilityInfo);
                $data['PatientData'] = $PatientData['result'];

                $EncounterData = $PHIEHealthcareController->PHIE_encounterLogData($this->facilityInfo, $PatientData['PHIE_patientsData']);
                $data['EncounterData'] = $EncounterData['PHIE_EncounterLog'];

                $data['DeathData'] = $PHIEPatientsController->PHIE_Patients_DeathInfo($this->facilityInfo);

                $data['VitalSignData'] = $PHIEExaminationController->PHIE_PE_VitalSigns($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $data['SkinData'] = $PHIEExaminationController->PHIE_PE_Skin($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $data['HeentData'] = $PHIEExaminationController->PHIE_PE_HEENT($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $data['ChestLungsData'] = $PHIEExaminationController->PHIE_PE_ChestLungs($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $data['HeartData'] = $PHIEExaminationController->PHIE_PE_Heart($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $data['AbdomenData'] = $PHIEExaminationController->PHIE_PE_Abdomen($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $data['ExtremitiesData'] = $PHIEExaminationController->PHIE_PE_Extremities($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $data['ExaminationData'] = $PHIEMedicalOrderController->PHIE_MO_Examinations($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $data['ProcedureData'] = $PHIEMedicalOrderController->PHIE_MO_Procedures($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $data['DrugMedicinePrescriptionData'] = $PHIEMedicalOrderController->PHIE_MO_Prescription($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $data['PregnancyData'] = $PHIEMaternalCareController->PHIE_Maternal_Pregnancy($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $data['DeliveryData'] = $PHIEMaternalCareController->PHIE_Maternal_Delivery($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                //History Data
                $data['PastMedicalHistoryData'] = $PHIEHistoryController->PHIE_Medical_History($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);
                $data['PastSurgicalHistoryData'] = $PHIEHistoryController->PHIE_Surgical_History($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);
                $data['FamilyHistoryData'] = $PHIEHistoryController->PHIE_Family_Medical_History($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);
                $data['PersonalSocialHistoryData'] = $PHIEHistoryController->PHIE_Personal_Social_History($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);
                $data['ImmunizationHistoryData_Children'] = $PHIEHistoryController->PHIE_Immunization_Children_History($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);
                $data['ImmunizationHistoryData_YoungWomen'] = $PHIEHistoryController->PHIE_Immunization_Young_Women_History($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);
                $data['ImmunizationHistoryData_Pregnancy'] = $PHIEHistoryController->PHIE_Immunization_Pregnancy_History($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);
                $data['MenstrualHistoryData'] = $PHIEHistoryController->PHIE_Menstrual_History($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);
                $data['PregnancyHistoryData'] = $PHIEHistoryController->PHIE_Pregnancy_History($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);
                $data['FamilyAccessData'] = $PHIEHistoryController->PHIE_FamilyPlanning_History($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);
                $data['DrugMedicineIntakeData'] = $PHIEHistoryController->PHIE_Drugs_Medicine_Intake_History($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                //PMRF Data
                //$data['PMRF'] = $PHIEPMRFController->PHIE_PMRF($this->facilityInfo, $EncounterData['PHIE_EncounterLog']);

                $this->install_log("done");
                $this->install_logg("ok");

                //Nothing to Sync
                if(!array_filter(array_values($data))) {
                    return 'Nothing to sync';
                } else {
                    return 'success';
                }
            } else {
                $this->install_log("done");
                $this->install_logg("ok");
                return 'PHIE Lite is under Maintenance';
            }
        //}
    }

    public function sendArray($data, $param_type, $EncounterData = NULL, $filer = NULL) {
        $result = $return = $filer = NULL;
        $origEncounterData = $EncounterData;
        $filtering = 0;

        //if this is not for checking
        if(Session::get('_global_phie_type') != 'check') {
            $from = NULL; $to = NULL;
            if($param_type == 'PatientData') {
                $filtering = 1;
                $thisKey = "Pat_Facility_No";
            } elseif($param_type == 'EncounterData') {
                $filtering = 1;
                $thisKey = "Encounter_ID";
            }
            
            //let us find out if data submission is more than 100
            $batching = 1;
            if(count($data[$param_type]) > 100) {
                $batching = ceil(count($data[$param_type])/100);
            }
            /*for ($x = 1; $x <= $batching; $x++) {
                //if start and end is given
                $from = ($x*100) - 99;
                $to = 100 * $x;
                if(count($data) < $to) {
                    $to = count($data);
                }*/
                //but if from and end are given use them
                if(Session::get('from') != NULL AND Session::get('to') != NULL){
                    $from = Session::get('from');
                    $to = Session::get('to');
                } else {
                    $from = 1;
                    $to = count($data[$param_type]);
                }
                foreach ($data as $key => $value) {
                    if($key!='PHIESync_all' AND $key!='PHIESync_success' AND $value!=NULL) {
                        $c = count($value);
                        $ct = 0;
                        $xtime = strtotime("now");
                        foreach ($value as $v_key => $v_value) {
                            $ct++;
                            //dd($from, $to, $ct, $v_key);
                            if($from AND $to AND $ct >= $from AND $ct <= $to) {
                                $ntime = strtotime("now") - $xtime;
                                //$this->install_logg("Processing ". $param_type .".<br />Submitting ". $ct ." of ". $c ." records. [". date('H:i:s', $ntime) ."]");
                                flush();
                                $PHIE_Soap_Connect = new PHIE_Soap_Connect;
                                //$resp = $PHIE_Soap_Connect->phielitetest();

                                //if service is running, continue
                                //if($resp['response_code'] == 100) {
                                    $result[$key][$v_key] = $PHIE_Soap_Connect->phie_test_two($key, $v_value);
                                    //filter out data with error
                                    if($result[$key][$v_key]['response_code']=='103' AND $filtering == 1) {
                                        $removeme = $result[$key][$v_key]['sentdata'][$thisKey];
                                        unset($EncounterData[array_search($removeme, $EncounterData)]);
                                        //add to $filer error log
                                        if($param_type == 'PatientData') {
                                            $filer[] = [ 'id'=>$key, 'Patient ID' => $result[$key][$v_key]['sentdata']['Pat_Facility_No'], 'Patient Name'=>$result[$key][$v_key]['sentdata']['Pat_First_Name']." ".$result[$key][$v_key]['sentdata']['Pat_Last_Name'], 'Data Error'=>$result[$key][$v_key]['response_desc'] ];
                                        } elseif($param_type == 'EncounterData' OR $param_type == 'VitalSignData') {
                                            $fullname = returnFullNameOfPatientID($result[$key][$v_key]['sentdata']['Pat_Facility_No']);
                                            $filer[] = [ 'id'=>$key, 'Encounter Date'=>$result[$key][$v_key]['sentdata']['Encounter_Date'], 'Patient ID' => $result[$key][$v_key]['sentdata']['Pat_Facility_No'], 'Healthcare ID' => $result[$key][$v_key]['sentdata']['Encounter_ID'], 'Patient Name'=>$fullname, 'Data Error:'=>$result[$key][$v_key]['response_desc'] ];
                                        } elseif($param_type == 'DrugMedicinePrescriptionData') {
                                            $fullname = returnFullNameOfPatientID($result[$key][$v_key]['sentdata']['Pat_Facility_No']);
                                            $PHIE_Excluded_Encounter[] = [ 'id'=>$result_key, 'Encounter Date'=>$result[$key][$v_key]['sentdata']['Encounter_Date'], 'Patient ID' => $result[$key][$v_key]['sentdata']['Pat_Facility_No'], 'Healthcare ID' => $result[$key][$v_key]['sentdata']['Encounter_ID'], 'Pateint Name'=>$fullname, 'Data Error:'=>$result[$key][$v_key]['response_desc'] ];
                                        }
                                    }
                                /*} else {
                                    $result = NULL;
                                    $this->install_log("done");
                                    $this->install_logg("ok");
                                    return 'Connection disconnected';
                                }*/
                                $this->install_logg("[ ".$ct." | ".$result[$key][$v_key]['response_code']." ]");
                                //$this->install_logg("Result: ".$result[$key][$v_key]['response_code']);
                            }
                            
                            $xtime = strtotime("now");
                        }
                        if($filtering == 1) {
                            $return = [ 'result'=>$result, 'newData'=>$EncounterData ];
                        } else {
                            $return = $result;
                        }
                    }
                }
                //save logs
                if(Session::get('_batch_facility')) {
                    $f = Session::get('_batch_facility');
                    $fid = $f->facility_id;
                } else {
                    $fid = $this->facilityInfo;
                }
                if($result!=NULL) {
                    $insertToDB = $this->insertToDB($fid, $this->userInfo->user_id, json_encode($result), $param_type."a", $origEncounterData, json_encode($filer));
                }
                $this->install_logg("<br>");
            //}
        }

        return $return;
    }

    public function insertToDB($facility_id, $user_id, $response, $param_type, $EncounterData=NULL, $filer=NULL) {

        $insert = new PHIESync;
        $insert->sync_id = IdGenerator::generateId();
        $insert->facility_id = $facility_id;
        $insert->user_id = $user_id;
        $insert->response = $response;
        $insert->param_type = $param_type;
        if($param_type == 'EncounterData' AND $EncounterData!=NULL) {
            $insert->param_type_id = json_encode($EncounterData);
        }
        if($filer!=NULL) {
            $insert->excludeFile = json_encode($filer);
        }
        $response = $insert->save();

        return $response;
    }

    public function ref_logical2($value) {
        if($value=="0" OR $value=="No" OR $value=="NO" OR $value=="N" OR $value=="n") {
            return "N";
        } elseif($value=="1" OR $value=="Yes" OR $value=="YES" OR $value=="Y" OR $value=="y") {
            return "Y";
        } else {
            return "U";
        }
        return "U";
    }

    public function install_log($text) {
        $phietype = Session::get('_global_phie_type');
        if($phietype == 'check' OR $phietype == 'batch' OR $phietype == 'single' OR $phietype == 'singles' OR $phietype == 'singlex') {
            echo $text."<br />";
            ob_flush(); flush();
            usleep(1125000);
        } else {
            $log_file = userfiles_path() .  'phie_logs/phie_log.txt';
            if (!is_file($log_file)) {
                @touch($log_file);

            }
            if (is_file($log_file)) {
                $json = array('date' => date('H:i:s'), 'msg' => $text);

                if ($text == 'Ready' or $text == 'done') {
                    @file_put_contents($log_file, $text . "\n");
                } else {
                    @file_put_contents($log_file, $text . "\n", FILE_APPEND);

                }
            }
        }
    }

    public function install_logg($text) {
        $phietype = Session::get('_global_phie_type');
        if($phietype == 'check' OR $phietype == 'batch' OR $phietype == 'single' OR $phietype == 'singles' OR $phietype == 'singlex') {
            echo $text;
            ob_flush(); flush();
            usleep(1125000);
        } else {
            $log_file = userfiles_path() .  'phie_logs/phiee_log.txt';
            if (!is_file($log_file)) {
                @touch($log_file);
            }
            if (is_file($log_file)) {
                $json = array('date' => date('H:i:s'), 'msg' => $text);

                if ($text == 'ok' or $text == 'Please wait...') {
                    @file_put_contents($log_file, $text . "\n");
                } else {
                    @file_put_contents($log_file, $text . "\n", FILE_APPEND);

                }
            }
        }
    }

}
