<?php namespace ShineOS\Core\PHIE\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\LOV\Entities\LovDiseases;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientMedicalHistory;
use ShineOS\Core\PHIE\Http\Controllers\PHIEController;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Request, Input, DateTime, Session;

class PHIEHistoryController extends Controller {
    public function __construct() {
        //NOTES
        //NULL => not required
        //ref_logical2 => from ref_logical2 reference
        //integer => number
        //datetime => 0000-00-00
        //text => Not Stated

        $this->Arr_PastMedicalHistoryData = array("Allergy_Specified" => "NULL",
                                        "Cancer_Specified" => "NULL",
                                        "Hepatitis_Specified" => "NULL",
                                        "Specified_Organ_ Tuberculosis" => "NULL",
                                        "PTB_Category" => "NULL",
                                        "Other_Past_Medical_History_Specified" => "NULL",
                                        "Allergy" => "ref_logical2",
                                        "Asthma" => "ref_logical2",
                                        "Cancer" => "ref_logical2",
                                        "Cerebrovascular_Disease" => "ref_logical2",
                                        "Coronary_Artery_Disease" => "ref_logical2",
                                        "Diabetes_Mellitus" => "ref_logical2",
                                        "Emphysema" => "ref_logical2",
                                        "Epilepsy_Seizure_Disorder" => "ref_logical2",
                                        "Hepatitis" => "ref_logical2",
                                        "Hyperlipidemia" => "ref_logical2",
                                        "Hypertension" => "ref_logical2",
                                        "BP_Diastolic" => "integer",
                                        "BP_Systolic" => "integer",
                                        "Peptic_Ulcer_Disease" => "ref_logical2",
                                        "Pneumonia" => "ref_logical2",
                                        "Thyroid_Disease" => "ref_logical2",
                                        "Tuberculosis" => "ref_logical2",
                                        "Urinary_Tract_Infection" => "ref_logical2",
                                        "Other_Past_Medical_History" => "ref_logical2"
                                        );

        $this->Arr_PastSurgicalHistoryData = array("Past_SurgicalOptn" => "text",
                                        "Date_of_Operation" => "datetime");

        $this->Arr_FamilyHistoryData = array("Allergy" => "ref_logical2",
                                        "Asthma" => "ref_logical2",
                                        "Cancer" => "ref_logical2",
                                        "Cerebrovascular_Disease" => "ref_logical2",
                                        "Coronary_Artery_Disease" => "ref_logical2",
                                        "Diabetes_Mellitus" => "ref_logical2",
                                        "Emphysema" => "ref_logical2",
                                        "Epilepsy_Seizure" => "ref_logical2",
                                        "Hepatitis" => "ref_logical2",
                                        "Hyperlipidemia" => "ref_logical2",
                                        "Hypertension" => "ref_logical2",
                                        "Peptic_Ulcer_Disease" => "ref_logical2",
                                        "Thyroid_Disease" => "ref_logical2",
                                        "Tuberculosis" => "ref_logical2",
                                        "Stroke" => "ref_logical2",
                                        "Heart_Attack" => "ref_logical2",
                                        "Kidney_Disease" => "ref_logical2",
                                        "Specified_Allergy" => "NULL",
                                        "Specified_Cancer" => "NULL",
                                        "Specified_Hepatitis" => "NULL",
                                        "Specified_Tuberculosis" => "NULL",
                                        "PTB_Category" => "NULL",
                                        "Other_Family_Histories" => "NULL"
                                        );

        $this->Arr_PersonalSocialHistoryData = array("Smoking" => "ref_logical3",
                                                    "Drinking_Alcohol" => "ref_logical3",
                                                    "Taking_illicit_Drugs" => "ref_logical1",
                                                    "No_Pack_Smoke" => "integer",
                                                    "No_Bottle" => "integer"
                                                    );

        $this->Arr_ImmunizationHistoryData_Children = array("BCG" => "ref_logical1",
                                                            "OPV1" => "ref_logical1",
                                                            "OPV2" => "ref_logical1",
                                                            "OPV3" => "ref_logical1",
                                                            "DPT1" => "ref_logical1",
                                                            "DPT2" => "ref_logical1",
                                                            "DPT3" => "ref_logical1",
                                                            "MEASLES" => "ref_logical1",
                                                            "HEPA_B1" => "ref_logical1",
                                                            "HEPA_B2" => "ref_logical1",
                                                            "HEPA_B3" => "ref_logical1",
                                                            "HEPA_A" => "ref_logical1",
                                                            "VARICELLA" => "ref_logical1"
                                                            );

        $this->Arr_ImmunizationHistoryData_YoungWomen = array("HPV" => "ref_logical1",
                                                    "MMR" => "ref_logical1"
                                                    );

        $this->Arr_ImmunizationHistoryData_Pregnancy = array("TET_TOXOID" => "ref_logical1");

        $this->Arr_MenstrualHistoryData = array("No_Pads" => "integer",
                                                    "Age_Menopausal" => "integer",
                                                    "Menarche" => "integer",
                                                    "Last_Mentrual_Period" => "datetime",
                                                    "Menstrual_Period_Duration" => "integer",
                                                    "Menstrual_Cycle" => "integer",
                                                    "Onset_Sex" => "integer",
                                                    "Birth_Control_Method" => "text",
                                                    "Menopausal_Status" => "ref_logical1"
                                                    );

        $this->Arr_PregnancyHistoryData = array(
                                                    "Type_of_Delivery" => "text",
                                                    "Gravidity" => "integer",
                                                    "Parity" => "integer",
                                                    "Full_Term" => "integer",
                                                    "Premature" => "integer",
                                                    "Abortion" => "integer",
                                                    "LiveBirth" => "integer",
                                                    "Pre_Eclampsia" => "ref_logical1"
                                                );
        $this->Arr_FamilyAccessData = array('Family_Planning'=>'ref_logical1');

        $this->Arr_DrugMedicineIntakeData = array("OralHypoAgents"=>"ref_logical1",
                                                        "Hypertensive_Med" =>"ref_logical1");
    }

    public function findPatientsByFacilityId($facility_id, $compareDate, $data_Encounter_ID=NULL) {
        $PHIEController = new PHIEController;
        $result = Healthcareservices::query();
        if(!empty($data_Encounter_ID)) {
            $result = $result->whereIn('healthcare_services.healthcareservice_id', $data_Encounter_ID);
        } else {
            $arr_param_type_id = array();
            $param_type_id = $PHIEController->PHIESync_list($facility_id, NULL, 'EncounterData');
            foreach ($param_type_id as $key => $value) {
                $arr_param_type_id[] = json_decode($value->param_type_id);
            }
            $result = $result->whereIn('healthcare_services.healthcareservice_id', array_unique(array_flatten($arr_param_type_id)));
        }
        $result = $result->join('facility_patient_user','healthcare_services.facilitypatientuser_id','=','facility_patient_user.facilitypatientuser_id')
                ->join('facility_user','facility_patient_user.facilityuser_id','=','facility_user.facilityuser_id')
                ->join('facilities','facilities.facility_id','=','facility_user.facility_id')
                ->where('facilities.facility_id', $facility_id)
                ->where('facility_patient_user.deleted_at', NULL)
                ->where('healthcare_services.updated_at','>',$compareDate)
                ->whereYear('healthcare_services.encounter_datetime', '=', 2017)
                ->whereMonth('healthcare_services.encounter_datetime', '>', 3)
                ->whereMonth('healthcare_services.encounter_datetime', '<', 7)
                ->orderBy('healthcare_services.updated_at', 'DESC')
                ->get();

        if(count($result)) {
            foreach ($result as $key => $value) {
                $value->patientsData = Patients::with('patientMedicalHistory')->has('patientMedicalHistory')
                        ->whereHas('facilityUser', function($query) use ($facility_id) {
                            $query->where('facility_id', '=', $facility_id);
                        })
                        ->where('updated_at','>',$compareDate)
                        ->whereYear('updated_at', '=', 2017)
                        ->whereMonth('updated_at', '>', 3)
                        ->whereMonth('updated_at', '<', 7)
                        ->where('patient_id',$value->patient_id)
                        ->first();
            }
        }

        return $result;
    }

    public function lovDiseases_name_byId($disease_category, $disease_id) {
        $LovDiseases = LovDiseases::where('disease_category', $disease_category)->where('disease_id', $disease_id)->pluck('phie_name');
        return $LovDiseases;
    }

    public function lovDiseases_name($disease_category) {
        $LovDiseases = LovDiseases::where('disease_category', $disease_category)->lists('phie_name','disease_id');
        return $LovDiseases;
    }

    public function getLovDiseases_cat($disease_id) {
        $LovDiseaseCat = LovDiseases::where('disease_id', $disease_id)->first();
        if($LovDiseaseCat) {
            return $LovDiseaseCat->disease_category;
        }
    }

    public function History($history, $facility_id, $compareToDate, $EncounterData) {
        $PHIE_Medical_History = [];
        $mh = [];
        $PatientMedicalHistory = $this->findPatientsByFacilityId($facility_id, $compareToDate, $EncounterData);

        $lovDiseases = $this->lovDiseases_name($history)->toArray();
        if($PatientMedicalHistory) {
            foreach ($PatientMedicalHistory as $key => $val) {
                if($val->patientsData != null) {
                    foreach ($val->patientsData->patientMedicalHistory as $k => $v) {
                        //let us check if this disease_id is part of this MedHistory Category
                        $hiscat = $this->getLovDiseases_cat($v->disease_id);
                        //if value is empty then do not include it
                        if($hiscat == $history AND ($v->disease_status != "" AND $v->disease_status != NULL AND $v->disease_status != "NULL")) {

                            foreach ($lovDiseases as $klov => $vlov) {
                                $mh[$v->disease_id] = $v->disease_status;
                                if($v->disease_id == $klov) {
                                    $PHIE_Medical_History[$key][$vlov] = $v->disease_status;
                                }
                            }
                            $PHIE_Medical_History[$key]['Pat_Facility_No'] = !is_null($val->patientsData->patient_id) ? $val->patientsData->patient_id : "";
                            $PHIE_Medical_History[$key]['Encounter_ID'] = !is_null($val->healthcareservice_id) ? $val->healthcareservice_id : "";
                        }
                    }
                }
            }
        }
        return $PHIE_Medical_History;
    }

    public function PHIE_data_validation_diff($return, $key, $diff, $history) {
        foreach ($diff as $key_diff => $value_diff) {
            if($history[$value_diff] == 'ref_logical1' OR $history[$value_diff] == 'ref_logical2' OR $history[$value_diff] == 'ref_logical3') {
                $return[$key][$value_diff] = 'U';
            } else if($history[$value_diff] == 'ref_logical4') {
                $return[$key][$value_diff] = 'N';
            } else if($history[$value_diff] == 'integer') {
                $return[$key][$value_diff] = '0';
            } else if($history[$value_diff] == 'datetime') {
                $return[$key][$value_diff] = '1970-01-01';
            } else if($history[$value_diff] == 'NULL') {
                $return[$key][$value_diff] 	= 'NA';
            } else if($history[$value_diff] == 'text') {
                $return[$key][$value_diff] = 'Not Stated';
            } else { }
        }
        return $return;
    }

    public function PHIE_data_validation_intersect($return, $key, $value, $history) {
        foreach ($value as $k_val => $v_val) {
            if(array_key_exists($k_val, $history)) {
                if($history[$k_val] == 'integer') {
                    $return[$key][$k_val] = is_numeric($v_val) ? $v_val : 0;
                } else if($history[$k_val] == 'ref_logical1' OR $history[$k_val] == 'ref_logical2' OR $history[$k_val] == 'ref_logical3') {
                    $return[$key][$k_val] = is_string($v_val) ? substr($v_val,0,1) : 'U';
                } else if($history[$k_val] == 'ref_logical4') {
                    $return[$key][$k_val] = is_string($v_val) ? substr($v_val,0,1) : 'N';
                } else if($history[$k_val] == 'datetime') {
                    if (is_a($v_val, 'DateTime')) {
                          $return[$key][$k_val] = $v_val;
                    } else {
                        $return[$key][$k_val] = '1970-01-01';
                    }

                } else {
                    //do nothing
                }
            }
        }
        return $return;
    }

    /**
     * Past Medical History
     */
    public function PHIE_Medical_History($facility_id, $EncounterData) {
        $param_type = 'PastMedicalHistoryData';
        $history = $this->Arr_PastMedicalHistoryData;

        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $return = $this->History('Medical History',$facility_id, $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if(count($return)) {
            $c = count($return); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($return as $key => $value) {
                $return = $this->PHIE_data_validation_intersect($return, $key, $value, $history);
                $diff = array_diff(array_keys($history), array_keys($value));
                if($diff) {
                    $return = $this->PHIE_data_validation_diff($return, $key, $diff, $history);
                }
            }

            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". $c ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $return;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

    /**
     * Past Surgical History
     */
    public function PHIE_Surgical_History($facility_id, $EncounterData) {
        $param_type = 'PastSurgicalHistoryData';
        $history = $this->Arr_PastSurgicalHistoryData;

        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $return = $this->History('Surgical History',$facility_id, $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if(count($return)) {
            $c = count($return); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($return as $key => $value) {
                $return = $this->PHIE_data_validation_intersect($return, $key, $value, $history);
                $diff = array_diff(array_keys($history), array_keys($value));
                if($diff) {
                    $return = $this->PHIE_data_validation_diff($return, $key, $diff, $history);
                }
            }

            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". $c ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $return;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

    /**
     * Family History
     */
    public function PHIE_Family_Medical_History($facility_id, $EncounterData) {
        // return $this->History('Family Medical History',$facility_id, $compareToDate);
        $param_type = 'FamilyHistoryData';
        $history = $this->Arr_FamilyHistoryData;

        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $return = $this->History('Family Medical History',$facility_id, $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if(count($return)) {
            $c = count($return); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($return as $key => $value) {
                $return = $this->PHIE_data_validation_intersect($return, $key, $value, $history);
                $diff = array_diff(array_keys($history), array_keys($value));
                if($diff) {
                    $return = $this->PHIE_data_validation_diff($return, $key, $diff, $history);
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". $c ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $return;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

    /**
     * Personal/Social History
     */
    public function PHIE_Personal_Social_History($facility_id, $EncounterData) {
        // return $this->History('Personal/Social History',$facility_id, $compareToDate);
        $param_type = 'PersonalSocialHistoryData';
        $history = $this->Arr_PersonalSocialHistoryData;

        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $return = $this->History('Personal/Social History',$facility_id, $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if(count($return)) {
            $c = count($return); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($return as $key => $value) {
                $return = $this->PHIE_data_validation_intersect($return, $key, $value, $history);
                $diff = array_diff(array_keys($history), array_keys($value));
                if($diff) {
                    $return = $this->PHIE_data_validation_diff($return, $key, $diff, $history);
                }
            }

            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". $c ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $return;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }
    /**
     * Immunization History - Children
     */
    public function PHIE_Immunization_Children_History($facility_id, $EncounterData) {
        // return $this->History('Immunization History for Children',$facility_id, $compareToDate);
        $param_type = 'ImmunizationHistoryData_Children';
        $history = $this->Arr_ImmunizationHistoryData_Children;
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $return = $this->History('Immunization History for Children',$facility_id, $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if(count($return)) {
            $c = count($return); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($return as $key => $value) {
                $return = $this->PHIE_data_validation_intersect($return, $key, $value, $history);
                $diff = array_diff(array_keys($history), array_keys($value));
                if($diff) {
                    $return = $this->PHIE_data_validation_diff($return, $key, $diff, $history);
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". $c ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $return;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }
    /**
     * Immunization History - Young Women
     */

    public function PHIE_Immunization_Young_Women_History($facility_id, $EncounterData) {
        // return $this->History('Immunization History for Young Women',$facility_id, $compareToDate);
        $param_type = 'ImmunizationHistoryData_YoungWomen';
        $history = $this->Arr_ImmunizationHistoryData_YoungWomen;
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $return = $this->History('Immunization History for Young Women',$facility_id, $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if(count($return)) {
            $c = count($return); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($return as $key => $value) {
                $return = $this->PHIE_data_validation_intersect($return, $key, $value, $history);
                $diff = array_diff(array_keys($history), array_keys($value));
                if($diff) {
                    $return = $this->PHIE_data_validation_diff($return, $key, $diff, $history);
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". $c ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $return;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }
    /**
     * Immunization History - Pregnancy
     */

    public function PHIE_Immunization_Pregnancy_History($facility_id, $EncounterData) {
        // return $this->History('Immunization History for Pregnancy',$facility_id, $compareToDate);
        $param_type = 'ImmunizationHistoryData_Pregnancy';
        $history = $this->Arr_ImmunizationHistoryData_Pregnancy;
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $return = $this->History('Immunization History for Pregnancy',$facility_id, $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if(count($return)) {
            $c = count($return); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($return as $key => $value) {
                $return = $this->PHIE_data_validation_intersect($return, $key, $value, $history);
                $diff = array_diff(array_keys($history), array_keys($value));
                if($diff) {
                    $return = $this->PHIE_data_validation_diff($return, $key, $diff, $history);
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". $c ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $return;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }
    /**
     * Immunization History - Elderly and Immunocompromised
     * ---------------- NOT INCLUDED -----------------------
     */


    /**
     * Menstrual History
     */
    public function PHIE_Menstrual_History($facility_id, $EncounterData) {
        // return $this->History('Menstrual History',$facility_id, $compareToDate);
        $param_type = 'MenstrualHistoryData';
        $history = $this->Arr_MenstrualHistoryData;
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $return = $this->History('Menstrual History',$facility_id, $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if(count($return)) {
            $c = count($return); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($return as $key => $value) {
                $return = $this->PHIE_data_validation_intersect($return, $key, $value, $history);
                $diff = array_diff(array_keys($history), array_keys($value));
                if($diff) {
                    $return = $this->PHIE_data_validation_diff($return, $key, $diff, $history);
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". $c ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $return;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

    /**
     * Pregnancy History
     */
    public function PHIE_Pregnancy_History($facility_id, $EncounterData) {
        // return $this->History('Pregnancy History',$facility_id, $compareToDate);
        $param_type = 'PregnancyHistoryData';
        $history = $this->Arr_PregnancyHistoryData;
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $return = $this->History('Pregnancy History',$facility_id, $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if(count($return)) {
            $c = count($return); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($return as $key => $value) {
                $return = $this->PHIE_data_validation_intersect($return, $key, $value, $history);
                $diff = array_diff(array_keys($history), array_keys($value));
                if($diff) {
                    $return = $this->PHIE_data_validation_diff($return, $key, $diff, $history);
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". $c ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $return;
            $result = $PHIEController->sendArray($data, $param_type, $EncounterData);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

    /**
     * Family Planning Access
     */
    public function PHIE_FamilyPlanning_History($facility_id, $EncounterData) {
        // return $this->History('Family Planning Counseling',$facility_id, $compareToDate);
        $param_type = 'FamilyAccessData';
        $history = $this->Arr_FamilyAccessData;
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $return = $this->History('Family Planning Counseling',$facility_id, $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if(count($return)) {
            $c = count($return); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($return as $key => $value) {
                $return = $this->PHIE_data_validation_intersect($return, $key, $value, $history);
                $diff = array_diff(array_keys($history), array_keys($value));
                if($diff) {
                    $return = $this->PHIE_data_validation_diff($return, $key, $diff, $history);
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". $c ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $return;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

    /**
     * Drugs & Medicine Intake
     */

    public function PHIE_Drugs_Medicine_Intake_History($facility_id, $EncounterData) {
        // return $this->History('Drugs & Medicine Intake',$facility_id, $compareToDate);
        $param_type = 'DrugMedicineIntakeData';
        $history = $this->Arr_DrugMedicineIntakeData;
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $return = $this->History('Drugs & Medicine Intake',$facility_id, $compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if(count($return)) {
            $c = count($return); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($return as $key => $value) {
                $return = $this->PHIE_data_validation_intersect($return, $key, $value, $history);
                $diff = array_diff(array_keys($history), array_keys($value));
                if($diff) {
                    $return = $this->PHIE_data_validation_diff($return, $key, $diff, $history);
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". $c ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $return;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }
}
