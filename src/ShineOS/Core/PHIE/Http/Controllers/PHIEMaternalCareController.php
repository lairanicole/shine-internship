<?php namespace ShineOS\Core\PHIE\Http\Controllers;

use Illuminate\Routing\Controller;
use Plugins\MaternalCare\MaternalCareModel;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Request, Input, DateTime, Session;

class PHIEMaternalCareController extends Controller {

    public function maternalcare($facility_id,$type,$type_2=NULL,$compareToDate, $data_Encounter_ID=NULL) {
        $PHIEController = new PHIEController;
        $MaternalCareModel = Healthcareservices::query();
        if(!empty($data_Encounter_ID)) {
            $MaternalCareModel = $MaternalCareModel->whereIn('healthcare_services.healthcareservice_id', $data_Encounter_ID);
        } else {
            $arr_param_type_id = array();
            $param_type_id = $PHIEController->PHIESync_list($facility_id, NULL, 'EncounterData');
            foreach ($param_type_id as $key => $value) {
                $arr_param_type_id[] = json_decode($value->param_type_id);
            }
            $MaternalCareModel = $MaternalCareModel->whereIn('healthcare_services.healthcareservice_id', array_unique(array_flatten($arr_param_type_id)));
        }
        $MaternalCareModel = $MaternalCareModel->has('MaternalCare')
                ->with(array('MaternalCare' => function($query) use ($type, $type_2) {
                        $query->with($type);
                        $query->has($type);
                        if($type_2!=NULL) {
                            $query->with($type_2);
                            $query->has($type_2);
                        }
                    }))
                ->join('facility_patient_user','healthcare_services.facilitypatientuser_id','=','facility_patient_user.facilitypatientuser_id')
                ->join('facility_user','facility_patient_user.facilityuser_id','=','facility_user.facilityuser_id')
                ->join('facilities','facilities.facility_id','=','facility_user.facility_id')
                ->where('facilities.facility_id', $facility_id)
                ->where('healthcare_services.deleted_at','=',NULL)
                ->where('facility_patient_user.deleted_at','=',NULL)
                ->where('healthcare_services.updated_at','>',$compareToDate)
                ->whereYear('healthcare_services.encounter_datetime', '=', 2017)
                ->whereMonth('healthcare_services.encounter_datetime', '>', 3)
                ->whereMonth('healthcare_services.encounter_datetime', '<', 7)
                ->get();

        return $MaternalCareModel;
    }

/**
 * Pregnancy
 */
    public function PHIE_Maternal_Pregnancy($facility_id, $EncounterData) {
        $param_type = 'PregnancyData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_Maternal_Pregnancy = [];
        $MaternalCare = $this->maternalcare($facility_id,'prenatal','deliveries',$compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if($MaternalCare) {
            $c = count($MaternalCare); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($MaternalCare as $key => $value) {
                foreach ($value->MaternalCare as $key_m => $value_m) {
                    $PHIE_Maternal_Pregnancy[$key]['Pat_Facility_No'] = !is_null($value->patient_id) ? $value->patient_id : "";
                    $PHIE_Maternal_Pregnancy[$key]['Encounter_ID'] = !is_null($value->healthcareservice_id) ?  $value->healthcareservice_id : "";
                    foreach ($value_m->prenatal as $key_pr => $value_pr) {
                        $m_last_menstruation_period = "";
                        $y_last_menstruation_period = "";
                        if(is_null($value_pr->last_menstruation_period)==FALSE) {
                            $datetime = new Datetime($value_pr->last_menstruation_period);
                            $m_last_menstruation_period = $datetime->format('m');
                            $y_last_menstruation_period = $datetime->format('Y');
                        }
                        $PHIE_Maternal_Pregnancy[$key]['Month_Pregnancy_Started'] =$m_last_menstruation_period;
                        $PHIE_Maternal_Pregnancy[$key]['Year_Pregnancy_Started'] = $y_last_menstruation_period;

                        $PHIE_Maternal_Pregnancy[$key]['Gravida'] = !is_null($value_pr->gravidity) ?  $value_pr->gravidity : "";
                        $PHIE_Maternal_Pregnancy[$key]['Parity'] = !is_null($value_pr->parity) ?  $value_pr->parity : "";
                        $PHIE_Maternal_Pregnancy[$key]['Term'] = !is_null($value_pr->term) ?  $value_pr->term : "";
                        $PHIE_Maternal_Pregnancy[$key]['Preterm'] = !is_null($value_pr->pre_term) ?  $value_pr->pre_term : "";
                        $PHIE_Maternal_Pregnancy[$key]['Abortion'] = !is_null($value_pr->abortion) ?  $value_pr->abortion : "";
                        $PHIE_Maternal_Pregnancy[$key]['Livebirths'] = !is_null($value_pr->abortion) ?  $value_pr->abortion : "";
                    }
                    foreach ($value_m->deliveries as $key_de => $value_de) {
                        $PHIE_Maternal_Pregnancy[$key]['Outcome'] = !is_null($value_de->termination_outcome) ?  $value_de->termination_outcome : "";
                        $PHIE_Maternal_Pregnancy[$key]['Outcome_Others_Specify'] = !is_null($value_de->Outcome_Others_Specify) ?  $value_de->Outcome_Others_Specify : "NA";
                    }
                }
            }

            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_Maternal_Pregnancy) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_Maternal_Pregnancy;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

/**
 * Delivery - Mother
 */
    public function PHIE_Maternal_Delivery($facility_id, $EncounterData) {
        $param_type = 'DeliveryData';
        //compare date with param type
        $PHIEController = new PHIEController;
        $updated_at = $PHIEController->checkLastSync($facility_id, $param_type);
        $compareToDate = $updated_at['updated_at'];

        $PHIE_Maternal_Delivery = [];
        $MaternalCare = $this->maternalcare($facility_id,'deliveries',NULL,$compareToDate, $EncounterData);
        $PHIEController->install_logg("Please wait...");

        if($MaternalCare) {
            $c = count($MaternalCare); $ct = 0;
            $PHIEController->install_log("Collecting ".$param_type.".");
            foreach ($MaternalCare as $key => $value) {
                foreach ($value->MaternalCare as $key_m => $value_m) {
                    $PHIE_Maternal_Delivery[$key]['Pat_Facility_No'] = !is_null($value->patient_id) ? $value->patient_id : "";
                    $PHIE_Maternal_Delivery[$key]['Encounter_ID'] = !is_null($value->healthcareservice_id) ?  $value->healthcareservice_id : "";

                    foreach ($value_m->deliveries as $key_de => $value_de) {
                        $mDeliveryDate = "";
                        $mDeliveryTime = "";
                        if(is_null($value_de->termination_datetime)==FALSE) {
                            $datetime = new Datetime($value_de->termination_datetime);
                            $mDeliveryDate = $datetime->format('Y-m-d');
                            $mDeliveryTime = $datetime->format('Y-m-d');
                        }
                        $PHIE_Maternal_Delivery[$key]['mDeliveryDate'] = $mDeliveryDate;
                        $PHIE_Maternal_Delivery[$key]['mDeliveryTime'] = $mDeliveryTime;


                        $PHIE_Maternal_Delivery[$key]['mModeDelivery'] = !is_null($value_de->delivered_type) ?  $value_de->delivered_type : "";
                        $PHIE_Maternal_Delivery[$key]['mModeDelivery_Operative'] = !is_null($value_de->delivery_type_mode) ?  $value_de->delivery_type_mode : "";
                        $PHIE_Maternal_Delivery[$key]['mPlaceDelivery'] = !is_null($value_de->delivery_place_type) ?  $value_de->delivery_place_type : "";

                        // dd($value_de->delivery_place_type);
                        if($value_de->delivery_place_type == "FB") {
                            $PHIE_Maternal_Delivery[$key]['mPlaceDelivery_FacilityBased'] = !is_null($value_de->delivery_place) ?  $value_de->delivery_place : "";
                            $PHIE_Maternal_Delivery[$key]['mPlaceDelivery_NID'] = "";
                            $PHIE_Maternal_Delivery[$key]['mPlaceDelivery_NID_Others_Specify'] = "";
                        } elseif($value_de->delivery_place_type == "NID") {
                            $PHIE_Maternal_Delivery[$key]['mPlaceDelivery_FacilityBased'] = "";
                            $PHIE_Maternal_Delivery[$key]['mPlaceDelivery_NID'] = !is_null($value_de->delivery_place) ?  $value_de->delivery_place : "";
                            $PHIE_Maternal_Delivery[$key]['mPlaceDelivery_NID_Others_Specify'] = "";
                        } else {
                            $PHIE_Maternal_Delivery[$key]['mPlaceDelivery_FacilityBased'] = "";
                            $PHIE_Maternal_Delivery[$key]['mPlaceDelivery_NID'] = "";
                            $PHIE_Maternal_Delivery[$key]['mPlaceDelivery_NID_Others_Specify'] = !is_null($value_de->mPlaceDelivery_NID_Others_Specify) ?  $value_de->mPlaceDelivery_NID_Others_Specify : "";
                        }

                        $PHIE_Maternal_Delivery[$key]['mBirthMultiplicity'] = !is_null($value_de->birth_multiplicity) ?  $value_de->birth_multiplicity : "";
                        $PHIE_Maternal_Delivery[$key]['mTypeofMultiplicity'] = !is_null($value_de->multiple_births) ?  $value_de->multiple_births : "";
                        $PHIE_Maternal_Delivery[$key]['mAttendantDelivery'] = !is_null($value_de->attendant) ?  $value_de->attendant : "";
                        $PHIE_Maternal_Delivery[$key]['mAttendantDelivery_Others_Specify'] = !is_null($value_de->mAttendantDelivery_Others_Specify) ?  $value_de->mAttendantDelivery_Others_Specify : "";
                        $PHIE_Maternal_Delivery[$key]['mPregnancyOutcome'] = !is_null($value_de->termination_outcome) ?  $value_de->termination_outcome : "";
                        $PHIE_Maternal_Delivery[$key]['Remarks'] = !is_null($value_de->Remarks) ?  $value_de->Remarks : "";
                    }
                }
            }
            $PHIEController->install_logg("Processing ".$param_type.".<br />Submitting ". count($PHIE_Maternal_Delivery) ." records.");
            if(Session::get('_global_phie_type') == 'check') {
            } else {
            $data[$param_type] = $PHIE_Maternal_Delivery;
            $result = $PHIEController->sendArray($data, $param_type);
            return $result;
            }
        } else {
            $PHIEController->install_log("No ".$param_type." to submit.");
            return 'Nothing to sync';
        }
    }

/**
 * Neonatal (Babies)
 */

// Pat_Facility_No
// Encounter_ID
// nLastName
// nFirstName
// nMiddleName
// nSuffixName
// nSex
// nAgeGestation
// nGestationTerm
// nBornAlive
// nBirthWeight
// nBirthOrder
// nBabyStatus
// nPlaceDelivery
// nPlaceDelivery_FacilityBased
// nPlaceDelivery_NID
// nPlaceDelivery_NID_Others_Specify

    public function PHIE_Maternal_Neonatal($facility_id, $compareToDate) {
        $PHIE_Maternal_Neonatal = [];
        return $PHIE_Maternal_Neonatal;
    }
}
