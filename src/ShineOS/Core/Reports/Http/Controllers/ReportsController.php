<?php
namespace ShineOS\Core\Reports\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Healthcareservices\Entities\Diagnosis;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientDeathInfo;
use ShineOS\Core\Referrals\Entities\Referrals;
use ShineOS\Core\Reports\Entities\Reports;
use ShineOS\Core\Reports\Entities\M1;
use ShineOS\Core\Reports\Entities\M2;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\Utils;
use Shine\Libraries\Utils\Lovs; 
use ShineOS\Core\Reports\Http\Controllers\FHSISReportsController;
use SplFixedArray;
use View,Response,Validator,Input,Mail,Session,Cache,Redirect,Hash,Auth,DateTime,DB;

class ReportsController extends Controller {

    protected $moduleName = 'Reports';
    protected $modulePath = 'Reports';
    protected $viewPath = 'reports::pages.';
    protected $fhsisPath = 'reports::pages.fhsis_reports.';

    public function __construct() { 
        View::addNamespace('reports', 'src/ShineOS/Core/Reports/Resources/Views');
 
        $this->monthlyPrograms = array(
                                    'prg_ChildCare' => 'Child Care',
                                    'prg_FamilyPlanning' => 'Family Planning',
                                    'prg_MaternalCare' => 'Maternal Care',
                                    'grp_DiseaseControl' => array(
                                            'prg_Filariasis' => 'Filariasis',
                                            'prg_Leprosy' => 'Leprosy',
                                            'prg_Malaria' => 'Malaria',
                                            'prg_Schistosomiasis' => 'Schistosomiasis',
                                            'prg_Tuberculosis' => 'Tuberculosis'
                                        ),
                                    'prg_Morbidity' => 'Morbidity M2',
                                    'prg_Morbidity_Others' => 'Morbidity Others M2',
                                );
        $this->quarterlyPrograms = array(
                                    'prg_ChildCare' => 'Child Care',
                                    'prg_DentalHealth' => 'Dental Health',
                                    'prg_FamilyPlanning' => 'Family Planning',
                                    'prg_MaternalCare' => 'Maternal Care',
                                    'grp_DiseaseControl' => array(
                                            'prg_Filariasis' => 'Filariasis',
                                            'prg_Leprosy' => 'Leprosy',
                                            'prg_Malaria' => 'Malaria',
                                            'prg_Schistosomiasis' => 'Schistosomiasis',
                                            'prg_Tuberculosis' => 'Tuberculosis'
                                        ),
                                    'prg_Morbidity' => 'Morbidity Q2',
                                    'prg_Morbidity_Others' => 'Morbidity Others Q2',
                                );
        $this->annualPrograms = array(
                                    'grp_Abrgy' => array(
                                            'prg_DemographicProfile' => 'Demographic Profile',
                                            'prg_EnvironmentalHealth' => 'Environmental Health',
                                            'prg_Population' => 'Population',
                                            'prg_Mortality' => 'Mortality',
                                            'prg_Natality' => 'Natality'
                                        ),
                                    'prg_Morbidity' => 'Morbidity A2',
                                    'prg_Morbidity_Others' => 'Morbidity Others A2',
                                    'prg_MortalityBHS' => 'Mortality A3',
                                    'prg_MortalityBHS_Others' => 'Mortality Others A3',

                                );

        $this->arrProgramType = array('prg_ChildCare' => 'Child Care',
                                'prg_DemographicProfile' => 'Demographic Profile',
                                'prg_DentalHealth' => 'Dental Health',
                                'prg_EnvironmentalHealth' => 'Environmental Health',
                                'prg_FamilyPlanning' => 'Family Planning',
                                'prg_Filariasis' => 'Filariasis',
                                'prg_Leprosy' => 'Leprosy',
                                'prg_Malaria' => 'Malaria',
                                'prg_MaternalCare' => 'Maternal Care',
                                'prg_Morbidity' => 'Morbidity BHS',
                                'prg_Mortality' => 'Mortality',
                                'prg_MortalityBHS' => 'Mortality BHS',
                                'prg_Natality' => 'Natality',
                                'prg_Population' => 'Population',
                                'prg_Schistosomiasis' => 'Schistosomiasis',
                                'prg_Tuberculosis' => 'Tuberculosis');
        $this->arrQuarterly = array('1st'=>array('1','2','3'),
                                    '2nd'=>array('4','5','6'),
                                    '3rd'=>array('7','8','9'),
                                    '4th'=>array('10','11','12'));
        
        // reports for student schedule: course variable
        $tempCourses = DB::select('SELECT DISTINCT(course_id) FROM patient_studentinfo');
        $this->courses = array_map(function($val){
            return (array) $val;
        }, $tempCourses);
        // dd($this->courses[0]);

        // reports for student schedule: year level variable
        $this->yearLevels = new SplFixedArray(7);
        for($i=0;$i<7;$i++){
            $this->yearLevels[$i] = $i + 1;
        }
    }
    
    public function index() {
        $thisfacility = json_decode(Session::get('facility_details'));

        if ($thisfacility->ownership_type == 'PRIVATE') {
            return $this->privateFacilityReports();
        }
        else {
            return $this->publicFacilityReports();
        }
    }

    private function privateFacilityReports() {
        $data['facility'] = json_decode(Session::get('facility_details'));

        return view($this->viewPath.'private', $data);
    }

    private function publicFacilityReports() {
        $data['nocatchment'] = FALSE;
        $data['facility'] = json_decode(Session::get('facility_details'));
        $getArrFacilityAllBrgyCode = Lovs::getArrFacilityAllBrgyCode($data['facility']->facility_id);

        if($getArrFacilityAllBrgyCode==null) {
            $facbrgys = NULL;
            $fbs = getPatientBrgys(); //get facility barangay by CITY
            foreach($fbs as $fbk => $fbv) {
                $facbrgys[$fbv->barangay_code] = $fbv->barangay_name;
            }
            $data['nocatchment'] = TRUE;
            $data['facilitybrgycodes'] = $facbrgys;
        } else {
            // dd($getArrFacilityAllBrgyCode,$data['facility']->facility_contact->barangay);
            $facilitybrgycodes = $getArrFacilityAllBrgyCode;
            
            $brgy_facility[$data['facility']->facility_contact->barangay] = getBrgyName($data['facility']->facility_contact->barangay);

            $facilitybrgycodes_merge = array_replace($facilitybrgycodes,$brgy_facility); 
            $data['facilitybrgycodes'] = array_map("unserialize", array_unique(array_map("serialize", $facilitybrgycodes_merge)));

            // dd($brgy_facility,$facilitybrgycodes,$data['facilitybrgycodes']);

        } 
        $data['arrMonth'] = getArrMonth();
        $datetime = new DateTime($data['facility']->created_at);
        $data['arrM1year'] = $datetime->format('Y');
        $data['yearNow'] = date('Y');
        $data['programType'] = $this->arrProgramType;
        $data['monthlyPrograms'] = $this->monthlyPrograms;
        $data['quarterlyPrograms'] = $this->quarterlyPrograms;
        $data['annualPrograms'] = $this->annualPrograms;
        $data['courses'] = $this->courses;
        $data['yearLevels'] = $this->yearLevels;
        return view($this->viewPath.'public', $data);
    }

    private function analytics() {
        $facility = FacilityHelper::facilityInfo();

        //to be removed later on
        $facility_id = FacilityHelper::facilityInfo();
        $data['chart'] = $this->getData();
        $data['patient_count'] = Patients::whereHas('facilityUser', function($query) use ($facility) {
                    $query->where('facility_id', '=', $facility_id->facility_id); })->count();
        $data['visit_count'] = Healthcareservices::count();
        $data['referral_count'] = Referrals::where('facility_id', '=', $facility_id->facility_id)->count(); // change this to facility ID

        return $data;
    }

    private function getFilteredResults ()
    {
        $from = Input::get('from');
        $to = Input::get('to');
        $filterBy = Input::get('filterBy');

        echo Reports::getGraphByAge($from,$to);
    }

    private function getData ($type=NULL, $from=NULL, $to=NULL)
    {
        if ($type == 'patient'):
            $patient_stats = Reports::getPatientData($from, $to);
            $patient_stats1 = Reports::getVisitData($from, $to);
            //dd($patient_stats1);
        else:
            Reports::getVisitData($from, $to);
        endif;
    }

    private function getReportDataJSON()
    {
        $data = array();

        if (Input::get('from') == NULL):
            $from = new DateTime('tomorrow -1 week');
            $from = $from->format('Y-m-d H:i:s');
            $to = new DateTime();
            $to = $to->format('Y-m-d H:i:s');
        else:
            $from = Input::get('from');
            $to = Input::get('to');
        endif;

        /**
         * Change query / variable name
         */
        $data['top_patients'] = DB::select('SELECT patients.last_name, patients.first_name, patients.middle_name, count(*) as visits FROM healthcare_services JOIN facility_patient_user ON healthcare_services.facilitypatientuser_id = facility_patient_user.facilitypatientuser_id JOIN patients ON facility_patient_user.patient_id = patients.patient_id WHERE healthcare_services.created_at BETWEEN :from_date AND :to_date AND facility_patient_user.deleted_at = NULL AND patients.deleted_at = NULL GROUP BY healthcare_services.facilitypatientuser_id ORDER BY count(*) DESC LIMIT 10', ['from_date' => $from, 'to_date', $to]);

        $data['count_by_gender_sex'] = DB::select('SELECT last_name, first_name, middle_name, patient_id, gender, age, count(*) as total FROM patients JOIN facility_patient_user ON patients.patient_id = facility_patient_user.patient_id WHERE patients.created_at BETWEEN :from_date AND :to_date AND facility_patient_user.deleted_at = NULL AND patients.deleted_at = NULL group by age, gender', ['from_date' => $from, 'to_date', $to]);
        $data['count_by_gender_sex'] = isset($data['count_by_gender_sex'][0]) ? $data['count_by_gender_sex'][0]->total : 0;

        $data['count_by_services_rendered'] = DB::select('SELECT healthcareservicetype_id, count(*) as total FROM healthcare_services JOIN facility_patient_user ON healthcare_services.facilitypatientuser_id = facility_patient_user.facilitypatientuser_id WHERE healthcare_services.created_at BETWEEN :from_date AND :to_date AND facility_patient_user.deleted_at = NULL group by facilitypatientuser_id ORDER BY count(*) DESC', ['from_date' => $from, 'to_date', $to]);
        $data['count_by_services_rendered'] = isset($data['count_by_services_rendered'][0]) ? $data['count_by_services_rendered'][0]->total : 0;

        $data['count_by_disease'] = DB::select('SELECT healthcareservicetype_id, count(*) as total FROM healthcare_services JOIN general_consultation ON healthcare_services.healthcareservice_id = general_consultation.healthcareservice_id JOIN diagnosis ON healthcare_services.healthcareservice_id = diagnosis.healthcareservice_id JOIN lov_diseases ON diagnosis.diagnosislist_id = lov_diseases.disease_id JOIN facility_patient_user ON healthcare_services.facilitypatientuser_id = facility_patient_user.facilitypatientuser_id WHERE healthcare_services.created_at BETWEEN :from_date AND :to_date AND facility_patient_user.deleted_at = NULL group by facilitypatientuser_id ORDER BY count(*) DESC', ['from_date' => $from, 'to_date', $to]);
        $data['count_by_disease'] = isset($data['count_by_disease'][0]) ? $data['count_by_disease'][0]->total : 0;

        echo json_encode($data);
    }

    public static function getGeoData($year,$brgycode=NULL)
    {
        $facility = Session::get('facility_details');
        $facility_id = $facility->facility_id;

        $file = plugins_path() . 'ShineLab_Geodata' . DS . 'GeodataModel.php';
        if(file_exists($file))
        {
            $location = 'Plugins\ShineLab_Geodata\GeodataModel';
            $geodatamodel = new $location;
            $geodata = $geodatamodel::where('facility_id', $facility_id);
            if($brgycode!=NULL) { $geodata=$geodata->whereIn('brgycode', (array)$brgycode)->get(); }
            else { $geodata=$geodata->get(); } 
            return $geodata;
        }
        return NULL;
    }
    // public function frequentSchedules(){
    //     $val = DB::select("SELECT sum(`M700`), sum(`M730`), sum(`M800`), sum(`M830`), sum(`M900`), sum(`M930`), sum(`M1000`), sum(`M1030`), sum(`M1100`), sum(`M1130`), sum(`M1200`), sum(`M1230`), sum(`M1300`), sum(`M1330`), sum(`M1400`), sum(`M1430`), sum(`M1500`), sum(`M1530`), sum(`M1600`), sum(`M1630`), sum(`M1700`), sum(`M1730`), sum(`M1800`), sum(`M1830`), sum(`M1900`), sum(`M1930`), sum(`M2000`), sum(`M2030`), sum(`M2100`), sum(`T700`), sum(`T730`), sum(`T800`), sum(`T830`), sum(`T900`), sum(`T930`), sum(`T1000`), sum(`T1030`), sum(`T1100`), sum(`T1130`), sum(`T1200`), sum(`T1230`), sum(`T1300`), sum(`T1330`), sum(`T1400`), sum(`T1430`), sum(`T1500`), sum(`T1530`), sum(`T1600`), sum(`T1630`), sum(`T1700`), sum(`T1730`), sum(`T1800`), sum(`T1830`), sum(`T1900`), sum(`T1930`), sum(`T2000`), sum(`T2030`), sum(`T2100`), sum(`W700`), sum(`W730`), sum(`W800`), sum(`W830`), sum(`W900`), sum(`W930`), sum(`W1000`), sum(`W1030`), sum(`W1100`), sum(`W1130`), sum(`W1200`), sum(`W1230`), sum(`W1300`), sum(`W1330`), sum(`W1400`), sum(`W1430`), sum(`W1500`), sum(`W1530`), sum(`W1600`), sum(`W1630`), sum(`W1700`), sum(`W1730`), sum(`W1800`), sum(`W1830`), sum(`W1900`), sum(`W1930`), sum(`W2000`), sum(`W2030`), sum(`W2100`), sum(`TH700`), sum(`TH730`), sum(`TH800`), sum(`TH830`), sum(`TH900`), sum(`TH930`), sum(`TH1000`), sum(`TH1030`), sum(`TH1100`), sum(`TH1130`), sum(`TH1200`), sum(`TH1230`), sum(`TH1300`), sum(`TH1330`), sum(`TH1400`), sum(`TH1430`), sum(`TH1500`), sum(`TH1530`), sum(`TH1600`), sum(`TH1630`), sum(`TH1700`), sum(`TH1730`), sum(`TH1800`), sum(`TH1830`), sum(`TH1900`), sum(`TH1930`), sum(`TH2000`), sum(`TH2030`), sum(`TH2100`), sum(`F700`), sum(`F730`), sum(`F800`), sum(`F830`), sum(`F900`), sum(`F930`), sum(`F1000`), sum(`F1030`), sum(`F1100`), sum(`F1130`), sum(`F1200`), sum(`F1230`), sum(`F1300`), sum(`F1330`), sum(`F1400`), sum(`F1430`), sum(`F1500`), sum(`F1530`), sum(`F1600`), sum(`F1630`), sum(`F1700`), sum(`F1730`), sum(`F1800`), sum(`F1830`), sum(`F1900`), sum(`F1930`), sum(`F2000`), sum(`F2030`), sum(`F2100`) FROM patient_studentinfo WHERE course_id = '$course' AND year_level = '$year';");
    //     $val = $val[0];
    //     $val = (array) $val;
    //     // arsort($val);
    //     $keys = collect($val)->map(function($k, $v){
    //         return substr($v, 5, strlen($v)-7);
    //     })->all();
    //     $values = collect($val)->values()->map(function($num){
    //         return (integer) $num;
    //     })->all();
    // }

    public function commonsched($course, $year){
        $checkStudents = DB::select("SELECT * FROM patient_studentinfo WHERE course_id = '$course' AND year_level = '$year';");
        if(!$checkStudents){
            return "No registered students in this course and year level.";
        }
        else{
            $val = DB::select("SELECT sum(`M700`), sum(`M730`), sum(`M800`), sum(`M830`), sum(`M900`), sum(`M930`), sum(`M1000`), sum(`M1030`), sum(`M1100`), sum(`M1130`), sum(`M1200`), sum(`M1230`), sum(`M1300`), sum(`M1330`), sum(`M1400`), sum(`M1430`), sum(`M1500`), sum(`M1530`), sum(`M1600`), sum(`M1630`), sum(`M1700`), sum(`M1730`), sum(`M1800`), sum(`M1830`), sum(`M1900`), sum(`M1930`), sum(`M2000`), sum(`M2030`), sum(`M2100`), sum(`T700`), sum(`T730`), sum(`T800`), sum(`T830`), sum(`T900`), sum(`T930`), sum(`T1000`), sum(`T1030`), sum(`T1100`), sum(`T1130`), sum(`T1200`), sum(`T1230`), sum(`T1300`), sum(`T1330`), sum(`T1400`), sum(`T1430`), sum(`T1500`), sum(`T1530`), sum(`T1600`), sum(`T1630`), sum(`T1700`), sum(`T1730`), sum(`T1800`), sum(`T1830`), sum(`T1900`), sum(`T1930`), sum(`T2000`), sum(`T2030`), sum(`T2100`), sum(`W700`), sum(`W730`), sum(`W800`), sum(`W830`), sum(`W900`), sum(`W930`), sum(`W1000`), sum(`W1030`), sum(`W1100`), sum(`W1130`), sum(`W1200`), sum(`W1230`), sum(`W1300`), sum(`W1330`), sum(`W1400`), sum(`W1430`), sum(`W1500`), sum(`W1530`), sum(`W1600`), sum(`W1630`), sum(`W1700`), sum(`W1730`), sum(`W1800`), sum(`W1830`), sum(`W1900`), sum(`W1930`), sum(`W2000`), sum(`W2030`), sum(`W2100`), sum(`TH700`), sum(`TH730`), sum(`TH800`), sum(`TH830`), sum(`TH900`), sum(`TH930`), sum(`TH1000`), sum(`TH1030`), sum(`TH1100`), sum(`TH1130`), sum(`TH1200`), sum(`TH1230`), sum(`TH1300`), sum(`TH1330`), sum(`TH1400`), sum(`TH1430`), sum(`TH1500`), sum(`TH1530`), sum(`TH1600`), sum(`TH1630`), sum(`TH1700`), sum(`TH1730`), sum(`TH1800`), sum(`TH1830`), sum(`TH1900`), sum(`TH1930`), sum(`TH2000`), sum(`TH2030`), sum(`TH2100`), sum(`F700`), sum(`F730`), sum(`F800`), sum(`F830`), sum(`F900`), sum(`F930`), sum(`F1000`), sum(`F1030`), sum(`F1100`), sum(`F1130`), sum(`F1200`), sum(`F1230`), sum(`F1300`), sum(`F1330`), sum(`F1400`), sum(`F1430`), sum(`F1500`), sum(`F1530`), sum(`F1600`), sum(`F1630`), sum(`F1700`), sum(`F1730`), sum(`F1800`), sum(`F1830`), sum(`F1900`), sum(`F1930`), sum(`F2000`), sum(`F2030`), sum(`F2100`) FROM patient_studentinfo WHERE course_id = '$course' AND year_level = '$year';");
            $val = $val[0];
            $val = (array) $val;
            // arsort($val);
            $keys = collect($val)->map(function($k, $v){
                return substr($v, 5, strlen($v)-7);
            })->all();
            $values = collect($val)->values()->map(function($num){
               return (integer) $num;
            })->all();

            // dd($keys, $values);
            // dd($val);
            return view($this->viewPath.'commonsched', compact('keys','values','val','days'));
        }
    }

    public function m1($type,$month,$year,$barangay) { //monthly
        $facilityInfo = Session::get('user_details');
        $facility = FacilityHelper::facilityInfo();
        $facility_id = $facilityInfo->facilities[0]->facility_id;

        if($barangay=='allm1barangay') {
            $catchment_area = json_decode($facility->catchment_area);
            if($catchment_area==NULL) {
                $b = DB::table('lov_barangays')->where('city_code',$facility->facility_contact->city)
                ->lists('barangay_code');
                $barangay = $b;
            } else {
                $barangay = $catchment_area;
            }
        }

        $geodata = $this->getGeoData($year,$barangay);
        $brgys = getPatientBrgys();
        $formType = 'M1';
        if($type == 'prg_Morbidity' OR $type == 'prg_Morbidity_Others') {
            $formType = 'M2';    
        }
        $classType = 'BRGY';
        $data = FHSISReportsController::view_fhsis($type,$month,$year,$barangay);
        // return view($this->fhsisPath.$type, compact('facilityInfo', 'facility', 'month', 'year', 'geodata', 'brgys', 'barangay', 'data','programType'));
        return view($this->fhsisPath.$type, compact('facilityInfo', 'facility', 'geodata', 'brgys','data','programType','formType','classType'));

    }

    public function q1($type,$quarter,$year,$barangay) { //quarterly
        $month = $this->arrQuarterly[$quarter];

        $facilityInfo = Session::get('user_details');
        $facility = FacilityHelper::facilityInfo();
        $facility_id = $facilityInfo->facilities[0]->facility_id;

        if($barangay=='allq1barangay') {
            $catchment_area = json_decode($facility->catchment_area);
            if($catchment_area==NULL) {
                $b = DB::table('lov_barangays')->where('city_code',$facility->facility_contact->city)
                ->lists('barangay_code');
                $barangay = $b;
            } else {
                $barangay = $catchment_area;
            }
        }

        $geodata = $this->getGeoData($year,$barangay);
        $brgys = getPatientBrgys();
        $formType = 'Q1';
        if($type == 'prg_Morbidity' OR $type == 'prg_Morbidity_Others') {
            $formType = 'Q2';    
        }
        $classType = 'RHU';
        $data = FHSISReportsController::view_fhsis($type,$month,$year,$barangay,$quarter);
        // return view($this->fhsisPath.$type, compact('facilityInfo', 'facility', 'month', 'year', 'geodata', 'brgys', 'barangay', 'data','programType'));
        return view($this->fhsisPath.$type, compact('facilityInfo', 'facility', 'geodata', 'brgys','data','programType','formType','classType'));

    }

    public function a1($type,$month,$year,$barangay) { //annual data
        $month = array('1','2','3','4','5','6','7','8','9','10','11','12');
        $facilityInfo = Session::get('user_details');
        $facility = FacilityHelper::facilityInfo();

        if($barangay=='alla1barangay') {
            $catchment_area = json_decode($facility->catchment_area);
            if($catchment_area==NULL) {
                $b = DB::table('lov_barangays')->where('city_code',$facility->facility_contact->city)
                ->lists('barangay_code');
                $barangay = $b;
            } else {
                $barangay = $catchment_area;
            }
        }
        $geodata = $this->getGeoData($year,$barangay);
        $brgys = getPatientBrgys();
        $formType = 'A1';
        if($type == 'prg_Morbidity' OR $type == 'prg_Morbidity_Others') {
            $formType = 'A2';    
        }
        if($type == 'prg_MortalityBHS') {
            $formType = 'A3';    
        }
        $classType = 'RHU';
        $data = FHSISReportsController::view_fhsis($type,$month,$year,$barangay);
        return view($this->fhsisPath.$type, compact('facilityInfo', 'facility', 'geodata', 'brgys','data','programType','formType','classType'));

    }

    public static function generateReport($type=NULL, $month=NULL, $year=NULL, $brgycode=NULL)
    { 
        $_this = new self();
        if(is_null($month)) { $month = date('m'); }
        $facility_id = Session::get('facility_details')->facility_id;
        $type = ($type == NULL) ? 'count' : $type;
        $list_patient_id = NULL;
        if($type == 'mortality' OR $type == 'mortality_others') {

           $facilityusers = FacilityUser::whereHas('facilities',function($query) use ($facility_id) {
                                    $query->where('facility_id', $facility_id);
                                })->lists('facilityuser_id');
            $list_patient_id = FacilityPatientUser::whereIn('facilityuser_id',$facilityusers)
                            ->whereHas('patients',function($query) use ($brgycode) {
                            $query->whereHas('patientContact',function($query) use ($brgycode) {
                                $query->whereIn('barangay', (array)$brgycode);
                            });
                        })
                        ->lists('patient_id');
        }

        if($type == 'morbidity_others') {
            $diseases =  DB::table('fhsis_m2')
                     ->where('diagnosisYear', '=', $year)
                     ->whereIn('diagnosisMonth',(array)$month)
                     ->where('facility_id',$facility_id)
                     ->where('diagnosislist_id','NOT LIKE','%to follow%')
                     ->where('diagnosislist_id','NOT LIKE','%test%')
                     ->where('diagnosislist_id','!=','')
                     ->where('diagnosislist_id','!=','*')
                     ->where('diagnosislist_id','NOT LIKE','%None%')
                     ->where('diagnosislist_id','NOT LIKE','%vaccine%')
                     ->whereNotIn('diagnosislist_id',array_keys(getDiseases()))
                     ->orderBy('count','DESC')
                     ->limit(30)
                     ->lists('count','diagnosislist_id');
            $type = 'count';
        } else if($type == 'mortality_others') {
            $diseases =  PatientDeathInfo::where('Immediate_Cause_of_Death','NOT LIKE','%to follow%')
                        ->where('Immediate_Cause_of_Death','NOT LIKE','%test%')
                        ->where('Immediate_Cause_of_Death','!=','')
                        ->whereNotNull('Immediate_Cause_of_Death')
                        ->where('Immediate_Cause_of_Death','!=','*')
                        ->where('Immediate_Cause_of_Death','NOT LIKE','%None%')
                        ->where('Immediate_Cause_of_Death','NOT LIKE','%vaccine%')
                        ->whereNotIn('Immediate_Cause_of_Death',array_keys(getDiseases()))
                        ->whereIn('patient_id',$list_patient_id)
                        ->whereYear('datetime_death','=',$year)
                        ->lists('id','Immediate_Cause_of_Death');
        }
        else {
            $diseases = getDiseases();
        }
        $ageGroup = getAgeGroups();
        
        
        $m2 = array(); 
        foreach ($diseases as $k => $v):
            $m2[$k]['code'] = $v;

            foreach($ageGroup as $key => $val):
                $m2[$k]['details'][$val]['F'] = $_this->getDiseaseCount($type, 'F', $val, $k, NULL, $month, $year, $brgycode,$list_patient_id);
                $m2[$k]['details'][$val]['M'] = $_this->getDiseaseCount($type, 'M', $val, $k, NULL, $month, $year, $brgycode,$list_patient_id);
            endforeach;
        endforeach;
        // dd($m2);
        return $m2;
    }

    private function getDiseaseCount($type, $gender, $age, $disease, $disease_code = NULL, $month, $year, $brgycode = NULL,$list_patient_id=NULL)
    {
        $facilityInfo = Session::get('user_details');
        $facility_id = $facilityInfo->facilities[0]->facility_id;
        $age = explode("-",$age);
        $disease_count = 0;

        if($type == 'count') { // count only
            $disease_query = NULL;
            $disease_split = (preg_split('/ +/', $disease)); 
            $ctr = 0;
            foreach ($disease_split as $disease_k => $disease_v) {
                $ctr += 1;
                $disease_query .= "`diagnosislist_id` LIKE '%".$disease_v."%'";
                if($ctr < count($disease_split)) {
                    $disease_query .= " AND ";
                }
            } 
            $disease_count = DB::table('fhsis_m2')
                    ->where('gender', $gender)
                    ->whereRaw( "(".$disease_query.")" )
                    ->where('facility_id', $facility_id)
                    ->whereIn('brgycode', (array)$brgycode)
                    ->whereIn('diagnosisMonth', (array)$month)
                    ->where('diagnosisYear', $year)
                    ->whereBetween('age', array($age[0], $age[1]))
                    ->sum('count');
        }
        elseif($type == 'mortality' OR $type == 'mortality_others') {
            $disease_query = NULL;
            $disease_split = (preg_split('/ +/', $disease)); 
            $ctr = 0;
            foreach ($disease_split as $disease_k => $disease_v) {
                $ctr += 1;
                $disease_query .= "`Immediate_Cause_of_Death` LIKE '%".$disease_v."%'";

                if($ctr < count($disease_split)) {
                    $disease_query .= " AND ";
                }
            }

            $disease_count = Patients::with('patientDeathInfo')
                        ->whereHas('patientDeathInfo',function($query) use ($year,$disease_query) {
                            $query->whereYear('datetime_death','=',$year);
                            $query->whereRaw( "(".$disease_query.")" );
                        })
                        ->whereIn('patient_id',$list_patient_id)
                        ->where('gender',$gender)
                        ->whereRaw("(TIMESTAMPDIFF(YEAR, `birthdate`, NOW()) >=".$age[0].")")
                        ->whereRaw("(TIMESTAMPDIFF(YEAR, `birthdate`, NOW()) <=".$age[1].")")
                        ->count(); 
        }
        else {
            $disease_count = FacilityPatientUser::sex($gender)->agerange($age)->hasdiagnosis($disease)->hasicd10($disease_code)->encounter($month, $year)->orderBy('created_at')->get();
        }
        if($disease_count==NULL) { $disease_count = 0; }
        return $disease_count;
    }

    public static function getmortalityCount($month=NULL,$year,$barangay) {
        $data = array();
        $data['TOTDEATH_M'] = $data['TOTDEATH_F'] = $data['INFDEATH_M'] = $data['INFDEATH_F'] = $data['MATDEATH_F'] = $data['NEOTET_M'] = $data['NEOTET_F'] = $data['PRENATDEATH_M'] = $data['PRENATDEATH_F'] = $data['DEATHUND5_M'] = $data['DEATHUND5_F'] = $data['FD_M'] = $data['FD_F'] = $data['NEON_M'] = $data['NEON_F'] = $data['FinalReports'] = 0;

        $facility_id = Session::get('facility_details')->facility_id;
        $facilityusers = FacilityUser::whereHas('facilities',function($query) use ($facility_id) { $query->where('facility_id', $facility_id); })->lists('facilityuser_id'); 
        $list_patient_id = FacilityPatientUser::whereIn('facilityuser_id',$facilityusers)->whereHas('patients', function($query) use ($barangay) { $query->whereHas('patientContact',function($query) use ($barangay) { $query->whereIn('barangay', (array)$barangay); }); })->lists('patient_id');

        //total number of male and female deaths.
        $TOTDEATH = Patients::whereHas('patientDeathInfo',function($query) use ($year) { $query->whereYear('datetime_death','=',$year); })
                    ->whereIn('patient_id',$list_patient_id)->select(DB::raw('count(*) as count, gender'))->groupBy('gender')->get()->toArray();
        if($TOTDEATH) {
            foreach ($TOTDEATH as $TOTDEATHkey => $TOTDEATHvalue) {
                if($TOTDEATHvalue['gender']=='M') { $data['TOTDEATH_M'] = $TOTDEATHvalue['count']; }
                if($TOTDEATHvalue['gender']=='F') { $data['TOTDEATH_F'] = $TOTDEATHvalue['count']; }
            }
        }
        // DB::connection()->enableQueryLog();
        //total number of infant deaths (less than 1 year).
        $INFDEATH = Patients::whereHas('patientDeathInfo',function($query) use ($year) {        $query->whereYear('datetime_death','=',$year);
                    $query->whereRaw("(TIMESTAMPDIFF(YEAR, DATE(birthdate), DATE(datetime_death)) < 1)");
                })
                    ->whereIn('patient_id',$list_patient_id)->select(DB::raw('count(*) as count, gender'))
                    ->groupBy('gender')->get()->toArray();
        if($INFDEATH) {
            foreach ($INFDEATH as $INFDEATHkey => $INFDEATHvalue) {
                if($INFDEATHvalue['gender']=='M') { $data['INFDEATH_M'] = $INFDEATHvalue['count']; }
                if($INFDEATHvalue['gender']=='F') { $data['INFDEATH_F'] = $INFDEATHvalue['count']; }
            }
        }
        // dd(DB::getQueryLog(),$INFDEATH);
        //total number of pregnant women who died due to causes related to pregnancy, childbirth and puerperium.
        $MATDEATH = Patients::whereHas('patientDeathInfo',function($query) use ($year) { $query->whereYear('datetime_death','=',$year); $query->where('Type_of_Death','=','M'); $query->where('Immediate_Cause_of_Death','like','%pregnant%'); $query->where('Immediate_Cause_of_Death','like','%pregnancy%'); $query->where('Immediate_Cause_of_Death','like','%child birth%'); $query->where('Immediate_Cause_of_Death','like','%delivery%');$query->where('Immediate_Cause_of_Death','like','%labor%'); $query->where('mStageDeath','=',02);})
                    ->whereIn('patient_id',$list_patient_id)
                    ->select(DB::raw('count(*) as count, gender'))->groupBy('gender')->get()->toArray();
        if($MATDEATH) {
            foreach ($MATDEATH as $MATDEATHkey => $MATDEATHvalue) {
                if($MATDEATHvalue['gender']=='F') { $data['MATDEATH_F'] = $MATDEATHvalue['count']; }
            }
        }

        // Deaths due to Neonatal Tetanus – total number of deaths 3 to 28 days of age due to tetanus neonatorum.
        $NEOTET = Patients::whereHas('patientDeathInfo',function($query) use ($year) { $query->whereYear('datetime_death','=',$year); $query->where('Immediate_Cause_of_Death','like','%neonatal tetanus%')->orWhere('Immediate_Cause_of_Death','like','%tetanus%');
            $query->whereRaw("(DATEDIFF(DATE(datetime_death),DATE(birthdate)) <= 28) AND (DATEDIFF(DATE(datetime_death),DATE(birthdate)) >= 3)");})
                    ->whereIn('patient_id',$list_patient_id)->select(DB::raw('count(*) as count, gender'))
                    ->groupBy('gender')->get()->toArray();
        if($NEOTET) {
            foreach ($NEOTET as $NEOTETkey => $NEOTETvalue) {
                if($NEOTETvalue['gender']=='M') { $data['NEOTET_M'] = $NEOTETvalue['count']; }
                if($NEOTETvalue['gender']=='F') { $data['NEOTET_F'] = $NEOTETvalue['count']; }
            }
        }

        // Perinatal Deaths – total number of fetus who died from 22nd week of gestation (the time when birth weight is normally 500mg) and ends 7 completed days after birth.
        // 
        $PRENATDEATH = Patients::whereHas('patientDeathInfo',function($query) use ($year) { $query->whereYear('datetime_death','=',$year); 
            $query->whereRaw("DATEDIFF(DATE(datetime_death),DATE(birthdate)) <= 7");})
                    ->whereIn('patient_id',$list_patient_id)->select(DB::raw('count(*) as count, gender'))->groupBy('gender')->get()->toArray();
        if($PRENATDEATH) {
            foreach ($PRENATDEATH as $PRENATDEATHkey => $PRENATDEATHvalue) {
                if($PRENATDEATHvalue['gender']=='M') { $data['PRENATDEATH_M'] = $PRENATDEATHvalue['count']; }
                if($PRENATDEATHvalue['gender']=='F') { $data['PRENATDEATH_F'] = $PRENATDEATHvalue['count']; }
            }
        }
        // dd(DB::getQueryLog());
        // Under Five Mortality – total number of deaths among children under five years of age.
        $DEATHUND5 = Patients::whereHas('patientDeathInfo',function($query) use ($year) { $query->whereYear('datetime_death','=',$year); $query->whereRaw("(TIMESTAMPDIFF(YEAR, DATE(birthdate), DATE(datetime_death)) <= 5) AND (DATEDIFF(DATE(datetime_death),DATE(birthdate)) >= 1)");})
                    ->whereIn('patient_id',$list_patient_id)->select(DB::raw('count(*) as count, gender'))->groupBy('gender')->get()->toArray();
        if($DEATHUND5) {
            foreach ($DEATHUND5 as $DEATHUND5key => $DEATHUND5value) {
                if($DEATHUND5value['gender']=='M') { $data['DEATHUND5_M'] = $DEATHUND5value['count']; }
                if($DEATHUND5value['gender']=='F') { $data['DEATHUND5_F'] = $DEATHUND5value['count']; }
            }
        }
        
        // Fetal Death - later in pregnancy (at 20 weeks of gestation or more, or 28 weeks or more, for example) are also sometimes referred to as stillbirths.
        $FD = Patients::whereHas('patientDeathInfo',function($query) use ($year) { $query->whereYear('datetime_death','=',$year); $query->where('Immediate_Cause_of_Death','like','%fetal%'); $query->where('Immediate_Cause_of_Death','like','%fetus%'); $query->whereRaw("(DATEDIFF(DATE(datetime_death),DATE(birthdate)) < 1)");})
                    ->whereIn('patient_id',$list_patient_id)
                    ->select(DB::raw('count(*) as count, gender'))->groupBy('gender')->get()->toArray();
        if($FD) {
            foreach ($FD as $FDkey => $FDvalue) {
                if($FDvalue['gender']=='M') { $data['FD_M'] = $FDvalue['count']; }
                if($FDvalue['gender']=='F') { $data['FD_F'] = $FDvalue['count']; }
            }
        } 

        // Neonatal Death – total number of deaths 3 to 28 days of age
        $NEON = Patients::whereHas('patientDeathInfo',function($query) use ($year) { $query->whereYear('datetime_death','=',$year); $query->orWhere('Type_of_Death','=','N'); $query->whereRaw("(DATEDIFF(DATE(datetime_death),DATE(birthdate)) <= 28) AND (DATEDIFF(DATE(datetime_death),DATE(birthdate))>= 3)");})
                    ->whereIn('patient_id',$list_patient_id)
                    ->select(DB::raw('count(*) as count, gender'))->groupBy('gender')->get()->toArray();
        if($NEON) {
            foreach ($NEON as $NEONkey => $NEONvalue) {
                if($NEONvalue['gender']=='M') { $data['NEON_M'] = $NEONvalue['count']; }
                if($NEONvalue['gender']=='F') { $data['NEON_F'] = $NEONvalue['count']; }
            }
        } 
        return $data;
    }

    public function getDiagnosis($from, $to)
    {
        $maxdate = date('Y-m-d H:i:s');
        $xdate = strtotime($maxdate .' -12 months');
        $mindate = date('Y-m-d H:i:s', $xdate);

        if ($from == NULL):
            $from = $mindate;
            $to = $maxdate;
        else:
            $from = $from;
            $to = $to;
        endif;

        $facilityInfo = Session::get('user_details');
        $facility = FacilityHelper::facilityInfo();

        $diag['diagnosis'] = DB::table('diagnosis_view')
                ->select('diagnosislist_id', DB::raw('count(*) as bilang'))
                ->where('facility_id', $facility->facility_id)
                ->where('hccreated', '<=', $to)
                ->where('hccreated', '>', $from)
                ->groupBy('diagnosislist_id')
                ->orderBy('bilang', 'desc')
                ->orderBy('hccreated', 'desc')
                ->take(12)
                ->get();

        $diag['total'] = DB::table('healthcare_view')
            ->where('facility_id', $facility->facility_id)
            ->where('deleted_at', NULL)
            ->count();

        return $diag;

    }
}
