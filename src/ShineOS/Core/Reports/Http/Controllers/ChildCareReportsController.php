<?php
namespace ShineOS\Core\Reports\Http\Controllers;

use Illuminate\Routing\Controller;

use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientDeathInfo;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Healthcareservices\Entities\Diagnosis;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;
use ShineOS\Core\Facilities\Entities\FacilityUser;

use ShineOS\Core\Referrals\Entities\Referrals;
use ShineOS\Core\Reports\Entities\Reports;
use ShineOS\Core\Reports\Entities\M1;
use ShineOS\Core\Reports\Entities\M2;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\Utils;

use View,Response,Validator,Input,Mail,Session,Cache,Redirect,Hash,Auth,DateTime,DB;

class ChildCareReportsController extends Controller {

    protected $moduleName = 'Reports';
    protected $modulePath = 'Reports';
    protected $viewPath = 'reports::pages.';
    protected $fhsisPath = 'reports::pages.fhsis_reports.m1';

    public function __construct()
    {
    }

    public function index()
    {
    }

    public static function recommendedImmunization($type, $month = NULL, $year = NULL, $barangay = NULL)
    {
        $facility = Session::get('facility_details');
        $facility_id = $facility->facility_id;
        $month = implode("','",(array)$month);
        $emptyArr = ['IMM_BCG' => 0,
                    'IMM_DPT1' => 0,
                    'IMM_DPT2' => 0,
                    'IMM_DPT3' => 0,
                    'IMM_OPV1' => 0,
                    'IMM_OPV2' => 0,
                    'IMM_OPV3' => 0,
                    'IMM_HEPAB1WIN24' => 0,
                    'IMM_HEPAB1AFT24' => 0,
                    'IMM_HEPAB2' => 0,
                    'IMM_HEPAB3' => 0,
                    'IMM_MEAS' => 0,
                    'IMM_PENTA1' => 0,
                    'IMM_PENTA2' => 0,
                    'IMM_PENTA3' => 0,
                    'IMM_MCV1' => 0,
                    'IMM_MCV2'=> 0,
                    'IMM_ROTA1' => 0,
                    'IMM_ROTA2' => 0,
                    'IMM_ROTA3' => 0,
                    'IMM_PCV1' => 0,
                    'IMM_PCV2' => 0,
                    'IMM_PCV3' => 0,
                    'IMM_FULLY_IMM' => 0,
                    'IMM_COMP_IMM' => 0,
                    'CPB' => 0,
                    'CHECKED_6MONTH' => 0,
                    'INF_BREAST' => 0,
                    'INF_NEWBS' => 0,
                    'INF_VITA611' => 0,
                    'INF_VITA1259' => 0,
                    'INF_VITA6071' => 0,
                    'INF_VITA1223' => 0,
                    'INF_VITA2435' => 0,
                    'INF_VITA3647' => 0,
                    'INF_VITA4859' => 0,
                    'INF_25FE' => 0,
                    'INF_611FE' => 0,
                    'INF_1223FE' => 0,
                    'INF_2435FE' => 0,
                    'INF_3647FE' => 0,
                    'INF_4859FE' => 0,
                    'INF26FE' => 0,
                    'INF26LBWNS' => 0,
                    'INF25FE' => 0,
                    'INF1223FE' => 0,
                    'INF2435FE' => 0,
                    'INF3647FE' => 0,
                    'INF4859FE' => 0,
                    'INF611FE' => 0];

        $file = plugins_path() . 'Pediatrics' . DS . 'PediatricsModel.php';
        if(file_exists($file))
        {
            $location = 'Plugins\Pediatrics\PediatricsModel';
            $pediatrics = new $location;
            $recoImmun = $pediatrics::join('healthcare_services', 'healthcare_services.healthcareservice_id','=','pediatrics_service.healthcareservice_id')
                ->join('facility_patient_user', 'healthcare_services.facilitypatientuser_id','=','facility_patient_user.facilitypatientuser_id')
                ->join('facility_user', 'facility_user.facilityuser_id','=','facility_patient_user.facilityuser_id')
                ->join('patients','patients.patient_id','=','facility_patient_user.patient_id')
                ->join('patient_contact','patient_contact.patient_id','=','facility_patient_user.patient_id')
                ->select(
                    'patients.gender',

                    // Immunizations
                    DB::raw("SUM(CASE WHEN bcg_actual_date IS NOT NULL 
                                AND MONTH(DATE(bcg_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(bcg_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_BCG'"),
                    DB::raw("SUM(CASE WHEN dpt1_actual_date IS NOT NULL 
                                AND MONTH(DATE(dpt1_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(dpt1_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_DPT1'"),
                    DB::raw("SUM(CASE WHEN dpt2_actual_date IS NOT NULL 
                                AND MONTH(DATE(dpt2_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(dpt2_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_DPT2'"),
                    DB::raw("SUM(CASE WHEN dpt3_actual_date IS NOT NULL 
                                AND MONTH(DATE(dpt3_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(dpt3_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_DPT3'"),
                    DB::raw("SUM(CASE WHEN opv1_actual_date IS NOT NULL 
                                AND MONTH(DATE(opv1_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(opv1_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_OPV1'"),
                    DB::raw("SUM(CASE WHEN opv2_actual_date IS NOT NULL 
                                AND MONTH(DATE(opv2_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(opv2_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_OPV2'"),
                    DB::raw("SUM(CASE WHEN opv3_actual_date IS NOT NULL 
                                AND MONTH(DATE(opv3_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(opv3_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_OPV3'"),
                    DB::raw("SUM(CASE WHEN hepa_b1_win24_actual_date IS NOT NULL 
                                AND MONTH(DATE(hepa_b1_win24_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(hepa_b1_win24_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_HEPAB1WIN24'"),
                    DB::raw("SUM(CASE WHEN hepa_b1_aft24_actual_date IS NOT NULL 
                                AND MONTH(DATE(hepa_b1_aft24_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(hepa_b1_aft24_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_HEPAB1AFT24'"),
                    DB::raw("SUM(CASE WHEN hepa_b2_actual_date IS NOT NULL 
                                AND MONTH(DATE(hepa_b2_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(hepa_b2_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_HEPAB2'"),
                    DB::raw("SUM(CASE WHEN hepa_b3_actual_date IS NOT NULL 
                                AND MONTH(DATE(hepa_b3_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(hepa_b3_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_HEPAB3'"),
                    DB::raw("SUM(CASE WHEN measles_actual_date IS NOT NULL 
                                AND MONTH(DATE(measles_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(measles_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_MEAS'"),
                    DB::raw("SUM(CASE WHEN penta1_actual_date IS NOT NULL 
                                AND MONTH(DATE(penta1_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(penta1_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_PENTA1'"),
                    DB::raw("SUM(CASE WHEN penta2_actual_date IS NOT NULL 
                                AND MONTH(DATE(penta2_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(penta2_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_PENTA2'"),
                    DB::raw("SUM(CASE WHEN penta3_actual_date IS NOT NULL 
                                AND MONTH(DATE(penta3_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(penta3_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_PENTA3'"),
                    DB::raw("SUM(CASE WHEN mcv1_actual_date IS NOT NULL 
                                AND MONTH(DATE(mcv1_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(mcv1_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_MCV1'"),
                    DB::raw("SUM(CASE WHEN mcv2_actual_date IS NOT NULL 
                                AND MONTH(DATE(mcv2_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(mcv2_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_MCV2'"),
                    DB::raw("SUM(CASE WHEN rota1_actual_date IS NOT NULL 
                                AND MONTH(DATE(rota1_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(rota1_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_ROTA1'"),
                    DB::raw("SUM(CASE WHEN rota2_actual_date IS NOT NULL 
                                AND MONTH(DATE(rota2_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(rota2_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_ROTA2'"),
                    DB::raw("SUM(CASE WHEN rota3_actual_date IS NOT NULL 
                                AND MONTH(DATE(rota3_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(rota3_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_ROTA3'"),
                    DB::raw("SUM(CASE WHEN pcv1_actual_date IS NOT NULL 
                                AND MONTH(DATE(pcv1_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(pcv1_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_PCV1'"),
                    DB::raw("SUM(CASE WHEN pcv2_actual_date IS NOT NULL 
                                AND MONTH(DATE(pcv2_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(pcv2_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_PCV2'"),
                    DB::raw("SUM(CASE WHEN pcv3_actual_date IS NOT NULL 
                                AND MONTH(DATE(pcv3_actual_date)) IN ('".$month."') 
                                AND YEAR(DATE(pcv3_actual_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'IMM_PCV3'"),

                    // Fully immunized and completely immunized
                    DB::raw("SUM(CASE WHEN bcg_actual_date IS NOT NULL 
                                AND TIMESTAMPDIFF(YEAR,patients.birthdate,bcg_actual_date) = 0 
                                AND dpt1_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,dpt1_actual_date) = 0
                                AND dpt2_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,dpt2_actual_date) = 0
                                AND dpt3_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,dpt3_actual_date) = 0
                                AND opv1_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,opv1_actual_date) = 0
                                AND opv2_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,opv2_actual_date) = 0
                                AND opv3_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,opv3_actual_date) = 0
                                AND hepa_b1_win24_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,hepa_b1_win24_actual_date) = 0
                                AND hepa_b1_aft24_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,hepa_b1_aft24_actual_date) = 0
                                AND hepa_b2_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,hepa_b2_actual_date) = 0
                                AND hepa_b3_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,hepa_b3_actual_date) = 0
                                AND measles_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,measles_actual_date) = 0
                                AND MONTH(DATE(pediatrics_service.updated_at)) IN ('".$month."') 
                                AND YEAR(DATE(pediatrics_service.updated_at)) = ".$year."
                                THEN 1 ELSE 0 END ) AS 'IMM_FULLY_IMM'"),
                    DB::raw("SUM(CASE WHEN bcg_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,bcg_actual_date) = 1 
                                AND dpt1_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,dpt1_actual_date) = 1
                                AND dpt2_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,dpt2_actual_date) = 1
                                AND dpt3_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,dpt3_actual_date) = 1
                                AND opv1_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,opv1_actual_date) = 1
                                AND opv2_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,opv2_actual_date) = 1
                                AND opv3_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,opv3_actual_date) = 1
                                AND hepa_b1_win24_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,hepa_b1_win24_actual_date) = 1
                                AND hepa_b1_aft24_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,hepa_b1_aft24_actual_date) = 1
                                AND hepa_b2_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,hepa_b2_actual_date) = 1
                                AND hepa_b3_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,hepa_b3_actual_date) = 1
                                AND measles_actual_date IS NOT NULL AND TIMESTAMPDIFF(YEAR,patients.birthdate,measles_actual_date) = 1
                                AND MONTH(DATE(pediatrics_service.updated_at)) IN ('".$month."') 
                                AND YEAR(DATE(pediatrics_service.updated_at)) = ".$year."
                                THEN 1 ELSE 0 END) AS 'IMM_COMP_IMM'"),

                    // Child Protected at Birth
                    DB::raw("SUM(CASE WHEN child_protection_date IS NOT NULL 
                                AND (tt_status LIKE '%Yes%' OR tt_status = 1 OR tt_status LIKE '%Complete%' OR tt_status LIKE '%Done%') 
                                AND MONTH(DATE(child_protection_date)) IN ('".$month."') 
                                AND YEAR(DATE(child_protection_date)) = ".$year." 
                                THEN 1 ELSE 0 END) AS 'CPB'"),

                    // Checked 6 months
                    DB::raw("SUM(CASE WHEN TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) = 6 
                                AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."') 
                                AND YEAR(healthcare_services.encounter_datetime) = '".$year."' 
                                THEN 1 ELSE 0 END) AS 'CHECKED_6MONTH'"),

                    // Breasfted for 6 months
                    DB::raw("SUM(CASE WHEN is_breastfed_first_month = 1 
                                AND is_breastfed_second_month = 1 
                                AND is_breastfed_third_month = 1
                                AND is_breastfed_fourth_month = 1 
                                AND is_breastfed_fifth_month = 1 
                                AND is_breastfed_sixth_month = 1 
                                AND MONTH(DATE(breastfeed_sixth_month)) IN ('".$month."') 
                                AND YEAR(DATE(breastfeed_sixth_month)) = '".$year."' 
                                THEN 1 ELSE 0 END) AS 'INF_BREAST'"),

                    DB::raw("SUM(CASE WHEN newborn_screening_referral_date IS NOT NULL
                                AND MONTH(newborn_screening_referral_date) IN ('".$month."') 
                                AND YEAR(newborn_screening_referral_date) = '".$year."' 
                                THEN 1 ELSE 0 END) AS 'INF_NEWBS'"),

                    // Given vitamin A supplements
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,vit_a_supp_first_date) BETWEEN 6 AND 11 
                                AND MONTH(DATE(vit_a_supp_first_date)) IN ('".$month."') 
                                AND YEAR(DATE(vit_a_supp_first_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF_VITA611'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,vit_a_supp_first_date) BETWEEN 12 AND 59 
                                AND MONTH(DATE(vit_a_supp_first_date)) IN ('".$month."') 
                                AND YEAR(DATE(vit_a_supp_first_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF_VITA1259'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,vit_a_supp_first_date) BETWEEN 60 AND 71 
                                AND MONTH(DATE(vit_a_supp_first_date)) IN ('".$month."') 
                                AND YEAR(DATE(vit_a_supp_first_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF_VITA6071'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,vit_a_supp_first_date) BETWEEN 12 AND 23 
                                AND MONTH(DATE(vit_a_supp_first_date)) IN ('".$month."') 
                                AND YEAR(DATE(vit_a_supp_first_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF_VITA1223'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,vit_a_supp_first_date) BETWEEN 24 AND 35 
                                AND MONTH(DATE(vit_a_supp_first_date)) IN ('".$month."') 
                                AND YEAR(DATE(vit_a_supp_first_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF_VITA2435'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,vit_a_supp_first_date) BETWEEN 36 AND 47 
                                AND MONTH(DATE(vit_a_supp_first_date)) IN ('".$month."') 
                                AND YEAR(DATE(vit_a_supp_first_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF_VITA3647'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,vit_a_supp_first_date) BETWEEN 48 AND 59 
                                AND MONTH(DATE(vit_a_supp_first_date)) IN ('".$month."') 
                                AND YEAR(DATE(vit_a_supp_first_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF_VITA4859'"),

                    // Given iron supplements
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,iron_supp_start_date) BETWEEN 2 AND 5 
                                AND MONTH(DATE(iron_supp_start_date)) IN ('".$month."') 
                                AND YEAR(DATE(iron_supp_start_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF25FE'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,iron_supp_start_date) BETWEEN 6 AND 11 
                                AND MONTH(DATE(iron_supp_start_date)) IN ('".$month."') 
                                AND YEAR(DATE(iron_supp_start_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF611FE'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,iron_supp_start_date) BETWEEN 12 AND 23 
                                AND MONTH(DATE(iron_supp_start_date)) IN ('".$month."') 
                                AND YEAR(DATE(iron_supp_start_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF1223FE'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,iron_supp_start_date) BETWEEN 24 AND 35 
                                AND MONTH(DATE(iron_supp_start_date)) IN ('".$month."') 
                                AND YEAR(DATE(iron_supp_start_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF2435FE'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,iron_supp_start_date) BETWEEN 36 AND 47 
                                AND MONTH(DATE(iron_supp_start_date)) IN ('".$month."') 
                                AND YEAR(DATE(iron_supp_start_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF3647FE'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,iron_supp_start_date) BETWEEN 48 AND 59 
                                AND MONTH(DATE(iron_supp_start_date)) IN ('".$month."') 
                                AND YEAR(DATE(iron_supp_start_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF4859FE'"),

                    // Low birthweight and lowbirthweight + given iron
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 2 AND 6 
                                AND birth_weight < 2.5
                                AND MONTH(DATE(healthcare_services.encounter_datetime)) IN ('".$month."') 
                                AND YEAR(DATE(healthcare_services.encounter_datetime)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF26LBWNS'"),
                    DB::raw("SUM(CASE WHEN (TIMESTAMPDIFF(MONTH,patients.birthdate,iron_supp_start_date) BETWEEN 2 AND 6 
                                AND birth_weight < 2.5
                                AND MONTH(DATE(iron_supp_start_date)) IN ('".$month."') 
                                AND YEAR(DATE(iron_supp_start_date)) = '".$year."') 
                                THEN 1 ELSE 0 END) AS 'INF26FE'")
                    )
                ->whereIn('patient_contact.barangay', (array)$barangay)
                ->where('facility_user.facility_id','=',$facility_id)
                ->groupBy('gender')
                ->get()
                ->toArray();

            $return = array();
            
            if($recoImmun){
                foreach($recoImmun as $key => $value) {
                    $temp = $value;
                    unset($temp['gender']);
                    switch ($value['gender']) {
                        case 'M': $return['M'] = $temp; break;
                        case 'F': $return['F'] = $temp; break;
                    }
                }
            }

            $genders = array('M','F');
            foreach($genders as $gender) {
                if(!isset($return[$gender]))
                {
                    $return[$gender] = $emptyArr;
                }
            }
            return $return;
        }
        return NULL;        
    }

    public static function diagnosisBased($type, $month = NULL, $year = NULL, $barangay = NULL)
    {
        $facility = Session::get('facility_details');
        $facility_id = $facility->facility_id;
        $month = implode("','",(array)$month);
        $emptyArr = ['SICK_611' => 0,
                     'SICK_1259' => 0,
                     'SICK_6071' => 0,
                     'SICKVITA_611' => 0,
                     'SICKVITA_1259' => 0,
                     'SICKVITA_6071' => 0,
                     'ANECHILDNS' => 0,
                     'ANECHILDFE' => 0,
                     'ANECHILD611' => 0,
                     'ANECHILD611FE' => 0,
                     'ANECHILD1259' => 0,
                     'ANECHILD1259FE' => 0,
                     'DIARRNC' => 0,
                     'DIARRORS' => 0,
                     'DIARRORSZ' => 0,
                     'PNEUNC' => 0,
                     'PNEUGT' => 0];

        $diagnosisBased = Healthcareservices::join('diagnosis','diagnosis.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                                            ->leftJoin('medicalorder','medicalorder.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                                            ->leftJoin('medicalorder_prescription','medicalorder_prescription.medicalorder_id','=','medicalorder.medicalorder_id')
                                            ->leftJoin('pediatrics_service','pediatrics_service.healthcareservice_id','=','healthcare_services.healthcareservice_id')
                                            ->join('facility_patient_user', 'healthcare_services.facilitypatientuser_id','=','facility_patient_user.facilitypatientuser_id')
                                            ->join('facility_user', 'facility_user.facilityuser_id','=','facility_patient_user.facilityuser_id')
                                            ->join('patients','patients.patient_id','=','facility_patient_user.patient_id')
                                            ->join('patient_contact','patient_contact.patient_id','=','facility_patient_user.patient_id')
                                            ->select(
                                                'patients.gender',

                                                // Sick Children
                                                DB::raw("SUM(CASE WHEN (diagnosis.diagnosislist_id LIKE '%Pneumonia%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Diarrhea%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Measles%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Under Nutrition%'
                                                            OR (diagnosis.diagnosislist_id LIKE '%Xerophthalmia%' 
                                                                OR diagnosis.diagnosislist_id LIKE '%Xerophthalmic%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Night blindness%'
                                                                OR (diagnosis.diagnosislist_id LIKE '%Bitot%' AND diagnosis.diagnosislist_id LIKE '%spots%')
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal xerosis%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal Ulcerations%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Keratomalacia%'))
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 6 AND 11
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'SICK_611'"),
                                                DB::raw("SUM(CASE WHEN (diagnosis.diagnosislist_id LIKE '%Pneumonia%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Diarrhea%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Measles%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Under Nutrition%'
                                                            OR (diagnosis.diagnosislist_id LIKE '%Xerophthalmia%' 
                                                                OR diagnosis.diagnosislist_id LIKE '%Xerophthalmic%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Night blindness%'
                                                                OR (diagnosis.diagnosislist_id LIKE '%Bitot%' AND diagnosis.diagnosislist_id LIKE '%spots%')
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal xerosis%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal Ulcerations%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Keratomalacia%')) 
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 12 AND 59
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'SICK_1259'"),                                                
                                                DB::raw("SUM(CASE WHEN (diagnosis.diagnosislist_id LIKE '%Pneumonia%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Diarrhea%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Measles%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Under Nutrition%'
                                                            OR (diagnosis.diagnosislist_id LIKE '%Xerophthalmia%' 
                                                                OR diagnosis.diagnosislist_id LIKE '%Xerophthalmic%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Night blindness%'
                                                                OR (diagnosis.diagnosislist_id LIKE '%Bitot%' AND diagnosis.diagnosislist_id LIKE '%spots%')
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal xerosis%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal Ulcerations%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Keratomalacia%')) 
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 60 AND 71
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'SICK_6071'"),

                                                // Sick Children Received Vitamin A
                                                DB::raw("SUM(CASE WHEN (diagnosis.diagnosislist_id LIKE '%Pneumonia%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Diarrhea%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Measles%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Under Nutrition%'
                                                            OR (diagnosis.diagnosislist_id LIKE '%Xerophthalmia%' 
                                                                OR diagnosis.diagnosislist_id LIKE '%Xerophthalmic%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Night blindness%'
                                                                OR (diagnosis.diagnosislist_id LIKE '%Bitot%' AND diagnosis.diagnosislist_id LIKE '%spots%')
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal xerosis%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal Ulcerations%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Keratomalacia%')) 
                                                            AND (TIMESTAMPDIFF(MONTH,patients.birthdate,vit_a_supp_first_date) BETWEEN 6 AND 11 
                                                            AND MONTH(DATE(vit_a_supp_first_date)) IN ('".$month."') 
                                                            AND YEAR(DATE(vit_a_supp_first_date)) = '".$year."')
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 6 AND 11
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'SICKVITA_611'"),
                                                DB::raw("SUM(CASE WHEN (diagnosis.diagnosislist_id LIKE '%Pneumonia%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Diarrhea%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Measles%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Under Nutrition%'
                                                            OR (diagnosis.diagnosislist_id LIKE '%Xerophthalmia%' 
                                                                OR diagnosis.diagnosislist_id LIKE '%Xerophthalmic%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Night blindness%'
                                                                OR (diagnosis.diagnosislist_id LIKE '%Bitot%' AND diagnosis.diagnosislist_id LIKE '%spots%')
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal xerosis%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal Ulcerations%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Keratomalacia%'))
                                                            AND (TIMESTAMPDIFF(MONTH,patients.birthdate,vit_a_supp_first_date) BETWEEN 12 AND 59 
                                                            AND MONTH(DATE(vit_a_supp_first_date)) IN ('".$month."') 
                                                            AND YEAR(DATE(vit_a_supp_first_date)) = '".$year."') 
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 12 AND 59
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'SICKVITA_1259'"),                                                
                                                DB::raw("SUM(CASE WHEN diagnosis.diagnosislist_id LIKE '%Pneumonia%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Diarrhea%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Measles%'
                                                            OR diagnosis.diagnosislist_id LIKE '%Under Nutrition%'
                                                            OR (diagnosis.diagnosislist_id LIKE '%Xerophthalmia%' 
                                                                OR diagnosis.diagnosislist_id LIKE '%Xerophthalmic%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Night blindness%'
                                                                OR (diagnosis.diagnosislist_id LIKE '%Bitot%' AND diagnosis.diagnosislist_id LIKE '%spots%')
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal xerosis%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Corneal Ulcerations%'
                                                                OR diagnosis.diagnosislist_id LIKE '%Keratomalacia%')
                                                            AND (TIMESTAMPDIFF(MONTH,patients.birthdate,vit_a_supp_first_date) BETWEEN 60 AND 71 
                                                            AND MONTH(DATE(vit_a_supp_first_date)) IN ('".$month."') 
                                                            AND YEAR(DATE(vit_a_supp_first_date)) = '".$year."') 
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 60 AND 71
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'SICKVITA_6071'"),

                                                // Anemic children
                                                DB::raw("SUM(CASE WHEN (diagnosis.diagnosislist_id LIKE '%Anemia%' OR diagnosis.diagnosislist_id LIKE '%Anemic%')
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 2 AND 59
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'ANECHILDNS'"),
                                                DB::raw("SUM(CASE WHEN (diagnosis.diagnosislist_id LIKE '%Anemia%' OR diagnosis.diagnosislist_id LIKE '%Anemic%')
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 6 AND 11
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'ANECHILD611'"),
                                                DB::raw("SUM(CASE WHEN (diagnosis.diagnosislist_id LIKE '%Anemia%' OR diagnosis.diagnosislist_id LIKE '%Anemic%')
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 12 AND 59
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'ANECHILD1259'"),

                                                // Anemic Children Received Iron
                                                DB::raw("SUM(CASE WHEN (diagnosis.diagnosislist_id LIKE '%Anemia%' OR diagnosis.diagnosislist_id LIKE '%Anemic%')
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 2 AND 59
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,iron_supp_start_date) BETWEEN 0 AND 59 
                                                            AND MONTH(DATE(iron_supp_start_date)) IN ('".$month."') 
                                                            AND YEAR(DATE(iron_supp_start_date)) = '".$year."'
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'ANECHILDFE'"),
                                                DB::raw("SUM(CASE WHEN (diagnosis.diagnosislist_id LIKE '%Anemia%' OR diagnosis.diagnosislist_id LIKE '%Anemic%')
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 6 AND 11
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,iron_supp_end_date) BETWEEN 0 AND 59 
                                                            AND MONTH(DATE(iron_supp_end_date)) IN ('".$month."') 
                                                            AND YEAR(DATE(iron_supp_end_date)) = '".$year."'
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'ANECHILD611FE'"),
                                                DB::raw("SUM(CASE WHEN (diagnosis.diagnosislist_id LIKE '%Anemia%' OR diagnosis.diagnosislist_id LIKE '%Anemic%')
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 12 AND 59
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,iron_supp_end_date) BETWEEN 0 AND 59 
                                                            AND MONTH(DATE(iron_supp_end_date)) IN ('".$month."') 
                                                            AND YEAR(DATE(iron_supp_end_date)) = '".$year."'
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'ANECHILD1259FE'"),

                                                // Children with Diarrhea
                                                DB::raw("SUM(CASE WHEN diagnosis.diagnosislist_id LIKE '%Diarrhea%'
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 0 AND 59
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'DIARRNC'"),
                                                DB::raw("SUM(CASE WHEN diagnosis.diagnosislist_id LIKE '%Diarrhea%'
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 0 AND 59
                                                            AND (medicalorder_prescription.generic_name LIKE '%Oral Rehydration Salt%' OR medicalorder_prescription.generic_name LIKE '%ORS%')
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'DIARRORS'"),
                                                DB::raw("SUM(CASE WHEN diagnosis.diagnosislist_id LIKE '%Diarrhea%'
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 0 AND 59
                                                            AND (medicalorder_prescription.generic_name LIKE '%Oral Rehydration Salt%' OR medicalorder_prescription.generic_name LIKE '%ORS%')
                                                            AND medicalorder_prescription.generic_name LIKE '%Zinc%'
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'DIARRORSZ'"),

                                                // Children with Pneumonia
                                                DB::raw("SUM(CASE WHEN diagnosis.diagnosislist_id LIKE '%Pneumonia%'
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 0 AND 59
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'PNEUNC'"),
                                                DB::raw("SUM(CASE WHEN diagnosis.diagnosislist_id LIKE '%Pneumonia%'
                                                            AND TIMESTAMPDIFF(MONTH,patients.birthdate,healthcare_services.encounter_datetime) BETWEEN 0 AND 59
                                                            AND MONTH(healthcare_services.encounter_datetime) IN ('".$month."')
                                                            AND YEAR(healthcare_services.encounter_datetime) = ".$year."
                                                            THEN 1 ELSE 0 END) AS 'PNEUGT'")

                                                )
                                            ->whereIn('patient_contact.barangay', (array)$barangay)
                                            ->where('facility_user.facility_id','=',$facility_id)
                                            ->groupBy('gender')
                                            ->get()
                                            ->toArray();

        $return = array();
        
        if($diagnosisBased){
            foreach($diagnosisBased as $key => $value) {
                $temp = $value;
                unset($temp['gender']);
                switch ($value['gender']) {
                    case 'M': $return['M'] = $temp; break;
                    case 'F': $return['F'] = $temp; break;
                }
            }
        }

        $genders = array('M','F');
        foreach($genders as $gender) {
            if(!isset($return[$gender]))
            {
                $return[$gender] = $emptyArr;
            }
        }
        return $return;       
    }
}