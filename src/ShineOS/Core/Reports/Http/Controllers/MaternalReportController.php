<?php

namespace ShineOS\Core\Reports\Http\Controllers;

use Illuminate\Routing\Controller;
use ShineOS\Core\Facilities\Entities\FacilityPatientUser;
use ShineOS\Core\Facilities\Entities\FacilityUser;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Reports\Entities\M1;
use Session,DateTime,DB;

class MaternalReportController extends Controller {

    public function __construct() {
        $this->maternalmodel = 'Plugins\MaternalCare\MaternalCareModel';
        $this->deliverymodel = 'Plugins\MaternalCare\MaternalCareDeliveryModel';

        $this->livebirth_termination_outcome = array('001','002','003','005','006','009','999');
        $this->checkNatality = array('pregnancies' => array(NULL,'F','M'),
                                    'attendant' => array(NULL,"unknown","001","002","003","004","999"),
                                    'livebirth_weight' => array(NULL,'unknown','less than 2500','greater than 2500'),
                                    'preg_outcome' => array(NULL,"unknown","001","002","003","004","005","006","007","008","009","999"),
                                    'delivered_type' => array(NULL,'unknown','N','O'),
                                    'delivery_place' => array(NULL,'unknown','bhs','rhu','hp','LBC','hme','hsp','oth'),
                                    'termination_outcome' => array(NULL,"unknown","001","002","003","004","005","006","007","008","009","999"));
        $this->checkGender = array('F','M');
    }

    public static function getData($type=NULL,$month=NULL,$year=NULL,$barangay=NULL) {
        // dd($type,$month,$year,$barangay);
        $_this = new self();
        $data = $maternal = $natality = array();
        $date = date('Y-m-d');
         // DB::enableQueryLog();
        $file_1 = plugins_path() . 'MaternalCare' . DS . 'MaternalCareModel.php';
        if(file_exists($file_1)) {
            $file_2 = plugins_path() . 'MaternalCare' . DS . 'MaternalCareDeliveryModel.php';
            if(file_exists($file_2)) {
                if(!is_null($type)) {
                    if(is_null($year)) { $year = date('Y'); }
                    if(is_null($month)) { $month = date('m'); }
                    $month = implode("','",(array)$month);

                    if(is_null($barangay)) {
                        $barangay = Session::get('facility_details')->toArray()['facility_contact']['barangay'];
                    }

                    $facility_id = Session::get('facility_details')->facility_id;

                    $facilityusers = FacilityUser::withAndWhereHas('facilities',function($query) use ($facility_id) {
                                            $query->where('facility_id', $facility_id);
                                        })->lists('facilityuser_id');
                    $facilitypatientusers = FacilityPatientUser::whereIn('facilityuser_id',$facilityusers)
                                        ->withAndWhereHas('patients',function($query) use ($barangay) {
                                        $query->withAndWhereHas('patientContact',function($query) use ($barangay) {
                                            $query->whereIn('barangay', (array)$barangay);
                                        });
                                    })
                                    ->lists('facilitypatientuser_id');

                    $healthcareservices = Healthcareservices::whereIn('facilitypatientuser_id', $facilitypatientusers)->lists('healthcareservice_id');                
                    
                    if($type=='maternal') {
                        $maternal = $_this->maternalmodel::whereRaw(DB::raw("month(`created_at`) IN ('".$month."')"))
                            ->whereYear('created_at','=',$year)
                            ->whereIn('healthcareservice_id', $healthcareservices)->with('deliveries')->with('prenatal')->with('postpartum')
                            ->get()->toArray();
                        $data = $_this->maternalcareQuery($maternal);
                    }

                    if ($type=='natality') {
                        $natality = $_this->maternalmodel::whereIn('healthcareservice_id', $healthcareservices);
                        $data = $_this->natalityQuery($natality->lists('maternalcase_id'),$month,$year);

                        //PREGNANCY COUNT REGARDLESS OF DELIVERY
                        $data['maternal_count'] = $natality->whereRaw(DB::raw("month(`created_at`) IN ('".$month."')"))
                            ->whereYear('created_at','=',$year)->count();
                    }
                }
            }
        } 
        // dd($data,$type,$month,$year,$barangay);
        return $data;
    }

    public static function maternalcareQuery($maternal) {
        // dd(DB::getQueryLog(), $maternal);
        $_this = new self();
        $date = date('Y-m-d');
        $PC1 = $PC2 = $PC3 = $PC4 = $PC5 = $PP1 = $PP2 = $PP3 = $PP4 = $W1049_VITA = $DELIV = 0;
        if($maternal) {
            foreach ($maternal as $key => $value) {
                if(count($value['prenatal'])>0) {
                    if(count($value['prenatal'][0]['visits']) >= 4) {
                        $PC1 += 1;
                    }
                    if(count($value['prenatal'][0]['immunizations']) >= 2) {
                        $PC2 += 1;
                    }
                    
                    foreach ($value['prenatal'][0]['supplements'] as $k => $v) {
                        if($v['supplement_type']==2) {
                            $PC4 += 1;
                        } else if($v['supplement_type']==1) {
                            $PC5 += 1;
                        }
                    }
                }
                
                if(count($value['postpartum'])>0) {
                    if(count($value['postpartum'][0]['visits']) >= 2) {
                        $PP1 += 1;
                    }

                    foreach ($value['postpartum'][0]['supplements'] as $a => $b) {
                        if($b['supplement_type']==2) {
                            $PP2 += 1;
                        } else if($b['supplement_type']==1) {
                            $PP3 += 1;
                        }
                    }

                    if($value['postpartum'][0]['breastfeeding_date']=$date) {
                        $PP4 += 1;
                    }
                }

                if(count($value['deliveries'])>0) {
                    $DELIV += 1;
                }
            }
        }

        $data['PC1'] = $PC1; //Pregnant women with 4 or more Prenatal visits
        $data['PC2'] = $PC2; //Pregnant women givin 2 doses of Tetanus Toxiod
        $data['PC3'] = $PC3; //Pregnant women given TT2 plus
        $data['PC4'] = $PC4; //Pregnant women given complete iron with folic acid supplementation
        $data['PC5'] = $PC5; //Pregnant women given Vitamin A supplementation
        $data['PP1'] = $PP1; //Postpartum women given at least 2 postpartum visits
        $data['PP2'] = $PP2; //Postpartum women given complete iron supplementation
        $data['PP3'] = $PP3; //Postpartum women given Vitamin A supplementation
        $data['PP4'] = $PP4; //Postpartum women initiated breastfeeding w/in 1 hour after delivery
        $data['W1049_VITA'] = $W1049_VITA; //Postpartum women initiated breastfeeding w/in 1 hour after delivery
        $data['DELIV'] = $DELIV; //Number of deliveries

        return $data;
    }

    public static function natalityQuery($natality,$month,$year) {
        $_this = new self();
        $result = array();
        $maternaldeliverymodel = new $_this->deliverymodel;

        //PREGNANCY COUNT WITH DELIVERY AND GENDER WHERE LIVEBIRTH
        $query_1 = $maternaldeliverymodel::whereIn('maternalcase_id', $natality)->select(DB::raw('count(*) as count,child_gender'))->whereIn('termination_outcome',$_this->livebirth_termination_outcome)->whereYear('termination_datetime','=',$year)->whereRaw(DB::raw("month(`termination_datetime`) IN ('".$month."')"))->groupBy('child_gender')->get()->toArray();
        
        //PREGNANCY COUNT WITH DELIVERY BY GENDER AND ATTENDANT WHERE LIVEBIRTH
        $query_2 = $maternaldeliverymodel::whereIn('maternalcase_id', $natality)->select(DB::raw('count(*) as count, child_gender, attendant'))->whereIn('termination_outcome',$_this->livebirth_termination_outcome)->whereYear('termination_datetime','=',$year)->whereRaw(DB::raw("month(`termination_datetime`) IN ('".$month."')"))->groupBy('child_gender','attendant')->get()->toArray();
        
        //PREGNANCY COUNT WITH DELIVERY BY GENDER AND LIVE BIRTH WHERE LIVEBIRTH
        $query_3 = $maternaldeliverymodel::whereIn('maternalcase_id', $natality)->select(DB::raw("count(*) as count, child_gender, (CASE WHEN `livebirth_weight` >= 2500 THEN 'greater than 2500' WHEN `livebirth_weight` < 2500 THEN 'less than 2500' WHEN `livebirth_weight` IS NULL THEN 'unknown' ELSE 'unknown' END) as birthweight"))->whereIn('termination_outcome',$_this->livebirth_termination_outcome)->whereYear('termination_datetime','=',$year)->whereRaw(DB::raw("month(`termination_datetime`) IN ('".$month."')"))->groupBy("child_gender","birthweight")->get()->toArray();

        //PREGNANCY COUNT WITH DELIVERY BY GENDER AND PREGNANCY OUTCOME
        DB::connection()->enableQueryLog();
        $query_4 = $maternaldeliverymodel::whereIn('maternalcase_id', $natality)->select(DB::raw('count(*) as count, child_gender, termination_outcome'))->whereYear('termination_datetime','=',$year)->whereRaw(DB::raw("month(`termination_datetime`) IN ('".$month."')"))->groupBy('child_gender','termination_outcome')->get()->toArray();
       // dd(DB::getQueryLog());
        // PREGNANCY COUNT WITH DELIVERY BY GENDER AND DELIVERY TYPE
        $query_5 = $maternaldeliverymodel::whereIn('maternalcase_id', $natality)->select(DB::raw('count(*) as count, delivered_type, child_gender'))->whereYear('termination_datetime','=',$year)->whereRaw(DB::raw("month(`termination_datetime`) IN ('".$month."')"))->groupBy('delivered_type','child_gender')->get()->toArray();

        //PREGNANCY COUNT WITH DELIVERY BY GENDER, DELIVERY TYPE AND DELIVERY PLACE
        $query_6 = $maternaldeliverymodel::whereIn('maternalcase_id', $natality)->select(DB::raw('count(*) as count, delivered_type, delivery_place, child_gender'))->whereYear('termination_datetime','=',$year)->whereRaw(DB::raw("month(`termination_datetime`) IN ('".$month."')"))->groupBy('delivered_type','delivery_place','child_gender')->get()->toArray();

        // dd($query_1,$query_2,$query_3,$query_4,$query_5,$query_6,$month,$year);
        foreach ($query_1 as $k_1 => $v_1) {
            if($v_1['child_gender']!=NULL) {
                $result['pregnancies'][$v_1['child_gender']] = $v_1['count'];
            }
        }

        foreach ($query_2 as $k_2 => $v_2) {
            if($v_2['child_gender']!=NULL) {
                $result['attendant'][$v_2['child_gender']][$v_2['attendant']] = $v_2['count'];
            }
        }

        foreach ($query_3 as $k_3 => $v_3) {
            if($v_3['child_gender']!=NULL) {
                $result['livebirth_weight'][$v_3['child_gender']][$v_3['birthweight']] = $v_3['count'];
            }
        }
        
        foreach ($query_4 as $k_4 => $v_4) {
            if($v_4['child_gender']!=NULL) {
                $result['preg_outcome'][$v_4['child_gender']][$v_4['termination_outcome']] = $v_4['count'];
            }
        }

        foreach ($query_5 as $k_5 => $v_5) {
            if($v_5['child_gender']!=NULL) {
                $result['delivered_type'][$v_5['child_gender']][$v_5['delivered_type']] = $v_5['count'];
            }
        }

        foreach ($query_6 as $k_6 => $v_6) {
            if($v_6['child_gender']!=NULL) {
                $result['delivery_place'][$v_6['child_gender']][$v_6['delivered_type']][$v_6['delivery_place']] = $v_6['count'];
            }
        }

        //IF BLANK
        $blank = array_diff(array_keys($_this->checkNatality),array_keys($result));
        foreach ($blank as $key => $value) {
            if($value!='maternal_count') {
                $result[$value] = array();
            } else {
                $result[$value] = 0;
            }
        }
        //GENDER CHECKING, INSERT KEY TO ARRAY IF NOT EXISTS
        foreach ($result as $key => $value) {
            if($key!='maternal_count') {
                $array_diff_key = array_diff($_this->checkGender, array_keys($value));
                foreach ($array_diff_key as $k => $v) {
                    if($key=='pregnancies') {
                        $result[$key][$v] = 0;
                    } else {
                        $result[$key][$v] = array();
                    }
                }
            }
        }
        //DATA CHECKING, INSERT KEY TO ARRAY IF NOT EXISTS
        foreach ($result as $key_2 => $value_2) {
            if($key_2!='maternal_count' AND $key_2!='pregnancies') {
                if(array_key_exists($key_2, $_this->checkNatality)) {
                    foreach ($_this->checkGender as $k_gender => $v_gender) {
                        if($key_2=='delivery_place') {
                            $array_diff_key_2 = array_diff($_this->checkNatality['delivered_type'], array_keys($value_2[$v_gender]));
                            foreach ($array_diff_key_2 as $diffkey_2 => $diffvalue_2) {
                                $result[$key_2][$v_gender][$diffvalue_2] = array();
                            }
                            foreach ($result[$key_2][$v_gender] as $k_dt => $v_dt) {
                                $array_diff_key_3 = array_diff($_this->checkNatality[$key_2], array_keys($v_dt));
                                foreach ($array_diff_key_3 as $diffkey_3 => $diffvalue_3) {
                                    $result[$key_2][$v_gender][$k_dt][$diffvalue_3] = 0;
                                }
                            }
                        } else {
                            $array_diff_key_2 = array_diff($_this->checkNatality[$key_2], array_keys($value_2[$v_gender]));
                            foreach ($array_diff_key_2 as $diffkey_2 => $diffvalue_2) {
                                $result[$key_2][$v_gender][$diffvalue_2] = 0;
                            }
                        }
                    }
                }
            }
        }
           // dd($result,$month,$year);
        return $result;
    }
}
