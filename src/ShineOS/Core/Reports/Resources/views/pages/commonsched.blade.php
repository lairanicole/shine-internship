<script src="node_modules/d3/dist/d3.js"></script>

<?php
    $days = new SplFixedArray(5);
    $days[0] = "M";
    $days[1] = "T";
    $days[2] = "W";
    $days[3] = "TH";
    $days[4] = "F";
    $timeslots = new SplFixedArray(29);
    $x = 700;
    $f = true;
    for($i=0;$i<29;$i++){
        if($f){
            $y = $x + 30;
        }
        else{
            $y = $x + 70;
        }
        $timeslots[$i] = $x . "-" . $y;
        $x = $y;
        $f = !$f;
    }
    $vals = $values;
?>

<div class="container">
    <div class="row">
        <div class="col-sm-1"></div>
        @foreach($days as $day)
            <div class="col-sm-1" style="text-align: center">{{ $day }}</div>
        @endforeach
    </div>
    <?php $cnt = 0; ?>
    @foreach ($timeslots as $slot)
        <div class="row">
            <div class="col-sm-1" style="text-align: right">{{ $slot }}</div>
            @foreach ($days as $day)
                <div class="col-sm-1">
                    <?php $split_string = explode("-", $slot); $day_time = $day . $split_string[0]; ?>
                    <div class="border margin" id={{ $day . $split_string[0] }} freq={{ $val["sum(`" . $day . $split_string[0] . "`)"] }} data-toggle="tooltip" data-placement="top" title={{ $day . $split_string[0] }}>&nbsp;</div>
                    <?php $cnt++; ?>
                </div>
            @endforeach
        </div>
    @endforeach
</div>

<script type="text/javascript">
    var colors = d3.extent(<?php echo json_encode($values); ?>);
    var colorScale = d3.scaleLinear()
        .domain(colors)
        .range(["lightblue", "darkblue"]);

    var rectangles = d3.selectAll(".border");

    rectangles
        .style("background-color", function(){
            console.log(d3.select(this));
            return colorScale(d3.select(this).attr("freq"));
        });

        
</script>