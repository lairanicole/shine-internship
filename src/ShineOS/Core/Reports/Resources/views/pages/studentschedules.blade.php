<script src="{{ asset('public/dist/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

<div class="row">
    <div class="col-md-12">
        <h4>Student Schedules</h4>
        <p class="box-content-para">See common schedules among students of the same year and course.</p>
    </div>
    <div class="col-md-4">
        <select class="form-control" id="course">
            <option disabled selected>Select Course ID</option>
            @foreach($courses[0] as $course)
                <option value={{ $course }}>{{ $course }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-2">
        <select class="form-control" id="year">
            <option disabled selected>Select Year Level</option>
            @foreach($yearLevels as $year)
                <option value={{ $year }}>{{ $year }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-2">
        <p>
            <button id="schedules" class="btn btn-primary form-control" data-url="{{ route('studentschedules.commonsched') }}">Find Common Schedules</button>
        </p>
    </div>
    <div class="col-md-12 modal-body" id="availability">
    </div> 
</div>

<script type="text/javascript">
    var selectedCourse = null;
    var selectedYear = null;
    $(document).ready(function(){
        $("#course").change(function(){
            selectedCourse = $(this).find(":selected").val();
        });
        $("#year").change(function(){
            selectedYear = $(this).find(":selected").val();
        });
        $("#schedules").click(function(){
            if(selectedCourse && selectedYear){
                $.get("reports/studentsschedules/commonsched/" + selectedCourse + '/' + selectedYear, function(data){
                    $("#availability").html(data);
                });
            }
            else{
                alert("Please choose a course ID and year level.");
            }
        });
    });
</script>