@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="table table-striped table-bordered table-report table-responsive">
    <thead>
    <tr>
    <th>SCHISTOSOMIASIS</th>
    <th width="15%">Male</th>
    <th width="15%">Female</th>
    <th width="15%">Total</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>Symptomatic Cases</td>
            <td>{{ $data['SYMPCASE_M'] }}</td>
            <td>{{ $data['SYMPCASE_F'] }}</td>
            <td>{{ $data['SYMPCASE_M'] + $data['SYMPCASE_F'] }}</td>
        </tr>
        <tr>
            <td>Positive Cases</td>
            <td>{{ $data['POSCASE_M'] }}</td>
            <td>{{ $data['POSCASE_F'] }}</td>
            <td>{{ $data['POSCASE_M'] + $data['POSCASE_F'] }}</td>
        </tr>
        <tr>
            <td>Case Examined, Low Intensity</td>
            <td>{{ $data['CASEX_LOW_M'] }}</td>
            <td>{{ $data['CASEX_LOW_F'] }}</td>
            <td>{{ $data['CASEX_LOW_M'] + $data['CASEX_LOW_F'] }}</td>
        </tr>
        <tr>
            <td>Case Examined, Medium Intensity</td>
            <td>{{ $data['CASEX_MED_M'] }}</td>
            <td>{{ $data['CASEX_MED_F'] }}</td>
            <td>{{ $data['CASEX_MED_M'] + $data['CASEX_MED_F'] }}</td>
        </tr>
        <tr>
            <td>Case Examined, High Intensity</td>
            <td>{{ $data['CASEX_HIGH_M'] }}</td>
            <td>{{ $data['CASEX_HIGH_F'] }}</td>
            <td>{{ $data['CASEX_HIGH_M'] + $data['CASEX_HIGH_F'] }}</td>
        </tr>
        <tr>
            <td>Cases Treated</td>
            <td>{{ $data['CASETX_M'] }}</td>
            <td>{{ $data['CASETX_F'] }}</td>
            <td>{{ $data['CASETX_M'] + $data['CASETX_F'] }}</td>
        </tr>
        <tr>
            <td>Cases referred to hospital facilities</td>
            <td>{{ $data['CASEREF_M'] }}</td>
            <td>{{ $data['CASEREF_F'] }}</td>
            <td>{{ $data['CASEREF_M'] + $data['CASEREF_F'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>{{ $data['COMPCASE_M'] }}</td>
            <td>{{ $data['COMPCASE_F'] }}</td>
            <td>{{ $data['COMPCASE_M'] + $data['COMPCASE_F'] }}</td>
        </tr>
    </tbody>
</table> @yield('modal-footer')