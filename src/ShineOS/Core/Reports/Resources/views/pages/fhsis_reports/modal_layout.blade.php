<!-- PROGRAM HEADER -->
    @section('modal-header')
    <?php 
        $population = 0;
        if(isset($geodata) AND $geodata!=NULL) {
            foreach ($geodata as $key => $value) { 
                $population += $value->population;
            } 
        }
        $m = arrMonth();
    ?>

    <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
    {!! HTML::style('public/dist/plugins/stickytableheader/css/component.css') !!}
    <style>
        tbody tr.mast {
            background-color: #FFFFFF !important;
        }
        .mast .thleft {
            background-color: #FFFFFF !important;
        }
        .mast .thleft {
            width: 40%;
        }
        .mastLogo, .mastCode {
            text-align: center;
        }
        .mastCode h1 {
            color:  #555555;
            font-family: '', sans-serif;
            font-size: 100px;
            font-weight: 900;
            margin: -25px 0;
        }
        .mastCode h2 {
            color:  #555555;
            font-family: '', sans-serif;
            font-size: 50px;
            font-weight: 900;
            margin: -22px 0;
            letter-spacing: 3px;
        }
        td, th {
            padding: 3px;
            text-align: left;
        }
        .mast td {
            border-top: 0 !important;
        }
        table table {
            margin-bottom: 0;
        }
        @media print {
            .table-report.table-bordered {
                    border-color: #000;
                }   
            }
    </style>

    <!--NOTE:: SEPARATE PORTIONS-->
    {!! Form::hidden('fhsis_data',json_encode($data)) !!}
    <div class="row">
    <div class="">
        <div class="box box-primary">
            <div class="">
                <table class="table table-report">
                    <tbody>
                        <tr class="mast">
                            <td width="180" class="mastLogos">
                                <img src="{{ asset('public/dist/img/UHC_logo.jpg') }}" height="70" /> <img src="{{ asset('public/dist/img/doh.png') }}" height="70" />
                            </td>
                            <td>
                                <table>
                                    <tr> 
                                        <td class="thleft">Barangay</td>
                                        <td colspan="2">
                                            @if(is_array($data['bgycode']))
                                                ALL
                                            @else
                                                {{ getBrgyName($data['bgycode']) }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="thleft">City/Municipality of</td>
                                        <td colspan="2">{{ getCityName($data['citycode']) or NULL }}</td>
                                    </tr>
                                    <tr>
                                        <td class="thleft">Province of</td>
                                        <td colspan="2">{{ getProvinceName($data['provcode']) or NULL }}</td>
                                    </tr>
                                    <tr>
                                        <td class="thleft">Month and Year</td>
                                        <td colspan="2">
                                            @if(is_array($data['DMonth']))
                                                @if(count($data['DMonth']) == 3)
                                                {{ $data['quarter'] }} quarter of
                                                @else
                                                All months of
                                                @endif
                                            @else
                                                {{ $m[$data['DMonth']] }}
                                            @endif
                                            {{ $data['DYear'] or NULL }}
                                            {{ $data['YEAR'] or NULL }}
                                            {{ $data['YEAR_ENV'] or NULL }}
                                            {{ $data['POP_YEAR'] or NULL }}
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="thleft">Projected Population of the Year</td>
                                        <td colspan="2">{{ $population or NULL }}</td>
                                    </tr>
                                </table>
                            </td>
                            <td width="130" class="mastCode">
                                <h1 id="formtype">{{ $formType }}</h1>
                                <h2>{{ $classType }}</h2>
                            </td>
                        </tr>
                    </tbody>
                </table><!-- /table details -->
    @stop
<!-- END OF PROGRAM HEADER -->

<!-- PROGRAM BODY -->
<!-- PROGRAM BODY -->

<!-- PROGRAM FOOTER -->
    @section('modal-footer')
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
    </div>
    <button type="submit" value="submit" class="btn btn-success hidden"> Submit to eFHSIS </button>
    @stop
<!-- END OF PROGRAM FOOTER -->