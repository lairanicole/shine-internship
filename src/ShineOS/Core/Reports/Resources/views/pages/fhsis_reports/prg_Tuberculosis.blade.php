@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="table table-striped table-bordered table-report table-responsive">
    <thead>
    <tr>
    <th>TUBERCULOSIS</th>
    <th width="15%">Male</th>
    <th width="15%">Female</th>
    <th width="15%">Total</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>TB symptomatics who underwent DSSM</td>
        <td>{{ $scopeTBDSSMM = $data['TBSYMP_M'] }}</td>
        <td>{{ $scopeTBDSSMF = $data['TBSYMP_F'] }}</td>
        <td>{{ $scopeTBDSSMM + $scopeTBDSSMF }}</td>
    </tr>
    <tr>
        <td>Smear Positive discovered and identified</td>
        <td>{{ $scopeTBDSSMPosM = $data['SMEARPOS_M'] }}</td>
        <td>{{ $scopeTBDSSMPosF = $data['SMEARPOS_F'] }}</td>
        <td>{{ $scopeTBDSSMPosM + $scopeTBDSSMPosF }}</td>
    </tr>
    <tr>
        <td>New Smear (+) cases initiated tx &amp; registered</td>
        <td>{{ $scopeTBDSSMPosRegisteredM = $data['NEWSMEAR_M'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredF = $data['NEWSMEAR_F'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredM + $scopeTBDSSMPosRegisteredF }}</td>
    </tr>

    <tr>
        <td>New Smear (+) cases cured</td>
        <td>{{ $scopeTBDSSMPosCuredM = $data['NWSMEARPOS_M'] }}</td>
        <td>{{ $scopeTBDSSMPosCuredF = $data['NWSMEARPOS_F'] }}</td>
        <td>{{ $scopeTBDSSMPosCuredM + $scopeTBDSSMPosCuredF }}</td>
    </tr>
    <tr>
        <td>Smear (+) retreatment cases cured</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredM = $data['SMEARET_M'] }}</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredF = $data['SMEARET_F'] }}</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredM + $scopeTBDSSMPosRetreatmentCuredF }}</td>
    </tr>
    <tr>
        <td>Smear (+) retreatment cases initiated tx &amp; registered</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Relapse</td>
        <td>{{ $scopeTBDSSMPosRegisteredRM = $data['SMEARET_REL_M'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredRF = $data['SMEARET_REL_F'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredRM + $scopeTBDSSMPosRegisteredRF }}</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Treatment Failure</td>
        <td>{{ $scopeTBDSSMPosRegisteredTAFM = $data['SMEARET_TXF_M'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredTAFF = $data['SMEARET_TXF_F'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredTAFM + $scopeTBDSSMPosRegisteredTAFF }}</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Return After Default</td>
        <td>{{ $scopeTBDSSMPosRegisteredRADM = $data['SMEARET_RAD_M'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredRADF = $data['SMEARET_RAD_F'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredRADM + $scopeTBDSSMPosRegisteredRADF }}</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Other type of TB</td>
        <td>{{ $scopeTBDSSMPosRegisteredOTHM = $data['SMEARET_OTB_M'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredOTHF = $data['SMEARET_OTB_F'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredOTHM + $scopeTBDSSMPosRegisteredOTHF }}</td>
    </tr>
    <tr>
        <td>No of smear (+) retreatment cured</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Relapse</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredRM = $data['SMEARET_CURED_REL_M'] }}</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredRF = $data['SMEARET_CURED_REL_F'] }}</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredRM + $scopeTBDSSMPosRetreatmentCuredRF }}</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Treatment Failure</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredTAFM = $data['SMEARET_CURED_TXF_M'] }}</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredTAFF = $data['SMEARET_CURED_TXF_F'] }}</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredTAFM + $scopeTBDSSMPosRetreatmentCuredTAFF }}</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Return After Default</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredRADM = $data['SMEARET_CURED_RAD_M'] }}</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredRADF = $data['SMEARET_CURED_RAD_F'] }}</td>
        <td>{{ $scopeTBDSSMPosRetreatmentCuredRADM + $scopeTBDSSMPosRetreatmentCuredRADF }}</td>
    </tr>
    <tr>
        <td>Total No. of TB cases (all forms) initiated treatment</td>
        <td>{{ $scopeTBDSSMPosRegisteredALLM = $data['TOT_TB_M'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredALLF = $data['TOT_TB_F'] }}</td>
        <td>{{ $scopeTBDSSMPosRegisteredALLM + $scopeTBDSSMPosRegisteredALLF }}</td>
    </tr>
    <tr>
        <td>TB all forms identified</td>
        <td>{{ $scopeTBM = $data['TB_ALL_M'] }}</td>
        <td>{{ $scopeTBF = $data['TB_ALL_F'] }}</td>
        <td>{{ $scopeTBM + $scopeTBF }}</td>
    </tr>
    <tr>
        <td>Cases Detection Rate</td>
        <td>0</td>
        <td>0</td>
        <td>0</td>
    </tr>
    </tbody>
</table> @yield('modal-footer')