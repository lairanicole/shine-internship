@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="table table-striped table-bordered table-report table-responsive">
    <thead>
        <tr><th colspan="4">LEPROSY</th></tr>
        <tr><th></th><th>Male</th><th>Female</th><th>Total</th></tr>
    </thead>
    <tbody>
        <tr>
            <td>Leprosy case</td>
            <td> {{ $data['LEPCASE_M'] }} </td>
            <td> {{ $data['LEPCASE_F'] }} </td>
            <td> {{ $data['LEPCASE_M'] + $data['LEPCASE_F'] }} </td>
        </tr>
        <tr>
            <td>Leprosy cases below 15</td>
            <td> {{ $data['LEPCASE15_M'] }} </td>
            <td> {{ $data['LEPCASE15_F'] }} </td>
            <td> {{ $data['LEPCASE15_M'] + $data['LEPCASE15_F'] }} </td>
        </tr>
        <tr>
            <td>new detected leprosy cases</td>
            <td> {{ $data['NEWDET_M'] }} </td>
            <td> {{ $data['NEWDET_F'] }} </td>
            <td> {{ $data['NEWDET_M'] + $data['NEWDET_F'] }} </td>
        </tr>
        <tr>
            <td>new detected leprosy cases with grade 2 disability</td>
            <td> {{ $data['NEWDET2_M'] }} </td>
            <td> {{ $data['NEWDET2_F'] }} </td>
            <td> {{ $data['NEWDET2_M'] + $data['NEWDET2_F'] }} </td>
        </tr>
        <tr>
            <td>cases cured</td>
            <td> {{ $data['CASECURED_M'] }} </td>
            <td> {{ $data['CASECURED_F'] }} </td>
            <td> {{ $data['CASECURED_M'] + $data['CASECURED_F'] }} </td>
        </tr>
    </tbody>
</table>
@yield('modal-footer')