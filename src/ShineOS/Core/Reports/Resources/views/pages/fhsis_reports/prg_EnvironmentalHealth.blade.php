@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="table table-striped table-bordered table-report table-responsive">
<thead>
    <tr>
        <th>ENVIRONMENTAL</th>
        <th width="15%">No.</th>
        <th width="15%">%</th>
    </tr>
</thead>

<tbody>
    <tr>
        <td>Households with access to improved or safe water supply ♣</td>
        <td>{{ $HHWATER_LEVEL = $data['HHWATER_LEVEL1'] + $data['HHWATER_LEVEL2'] + $data['HHWATER_LEVEL3'] }}</td>
        <td>{{ getPercentage(($HHWATER_LEVEL), $data['HH'], 100, 2) }}%</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level I (Point Source) ♣</td>
        <td>{{ $data['HHWATER_LEVEL1'] }}</td>
        <td>{{ getPercentage($data['HHWATER_LEVEL1'],$data['HH'],100,2) }}%</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level II (Communal Faucet System or Standpost) ♣</td>
        <td>{{ $data['HHWATER_LEVEL2'] }}</td>
        <td>{{ getPercentage($data['HHWATER_LEVEL2'],$data['HH'],100,2) }}%</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level III (Waterworks System) ♣</td>
        <td>{{ $data['HHWATER_LEVEL3'] }}</td>
        <td>{{ getPercentage($data['HHWATER_LEVEL3'],$data['HH'],100,2) }}%</td>
    </tr>
    <tr>
        <td>Households with sanitary toilet facilities ♣</td>
        <td>{{ $data['HHSANTOILET'] }}</td>
        <td>{{ getPercentage($data['HHSANTOILET'],$data['HH'],100,2) }}%</td>
    </tr>
    <tr>
        <td>Households with satisfactory disposal of solid waste ♣</td>
        <td>{{ $data['HHWASTE'] }}</td>
        <td>{{ getPercentage($data['HHWASTE'],$data['HH'],100,2) }}%</td>
    </tr>
    <tr>
        <td>Households with complete basic sanitation facilities ♣</td>
        <td>{{ $data['HHSANFAC'] }}</td>
        <td>{{ getPercentage($data['HHSANFAC'],$data['HH'],100,2) }}%</td>
    </tr>
    <tr>
        <td>Food Establishments </td>
        <td>{{ $data['FOODEST'] }}</td><td bgcolor="#A9A9A9"></td>
    </tr>
    <tr>
        <td>Food Establishments with sanitary permit ♥</td>
        <td>{{ $data['FOODEST_SAN'] }}</td>
        <td>{{ getPercentage($data['FOODEST_SAN'],$data['FOODEST'],100,2) }}%</td>
    </tr>
    <tr>
        <td>Food Handlers</td>
        <td>{{ $data['FOODHAND'] }}</td><td bgcolor="#A9A9A9"></td>
    </tr>
    <tr>
        <td>Food Handlers  with health certificate ☻</td>
        <td>{{ $data['FOODHAND_HC'] }}</td>
        <td>{{ getPercentage($data['FOODHAND_HC'],$data['FOODHAND'],100,2) }}%</td>
    </tr>
    <tr>
        <td>Salt Samples Tested</td>
        <td>{{ $data['SALT_TEST'] }}</td><td bgcolor="#A9A9A9"></td>
    </tr>
    <tr>
        <td>Salt Samples Tested (+) for iodine</td>
        <td>{{ $data['SALT_TESTIOD'] }}</td>
        <td>{{ getPercentage($data['SALT_TESTIOD'],$data['SALT_TEST'],100,2) }}%</td>
    </tr>
    <tr>
        <td colspan="3"><em class="small">Denominator: &nbsp; ♣No. Households &nbsp; ♥No.Food Establishments &nbsp; ☻No.Food Handlers  </em></td>
    </tr>
</tbody>
</table>
@yield('modal-footer') 