@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="table table-striped table-bordered table-report table-responsive">
    <thead>
    <tr>
        <th>MALARIA</th>
        <th width="15%">Male</th>
        <th width="15%">Female</th>
        <th width="15%">Total</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>Total Population</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <td>Population at Risk</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <td>Annual parasite incidence</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <td>Confirmed Malaria Cases 
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;5 yrs. old</td>
            <td>{{ $scopeMalariaLessFiveM = ShineOS\Core\Reports\Entities\M1::scopeMalariaLessFive('M', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaLessFiveF = ShineOS\Core\Reports\Entities\M1::scopeMalariaLessFive('F', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaLessFiveM + $scopeMalariaLessFiveF }}</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&gt;=5 yrs. old</td>
            <td>{{ $scopeMalariaGreaterFiveM = ShineOS\Core\Reports\Entities\M1::scopeMalariaGreaterFive('M', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaGreaterFiveF = ShineOS\Core\Reports\Entities\M1::scopeMalariaGreaterFive('F', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaGreaterFiveM + $scopeMalariaGreaterFiveF }}</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pregnant</td>
            <td bgcolor="#A9A9A9"></td>
            <td>{{ $scopeMalariaGreaterPregnant = ShineOS\Core\Reports\Entities\M1::scopeMalariaPregnant('m', $month, $year, $barangay) }}</td>
            <td bgcolor="#A9A9A9"></td>
        </tr>
        <tr>
            <td>Confirmed Malaria Cases by Species</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Falciparum</td>
            <td>{{ $scopeMalariaFalciparumM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Falciparum','M', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaFalciparumF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Falciparum','F', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaFalciparumM + $scopeMalariaFalciparumF }}</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Vivax</td>
            <td>{{ $scopeMalariaVivaxM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Vivax','M', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaVivaxF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Vivax','F', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaVivaxM + $scopeMalariaVivaxF }}</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Ovale</td>
            <td>{{ $scopeMalariaOvaleM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Ovale','M', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaOvaleF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Ovale','F', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaOvaleM + $scopeMalariaOvaleF }}</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Malariae</td>
            <td>{{ $scopeMalariaMalariaeM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Malariae','M', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaMalariaeF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Malariae','F', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaMalariaeM + $scopeMalariaMalariaeF }}</td>
        </tr>
        <tr>
            <td>By Method</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Slide</td>
            <td bgcolor="#A9A9A9"></td>
            <td bgcolor="#A9A9A9"></td>
            <td>0</td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RDT</td>
            <td bgcolor="#A9A9A9"></td>
            <td bgcolor="#A9A9A9"></td>
            <td>0</td>
        </tr>
        <tr>
            <td>Malaria Deaths</td>
            <td>{{ $scopeMalariaDeathM = ShineOS\Core\Reports\Entities\M1::scopeMalariaDeaths('M', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaDeathF = ShineOS\Core\Reports\Entities\M1::scopeMalariaDeaths('F', 'm', $month, $year, $barangay) }}</td>
            <td>{{ $scopeMalariaDeathM + $scopeMalariaDeathF }}</td>
        </tr>
        <tr>
            <td>Number of LLIN given</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
    </tbody>
</table>
@yield('modal-footer')