@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
    <table class="table table-striped table-bordered table-report table-responsive">
        <thead>
        <tr>
            <th width="55%">MATERNAL CARE</th>
            <th>NO.</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Pregnant women with 4 or more Prenatal visits</td>
            <td>{{ $data['PC1'] }}</td>
        </tr>
        <tr>
            <td>Pregnant women given 2 doses of Tetanus Toxoid</td>
            <td>{{ $data['PC2'] }}</td>
        </tr>
        <tr>
            <td>Pregnant women given TT2 plus</td>
            <td>{{ $data['PC3'] }}</td>
        </tr>
        <tr>
            <td>Pregnant women given complete iron w/ folic acid supplementation</td>
            <td>{{ $data['PC4'] }}</td>
        </tr>
        <tr>
            <td>Pregnant women given Vitamin A supplementation</td>
            <td>{{ $data['PC5'] }}</td>
        </tr>
        <tr>
            <td>Postpartum women with at least 2 postpartum visits</td>
            <td>{{ $data['PP1'] }}</td>
        </tr>
        <tr>
            <td>Postpartum women given complete iron supplementation</td>
            <td>{{ $data['PP2'] }}</td>
        </tr>
        <tr>
            <td>Postpartum women given Vitamin A supplementation</td>
            <td>{{ $data['PP3'] }}</td>
        </tr>
        <tr>
            <td>PP women initiated breastfeeding w/in 1 hr. after delivery</td>
            <td>{{ $data['PP4'] }}</td>
        </tr>
        <tr>
            <td>Women 10-49 years old given Iron supplementation</td>
            <td>{{ $data['W1049_VITA'] }}</td>
        </tr>
        <tr>
            <td>Deliveries</td>
            <td>{{ $data['DELIV'] }}</td>
        </tr>
        </tbody>
    </table><!-- /table Maternal -->
 @yield('modal-footer')   