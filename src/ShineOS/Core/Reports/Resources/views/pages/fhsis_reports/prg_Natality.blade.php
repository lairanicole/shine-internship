@include('reports::pages.fhsis_reports.modal_layout')
@yield('modal-header')
<table class="table table-striped table-bordered table-report table-responsive">
    <thead>
        <tr>
            <th colspan="8">NATALITY</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td colspan="2">No. of livebirths</td>
            <td colspan="2">{{ $data['LBTOT_M'] + $data['LBTOT_F'] }}</td>
            <td><strong>Birthweight</strong></td>
            <td><strong>Male</strong></td>
            <td><strong>Female</strong></td>
            <td><strong>Total</td></strong>
        </tr>
        <tr>
            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No. of male</td>
            <td colspan="2">{{ $data['LBTOT_M'] }}</td>
            <td>2500 grams &amp; greater</td>
            <td>{{ $data['BW2500UP_M'] }}</td>
            <td>{{ $data['BW2500UP_F'] }}</td>
            <td>{{ $data['BW2500UP_M'] + $data['BW2500UP_F'] }}</td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No. of female</td>
            <td colspan="2">{{ $data['LBTOT_F'] }}</td>
            <td>Less than 2500 grams</td>
            <td>{{ $data['BW2500LESS_M'] }}</td>
            <td>{{ $data['BW2500LESS_F'] }}</td>
            <td>{{ $data['BW2500LESS_M'] + $data['BW2500LESS_F'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td><strong>Male</strong></td>
            <td><strong>Female</strong></td>
            <td><strong>Total</strong></td>
            <td><strong>Total</strong></td>
            <td>{{ $data['BW2500UP_M'] + $data['BW2500LESS_M'] }}</td>
            <td>{{ $data['BW2500UP_F'] + $data['BW2500LESS_F'] }}</td>
            <td>{{ $data['BW2500LESS_M'] + $data['BW2500LESS_F'] + $data['BW2500UP_M'] + $data['BW2500UP_F'] }}</td>
        </tr>
        <tr>
            <td>Doctors</td>
            <td>{{ $data['ATTDOC_M'] }}</td>
            <td>{{ $data['ATTDOC_F'] }}</td>
            <td>{{ $data['ATTDOC_M'] + $data['ATTDOC_F'] }}</td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td>Nurses</td>
            <td>{{ $data['ATTNURSE_M'] }}</td>
            <td>{{ $data['ATTNURSE_F'] }}</td>
            <td>{{ $data['ATTNURSE_M'] + $data['ATTNURSE_F'] }}</td>
            <td><strong>Pregnancy Outcome</strong></td>
            <td><strong>Male</strong></td>
            <td><strong>Female</strong></td>
            <td><strong>Total</strong></td>
        </tr>
        <tr>
            <td>Midwives</td>
            <td>{{ $data['ATTMIDW_M'] }}</td>
            <td>{{ $data['ATTMIDW_F'] }}</td>
            <td>{{ $data['ATTMIDW_M'] + $data['ATTMIDW_F'] }}</td>
            <td>Livebirths</td>
            <td>{{ $data['NOPREG_M'] }}</td>
            <td>{{ $data['NOPREG_F'] }}</td>
            <td>{{ $data['NOPREG_M'] + $data['NOPREG_F'] }}</td>
        </tr>
        <tr>
            <td>Hilot/TBA</td>
            <td>{{ $data['ATTTH_M'] }}</td>
            <td>{{ $data['ATTTH_F'] }}</td>
            <td>{{ $data['ATTTH_M'] + $data['ATTTH_F'] }}</td>
            <td>Fetal Deaths</td>
            <td>{{ $data['FDTOT_M'] }}</td>
            <td>{{ $data['FDTOT_F'] }}</td>
            <td>{{ $data['FDTOT_M'] + $data['FDTOT_F'] }}</td>
        </tr>
        <tr>
            <td>Others</td>
            <td>{{ $data['ATTOTHER_M'] }}</td>
            <td>{{ $data['ATTOTHER_F'] }}</td>
            <td>{{ $data['ATTOTHER_M'] + $data['ATTOTHER_F'] }}</td>
            <td>Abortion</td>
            <td>{{ $data['ABRTOT_M'] }}</td>
            <td>{{ $data['ABRTOT_F'] }}</td>
            <td>{{ $data['ABRTOT_M'] + $data['ABRTOT_F'] }}</td>
        </tr>
        <tr>
            <td colspan="4"><strong>Deliveries by Type and Place</strong></td>
        </tr>
        <tr>
            <td rowspan="2">Type</td><td colspan="2">NID</td><td rowspan="2">Health Facility</td>
        </tr>
        <tr>
            <td>Home</td>
            <td>Others</td>
        </tr>
        <tr>
            <td>Normal</td>
            <td>{{ $data['NORMDEL_HOME_M'] + $data['NORMDEL_HOME_F'] }}</td>
            <td>{{ $data['NORMDEL_OTH_M'] + $data['NORMDEL_OTH_F'] }}</td>
            <td>{{ $data['NORMDEL_HOSP_M'] + $data['NORMDEL_HOSP_F'] }}</td>
        </tr>
        <tr>
            <td>Operative</td>
            <td>{{ $data['OTD_HOME_M'] + $data['OTD_HOME_F'] }}</td>
            <td>{{ $data['OTD_OTHER_M'] + $data['OTD_OTHER_F'] }}</td>
            <td>{{ $data['OTD_HOSP_M'] + $data['OTD_HOSP_F'] }}</td>
        </tr>
    </tbody>
</table> @yield('modal-footer')