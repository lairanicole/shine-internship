<div class="row">
    <div class="col-md-12">
        <h4>DOH FHSIS Reporting</h4>
        <p class="box-content-para">To generate your FHSIS Report by setting the date range you want and clicking on the Generate button.</p>
        @if($nocatchment)
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
            <p>You have not set your Barangay Catchment Areas. All barangays in your municipality will appear on the selections. You can set them now, <kbd><a href="facilities#tab_5">click here</a></kbd>.</p>
        </div>
        
        @endif
    </div>

    <div class="col-md-4">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Monthly</h3>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <p><label>Monthly Program Report</label></p>
                    <p>
                        <select name="m1program" class="form-control" id="m1program">
                            <option>Select Program Type</option>
                            @foreach($monthlyPrograms as $type_k => $type_v)
                                @if($type_k == 'grp_DiseaseControl') {
                                    <optgroup label='Disease Control'>
                                    @foreach($type_v as $dc_k => $dc_v)
                                        <option value="{{$dc_k}}">{{ $dc_v }}</option>        
                                    @endforeach
                                    </optgroup>
                                @else
                                <option value="{{$type_k}}">{{ $type_v }}</option>
                                @endif
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <select name="m1barangay" class="form-control" id="m1barangay">
                            <option>Select Barangay</option>
                            <option value="allm1barangay">All Barangay</option>
                            @foreach($facilitybrgycodes as $brgy_k => $brgy_v)
                                <option value="{{$brgy_k}}">{{ $brgy_v }}</option>
                            @endforeach
                        </select>

                    </p>
                    <p>
                        <select class="form-control" id="m1year" name="m1year" onchange="updateMon(this.value);">
                            <option>Select Year</option>
                            @for ($i = $arrM1year; $i <= $yearNow; $i++)
                                <option value="{{$i}}">{{ $i }}</option>
                            @endfor
                        </select>
                    </p>
                    <p>
                        <select class="form-control" id="m1month" name="m1month">
                            <option>Select year first</option>
                        </select>
                    </p>
                    
                    <p>
                        <button class="btn btn-primary form-control" id="reportM1" data-toggle="modal" data-target="#myInfoModal" data-url="{{ url('reports/fhsis/m1') }}">Generate Month Report</button>
                    </p>
                </div> 
            </div><!-- /.box-body -->
        </div>
    </div>

    <div class="col-md-4">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Quarterly</h3>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <p><label>Quarterly Program Report</label></p>
                    <p>
                        <select name="q1program" class="form-control" id="q1program">
                            <option>Select Program Type</option>
                            @foreach($quarterlyPrograms as $type_k => $type_v)
                                @if($type_k == 'grp_DiseaseControl') {
                                    <optgroup label='Disease Control'>
                                    @foreach($type_v as $dc_k => $dc_v)
                                        <option value="{{$dc_k}}">{{ $dc_v }}</option>        
                                    @endforeach
                                    </optgroup>
                                @else
                                <option value="{{$type_k}}">{{ $type_v }}</option>
                                @endif
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <select class="form-control" id="q1barangay" name="q1barangay">
                            <option>Select Barangay</option>
                            <option value="allq1barangay">All Barangay</option>
                            @foreach($facilitybrgycodes as $brgy_k => $brgy_v)
                                <option value="{{$brgy_k}}">{{ $brgy_v }}</option>
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <select class="form-control" name="q1year" id="q1year" onchange="updateQtr(this.value);">
                            <option>Select Year</option>
                            @for ($i = $arrM1year; $i <= $yearNow; $i++)
                                <option value="{{$i}}">{{ $i }}</option>
                            @endfor
                        </select>
                    </p>
                    <p>
                        <select class="form-control" id="q1month" name="q1month">
                            <option>Select year first</option>
                        </select>
                    </p>
                    
                    <p>
                        <button class="btn btn-primary form-control" id="reportQ1" data-toggle="modal" data-target="#myInfoModal" data-url="{{ url('reports/fhsis/q1') }}">Generate Quarter Report</button>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Annual</h3>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <p><label>Annual Program Report</label></p>
                    <p>
                        <select name="a1program" class="form-control" id="a1program">
                            <option>Select Program Type</option>
                            @foreach($annualPrograms as $type_k => $type_v)
                                @if($type_k == 'grp_Abrgy') {
                                    <optgroup label='Annual A1 (A-Brgy)'>
                                    @foreach($type_v as $dc_k => $dc_v)
                                        <option value="{{$dc_k}}">{{ $dc_v }}</option>        
                                    @endforeach
                                    </optgroup>
                                @else
                                <option value="{{$type_k}}">{{ $type_v }}</option>
                                @endif
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <select class="form-control" id="a1barangay" name="a1barangay">
                            <option>Select Barangay</option>
                            <option value="alla1barangay">All Barangay</option>
                            @foreach($facilitybrgycodes as $brgy_k => $brgy_v)
                                <option value="{{$brgy_k}}">{{ $brgy_v }}</option>
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <select class="form-control" id="a1year">
                            <option>Select Year</option>
                            @for ($i = $arrM1year; $i <= $yearNow; $i++)
                                <option value="{{$i}}">{{ $i }}</option>
                            @endfor
                        </select>
                    </p>
                    <p>
                        <button class="btn btn-primary form-control" id="reportA1" data-toggle="modal" data-target="#myInfoModal" data-url="{{ url('reports/fhsis/a1') }}">Generate Annual Report</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myInfoModal" tabindex="-1" role="dialog" aria-labelledby="myInfoModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close margin" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <button type="button" class="close margin btn btn-sm btn-default printBut"><span aria-hidden="true"><i class="fa fa-print"></i></span></button>

                <h4 class="modal-title" id="myInfoModalLabel"> FHSIS Report </h4>
            </div>
            
            {!! Form::open(array('url' => 'reports/fhsis/send_fhsis','class'=>'form-horizontal', 'method'=>'POST') ) !!}
            <div class="modal-body" id="printable">
            </div> 
            <div class="modal-footer">
                <!-- <button type="submit" class="btn btn-default sendFHSIS" data-url="{{ url('reports/fhsis/send_fhsis') }}">Submit to eFHSIS</button> -->
                <button type="button" class="btn btn-default printBut">Print</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@section('page_scripts')
<script type="text/javascript">

    function updateMon(val) {
        $("#m1month").html("");
        //get current year
        var monsel = "<option>Select Month</option>";
        var currentTime = new Date();
        var year = currentTime.getFullYear();
        var month = currentTime.getMonth();
        var mons = [1,2,3,4,5,6,7,8,9,10,11,12];
        var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];

        mons.forEach(function(i){
            if((val == year && i < (month+1)) || val != year) {
                monsel += "<option value='"+i+"'>"+months[i-1]+"</option>"
            }
        })

        $("#m1month").html(monsel);
    }
    function updateQtr(val) {
        $("#q1month").html("");
        //get current year
        var monsel = "<option>Select Quarter</option>";
        var currentTime = new Date();
        var year = currentTime.getFullYear();
        var month = currentTime.getMonth();
        var quarter = Math.floor((month + 1)/3);
        var mons = [1,2,3,4];
        var qyrs = ["1st","2nd","3rd","4th"];
        var months = ["1st Quarter","2nd Quarter","3rd Quarter","4th Quarter"];

        mons.forEach(function(i){
            if((val == year && i <= quarter) || val != year) {
                monsel += "<option value='"+qyrs[i-1]+"'>"+months[i-1]+"</option>"
            }
        })

        $("#q1month").html(monsel);
    }

    var loadingContent = '<div class="row text-center"><i class="fa fa-spinner fa-spin fa-pulse fa-3x"></i></div>';

    $("#myInfoModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        var ttle = link.parents('.col-md-12').find("label").text()
        $(this).find("div.modal-body").html(loadingContent);

        if(link.attr('id') == 'reportM1') {
            var program = link.parents('.col-md-12').find("select#m1program").val();
            var programTitle = link.parents('.col-md-12').find("select#m1program option:selected").text();
            var barangay = link.parents('.col-md-12').find("select#m1barangay").val();
            var month = link.parents('.col-md-12').find("select#m1month").val();
            var year = link.parents('.col-md-12').find("select#m1year").val();

            $(this).find("h4.modal-title").text(ttle+" for "+programTitle);
            $(this).find("div.modal-body").load(link.attr("data-url")+'/'+program+'/'+month+'/'+year+'/'+barangay);
        }

        if(link.attr('id') == 'reportQ1') {
            var program = link.parents('.col-md-12').find("select#q1program").val();
            var programTitle = link.parents('.col-md-12').find("select#q1program option:selected").text();
            var barangay = link.parents('.col-md-12').find("select#q1barangay").val();
            var month = link.parents('.col-md-12').find("select#q1month").val();
            var year = link.parents('.col-md-12').find("select#q1year").val();
            $(this).find("h4.modal-title").text(ttle+" for "+programTitle);
            $(this).find("div.modal-body").load(link.attr("data-url")+'/'+program+'/'+month+'/'+year+'/'+barangay);
        }

        if(link.attr('id') == 'reportA1') {
            var program = link.parents('.col-md-12').find("select#a1program").val();
            var programTitle = link.parents('.col-md-12').find("select#a1program option:selected").text();
            var barangay = link.parents('.col-md-12').find("select#a1barangay").val();
            var month = 0;
            var year = link.parents('.col-md-12').find("select#a1year").val();
            $(this).find("h4.modal-title").text(ttle+" for "+programTitle);
            $(this).find("div.modal-body").load(link.attr("data-url")+'/'+program+'/'+month+'/'+year+'/'+barangay);

        }
    });

    $('.printBut').on('click', function(){
        $('#printable').printThis();
    })
</script>
@stop
