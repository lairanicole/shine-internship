<?php
$population = 0;

if(isset($geodata))
{
    $population = $geodata->population;
}

?>

@extends('reports::layouts.fhsis_master')

@section('heads')
{!! HTML::style('public/dist/plugins/stickytableheader/css/component.css') !!}
<style>
    #diseaseTable tr th {
        width: 65px;
    }
</style>
@stop
@section('reportGroup')FHSIS @stop
@section('reportTitle')M2 @stop
@section('content')
    <section class="fhsis">
      <div class="row report-header">
        <div class="col-xs-6">
            <h4>Morbidity Diseases Report M2</h4>
        </div>

        <table class="table table-striped table-bordered table-report">
            <tbody><tr><th class="thleft">FHSIS Report Month/Year</th><td>
                {!! Form::open(array( 'url'=>'reports/fhsis/m2/', 'id'=>'dateFilter', 'name'=>'dateFilter', 'class'=>'form-horizontal' )) !!}
                    <label class="col-sm-1 control-label">Month</label>
                    <div class="col-sm-2">
                        <input type="hidden" value="m" name="range">
                        <select name="month" class="form-control" id="month">
                            <option value="" selected="selected"></option>
                            <option value="1" @if($month[0] == '1') selected='selected'@endif >January</option>
                            <option value="2" @if($month[0] == '2') selected='selected'@endif >February</option>
                            <option value="3" @if($month[0] == '3') selected='selected'@endif >March</option>
                            <option value="4" @if($month[0] == '4') selected='selected'@endif >April</option>
                            <option value="5" @if($month[0] == '5') selected='selected'@endif >May</option>
                            <option value="6" @if($month[0] == '6') selected='selected'@endif >June</option>
                            <option value="7" @if($month[0] == '7') selected='selected'@endif >July</option>
                            <option value="8" @if($month[0] == '8') selected='selected'@endif >August</option>
                            <option value="9" @if($month[0] == '9') selected='selected'@endif >September</option>
                            <option value="10" @if($month[0] == '10') selected='selected'@endif >October</option>
                            <option value="11" @if($month[0] == '11') selected='selected'@endif >November</option>
                            <option value="12" @if($month[0] == '12') selected='selected'@endif >December</option>
                        </select>
                    </div>
                    <label class="col-sm-1 control-label">Year</label>
                    <div class="col-sm-2">
                        <?php $thisyear = date('Y'); ?>
                        <select name="year" class="form-control" id="year">
                            @for( $y=$thisyear-5; $y<=$thisyear; $y++)
                            <option @if($year == $y) selected='selected'@endif >{{ $y }}</option>
                            @endfor
                        </select>
                    </div>
                    <label class="col-sm-1 control-label">Barangay</label>
                    <div class="col-sm-3">
                        <select name="barangay" class="form-control" id="barangay">
                            @foreach($brgys as $brgy)
                            <option value="{{$brgy->barangay_code}}" @if($barangay == $brgy->barangay_code) selected='selected' @endif >{{ $brgy->barangay_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="submit" class="btn btn-primary" value="View">
                {!! Form::close() !!}
            </td></tr>
            <tr><th class="thleft">Name of BHS</th><td></td></tr>
            <tr><th class="thleft">Barangay</th><td>{{ getBrgyName($barangay) }}</td></tr>
            <tr><th class="thleft">City/Municipality of</th><td>{{ getCityName($facility->facility_contact->city) }}</td></tr>
            <tr><th class="thleft">Province of</th><td>{{ getProvinceName($facility->facility_contact->province) }}</td></tr>
            <tr><th class="thleft">Projected Population of the Year</th><td>{{ $population }}</td></tr>
            </tbody>
        </table>

      <div class="row">
        <div class="col-xs-12 table-responsive">
          @include('reports::pages.fhsis_reports.disease_table')
        </div>
      </div>

      <div class="row no-print">
        <div class="col-xs-12">
          <a href="fhsis-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button class="btn btn-primary pull-right hidden" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
        </div>
      </div>
    </section>
@stop

@section('scripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js"></script>
    {!! HTML::script('public/dist/plugins/stickytableheader/js/jquery.stickyheader.js') !!}
@stop
