<?php
$population = 0;
if(isset($geodata)) {
    $population = $geodata->population;
}
?>

<link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
{!! HTML::style('public/dist/plugins/stickytableheader/css/component.css') !!}
<style>
    tbody tr.mast {
        background-color: #FFFFFF !important;
    }
    .mast .thleft {
        background-color: #FFFFFF !important;
    }
    .mast .thleft {
        width: 40%;
    }
    .mastLogo, .mastCode {
        text-align: center;
    }
    .mastCode h1 {
        color:  #555555;
        font-family: 'Work Sans', sans-serif;
        font-size: 100px;
        font-weight: 900;
        margin: -25px 0;
    }
    .mastCode h2 {
        color:  #555555;
        font-family: 'Work Sans', sans-serif;
        font-size: 50px;
        font-weight: 900;
        margin: -22px 0;
        letter-spacing: 3px;
    }
    td, th {
        padding: 3px;
        text-align: left;
    }
    .mast td {
        border-top: 0 !important;
    }
    table table {
        margin-bottom: 0;
    }
</style>

<!--NOTE:: SEPARATE PORTIONS-->
<div class="row">
<div class="col-md-12">
    <div class="box box-primary">
        
        <div class="box-body text-center">
            <table class="table table-report">
                <tbody>
                    <tr class="mast">
                        <td width="180" class="mastLogos">
                            <img src="{{ asset('public/dist/img/UHC_logo.jpg') }}" height="70" /> <img src="{{ asset('public/dist/img/doh.png') }}" height="70" />
                        </td>
                        <td>
                            <table>
                                <tr><td class="thleft">Name of BHS</td><td colspan="2"></td></tr>
                                <tr><td class="thleft">Barangay</td><td colspan="2">{{ getBrgyName($barangay) }}</td></tr>
                                <tr><td class="thleft">City/Municipality of</td><td colspan="2">{{ getCityName($facility->facility_contact->city) }}</td></tr>
                                <tr><td class="thleft">Province of</td><td colspan="2">{{ getProvinceName($facility->facility_contact->province) }}</td></tr>
                                <tr><td class="thleft">Projected Population of the Year</td><td colspan="2">{{ $population }}</td></tr>
                            </table>
                        </td>
                        <td width="150" class="mastCode">
                            <h1>M1</h1>
                            <h2>Brgy</h2>
                        </td>
                    </tr>
                </tbody>
            </table><!-- /table details -->

            <table class="table table-striped table-bordered table-report">
                <thead>
                <tr>
                    <th width="55%">MATERNAL CARE</th>
                    <th>NO.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Pregnant women with 4 or more Prenatal visits</td>
                    <td>{{ $Pre4Visit = ShineOS\Core\Reports\Entities\M1::scopePre4Visit('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Pregnant women given 2 doses of Tetanus Toxoid</td>
                    <td>{{ $TT2X = ShineOS\Core\Reports\Entities\M1::scopeTT2x('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Pregnant women given TT2 plus</td>
                    <td>{{ $TT2 = ShineOS\Core\Reports\Entities\M1::scopeTT2('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Pregnant women given complete iron w/ folic acid supplementation</td>
                    <td>{{ $PreIron = ShineOS\Core\Reports\Entities\M1::scopePreIron('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Postpartum women with at least 2 postpartum visits</td>
                    <td>{{ $PP2V = ShineOS\Core\Reports\Entities\M1::scopePP2V('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Postpartum women given complete iron supplementation</td>
                    <td>{{ $PPIron = ShineOS\Core\Reports\Entities\M1::scopePPIron('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Postpartum women given Vitamin A supplementation</td>
                    <td>{{ $PPVitA = ShineOS\Core\Reports\Entities\M1::scopePPVitA('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>PP women initiated breastfeeding w/in 1 hr. after delivery</td>
                    <td>{{ $BFeeding = ShineOS\Core\Reports\Entities\M1::scopeBFeeding('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Women 10-49 years old given Iron supplementation</td>
                    <td>{{ $deliveries = ShineOS\Core\Reports\Entities\M1::scopeIronSup('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Deliveries</td>
                    <td>{{ $deliveries = ShineOS\Core\Reports\Entities\M1::scopeDeliveries('m', $month, $year, $barangay) }}</td>
                </tr>
                </tbody>
            </table><!-- /table Maternal -->
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
</div>
