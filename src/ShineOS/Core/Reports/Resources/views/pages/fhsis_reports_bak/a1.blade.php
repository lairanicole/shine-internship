<?php
$population = 0;

if(isset($geodata))
{
    $population = $geodata->population;
}

?>

@extends('reports::layouts.fhsis_master')

@section('heads')
{!! HTML::style('public/dist/plugins/stickytableheader/css/component.css') !!}

@stop
@section('reportGroup')FHSIS @stop
@section('reportTitle')FHSIS A1 @stop
@section('content')

<!--NOTE:: SEPARATE PORTIONS-->
<div class="row">
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Program Report A1</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-print"></i></button>
          </div><!-- /.box-tool -->
        </div><!-- /.box-header -->
        <div class="box-body text-center">
            <table class="table table-striped table-bordered table-report">
                <tbody><tr><th class="thleft">FHSIS Report Year</th><td>
                    {!! Form::open(array( 'url'=>'reports/fhsis/a1/', 'id'=>'dateFilter', 'name'=>'dateFilter', 'class'=>'form-horizontal' )) !!}
                        <label class="col-sm-2 control-label">Year</label>
                        <div class="col-sm-3">
                            <?php $thisyear = date('Y'); ?>
                            <select name="year" class="form-control" id="year">
                                @for( $y=$thisyear-5; $y<=$thisyear; $y++)
                                <option @if($year == $y) selected='selected'@endif >{{ $y }}</option>
                                @endfor
                            </select>
                        </div>
                        <input type="submit" class="btn btn-primary" value="View">
                    {!! Form::close() !!}
                </td></tr>
                <tr><th class="thleft">City/Municipality of</th><td>{{ getCityName($facility->facility_contact->city) }}</td></tr>
                <tr><th class="thleft">Province of</th><td>{{ getProvinceName($facility->facility_contact->province) }}</td></tr>
                <tr><th class="thleft">Projected Population of the Year</th><td>{{ $population }}</td></tr>
                </tbody>
            </table><!-- /table details -->

            <table class="table table-striped table-bordered table-report">
                <thead>
                <tr>
                    <th width="55%">MATERNAL CARE</th>
                    <th>NO.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Pregnant women with 4 or more Prenatal visits</td>
                    <td>{{ $Pre4Visit = ShineOS\Core\Reports\Entities\M1::scopePre4Visit('a', '12', $year) }}</td>
                </tr>
                <tr>
                    <td>Pregnant women given 2 doses of Tetanus Toxoid</td>
                    <td>{{ $TT2X = ShineOS\Core\Reports\Entities\M1::scopeTT2x('a', '12', $year) }}</td>
                </tr>
                <tr>
                    <td>Pregnant women given TT2 plus</td>
                    <td>{{ $TT2 = ShineOS\Core\Reports\Entities\M1::scopeTT2('a', '12', $year) }}</td>
                </tr>
                <tr>
                    <td>Preg. women given complete iron w/ folic acid supplementation</td>
                    <td>{{ $PreIron = ShineOS\Core\Reports\Entities\M1::scopePreIron('a', '12', $year) }}</td>
                </tr>
                <tr>
                    <td>Postpartum women with at least 2 postpartum visits</td>
                    <td>{{ $PP2V = ShineOS\Core\Reports\Entities\M1::scopePP2V('a', '12', $year) }}</td>
                </tr>
                <tr>
                    <td>Postpartum women given complete iron supplementation</td>
                    <td>{{ $PPIron = ShineOS\Core\Reports\Entities\M1::scopePPIron('a', '12', $year) }}</td>
                </tr>
                <tr>
                    <td>Postpartum women given Vitamin A supplementation</td>
                    <td>{{ $PPVitA = ShineOS\Core\Reports\Entities\M1::scopePPVitA('a', '12', $year) }}</td>
                </tr>
                <tr>
                    <td>PP women initiated breastfeeding w/in 1 hr. after delivery</td>
                    <td>{{ $BFeeding = ShineOS\Core\Reports\Entities\M1::scopeBFeeding('a', '12', $year) }}</td>
                </tr>
                <tr>
                    <td>Women 10-49 years old given Iron supplementation</td>
                    <td>{{ $deliveries = ShineOS\Core\Reports\Entities\M1::scopeIronSup('a', '12', $year) }}</td>
                </tr>
                <tr>
                    <td>Deliveries</td>
                    <td>{{ $deliveries = ShineOS\Core\Reports\Entities\M1::scopeDeliveries('a', '12', $year) }}</td>
                </tr>
                </tbody>
            </table><!-- /table Maternal -->
            <table class="table table-striped table-bordered table-report table-responsive">
                <thead>
                <tr>
                <th rowspan="3">FAMILY PLANNING METHOD</th>
                <th rowspan="3" width="10%">Current User<br>(Beginning Month)</th>
                <th colspan="2">Acceptors</th>
                <th rowspan="3" width="10%">Dropout<br>(Present Month)</th>
                <th rowspan="3" width="10%">Current User<br>(End of Month)</th>
                <th rowspan="3" width="10%">New Acceptors of<br>the present Month</th>
                </tr>
                <tr><th width="10%">New Acceptors</th><th width="10%">Other Acceptors</th></tr>
                <tr><th>Previous Month</th><th>Present Month</th></tr>
                </thead>

                <tbody>
                <tr>
                    <td>a. Female Sterilization/BTL</td>
                    <td>{{ $fstrbtlcub = ShineOS\Core\Reports\Entities\M1FP::doFP('FSTR/BTL','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $fstrbtlpna = ShineOS\Core\Reports\Entities\M1FP::doFP('FSTR/BTL','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $fstrbtloa = ShineOS\Core\Reports\Entities\M1FP::doFP('FSTR/BTL','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $fstrbtldo = ShineOS\Core\Reports\Entities\M1FP::doFP('FSTR/BTL','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $fstrbtlcue = ShineOS\Core\Reports\Entities\M1FP::doFP('FSTR/BTL','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $fstrbtlcna = ShineOS\Core\Reports\Entities\M1FP::doFP('FSTR/BTL','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>b. Male Sterilization/Vasectomy</td>
                    <td>{{ $vastcub = ShineOS\Core\Reports\Entities\M1FP::doFP('MSTR/VASECTOMY','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $vastpna = ShineOS\Core\Reports\Entities\M1FP::doFP('MSTR/VASECTOMY','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $vastoa = ShineOS\Core\Reports\Entities\M1FP::doFP('MSTR/VASECTOMY','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $vastdo = ShineOS\Core\Reports\Entities\M1FP::doFP('MSTR/VASECTOMY','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $vastcue = ShineOS\Core\Reports\Entities\M1FP::doFP('MSTR/VASECTOMY','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $vastcna = ShineOS\Core\Reports\Entities\M1FP::doFP('MSTR/VASECTOMY','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>c. Pills</td>
                    <td>{{ $pillcub = ShineOS\Core\Reports\Entities\M1FP::doFP('PILLS','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $pillpna = ShineOS\Core\Reports\Entities\M1FP::doFP('PILLS','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $pilloa = ShineOS\Core\Reports\Entities\M1FP::doFP('PILLS','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $pilldo = ShineOS\Core\Reports\Entities\M1FP::doFP('PILLS','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $pillcue = ShineOS\Core\Reports\Entities\M1FP::doFP('PILLS','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $pillcna = ShineOS\Core\Reports\Entities\M1FP::doFP('PILLS','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>d. IUD (Intrauterine Device)</td>
                    <td>{{ $iudcub = ShineOS\Core\Reports\Entities\M1FP::doFP('IUD','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $iudpna = ShineOS\Core\Reports\Entities\M1FP::doFP('IUD','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $iudoa = ShineOS\Core\Reports\Entities\M1FP::doFP('IUD','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $iuddo = ShineOS\Core\Reports\Entities\M1FP::doFP('IUD','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $iudcue = ShineOS\Core\Reports\Entities\M1FP::doFP('IUD','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $iudcna = ShineOS\Core\Reports\Entities\M1FP::doFP('IUD','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>e. Injectables (DMPA/CIC)</td>
                    <td>{{ $injcub = ShineOS\Core\Reports\Entities\M1FP::doFP('INJ','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $injpna = ShineOS\Core\Reports\Entities\M1FP::doFP('INJ','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $injoa = ShineOS\Core\Reports\Entities\M1FP::doFP('INJ','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $injdo = ShineOS\Core\Reports\Entities\M1FP::doFP('INJ','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $injcue = ShineOS\Core\Reports\Entities\M1FP::doFP('INJ','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $injcna = ShineOS\Core\Reports\Entities\M1FP::doFP('INJ','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>f. NFP-CM (Cervical Mucus)</td>
                    <td>{{ $nfpcmcub = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-CM','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpcmpna = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-CM','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpcmoa = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-CM','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpcmdo = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-CM','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $nfpcmcue = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-CM','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpcmcna = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-CM','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>g. NFP-BBT (Basal Body Temperature)</td>
                    <td>{{ $nfpbbtcub = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-BBT','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpbbtpna = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-BBT','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpbbtoa = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-BBT','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpbbtdo = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-BBT','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $nfpbbtcue = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-BBT','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpbbtcna = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-BBT','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>h. NFP-STM (Symptothermal Method)</td>
                    <td>{{ $nfpstmcub = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-STM','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpstmpna = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-STM','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpstmoa = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-STM','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpstmdo = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-STM','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $nfpstmcue = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-STM','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpstmcna = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-STM','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>i. NFP-SDM (Standard Days Method)</td>
                    <td>{{ $nfpsdmcub = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-SDM','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpsdmpna = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-SDM','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpsdmoa = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-SDM','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpsdmdo = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-SDM','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $nfpsdmcue = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-SDM','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfpsdmcna = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-SDM','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>j. NFP-LAM (Lactational Amenorrhea Method)</td>
                    <td>{{ $nfplamcub = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-LAM','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfplampna = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-LAM','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfplamoa = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-LAM','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfplamdo = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-LAM','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $nfplamcue = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-LAM','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $nfplamcna = ShineOS\Core\Reports\Entities\M1FP::doFP('NFP-LAM','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>k. Condom</td>
                    <td>{{ $concub = ShineOS\Core\Reports\Entities\M1FP::doFP('CON','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $conpna = ShineOS\Core\Reports\Entities\M1FP::doFP('CON','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $conoa = ShineOS\Core\Reports\Entities\M1FP::doFP('CON','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $condo = ShineOS\Core\Reports\Entities\M1FP::doFP('CON','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $concue = ShineOS\Core\Reports\Entities\M1FP::doFP('CON','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $concna = ShineOS\Core\Reports\Entities\M1FP::doFP('CON','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>l. Implant</td>
                    <td>{{ $impcub = ShineOS\Core\Reports\Entities\M1FP::doFP('IMPLANT','CU_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $imppna = ShineOS\Core\Reports\Entities\M1FP::doFP('IMPLANT','NA_begin','a', '12', $year, 'current') }}</td>
                    <td>{{ $impoa = ShineOS\Core\Reports\Entities\M1FP::doFP('IMPLANT','OA','a', '12', $year, 'current') }}</td>
                    <td>{{ $impdo = ShineOS\Core\Reports\Entities\M1FP::doFP('IMPLANT','Dropout','a', '12', $year, 'dropout') }}</td>
                    <td>{{ $impcue = ShineOS\Core\Reports\Entities\M1FP::doFP('IMPLANT','CU_end','a', '12', $year, 'current') }}</td>
                    <td>{{ $impcna = ShineOS\Core\Reports\Entities\M1FP::doFP('IMPLANT','NA_end','a', '12', $year, 'current') }}</td>
                </tr>
                <tr>
                    <td>TOTAL</td>
                    <td>{{ $totalcub = $fstrbtlcub + $vastcub + $pillcub + $iudcub + $injcub + $nfpcmcub + $nfpbbtcub + $nfpstmcub + $nfpsdmcub + $nfplamcub + $concub + $impcub }}</td>
                    <td>{{ $totalpna = $fstrbtlpna + $vastpna + $pillpna + $iudpna + $injpna + $nfpcmpna + $nfpbbtpna + $nfpstmpna + $nfpsdmpna + $nfplampna + $conpna + $imppna }}</td>
                    <td>{{ $totaloa = $fstrbtloa + $vastoa + $pilloa + $iudoa + $injoa + $nfpcmoa + $nfpbbtoa + $nfpstmoa + $nfpsdmoa + $nfplamoa + $conoa + $impoa }}</td>
                    <td>{{ $totaldo = $fstrbtldo + $vastdo + $pilldo + $iuddo + $injdo + $nfpcmdo + $nfpbbtdo + $nfpstmdo + $nfpsdmdo + $nfplamdo + $condo + $impdo }}</td>
                    <td>{{ $totalcue = $fstrbtlcue + $vastcue + $pillcue + $iudcue + $injcue + $nfpcmcue + $nfpbbtcue + $nfpstmcue + $nfpsdmcue + $nfplamcue + $concue + $impcue }}</td>
                    <td>{{ $totalcna = $fstrbtlcna + $vastcna + $pillcna + $iudcna + $injcna + $nfpcmcna + $nfpbbtcna + $nfpstmcna + $nfpsdmcna + $nfplamcna + $concna + $impcna }}</td>
                </tr>
                </tbody>
            </table><!-- /table family planning -->
            <table class="table table-striped table-bordered table-report">
                <thead>
                <tr>
                <th>CHILD CARE - Part 1</th>
                <th width="15%">Male</th>
                <th width="15%">Female</th>
                <th width="15%">Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>BCG</td>
                    <td>{{ $scopeBCGM = ShineOS\Core\Reports\Entities\M1::scopeCCare('BCG', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeBCGF = ShineOS\Core\Reports\Entities\M1::scopeCCare('BCG', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeBCGM + $scopeBCGF }}</td>
                </tr>
                <tr>
                    <td>Hepa B1 (w/in 24 hrs)</td>
                    <td>{{ $scopeHPB1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB1', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeHPB1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB1', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeHPB1M + $scopeHPB1F  }}</td>
                </tr>
                <tr>
                    <td>Hepa B1 (&gt;24 hrs)</td>
                    <td>{{ $scopeHPB2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB2', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeHPB2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB2', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeHPB2M + $scopeHPB2F  }}</td>
                </tr>
                <tr>
                    <td>Penta 1</td>
                    <td>{{ $scopePENTA1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopePENTA1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopePENTA1M + $scopePENTA1F }}</td>
                </tr>
                <tr>
                    <td>Penta 2</td>
                    <td>{{ $scopePENTA2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopePENTA2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopePENTA2M + $scopePENTA2F }}</td>
                </tr>
                <tr>
                    <td>Penta 3</td>
                    <td>{{ $scopePENTA3M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopePENTA3F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopePENTA3M + $scopePENTA3F }}</td>
                </tr>
                <tr>
                    <td>OPV 1</td>
                    <td>{{ $scopeOPV1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV1', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeOPV1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV1', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeOPV1M + $scopeOPV1F }}</td>
                </tr>
                <tr>
                    <td>OPV 2</td>
                    <td>{{ $scopeOPV2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV2', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeOPV2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV2', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeOPV2M + $scopeOPV2F }}</td>
                </tr>
                <tr>
                    <td>OPV 3</td>
                    <td>{{ $scopeOPV3M = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV3', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeOPV3F = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV3', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeOPV3M + $scopeOPV3F }}</td>
                </tr>
                <tr>
                    <td>MCV1 (AMV)</td>
                    <td>{{ $scopeMCV1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('MCV1', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeMCV1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('MCV1', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeMCV1M + $scopeMCV1F }}</td>
                </tr>
                <tr>
                    <td>MCV2 (MMR)</td>
                    <td>{{ $scopeMCV2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('MCV2', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeMCV2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('MCV2', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeMCV2M + $scopeMCV2F }}</td>
                </tr>
                <tr>
                    <td>Rota 1</td>
                    <td>{{ $scopeROTA1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA1', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeROTA1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA1', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeROTA1M + $scopeROTA1F }}</td>
                </tr>
                <tr>
                    <td>Rota 2</td>
                    <td>{{ $scopeROTA2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA2', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeROTA2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA2', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeROTA2M + $scopeROTA2F }}</td>
                </tr>
                <tr>
                    <td>Rota 3</td>
                    <td>{{ $scopeROTA3M = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA3', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeROTA3F = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA3', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeROTA3M + $scopeROTA3F }}</td>
                </tr>
                <tr>
                    <td>PCV 1</td>
                    <td>{{ $scopePCV1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV1', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopePCV1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV1', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopePCV1M + $scopePCV1F }}</td>
                </tr>
                <tr>
                    <td>PCV 2</td>
                    <td>{{ $scopePCV2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV2', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopePCV2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV2', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopePCV2M + $scopePCV2F }}</td>
                </tr>
                <tr>
                    <td>PCV 3</td>
                    <td>{{ $scopePCV3M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV3', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopePCV3F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV3', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopePCV3M + $scopePCV3F }}</td>
                </tr>
                <tr>
                    <td>Fully Immunized Child (0-11 mos)</td>
                    <td>{{ $scopeFullM = ShineOS\Core\Reports\Entities\M1::scopeFullImmune('M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeFullF = ShineOS\Core\Reports\Entities\M1::scopeFullImmune('F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeFullM + $scopeFullF }}</td>
                </tr>
                <tr>
                    <td>Completely Immunized Child (12-23 mos)</td>
                    <td>{{ $scopeComplM = ShineOS\Core\Reports\Entities\M1::scopeCompleteImmune('M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeComplF = ShineOS\Core\Reports\Entities\M1::scopeCompleteImmune('F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeComplM + $scopeComplF }}</td>
                </tr>
                <tr>
                    <td>Total Live births</td>
                    <td>{{ $scopeLBM = ShineOS\Core\Reports\Entities\M1::scopeLiveBirth('M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeLBF = ShineOS\Core\Reports\Entities\M1::scopeLiveBirth('F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeLBM + $scopeLBF }}</td>
                </tr>
                <tr>
                    <td>Child Protected at Birth</td>
                    <td>{{ $scopeCProM = ShineOS\Core\Reports\Entities\M1::scopeChildProtect('M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeCProF = ShineOS\Core\Reports\Entities\M1::scopeChildProtect('F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeCProM + $scopeCProF }}</td>
                </tr>
                <tr>
                    <td>Infant Age 6 months seen</td>
                    <td>{{ $scopeSeenM = ShineOS\Core\Reports\Entities\M1::scopeSixMonthSeen('M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeSeenF = ShineOS\Core\Reports\Entities\M1::scopeSixMonthSeen('F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeSeenM + $scopeSeenF }}</td>
                </tr>
                <tr>
                    <td>Infant exclusively breastfed until 6 months</td>
                    <td>{{ $scopeBFM = ShineOS\Core\Reports\Entities\M1::scopeBreastFeed('M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeBFF = ShineOS\Core\Reports\Entities\M1::scopeBreastFeed('F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeBFM + $scopeBFF }}</td>
                </tr>
                <thead>
                <tr>
                <th>CHILD CARE - Part 2</th>
                <th width="15%">Male</th>
                <th width="15%">Female</th>
                <th width="15%">Total</th>
                </tr>
                </thead>
                    <tr>
                        <td>Infant given complimentary food from 6-8 months</td>
                        <td>{{ $scopeCompFoodM = ShineOS\Core\Reports\Entities\M1::scopeCompFood('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeCompFoodF = ShineOS\Core\Reports\Entities\M1::scopeCompFood('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeCompFoodM + $scopeCompFoodF }}</td>
                    </tr>
                    <tr>
                        <td>Infant for newborn screening : referred</td>
                        <td>{{ $scopeNBornRefM = ShineOS\Core\Reports\Entities\M1::scopeNBornRef('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeNBornRefF = ShineOS\Core\Reports\Entities\M1::scopeNBornRef('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeNBornRefM + $scopeNBornRefF }}</td>
                    </tr>
                    <tr>
                        <td>Infant for newborn screening : done</td>
                        <td>{{ $scopeNBornDM = ShineOS\Core\Reports\Entities\M1::scopeNBornDone('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeNBornDF = ShineOS\Core\Reports\Entities\M1::scopeNBornDone('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeNBornDM + $scopeNBornDF }}</td>
                    </tr>
                    <tr>
                        <td>Infant 6-11 months old received Vitamin A</td>
                        <td>{{ $scopeVitA1M = ShineOS\Core\Reports\Entities\M1::scopeVitAFirst('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeVitA1F = ShineOS\Core\Reports\Entities\M1::scopeVitAFirst('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeVitA1M + $scopeVitA1F }}</td>
                    </tr>
                    <tr>
                        <td>Chidren 12-59 months old received Vitamin A</td>
                        <td>{{ $scopeVitA2M = ShineOS\Core\Reports\Entities\M1::scopeVitASecond('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeVitA2F = ShineOS\Core\Reports\Entities\M1::scopeVitASecond('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeVitA2M + $scopeVitA2F }}</td>
                    </tr>
                    <tr>
                        <td>Infant 6-11 months old received Iron</td>
                        <td>{{ $scopeIronAM = ShineOS\Core\Reports\Entities\M1::scopeIronA('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeIronAF = ShineOS\Core\Reports\Entities\M1::scopeIronA('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeIronAM + $scopeIronAF }}</td>
                    </tr>
                    <tr>
                        <td>Children 12-59 months old received Iron</td>
                        <td>{{ $scopeIronBM = ShineOS\Core\Reports\Entities\M1::scopeIronB('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeIronBF = ShineOS\Core\Reports\Entities\M1::scopeIronB('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeIronBM + $scopeIronBF }}</td>
                    </tr>
                    <tr>
                        <td>Infant 6-11 months received MNP</td>
                        <td>{{ $scopeMNPAM = ShineOS\Core\Reports\Entities\M1::scopeMNPA('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMNPAF = ShineOS\Core\Reports\Entities\M1::scopeMNPA('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMNPAM + $scopeMNPAF }}</td>
                    </tr>
                    <tr>
                        <td>Children 12-23 months received MNP</td>
                        <td>{{ $scopeMNPBM = ShineOS\Core\Reports\Entities\M1::scopeMNPB('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMNPBF = ShineOS\Core\Reports\Entities\M1::scopeMNPB('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMNPBM + $scopeMNPBF }}</td>
                    </tr>
                    <tr>
                        <td>Sick Children 6-11 months seen</td>
                        <td>{{ $scopeSickAM = ShineOS\Core\Reports\Entities\M1::scopeSickSeen(6,11,'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSickAF = ShineOS\Core\Reports\Entities\M1::scopeSickSeen(6,11,'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSickAM + $scopeSickAF }}</td>
                    </tr>
                    <tr>
                        <td>Sick Children 6-11 months received Vitamin A</td>
                        <td>{{ $scopeSickVitAAM = ShineOS\Core\Reports\Entities\M1::scopeSickVitA(6,11,'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSickVitAAF = ShineOS\Core\Reports\Entities\M1::scopeSickVitA(6,11,'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSickVitAAM + $scopeSickVitAAF }}</td>
                    </tr>
                    <tr>
                        <td>Sick Children 12-59 months seen</td>
                        <td>{{ $scopeSickBM = ShineOS\Core\Reports\Entities\M1::scopeSickSeen(12,59,'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSickBF = ShineOS\Core\Reports\Entities\M1::scopeSickSeen(12,59,'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSickBM + $scopeSickBF }}</td>
                    </tr>
                    <tr>
                        <td>Sick Children 12-59 months received Vitamin A</td>
                        <td>{{ $scopeSickVitABM = ShineOS\Core\Reports\Entities\M1::scopeSickVitA(12,59,'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSickVitABF = ShineOS\Core\Reports\Entities\M1::scopeSickVitA(12,59,'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSickVitABM + $scopeSickVitABF }}</td>
                    </tr>
                    <tr>
                        <td>Children 12-59 mos. old given de-worming tablet/syrup</td>
                        <td>{{ $scopeDeWormM = ShineOS\Core\Reports\Entities\M1::scopeDeWorm('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeDeWormF = ShineOS\Core\Reports\Entities\M1::scopeDeWorm('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeDeWormM + $scopeDeWormF }}</td>
                    </tr>
                    <tr>
                        <td>Infant 2-5 mos w/ Low Birth Weight seen</td>
                        <td>{{ $scopeLowWtM = ShineOS\Core\Reports\Entities\M1::scopeLowWt(2, 5, 'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeLowWtF = ShineOS\Core\Reports\Entities\M1::scopeLowWt(2, 5, 'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeLowWtM + $scopeLowWtF }}</td>
                    </tr>
                    <tr>
                        <td>Infant 2-5 mos w/ LBW received full dose iron</td>
                        <td>{{ $scopeLowWtIronM = ShineOS\Core\Reports\Entities\M1::scopeLowWtIron(2, 5, 'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeLowWtIronF = ShineOS\Core\Reports\Entities\M1::scopeLowWtIron(2, 5, 'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeLowWtIronM + $scopeLowWtIronF }}</td>
                    </tr>
                    <tr>
                        <td>Anemic Children 6-11 months old seen</td>
                        <td>{{ $scopeAnemia611M = ShineOS\Core\Reports\Entities\M1::scopeAnemia(06, 11, 'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeAnemia611F = ShineOS\Core\Reports\Entities\M1::scopeAnemia(06, 11, 'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeAnemia611M + $scopeAnemia611F }}</td>
                    </tr>
                    <tr>
                        <td>Anemic Children 6-11 mos received full dose iron</td>
                        <td>{{ $scopeAnemia1259WithIronM = ShineOS\Core\Reports\Entities\M1::scopeAnemiaWithIron(06, 11, 'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeAnemia1259WithIronF = ShineOS\Core\Reports\Entities\M1::scopeAnemiaWithIron(06, 11, 'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeAnemia1259WithIronM + $scopeAnemia1259WithIronF }}</td>
                    </tr>
                    <tr>
                        <td>Anemic Children 12-59 months old seen</td>
                        <td>{{ $scopeAnemia1259M = ShineOS\Core\Reports\Entities\M1::scopeAnemia(12, 59, 'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeAnemia1259F = ShineOS\Core\Reports\Entities\M1::scopeAnemia(12, 59, 'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeAnemia1259M + $scopeAnemia1259F }}</td>
                    </tr>
                    <tr>
                        <td>Anemic Children 12-59 mos received full dose iron</td>
                        <td>{{ $scopeAnemia1259WithIronM = ShineOS\Core\Reports\Entities\M1::scopeAnemiaWithIron(12, 59, 'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeAnemia1259WithIronF = ShineOS\Core\Reports\Entities\M1::scopeAnemiaWithIron(12, 59, 'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeAnemia1259WithIronM + $scopeAnemia1259WithIronF }}</td>
                    </tr>
                    <tr>
                        <td>Diarrhea cases 0-59 months old seen</td>
                        <td>{{ $scopeDiarrheaM = ShineOS\Core\Reports\Entities\M1::scopeDiarrhea(0, 59, 'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeDiarrheaF = ShineOS\Core\Reports\Entities\M1::scopeDiarrhea(0, 59, 'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeDiarrheaM + $scopeDiarrheaF }}</td>
                    </tr>
                    <tr>
                        <td>Diarrhea cases 0-59 mos old received ORS</td>
                        <td>{{ $scopeDiarrheaWtORSM = ShineOS\Core\Reports\Entities\M1::scopeDiarrheaWtORS(0, 59, 'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeDiarrheaWtORSF = ShineOS\Core\Reports\Entities\M1::scopeDiarrheaWtORS(0, 59, 'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeDiarrheaWtORSM + $scopeDiarrheaWtORSF }}</td>
                    </tr>
                    <tr>
                        <td>Diarrhea 0-59 mos received ORS/ORT w/ zinc</td>
                        <td>{{ $scopeDiarrheaWtORSZincM = ShineOS\Core\Reports\Entities\M1::scopeDiarrheaWtORSZinc(0, 59, 'M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeDiarrheaWtORSZincF = ShineOS\Core\Reports\Entities\M1::scopeDiarrheaWtORSZinc(0, 59, 'F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeDiarrheaWtORSZincM + $scopeDiarrheaWtORSZincF }}</td>
                    </tr>
                    <tr>
                        <td>Pneumonia cases 0-59 months old</td>
                        <td>{{ $scopePneumoniaM = ShineOS\Core\Reports\Entities\M1::scopePneumonia('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopePneumoniaF = ShineOS\Core\Reports\Entities\M1::scopePneumonia('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopePneumoniaM + $scopePneumoniaF }}</td>
                    </tr>
                    <tr>
                        <td>Pneumonia cases 0-59 mos. old completed Tx</td>
                        <td>{{ $scopePneumoniaTM = ShineOS\Core\Reports\Entities\M1::scopePneumoniaTreat('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopePneumoniaTF = ShineOS\Core\Reports\Entities\M1::scopePneumoniaTreat('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopePneumoniaTM + $scopePneumoniaTF }}</td>
                    </tr>

                </tbody>
            </table><!-- /.table child care -->
            <table class="table table-striped table-bordered table-report">
                <thead>
                <tr>
                <th>MALARIA</th>
                <th width="15%">Male</th>
                <th width="15%">Female</th>
                <th width="15%">Total</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Total Population</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Population at Risk</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Annual parasite incidence</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Confirmed Malaria Cases 
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;5 yrs. old</td>
                        <td>{{ $scopeMalariaLessFiveM = ShineOS\Core\Reports\Entities\M1::scopeMalariaLessFive('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaLessFiveF = ShineOS\Core\Reports\Entities\M1::scopeMalariaLessFive('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaLessFiveM + $scopeMalariaLessFiveF }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&gt;=5 yrs. old</td>
                        <td>{{ $scopeMalariaGreaterFiveM = ShineOS\Core\Reports\Entities\M1::scopeMalariaGreaterFive('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaGreaterFiveF = ShineOS\Core\Reports\Entities\M1::scopeMalariaGreaterFive('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaGreaterFiveM + $scopeMalariaGreaterFiveF }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pregnant</td>
                        <td bgcolor="#A9A9A9"></td>
                        <td>{{ $scopeMalariaGreaterPregnant = ShineOS\Core\Reports\Entities\M1::scopeMalariaPregnant('a', '12', $year) }}</td>
                        <td bgcolor="#A9A9A9"></td>
                    </tr>
                    <tr>
                        <td>Confirmed Malaria Cases by Species</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Falciparum</td>
                        <td>{{ $scopeMalariaFalciparumM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Falciparum','M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaFalciparumF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Falciparum','F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaFalciparumM + $scopeMalariaFalciparumF }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Vivax</td>
                        <td>{{ $scopeMalariaVivaxM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Vivax','M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaVivaxF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Vivax','F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaVivaxM + $scopeMalariaVivaxF }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Ovale</td>
                        <td>{{ $scopeMalariaOvaleM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Ovale','M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaOvaleF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Ovale','F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaOvaleM + $scopeMalariaOvaleF }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Malariae</td>
                        <td>{{ $scopeMalariaMalariaeM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Malariae','M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaMalariaeF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Malariae','F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaMalariaeM + $scopeMalariaMalariaeF }}</td>
                    </tr>
                    <tr>
                        <td>By Method</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Slide</td>
                        <td bgcolor="#A9A9A9"></td>
                        <td bgcolor="#A9A9A9"></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RDT</td>
                        <td bgcolor="#A9A9A9"></td>
                        <td bgcolor="#A9A9A9"></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Malaria Deaths</td>
                        <td>{{ $scopeMalariaDeathM = ShineOS\Core\Reports\Entities\M1::scopeMalariaDeaths('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaDeathF = ShineOS\Core\Reports\Entities\M1::scopeMalariaDeaths('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeMalariaDeathM + $scopeMalariaDeathF }}</td>
                    </tr>
                    <tr>
                        <td>Number of LLIN given</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table><!-- /table malaria -->
            <table class="table table-striped table-bordered table-report">
                <thead>
                <tr>
                <th>TUBERCULOSIS</th>
                <th width="15%">Male</th>
                <th width="15%">Female</th>
                <th width="15%">Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>TB symptomatics who underwent DSSM</td>
                    <td>{{ $scopeTBDSSMM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSM('M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSM('F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMM + $scopeTBDSSMF }}</td>
                </tr>
                <tr>
                    <td>Smear Positive discovered and identified</td>
                    <td>{{ $scopeTBDSSMPosM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPos('M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPos('F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosM + $scopeTBDSSMPosF }}</td>
                </tr>
                <tr>
                    <td>New Smear (+) cases initiated tx &amp; registered</td>
                    <td>{{ $scopeTBDSSMPosRegisteredM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('N','M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('N','F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredM + $scopeTBDSSMPosRegisteredF }}</td>
                </tr>

                <tr>
                    <td>New Smear (+) cases cured</td>
                    <td>{{ $scopeTBDSSMPosCuredM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('N', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosCuredF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('N', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosCuredM + $scopeTBDSSMPosCuredF }}</td>
                </tr>
                <tr>
                    <td>Smear (+) retreatment cases cured</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('ALLRETREATMENT', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('ALLRETREATMENT', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredM + $scopeTBDSSMPosRetreatmentCuredF }}</td>
                </tr>
                <tr>
                    <td>Smear (+) retreatment cases initiated tx &amp; registered</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Relapse</td>
                    <td>{{ $scopeTBDSSMPosRegisteredRM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('R','M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredRF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('R','F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredRM + $scopeTBDSSMPosRegisteredRF }}</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Treatment Failure</td>
                    <td>{{ $scopeTBDSSMPosRegisteredTAFM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('TAF','M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredTAFF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('TAF','F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredTAFM + $scopeTBDSSMPosRegisteredTAFF }}</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Return After Default</td>
                    <td>{{ $scopeTBDSSMPosRegisteredRADM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('RAD','M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredRADF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('RAD','F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredRADM + $scopeTBDSSMPosRegisteredRADF }}</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Other type of TB</td>
                    <td>{{ $scopeTBDSSMPosRegisteredOTHM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('OTH','M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredOTHF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('OTH','F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredOTHM + $scopeTBDSSMPosRegisteredOTHF }}</td>
                </tr>
                <tr>
                    <td>No of smear (+) retreatment cured</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Relapse</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredRM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('R', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredRF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('R', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredRM + $scopeTBDSSMPosRetreatmentCuredRF }}</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Treatment Failure</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredTAFM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('TAF', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredTAFF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('TAF', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredTAFM + $scopeTBDSSMPosRetreatmentCuredTAFF }}</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Return After Default</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredRADM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('RAD', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredRADF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('RAD', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRetreatmentCuredRADM + $scopeTBDSSMPosRetreatmentCuredRADF }}</td>
                </tr>
                <tr>
                    <td>Total No. of TB cases (all forms) initiated treatment</td>
                    <td>{{ $scopeTBDSSMPosRegisteredALLM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('ALL', 'M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredALLF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('ALL', 'F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBDSSMPosRegisteredALLM + $scopeTBDSSMPosRegisteredALLF }}</td>
                </tr>
                <tr>
                    <td>TB all forms identified</td>
                    <td>{{ $scopeTBM = ShineOS\Core\Reports\Entities\M1::scopeTB('M', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBF = ShineOS\Core\Reports\Entities\M1::scopeTB('F', 'a', '12', $year) }}</td>
                    <td>{{ $scopeTBM + $scopeTBF }}</td>
                </tr>
                <tr>
                    <td>Cases Detection Rate</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
                </tbody>
            </table><!-- /table tuberculosis -->
            <table class="table table-striped table-bordered table-report">
                <thead>
                <tr>
                <th>SCHISTOSOMIASIS</th>
                <th width="15%">Male</th>
                <th width="15%">Female</th>
                <th width="15%">Total</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>No. of symptomatic case</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>No. of cases examined</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>No. of positive cases</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;Low intensity</td>
                        <td>{{ $scopeSchistosomiasisLowM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Low Intensity','M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisLowF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Low Intensity','F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisLowM + $scopeSchistosomiasisLowF }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;Medium intensity</td>
                        <td>{{ $scopeSchistosomiasisMediumM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Medium Intensity','M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisMediumF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Medium Intensity','F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisMediumM + $scopeSchistosomiasisMediumF }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;High intensity</td>
                        <td>{{ $scopeSchistosomiasisHighM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - High Intensity','M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisHighF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - High Intensity','F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisHighM + $scopeSchistosomiasisHighF }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;Unknown intensity</td>
                        <td>{{ $scopeSchistosomiasisHighM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Intensity Unknown','M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisHighF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Intensity Unknown','F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisHighM + $scopeSchistosomiasisHighF }}</td>
                    </tr>
                    <tr>
                        <td>No. of cases treated</td>
                        <td>{{ $scopeSchistosomiasisTreatedM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisWtTreatment('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisTreatedF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisWtTreatment('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisTreatedM + $scopeSchistosomiasisTreatedF }}</td>
                    </tr>
                    <tr>
                        <td>No. of complicated cases</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>No. of complicated cases referred</td>
                        <td>{{ $scopeSchistosomiasisComplicatedM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisComplicated('M', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisComplicatedF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisComplicated('F', 'a', '12', $year) }}</td>
                        <td>{{ $scopeSchistosomiasisComplicatedM + $scopeSchistosomiasisComplicatedF }}</td>
                    </tr>
                </tbody>
            </table><!-- /table SCHISTOSOMIASIS -->
            <table class="table table-striped table-bordered table-report">
                <thead>
                <tr>
                <th>FILARIASIS</th>
                <th width="15%">Male</th>
                <th width="15%">Female</th>
                <th width="15%">Total</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>No. of cases w/Hydrocele, Lymphedema, Elephantasis and Chyluria</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>No. of cases examined</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Clinical Rate</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>No. of cases Examined found for MF</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Average MFD</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Eligible population given MDA (94.6% of TP)</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Total Population given MDA</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table><!-- /table FILARIASIS -->
            <table class="table table-striped table-bordered table-report">
                <thead>
                <tr>
                <th>LEPROSY</th>
                <th width="15%">Male</th>
                <th width="15%">Female</th>
                <th width="15%">Total</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Total Population</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Total No. of Leprosy cases (undergoing treatment)</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>No. of Newly detected Leprosy cases (&lt;15yo, Grade 2 disability)</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>No. of Leprosy cases cured</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table><!-- /table LEPROSY -->
            <table class="table table-striped table-bordered table-report">
                <thead>
                <tr>
                <th>STI SURVEILLANCE</th>
                <th width="15%">Male</th>
                <th width="15%">Female</th>
                <th width="15%">Total</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>No. of pregnant women seen</td>
                        <td bgcolor="#A9A9A9"></td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>No. of pregnant women tested for Syphillis</td>
                        <td bgcolor="#A9A9A9"></td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>No. of pregnant women positive for Syphillis</td>
                        <td bgcolor="#A9A9A9"></td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>No. of pregnant women given Penicillin</td>
                        <td bgcolor="#A9A9A9"></td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table><!-- /table STI SURVEILLANCE -->
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
</div>
@stop
