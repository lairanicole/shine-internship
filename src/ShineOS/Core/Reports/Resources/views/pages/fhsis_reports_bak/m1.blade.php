<?php
$population = 0;

if(isset($geodata))
{
    $population = $geodata->population;
}
?>
@extends('reports::layouts.fhsis_master')
@section('heads')
<link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
{!! HTML::style('public/dist/plugins/stickytableheader/css/component.css') !!}

<style>
    tbody tr.mast {
        background-color: #FFFFFF !important;
    }
    .mast .thleft {
        background-color: #FFFFFF !important;
    }
    .mast .thleft {
        width: 25%;
    }
    .mastLogo, .mastCode {
        text-align: center;
    }
    .mastCode h1 {
        color:  #555555;
        font-family: 'Work Sans', sans-serif;
        font-size: 100px;
        font-weight: 900;
        margin: -20px;
    }
    .mastCode h2 {
        color:  #555555;
        font-family: 'Work Sans', sans-serif;
        font-size: 60px;
        font-weight: 900;
        margin: -22px;
    }
</style>

@stop
@section('reportGroup')FHSIS @stop
@section('reportTitle')FHSIS M1 @stop
@section('content')

<!--NOTE:: SEPARATE PORTIONS-->
<div class="row">
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Program Report M1</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-print"></i></button>
          </div><!-- /.box-tool -->
        </div><!-- /.box-header -->
        <div class="box-body text-center">
            <table class="table table-report">
                <tbody>
                    <tr>
                        <th class="thleft">FHSIS Report Month/Year</th>
                        <td colspan="2">
                            {!! Form::open(array( 'url'=>'reports/fhsis/m1/', 'id'=>'dateFilter', 'name'=>'dateFilter', 'class'=>'form-horizontal' )) !!}
                                <label class="col-sm-1 control-label">Month</label>
                                <div class="col-sm-2">
                                    <input type="hidden" value="m" name="range">
                                    <select name="month" class="form-control" id="month">
                                        <option value="" selected="selected"></option>
                                        <option value="1" @if($month == '1') selected='selected'@endif >January</option>
                                        <option value="2" @if($month == '2') selected='selected'@endif >February</option>
                                        <option value="3" @if($month == '3') selected='selected'@endif >March</option>
                                        <option value="4" @if($month == '4') selected='selected'@endif >April</option>
                                        <option value="5" @if($month == '5') selected='selected'@endif >May</option>
                                        <option value="6" @if($month == '6') selected='selected'@endif >June</option>
                                        <option value="7" @if($month == '7') selected='selected'@endif >July</option>
                                        <option value="8" @if($month == '8') selected='selected'@endif >August</option>
                                        <option value="9" @if($month == '9') selected='selected'@endif >September</option>
                                        <option value="10" @if($month == '10') selected='selected'@endif >October</option>
                                        <option value="11" @if($month == '11') selected='selected'@endif >November</option>
                                        <option value="12" @if($month == '12') selected='selected'@endif >December</option>
                                    </select>
                                </div>
                                <label class="col-sm-1 control-label">Year</label>
                                <div class="col-sm-2">
                                    <?php $thisyear = date('Y'); ?>
                                    <select name="year" class="form-control" id="year">
                                        @for( $y=$thisyear-5; $y<=$thisyear; $y++)
                                        <option @if($year == $y) selected='selected'@endif >{{ $y }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <label class="col-sm-1 control-label">Barangay</label>
                                <div class="col-sm-3">
                                    <select name="barangay" class="form-control" id="barangay">
                                        @foreach($brgys as $brgy)
                                        <option value="{{$brgy->barangay_code}}" @if($barangay == $brgy->barangay_code) selected='selected' @endif >{{ $brgy->barangay_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <input type="submit" class="btn btn-primary" value="View">
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    <tr><th class="thleft">Name of BHS</th><td colspan="2"></td></tr>
                    <tr><th class="thleft">Barangay</th><td colspan="2">{{ getBrgyName($barangay) }}</td></tr>
                    <tr><th class="thleft">City/Municipality of</th><td colspan="2">{{ getCityName($facility->facility_contact->city) }}</td></tr>
                    <tr><th class="thleft">Province of</th><td colspan="2">{{ getProvinceName($facility->facility_contact->province) }}</td></tr>
                    <tr><th class="thleft">Projected Population of the Year</th><td colspan="2">{{ $population }}</td></tr>
                    <tr class="mast">
                        <td width="250" class="mastLogos">
                            <img src="{{ asset('public/dist/img/UHC_logo.jpg') }}" height="100" /> <img src="{{ asset('public/dist/img/doh.png') }}" height="100" />
                        </td>
                        <td>
                            <table>
                                <tr><td class="thleft">Name of BHS</td><td colspan="2"></td></tr>
                                <tr><td class="thleft">Barangay</td><td colspan="2">{{ getBrgyName($barangay) }}</td></tr>
                                <tr><td class="thleft">City/Municipality of</td><td colspan="2">{{ getCityName($facility->facility_contact->city) }}</td></tr>
                                <tr><td class="thleft">Province of</td><td colspan="2">{{ getProvinceName($facility->facility_contact->province) }}</td></tr>
                                <tr><td class="thleft">Projected Population of the Year</td><td colspan="2">{{ $population }}</td></tr>
                            </table>
                        </td>
                        <td width="200" class="mastCode">
                            <h1>M1</h1>
                            <h2>Brgy</h2>
                        </td>
                    </tr>
                </tbody>
            </table><!-- /table details -->

            <table class="table table-striped table-bordered table-report">
                <thead>
                <tr>
                    <th width="55%">MATERNAL CARE</th>
                    <th>NO.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Pregnant women with 4 or more Prenatal visits</td>
                    <td>{{ $Pre4Visit = ShineOS\Core\Reports\Entities\M1::scopePre4Visit('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Pregnant women given 2 doses of Tetanus Toxoid</td>
                    <td>{{ $TT2X = ShineOS\Core\Reports\Entities\M1::scopeTT2x('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Pregnant women given TT2 plus</td>
                    <td>{{ $TT2 = ShineOS\Core\Reports\Entities\M1::scopeTT2('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Pregnant women given complete iron w/ folic acid supplementation</td>
                    <td>{{ $PreIron = ShineOS\Core\Reports\Entities\M1::scopePreIron('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Postpartum women with at least 2 postpartum visits</td>
                    <td>{{ $PP2V = ShineOS\Core\Reports\Entities\M1::scopePP2V('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Postpartum women given complete iron supplementation</td>
                    <td>{{ $PPIron = ShineOS\Core\Reports\Entities\M1::scopePPIron('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Postpartum women given Vitamin A supplementation</td>
                    <td>{{ $PPVitA = ShineOS\Core\Reports\Entities\M1::scopePPVitA('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>PP women initiated breastfeeding w/in 1 hr. after delivery</td>
                    <td>{{ $BFeeding = ShineOS\Core\Reports\Entities\M1::scopeBFeeding('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Women 10-49 years old given Iron supplementation</td>
                    <td>{{ $deliveries = ShineOS\Core\Reports\Entities\M1::scopeIronSup('m', $month, $year, $barangay) }}</td>
                </tr>
                <tr>
                    <td>Deliveries</td>
                    <td>{{ $deliveries = ShineOS\Core\Reports\Entities\M1::scopeDeliveries('m', $month, $year, $barangay) }}</td>
                </tr>
                </tbody>
            </table><!-- /table Maternal -->
            <table class="table table-striped table-bordered table-report table-responsive">
                <thead>
                <tr>
                <th rowspan="3">FAMILY PLANNING METHOD</th>
                <th rowspan="3" width="10%">Current User<br>(Beginning Month)</th>
                <th colspan="2">Acceptors</th>
                <th rowspan="3" width="10%">Dropout<br>(Present Month)</th>
                <th rowspan="3" width="10%">Current User<br>(End of Month)</th>
                <th rowspan="3" width="10%">New Acceptors of<br>the present Month</th>
                </tr>
                <tr><th width="10%">New Acceptors</th><th width="10%">Other Acceptors</th></tr>
                <tr><th>Previous Month</th><th>Present Month</th></tr>
                </thead>

                <tbody>
                <tr>
                    <td>a. Female Sterilization/BTL</td>
                    <td>{{$fstrbtlcub=$fp['currentMethod']['FSTR/BTL']['CU_begin']}}</td>
                    <td>{{$fstrbtlpna=$fp['currentMethod']['FSTR/BTL']['NA_begin']}}</td>
                    <td>{{$fstrbtloa=$fp['currentMethod']['FSTR/BTL']['OA']}}</td>
                    <td>{{$fstrbtldo=$fp['previousMethod']['FSTR/BTL']['Dropout']}}</td>
                    <td>{{$fstrbtlcue=$fp['currentMethod']['FSTR/BTL']['CU_end']}}</td>
                    <td>{{$fstrbtlcna=$fp['currentMethod']['FSTR/BTL']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>b. Male Sterilization/Vasectomy</td>
                    <td>{{$vastcub=$fp['currentMethod']['MSTR/VASECTOMY']['CU_begin']}}</td>
                    <td>{{$vastpna=$fp['currentMethod']['MSTR/VASECTOMY']['NA_begin']}}</td>
                    <td>{{$vastoa=$fp['currentMethod']['MSTR/VASECTOMY']['OA'] }}</td>
                    <td>{{$vastdo=$fp['previousMethod']['MSTR/VASECTOMY']['Dropout']}}</td>
                    <td>{{$vastcue=$fp['currentMethod']['MSTR/VASECTOMY']['CU_end']}}</td>
                    <td>{{$vastcna=$fp['currentMethod']['MSTR/VASECTOMY']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>c. Pills</td>
                    <td>{{$pillcub=$fp['currentMethod']['PILLS']['CU_begin']}}</td>
                    <td>{{$pillpna=$fp['currentMethod']['PILLS']['NA_begin']}}</td>
                    <td>{{$pilloa=$fp['currentMethod']['PILLS']['OA']}}</td>
                    <td>{{$pilldo=$fp['previousMethod']['PILLS']['Dropout']}}</td>
                    <td>{{$pillcue=$fp['currentMethod']['PILLS']['CU_end']}}</td>
                    <td>{{$pillcna=$fp['currentMethod']['PILLS']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>d. IUD (Intrauterine Device)</td>
                    <td>{{$iudcub=$fp['currentMethod']['IUD']['CU_begin']}}</td>
                    <td>{{$iudpna=$fp['currentMethod']['IUD']['NA_begin']}}</td>
                    <td>{{$iudoa=$fp['currentMethod']['IUD']['OA']}}</td>
                    <td>{{$iuddo=$fp['previousMethod']['IUD']['Dropout']}}</td>
                    <td>{{$iudcue=$fp['currentMethod']['IUD']['CU_end']}}</td>
                    <td>{{$iudcna=$fp['currentMethod']['IUD']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>e. Injectables (DMPA/CIC)</td>
                    <td>{{$injcub=$fp['currentMethod']['INJ']['CU_begin']}}</td>
                    <td>{{$injpna=$fp['currentMethod']['INJ']['NA_begin']}}</td>
                    <td>{{$injoa=$fp['currentMethod']['INJ']['OA']}}</td>
                    <td>{{$injdo=$fp['previousMethod']['INJ']['Dropout']}}</td>
                    <td>{{$injcue=$fp['currentMethod']['INJ']['CU_end']}}</td>
                    <td>{{$injcna=$fp['currentMethod']['INJ']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>f. NFP-CM (Cervical Mucus)</td>
                    <td>{{$nfpcmcub=$fp['currentMethod']['NFP-CM']['CU_begin']}}</td>
                    <td>{{$nfpcmpna=$fp['currentMethod']['NFP-CM']['NA_begin']}}</td>
                    <td>{{$nfpcmoa=$fp['currentMethod']['NFP-CM']['OA']}}</td>
                    <td>{{$nfpcmdo=$fp['previousMethod']['NFP-CM']['Dropout']}}</td>
                    <td>{{$nfpcmcue=$fp['currentMethod']['NFP-CM']['CU_end']}}</td>
                    <td>{{$nfpcmcna=$fp['currentMethod']['NFP-CM']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>g. NFP-BBT (Basal Body Temperature)</td>
                    <td>{{$nfpbbtcub=$fp['currentMethod']['NFP-BBT']['CU_begin']}}</td>
                    <td>{{$nfpbbtpna=$fp['currentMethod']['NFP-BBT']['NA_begin']}}</td>
                    <td>{{$nfpbbtoa=$fp['currentMethod']['NFP-BBT']['OA']}}</td>
                    <td>{{$nfpbbtdo=$fp['previousMethod']['NFP-BBT']['Dropout']}}</td>
                    <td>{{$nfpbbtcue=$fp['currentMethod']['NFP-BBT']['CU_end']}}</td>
                    <td>{{$nfpbbtcna=$fp['currentMethod']['NFP-BBT']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>h. NFP-STM (Symptothermal Method)</td>
                    <td>{{$nfpstmcub=$fp['currentMethod']['NFP-STM']['CU_begin']}}</td>
                    <td>{{$nfpstmpna=$fp['currentMethod']['NFP-STM']['NA_begin']}}</td>
                    <td>{{$nfpstmoa=$fp['currentMethod']['NFP-STM']['OA']}}</td>
                    <td>{{$nfpstmdo=$fp['previousMethod']['NFP-STM']['Dropout']}}</td>
                    <td>{{$nfpstmcue=$fp['currentMethod']['NFP-STM']['CU_end']}}</td>
                    <td>{{$nfpstmcna=$fp['currentMethod']['NFP-STM']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>i. NFP-SDM (Standard Days Method)</td>
                    <td>{{$nfpsdmcub=$fp['currentMethod']['NFP-SDM']['CU_begin']}}</td>
                    <td>{{$nfpsdmpna=$fp['currentMethod']['NFP-SDM']['NA_begin']}}</td>
                    <td>{{$nfpsdmoa=$fp['currentMethod']['NFP-SDM']['OA']}}</td>
                    <td>{{$nfpsdmdo=$fp['previousMethod']['NFP-SDM']['Dropout']}}</td>
                    <td>{{$nfpsdmcue=$fp['currentMethod']['NFP-SDM']['CU_end']}}</td>
                    <td>{{$nfpsdmcna=$fp['currentMethod']['NFP-SDM']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>j. NFP-LAM (Lactational Amenorrhea Method)</td>
                    <td>{{$nfplamcub=$fp['currentMethod']['NFP-LAM']['CU_begin']}}</td>
                    <td>{{$nfplampna=$fp['currentMethod']['NFP-LAM']['NA_begin']}}</td>
                    <td>{{$nfplamoa=$fp['currentMethod']['NFP-LAM']['OA']}}</td>
                    <td>{{$nfplamdo=$fp['previousMethod']['NFP-LAM']['Dropout']}}</td>
                    <td>{{$nfplamcue=$fp['currentMethod']['NFP-LAM']['CU_end']}}</td>
                    <td>{{$nfplamcna=$fp['currentMethod']['NFP-LAM']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>k. Condom</td>
                    <td>{{$concub=$fp['currentMethod']['CON']['CU_begin']}}</td>
                    <td>{{$conpna=$fp['currentMethod']['CON']['NA_begin']}}</td>
                    <td>{{$conoa=$fp['currentMethod']['CON']['OA']}}</td>
                    <td>{{$condo=$fp['previousMethod']['CON']['Dropout']}}</td>
                    <td>{{$concue=$fp['currentMethod']['CON']['CU_end']}}</td>
                    <td>{{$concna=$fp['currentMethod']['CON']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>l. Implant</td>
                    <td>{{$impcub=$fp['currentMethod']['IMPLANT']['CU_begin']}}</td>
                    <td>{{$imppna=$fp['currentMethod']['IMPLANT']['NA_begin']}}</td>
                    <td>{{$impoa=$fp['currentMethod']['IMPLANT']['OA']}}</td>
                    <td>{{$impdo=$fp['previousMethod']['IMPLANT']['Dropout']}}</td>
                    <td>{{$impcue=$fp['currentMethod']['IMPLANT']['CU_end']}}</td>
                    <td>{{$impcna=$fp['currentMethod']['IMPLANT']['NA_end']}}</td>
                </tr>
                <tr>
                    <td>TOTAL</td>
                    <td>{{ $totalcub = $fstrbtlcub + $vastcub + $pillcub + $iudcub + $injcub + $nfpcmcub + $nfpbbtcub + $nfpstmcub + $nfpsdmcub + $nfplamcub + $concub + $impcub }}</td>
                    <td>{{ $totalpna = $fstrbtlpna + $vastpna + $pillpna + $iudpna + $injpna + $nfpcmpna + $nfpbbtpna + $nfpstmpna + $nfpsdmpna + $nfplampna + $conpna + $imppna }}</td>
                    <td>{{ $totaloa = $fstrbtloa + $vastoa + $pilloa + $iudoa + $injoa + $nfpcmoa + $nfpbbtoa + $nfpstmoa + $nfpsdmoa + $nfplamoa + $conoa + $impoa }}</td>
                    <td>{{ $totaldo = $fstrbtldo + $vastdo + $pilldo + $iuddo + $injdo + $nfpcmdo + $nfpbbtdo + $nfpstmdo + $nfpsdmdo + $nfplamdo + $condo + $impdo }}</td>
                    <td>{{ $totalcue = $fstrbtlcue + $vastcue + $pillcue + $iudcue + $injcue + $nfpcmcue + $nfpbbtcue + $nfpstmcue + $nfpsdmcue + $nfplamcue + $concue + $impcue }}</td>
                    <td>{{ $totalcna = $fstrbtlcna + $vastcna + $pillcna + $iudcna + $injcna + $nfpcmcna + $nfpbbtcna + $nfpstmcna + $nfpsdmcna + $nfplamcna + $concna + $impcna }}</td>
                </tr>
                </tbody>
            </table><!-- /table family planning -->

            <table class="table table-report table-responsive page-break">
                <tr>
                    <td width="50%" style="padding:0;">
                        <table class="table table-striped table-bordered table-report">
                        <thead>
                        <tr>
                        <th>CHILD CARE - Part 1</th>
                        <th width="15%">Male</th>
                        <th width="15%">Female</th>
                        <th width="15%">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>BCG</td>
                            <td>{{ $scopeBCGM = ShineOS\Core\Reports\Entities\M1::scopeCCare('BCG', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeBCGF = ShineOS\Core\Reports\Entities\M1::scopeCCare('BCG', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeBCGM + $scopeBCGF }}</td>
                        </tr>
                        <tr>
                            <td>Hepa B1 (w/in 24 hrs)</td>
                            <td>{{ $scopeHPB1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB1_win24', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeHPB1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB1_win24', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeHPB1M + $scopeHPB1F  }}</td>
                        </tr>
                        <tr>
                            <td>Hepa B1 (&gt;24 hrs)</td>
                            <td>{{ $scopeHPB2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB1_aft24', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeHPB2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB1_aft24', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeHPB2M + $scopeHPB2F  }}</td>
                        </tr>
                        <tr>
                            <td>Hepa B2</td>
                            <td>{{ $scopeHPB2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB2', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeHPB2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB2', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeHPB2M + $scopeHPB2F  }}</td>
                        </tr>
                        <tr>
                            <td>Hepa B3</td>
                            <td>{{ $scopeHPB3M = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB3', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeHPB3F = ShineOS\Core\Reports\Entities\M1::scopeCCare('HPB3', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeHPB3M + $scopeHPB3F  }}</td>
                        </tr>
                        <tr>
                            <td>Penta 1</td>
                            <td>{{ $scopePENTA1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePENTA1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePENTA1M + $scopePENTA1F }}</td>
                        </tr>
                        <tr>
                            <td>Penta 2</td>
                            <td>{{ $scopePENTA2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePENTA2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePENTA2M + $scopePENTA2F }}</td>
                        </tr>
                        <tr>
                            <td>Penta 3</td>
                            <td>{{ $scopePENTA3M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePENTA3F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PENTA1', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePENTA3M + $scopePENTA3F }}</td>
                        </tr>
                        <tr>
                            <td>OPV 1</td>
                            <td>{{ $scopeOPV1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV1', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeOPV1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV1', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeOPV1M + $scopeOPV1F }}</td>
                        </tr>
                        <tr>
                            <td>OPV 2</td>
                            <td>{{ $scopeOPV2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV2', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeOPV2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV2', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeOPV2M + $scopeOPV2F }}</td>
                        </tr>
                        <tr>
                            <td>OPV 3</td>
                            <td>{{ $scopeOPV3M = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV3', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeOPV3F = ShineOS\Core\Reports\Entities\M1::scopeCCare('OPV3', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeOPV3M + $scopeOPV3F }}</td>
                        </tr>
                        <tr>
                            <td>MCV1 (AMV)</td>
                            <td>{{ $scopeMCV1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('MCV1', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeMCV1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('MCV1', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeMCV1M + $scopeMCV1F }}</td>
                        </tr>
                        <tr>
                            <td>MCV2 (MMR)</td>
                            <td>{{ $scopeMCV2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('MCV2', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeMCV2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('MCV2', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeMCV2M + $scopeMCV2F }}</td>
                        </tr>
                        <tr>
                            <td>Rota 1</td>
                            <td>{{ $scopeROTA1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA1', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeROTA1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA1', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeROTA1M + $scopeROTA1F }}</td>
                        </tr>
                        <tr>
                            <td>Rota 2</td>
                            <td>{{ $scopeROTA2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA2', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeROTA2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA2', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeROTA2M + $scopeROTA2F }}</td>
                        </tr>
                        <tr>
                            <td>Rota 3</td>
                            <td>{{ $scopeROTA3M = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA3', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeROTA3F = ShineOS\Core\Reports\Entities\M1::scopeCCare('ROTA3', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeROTA3M + $scopeROTA3F }}</td>
                        </tr>
                        <tr>
                            <td>PCV 1</td>
                            <td>{{ $scopePCV1M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV1', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePCV1F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV1', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePCV1M + $scopePCV1F }}</td>
                        </tr>
                        <tr>
                            <td>PCV 2</td>
                            <td>{{ $scopePCV2M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV2', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePCV2F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV2', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePCV2M + $scopePCV2F }}</td>
                        </tr>
                        <tr>
                            <td>PCV 3</td>
                            <td>{{ $scopePCV3M = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV3', 'M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePCV3F = ShineOS\Core\Reports\Entities\M1::scopeCCare('PCV3', 'F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopePCV3M + $scopePCV3F }}</td>
                        </tr>
                        <tr>
                            <td>Fully Immunized Child (0-11 mos)</td>
                            <td>{{ $scopeFullM = ShineOS\Core\Reports\Entities\M1::scopeFullImmune('M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeFullF = ShineOS\Core\Reports\Entities\M1::scopeFullImmune('F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeFullM + $scopeFullF }}</td>
                        </tr>
                        <tr>
                            <td>Completely Immunized Child (12-23 mos)</td>
                            <td>{{ $scopeComplM = ShineOS\Core\Reports\Entities\M1::scopeCompleteImmune('M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeComplF = ShineOS\Core\Reports\Entities\M1::scopeCompleteImmune('F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeComplM + $scopeComplF }}</td>
                        </tr>
                        <tr>
                            <td>Total Live births</td>
                            <td>{{ $scopeLBM = ShineOS\Core\Reports\Entities\M1::scopeLiveBirth('M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeLBF = ShineOS\Core\Reports\Entities\M1::scopeLiveBirth('F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeLBM + $scopeLBF }}</td>
                        </tr>
                        <tr>
                            <td>Child Protected at Birth</td>
                            <td>{{ $scopeCProM = ShineOS\Core\Reports\Entities\M1::scopeChildProtect('M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeCProF = ShineOS\Core\Reports\Entities\M1::scopeChildProtect('F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeCProM + $scopeCProF }}</td>
                        </tr>
                        <tr>
                            <td>Infant Age 6 months seen</td>
                            <td>{{ $scopeSeenM = ShineOS\Core\Reports\Entities\M1::scopeSixMonthSeen('M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeSeenF = ShineOS\Core\Reports\Entities\M1::scopeSixMonthSeen('F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeSeenM + $scopeSeenF }}</td>
                        </tr>
                        <tr>
                            <td>Infant exclusively breastfed until 6 months</td>
                            <td>{{ $scopeBFM = ShineOS\Core\Reports\Entities\M1::scopeBreastFeed('M', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeBFF = ShineOS\Core\Reports\Entities\M1::scopeBreastFeed('F', 'm', $month, $year, $barangay) }}</td>
                            <td>{{ $scopeBFM + $scopeBFF }}</td>
                        </tr>
                        </tbody>
                        </table>
                    </td>
                    <td width="50%" style="padding:0;">
                        <table class="table table-striped table-bordered table-report">
                        <thead>
                        <tr>
                        <th>CHILD CARE - Part 2</th>
                        <th width="15%">Male</th>
                        <th width="15%">Female</th>
                        <th width="15%">Total</th>
                        </tr>
                        </thead>
                            <tr>
                                <td>Infant given complimentary food from 6-8 months</td>
                                <td>{{ $scopeCompFoodM = ShineOS\Core\Reports\Entities\M1::scopeCompFood('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeCompFoodF = ShineOS\Core\Reports\Entities\M1::scopeCompFood('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeCompFoodM + $scopeCompFoodF }}</td>
                            </tr>
                            <tr>
                                <td>Infant for newborn screening : referred</td>
                                <td>{{ $scopeNBornRefM = ShineOS\Core\Reports\Entities\M1::scopeNBornRef('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeNBornRefF = ShineOS\Core\Reports\Entities\M1::scopeNBornRef('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeNBornRefM + $scopeNBornRefF }}</td>
                            </tr>
                            <tr>
                                <td>Infant for newborn screening : done</td>
                                <td>{{ $scopeNBornDM = ShineOS\Core\Reports\Entities\M1::scopeNBornDone('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeNBornDF = ShineOS\Core\Reports\Entities\M1::scopeNBornDone('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeNBornDM + $scopeNBornDF }}</td>
                            </tr>
                            <tr>
                                <td>Infant 6-11 months old received Vitamin A</td>
                                <td>{{ $scopeVitA1M = ShineOS\Core\Reports\Entities\M1::scopeVitAFirst('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeVitA1F = ShineOS\Core\Reports\Entities\M1::scopeVitAFirst('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeVitA1M + $scopeVitA1F }}</td>
                            </tr>
                            <tr>
                                <td>Chidren 12-59 months old received Vitamin A</td>
                                <td>{{ $scopeVitA2M = ShineOS\Core\Reports\Entities\M1::scopeVitASecond('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeVitA2F = ShineOS\Core\Reports\Entities\M1::scopeVitASecond('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeVitA2M + $scopeVitA2F }}</td>
                            </tr>
                            <tr>
                                <td>Infant 6-11 months old received Iron</td>
                                <td>{{ $scopeIronAM = ShineOS\Core\Reports\Entities\M1::scopeIronA('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeIronAF = ShineOS\Core\Reports\Entities\M1::scopeIronA('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeIronAM + $scopeIronAF }}</td>
                            </tr>
                            <tr>
                                <td>Children 12-59 months old received Iron</td>
                                <td>{{ $scopeIronBM = ShineOS\Core\Reports\Entities\M1::scopeIronB('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeIronBF = ShineOS\Core\Reports\Entities\M1::scopeIronB('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeIronBM + $scopeIronBF }}</td>
                            </tr>
                            <tr>
                                <td>Infant 6-11 months received MNP</td>
                                <td>{{ $scopeMNPAM = ShineOS\Core\Reports\Entities\M1::scopeMNPA('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeMNPAF = ShineOS\Core\Reports\Entities\M1::scopeMNPA('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeMNPAM + $scopeMNPAF }}</td>
                            </tr>
                            <tr>
                                <td>Children 12-23 months received MNP</td>
                                <td>{{ $scopeMNPBM = ShineOS\Core\Reports\Entities\M1::scopeMNPB('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeMNPBF = ShineOS\Core\Reports\Entities\M1::scopeMNPB('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeMNPBM + $scopeMNPBF }}</td>
                            </tr>
                            <tr>
                                <td>Sick Children 6-11 months seen</td>
                                <td>{{ $scopeSickAM = ShineOS\Core\Reports\Entities\M1::scopeSickSeen(6,11,'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeSickAF = ShineOS\Core\Reports\Entities\M1::scopeSickSeen(6,11,'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeSickAM + $scopeSickAF }}</td>
                            </tr>
                            <tr>
                                <td>Sick Children 6-11 months received Vitamin A</td>
                                <td>{{ $scopeSickVitAAM = ShineOS\Core\Reports\Entities\M1::scopeSickVitA(6,11,'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeSickVitAAF = ShineOS\Core\Reports\Entities\M1::scopeSickVitA(6,11,'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeSickVitAAM + $scopeSickVitAAF }}</td>
                            </tr>
                            <tr>
                                <td>Sick Children 12-59 months seen</td>
                                <td>{{ $scopeSickBM = ShineOS\Core\Reports\Entities\M1::scopeSickSeen(12,59,'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeSickBF = ShineOS\Core\Reports\Entities\M1::scopeSickSeen(12,59,'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeSickBM + $scopeSickBF }}</td>
                            </tr>
                            <tr>
                                <td>Sick Children 12-59 months received Vitamin A</td>
                                <td>{{ $scopeSickVitABM = ShineOS\Core\Reports\Entities\M1::scopeSickVitA(12,59,'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeSickVitABF = ShineOS\Core\Reports\Entities\M1::scopeSickVitA(12,59,'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeSickVitABM + $scopeSickVitABF }}</td>
                            </tr>
                            <tr>
                                <td>Children 12-59 mos. old given de-worming tablet/syrup</td>
                                <td>{{ $scopeDeWormM = ShineOS\Core\Reports\Entities\M1::scopeDeWorm('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeDeWormF = ShineOS\Core\Reports\Entities\M1::scopeDeWorm('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeDeWormM + $scopeDeWormF }}</td>
                            </tr>
                            <tr>
                                <td>Infant 2-5 mos w/ Low Birth Weight seen</td>
                                <td>{{ $scopeLowWtM = ShineOS\Core\Reports\Entities\M1::scopeLowWt(2, 5, 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeLowWtF = ShineOS\Core\Reports\Entities\M1::scopeLowWt(2, 5, 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeLowWtM + $scopeLowWtF }}</td>
                            </tr>
                            <tr>
                                <td>Infant 2-5 mos w/ LBW received full dose iron</td>
                                <td>{{ $scopeLowWtIronM = ShineOS\Core\Reports\Entities\M1::scopeLowWtIron(2, 5, 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeLowWtIronF = ShineOS\Core\Reports\Entities\M1::scopeLowWtIron(2, 5, 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeLowWtIronM + $scopeLowWtIronF }}</td>
                            </tr>
                            <tr>
                                <td>Anemic Children 6-11 months old seen</td>
                                <td>{{ $scopeAnemia611M = ShineOS\Core\Reports\Entities\M1::scopeAnemia(06, 11, 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeAnemia611F = ShineOS\Core\Reports\Entities\M1::scopeAnemia(06, 11, 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeAnemia611M + $scopeAnemia611F }}</td>
                            </tr>
                            <tr>
                                <td>Anemic Children 6-11 mos received full dose iron</td>
                                <td>{{ $scopeAnemia1259WithIronM = ShineOS\Core\Reports\Entities\M1::scopeAnemiaWithIron(06, 11, 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeAnemia1259WithIronF = ShineOS\Core\Reports\Entities\M1::scopeAnemiaWithIron(06, 11, 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeAnemia1259WithIronM + $scopeAnemia1259WithIronF }}</td>
                            </tr>
                            <tr>
                                <td>Anemic Children 12-59 months old seen</td>
                                <td>{{ $scopeAnemia1259M = ShineOS\Core\Reports\Entities\M1::scopeAnemia(12, 59, 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeAnemia1259F = ShineOS\Core\Reports\Entities\M1::scopeAnemia(12, 59, 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeAnemia1259M + $scopeAnemia1259F }}</td>
                            </tr>
                            <tr>
                                <td>Anemic Children 12-59 mos received full dose iron</td>
                                <td>{{ $scopeAnemia1259WithIronM = ShineOS\Core\Reports\Entities\M1::scopeAnemiaWithIron(12, 59, 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeAnemia1259WithIronF = ShineOS\Core\Reports\Entities\M1::scopeAnemiaWithIron(12, 59, 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeAnemia1259WithIronM + $scopeAnemia1259WithIronF }}</td>
                            </tr>
                            <tr>
                                <td>Diarrhea cases 0-59 months old seen</td>
                                <td>{{ $scopeDiarrheaM = ShineOS\Core\Reports\Entities\M1::scopeDiarrhea(0, 59, 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeDiarrheaF = ShineOS\Core\Reports\Entities\M1::scopeDiarrhea(0, 59, 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeDiarrheaM + $scopeDiarrheaF }}</td>
                            </tr>
                            <tr>
                                <td>Diarrhea cases 0-59 mos old received ORS</td>
                                <td>{{ $scopeDiarrheaWtORSM = ShineOS\Core\Reports\Entities\M1::scopeDiarrheaWtORS(0, 59, 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeDiarrheaWtORSF = ShineOS\Core\Reports\Entities\M1::scopeDiarrheaWtORS(0, 59, 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeDiarrheaWtORSM + $scopeDiarrheaWtORSF }}</td>
                            </tr>
                            <tr>
                                <td>Diarrhea 0-59 mos received ORS/ORT w/ zinc</td>
                                <td>{{ $scopeDiarrheaWtORSZincM = ShineOS\Core\Reports\Entities\M1::scopeDiarrheaWtORSZinc(0, 59, 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeDiarrheaWtORSZincF = ShineOS\Core\Reports\Entities\M1::scopeDiarrheaWtORSZinc(0, 59, 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeDiarrheaWtORSZincM + $scopeDiarrheaWtORSZincF }}</td>
                            </tr>
                            <tr>
                                <td>Pneumonia cases 0-59 months old</td>
                                <td>{{ $scopePneumoniaM = ShineOS\Core\Reports\Entities\M1::scopePneumonia('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopePneumoniaF = ShineOS\Core\Reports\Entities\M1::scopePneumonia('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopePneumoniaM + $scopePneumoniaF }}</td>
                            </tr>
                            <tr>
                                <td>Pneumonia cases 0-59 mos. old completed Tx</td>
                                <td>{{ $scopePneumoniaTM = ShineOS\Core\Reports\Entities\M1::scopePneumoniaTreat('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopePneumoniaTF = ShineOS\Core\Reports\Entities\M1::scopePneumoniaTreat('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopePneumoniaTM + $scopePneumoniaTF }}</td>
                            </tr>
                        </tbody>
                        </table><!-- /.table child care -->
                    </td>
                </tr>
            </table>

            <table class="table table-report table-responsive">
                <tr>
                    <td width="50%" style="padding:0;vertical-align: top">
                        <table class="table table-striped table-bordered table-report">
                            <thead>
                            <tr>
                            <th>MALARIA</th>
                            <th width="15%">Male</th>
                            <th width="15%">Female</th>
                            <th width="15%">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Total Population</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>Population at Risk</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>Annual parasite incidence</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>Confirmed Malaria Cases
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;5 yrs. old</td>
                                    <td>{{ $scopeMalariaLessFiveM = ShineOS\Core\Reports\Entities\M1::scopeMalariaLessFive('M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaLessFiveF = ShineOS\Core\Reports\Entities\M1::scopeMalariaLessFive('F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaLessFiveM + $scopeMalariaLessFiveF }}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&gt;=5 yrs. old</td>
                                    <td>{{ $scopeMalariaGreaterFiveM = ShineOS\Core\Reports\Entities\M1::scopeMalariaGreaterFive('M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaGreaterFiveF = ShineOS\Core\Reports\Entities\M1::scopeMalariaGreaterFive('F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaGreaterFiveM + $scopeMalariaGreaterFiveF }}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pregnant</td>
                                    <td bgcolor="#A9A9A9"></td>
                                    <td>{{ $scopeMalariaGreaterPregnant = ShineOS\Core\Reports\Entities\M1::scopeMalariaPregnant('m', $month, $year, $barangay) }}</td>
                                    <td bgcolor="#A9A9A9"></td>
                                </tr>
                                <tr>
                                    <td>Confirmed Malaria Cases by Species</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Falciparum</td>
                                    <td>{{ $scopeMalariaFalciparumM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Falciparum','M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaFalciparumF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Falciparum','F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaFalciparumM + $scopeMalariaFalciparumF }}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Vivax</td>
                                    <td>{{ $scopeMalariaVivaxM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Vivax','M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaVivaxF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Vivax','F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaVivaxM + $scopeMalariaVivaxF }}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Ovale</td>
                                    <td>{{ $scopeMalariaOvaleM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Ovale','M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaOvaleF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Ovale','F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaOvaleM + $scopeMalariaOvaleF }}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P. Malariae</td>
                                    <td>{{ $scopeMalariaMalariaeM = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Malariae','M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaMalariaeF = ShineOS\Core\Reports\Entities\M1::scopeMalariaBySpecies('Malariae','F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaMalariaeM + $scopeMalariaMalariaeF }}</td>
                                </tr>
                                <tr>
                                    <td>By Method</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Slide</td>
                                    <td bgcolor="#A9A9A9"></td>
                                    <td bgcolor="#A9A9A9"></td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RDT</td>
                                    <td bgcolor="#A9A9A9"></td>
                                    <td bgcolor="#A9A9A9"></td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>Malaria Deaths</td>
                                    <td>{{ $scopeMalariaDeathM = ShineOS\Core\Reports\Entities\M1::scopeMalariaDeaths('M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaDeathF = ShineOS\Core\Reports\Entities\M1::scopeMalariaDeaths('F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeMalariaDeathM + $scopeMalariaDeathF }}</td>
                                </tr>
                                <tr>
                                    <td>Number of LLIN given</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table><!-- /table malaria -->
                        <table class="table table-striped table-bordered table-report">
                            <thead>
                            <tr>
                            <th>TUBERCULOSIS</th>
                            <th width="15%">Male</th>
                            <th width="15%">Female</th>
                            <th width="15%">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>TB symptomatics who underwent DSSM</td>
                                <td>{{ $scopeTBDSSMM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSM('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSM('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMM + $scopeTBDSSMF }}</td>
                            </tr>
                            <tr>
                                <td>Smear Positive discovered and identified</td>
                                <td>{{ $scopeTBDSSMPosM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPos('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPos('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosM + $scopeTBDSSMPosF }}</td>
                            </tr>
                            <tr>
                                <td>New Smear (+) cases initiated tx &amp; registered</td>
                                <td>{{ $scopeTBDSSMPosRegisteredM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('N','M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('N','F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredM + $scopeTBDSSMPosRegisteredF }}</td>
                            </tr>
                            <tr>
                                <td>New Smear (+) cases cured</td>
                                <td>{{ $scopeTBDSSMPosCuredM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('N', 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosCuredF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('N', 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosCuredM + $scopeTBDSSMPosCuredF }}</td>
                            </tr>
                            <tr>
                                <td>Smear (+) retreatment cases cured</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('ALLRETREATMENT', 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('ALLRETREATMENT', 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredM + $scopeTBDSSMPosRetreatmentCuredF }}</td>
                            </tr>
                            <tr>
                                <td>Smear (+) retreatment cases initiated tx &amp; registered</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;Relapse</td>
                                <td>{{ $scopeTBDSSMPosRegisteredRM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('R','M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredRF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('R','F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredRM + $scopeTBDSSMPosRegisteredRF }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;Treatment Failure</td>
                                <td>{{ $scopeTBDSSMPosRegisteredTAFM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('TAF','M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredTAFF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('TAF','F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredTAFM + $scopeTBDSSMPosRegisteredTAFF }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;Return After Default</td>
                                <td>{{ $scopeTBDSSMPosRegisteredRADM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('RAD','M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredRADF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('RAD','F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredRADM + $scopeTBDSSMPosRegisteredRADF }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;Other type of TB</td>
                                <td>{{ $scopeTBDSSMPosRegisteredOTHM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('OTH','M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredOTHF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('OTH','F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredOTHM + $scopeTBDSSMPosRegisteredOTHF }}</td>
                            </tr>
                            <tr>
                                <td>No of smear (+) retreatment cured</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;Relapse</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredRM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('R', 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredRF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('R', 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredRM + $scopeTBDSSMPosRetreatmentCuredRF }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;Treatment Failure</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredTAFM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('TAF', 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredTAFF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('TAF', 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredTAFM + $scopeTBDSSMPosRetreatmentCuredTAFF }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;Return After Default</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredRADM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('RAD', 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredRADF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosCuredByType('RAD', 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRetreatmentCuredRADM + $scopeTBDSSMPosRetreatmentCuredRADF }}</td>
                            </tr>
                            <tr>
                                <td>Total No. of TB cases (all forms) initiated treatment</td>
                                <td>{{ $scopeTBDSSMPosRegisteredALLM = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('ALL', 'M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredALLF = ShineOS\Core\Reports\Entities\M1::scopeTBDSSMPosRegistered('ALL', 'F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBDSSMPosRegisteredALLM + $scopeTBDSSMPosRegisteredALLF }}</td>
                            </tr>
                            <tr>
                                <td>TB all forms identified</td>
                                <td>{{ $scopeTBM = ShineOS\Core\Reports\Entities\M1::scopeTB('M', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBF = ShineOS\Core\Reports\Entities\M1::scopeTB('F', 'm', $month, $year, $barangay) }}</td>
                                <td>{{ $scopeTBM + $scopeTBF }}</td>
                            </tr>
                            <tr>
                                <td>Cases Detection Rate</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                            </tr>
                            </tbody>
                        </table><!-- /table tuberculosis -->
                    </td>
                    <td width="50%" style="padding:0;vertical-align: top">
                        <table class="table table-striped table-bordered table-report">
                            <thead>
                            <tr>
                            <th>SCHISTOSOMIASIS</th>
                            <th width="15%">Male</th>
                            <th width="15%">Female</th>
                            <th width="15%">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>No. of symptomatic case</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>No. of cases examined</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>No. of positive cases</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Low intensity</td>
                                    <td>{{ $scopeSchistosomiasisLowM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Low Intensity','M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisLowF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Low Intensity','F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisLowM + $scopeSchistosomiasisLowF }}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Medium intensity</td>
                                    <td>{{ $scopeSchistosomiasisMediumM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Medium Intensity','M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisMediumF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Medium Intensity','F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisMediumM + $scopeSchistosomiasisMediumF }}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;High intensity</td>
                                    <td>{{ $scopeSchistosomiasisHighM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - High Intensity','M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisHighF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - High Intensity','F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisHighM + $scopeSchistosomiasisHighF }}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Unknown intensity</td>
                                    <td>{{ $scopeSchistosomiasisHighM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Intensity Unknown','M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisHighF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisByIntensity('Schistosomiasis - Intensity Unknown','F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisHighM + $scopeSchistosomiasisHighF }}</td>
                                </tr>
                                <tr>
                                    <td>No. of cases treated</td>
                                    <td>{{ $scopeSchistosomiasisTreatedM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisWtTreatment('M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisTreatedF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisWtTreatment('F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisTreatedM + $scopeSchistosomiasisTreatedF }}</td>
                                </tr>
                                <tr>
                                    <td>No. of complicated cases</td>
                                    <td>{{ $scopeSchistosomiasisComplicatedM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisComplicated('M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisComplicatedF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisComplicated('F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisComplicatedM + $scopeSchistosomiasisComplicatedF }}</td>
                                </tr>
                                <tr>
                                    <td>No. of complicated cases referred</td>
                                    <td>{{ $scopeSchistosomiasisComplicatedReferredM = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisComplicatedReferred('M', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisComplicatedReferredF = ShineOS\Core\Reports\Entities\M1::scopeSchistosomiasisComplicatedReferred('F', 'm', $month, $year, $barangay) }}</td>
                                    <td>{{ $scopeSchistosomiasisComplicatedReferredM + $scopeSchistosomiasisComplicatedReferredF }}</td>
                                </tr>
                            </tbody>
                        </table><!-- /table SCHISTOSOMIASIS -->
                        <table class="table table-striped table-bordered table-report">
                            <thead>
                            <tr>
                            <th>FILARIASIS</th>
                            <th width="15%">Male</th>
                            <th width="15%">Female</th>
                            <th width="15%">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>No. of cases w/Hydrocele, Lymphedema, Elephantasis and Chyluria</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>No. of cases examined</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>Clinical Rate</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>No. of cases Examined found for MF</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>Average MFD</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>Eligible population given MDA (94.6% of TP)</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>Total Population given MDA</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table><!-- /table FILARIASIS -->
                        <table class="table table-striped table-bordered table-report">
                            <thead>
                            <tr>
                            <th>LEPROSY</th>
                            <th width="15%">Male</th>
                            <th width="15%">Female</th>
                            <th width="15%">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Total Population</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>Total No. of Leprosy cases (undergoing treatment)</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>No. of Newly detected Leprosy cases (&lt;15yo)</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>No. of Newly detected Leprosy cases (Grade 2 disability)</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>No. of Leprosy cases cured</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table><!-- /table LEPROSY -->
                        <table class="table table-striped table-bordered table-report">
                            <thead>
                            <tr>
                            <th>STI SURVEILLANCE</th>
                            <th width="15%">Male</th>
                            <th width="15%">Female</th>
                            <th width="15%">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>No. of pregnant women seen</td>
                                    <td bgcolor="#A9A9A9"></td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>No. of pregnant women tested for Syphillis</td>
                                    <td bgcolor="#A9A9A9"></td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>No. of pregnant women positive for Syphillis</td>
                                    <td bgcolor="#A9A9A9"></td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>No. of pregnant women given Penicillin</td>
                                    <td bgcolor="#A9A9A9"></td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table><!-- /table STI SURVEILLANCE -->
                    </td>
                </tr>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
</div>
@stop
