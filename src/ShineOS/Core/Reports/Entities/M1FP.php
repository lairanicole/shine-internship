<?php
namespace ShineOS\Core\Reports\Entities;

use Shine\Libraries\FacilityHelper;
use ShineOS\Core\Patients\Entities\Patients;
use App\Libraries\CSSColors;
use DB, Input, DateTime, Session;
use Illuminate\Database\Eloquent\Model;

class M1FP extends Model {

    /**
     *  REUSABLE SCOPE HERE
     */

    protected $table = 'fhsis_m1fp';
    protected static $table_name = 'fhsis_m1fp';
    protected $primaryKey = 'id';

    public function __construct(){
        
    }
    public static function doFP($method,$col,$tmp,$month,$year,$type,$brgycode = NULL) 
    {
        

        $facility = Session::get('facility_details');

        if($brgycode != NULL)
        {
            $raw = "barangaycode = " . $brgycode;
        }
        else
        {
            $raw = "facility_id IS NOT NULL";
        }

        switch ($type) {
            case 'dropout':
                $val = DB::table(self::$table_name)
                            ->select($col)
                            ->where('facility_id', $facility->facility_id)
                            ->whereRaw( DB::raw($raw) ) //barangay filter
                            ->where('previous_method', $method)
                            ->whereRaw( DB::raw($str) ) //month filter
                            ->where('FPyear', $year)
                            ->first();
                break;
            case 'current':
                $val = DB::table(self::$table_name)
                            ->select($col)
                            ->where('facility_id', $facility->facility_id)
                            ->whereRaw( DB::raw($raw) ) //barangay filter
                            ->where('current_method', $method)
                            ->whereRaw( DB::raw($str) ) //month filter
                            ->where('FPyear', $year)
                            ->first();
                break;
        }

        if($val) {
            return $val->$col;
        } else {
            return 0;
        }
    }

    public static function fpQuery($month=NULL,$year=NULL,$brgycode=NULL) {
        $fp_method = getArrFamilyPlanningMethod();
        $fp_type = array('CU_begin'=>0,'NA_begin'=>0,'OA'=>0,'Dropout'=>0,'CU_end'=>0,'NA_end'=>0);

        $_this = new self();
        $facility = Session::get('facility_details');
        $str = $raw = NULL;
        $currArray = $prevArray = array();
        
        $fp_current = M1FP::select(DB::raw('current_method, CU_begin, NA_begin, OA, Dropout, CU_end, NA_end'))->where('facility_id', $facility->facility_id);

        $fp_previous = M1FP::select(DB::raw('previous_method, CU_begin, NA_begin, OA, Dropout, CU_end, NA_end'))->where('facility_id', $facility->facility_id);

            if($brgycode != NULL) {
                $fp_current = $fp_current->whereIn('barangaycode', (array)$brgycode);
                $fp_previous = $fp_previous->whereIn('barangaycode', (array)$brgycode);
            }
            if($year != NULL) {
                $fp_current = $fp_current->where('FPyear', $year);
                $fp_previous = $fp_previous->where('FPyear', $year);
            }
            if($str!=NULL) {
                $fp_current = $fp_current->whereRaw(DB::raw($str));
                $fp_previous = $fp_previous->whereRaw(DB::raw($str));
            }
        $fp_current = $fp_current->whereIn('FPmonth',(array)$month)->get()->toArray();
        $fp_previous = $fp_previous->whereIn('FPmonth',(array)$month)->get()->toArray();

        foreach ($fp_current as $key_current => $value_current) {
            foreach ($fp_type as $value_type => $key_type) {
                $currArray[$value_current['current_method']][$value_type] = $value_current[$value_type];
            }
        }

        foreach ($fp_previous as $key_current => $value_current) {
            foreach ($fp_type as $value_type => $key_type) {
                $prevArray[$value_current['previous_method']][$value_type] = $value_current[$value_type];
            }
        }

            $currDiff = array_diff(array_keys($fp_method), array_keys($currArray));
            if($currDiff) {
                foreach ($currDiff as $key_curr => $value_curr) {
                    $currArray[$value_curr] = $fp_type;
                }
            }
            $prevDiff = array_diff(array_keys($fp_method), array_keys($prevArray));
            if($prevDiff) {
                foreach ($prevDiff as $key_prev => $value_prev) {
                    $prevArray[$value_prev] = $fp_type;
                }
            }

        return array('previousMethod'=>$prevArray,'currentMethod'=>$currArray);
    }
}
