<?php
namespace Widgets\analytics\TopDiagnosis;

use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\FacilityRepositoryInterface;
use Shine\Libraries\HealthcareservicesHelper;
use Shine\Libraries\FacilityHelper;
use Arrilot\Widgets\AbstractWidget;
use View;

//DO NOT FORGET TO UNCOMMENT LOOP WHEN DONE WITH REPOSITORIES
class TopDiagnosis extends AbstractWidget
{
    private $UserRepository;
    private $healthcareRepository;

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function placeholder()
    {
        $loading = '<div class="box box-primary"><!--Consultations-->
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-cog fa-spin fa-fw"></i> Loading widget...</h3>
                </div>
            </div>';


        return $loading;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(UserRepository $UserRepository, HealthcareRepository $healthcareRepository)
    {
        $start = date('Y-m-01');
        $end = date('Y-m-d', strtotime('last day of this month'));

        $diagnosis = getCountByDiagnosis($start,$end,10);
        $diagnosisCount = $diagnosis['diagnosis'];
        $totalCount = $diagnosis['totalCount'];

        View::addNamespace('top_diagnosis', 'widgets/analytics/TopDiagnosis/');
        return view("top_diagnosis::index", [
            'config' => $this->config,
            'diagnosisCount' => $diagnosisCount,
            'totalCount' => $totalCount
        ]);
    }
}
