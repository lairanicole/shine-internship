<?php
namespace Widgets\analytics\TopMedication;

use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\FacilityRepositoryInterface;
use Shine\Libraries\HealthcareservicesHelper;
use Shine\Libraries\FacilityHelper;
use Arrilot\Widgets\AbstractWidget;
use View, Session;

use Modules\MedicationDispensing\Entities\MedicationDispensing;

//DO NOT FORGET TO UNCOMMENT LOOP WHEN DONE WITH REPOSITORIES
class TopMedication extends AbstractWidget
{
    private $UserRepository;
    private $healthcareRepository;

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function placeholder()
    {
        $loading = '<div class="box box-primary"><!--Consultations-->
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-cog fa-spin fa-fw"></i> Loading widget...</h3>
                </div>
            </div>';


        return $loading;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(UserRepository $UserRepository, HealthcareRepository $healthcareRepository)
    {
        $start = date('Y-m-01');
        $end = date('Y-m-d', strtotime('last day of this month'));

        $diagnosis = MedicationDispensing::getCountByBrandName($start,$end,10);
        $medicationCount = $diagnosis['visits'];
        $totalCount = $diagnosis['total'];

        View::addNamespace('top_medication', 'widgets/analytics/TopMedication/');
        return view("top_medication::index", [
            'config' => $this->config,
            'medicationCount' => $medicationCount,
            'totalCount' => $totalCount
        ]);
    }
}
