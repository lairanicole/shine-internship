<?php
namespace Plugins\AnimalBite;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class AnimalBiteModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'animalbite_service';
    protected static $table_name = 'animalbite_service';
    protected $primaryKey = 'animalbite_id';
    protected $touches = array('Healthcareservices');

    public function Healthcareservices() {
        return $this->belongsTo('ShineOS\Core\Healthcareservices\Entities\Healthcareservices', 'healthcareservice_id', 'healthcareservice_id');
    }
}
