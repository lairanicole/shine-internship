<?php

use Plugins\AnimalBite\AnimalBiteModel as animalbite;
use Modules\Helathcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

class AnimalBiteController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        //no index
    }

    public function save($data)
    {
      $animalbite_id = $data['fpservice_id'];
      $hservice_id = $data['hservice_id'];
      $patient_id = $data['patient_id'];

      $animalbite_service = animalbite::where('animalbite_id','=',$animalbite_id)->first();

      if($animalbite_service == NULL) {
          $animalbite_service =  new animalbite;
          $animalbite_id = $data['fpservice_id'];
          $animalbite_service->healthcareservice_id = $hservice_id;
      }

            $animalbite_service->typeOfBite = isset($data['typeOfBite']) ? $data['typeOfBite'] : NULL;
            $animalbite_service->DateOfExposure = $data['DateOfExposure'] ? date('Y-m-d', strtotime($data['DateOfExposure'])) : NULL;
            $animalbite_service->SiteOfBite = $data['SiteOfBite'];
            $animalbite_service->NatureOfBite = $data['NatureOfBite'];
            $animalbite_service->NoOfBites = isset($data['NoOfBites']) ? $data['NoOfBites'] : NULL;
            $animalbite_service->Vicinity = isset($data['vicinity']) ? $data['vicinity'] : NULL;
            $animalbite_service->VicinityOthers = $data['VicinityOthers'];
            $animalbite_service->BitingAnimal = isset($data['BitingAnimal']) ? $data['BitingAnimal'] : NULL;
            $animalbite_service->BitingAnimalOthers = $data['BitingAnimalOthers'];
            $animalbite_service->Specie = $data['Specie'];
            $animalbite_service->StatusOfAnimal = isset($data['StatusOfAnimal']) ? $data['StatusOfAnimal'] : NULL;
            $animalbite_service->DateDied = $data['DateDied'] ? date('Y-m-d', strtotime($data['DateDied'])) : NULL;
            $animalbite_service->LabConfirmation = isset($data['labConfirm']) ? $data['labConfirm'] : NULL;
            $animalbite_service->LabDate = $data['labDate'] ? date('Y-m-d', strtotime($data['labDate'])) : NULL;
            $animalbite_service->LabName = $data['labName'];
            $animalbite_service->Vaccination = isset($data['antiRabiesVaccination']) ? $data['antiRabiesVaccination'] : NULL;
            $animalbite_service->vaccinationDateText = $data['vaccinationDateText'];
            $animalbite_service->DateOfVaccination = $data['DateOfVaccination'] ? date('Y-m-d', strtotime($data['DateOfVaccination'])) : NULL;
            $animalbite_service->WhoVaccinatedIt = isset($data['whoVaccinatedIt']) ? $data['whoVaccinatedIt'] : NULL;
            $animalbite_service->Ownership = isset($data['ownershipStatus']) ? $data['ownershipStatus'] : NULL;
            $animalbite_service->ContactWithOtherAnimal = isset($data['ContactWithOtherAnimal']) ? $data['ContactWithOtherAnimal'] : NULL;
            $animalbite_service->LocationOfContact = isset($data['LocationOfContact']) ? $data['LocationOfContact'] : NULL;
            $animalbite_service->ConditionBeforeBite = isset($data['condition']) ? $data['condition'] : NULL;
            $animalbite_service->NonBiteCase = isset($data['nonBiteCase']) ? $data['nonBiteCase'] : NULL;
            $animalbite_service->NonBiteCaseOthers = $data['NonBiteCaseOthers'];
            $animalbite_service->WoundCare = isset($data['WoundCare']) ? $data['WoundCare'] : NULL;
            $animalbite_service->YesWoundCareOthers = $data['YesWoundCareOthers'];
            //$animalbite_service->Consulted = isset($data['Consulted']) ? $data['Consulted'] : NULL; checkbox
            //$animalbite_service->IncidentReported = isset($data['IncidentReportedTo']) ? $data['IncidentReportedTo'] : NULL; checkbox
            $animalbite_service->ReportedToMunicipalOfficial = $data['ReportedToMunicipalOfficial'];
            $animalbite_service->ReportedToBarangayOfficial = $data['ReportedToBarangayOfficial'];
            $animalbite_service->ReportedToHealthPersonnel = $data['ReportedToHealthPersonnel'];
            $animalbite_service->ReportedToOthers = $data['ReportedToOthers'];
            $animalbite_service->CategoryOfExposure = isset($data['CategoryOfExposure']) ? $data['CategoryOfExposure'] : NULL;
            $animalbite_service->HistoryOfVaccination = isset($data['HistoryOfVaccination']) ? $data['HistoryOfVaccination'] : NULL;
            $animalbite_service->YesPastVaccination = isset($data['YesPastVaccination']) ? $data['YesPastVaccination'] : NULL;
            $animalbite_service->DateOfLastVaccination = $data['DateOfLastVaccination'] ? date('Y-m-d', strtotime($data['DateOfLastVaccination'])) : NULL;
            //$animalbite_service->VaccineReceived = isset($data['VaccineReceived']) ? $data['VaccineReceived'] : NULL; checkbox
            $animalbite_service->RouteOfAdmin = isset($data['RouteOfAdmin']) ? $data['RouteOfAdmin'] : NULL;
            $animalbite_service->WhoGaveTheVaccine = isset($data['WhoGaveTheVaccine']) ? $data['WhoGaveTheVaccine'] : NULL;
            $animalbite_service->Management = Input::get('Management');
            //$animalbite_service->TakeHomeInstrcutions = isset($data['TakeHomeInstrcutions']) ? $data['TakeHomeInstrcutions'] : NULL; checkbox

            $YesWoundCareString = "";
            if(isset($data['YesWoundCare']))
            {
                $YesWoundCare = $data['YesWoundCare'];

                $YesWoundCareString = implode(',', $YesWoundCare);
            }
            $animalbite_service->YesWoundCare = $YesWoundCareString;

            $ConsultedString = "";
            if(isset($data['Consulted']))
            {
                $Consulted = $data['Consulted'];

                $ConsultedString = implode(',', $Consulted);
            }
            $animalbite_service->Consulted = $ConsultedString;

            $IncidentReportedString = "";
            if(isset($data['IncidentReported']))
            {
                $IncidentReported = $data['IncidentReported'];

                $IncidentReportedString = implode(',', $IncidentReported);
            }
            $animalbite_service->IncidentReported = $IncidentReportedString;

            $VaccineReceivedString = "";
            if(isset($data['VaccineReceived']))
            {
                $VaccineReceived = $data['VaccineReceived'];

                $VaccineReceivedString = implode(',', $VaccineReceived);
            }
            $animalbite_service->VaccineReceived = $VaccineReceivedString;

            $TakeHomeInstrcutionsString = "";
            if(isset($data['TakeHomeInstrcutions']))
            {
                $TakeHomeInstrcutions = $data['TakeHomeInstrcutions'];

                $TakeHomeInstrcutionsString = implode(',', $TakeHomeInstrcutions);
            }
            $animalbite_service->TakeHomeInstrcutions = $TakeHomeInstrcutionsString;

            $animalbite_service->save();

            //$patient_id = getPatientIDByHealthcareserviceID($hservice_id);
            //header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
            //exit;
        }
    }
