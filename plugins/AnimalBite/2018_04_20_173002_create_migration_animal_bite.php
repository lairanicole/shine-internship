<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMigrationAnimalBite extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('animalbite_service')!=TRUE) {
            Schema::create('animalbite_service', function (Blueprint $table) {
                $table->increments('id');
                $table->string('animalbite_id', 32);
                $table->string('healthcareservice_id', 32);

                $table->string('typeOfBite', 32)->nullable();
                $table->string('DateOfExposure', 32)->nullable();
                $table->string('SiteOfBite', 32)->nullable();
                $table->string('NatureOfBite', 32)->nullable();
                $table->string('NoOfBites', 32)->nullable();
                $table->string('Vicinity', 32)->nullable();
                $table->string('VicinityOthers', 32)->nullable();
                $table->string('BitingAnimal', 32)->nullable();
                $table->string('BitingAnimalOthers', 32)->nullabe();
                $table->string('Specie', 32)->nullable();
                $table->string('StatusOfAnimal', 32)->nullable();
                $table->string('DateDied', 32)->nullable();
                $table->string('LabConfirmation', 32)->nullable();
                $table->string('LabDate', 32)->nullable();
                $table->string('LabName', 32)->nullable();
                $table->string('Vaccination', 32)->nullable();
                $table->string('vaccinationDateText', 32)->nullable();
                $table->string('DateOfVaccination', 32)->nullable();
                $table->string('WhoVaccinatedIt', 32)->nullable();
                $table->string('Ownership', 32)->nullable();
                $table->string('ContactWithOtherAnimal', 32)->nullable();
                $table->string('LocationOfContact', 32)->nullable();
                $table->string('ConditionBeforeBite', 32)->nullable();
                $table->string('NonBiteCase', 32)->nullable();
                $table->string('NonBiteCaseOthers', 32)->nullable();
                $table->string('WoundCare', 32)->nullable();
                $table->string('YesWoundCare', 32)->nullable();
                $table->string('YesWoundCareOthers',32)->nullabe();
                $table->string('Consulted', 32)->nullable();
                $table->string('IncidentReported', 32)->nullable();
                $table->string('ReportedToMunicipalOfficial',32)->nullabe();
                $table->string('ReportedToBarangayOfficial',32)->nullabe();
                $table->string('ReportedToHealthPersonnel',32)->nullabe();
                $table->string('ReportedToOthers',32)->nullabe();
                $table->string('CategoryOfExposure', 32)->nullable();
                $table->string('HistoryOfVaccination', 32)->nullable();
                $table->string('YesPastVaccination', 32)->nullable();
                $table->string('DateOfLastVaccination', 32)->nullable();
                $table->string('VaccineReceived', 32)->nullable();
                $table->string('RouteOfAdmin', 32)->nullable();
                $table->string('WhoGaveTheVaccine', 32)->nullable();
                $table->text('Management')->default(NULL)->nullable();
                $table->string('TakeHomeInstrcutions', 100)->nullable();

                $table->softDeletes();
                $table->timestamps();
                $table->unique('healthcareservice_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('animalbite_service');
    }

}
