<?php
$plugin_id = 'AnimalBite';                       //plugin ID
$plugin_module = 'healthcareservices';          //module owner
$plugin_location = 'dropdown';                  //UI location where plugin will be accessible
$plugin_primaryKey = 'animalbite_id';        //primary_key used to find data
$plugin_table = 'animalbite_service';            //plugintable default; table_name custom table
$plugin_tabs_child = array('addservice', 'complaints', 'vitals', 'animalbite_plugin', 'impanddiag', 'medicalorders', 'disposition'); //,
$plugin_type = 'program';
$plugin_age = "0-100";
$plugin_gender = "all";
//plugin maximum role to access this plugin (MANDATORY VALUE)
$plugin_role = 5;

$plugin_relationship = array();
$plugin_folder = 'AnimalBite'; //module owner
$plugin_title = 'Animal Bite';            //plugin title
$plugin_description = 'Animal Bite';
$plugin_version = '1.2';
$plugin_developer = 'ShineLabs';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2018";

$plugin_tabs = [
    'addservice' => 'Basic Information',
    'complaints' => 'Complaints',
    'impanddiag' => 'Impressions & Diagnosis',
    'disposition' => 'Disposition',
    'medicalorders' => 'Medical Orders',
    'vitals' => 'Vitals & Physical',
    'animalbite_plugin' => 'Animal Bite'
];

$plugin_tabs_models = [
    'complaints' => 'GeneralConsultation',
    'disposition' => 'Disposition',
    'medicalorders' => 'MedicalOrder',
    'vitals' => 'VitalsPhysical',
    'animalbite_plugin' => 'AnimalBiteModel'
];
