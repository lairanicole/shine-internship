<?php

  function procDate($date) {
        if($date) {
          $d = date('m/d/Y', strtotime($date));
          return $d;
      } else {
          return NULL;
      }
  }

  $hservice_id = $allData['healthcareserviceid'];
  $fpservice_id = $data['animalbite_id'];
  $patient_id = $allData['patient']->patient_id;
  $currentdate = date("m/d/Y");


  // for hiding forms
  $hiddenBite = "hidden";
  $hiddennBite = "hidden";
  $hiddenVicinity = "hidden";
  $hiddenWoundCare = "hidden";
  $hiddenAnimal = "hidden";
  $hiddenSentToLab = "hidden";
  $hiddenDead = "hidden";
  $hiddenVaccination = "hidden";
  $hiddenPastvaccination = "hidden";
  if(isset($data['typeOfBite'])){
      if($data['typeOfBite'] == 'Bite') {
          $hiddenBite = "";
          $hiddennBite = "hidden";
      }
      if($data['typeOfBite'] == 'Non-Bite'){
          $hiddennBite = "";
          $hiddenBite = "hidden";
      }
    }
  else {
      $hiddenBite = "hidden";
      $hiddennBite = "hidden";
    }
  if(isset($data['WoundCare'])){
      if($data['WoundCare'] == 'Yes'){
          $hiddenWoundCare = "";
      }
  }
  else {
      $hiddenWoundCare = "hidden";
    }
  if(isset($data['Vicinity'])){
    if($data['Vicinity'] == 'Others'){
        $hiddenVicinity = "";
    }
  }
  else {
    $hiddenVicinity = "hidden";
  }
  if(isset($data['BitingAnimal'])){
    if($data['BitingAnimal'] == 'Others'){
        $hiddenAnimal = "";
    }
  }
  else {
        $hiddenAnimal = "hidden";
  }
  if(isset($data['StatusOfAnimal'])){
    if($data['StatusOfAnimal'] == 'Dead'){
        $hiddenDead = "";
    }
  }
  else {
        $hiddenDead = "hidden";
  }
  if(isset($data['LabConfirmation'])){
    if($data['LabConfirmation'] == 'Yes'){
        $hiddenSentToLab = "";
    }
  }
  else {
    $hiddenSentToLab = "hidden";
  }
  if(isset($data['Vaccination'])){
    if($data['Vaccination'] == 'Yes'){
        $hiddenVaccination = "";
    }
  }
  else {
    $hiddenVaccination = "hidden";
  }
  if(isset($data['HistoryOfVaccination'])){
    if($data['HistoryOfVaccination'] == 'Yes'){
        $hiddenPastvaccination = "";
    }
  }
  else {
        $hiddenPastvaccination = "hidden";
  }

  //let us disable this form if this consult is already disposed
  if(empty($allData['disposition_record']['disposition'])) {
     $read = '';
   }
  else {
    $read = 'disabled';

  }


  // for storage
  if(isset($data['typeOfBite'])){
      if($data['typeOfBite'] == 'Bite') {
          $data['NonBiteCase'] = NULL;
          $data['NonBiteCaseOthers'] = NULL;
          $hiddennBite = "hidden";
      }
      if($data['typeOfBite'] == 'Non-Bite'){
          $data['SiteOfBite'] = NULL;
          $data['NatureOfBite'] = NULL;
          $data['NoOfBites'] = NULL;
          $data['Vicinity'] = NULL;
          $data['VicinityOthers'] = NULL;
          $data['BitingAnimal'] = NULL;
          $data['BitingAnimalOthers'] = NULL;
          $data['DateDied'] = NULL;
          $data['LabConfirmation'] = NULL;
          $data['LabDate'] = NULL;
          $data['LabName'] = NULL;
          $data['Vaccination'] = NULL;
          $data['vaccinationDateText'] = NULL;
          $data['WhoVaccinatedIt'] = NULL;
          $data['Ownership'] = NULL;
          $data['ContactWithOtherAnimal'] = NULL;
          $data['LocationOfContact'] = NULL;
          $data['ConditionBeforeBite'] = NULL;
          $data['Specie'] = NULL;
          $data['StatusOfAnimal'] = NULL;
          $data['DateOfVaccination'] = NULL;
          $hiddenBite = "hidden";
          $hiddenVicinity = "hidden";
          $hiddenAnimal = "hidden";
          $hiddenSentToLab = "hidden";
          $hiddenDead = "hidden";
          $hiddenVaccination = "hidden";
      }
    }
  else {
      $data['NonBiteCase'] = NULL;
      $data['NonBiteCaseOthers'] = NULL;
      $data['SiteOfBite'] = NULL;
      $data['NatureOfBite'] = NULL;
      $data['NoOfBites'] = NULL;
      $data['Vicinity'] = NULL;
      $data['VicinityOthers'] = NULL;
      $data['BitingAnimal'] = NULL;
      $data['BitingAnimalOthers'] = NULL;
      $data['DateDied'] = NULL;
      $data['LabConfirmation'] = NULL;
      $data['LabDate'] = NULL;
      $data['LabName'] = NULL;
      $data['Vaccination'] = NULL;
      $data['vaccinationDateText'] = NULL;
      $data['WhoVaccinatedIt'] = NULL;
      $data['Ownership'] = NULL;
      $data['ContactWithOtherAnimal'] = NULL;
      $data['LocationOfContact'] = NULL;
      $data['ConditionBeforeBite'] = NULL;
      $data['Specie'] = NULL;
      $data['StatusOfAnimal'] = NULL;
      $data['DateOfVaccination'] = NULL;
      $hiddenBite = "hidden";
      $hiddennBite = "hidden";
    }
?>

{!! Form::hidden('animalbite[fpservice_id]',$fpservice_id) !!}
{!! Form::hidden('animalbite[patient_id]',$patient_id) !!}
{!! Form::hidden('animalbite[hservice_id]',$hservice_id) !!}

    <fieldset>
      <legend>Type of Exposure and Date of Exposure</legend>
      <div class="col-md-12">
        <div class="col-md-6">
          <label class="col-sm-4 control-label">Type of Bite</label>
          <div class="col-sm-8">
            <div class="btn-group toggler" data-toggle="buttons">
              <label id="bite" class="btn btn-default <?php if($data['typeOfBite']=='Bite'){echo "active";}?>" >
                <i class="fa fa-check"></i> <input name="animalbite[typeOfBite]" type="radio" value="Bite" <?php if($data['typeOfBite']=='Bite'){echo "checked";}?>> Bite </label>
              <label id="nonBite" class="btn btn-default <?php if($data['typeOfBite']=='Non-Bite'){echo "active";}?>">
                <i class="fa fa-check"></i> <input name="animalbite[typeOfBite]" type="radio" value="Non-Bite" <?php if($data['typeOfBite']=='Non-Bite'){echo "checked";}?>> Non-Bite </label>
            </div>
          </div>
        </div>
          <div class="col-md-6">
            <label class="col-sm-4 control-label">Date of Exposure</label>
            <div class="col-sm-8">
              <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                    <input class="form-control datepicker_null" id="" name="animalbite[DateOfExposure]" type="text" value="{{ procDate($data['DateOfExposure']) }}">
              </div>
            </div>
          </div>
        </div>
      </fieldset>

    <!-- I. For Bite Cases -->
      <fieldset>
        <div id="forBiteCase" {{ $hiddenBite }}>
          <legend>For Bite Cases</legend>
          <div class="col-md-12">
            <div class="col-md-6">
              <label class="col-sm-4 control-label">Site of Bite:</label>
                <div class="col-sm-8" >
                  <input id="" type="text" class="form-control alphanumeric" placeholder="area of body bitten" name="animalbite[SiteOfBite]" onfocus="this.value=''" value="{{ $data['SiteOfBite'] or NULL }}"/>
                </div>
            </div>

            <div class="col-md-6">
              <label class="col-sm-4 control-label">Nature of Bite:</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control alphanumeric" placeholder="how were you bitten" name="animalbite[NatureOfBite]" onfocus="this.value=''" value="{{ $data['NatureOfBite'] or NULL }}"/>
                </div>
            </div>
          </div>

            <div class="col-md-12"><br></div>

          <div class="col-md-12">
            <div class="col-md-6">
              <label class="col-sm-4 control-label">No. Of Bites:</label>
              <div class="col-sm-8">
                <div class="btn-group toggler" data-toggle="buttons">
                  <label class="btn btn-default <?php if($data['NoOfBites']=='single'){echo "active";}?>" {{$read}}>
                    <i class="fa fa-check"></i> <input name="animalbite[NoOfBites]" type="radio" value="single" <?php if($data['NoOfBites']=='single'){echo "checked";}?>> Single </label>
                  <label class="btn btn-default <?php if($data['NoOfBites']=='multiple'){echo "active";}?> ">
                    <i class="fa fa-check"></i> <input name="animalbite[NoOfBites]" type="radio" value="multiple" <?php if($data['NoOfBites']=='multiple'){echo "active";}?>> Mutiple </label>
                </div>
              </div>
            </div>
          </div>


            <div class="col-md-12"><br></div>

            <div class="col-md-12">
              <div class="col-md-6">
                <label class="col-sm-4 control-label">Vicinity where bitten:</label>
                <div class="col-sm-8">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label id="VicinityHouse" class="btn btn-default <?php if($data['Vicinity']=='House'){echo "active";}?>" {{$read}}>
                      <i class="fa fa-check"></i> <input name="animalbite[vicinity]" type="radio" value="House" <?php if($data['Vicinity']=='House'){echo "checked";}?>> House </label>
                      <label id="VicinityNeighbor" class="btn btn-default <?php if($data['Vicinity']=='Neighbor'){echo "active";}?>" {{$read}}>
                        <i class="fa fa-check"></i> <input name="animalbite[vicinity]" type="radio" value="Neighbor" <?php if($data['Vicinity']=='Neighbor'){echo "checked";}?>> Neighbor </label>
                    <label id="VicinityOthers" class="btn btn-default <?php if($data['Vicinity']=='Others'){echo "active";}?>" {{$read}}>
                      <i class="fa fa-check"></i> <input name="animalbite[vicinity]" type="radio" value="Others" <?php if($data['Vicinity']=='Others'){echo "checked";}?>> Others </label>
                  </div>
                </div>
              </div>
              <div id="dvVicinityOthers" class="col-md-6" {{$hiddenVicinity}}>
                <label class="col-sm-4 control-label">Pls. Specify:</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control alphanumeric" placeholder="Specify" name="animalbite[VicinityOthers]" onfocus="this.value=''" value="{{ $data['VicinityOthers'] or NULL }}"/>
                </div>
              </div>
          </div>

            <div class="col-md-12"><br></div>

            <div class="col-md-12">
              <div class="col-md-6">
                <label class="col-sm-4 control-label">Biting Animal:</label>
                <div class="col-sm-8">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label id="Dog" class="btn btn-default <?php if($data['BitingAnimal']=='Dog'){echo "active";}?>" {{$read}}>
                      <i class="fa fa-check"></i> <input name="animalbite[BitingAnimal]" type="radio" value="Dog" <?php if($data['BitingAnimal']=='Dog'){echo "checked";}?>> Dog </label>
                    <label id="Cat" class="btn btn-default <?php if($data['BitingAnimal']=='Cat'){echo "active";}?>" {{$read}}>
                      <i class="fa fa-check"></i> <input name="animalbite[BitingAnimal]" type="radio" value="Cat" <?php if($data['BitingAnimal']=='Cat'){echo "checked";}?>> Cat </label>
                    <label id="Monkey" class="btn btn-default <?php if($data['BitingAnimal']=='Monkey'){echo "active";}?>" {{$read}}>
                      <i class="fa fa-check"></i> <input name="animalbite[BitingAnimal]" type="radio" value="Monkey" <?php if($data['BitingAnimal']=='Monkey'){echo "checked";}?>> Monkey </label>
                    <label id="OtherAnimal" class="btn btn-default <?php if($data['BitingAnimal']=='Others'){echo "active";}?>" {{$read}}>
                      <i class="fa fa-check"></i> <input name="animalbite[BitingAnimal]" type="radio" value="Others" <?php if($data['BitingAnimal']=='Others'){echo "checked";}?>> Others </label>
                  </div>
                </div>
              </div>

            <div id="dvAnimalOthers" {{$hiddenAnimal}}>
              <div class="col-md-6">
                <label class="col-sm-4 control-label">Pls. Specify:</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control alphanumeric" placeholder="Specify" name="animalbite[BitingAnimalOthers]" onfocus="this.value=''" value="{{ $data['BitingAnimalOthers'] or NULL }}"/>
                </div>
              </div>
            </div>
          </div>

           <div class="col-md-12"><br></div>

           <div class="col-md-12">
             <div class="col-md-6">
                  <label class="col-sm-4 control-label">Specie:</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control alphanumeric" placeholder="Specie" name="animalbite[Specie]" onfocus="this.value=''" value="{{ $data['Specie'] or NULL }}"/>
                </div>
             </div>
          </div>

           <div class="col-md-12"><br></div>

         <div class="col-md-12">
           <div class="col-md-6">
             <label class="col-sm-4 control-label">Status of Biting Animal:</label>
             <div class="col-sm-8">
               <div class="btn-group toggler" data-toggle="buttons">
                 <label id="Alive" class="btn btn-default <?php if($data['StatusOfAnimal']=='Alive'){echo "active";}?>" {{$read}}>
                   <i class="fa fa-check"></i> <input name="animalbite[StatusOfAnimal]" type="radio" value="Alive" <?php if($data['StatusOfAnimal']=='Alive'){echo "checked";}?>> Alive </label>
                 <label id="Unknown" class="btn btn-default <?php if($data['StatusOfAnimal']=='Unknown'){echo "active";}?>" {{$read}}>
                   <i class="fa fa-check"></i> <input name="animalbite[StatusOfAnimal]" type="radio" value="Unknown" <?php if($data['StatusOfAnimal']=='Unknown'){echo "checked";}?>> Unknown </label>
                 <label id="Dead" class="btn btn-default <?php if($data['StatusOfAnimal']=='Dead'){echo "active";}?>" {{$read}}>
                   <i class="fa fa-check"></i> <input name="animalbite[StatusOfAnimal]" type="radio" value="Dead" <?php if($data['StatusOfAnimal']=='Dead'){echo "checked";}?>> Died </label>
               </div>
             </div>
           </div>
            <div id="DateDied" {{$hiddenDead}}>
              <div class="col-md-6">
                <label class="col-sm-4 control-label">Date Died:</label>
                  <div class="col-sm-8">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                        <input class="form-control datepicker_null" id="" name="animalbite[DateDied]" type="text" value="{{ procDate($data['DateDied']) }}">
                    </div>
                  </div>
              </div>
            </div>
          </div>

            <div class="col-md-12"><br></div>

            <div id="ItDied" {{$hiddenDead}}>
              <div class="col-md-12">
                <div class="col-md-6">
                  <label class="col-sm-4 control-label">If died: Head sent for Laboratory Confirmation:</label>
                  <div class="col-sm-4">
                    <div class="btn-group toggler" data-toggle="buttons">
                      <label id="ItDiedYes" class="btn btn-default <?php if($data['LabConfirmation']=='Yes'){echo "active";}?>" {{$read}}>
                        <i class="fa fa-check"></i> <input name="animalbite[labConfirm]" type="radio" value="Yes"  <?php if($data['LabConfirmation']=='Yes'){echo "checked";}?>> Yes </label>
                      <label id="ItDiedNo" class="btn btn-default <?php if($data['LabConfirmation']=='No'){echo "active";}?>" {{$read}}>
                        <i class="fa fa-check"></i> <input id="11" name="animalbite[labConfirm]" type="radio" value="No" <?php if($data['LabConfirmation']=='No'){echo "checked";}?>> No </label>
                    </div>
                  </div>
                </div>
              </div>

            <div class="col-md-12"><br></div>

            <div id="SentToLab" {{$hiddenSentToLab}}>
              <div class="col-md-12">
                  <div class="col-md-6">
                    <label class="col-sm-4 control-label">Date:</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                            <input class="form-control datepicker_null" id="" name="animalbite[labDate]" type="text" onfocus="this.value=''" value="{{ procDate($data['LabDate']) }}">
                        </div>
                      </div>
                    </div>

                  <div class="col-md-6">
                    <label class="col-sm-4 control-label">Name of Laboratory:</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control alphanumeric" placeholder="Specify" name="animalbite[labName]" onfocus="this.value=''" value="{{ $data['LabName'] or NULL }}"/>
                      </div>
                  </div>
              </div>
            </div>
          </div>

          <div class="col-md-12"><br></div>

          <div class="col-md-12">
            <div class="col-md-6">
              <label class="col-sm-4 control-label">Anti-Rabies Vaccination of Biting Animal:</label>
              <div class="col-sm-4">
                <div class="btn-group toggler" data-toggle="buttons">
                  <label id="YesVaccinated" class="btn btn-default <?php if($data['Vaccination']=='Yes'){echo "active";}?>" {{$read}}>
                      <i class="fa fa-check"></i> <input name="animalbite[antiRabiesVaccination]" type="radio" value="Yes" <?php if($data['Vaccination']=='Yes'){echo "checked";}?>> Yes </label>
                  <label id="NoVaccinated" class="btn btn-default <?php if($data['Vaccination']=='No'){echo "active";}?>" {{$read}}>
                      <i class="fa fa-check"></i> <input name="animalbite[antiRabiesVaccination]" type="radio" value="No" <?php if($data['Vaccination']=='No'){echo "checked";}?>> No </label>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12"><br></div>

          <div id="Vaccination" {{$hiddenVaccination}}>
            <div class="col-md-12">
                <div class="col-md-6">
                    <label class="col-sm-4 control-label">If yes, when:</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control alphanumeric" placeholder="Specify" name="animalbite[vaccinationDateText]" onfocus="this.value=''" value="{{ $data['vaccinationDateText'] or NULL }}"/>
                      </div>
                </div>

                <div class="col-md-6">
                    <label class="col-sm-4 control-label">Date of Vaccination:</label>
                      <div class="col-sm-8">
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                              <input class="form-control datepicker_null" id="" name="animalbite[DateOfVaccination]" type="text" onfocus="this.value=''" value="{{ procDate($data['DateOfVaccination']) }}">
                          </div>
                      </div>
                </div>
            </div>

                <div class="col-md-12"><br></div>

                <div class="col-md-12">
                  <div class="col-md-6">
                    <label class="col-sm-4 control-label">By whom:</label>
                    <div class="col-sm-8">
                      <div class="btn-group toggler" data-toggle="buttons">
                        <label class="btn btn-default <?php if($data['WhoVaccinatedIt']=='Government'){echo "active";}?>" {{$read}}>
                            <i class="fa fa-check"></i> <input name="animalbite[whoVaccinatedIt]" type="radio" value="Government" <?php if($data['WhoVaccinatedIt']=='Government'){echo "checked";}?>> Government </label>
                        <label class="btn btn-default <?php if($data['WhoVaccinatedIt']=='Private'){echo "active";}?>" {{$read}}>
                            <i class="fa fa-check"></i> <input name="animalbite[whoVaccinatedIt]" type="radio" value="Private" <?php if($data['WhoVaccinatedIt']=='Private'){echo "checked";}?>> Private </label>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="col-md-12"><br></div>

            <div class="col-md-12">
              <div class="col-md-6">
                <label class="col-sm-4 control-label">Ownership status:</label>
                <div class="col-sm-8">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label class="btn btn-default <?php if($data['Ownership']=='Pet'){echo "active";}?>" {{$read}}>
                        <i class="fa fa-check"></i> <input name="animalbite[ownershipStatus]" type="radio" value="Pet" <?php if($data['Ownership']=='Pet'){echo "checked";}?>> Pet </label>
                    <label class="btn btn-default <?php if($data['Ownership']=='Neighbor'){echo "active";}?>" {{$read}}>
                        <i class="fa fa-check"></i> <input name="animalbite[ownershipStatus]" type="radio" value="Neighbor" <?php if($data['Ownership']=='Neighbor'){echo "checked";}?>> Neighbor </label>
                    <label class="btn btn-default <?php if($data['Ownership']=='Stray'){echo "active";}?>" {{$read}}>
                        <i class="fa fa-check"></i> <input name="animalbite[ownershipStatus]" type="radio" value="Stray" <?php if($data['Ownership']=='Stray'){echo "checked";}?>> Stray </label>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <label class="col-sm-4 control-label">In contact with other animal:</label>
                <div class="col-sm-8">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label class="btn btn-default <?php if($data['ContactWithOtherAnimal']=='Yes'){echo "active";}?>" {{$read}}>
                        <i class="fa fa-check"></i> <input name="animalbite[ContactWithOtherAnimal]" type="radio" value="Yes" <?php if($data['ContactWithOtherAnimal']=='Yes'){echo "checked";}?>> Yes </label>
                    <label class="btn btn-default <?php if($data['ContactWithOtherAnimal']=='No'){echo "active";}?>" {{$read}}>
                        <i class="fa fa-check"></i> <input name="animalbite[ContactWithOtherAnimal]" type="radio" value="No" <?php if($data['ContactWithOtherAnimal']=='No'){echo "checked";}?>> No </label>
                  </div>
                </div>
              </div>
            </div>

              <div class="col-md-12"><br></div>

              <div class="col-md-12">
                <div class="col-md-6">
                  <label class="col-sm-4 control-label">Where?:</label>
                  <div class="col-sm-8">
                    <div class="btn-group toggler" data-toggle="buttons">
                      <label class="btn btn-default <?php if($data['LocationOfContact']=='Household'){echo "active";}?>" {{$read}}>
                          <i class="fa fa-check"></i> <input name="animalbite[LocationOfContact]" type="radio" value="Household" <?php if($data['LocationOfContact']=='Household'){echo "checked";}?>> Household </label>
                      <label class="btn btn-default <?php if($data['LocationOfContact']=='Neighborhood'){echo "active";}?>" {{$read}}>
                          <i class="fa fa-check"></i> <input name="animalbite[LocationOfContact]" type="radio" value="Neighborhood" <?php if($data['LocationOfContact']=='Neighborhood'){echo "checked";}?>> Neighborhood </label>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <label class="col-sm-4 control-label">Condition before bite:</label>
                  <div class="col-sm-8">
                    <div class="btn-group toggler" data-toggle="buttons">
                      <label class="btn btn-default <?php if($data['ConditionBeforeBite']=='Sick'){echo "active";}?>" {{$read}}>
                          <i class="fa fa-check"></i> <input name="animalbite[condition]" type="radio" value="Sick" <?php if($data['ConditionBeforeBite']=='Sick'){echo "checked";}?>> Sick </label>
                      <label class="btn btn-default <?php if($data['ConditionBeforeBite']=='Healthy'){echo "active";}?>" {{$read}}>
                          <i class="fa fa-check"></i> <input name="animalbite[condition]" type="radio" value="Healthy" <?php if($data['ConditionBeforeBite']=='Healthy'){echo "checked";}?>> Healthy </label>
                      <label class="btn btn-default <?php if($data['ConditionBeforeBite']=='Unknown'){echo "active";}?>" {{$read}}>
                          <i class="fa fa-check"></i> <input name="animalbite[condition]" type="radio" value="Unknown" <?php if($data['ConditionBeforeBite']=='Unknown'){echo "checked";}?>> Unknown </label>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </fieldset>


    <div class="icheck">
      <fieldset>
        <!-- II. For Non Bite Case -->

        <div id="forNonBiteCase" {{ $hiddennBite }}>
          <legend>For Non-Bite Cases</legend>
          <div class="col-md-12">
            <div class="col-sm-12">
                  <label id="NBite1" class=" <?php if($data['NonBiteCase'] == 'A') {echo 'active';} ?>" {{$read}}>
                      <input name="animalbite[nonBiteCase]" type="radio" value="A" <?php if($data['NonBiteCase']=='A'){echo "checked";}?> {{$read}}>exposure to a rabid patient
                  </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="col-sm-12">
                  <label id="NBite2" class=" <?php if($data['NonBiteCase'] == 'B') {echo 'active';} ?>" {{$read}}>
                      <input name="animalbite[nonBiteCase]" type="radio" value="B" <?php if($data['NonBiteCase']=='B'){echo "checked";}?> {{$read}}>handling of infected carcass
                  </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="col-sm-12">
                  <label id="NBite3" class=" <?php if($data['NonBiteCase'] == 'C') {echo 'active';} ?>" {{$read}}>
                      <input name="animalbite[nonBiteCase]" type="radio" value="C" <?php if($data['NonBiteCase']=='C'){echo "checked";}?> {{$read}}>ingestion of raw infected meat
                  </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="col-sm-2">
                  <label id="NBite4" class=" <?php if($data['NonBiteCase'] == 'D') {echo 'active';} ?>" {{$read}}>
                      <input name="animalbite[nonBiteCase]" type="radio" value="D" <?php if($data['NonBiteCase']=='D'){echo "checked";}?> {{$read}}>Others, Pls. Specify
                  </label>
            </div>
            <div class="col-sm-3">
                    <input type="text" class="form-control alphanumeric" placeholder="Specify" name="animalbite[NonBiteCaseOthers]" onfocus="this.value=''" value="{{ $data['NonBiteCaseOthers'] or NULL }}"/>
            </div>
          </div>
        </div>

      </fieldset>
    </div>
    <!-- III Action Taken After the Bite Incident -->
      <fieldset>
        <legend>Actions Taken After the Bite Incident</legend>
          <div class="col-md-6">
            <label class="col-sm-3 control-label">Wound care done:</label>
            <div class="col-md-4">
              <div class="btn-group toggler" data-toggle="buttons">
                <label id="Yes" class="btn btn-default <?php if($data['WoundCare'] == "Yes") {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i><input name="animalbite[WoundCare]" type="radio" id="" value="Yes" <?php if($data['WoundCare'] == "Yes") {echo 'checked';} ?>>Yes
                </label>
                <label id="No" class="btn btn-default <?php if($data['WoundCare'] == "No") {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i><input name="animalbite[WoundCare]" type="radio" id="" value="No" <?php if($data['WoundCare'] == "No") {echo 'checked';} ?>>No
                </label>
              </div>
            </div>
          </div>

        <div class="col-md-12"></div>

        <div class="icheck">
          <div id="dvWoundCare" {{ $hiddenWoundCare }}>
            <div class="col-md-6">
              <dl class="col-sm-12">
                <dt> If yes: </dt>
                <dd>
                  <div class="checkbox col-sm-12">
                    <label class="col-sm-12">
                      <input type="checkbox" {{ $read }} name="animalbite[YesWoundCare][]" value="A"
                        <?php
                            foreach(explode(",", $data['YesWoundCare']) as $value)
                            {
                                if($value=="A")
                                {
                                    echo "checked";
                                }
                            }
                        ?>
                       > wound washed & flushed with soap and water
                    </label>
                  </div>
                <div class="checkbox col-sm-12">
                    <label class="col-sm-12">
                      <input type="checkbox" {{ $read }} name="animalbite[YesWoundCare][]" value="B"
                        <?php
                            foreach(explode(",", $data['YesWoundCare']) as $value)
                            {
                                if($value=="B")
                                {
                                    echo "checked";
                                }
                            }
                        ?>
                       > disinfectants applied-providone iodine, alcohol
                    </label>
                </div>
                <div class="checkbox col-sm-12">
                    <label class="col-sm-6">
                      <input type="checkbox" {{ $read }} name="animalbite[YesWoundCare][]"  value="C"
                        <?php
                            foreach(explode(",", $data['YesWoundCare']) as $value)
                            {
                                if($value=="C")
                                {
                                    echo "checked";
                                }
                            }
                        ?>
                       > used garlic, vinegar, papaya, others,
                    </label>
                    <div id="otherWoundCare" class="col-sm-6">
                      <input type="text" class="form-control alphanumeric" placeholder="Specify" name="animalbite[YesWoundCareOthers]" onfocus="this.value=''" value="{{ $data['YesWoundCareOthers'] or NULL }}"/>
                    </div>
                </div>
              </dd>
          </dl>
        </div>
      </div>

      <div class="col-md-12"><br></div>

        <div class="col-md-6">
          <dl class="col-sm-12">
              <dt> Incident Reported to: </dt>
              <dd>
                <div class="checkbox col-sm-12">
                    <label class="col-sm-12">
                      <input type="checkbox" {{ $read }} name="animalbite[IncidentReported][]" id="" value="MHO"
                        <?php
                            foreach(explode(",", $data['IncidentReported']) as $value)
                            {
                                if($value=="MHO")
                                {
                                    echo "checked";
                                }
                            }
                        ?>
                       > MHO
                    </label>
                </div>
                <div class="checkbox col-sm-12">
                    <label class="col-sm-6">
                      <input type="checkbox" {{ $read }} name="animalbite[IncidentReported][]" id="" value="MO"
                        <?php
                            foreach(explode(",", $data['IncidentReported']) as $value)
                            {
                                if($value=="MO")
                                {
                                    echo "checked";
                                }
                            }
                        ?>
                       > Municipal official, Pls. Identify:
                    </label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control alphanumeric" placeholder="Specify" name="animalbite[ReportedToMunicipalOfficial]" onfocus="this.value=''" value="{{ $data['ReportedToMunicipalOfficial'] or NULL }}"/>
                  </div>
                </div>
                <div class="checkbox col-sm-12">
                    <label class="col-sm-6">
                      <input type="checkbox" {{ $read }} name="animalbite[IncidentReported][]" id="" value="BO"
                        <?php
                            foreach(explode(",", $data['IncidentReported']) as $value)
                            {
                                if($value=="BO")
                                {
                                    echo "checked";
                                }
                            }
                        ?>
                       > Barangay Official, Pls. Identify:
                    </label>
                  <div class="col-sm-6">
                      <input type="text" class="form-control alphanumeric" placeholder="Specify" name="animalbite[ReportedToBarangayOfficial]" onfocus="this.value=''" value="{{ $data['ReportedToBarangayOfficial'] or NULL }}"/>
                  </div>
                </div>
                 <div class="checkbox col-sm-12">
                     <label class="col-sm-6">
                       <input type="checkbox" {{ $read }} name="animalbite[IncidentReported][]" id="" value="HP"
                         <?php
                             foreach(explode(",", $data['IncidentReported']) as $value)
                             {
                                 if($value=="HP")
                                 {
                                     echo "checked";
                                 }
                             }
                         ?>
                        > Health personnel, Pls. Identify:
                     </label>
                   <div class="col-sm-6">
                       <input type="text" class="form-control alphanumeric" placeholder="Specify" name="animalbite[ReportedToHealthPersonnel]" onfocus="this.value=''" value="{{ $data['ReportedToHealthPersonnel'] or NULL }}"/>
                   </div>
                 </div>
                 <div class="checkbox col-sm-12">
                     <label class="col-sm-6">
                       <input type="checkbox" {{ $read }} name="animalbite[IncidentReported][]" id="" value="O"
                         <?php
                             foreach(explode(",", $data['IncidentReported']) as $value)
                             {
                                 if($value=="O")
                                 {
                                     echo "checked";
                                 }
                             }
                         ?>
                        > Others:
                     </label>
                   <div class="col-sm-6">
                       <input type="text" class="form-control alphanumeric" placeholder="Specify" name="animalbite[ReportedToOthers]" onfocus="this.value=''" value="{{ $data['ReportedToOthers'] or NULL }}"/>
                   </div>
                </div>
              </dd>
          </dl>
        </div>
        <div class="col-md-6">
          <dl class="col-sm-12">
              <dt> Consulted: </dt>
              <dd>
                  <div class="checkbox col-sm-12">
                      <label>
                        <input type="checkbox" {{ $read }} name="animalbite[Consulted][]" id="" value="Tandok"
                          <?php
                              foreach(explode(",", $data['Consulted']) as $value)
                              {
                                  if($value=="Tandok")
                                  {
                                      echo "checked";
                                  }
                              }
                          ?>
                        > Tandok
                      </label>
                  </div>
                  <div class="checkbox col-sm-12">
                      <label>
                        <input type="checkbox" {{ $read }} name="animalbite[Consulted][]" id="" value="HealthWorker"
                          <?php
                              foreach(explode(",", $data['Consulted']) as $value)
                              {
                                  if($value=="HealthWorker")
                                  {
                                      echo "checked";
                                  }
                              }
                          ?>
                         > Health Worker
                      </label>
                  </div>
              </dd>
          </dl>
        </div>
      </div>
    </fieldset>
    <!-- IV. Category of Exposure-->
      <fieldset>
        <legend>Category of Exposure</legend>
        <div class="col-md-12">
          <div class="col-md-6">
            <label class="col-sm-5 control-label">Category Of Exposure:</label>
            <div class="col-sm-4">
              <div class="btn-group toggler" data-toggle="buttons">
                <label class="btn btn-default <?php if($data['CategoryOfExposure']=='1'){echo "active";}?>" >
                  <i class="fa fa-check"></i> <input name="animalbite[CategoryOfExposure]" type="radio" value="1" <?php if($data['CategoryOfExposure']=='1'){echo "checked";}?>> 1 </label>
                <label class="btn btn-default <?php if($data['CategoryOfExposure']=='2'){echo "active";}?>" >
                  <i class="fa fa-check"></i> <input name="animalbite[CategoryOfExposure]" type="radio" value="2" <?php if($data['CategoryOfExposure']=='2'){echo "checked";}?>> 2 </label>
                <label class="btn btn-default <?php if($data['CategoryOfExposure']=='3'){echo "active";}?>" >
                  <i class="fa fa-check"></i> <input name="animalbite[CategoryOfExposure]" type="radio" value="3" <?php if($data['CategoryOfExposure']=='3'){echo "checked";}?>> 3 </label>
              </div>
            </div>
          </div>


          <div class="col-md-6">
            <label class="col-sm-5 control-label">Route of Administration:</label>
            <div class="col-sm-4">
              <div class="btn-group toggler" data-toggle="buttons">
                <label class="btn btn-default <?php if($data['RouteOfAdmin']=='ID'){echo "active";}?>" >
                  <i class="fa fa-check"></i> <input name="animalbite[RouteOfAdmin]" type="radio" value="ID" <?php if($data['RouteOfAdmin']=='ID'){echo "checked";}?>> ID </label>
                <label class="btn btn-default <?php if($data['RouteOfAdmin']=='IM'){echo "active";}?>" >
                  <i class="fa fa-check"></i> <input name="animalbite[RouteOfAdmin]" type="radio" value="IM" <?php if($data['RouteOfAdmin']=='IM'){echo "checked";}?>> IM </label>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-12"><br></div>

        <div class="col-md-12">
          <div class="col-md-6">
          <label class="col-sm-5 control-label">History of Past Anti-Rabies Vaccination:</label>
          <div class="col-sm-4">
            <div class="btn-group toggler" data-toggle="buttons">
              <label id="YesPastVaccination" class="btn btn-default <?php if($data['HistoryOfVaccination']=='Yes'){echo "active";}?>" >
                <i class="fa fa-check"></i> <input name="animalbite[HistoryOfVaccination]" type="radio" value="Yes" <?php if($data['HistoryOfVaccination']=='Yes'){echo "checked";}?>> Yes </label>
              <label id="NoPastVaccination" class="btn btn-default <?php if($data['HistoryOfVaccination']=='No'){echo "active";}?>" >
                <i class="fa fa-check"></i> <input name="animalbite[HistoryOfVaccination]" type="radio" value="No" <?php if($data['HistoryOfVaccination']=='No'){echo "checked";}?>> No </label>
            </div>
          </div>
        </div>


          <div class="col-md-6">
            <label class="col-sm-5 control-label">By whom:</label>
            <div class="col-sm-4">
              <div class="btn-group toggler" data-toggle="buttons">
                <label class="btn btn-default <?php if($data['WhoGaveTheVaccine']=='Government'){echo "active";}?>" >
                  <i class="fa fa-check"></i> <input name="animalbite[WhoGaveTheVaccine]" type="radio" value="Government" <?php if($data['WhoGaveTheVaccine']=='Government'){echo "checked";}?>> Government </label>
                <label class="btn btn-default <?php if($data['WhoGaveTheVaccine']=='Private'){echo "active";}?>" >
                  <i class="fa fa-check"></i> <input name="animalbite[WhoGaveTheVaccine]" type="radio" value="Private" <?php if($data['WhoGaveTheVaccine']=='Private'){echo "checked";}?>> Private </label>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12"><br></div>

          <div id="PastVaccination" {{$hiddenPastvaccination}}>
            <div class="col-md-12">
              <div class="col-md-6">
                <label class="col-sm-5 control-label">If yes:</label>
                <div class="col-sm-4">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label class="btn btn-default <?php if($data['YesPastVaccination']=='PEP'){echo "active";}?>" >
                      <i class="fa fa-check"></i> <input name="animalbite[YesPastVaccination]" type="radio" value="PEP" <?php if($data['YesPastVaccination']=='PEP'){echo "checked";}?>> PEP </label>
                    <label class="btn btn-default <?php if($data['YesPastVaccination']=='Prep'){echo "active";}?>" >
                      <i class="fa fa-check"></i> <input name="animalbite[YesPastVaccination]" type="radio" value="Prep" <?php if($data['YesPastVaccination']=='Prep'){echo "checked";}?>> Prep </label>
                  </div>
                </div>
              </div>


              <div class="col-md-6">
                <label class="col-sm-5 control-label">Date of last dose:</label>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                        <input class="form-control datepicker_null" id="" name="animalbite[DateOfLastVaccination]" type="text" value="{{ procDate($data['DateOfLastVaccination']) }}">
                    </div>
                  </div>
              </div>
            </div>

            <div class="col-md-12"></br></div>

          <div class="icheck col-md-6">
            <dl class="col-sm-5">
                <dt> Vaccine Received: </dt>
                <dd>
                    <div class="checkbox col-sm-12">
                        <label>
                          <input type="checkbox" {{ $read }} name="animalbite[VaccineReceived][]" id="" value="PVRV"
                            <?php
                                foreach(explode(",", $data['VaccineReceived']) as $value)
                                {
                                    if($value=="PVRV")
                                    {
                                        echo "checked";
                                    }
                                }
                            ?>
                          > PVRV
                        </label>
                    </div>
                    <div class="checkbox col-sm-12">
                        <label>
                          <input type="checkbox" {{ $read }} name="animalbite[VaccineReceived][]" id="" value="PCECV"
                            <?php
                                foreach(explode(",", $data['VaccineReceived']) as $value)
                                {
                                    if($value=="PCECV")
                                    {
                                        echo "checked";
                                    }
                                }
                            ?>
                          > PCECV
                        </label>
                    </div>
                    <div class="checkbox col-sm-12">
                        <label>
                          <input type="checkbox" {{ $read }} name="animalbite[VaccineReceived][]" id="" value="ERIG"
                            <?php
                                foreach(explode(",", $data['VaccineReceived']) as $value)
                                {
                                    if($value=="ERIG")
                                    {
                                        echo "checked";
                                    }
                                }
                            ?>
                          > ERIG
                        </label>
                    </div>
                    <div class="checkbox col-sm-12">
                        <label>
                          <input type="checkbox" {{ $read }} name="animalbite[VaccineReceived][]" id="" value="HRIG"
                            <?php
                                foreach(explode(",", $data['VaccineReceived']) as $value)
                                {
                                    if($value=="HRIG")
                                    {
                                        echo "checked";
                                    }
                                }
                            ?>
                          > HRIG
                        </label>
                    </div>
                </dd>
            </dl>
          </div>
        </div>
      </fieldset>

<!-- V. Management -->
      <fieldset>
        <legend>Management</legend>
        <div class="col-md-12">
          <div class="col-md-2">
            <label class="col-sm-4 control-label">Management:</label>
          </div>
          <div class="col-md-10 ui-widget">
            {!! Form::textarea('Management', $data['Management'], ['class' => 'form-control noresize', 'placeholder' => 'Management', 'cols'=>'15', 'rows'=>'7', $read]) !!}
          </div>
        </div>
      </fieldset>

<!-- Take Home Instructions  -->

      <fieldset>
        <legend>Take Home Instructions</legend>
          <div class="icheck col-md-12">
            <dl class="col-sm-12">
                <dd>
                    <div class="checkbox col-sm-12">
                        <label>
                          <input type="checkbox" {{ $read }} name="animalbite[TakeHomeInstrcutions][]" id="" value="ScheduleFollowUpDose"
                            <?php
                                foreach(explode(",", $data['TakeHomeInstrcutions']) as $value)
                                {
                                    if($value=="ScheduleFollowUpDose")
                                    {
                                        echo "checked";
                                    }
                                }
                            ?>
                          > Schedule of follow-up doses should be stricly followed
                        </label>
                    </div>
                    <div class="checkbox col-sm-12">
                        <label>
                          <input type="checkbox" {{ $read }} name="animalbite[TakeHomeInstrcutions][]" id="" value="ObserveAnimal"
                            <?php
                                foreach(explode(",", $data['TakeHomeInstrcutions']) as $value)
                                {
                                    if($value=="ObserveAnimal")
                                    {
                                        echo "checked";
                                    }
                                }
                            ?>
                          > Advise to observe dog for 14 days and return immediately & report when dog died, lost or cannot be observed anymore
                        </label>
                    </div>
                    <div class="checkbox col-sm-12">
                        <label>
                          <input type="checkbox" {{ $read }} name="animalbite[TakeHomeInstrcutions][]" id="" value="IncidentReport"
                            <?php
                                foreach(explode(",", $data['TakeHomeInstrcutions']) as $value)
                                {
                                    if($value=="IncidentReport")
                                    {
                                        echo "checked";
                                    }
                                }
                            ?>
                          > If incident not reported- advised to report to Punong Barangay and Health worker
                        </label>
                    </div>
                    <div class="checkbox col-sm-12">
                        <label>
                          <input type="checkbox" {{ $read }} name="animalbite[TakeHomeInstrcutions][]" id="" value="SendAnimalToLab"
                            <?php
                                foreach(explode(",", $data['TakeHomeInstrcutions']) as $value)
                                {
                                    if($value=="SendAnimalToLab")
                                    {
                                        echo "checked";
                                    }
                                }
                            ?>
                          > Advise to send biting animals' head to laboratory for confirmation whenever animal dies within observation period
                        </label>
                    </div>
                    <div class="checkbox col-sm-12">
                        <label>
                          <input type="checkbox" {{ $read }} name="animalbite[TakeHomeInstrcutions][]" id="" value="ReferredToPCSO"
                            <?php
                                foreach(explode(",", $data['TakeHomeInstrcutions']) as $value)
                                {
                                    if($value=="ReferredToPCSO")
                                    {
                                        echo "checked";
                                    }
                                }
                            ?>
                          > Referred to PCSO/Mayor for RIG
                        </label>
                    </div>
                    <div class="checkbox col-sm-12">
                        <label>
                          <input type="checkbox" {{ $read }} name="animalbite[TakeHomeInstrcutions][]" id="" value="ReferredToRHU"
                            <?php
                                foreach(explode(",", $data['TakeHomeInstrcutions']) as $value)
                                {
                                    if($value=="ReferredToRHU")
                                    {
                                        echo "checked";
                                    }
                                }
                            ?>
                          > Referred to RHU for Anti-Tetanus prophylaxis
                        </label>
                    </div>
                </dd>
            </dl>
          </div>
      </fieldset>

<!-- end div -->
  <!-- function for hiding forms prototype 1. to be redesigned and made into an efficient one -->    
  {!! HTML::script('public/dist/plugins/jQuery/jQuery-2.1.4.min.js') !!}
  <script type="text/javascript">
  //hide script for the bite and nonbite forms
  $(document).ready(function(){
      $("#bite").click(function(){
          $("#forBiteCase").show();
          $("#forNonBiteCase").hide();
      });
      $("#nonBite").click(function(){
          $("#forBiteCase").hide();
          $("#forNonBiteCase").show();
      });
      $("#Yes").click(function(){
          $("#dvWoundCare").show();
      });
      $("#No").click(function(){
          $("#dvWoundCare").hide();
      });
      $("#VicinityHouse, #VicinityNeighbor").click(function(){
          $("#dvVicinityOthers").hide();
      });
      $("#VicinityOthers").click(function(){
          $("#dvVicinityOthers").show();
      });
      $("#Dog, #Cat, #Monkey").click(function(){
          $("#dvAnimalOthers").hide();
      });
      $("#OtherAnimal").click(function(){
          $("#dvAnimalOthers").show();
      });
      $("#Alive, #Unknown").click(function(){
          $("#ItDied").hide();
          $("#DateDied").hide();
      });
      $("#Dead").click(function(){
          $("#ItDied").show();
          $("#DateDied").show();
      });
      $("#ItDiedYes").click(function(){
          $("#SentToLab").show();
      });
      $("#ItDiedNo").click(function(){
          $("#SentToLab").hide();
      });
      $("#YesVaccinated").click(function(){
          $("#Vaccination").show();
      });
      $("#NoVaccinated").click(function(){
          $("#Vaccination").hide();
      });
      $("#YesPastVaccination").click(function(){
          $("#PastVaccination").show();
      });
      $("#NoPastVaccination").click(function(){
          $("#PastVaccination").hide();
      });
  });
  </script>
