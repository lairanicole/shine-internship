<?php

namespace Plugins\Tuberculosis;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class TuberculosisClinicalHistoryModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tuberculosis_clinical_history_record';
    protected static $table_name = 'tuberculosis_clinical_history_record';
    protected $primaryKey = 'tuberculosis_clinical_history_id';

    protected $fillable = [];
    protected $touches = array('tuberculosis');
    public function tuberculosis()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\Tuberculosis\TuberculosisModel','tb_record_number','tb_record_number');
    }


}
