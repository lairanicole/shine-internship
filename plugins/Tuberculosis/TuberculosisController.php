<?php

use src\ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Plugins\Tuberculosis\TuberculosisModel as Tuberculosis;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

use Plugins\Tuberculosis\TuberculosisClinicalHistoryModel as TuberculosisClinicalHistory;
use Plugins\Tuberculosis\TuberculosisDosagesPreparationsModel as TuberculosisDosagesPreparations;
use Plugins\Tuberculosis\TuberculosisDrugIntakeModel as TuberculosisDrugIntake;
use Plugins\Tuberculosis\TuberculosisDssmModel as TuberculosisDssm;


class TuberculosisController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
        $this->tbcase_id = Input::get('tbcase_id');
        $this->healthcareservice_id = Input::get('hservice_id');
        $this->tbservice_id = Input::get('tbservice_id');
    }

    public function index()
    {
        //no index
    }

    public function save($data)
    {
      $tuberculosis_id = Input::get('tbservice_id');
      $Tb_Record_Number = Input::get('Tb_Record_Number');
      $hservice_id = Input::get('hservice_id');
      if($Tb_Record_Number==NULL) { $Tb_Record_Number = Tuberculosis::select('tb_record_number')->where('healthcareservice_id', $hservice_id)->first(); }

      $tuberculosis_record = Tuberculosis::where('healthcareservice_id',$hservice_id)->first();
      // dd($tuberculosis_record);
      if($data!=FALSE) { //if this is an active TB record
        foreach($data as $key=>$d)
        {
            $EmptyTestArray = array_filter($d);

            if(!empty($EmptyTestArray)) {
                $func = 'save'.ucfirst($key);
                $this->$func($d);
            }
        }
        $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

      } else { //else new TB case
        if($tuberculosis_record==NULL) { 
          $tuberculosis_record = new Tuberculosis;
          $tuberculosis_record->tuberculosis_id = IdGenerator::generateId();
          
        } else {
          
          $tuberculosis_record->tuberculosis_id = $tuberculosis_record['tuberculosis_id'];
        }
          $tuberculosis_record->tb_record_number =$Tb_Record_Number;
          $tuberculosis_record->healthcareservice_id = $hservice_id;
          $tuberculosis_record->save();

        $patient_id = Input::get('patient_id');
      }

      header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
      exit;
    }

    // public function newRecord()
    // {
    //     $hservice_id = Input::get('hservice_id');
    //     $tuberculosis_id = Input::get('tbservice_id');
    //     $tuberculosis_record = Tuberculosis::find($tuberculosis_id);
    //     // $tuberculosis_record->is_saved = 1;

    //     $tuberculosis_record->tb_record_number = Input::get('Tb_Record_Number');

    //     $tuberculosis_record->save();

    //     // $flash_message = 'Tuberculosis Case Report was Saved!';

    //     // $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

    //     // header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
    //     // exit;
    // }

    public function saveDssm()
    {
      $hservice_id = Input::get('hservice_id');
      $tuberculosis_id = Input::get('tbservice_id');

      // dd(dd($this->tbcase_id));
      $tuberculosis_data = Tuberculosis::select('tb_record_number')->where('healthcareservice_id', $hservice_id)->first();

      $tb_dssm_data = TuberculosisDssm::where('tb_record_number', $this->tbcase_id)->count();

      // dd($this->tbcase_id);

      if ($tb_dssm_data > 0):
          $tuberculosis_dssm_record = TuberculosisDssm::where('tb_record_number', $this->tbcase_id)->first();
      else:
          $tuberculosis_dssm_record = new TuberculosisDssm();
          $tuberculosis_dssm_record->tb_record_number = IdGenerator::generateId();
          $tuberculosis_dssm_record->tb_record_number = $tuberculosis_data->tb_record_number;
      endif;


      $tuberculosis_dssm_record->tuberculosis_dssm_id = IdGenerator::generateId();
      $tuberculosis_dssm_record->category_of_treatment = Input::get('Category_Of_Treatment');
      $tuberculosis_dssm_record->tb_drugs_taken = Input::get('Tb_Drugs_Taken');
      $tuberculosis_dssm_record->month = Input::get('Month');
      $tuberculosis_dssm_record->date_examined = (new Datetime(Input::get('Date_Examined')))->format('Y-m-d H:i:s');
      $tuberculosis_dssm_record->result = Input::get('Dssm_Result');

      $tuberculosis_dssm_record->save();


      $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

      header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
      exit;
    }

    public function saveDiagnosticTestsTab($data)
    {

        $diagTests = $data;
        // dd($diagTests);

        $tuberculosis_id = Input::get('tbservice_id');
        $Tb_Record_Number = Input::get('Tb_Record_Number');
        $hservice_id = Input::get('hservice_id');
        if($Tb_Record_Number==NULL) { $Tb_Record_Number = Tuberculosis::select('tb_record_number')->where('healthcareservice_id', $hservice_id)->first(); }
        

        $tuberculosis_record = Tuberculosis::where('tb_record_number',$Tb_Record_Number)->where('healthcareservice_id',$hservice_id)->first();

        $tuberculosis_record->tb_record_number = $Tb_Record_Number;
        $tuberculosis_record->tuberculin_result = $diagTests['Tuberculin_Result'];
        $tuberculosis_record->tuberculin_date_read = $diagTests['Tuberculin_Date_Read'];

        $tuberculosis_record->cxr_findings = $diagTests['Cxr_Findings'];
        $tuberculosis_record->cxr_date_of_exam = (new Datetime($diagTests['Cxr_Date_Of_Exam']))->format('Y-m-d H:i:s');

        $tuberculosis_record->cxr_tbdc = $diagTests['Cxr_Tbdc'];
        $tuberculosis_record->xpert_results = $diagTests['Xpert_Results'];
        $tuberculosis_record->xpert_date_of_collection = (new Datetime($diagTests['Xpert_Date_Of_Collection']))->format('Y-m-d H:i:s');
        $tuberculosis_record->pict_done = $diagTests['Pict_Done'];
        $tuberculosis_record->pict_date_administered = (new Datetime($diagTests['Pict_Date_Administered']))->format('Y-m-d H:i:s');
        $tuberculosis_record->other_exam_exam_conducted = $diagTests['Other_Exam_Exam_Conducted'];
        $tuberculosis_record->other_exam_date_of_exam = (new Datetime($diagTests['Other_Exam_Date_Of_Exam']))->format('Y-m-d H:i:s');
        // dd($tuberculosis_record->other_exam_date_of_exam);
        $tuberculosis_record->bcg_scar = $diagTests['Bcg_Scar'];
        $tuberculosis_record->diagnosis_tuberculosis_type = $diagTests['Diagnosis_Tuberculosis_Type'];
        $tuberculosis_record->diagnosis_remarks = $diagTests['Diagnosis_Remarks'];

        $tuberculosis_record->save();

        // $flash_message = 'Tuberculosis Case Report was Saved!';

        // $patient_id = getPatientIDByHealthcareserviceID($hservice_id);
        // header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
        // exit;

        //return Redirect::back()
         //            ->with('flash_message', $flash_message)
           //          ->with('flash_type', 'alert-success')
             //        ->with('flash_tab', 'dengue');
    }

    public function saveTreatmentInformationTab($data)
    {
        $tInformation = $data;
        // dd($tInformation);
        $hservice_id = Input::get('hservice_id');
        $tuberculosis_id = Input::get('tbservice_id');
        $Tb_Record_Number = Input::get('Tb_Record_Number');
        if($Tb_Record_Number==NULL) { $Tb_Record_Number = Tuberculosis::select('tb_record_number')->where('healthcareservice_id', $hservice_id)->first(); }
        // dd(Input::get('tbservice_id'));
        // $tuberculosis_record = Tuberculosis::find($tuberculosis_id);
        $tuberculosis_record = Tuberculosis::where('tb_record_number',$Tb_Record_Number)->where('healthcareservice_id',$hservice_id)->first();
        $tuberculosis_record->tb_record_number = $Tb_Record_Number;
        $tuberculosis_record->tb_drugs_before = $tInformation['Tb_Drugs_Before'];
        $tuberculosis_record->tb_drugs_date_administered = (new Datetime($tInformation['Tb_Drugs_Date_Administered']))->format('Y-m-d H:i:s');
        $tuberculosis_record->tb_drugs_duration = $tInformation['Tb_Drugs_Duration'];

        // $tuberculosis_record->tb_drugs_taken = json_encode(Input::get('Tb_Drugs_Taken'));

        $tuberculosis_record->bacteriological_status = $tInformation['Bacteriological_Status'];
        $tuberculosis_record->anatomical_site = $tInformation['Anatomical_Site'];
        $tuberculosis_record->anatomical_site_specify = $tInformation['Anatomical_Site_Specify'];
        $tuberculosis_record->bacteriology_registration_group = $tInformation['Bacteriology_Registration_Group'];
        $tuberculosis_record->bacteriology_registration_group_specify = $tInformation['Bacteriology_Registration_Group_Specify'];
        $tuberculosis_record->treatment_outcome_date_started = (new Datetime($tInformation['Treatment_Outcome_Date_Started']))->format('Y-m-d H:i:s');
        $tuberculosis_record->treatment_outcome_date_last_intake = (new Datetime($tInformation['Treatment_Outcome_Date_Last_Intake']))->format('Y-m-d H:i:s');
        $tuberculosis_record->treatment_outcome_result = $tInformation['Treatment_Outcome_Result'];
        $tuberculosis_record->treatment_outcome_additional_remarks = $tInformation['Treatment_Outcome_Additional_Remarks'];

        // dd($tuberculosis_record);



        $tuberculosis_record->save();

        // $flash_message = 'Tuberculosis Case Report was Saved!';

        // $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

        // header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
        // exit;

        //return Redirect::back()
         //            ->with('flash_message', $flash_message)
           //          ->with('flash_type', 'alert-success')
             //        ->with('flash_tab', 'dengue');
    }

    public function saveDrugs()
    {
        $hservice_id = Input::get('hservice_id');
        $tuberculosis_id = Input::get('tbservice_id');
        // dd(Input::All());

        $tuberculosis_data = Tuberculosis::select('tb_record_number')->where('healthcareservice_id', $hservice_id)->first();
        
        $tb_dosages_preparation_data = TuberculosisDosagesPreparations::where('tb_record_number', $this->tbcase_id)->count();

        // dd($this->tbcase_id);

        if ($tb_dosages_preparation_data > 0):
            $tuberculosis_dosages_preparations_record = TuberculosisDosagesPreparations::where('tb_record_number', $this->tbcase_id)->first();
        else:
            $tuberculosis_dosages_preparations_record = new TuberculosisDosagesPreparations();
            // $tuberculosis_dosages_preparations_record->tb_record_number = IdGenerator::generateId();
          // dd($tuberculosis_data->tb_record_number);
            $tuberculosis_dosages_preparations_record->tb_record_number = $tuberculosis_data->tb_record_number;
        endif;

        // $tuberculosis_dosages_preparations_record = new TuberculosisDosagesPreparations();

        $tuberculosis_dosages_preparations_record->tuberculosis_dosages_preparations_id = IdGenerator::generateId();
        $tuberculosis_dosages_preparations_record->tuberculosis_id = $tuberculosis_id;

        $tuberculosis_dosages_preparations_record->child_isoniazid = Input::get('Child_Isoniazid');
        $tuberculosis_dosages_preparations_record->child_rifampicin = Input::get('Child_Rifampicin');
        $tuberculosis_dosages_preparations_record->child_pyrazinamide = Input::get('Child_Pyrazinamide');
        $tuberculosis_dosages_preparations_record->child_ethambutol = Input::get('Child_Ethambutol');
        $tuberculosis_dosages_preparations_record->child_streptomycin = Input::get('Child_Streptomycin');

        $tuberculosis_dosages_preparations_record->save();

        // dd($tuberculosis_record);
        // $flash_message = 'SAVE SAVE SAVE!';

        $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

        header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
        exit;

        //return Redirect::back()
         //            ->with('flash_message', $flash_message)
           //          ->with('flash_type', 'alert-success')
             //        ->with('flash_tab', 'dengue');
    }

    public function saveClinicalExamination()
    {
      // dd($this->tbcase_id);
        $hservice_id = Input::get('hservice_id');
        $tuberculosis_id = Input::get('tbservice_id');

        $tuberculosis_data = Tuberculosis::select('tb_record_number')->where('healthcareservice_id', $hservice_id)->first();


        $tb_clinical_examination_data = TuberculosisClinicalHistory::where('tb_record_number', $this->tbcase_id)->count();

        if ($tb_clinical_examination_data > 0):
          $tuberculosis_clinical_history_record = TuberculosisClinicalHistory::where('tb_record_number', $this->tbcase_id)->first();
        else:
          $tuberculosis_clinical_history_record = new TuberculosisClinicalHistory();
          // $tuberculosis_clinical_history_record->tb_record_number = IdGenerator::generateId();
          $tuberculosis_clinical_history_record->tb_record_number = $tuberculosis_data->tb_record_number;
          // dd($tuberculosis_data->tb_record_number);
        endif;

        // $tuberculosis_clinical_history_record = new TuberculosisClinicalHistory();

        //can't get value of tbcase_id

        $tuberculosis_clinical_history_record->tuberculosis_clinical_history_id = IdGenerator::generateId();

        // $tuberculosis_dssm_record->tb_record_number = $this->tbcase_id;

        $tuberculosis_clinical_history_record->tuberculosis_id = $tuberculosis_id;

        //start of variables
        $tuberculosis_clinical_history_record->weight_in_kg = Input::get('Weight_In_Kg');
        $tuberculosis_clinical_history_record->unexplained_fever = Input::get('Unexplained_Fever');
        $tuberculosis_clinical_history_record->unexplained_cough = Input::get('Unexplained_Cough');
        $tuberculosis_clinical_history_record->unimproved_well_being = Input::get('Unimproved_Well_Being');
        $tuberculosis_clinical_history_record->poor_appetite = Input::get('Poor_Appetite');
        $tuberculosis_clinical_history_record->positive_pe_findings = Input::get('Positive_Pe_Findings');
        $tuberculosis_clinical_history_record->side_effects = Input::get('Side_Effects');
        $tuberculosis_clinical_history_record->clinical_exam_category = Input::get('Clinical_Exam_Category');

        $tuberculosis_clinical_history_record->save();

        $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

        header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
        exit;

    }



    public function saveDrugIntakeIntensive()
    {

        $hservice_id = Input::get('hservice_id');
        $tuberculosis_id = Input::get('tbservice_id');

        $adultDateOfAdmin = (new Datetime(Input::get('Adult_Intensive_Date_Of_Administration')))->format('Y-m-d H:i:s');
        $duplicateCount = TuberculosisDrugIntake::where('adult_intake_type', 'I')->where('adult_date_of_administration', $adultDateOfAdmin)->count();
        $tuberculosis_drug_intake_record = "";

        $tuberculosis_data = Tuberculosis::select('tb_record_number')->where('healthcareservice_id', $hservice_id)->first();

        if($duplicateCount == 0) {
            // dd($tuberculosis_id);


            $tuberculosis_drug_intake_record = new TuberculosisDrugIntake();
            $tuberculosis_drug_intake_record->tuberculosis_drug_intake_id = IdGenerator::generateId();

            $this->insertDrugIntake($tuberculosis_drug_intake_record, $tuberculosis_id, $adultDateOfAdmin, Input::get('Adult_Intensive_Drug_Administrator'), Input::get('Drug_Intake_Type_Intensive'), $tuberculosis_data->tb_record_number);


        }
        else {
            // insertDrugIntake();
            $tuberculosis_drug_intake_record = TuberculosisDrugIntake::where('adult_intake_type', 'I')->where('adult_date_of_administration', $adultDateOfAdmin)->first();


            $this->insertDrugIntake($tuberculosis_drug_intake_record, $tuberculosis_id, $adultDateOfAdmin, Input::get('Adult_Intensive_Drug_Administrator'), Input::get('Drug_Intake_Type_Intensive'), $tuberculosis_data->tb_record_number);

            // dd($tuberculosis_drug_intake_record);
        }

        $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

        header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
        exit;

        //return Redirect::back()
         //            ->with('flash_message', $flash_message)
           //          ->with('flash_type', 'alert-success')
             //        ->with('flash_tab', 'dengue');
    }

    public function insertDrugIntake($tuberculosis_drug_intake_record, $tuberculosis_id, $adultDateOfAdmin, $adult_drug_administrator, $adult_intake_type, $tb_record_number ) {

        $tuberculosis_drug_intake_record->tuberculosis_id = $tuberculosis_id;
        $tuberculosis_drug_intake_record->adult_date_of_administration = $adultDateOfAdmin;
        $tuberculosis_drug_intake_record->adult_drug_administrator = $adult_drug_administrator;
        $tuberculosis_drug_intake_record->tb_record_number = $tb_record_number;
        // $tuberculosis_drug_intake_record->adult_drug_intake_remarks = Input::get('Adult_Intensive_Drug_Intake_Remarks');
        $tuberculosis_drug_intake_record->adult_intake_type = $adult_intake_type;

        $tuberculosis_drug_intake_record->save();

        $flash_message = 'SAVE SAVE SAVE!';


    }

    public function deleteDrugIntakeIntensive() {
        $hservice_id = Input::get('hservice_id');
        $tuberculosis_id = Input::get('tbservice_id');

        // dd(Input::get('Date_To_Be_Deleted'));

        $adultDateOfDelete = (new Datetime(Input::get('Date_To_Be_Deleted')))->format('Y-m-d H:i:s');

        $tuberculosis_drug_intake_record = TuberculosisDrugIntake::where('adult_intake_type', 'I')->where('adult_date_of_administration', $adultDateOfDelete)->first()->delete();

        $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

        header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
        exit;
    }

    public function deleteDrugIntakeContinuous() {
        $hservice_id = Input::get('hservice_id');
        $tuberculosis_id = Input::get('tbservice_id');

        // dd(Input::get('Date_To_Be_Deleted'));

        $adultDateOfDelete = (new Datetime(Input::get('Date_To_Be_Deleted')))->format('Y-m-d H:i:s');
        // dd($adultDateOfDelete);
        $tuberculosis_drug_intake_record = TuberculosisDrugIntake::where('adult_intake_type', 'C')->where('adult_date_of_administration', $adultDateOfDelete)->first()->delete();

        $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

        header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
        exit;
    }

    public function saveDrugIntakeContinuous()
    {

        $hservice_id = Input::get('hservice_id');
        $tuberculosis_id = Input::get('tbservice_id');

        $adultDateOfAdmin = (new Datetime(Input::get('Adult_Continuous_Date_Of_Administration')))->format('Y-m-d H:i:s');
        $duplicateCount = TuberculosisDrugIntake::where('adult_intake_type', 'C')->where('adult_date_of_administration', $adultDateOfAdmin)->count();
        $tuberculosis_drug_intake_record = "";

        $tuberculosis_data = Tuberculosis::select('tb_record_number')->where('healthcareservice_id', $hservice_id)->first();

        if($duplicateCount == 0) {
            // dd($tuberculosis_id);
            $tuberculosis_drug_intake_record = new TuberculosisDrugIntake();
            $tuberculosis_drug_intake_record->tuberculosis_drug_intake_id = IdGenerator::generateId();

            $this->insertDrugIntake($tuberculosis_drug_intake_record, $tuberculosis_id, $adultDateOfAdmin, Input::get('Adult_Continuous_Drug_Administrator'), Input::get('Drug_Intake_Type_Continuous'), $tuberculosis_data->tb_record_number);


        }
        else {
            // insertDrugIntake();
            $tuberculosis_drug_intake_record = TuberculosisDrugIntake::where('adult_intake_type', 'C')->where('adult_date_of_administration', $adultDateOfAdmin)->first();


            $this->insertDrugIntake($tuberculosis_drug_intake_record, $tuberculosis_id, $adultDateOfAdmin, Input::get('Adult_Continuous_Drug_Administrator'), Input::get('Drug_Intake_Type_Continuous'), $tuberculosis_data->tb_record_number);

            // dd($tuberculosis_drug_intake_record);
        }

        $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

        header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
        exit;

        //return Redirect::back()
         //            ->with('flash_message', $flash_message)
           //          ->with('flash_type', 'alert-success')
             //        ->with('flash_tab', 'dengue');
    }

    public function view()
    {

    }

    public static function update()
    {

    }
}
