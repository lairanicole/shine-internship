<?php

namespace Plugins\Tuberculosis;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class TuberculosisDssmModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tuberculosis_dssm_record';
    protected static $table_name = 'tuberculosis_dssm_record';
    protected $primaryKey = 'tuberculosis_dssm_id';

    protected $fillable = [];
    protected $touches = array('tuberculosis');
    
    public function tuberculosis()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\Tuberculosis\TuberculosisModel','tb_record_number','tb_record_number');
    }
    public function scopeWithAndWhereHas($query, $relation, $constraint){
        return $query->whereHas($relation, $constraint)
                     ->with([$relation => $constraint]);
    }

}
