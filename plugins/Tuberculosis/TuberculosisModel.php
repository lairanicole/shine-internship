<?php

namespace Plugins\Tuberculosis;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class TuberculosisModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tuberculosis_record';
    protected static $table_name = 'tuberculosis_record';
    protected $primaryKey = 'tuberculosis_id';

    protected $fillable = [];
    protected $touches = array('Healthcareservices');
    
    public function Healthcareservices() {
        return $this->belongsTo('ShineOS\Core\Healthcareservices\Entities\Healthcareservices', 'healthcareservice_id', 'healthcareservice_id');
    }
    
    public function clinicalHistory()
    {
        DB::enableQueryLog();
        return $this->hasMany('Plugins\Tuberculosis\TuberculosisClinicalHistoryModel','tb_record_number','tb_record_number');
    }

    public function dosagesPreparations()
    {
        DB::enableQueryLog();
        return $this->hasMany('Plugins\Tuberculosis\TuberculosisDosagesPreparationsModel','tb_record_number','tb_record_number');
    }

    public function drugIntake()
    {
        DB::enableQueryLog();
        return $this->hasMany('Plugins\Tuberculosis\TuberculosisDrugIntakeModel','tb_record_number','tb_record_number');
    }

    public function dssm()
    {
        DB::enableQueryLog();
        return $this->hasMany('Plugins\Tuberculosis\TuberculosisDssmModel','tb_record_number','tb_record_number');
    }
    
    public function scopeWithAndWhereHas($query, $relation, $constraint){
        return $query->whereHas($relation, $constraint)
                     ->with([$relation => $constraint]);
    }
}
