<?php

namespace Plugins\Tuberculosis;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TuberculosisDosagesPreparationsModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tuberculosis_dosages_preparations_record';
    protected static $table_name = 'tuberculosis_dosages_preparations_record';
    protected $primaryKey = 'tuberculosis_dosages_preparations_ID';

    protected $fillable = [];
    protected $touches = array('tuberculosis');
    public function tuberculosis()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\Tuberculosis\TuberculosisModel','tb_record_number','tb_record_number');
    }


}
