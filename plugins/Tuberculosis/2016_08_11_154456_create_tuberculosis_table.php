<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTuberculosisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tuberculosis_record')!=TRUE) {
          Schema::create('tuberculosis_record', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tuberculosis_id', 60)->nullable();
            $table->string('healthcareservice_id', 60);
            $table->string('tb_record_number', 60)->nullable();
            $table->string('tuberculin_result', 60)->nullable();
            $table->dateTime('tuberculin_date_read')->nullable()->default(NULL);
            $table->string('cxr_findings', 60)->nullable();
            $table->dateTime('cxr_date_of_exam')->nullable()->default(NULL);
            $table->string('cxr_tbdc', 60)->nullable();
            $table->string('other_exam_exam_conducted', 60)->nullable();
            $table->dateTime('other_exam_date_of_exam')->nullable()->default(NULL);
            $table->enum('bcg_scar', ['Y','N','D'])->default('D');
            $table->enum('category_of_treatment', ['I','II','III'])->default('I');
            $table->string('xpert_results', 60)->nullable();
            $table->dateTime('xpert_date_of_collection')->nullable()->default(NULL);
            $table->enum('pict_done', ['Y','N','U'])->default('U');
            $table->dateTime('pict_date_administered')->nullable()->default(NULL);
            $table->enum('diagnosis_tuberculosis_type', ['TB','TBI','TBE','NTB'])->default('NTB');
            $table->string('diagnosis_remarks', 60)->nullable();
            $table->enum('tb_drugs_before', ['Y','N','U'])->default('U');
            $table->dateTime('tb_drugs_date_administered')->nullable()->default(NULL);
            $table->enum('tb_drugs_duration', ['L','M'])->default('L');
            $table->string('tb_drugs_taken', 40)->nullable();
            $table->enum('bacteriological_status', ['BC','CD','ND'])->default('ND');
            $table->enum('anatomical_site', ['P','EP'])->default('EP');
            $table->string('anatomical_site_specify', 60)->nullable();
            $table->enum('bacteriology_registration_group', ['N','R','TAF','TALF','PTOU','OTH'])->default('OTH');
            $table->string('bacteriology_registration_group_specify', 60)->nullable();
            $table->dateTime('treatment_outcome_date_started')->nullable()->default(NULL);
            $table->dateTime('treatment_outcome_date_last_intake')->nullable()->default(NULL);
            $table->enum('treatment_outcome_result', ['C','TC','D','F','LTF','NE','EFC'])->default('EFC');
            $table->string('treatment_outcome_additional_remarks', 60)->nullable();

            $table->softDeletes();
            $table->timestamps();
            $table->unique('tuberculosis_id');
          });
      }

      if (Schema::hasTable('tuberculosis_dssm_record')!=TRUE) {
          Schema::create('tuberculosis_dssm_record', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tuberculosis_dssm_id', 60);
            $table->string('tb_record_number', 60)->nullable();
            $table->string('tuberculosis_id', 60);
            $table->dateTime('month')->nullable()->default(NULL);
            $table->dateTime('due_date')->nullable()->default(NULL);
            $table->dateTime('date_examined')->nullable()->default(NULL);
            $table->text('tb_drugs_taken')->nullable();
            $table->string('result', 60)->nullable();
            $table->enum('category_of_treatment', ['1','2','1A','2A'])->default('1');

            $table->softDeletes();
            $table->timestamps();
            $table->unique('tuberculosis_dssm_id');
          });
        }

        if (Schema::hasTable('tuberculosis_clinical_history_record')!=TRUE) {
          Schema::create('tuberculosis_clinical_history_record', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tuberculosis_clinical_history_id', 60);
            $table->string('tb_record_number', 60)->nullable();
            $table->string('tuberculosis_id', 60);
            $table->integer('weight_in_kg')->nullable();
            $table->enum('unexplained_fever', ['Y','N','U'])->default('U');
            $table->enum('unexplained_cough', ['Y','N','U'])->default('U');
            $table->enum('unimproved_well_being', ['F','RP','L'])->default('F');
            $table->enum('poor_appetite', ['Y','N','U'])->default('U');
            $table->enum('positive_pe_findings', ['Y','N','U'])->default('U');
            $table->text('side_effects', 60)->nullable();
            $table->enum('clinical_exam_category', ['CHILD','ADULT'])->default('CHILD');

            $table->softDeletes();
            $table->timestamps();
            $table->unique('tuberculosis_clinical_history_id', "tb_clinical_record_id");
          });
      }

      if (Schema::hasTable('tuberculosis_dosages_preparations_record')!=TRUE) {
          Schema::create('tuberculosis_dosages_preparations_record', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tuberculosis_dosages_preparations_id', 60);
            $table->string('tb_record_number', 60)->nullable();
            $table->string('tuberculosis_id', 60);
            $table->integer('child_isoniazid')->nullable();
            $table->integer('child_rifampicin')->nullable();
            $table->integer('child_pyrazinamide')->nullable();
            $table->integer('child_ethambutol')->nullable();
            $table->integer('child_streptomycin')->nullable();

            $table->softDeletes();
            $table->timestamps();
            $table->unique('tuberculosis_dosages_preparations_id', 'tb_dosages_prep_id');
          });
        }
        if (Schema::hasTable('tuberculosis_drug_intake_record')!=TRUE) {
          Schema::create('tuberculosis_drug_intake_record', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tuberculosis_drug_intake_id', 60);
            $table->string('tb_record_number', 60)->nullable();
            $table->string('tuberculosis_id', 60);
            $table->dateTime('adult_date_of_administration')->nullable()->default(NULL);
            $table->enum('adult_drug_administrator', ['TP','SLF','MISS'])->default('MISS');
            $table->enum('adult_intake_type', ['I','C'])->default('I');
            $table->text('adult_drug_intake_remarks')->nullable()->default(NULL);
            $table->softDeletes();
            $table->timestamps();
            $table->unique('tuberculosis_drug_intake_id', "tb_intake_id");
          });
        }

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tuberculosis_record');
        Schema::drop('tuberculosis_dssm_record');
        Schema::drop('tuberculosis_clinical_history_record');
        Schema::drop('tuberculosis_dosages_preparations_record');
        Schema::drop('tuberculosis_drug_intake_record');
    }
}
