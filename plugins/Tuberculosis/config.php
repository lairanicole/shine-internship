<?php

$plugin_id = 'Tuberculosis';                     //plugin ID
$plugin_module = 'healthcareservices';          //module owner
$plugin_location = 'dropdown';                  //UI location where plugin will be accessible
$plugin_primaryKey = 'tuberculosis_id';        //primary_key used to find data
$plugin_table = 'tuberculosis_record';            //plugintable default; table_name custom table
$plugin_relationship = array('tuberculosis_dssm_record', 'tuberculosis_clinical_history_record', 'tuberculosis_dosages_preparations_record', 'tuberculosis_drug_intake_record');           //SUBTABLES

$plugin_tabs_child = array('addservice', 'complaints', 'vitals', 'tuberculosis_plugin', 'medicalorders', 'disposition');
$plugin_type = "program";
$plugin_gender = "all";
//plugin maximum role to access this plugin (MANDATORY VALUE)
$plugin_role = 5;

$plugin_folder = 'Tuberculosis'; //module owner
$plugin_title = 'TB DOTS';            //plugin title
$plugin_description = 'Tuberculosis';
$plugin_version = '1.0';
$plugin_developer = 'ShineLabs';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2016";


$plugin_tabs = [
    'addservice' => 'Basic Information',
    'disposition' => 'Disposition',
    'medicalorders' => 'Medical Orders',
    'vitals' => 'Vitals & Physical',
    // 'impanddiag' => 'Impressions and Diagnosis',
    'complaints' => 'Complaints',
    'tuberculosis_plugin' => 'Tuberculosis'
];

$plugin_tabs_models = [
    'complaints' => 'GeneralConsultation',
    'disposition' => 'Disposition',
    'medicalorders' => 'MedicalOrder',
    'vitals' => 'VitalsPhysical',
    'tuberculosis_plugin' => 'TuberculosisModel'
];
