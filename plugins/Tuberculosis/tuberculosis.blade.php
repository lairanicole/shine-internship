
<?php
// dd($data);
  $hservice_id = $data['healthcareservice_id'];
  $tbservice_id = $data['tuberculosis_id'];
  $patient_age = getAge(($allData['patient']['birthdate']));
  $patient_id = $allData['patient']['patient_id'];

  $isNewHealthcare = false;
  if($data['tb_record_number'] == NULL) {
    $isNewHealthcare = true;
  }

  if($pdata AND $isNewHealthcare == true) {
    $data = $pdata;
  }

  if( isset($allData['pluginparentdata']) ) {
      $tbcaseID = $allData['pluginparentdata']->tb_record_number;
  } elseif(isset($data->tb_record_number)) {
      $tbcaseID = $data->tb_record_number;
  } else {
      $tbcaseID = NULL;
  }
$isDisabled = '';
$ajaxIsDisabled = "";

?>

{!! Form::hidden('tbcase_id',$tbcaseID) !!}
<input {{$isDisabled}} type="hidden" name="hservice_id" id="hservice_id" value="<?php echo $hservice_id; ?>">
<input {{$isDisabled}} type="hidden" name="tbservice_id" id="tbservice_id" value="<?php echo $tbservice_id; ?>">

{!! HTML::style('plugins/Tuberculosis/Assets/css/style.css') !!}

<div  id="tbform">
<div class="box-body no-padding">

  @if (($data['tb_record_number']) != "")
  <input {{$isDisabled}} type="hidden" class="form-control" name="Tb_Record_Number" value="<?php echo $data['tb_record_number']; ?>" placeholder="Record ID" {{$read}}>
  <!-- <input {{$isDisabled}} type="hidden" name="tuberculosis[DiagnosticTestsTab][Is_Saved]" value="1"> -->
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li>
        <a><b><?php echo 'Case ID: ' . ($data['tb_record_number']) ?></b></a>
      </li>
      <li class="active">
        <a href="#clinical_history_tab" data-toggle="tab">Clinical Exam History </a>
      </li>
      <li class="">
        <a href="#diagnostic_tests_tab" data-toggle="tab">Diagnostic Tests</a>
      </li>
      <li class="">
        <a href="#treatment_information_tab" data-toggle="tab">Treatment Information</a>
      </li>
      <li class="">
        <a href="#dssm_tab" data-toggle="tab">DSSM Results</a>
      </li>
      <?php if ($patient_age < 18) { ?>
      <li class="">
        <a href="#drugs_dosages_and_preparations_tab" data-toggle="tab">Drugs Dosages Record</a>
      </li>
      <?php } else { ?>
      <li class="">
        <a href="#drug_intake_record_tab" data-toggle="tab">Drug Intake Record</a>
      </li>
      <?php } ?>
    </ul>


    <div class="tab-content">
      <div class="tab-pane" id="diagnostic_tests_tab">
        <div class = "row">
          <div class="col-md-12">
            <legend>Tuberculin Skin Testing</legend>
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label sampleClass">Result</label>
              <div class="col-md-6">
               <input {{$isDisabled}} type="number" class="form-control" name="tuberculosis[DiagnosticTestsTab][Tuberculin_Result]" placeholder="Result in millimeters" value="<?php echo $data['tuberculin_result'] ?>">
              </div>
              <div class="col-md-2">
               <input {{$isDisabled}} type="text" class="form-control" placeholder="mm" value="mm" disabled>
              </div>
            </div>
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label">Date Read</label>
              <div class="col-md-8">
                <div class="input-group">
                    <i class="fa fa-calendar inner-icon"></i>
                    {!! Form::text('tuberculosis[DiagnosticTestsTab][Tuberculin_Date_Read]', (empty($data['tuberculin_date_read']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['tuberculin_date_read']))), ['class' => 'form-control', 'id'=>'datepicker', $read]); !!}
                    <!-- <textarea name="Tuberculin_Date_Read" col="10" row ="5"></textarea> -->
                </div>
              </div>
            </div>
          </div>
        </div>


        <!-- CXR FINDINGS LEGEND -->
        <div class = "row">
          <div class="col-md-12">
            <legend>CXR Findings</legend>
            <div class="form-group col-md-12">
              <label class="col-md-2 control-label">Findings</label>
              <div class="col-md-10">
               <input {{$isDisabled}} type="text" class="form-control" name="tuberculosis[DiagnosticTestsTab][Cxr_Findings]" placeholder="Type Findings Here" value="<?php echo $data['cxr_findings'] ?>">
              </div>
            </div>
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label">Date of Exam</label>
              <div class="col-md-8">
                <div class="input-group">
                    <i class="fa fa-calendar inner-icon"></i>
                    {!! Form::text('tuberculosis[DiagnosticTestsTab][Cxr_Date_Of_Exam]', (empty($data['cxr_date_of_exam']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['cxr_date_of_exam']))), ['class' => 'form-control', 'id'=>'datepicker', $read]); !!}
                </div>
              </div>
            </div>
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label">TBDC</label>
              <div class="col-md-8">
               <input {{$isDisabled}} type="text" class="form-control" name="tuberculosis[DiagnosticTestsTab][Cxr_Tbdc]" placeholder="TBDC" value="<?php echo $data['cxr_tbdc'] ?>" >
              </div>
            </div>
          </div>
        </div>

        <!-- XPERT MTB -->
        <div class = "row">
          <div class="col-md-12">
            <legend>XPERT MTB/RIF Result</legend>
            <div class="col-md-6">
              <label class="col-md-4 control-label">Results</label>
              <div class="col-md-8">
               <input {{$isDisabled}} type="text" class="form-control" name="tuberculosis[DiagnosticTestsTab][Xpert_Results]" placeholder="Type Results Here" value="<?php echo $data['xpert_results'] ?>" >
              </div>
            </div>
            <div class="col-md-6">
              <label class="col-md-4 control-label">Date of Collection</label>
              <div class="col-md-8">
                <div class="input-group">
                    <i class="fa fa-calendar inner-icon"></i>
                    {!! Form::text('tuberculosis[DiagnosticTestsTab][Xpert_Date_Of_Collection]', (empty($data['xpert_date_of_collection']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['xpert_date_of_collection']))), ['class' => 'form-control', 'id'=>'datepicker', $read]); !!}
                </div>
              </div>
            </div>
          </div>
        </div>



        <div class = "row">
          <div class="col-md-12">
            <legend>PICT Examination Results</legend>
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label">PICT Done?</label>
              <div class="col-md-8">
                <div class="btn-group toggler" data-toggle="buttons">
                  <label {{$isDisabled}} class="btn btn-default <?php if($data['pict_done'] == 'Y') {echo 'active';} ?>" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[DiagnosticTestsTab][Pict_Done]" value="Y" <?php if($data['pict_done'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
                  </label>
                  <label {{$isDisabled}} class="btn btn-default <?php if($data['pict_done'] == 'N') {echo 'active';} ?>" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[DiagnosticTestsTab][Pict_Done]" value="N" <?php if($data['pict_done'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
                  </label>
                  <label {{$isDisabled}} class="btn btn-default <?php if($data['pict_done'] == 'U') {echo 'active';} ?>" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[DiagnosticTestsTab][Pict_Done]" value="U" <?php if($data['pict_done'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label">Date Administered</label>
              <div class="col-md-8">
                <div class="input-group">
                    <i class="fa fa-calendar inner-icon"></i>
                    {!! Form::text('tuberculosis[DiagnosticTestsTab][Pict_Date_Administered]', (empty($data['pict_date_administered']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['pict_date_administered']))), ['class' => 'form-control', 'id'=>'datepicker', $read]); !!}
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Others Legend -->
        <div class = "row">
          <div class="col-md-12">
            <legend>Other Details</legend>
            <!-- <div class="row"> -->
              <div class="form-group col-md-6">
                <label class="col-md-4 control-label">Exam Conducted</label>
                <div class="col-md-8">
                 <input {{$isDisabled}} type="text" class="form-control" name="tuberculosis[DiagnosticTestsTab][Other_Exam_Exam_Conducted]" placeholder="Exam Conducted" value="<?php echo $data['other_exam_exam_conducted'] ?>" >
                </div>
              </div>
              <div class="form-group col-md-6">
                <label class="col-md-4 control-label">Date of Exam</label>
                <div class="col-md-8">
                 <div class="input-group">
                      <i class="fa fa-calendar inner-icon"></i>

                      {!! Form::text('tuberculosis[DiagnosticTestsTab][Other_Exam_Date_Of_Exam]', (empty($data['other_exam_date_of_exam']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['other_exam_date_of_exam']))), ['class' => 'form-control', 'id'=>'datepicker', $read]); !!}
                  </div>
                </div>
              </div>
            <!-- </div> -->
            <!-- <div class="row"> -->
              <div class="form-group col-md-6">
                <label class="col-md-4 control-label">BCG Scar</label>
                <div class="col-md-8">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bcg_scar'] == 'Y') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[DiagnosticTestsTab][Bcg_Scar]" value="Y" <?php if($data['bcg_scar'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bcg_scar'] == 'N') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[DiagnosticTestsTab][Bcg_Scar]" value="N" <?php if($data['bcg_scar'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bcg_scar'] == 'D') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[DiagnosticTestsTab][Bcg_Scar]" value="D" <?php if($data['bcg_scar'] == 'D') {echo "checked=\'checked\' ";} ?>> Doubtful
                    </label>
                  </div>
                </div>
              </div>
            <!-- </div> -->

          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <legend>Diagnosis</legend>
            <div class="form-group col-md-12">
              <label class="col-md-2 control-label">Tuberculosis Type</label>
              <div class="col-md-10">
                <div class="btn-group toggler" data-toggle="buttons">
                  <label {{$isDisabled}} class="btn btn-default <?php if($data['diagnosis_tuberculosis_type'] == 'TB') {echo 'active';} ?>" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[DiagnosticTestsTab][Diagnosis_Tuberculosis_Type]" value="TB" <?php if($data['diagnosis_tuberculosis_type'] == 'TB') {echo 'checked=\'checked\' ';} ?>> TB Disease
                  </label>
                  <label {{$isDisabled}} class="btn btn-default <?php if($data['diagnosis_tuberculosis_type'] == 'TBI') {echo 'active';} ?>" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[DiagnosticTestsTab][Diagnosis_Tuberculosis_Type]" value="TBI" <?php if($data['diagnosis_tuberculosis_type'] == 'TBI') {echo 'checked=\'checked\' ';} ?>> TB Infection <small>for children below 5 y/o</small>
                  </label>
                  <label {{$isDisabled}} class="btn btn-default <?php if($data['diagnosis_tuberculosis_type'] == 'TBE') {echo 'active';} ?>" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[DiagnosticTestsTab][Diagnosis_Tuberculosis_Type]" value="TBE" <?php if($data['diagnosis_tuberculosis_type'] == 'TBE') {echo 'checked=\'checked\' ';} ?>> TB Exposure <small>for children below 5 y/o</small>
                  </label>
                  <label {{$isDisabled}} class="btn btn-default <?php if($data['diagnosis_tuberculosis_type'] == 'NTB') {echo 'active';} ?>" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[DiagnosticTestsTab][Diagnosis_Tuberculosis_Type]" value="NTB" <?php if($data['diagnosis_tuberculosis_type'] == 'NTB') {echo 'checked=\'checked\' ';} ?>> No TB Infection
                  </label>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <label class="col-md-2 control-label">Remarks</label>
              <div class="col-md-10 ui-widget">
                {!! Form::textarea('tuberculosis[DiagnosticTestsTab][Diagnosis_Remarks]', $data['diagnosis_remarks'], ['class' => 'form-control noresize', 'placeholder' => 'Put Remarks Here', 'cols'=>'10', 'rows'=>'5', $read]) !!}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="tab-pane" id="treatment_information_tab">
        <div class = "row">
          <div class="col-md-12">
            <legend>History of Anti-TB Drug</legend>
            <div class="row">
              <div class="form-group col-md-6">
                <label class="col-md-4 control-label"><small>Had taken Anti-TB drugs before?</small></label>
                <div class="col-md-8">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['tb_drugs_before'] == 'Y') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Tb_Drugs_Before]" value="Y" <?php if($data['tb_drugs_before'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['tb_drugs_before'] == 'N') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Tb_Drugs_Before]" value="N" <?php if($data['tb_drugs_before'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['tb_drugs_before'] == 'U') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Tb_Drugs_Before]" value="U" <?php if($data['tb_drugs_before'] == 'U') {echo 'checked=\'checked\' ';} ?>> Unknown
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label class="col-md-4 control-label">Date Administered</label>
                <div class="col-md-8">
                  <div class="input-group">
                      <i class="fa fa-calendar inner-icon"></i>
                      {!! Form::text('tuberculosis[TreatmentInformationTab][Tb_Drugs_Date_Administered]', (empty($data['tb_drugs_date_administered']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['tb_drugs_date_administered']))), ['class' => 'form-control', 'id'=>'datepicker', $read]); !!}
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                <label class="col-md-2 control-label">Duration</label>
                <div class="col-md-10">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['tb_drugs_duration'] == 'L') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Tb_Drugs_Duration]" value="L" <?php if($data['tb_drugs_duration'] == 'L') {echo 'checked=\'checked\' ';} ?>> Less than 1 Month
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['tb_drugs_duration'] == 'M') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Tb_Drugs_Duration]" value="M" <?php if($data['tb_drugs_duration'] == 'M') {echo 'checked=\'checked\' ';} ?>> More than 1 Month
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <legend>Bacteriology</legend>
            <div class="row">
              <div class="form-group col-md-12">
                <label class="col-md-2 control-label">Bacteriological Status</label>
                <div class="col-md-10">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bacteriological_status'] == 'BC') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Bacteriological_Status]" value="BC" <?php if($data['bacteriological_status'] == 'BC') {echo 'checked=\'checked\' ';} ?>> Bacteriologically Confirmed
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bacteriological_status'] == 'CD') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Bacteriological_Status]" value="CD" <?php if($data['bacteriological_status'] == 'CD') {echo 'checked=\'checked\' ';} ?>> Clinically Diagnosed
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bacteriological_status'] == 'ND') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Bacteriological_Status]" value="ND" <?php if($data['bacteriological_status'] == 'ND') {echo 'checked=\'checked\' ';} ?>> Not Yet Diagnosed
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                <label class="col-md-2 control-label">Anatomical Site</label>
                <div class="col-md-4">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['anatomical_site'] == 'P') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Anatomical_Site]" value="P" <?php if($data['anatomical_site'] == 'P') {echo 'checked=\'checked\' ';} ?>> Pulmonary
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['anatomical_site'] == 'EP') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Anatomical_Site]" value="EP" <?php if($data['anatomical_site'] == 'EP') {echo 'checked=\'checked\' ';} ?>> Extra-Pulmunary
                    </label>
                  </div>
                </div>
                <div class="col-md-4">
                    {!! Form::textarea('tuberculosis[TreatmentInformationTab][Anatomical_Site_Specify]', $data['anatomical_site_specify'], ['class' => 'form-control noresize', 'placeholder' => 'Specify', 'cols'=>'5', 'rows'=>'1', $read]) !!}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                <label class="col-md-2 control-label">Registration Group</label>
                <div class="col-md-7">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bacteriology_registration_group'] == 'N') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Bacteriology_Registration_Group]" value="N" <?php if($data['bacteriology_registration_group'] == 'N') {echo 'checked=\'checked\' ';} ?>> New
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bacteriology_registration_group'] == 'R') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Bacteriology_Registration_Group]" value="R" <?php if($data['bacteriology_registration_group'] == 'R') {echo 'checked=\'checked\' ';} ?>> Relapse
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bacteriology_registration_group'] == 'TAF') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Bacteriology_Registration_Group]" value="TAF" <?php if($data['bacteriology_registration_group'] == 'TAF') {echo 'checked=\'checked\' ';} ?>> Treatment After Failure
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bacteriology_registration_group'] == 'TALF') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Bacteriology_Registration_Group]" value="TALF" <?php if($data['bacteriology_registration_group'] == 'TALF') {echo 'checked=\'checked\' ';} ?>> TALF
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bacteriology_registration_group'] == 'PTOU') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Bacteriology_Registration_Group]" value="PTOU" <?php if($data['bacteriology_registration_group'] == 'PTOU') {echo 'checked=\'checked\' ';} ?>> PTOU
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['bacteriology_registration_group'] == 'OTH') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Bacteriology_Registration_Group]" value="OTH" <?php if($data['bacteriology_registration_group'] == 'OTH') {echo 'checked=\'checked\' ';} ?>> Others
                    </label>
                  </div>
                </div>
                <div class="col-md-3">
                    {!! Form::textarea('tuberculosis[TreatmentInformationTab][Bacteriology_Registration_Group_Specify]', $data['bacteriology_registration_group_specify'], ['class' => 'form-control noresize', 'placeholder' => 'Specify', 'cols'=>'5', 'rows'=>'1', $read]) !!}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <legend>Treatment Outcome</legend>
            <div class = "row">
              <div class="form-group col-md-6">
                <label class="col-md-4 control-label">Date Treatment/IPT Started</label>
                <div class="col-md-8">
                  <div class="input-group">
                      <i class="fa fa-calendar inner-icon"></i>
                      {!! Form::text('tuberculosis[TreatmentInformationTab][Treatment_Outcome_Date_Started]', (empty($data['treatment_outcome_date_started']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['treatment_outcome_date_started']))), ['class' => 'form-control', 'id'=>'datepicker', $read]); !!}
                  </div>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label class="col-md-4 control-label">Date of Last Intake</label>
                <div class="col-md-8">
                    <div class="input-group">
                      <i class="fa fa-calendar inner-icon"></i>
                      {!! Form::text('tuberculosis[TreatmentInformationTab][Treatment_Outcome_Date_Last_Intake]', (empty($data['treatment_outcome_date_last_intake']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['treatment_outcome_date_last_intake']))), ['class' => 'form-control', 'id'=>'datepicker', $read]); !!}
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                <label class="col-md-2 control-label">Treatment Result</label>
                <div class="col-md-10">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['treatment_outcome_result'] == 'C') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Treatment_Outcome_Result]" value="C" <?php if($data['treatment_outcome_result'] == 'C') {echo 'checked=\'checked\' ';} ?>> Cured
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['treatment_outcome_result'] == 'TC') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Treatment_Outcome_Result]" value="TC" <?php if($data['treatment_outcome_result'] == 'TC') {echo 'checked=\'checked\' ';} ?>> Treatment Completed
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['treatment_outcome_result'] == 'D') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Treatment_Outcome_Result]" value="D" <?php if($data['treatment_outcome_result'] == 'D') {echo 'checked=\'checked\' ';} ?>> Died
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['treatment_outcome_result'] == 'F') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Treatment_Outcome_Result]" value="F" <?php if($data['treatment_outcome_result'] == 'F') {echo 'checked=\'checked\' ';} ?>> Failed
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['treatment_outcome_result'] == 'LTF') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Treatment_Outcome_Result]" value="LTF" <?php if($data['treatment_outcome_result'] == 'LTF') {echo 'checked=\'checked\' ';} ?>> Lost to Follow-up
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['treatment_outcome_result'] == 'NE') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Treatment_Outcome_Result]" value="NE" <?php if($data['treatment_outcome_result'] == 'NE') {echo 'checked=\'checked\' ';} ?>> Not Evaluated
                    </label>
                    <label {{$isDisabled}} class="btn btn-default <?php if($data['treatment_outcome_result'] == 'EFC') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tuberculosis[TreatmentInformationTab][Treatment_Outcome_Result]" value="EFC" <?php if($data['treatment_outcome_result'] == 'EFC') {echo 'checked=\'checked\' ';} ?>> Excluded from Cohort
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="form-group col-md-12">
                <label class="col-md-2 control-label">Additional Remarks:</label>
                <div class="col-md-8 ui-widget">
                  {!! Form::textarea('tuberculosis[TreatmentInformationTab][Treatment_Outcome_Additional_Remarks]', $data['treatment_outcome_additional_remarks'], ['class' => 'form-control noresize', 'placeholder' => 'e.g. Reason for death, Reason for Lost to Follow-up', 'cols'=>'10', 'rows'=>'3', $read]) !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="tab-pane" id="dssm_tab">
        <legend>DSSM Result History</legend>
        @if (count($data['dssm']) != 0)
        <table class="table table-striped table-bordered">
          <tr>
            <th style="width:5%;">Exam Number</th>
            <th style="width:5%;">Category</th>
            <th style="width:5%;">Month Number</th>
            <th style="width:10%;">Date Examined</th>
            <th style="width:15%;">Drugs Prescribed</th>
            <th style="width:60%;">Result</th>
          </tr>

          <?php

          // dd(count($data['dssm']));

          foreach ($data['dssm'] as $key => $value) {

            parse_str($value->tb_drugs_taken, $tb_drugs_taken_array);
            // dd($tb_drugs_taken_array);
            // dd($other_source_treatment_initial_array['Distant_Metastasis_Sites']);
            // dd($value->other_source_treatment);

            $tb_drugs_taken_value = "";
            if (isset($tb_drugs_taken_array['Tb_Drugs_Taken'])) {
              foreach ($tb_drugs_taken_array['Tb_Drugs_Taken'] as $site => $tb_drugs_taken_curr_value) {
              $tb_drugs_taken_value .= " <span class='label label-success'>" .$tb_drugs_taken_curr_value . "</span> ";
              }
            } else {
              $tb_drugs_taken_value = "<span class='label label-danger'> None </span>";
            }

            ?>
            <tr>
              <td style="width:5%;"><?php echo $key+1; ?></td>
              <td style="width:5%;"><?php if($value->category_of_treatment != "") echo "<span class='label label-default'>Category " . $value->category_of_treatment . "</span>"; else {echo "<span class='label label-danger'> Unknown </span>";} ?></td>
              <td style="width:5%;"><?php if($value->month != "") echo "<span class='label label-success'>" . $value->month . "</span>"; else {echo "<span class='label label-danger'> Unknown </span>";} ?></td>
              <td style="width:10%;"><?php echo date('m/d/Y', strtotime($value->date_examined)) ?></td>
              <td style="width:15%;"><?php echo $tb_drugs_taken_value; ?></td>
              <td style="width:60%; text-align:left;"><?php if($value->result != "") echo $value->result; else {echo "<span class='label label-danger'> None Indicated </span>";} ?></td>
            </tr>
          <?php } ?>

        </table>
        @else
          <div class="alert alert-warning">
            <i class="icon fa fa-minus-circle"></i> No DSSM Result History was Found
          </div>
        @endif


        <div class = "row">
          <div class="col-md-12">
            @if($isNewHealthcare === false && $isDisabled === "" || $isNewHealthcare == true)
            <legend style="padding-bottom:10px;">Direct Sputum Smear Microscopy Results</legend>

            <div class="row">
              <div class="form-group col-md-7">
                <label class="col-md-2 control-label">Disease Treatment Regimen</label>
                <div class="col-md-10">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default">
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Category_Of_Treatment" value="1" > Category 1
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default">
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Category_Of_Treatment" value="1A"> Category 1A
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Category_Of_Treatment" value="2"> Category 2
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Category_Of_Treatment" value="2A"> Category 2A
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group col-md-5 icheck">
                <label class="col-md-2 control-label">Drugs to be Taken</label>
                <div class="col-md-2">
                  <div class="checkbox">
                    <label>
                      <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" id="H_Drug_Button" name="Tb_Drugs_Taken[]" value="H"> H
                    </label>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="checkbox">
                    <label>
                      <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" id="R_Drug_Button" name="Tb_Drugs_Taken[]" value="R"> R
                    </label>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="checkbox">
                    <label>
                      <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" id="Z_Drug_Button" name="Tb_Drugs_Taken[]" value="Z"> Z
                    </label>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="checkbox">
                    <label>
                      <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" id="E_Drug_Button" name="Tb_Drugs_Taken[]" value="E"> E
                    </label>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="checkbox">
                    <label>
                      <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" id="S_Drug_Button" name="Tb_Drugs_Taken[]" value="S"> S
                    </label>
                  </div>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="form-group col-md-7">
                <label class="col-md-2 control-label">Month</label>
                <div class="col-md-10">
                  <div class="btn-group toggler" data-toggle="buttons">
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default <?php if($data['month'] == '0') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Month" value="0" <?php if($data['month'] == '0') {echo 'checked=\'checked\' ';} ?>> 0
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default <?php if($data['month'] == '1') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Month" value="1" <?php if($data['month'] == '1') {echo 'checked=\'checked\' ';} ?>> 1
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default <?php if($data['month'] == '2') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Month" value="2" <?php if($data['month'] == '2') {echo 'checked=\'checked\' ';} ?>> 2
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default <?php if($data['month'] == '3') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Month" value="3" <?php if($data['month'] == '3') {echo 'checked=\'checked\' ';} ?>> 3
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default <?php if($data['month'] == '4') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Month" value="4" <?php if($data['month'] == '4') {echo 'checked=\'checked\' ';} ?>> 4
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default <?php if($data['month'] == '5') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Month" value="5" <?php if($data['month'] == '5') {echo 'checked=\'checked\' ';} ?>> 5
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default <?php if($data['month'] == '6') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Month" value="6" <?php if($data['month'] == '6') {echo 'checked=\'checked\' ';} ?>> 6
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default <?php if($data['month'] == '7') {echo 'active';} ?>" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Month" value="7" <?php if($data['month'] == '7') {echo 'checked=\'checked\' ';} ?>> >7
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group col-md-5">
                <label class="col-md-4 control-label">Date of Examination</label>
                <div class="col-md-8">
                  <div class="input-group">
                    <i class="fa fa-calendar inner-icon"></i>
                    {!! Form::text('Date_Examined', (empty($data['date_examined']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['dssm']->last()->date_examined))), ['class' => 'form-control', 'id'=>'datepicker', $isDisabled, $ajaxIsDisabled]); !!}

                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label class="col-md-2 control-label">Result</label>
                <div class="col-md-10 ui-widget">
                  {!! Form::textarea('Dssm_Result', $data['result'], ['class' => 'form-control noresize', 'placeholder' => 'Put Results Here', 'cols'=>'10', 'rows'=>'5', $isDisabled, $ajaxIsDisabled]) !!}
                </div>
              </div>
            </div>
            @endif
          </div>
        </div>
        <div class="box-footer">
          @if($isNewHealthcare === false && $isDisabled === "")
          <div class="col-md-12">
            <input {{$isDisabled}} type="button" id="SaveDssmResultButton" class="btn btn-primary pull-left" value="Save DSSM Result">
          </div>
          @elseif($isNewHealthcare == true && $isDisabled === "")
          <div class="col-md-12">
            <input disabled type="button" id="SaveDssmResultButton" class="btn btn-primary pull-left" value="Save Healthcare to Enable">
          </div>
          @endif
        </div>
      </div>

      <div class="tab-pane" id="drugs_dosages_and_preparations_tab">
        <legend style="padding:10px;">Drugs: Dosages and Preparations

          @if ($isDisabled == "")
          <a href="#dosages_and_preparations_modal"  role="button" data-toggle="modal" data-dismiss = "modal" class="pull-right btn btn-medium btn-success" style="margin-bottom:15px">View Drugs and Preparations History</a>@endif</legend>
        @if ($isDisabled == "")
        <div class = "row">
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label">Isoniazid [H] 10mg/kg (200mg/5ml)</label>
              <div class="col-md-6">
               <input {{$ajaxIsDisabled}} {{$isDisabled}} type="number" class="form-control" name="Child_Isoniazid" placeholder="Measure in Milliliters" value="" >
              </div>
              <div class="col-md-2">
               <input {{$isDisabled}} type="text" class="form-control" placeholder="ml" value="ml" disabled>
              </div>
            </div>
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label sampleClass">Rifampicin [R] 15mg/kg (200mg/5ml)</label>
              <div class="col-md-6">
               <input {{$ajaxIsDisabled}} {{$isDisabled}} type="number" class="form-control" name="Child_Rifampicin" placeholder="Measure in Milliliters" value="" >
              </div>
              <div class="col-md-2">
               <input {{$isDisabled}} type="text" class="form-control" placeholder="ml" value="ml" disabled>
              </div>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label sampleClass">Pyrazinamide [Z] 30mg/kg (250mg/5ml)</label>
              <div class="col-md-6">
               <input {{$ajaxIsDisabled}} {{$isDisabled}} type="number" class="form-control" name="Child_Pyrazinamide" placeholder="Measure in Milliliters" value="" >
              </div>
              <div class="col-md-2">
               <input {{$isDisabled}} type="text" class="form-control" placeholder="ml" value="ml" disabled>
              </div>
            </div>
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label sampleClass">Ethambutol [E] 20mg/kg (400mg tab)</label>
              <div class="col-md-6">
               <input {{$ajaxIsDisabled}} {{$isDisabled}} type="number" class="form-control" name="Child_Ethambutol" placeholder="Measure in Milliliters" value="" >
              </div>
              <div class="col-md-2">
               <input {{$isDisabled}} type="text" class="form-control" name="Temperature" placeholder="ml" value="ml" disabled>
              </div>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label sampleClass">Streptomycin [S] 15mg/kg (1g/vial)</label>
              <div class="col-md-6">
                <input {{$ajaxIsDisabled}} {{$isDisabled}} type="number" class="form-control" name="Child_Streptomycin" placeholder="Measure in Milliliters" value="" >
              </div>
              <div class="col-md-2">
                <input {{$isDisabled}} type="text" class="form-control" name="Temperature" placeholder="ml" value="ml" disabled>
              </div>
            </div>
          </div>
        </div>

        <div class="box-footer">
          <div class="col-md-12">
            @if($isNewHealthcare === false && $isDisabled === "")
            <input {{$isDisabled}} type="button" id="SaveDosagesAndPreparationButton" class="btn btn-primary pull-left" value="Save Dosages and Preparation Tab">
            @elseif($isNewHealthcare == true && $isDisabled === "")
            <input disabled type="button" id="SaveDosagesAndPreparationButton" class="btn btn-primary pull-left" value="Save Healthcare to Enable">
            @endif
          </div>
        </div>
        @else
          <?php if (null !== ($data['dosagesPreparations']->first())) {

           // dd($data['dosagesPreparations']);
          if ($data['dosagesPreparations']) { ?>
          <table class ="table table-bordered table-striped">
              <?php
              $drugsParamArray = array("Date", "Isoniazid [H] 10mg/kg (200mg/5ml)", "Rifampicin [R] 15mg/kg (200mg/5ml)", "Pyrazinamide [Z] 30mg/kg (250mg/5ml)", "Ethambutol [E] 20mg/kg (400mg tab)", "Streptomycin [S] 15mg/kg (1g/vial)");
              ?>

              <tr class="thead-inverse">
                <th>Drugs</th>
                <?php
                  foreach ($data['dosagesPreparations'] as $key => $value) {
                    if ($key != 0 ) {
                      echo "<th><center>" . $key . "</center></th>";
                    }
                    else {
                      echo "<th><center>Initial Visit</center></th>";
                    }
                  }
                ?>
              </tr>

              <?php
              foreach($drugsParamArray as $drugsParamKey => $drugsParam) { ?>

                <tr>
                  <td><?php echo $drugsParam ?> </td>
                  <?php

                  foreach ($data['dosagesPreparations'] as $key => $value) {
                    //echo "<td>" . $count++ ."</td>";

                    switch ($drugsParamKey) {
                    case 0:
                      echo "<td>". date('m/d/Y', strtotime($value->created_at)) . "</td>";
                      break;
                    case 1:
                      echo "<td>". $value->child_isoniazid . "</td>";
                      break;
                    case 2:
                      echo "<td>". $value->child_rifampicin . "</td>";
                      break;
                    case 3:
                      echo "<td>". $value->child_pyrazinamide . "</td>";
                      break;
                    case 4:
                      echo "<td>". $value->child_ethambutol . "";
                      break;
                    case 5:
                      echo "<td>". $value->child_streptomycin . "</td>";
                      break;
                  }

                  } ?>
                </tr>

              <?php  } ?>


          </table>
          <?php } else {
            echo "No Record was Found";
          }
          ?>
          <?php } else { ?>
            <div class="alert alert-warning">
              <i class="icon fa fa-minus-circle"></i> No Drugs Dosages and Preparations Record History was Found
            </div>
          <?php } ?>
        @endif
      </div>

      <div class="tab-pane active" id="clinical_history_tab">

        <legend style="padding:10px;">Clinical Examination before and during treatment:
          @if($isDisabled == "")
          <a href="#clinical_exam_history_modal"  role="button" data-toggle="modal" data-dismiss = "modal" class="btn btn-medium btn-success pull-right">View Clinical Examination History</a>
          @endif</legend>

        <?php
        $cExamCategory="";
        if($patient_age < 18) {
          $cExamCategory = "CHILD";
        }
        else {
          $cExamCategory = "ADULT";
        }

        ?>

        {!! Form::hidden('Clinical_Exam_Category', $cExamCategory) !!}
        @if ($isDisabled == "")
        <div class="row">
          <div class="col-md-12">
            <div class="form-group col-md-5">
              <label class="col-md-5 control-label">Weight in KG</label>
              <div class="col-md-7">
                  <input {{$ajaxIsDisabled}} {{$isDisabled}} type="number" class="form-control" placeholder="Weight in Kilogram" min="0" name="Weight_In_Kg"/>
              </div>
            </div>
            <div class="form-group col-md-7">
                <label class="col-md-4 control-label">Unexplained Fever <small>(>2 Weeks)</small></label>
                <div class="col-md-8">
                  <div class="btn-group toggler" data-toggle="buttons" required="required">
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Unexplained_Fever" value="Y"> Yes
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Unexplained_Fever" value="N"> No
                    </label>
                    <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                     <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Unexplained_Fever" value="U"> Unknown
                    </label>
                  </div>
                </div>
              </div>
          </div><!--./col-md-6-->
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group col-md-5">
              <label class="col-md-5 control-label">Unexplained Cough/Wheezing <small>(>2 Weeks)</small></label>
              <div class="col-md-7">
                <div class="btn-group toggler" data-toggle="buttons" required="required">
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Unexplained_Cough" value="Y"> Yes
                  </label>
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Unexplained_Cough" value="N"> No
                  </label>
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Unexplained_Cough" value="U"> Unknown
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group col-md-7">
              <label class="col-md-4 control-label">Unimproved General Well-Being</label>
              <div class="col-md-8">
                <div class="btn-group toggler" data-toggle="buttons" required="required">
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Unimproved_Well_Being" value="F"> Fatigue
                  </label>
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Unimproved_Well_Being" value="RP"> Reduced Playfulness
                  </label>
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Unimproved_Well_Being" value="L"> Lethargy
                  </label>
                </div>
              </div>
            </div>
          </div><!--./col-md-6-->
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="form-group col-md-5">
              <label class="col-md-5 control-label">Poor Appetite<small>(>2 Weeks)</small></label>
              <div class="col-md-7">
                <div class="btn-group toggler" data-toggle="buttons" required="required">
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Poor_Appetite" value="Y"> Yes
                  </label>
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Poor_Appetite" value="N"> No
                  </label>
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Poor_Appetite" value="U"> Unknown
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group col-md-7">
              <label class="col-md-4 control-label">Positive PE findings <small> (for Extra-pulmonary TB) </small></label>
              <div class="col-md-8">
                <div class="btn-group toggler" data-toggle="buttons" required="required">
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Positive_Pe_Findings" value="Y"> Yes
                  </label>
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Positive_Pe_Findings" value="N"> No
                  </label>
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Positive_Pe_Findings" value="U"> Unknown
                  </label>
                </div>
              </div>
            </div>
          </div><!--./col-md-6-->
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="form-group col-md-12 icheck">
              <label class="col-md-2 control-label">Side Effects</label>
              <div class="col-md-3">
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" name="Side_Effects[]" value="Itchiness" > Itchiness
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" name="Side_Effects[]" value="Skin Rashes" > Skin Rashes
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" name="Side_Effects[]" value="Vomiting" > Vomiting
                  </label>
                </div>
              </div>
              <div class="col-md-3">
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" name="Side_Effects[]" value="Abdominal Pain" > Abdominal Pain
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" name="Side_Effects[]" value="Joint Pains" > Joint Pains
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" name="Side_Effects[]" value="Numbness" > Numbness
                  </label>
                </div>
              </div>
              <div class="col-md-3">
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" name="Side_Effects[]" value="Yellowing of Skin" > Yellowing of Skin
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" name="Side_Effects[]" value="Visual Disturbance" > Visual Disturbance
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" name="Side_Effects[]" value="Hearing Disturbance" > Hearing Disturbance
                  </label>
                </div>
              </div>
            </div>
          </div><!--./col-md-6-->
        </div>
        @else
        <?php if (null !== ($data['clinicalHistory']->first())) {

           // dd(is_null($data['clinicalHistory']));
          // if (!is_null($data['clinicalHistory'])) { ?>
            <table class="table table-bordered table-striped">
            <?php
              $clinicalExamArray = array("Date", "Weight in KG", "Unexplained fever > 2 weeks", "Unexplained cough / wheezing > 2 weeks", "Unimproved general well being","Poor appetite","Positive PE findings for Extra-pulmonary TB", "Side Effects", "Category");
            ?>
              <tr>
                <th style="width:10%;">Results</th>
                <?php
                  foreach ($data['clinicalHistory'] as $key => $value) {
                    if ($key != 0 ) {
                      echo "<th><center>" . $key . "</center></th>";
                    }
                    else {
                      echo "<th><center>Initial Visit</center></th>";
                    }
                  }
                ?>
              </tr>


              <?php
              foreach($clinicalExamArray as $clinicalExamKey =>$clinicalExam) { ?>
                <tr>
                  <td style="width:10%;"><?php echo $clinicalExam ?> </td>
                  <?php foreach ($data['clinicalHistory'] as $key => $value) {

                    $sEffect = "";

                    parse_str($value->side_effects, $side_effects_array);

                    $side_effects_value = "";

                    if (isset($side_effects_array['Side_Effects'])) {
                      foreach ($side_effects_array['Side_Effects'] as $key => $side_effects) {
                        $side_effects_value .= " <span class='label label-default'>" . $side_effects . "</span> ";
                        }
                      } else {
                        $side_effects_value = "<span class='label label-danger'>None</span>";
                    }



                    if($value->unimproved_well_being == "F") {
                      $unimproved_well_being_value ="Fatigue";
                    } elseif ($value->unimproved_well_being == "L") {
                      $unimproved_well_being_value ="Lethargy";
                    } else {
                      $unimproved_well_being_value ="Reduced Playfulness";
                    }

                    switch ($clinicalExamKey) {
                      case 0:
                        echo "<td style='width:10%;'>". date('m/d/Y', strtotime($value->created_at)) . "</td>";
                        break;
                      case 1:
                        echo "<td style='width:10%;'>". $value->weight_in_kg . " KG</td>";
                        break;
                      case 2:
                        $fevColor = "danger";
                        $fevMeaning = "None";

                        if ($value->unexplained_fever == "Y") {
                          $fevColor = "success";
                          $fevMeaning = "Yes";
                        } elseif ($value->unexplained_fever == "U") {
                          $fevColor = "warning";
                          $fevMeaning = "Unknown";
                        }

                        echo "<td style='width:10%;'><span class='label label-" . $fevColor . "'>". $fevMeaning . "</span></td>";

                        // echo "<td style='width:10%;'><span class='label label-success'>". $value->unexplained_fever . "</span></td>";
                        break;
                      case 3:
                        $coughColor = "danger";
                        $coughMeaning = "None";

                        if ($value->unexplained_cough == "Y") {
                          $coughColor = "success";
                          $coughMeaning = "Yes";
                        } elseif ($value->unexplained_cough == "U") {
                          $coughColor = "warning";
                          $coughMeaning = "Unknown";
                        }

                        echo "<td style='width:10%;'><span class='label label-" . $coughColor . "'>". $coughMeaning . "</span></td>";

                        // echo "<td style='width:10%;'>". $value->unexplained_cough . "</td>";
                        break;
                      case 4:

                        echo "<td style='width:10%;'><span class='label label-default'>" . $unimproved_well_being_value . "</span></td>";
                        break;
                      case 5:
                        $appColor = "danger";
                        $appMeaning = "None";

                        if ($value->poor_appetite == "Y") {
                          $appColor = "success";
                          $appMeaning = "Yes";
                        } elseif ($value->poor_appetite == "U") {
                          $appColor = "warning";
                          $appMeaning = "Unknown";
                        }

                        echo "<td style='width:10%;'><span class='label label-" . $appColor . "'>". $appMeaning . "</span></td>";
                        // echo "<td style='width:10%;'>". $value->poor_appetite . "</td>";
                        break;
                      case 6:
                        $peColor = "danger";
                        $peMeaning = "None";

                        if ($value->positive_pe_findings == "Y") {
                          $peColor = "success";
                          $peMeaning = "Yes";
                        } elseif ($value->positive_pe_findings == "U") {
                          $peColor = "warning";
                          $peMeaning = "Unknown";
                        }

                        echo "<td style='width:10%;'><span class='label label-" . $peColor . "'>". $peMeaning . "</span></td>";
                        // echo "<td style='width:10%;'>". $value->positive_pe_findings . "</td>";
                        break;
                      case 7:
                        echo "<td style='width:10%;'>". $side_effects_value . "</td>";
                        break;
                      case 8:
                        $catColor = "danger";

                        if ($value->clinical_exam_category == "ADULT") {
                          $catColor = "success";
                        }
                        echo "<td style='width:10%;'><span class='label label-" . $catColor . "'>". $value->clinical_exam_category . "</span></td>";
                        break;
                    }
                } ?>
                </tr>

              <?php } ?>
            </table>
            <!-- <small>* F-Fatigue, RP-Reduced Playfulness, L-Lethargy</small><br /> -->
            <!-- <small>** I-Itchiness, SR-Skin Rashes, V-Vomiting, AP-Abdominal Pain, JP-Joint Pains, N-Numbness, YoS-Yellowing of Skin, VD-Visual Disturbance, HD-Hearing Disturbance</small> -->

          <?php
        } else { ?>
          <div class="alert alert-warning">
            <i class="icon fa fa-minus-circle"></i> No Clinical Examination History was Found
          </div>
        <?php } ?>
        @endif
        @if($isNewHealthcare === false && $isDisabled === "")
        <div class="row">
          <div class="col-md-12">
            <input type="button" id="SaveClinicalHistoryButton" class="btn btn-primary pull-left" value="Save Clinical History">
          </div>
        </div>
        @elseif($isNewHealthcare == true && $isDisabled === "")
        <div class="row">
          <div class="col-md-12">
            <input disabled type="button" id="SaveClinicalHistoryButton" class="btn btn-primary pull-left" value="Save Healthcare to Enable">
          </div>
        </div>
        @endif
        </br>

      </div>

      <div class="tab-pane" id="drug_intake_record_tab">
        <div class="row">
          <div class="col-md-12">

            <div class="box-body">
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel box box-primary">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" style="color:#f56954;" href="#collapseOne">
                        Drug Intake (Intensive Phase) &#9660;
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="box-body">
            {!! Form::hidden('Drug_Intake_Type_Intensive','I') !!}

            <div class="row">
              <div class="col-md-12">
                <?php if (null !== ($data['drugIntake']->where('adult_intake_type', 'I')->first())) { ?>
                  <div><center>
                      <input {{$isDisabled}} type="button" value="&#xf058;" class="specbtn specbtnx atp"/> Administered by Treatment Partner |
                      <input {{$isDisabled}} type="button" value="&#xf057;" class="specbtn specbtnx self" /> Self-administered |
                      <input {{$isDisabled}} type="button" value="&#xf111;" class="specbtn specbtnx miss" /> Missed </p>
                      @if ($isDisabled == "")
                      <small>(Click the circles to remove that day's record)</small>
                      @endif
                  </center></div>
                  <table>
                    <tr>
                      <th style="width: 10%">Month</th>
                      <?php
                        for($x=1; $x<=31; $x++) {
                          echo "<th style='width: 2%'>". $x ."</th>";
                        }
                      ?>
                      <th style="width: 10%">Doses Given<br />this Month</th>
                      <th style="width: 10%">Cumulative<br />Doses</th>
                    </tr>



              <?php

              // dd($data['drugIntake']->sortBy('adult_date_of_administration')->first()->adult_date_of_administration);
              $startMonth = (date('m', (strtotime($data['drugIntake']->where('adult_intake_type', 'I')->sortBy('adult_date_of_administration')->first()->adult_date_of_administration))));
              $endMonth = (date('m', (strtotime($data['drugIntake']->where('adult_intake_type', 'I')->sortBy('adult_date_of_administration')->last()->adult_date_of_administration))));
              $startYear = (date('Y', (strtotime($data['drugIntake']->where('adult_intake_type', 'I')->sortBy('adult_date_of_administration')->first()->adult_date_of_administration))));
              $endYear = (date('Y', (strtotime($data['drugIntake']->where('adult_intake_type', 'I')->sortBy('adult_date_of_administration')->last()->adult_date_of_administration))));


              $numOfIterations = 2;
              if ($startMonth >= $endMonth) {
                $numOfIterations = (($endMonth+13)-$startMonth) + (($endYear-$startYear-1)*12);
              }
              elseif ($endMonth > $startMonth) {
                $numOfIterations = (($endMonth+1)-$startMonth) + (($endYear-$startYear)*12);
              }

              // dd($data['drugIntake']->sortByDesc('adult_date_of_administration')->first()->adult_date_of_administration);
              $a = $startMonth;
              $monthCounter = (12-$startMonth)+1;

              $cumulativeCaseCounter = 0;

              for ($year = $startYear; $year <= $endYear; $year++) {
                for($a; $a<($startMonth+$numOfIterations) && $monthCounter > 0; $a++) {
                  $dateObj   = DateTime::createFromFormat('!m', ($a%12));
                  $monthName = $dateObj->format('F') ;
                  $monthCounter--;
                  $caseCounter = 0;

                  ?>



                  <?php

                  $form_circles = "";



                  for($b=1; $b<=31; $b++) {
                    $hasMatch = false;
                    // print("HELLO");
                    $icon = "&#xf058";
                    $color = "specbtn";
                    $month_intake_int = "";
                    $day_intake_int = "";
                    $year_intake_int = "";
                    $monthSave = "";
                    $daySave = "";
                    $yearSave = "";

                    foreach ($data['drugIntake']->where('adult_intake_type', 'I') as $key => $value) {
                      $value;
                      // echo (date('m', (strtotime($value->adult_date_of_administration))) . " " . $a . "</br>");

                      $month_intake_int = (date('m', (strtotime($value->adult_date_of_administration))));
                      $day_intake_int = (date('d', (strtotime($value->adult_date_of_administration))));
                      $year_intake_int = (date('Y', (strtotime($value->adult_date_of_administration))));

                      if (($month_intake_int == $a%12) && ($day_intake_int == $b) && ($year_intake_int == $year) )  {

                        // echo (date('mdY', (strtotime($value->adult_date_of_administration)))) . "</br>";
                        $hasMatch = true;
                        $caseCounter++;
                        $cumulativeCaseCounter++;

                        switch ($value->adult_drug_administrator) {
                          case 'TP':
                            $icon = '&#xf058';
                            break;
                          case 'SLF':
                            $icon = '&#xf057';
                            break;
                          case 'MISS':
                            $icon = '&#xf111';
                            break;
                          default:
                            $icon = '&#xf10c';
                            break;
                        }

                        switch ($value->adult_drug_administrator) {
                          case 'TP':
                            $color =  'specbtn atp';
                            break;
                          case 'SLF':
                            $color =  'specbtn self';
                            break;
                          case 'MISS':
                            $color =  'specbtn miss';
                            break;
                          default:
                            $color =  'specbtn';
                            break;
                        }

                        $monthSave = $month_intake_int;
                        $daySave = $day_intake_int;
                        $yearSave = $year_intake_int;
                      }
                    }

                    if ($hasMatch == true) {
                      $form_circles .= "<td class='noPadding center'>
                      <input " . $isDisabled . " type='button' value='" . $icon . "' class='circleButtonIntensive " . $color . "' id='" . $monthSave . "/" . $daySave . "/" . $yearSave ."' /></td>";
                      }
                      else {
                        $form_circles .= "<td class='noPadding center'>
                          <input " . $isDisabled . " type='button' value='&#xf10c' class='specbtn' /></td>";
                      }

                    // if ($caseCounter > 0) {

                    // }
                  }

                  if($caseCounter > 0) {
                    echo "<tr id='drugIntakeRow" . $a ."'>
                    <td class='noPadding center'>
                      <input class='form-control' type='text' class='form-control' style='width:200px' value='" . $monthName . " " . $year ."' disabled></td>";

                    echo $form_circles;
                    // dd($data['drugIntake']->where('adult_intake_type', 'I')->where('adult_date_of_administration', '2016-03-09 00:00:00'));

                    echo "<td class='noPadding center'>
                      <input class='' type='text' value='" . $caseCounter . "'  disabled/></td><td class='noPadding center'><input {{$isDisabled}} class='' type='text' value='" . $cumulativeCaseCounter . "' disabled/></td></tr>";
                  }



                 }
                $monthCounter = 12;
                } ?>
                  </table>
                <!-- </div> --> &nbsp;
                <?php } else { ?>
                  <div class="alert alert-warning">
                    <i class="icon fa fa-minus-circle"></i> No Drug Intake (Intensive Phase) Record History was Found
                  </div>
                <?php } ?>
            </div>

            @if($isNewHealthcare === false && $isDisabled === "" || $isNewHealthcare == true)
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label">Date of Administration</label>
              <div class="col-md-8">
                <div class="input-group">
                  <i class="fa fa-calendar inner-icon"></i>
                  {!! Form::text('Adult_Intensive_Date_Of_Administration',  getCurrentDate('m/d/Y'), ['class' => 'form-control', 'id'=>'datepicker', $isDisabled, $ajaxIsDisabled]); !!}
                </div>
              </div>
            </div>
            <div class="form-group col-md-6">
              <label class="col-md-4 control-label"><small>Who administered the drug?</small></label>
              <div class="col-md-8">
                <div class="btn-group toggler" data-toggle="buttons">
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default active" >
                    <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Adult_Intensive_Drug_Administrator" value="TP" checked> Treatment Partner
                  </label>
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                    <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Adult_Intensive_Drug_Administrator" value="SLF"> Self
                  </label>
                  <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
                    <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Adult_Intensive_Drug_Administrator" value="MISS"> Missed
                  </label>
                </div>
              </div>
            </div>
            @endif

            <div class="row">
              @if($isNewHealthcare === false && $isDisabled === "")
              <div class="col-md-12">
                  <input {{$isDisabled}} type="button" id="SaveDrugHistoryIntensiveButton" class="btn btn-primary pull-left" value="Save Drug History (Intensive Phase)">
              </div>
              @elseif($isNewHealthcare == true && $isDisabled === "")
              <div class="col-md-12">
                  <input disabled type="button" id="SaveDrugHistoryIntensiveButton" class="btn btn-primary pull-left" value="Save Healthcare to Enable">
              </div>
              @endif
            </div>
                    </div>
                  </div>
                </div>
                </div>
                <div class="panel box box-danger">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" style="color:#f56954;" href="#collapseTwo">
                        Drug Intake (Continuous Phase) &#9660;
                      </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="box-body">
{!! Form::hidden('Drug_Intake_Type_Continuous','C') !!}


    <div class="row">
      <div class="col-md-12">
        <?php if (null !== ($data['drugIntake']->where('adult_intake_type', 'C')->first())) { ?>
          <div><center>
              <input {{$isDisabled}} type="button" value="&#xf058;" class="specbtn specbtnx atp"/> Administered by Treatment Partner |
              <input {{$isDisabled}} type="button" value="&#xf057;" class="specbtn specbtnx self" /> Self-administered |
              <input {{$isDisabled}} type="button" value="&#xf111;" class="specbtn specbtnx miss" /> Missed</p>
              @if($isDisabled == "")
              <small>(Click the circles to remove that day's record)</small>
              @endif
          </center></div>
          <table>
            <tr>
              <th style="width: 10%">Month</th>
              <?php
                for($x=1; $x<=31; $x++) {
                  echo "<th style='width: 2%'>". $x ."</th>";
                }
              ?>
              <th style="width: 10%">Doses Given<br />this Month</th>
              <th style="width: 10%">Cumulative<br />Doses</th>
            </tr>



      <?php

      // dd($data['drugIntake']->sortBy('adult_date_of_administration')->first()->adult_date_of_administration);
      $startMonth = (date('m', (strtotime($data['drugIntake']->where('adult_intake_type', 'C')->sortBy('adult_date_of_administration')->first()->adult_date_of_administration))));
      $endMonth = (date('m', (strtotime($data['drugIntake']->where('adult_intake_type', 'C')->sortBy('adult_date_of_administration')->last()->adult_date_of_administration))));
      $startYear = (date('Y', (strtotime($data['drugIntake']->where('adult_intake_type', 'C')->sortBy('adult_date_of_administration')->first()->adult_date_of_administration))));
      $endYear = (date('Y', (strtotime($data['drugIntake']->where('adult_intake_type', 'C')->sortBy('adult_date_of_administration')->last()->adult_date_of_administration))));


      $numOfIterations = 2;
      if ($startMonth >= $endMonth) {
        $numOfIterations = (($endMonth+13)-$startMonth) + (($endYear-$startYear-1)*12);
      }
      elseif ($endMonth > $startMonth) {
        $numOfIterations = (($endMonth+1)-$startMonth) + (($endYear-$startYear)*12);
      }

      // dd($data['drugIntake']->sortByDesc('adult_date_of_administration')->first()->adult_date_of_administration);
      $a = $startMonth;
      $monthCounter = (12-$startMonth)+1;

      $cumulativeCaseCounter = 0;

      for ($year = $startYear; $year <= $endYear; $year++) {
        for($a; $a<($startMonth+$numOfIterations) && $monthCounter > 0; $a++) {
          $dateObj   = DateTime::createFromFormat('!m', ($a%12));
          $monthName = $dateObj->format('F') ;
          $monthCounter--;
          $caseCounter = 0;


          $form_circles = "";

          for($b=1; $b<=31; $b++) {
            $hasMatch = false;
            $icon = "&#xf058";
            $color = "specbtn";
            $month_intake_cont = "";
            $day_intake_cont = "";
            $year_intake_cont = "";
            $monthSave = "";
            $daySave = "";
            $yearSave = "";

            // echo $year . " " . $a%12 . " " . $b . "\n";

            foreach ($data['drugIntake']->where('adult_intake_type', 'C') as $key => $value) {

              // echo (date('m', (strtotime($value->adult_date_of_administration))) . " " . $a . "</br>");
              $month_intake_cont = (date('m', (strtotime($value->adult_date_of_administration))));
              $day_intake_cont = (date('d', (strtotime($value->adult_date_of_administration))));
              $year_intake_cont = (date('Y', (strtotime($value->adult_date_of_administration))));

              if (($month_intake_cont == $a%12) && ($day_intake_cont == $b) && ($year_intake_cont == $year) )  {

                // echo (date('mdY', (strtotime($value->adult_date_of_administration)))) . "</br>";
                $hasMatch = true;
                $caseCounter++;
                $cumulativeCaseCounter++;

                switch ($value->adult_drug_administrator) {
                  case 'TP':
                    $icon = '&#xf058';
                    break;
                  case 'SLF':
                    $icon = '&#xf057';
                    break;
                  case 'MISS':
                    $icon = '&#xf111';
                    break;
                  default:
                    $icon = '&#xf10c';
                    break;
                }

                switch ($value->adult_drug_administrator) {
                case 'TP':
                  $color =  'specbtn atp';
                  break;
                case 'SLF':
                  $color =  'specbtn self';
                  break;
                case 'MISS':
                  $color =  'specbtn miss';
                  break;
                default:
                  $color =  'specbtn';
                  break;
                }

                $monthSave = $month_intake_cont;
                $daySave = $day_intake_cont;
                $yearSave = $year_intake_cont;

                $form = "<td class='noPadding center'>
              <input {{$isDisabled}} type='button' value='" . $icon . "' class='circleButtonContinuous " . $color . "' id='" . $month_intake_cont . "/" . $day_intake_cont . "/" . $year_intake_cont ."' /></td>";
              // dd($form);
              }
            }


            if ($hasMatch == true) {
              $form_circles .= "<td class='noPadding center'><input " . $isDisabled . " type='button' value='" . $icon . "' class='circleButtonContinuous " . $color . "' id='" . $monthSave . "/" . $daySave . "/" . $yearSave ."' /></td>";
            }
            else {
              $form_circles .= "<td class='noPadding center'><input " . $isDisabled . " type='button' value='&#xf10c' class='specbtn' /></td>";
            }
          }

          if($caseCounter > 0) {
            echo "<tr id='drugIntakeRow" . $a ."'>
            <td class='noPadding center'>
              <input {{$isDisabled}} class='form-control' type='text' class='form-control' style='width:200px' value='" . $monthName . " " . $year ."' disabled></td>";

            echo $form_circles;

            echo "<td class='noPadding center'>
              <input {{$isDisabled}} class='' type='text' value='" . $caseCounter . "'  disabled/></td><td class='noPadding center'><input {{$isDisabled}} class='' type='text' value='" . $cumulativeCaseCounter . "' disabled/></td></tr>";
          }

        }
        $monthCounter = 12;
        } ?>
          </table>
        <?php } else { ?>
          <div class="alert alert-warning">
            <i class="icon fa fa-minus-circle"></i> No Drug Intake (Continuous Phase) Record History was Found
          </div>
        <?php } ?>
      </div>
      &nbsp;
@if($isNewHealthcare === false && $isDisabled === "" || $isNewHealthcare == true)
<div class="form-group col-md-6">
  <label class="col-md-4 control-label">Date of Administration</label>
  <div class="col-md-8">
    <div class="input-group">
      <i class="fa fa-calendar inner-icon"></i>
      {!! Form::text('Adult_Continuous_Date_Of_Administration',  getCurrentDate('m/d/Y'), ['class' => 'form-control', 'id'=>'datepicker', $isDisabled, $ajaxIsDisabled]); !!}
    </div>
  </div>
</div>
<div class="form-group col-md-6">
  <label class="col-md-4 control-label"><small>Who administered the drug?</small></label>
  <div class="col-md-8">
    <div class="btn-group toggler" data-toggle="buttons">
      <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default active" >
        <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Adult_Continuous_Drug_Administrator" value="TP" checked> Treatment Partner
      </label>
      <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
        <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Adult_Continuous_Drug_Administrator" value="SLF"> Self
      </label>
      <label {{$ajaxIsDisabled}} {{$isDisabled}} class="btn btn-default" >
        <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="Adult_Continuous_Drug_Administrator" value="MISS"> Missed
      </label>
    </div>
  </div>
</div>
@endif
<div class="row">
  @if($isNewHealthcare === false && $isDisabled === "")
  <div class="col-md-12">
      <input {{$isDisabled}} type="button" id="SaveDrugHistoryContinuousButton" class="btn btn-primary pull-left" value="Save Drug History (Continuous Phase)">
  </div>
  @elseif($isNewHealthcare == true && $isDisabled === "")
  <div class="col-md-12">
      <input disabled type="button" id="SaveDrugHistoryContinuousButton" class="btn btn-primary pull-left" value="Save Healthcare to Enable">
  </div>
  @endif
  </div>
    </div>



                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
  @else
  <div class="col-md-12">
          <legend>Create New Tuberculosis Case</legend>
          <label class="col-md-3 control-label">Tuberculosis Record ID:</label>
          <div class="col-md-3">
            <input {{$isDisabled}} required type="text" class="form-control" name="Tb_Record_Number" value="" placeholder="Record ID" {{$read}}>
          </div>
          <div class="col-md-6">
              <input {{$isDisabled}} disabled type="button" id="SaveRecordNumberButton" class="btn btn-success pull-left" value="Save Healthcare to Proceed">
          </div>
        </div>

  @endif


  <!-- MODALS -->
  <!-- CLINICAL EXAM HISTORY MODAL -->
  <div id = "clinical_exam_history_modal" class = "modal fade">
    <div class = "modal-dialog">
      <div class = "modal-content">
        <div class = "modal-header"><h3><center>Clinical Examination History</center></h3></div>
        <div class = "modal-body"><center>
          <?php if (!is_null($data['clinicalHistory'])) {

           // dd(is_null($data['clinicalHistory']));
          // if (!is_null($data['clinicalHistory'])) { ?>
            <table class="table table-bordered table-striped">
            <?php
              $clinicalExamArray = array("Date", "Weight in KG", "Unexplained fever > 2 weeks", "Unexplained cough / wheezing > 2 weeks", "Unimproved general well being","Poor appetite","Positive PE findings for Extra-pulmonary TB", "Side Effects", "Category");
            ?>
              <tr>
                <th style="width:10%;">Results</th>
                <?php
                  foreach ($data['clinicalHistory'] as $key => $value) {
                    if ($key != 0 ) {
                      echo "<th><center>" . $key . "</center></th>";
                    }
                    else {
                      echo "<th><center>Initial Visit</center></th>";
                    }
                  }
                ?>
              </tr>


              <?php
              foreach($clinicalExamArray as $clinicalExamKey =>$clinicalExam) { ?>
                <tr>
                  <td style="width:10%;"><?php echo $clinicalExam ?> </td>
                  <?php foreach ($data['clinicalHistory'] as $key => $value) {

                    $sEffect = "";

                    parse_str($value->side_effects, $side_effects_array);

                    $side_effects_value = "";

                    if (isset($side_effects_array['Side_Effects'])) {
                      foreach ($side_effects_array['Side_Effects'] as $key => $side_effects) {
                        $side_effects_value .= " <span class='label label-default'>" . $side_effects . "</span> ";
                        }
                      } else {
                        $side_effects_value = "<span class='label label-danger'>None</span>";
                    }



                    if($value->unimproved_well_being == "F") {
                      $unimproved_well_being_value ="Fatigue";
                    } elseif ($value->unimproved_well_being == "L") {
                      $unimproved_well_being_value ="Lethargy";
                    } else {
                      $unimproved_well_being_value ="Reduced Playfulness";
                    }

                    switch ($clinicalExamKey) {
                      case 0:
                        echo "<td style='width:10%;'>". date('m/d/Y', strtotime($value->created_at)) . "</td>";
                        break;
                      case 1:
                        echo "<td style='width:10%;'>". $value->weight_in_kg . " KG</td>";
                        break;
                      case 2:
                        $fevColor = "danger";
                        $fevMeaning = "None";

                        if ($value->unexplained_fever == "Y") {
                          $fevColor = "success";
                          $fevMeaning = "Yes";
                        } elseif ($value->unexplained_fever == "U") {
                          $fevColor = "warning";
                          $fevMeaning = "Unknown";
                        }

                        echo "<td style='width:10%;'><span class='label label-" . $fevColor . "'>". $fevMeaning . "</span></td>";

                        // echo "<td style='width:10%;'><span class='label label-success'>". $value->unexplained_fever . "</span></td>";
                        break;
                      case 3:
                        $coughColor = "danger";
                        $coughMeaning = "None";

                        if ($value->unexplained_cough == "Y") {
                          $coughColor = "success";
                          $coughMeaning = "Yes";
                        } elseif ($value->unexplained_cough == "U") {
                          $coughColor = "warning";
                          $coughMeaning = "Unknown";
                        }

                        echo "<td style='width:10%;'><span class='label label-" . $coughColor . "'>". $coughMeaning . "</span></td>";

                        // echo "<td style='width:10%;'>". $value->unexplained_cough . "</td>";
                        break;
                      case 4:

                        echo "<td style='width:10%;'><span class='label label-default'>" . $unimproved_well_being_value . "</span></td>";
                        break;
                      case 5:
                        $appColor = "danger";
                        $appMeaning = "None";

                        if ($value->poor_appetite == "Y") {
                          $appColor = "success";
                          $appMeaning = "Yes";
                        } elseif ($value->poor_appetite == "U") {
                          $appColor = "warning";
                          $appMeaning = "Unknown";
                        }

                        echo "<td style='width:10%;'><span class='label label-" . $appColor . "'>". $appMeaning . "</span></td>";
                        // echo "<td style='width:10%;'>". $value->poor_appetite . "</td>";
                        break;
                      case 6:
                        $peColor = "danger";
                        $peMeaning = "None";

                        if ($value->positive_pe_findings == "Y") {
                          $peColor = "success";
                          $peMeaning = "Yes";
                        } elseif ($value->positive_pe_findings == "U") {
                          $peColor = "warning";
                          $peMeaning = "Unknown";
                        }

                        echo "<td style='width:10%;'><span class='label label-" . $peColor . "'>". $peMeaning . "</span></td>";
                        // echo "<td style='width:10%;'>". $value->positive_pe_findings . "</td>";
                        break;
                      case 7:
                        echo "<td style='width:10%;'>". $side_effects_value . "</td>";
                        break;
                      case 8:
                        $catColor = "danger";

                        if ($value->clinical_exam_category == "ADULT") {
                          $catColor = "success";
                        }
                        echo "<td style='width:10%;'><span class='label label-" . $catColor . "'>". $value->clinical_exam_category . "</span></td>";
                        break;
                    }
                } ?>
                </tr>

              <?php } ?>
            </table>
            <!-- <small>* F-Fatigue, RP-Reduced Playfulness, L-Lethargy</small><br /> -->
            <!-- <small>** I-Itchiness, SR-Skin Rashes, V-Vomiting, AP-Abdominal Pain, JP-Joint Pains, N-Numbness, YoS-Yellowing of Skin, VD-Visual Disturbance, HD-Hearing Disturbance</small> -->

          <?php
        } else { ?>
          <div class="alert alert-warning">
            <i class="icon fa fa-minus-circle"></i> No Clinical Examination History was Found
          </div>
        <?php } ?>

        </div>
        <div class = "modal-footer">
          <button class ="btn btn-medium btn-default no-border-radius" data-dismiss = "modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>

  <div id = "dosages_and_preparations_modal" class = "modal fade">
    <div class = "modal-dialog">
      <div class = "modal-content">
        <div class = "modal-header"><h3><center> Drugs Dosages and Preparations Record Historyy</center></h3></div>
        <div class = "modal-body"><center>
          <?php if (!is_null($data['dosagesPreparations'])) {

           // dd($data['dosagesPreparations']);
          if ($data['dosagesPreparations']) { ?>
          <table class ="table table-bordered table-striped">
              <?php
              $drugsParamArray = array("Date", "Isoniazid [H] 10mg/kg (200mg/5ml)", "Rifampicin [R] 15mg/kg (200mg/5ml)", "Pyrazinamide [Z] 30mg/kg (250mg/5ml)", "Ethambutol [E] 20mg/kg (400mg tab)", "Streptomycin [S] 15mg/kg (1g/vial)");
              ?>

              <tr class="thead-inverse">
                <th>Drugs</th>
                <?php
                  foreach ($data['dosagesPreparations'] as $key => $value) {
                    if ($key != 0 ) {
                      echo "<th><center>" . $key . "</center></th>";
                    }
                    else {
                      echo "<th><center>Initial Visit</center></th>";
                    }
                  }
                ?>
              </tr>

              <?php
              foreach($drugsParamArray as $drugsParamKey => $drugsParam) { ?>

                <tr>
                  <td><?php echo $drugsParam ?> </td>
                  <?php

                  foreach ($data['dosagesPreparations'] as $key => $value) {
                    //echo "<td>" . $count++ ."</td>";

                    switch ($drugsParamKey) {
                    case 0:
                      echo "<td>". date('m/d/Y', strtotime($value->created_at)) . "</td>";
                      break;
                    case 1:
                      echo "<td>". $value->child_isoniazid . "</td>";
                      break;
                    case 2:
                      echo "<td>". $value->child_rifampicin . "</td>";
                      break;
                    case 3:
                      echo "<td>". $value->child_pyrazinamide . "</td>";
                      break;
                    case 4:
                      echo "<td>". $value->child_ethambutol . "";
                      break;
                    case 5:
                      echo "<td>". $value->child_streptomycin . "</td>";
                      break;
                  }

                  } ?>
                </tr>

              <?php  } ?>


          </table>
          <?php } else {
            echo "No Record was Found";
          }
          ?>
          <?php } else { ?>
          <div class="alert alert-warning">
            <i class="icon fa fa-minus-circle"></i> No Drugs Dosages and Preparations Record History was Found
          </div>
          <?php  } ?>

        </div>
        <div class = "modal-footer">
          <button class ="btn btn-medium btn-default no-border-radius" data-dismiss = "modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>


<!-- CONTINUOUS PHASE MODAL -->

</div>
</div>

@section('plugin_jsscripts')

<script>
$(function(){
  $('#SaveRecordNumberButton').on('click', function(e){
    e.preventDefault();
    // alert("HEHHEHEH");
    // alert(jQuery("#hservice_id").val());
    var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
    var data={
      hservice_id: $('[name=hservice_id]').val(),
      tbservice_id: $('[name=tbservice_id]').val(),
      Tb_Record_Number: $('[name="Tb_Record_Number"]').val()
    }

    $.ajax({
       headers: {
                'X-CSRF-TOKEN': $('span[name="_token"]').attr('content')
            },
        url: '<?php echo site_url(); ?>plugin/call/Tuberculosis/Tuberculosis/newRecord/'+healthcareservice_id_hehe,
        type: "POST",
        data:data,
        error: function() {
          $('#info').html('<p>An error has occurred</p>');
        },
        success: function(data) {
          console.log(data);
          // alert("SAVED");
          //window.location.reload();
            window.location.href='<?php echo site_url(); ?>/healthcareservices/edit/<?php echo $patient_id; ?>/<?php echo $hservice_id; ?>';
        }
    });
  });

  $('#SaveClinicalHistoryButton').on('click', function(e){
    e.preventDefault();
    // alert("HEHHEHEH");
    // alert(jQuery("#hservice_id").val());
    var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
    var data={
      hservice_id: $('[name=hservice_id]').val(),
      tbservice_id: $('[name=tbservice_id]').val(),
      Tb_Record_Number: $('[name="Tb_Record_Number"]').val(),
      Weight_In_Kg: $('[name="Weight_In_Kg"]').val(),
      Unexplained_Fever: $('[name="Unexplained_Fever"]:checked').val(),
      Unexplained_Cough: $('[name="Unexplained_Cough"]:checked').val(),
      Unimproved_Well_Being: $('[name="Unimproved_Well_Being"]:checked').val(),
      Poor_Appetite: $('[name="Poor_Appetite"]:checked').val(),
      Positive_Pe_Findings: $('[name="Positive_Pe_Findings"]:checked').val(),
      Clinical_Exam_Category: $('[name="Clinical_Exam_Category"]').val(),
      Side_Effects: $('[name="Side_Effects[]"]:checked').serialize()
    }

    $.ajax({
       headers: {
                'X-CSRF-TOKEN': $('span[name="_token"]').attr('content')
            },
        url: '<?php echo site_url(); ?>plugin/call/Tuberculosis/Tuberculosis/saveClinicalExamination/'+healthcareservice_id_hehe,
        type: "POST",
        data:data,
        error: function() {
          $('#info').html('<p>An error has occurred</p>');
        },
        success: function(data) {
          console.log(data);
          // alert("SAVED");
          window.location.reload();
        }
    });
  });

  $('#SaveDssmResultButton').on('click', function(e){
    e.preventDefault();
    // alert("HEHHEHEH");
    // alert(jQuery("#hservice_id").val());
    var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
    var data={
      hservice_id: $('[name=hservice_id]').val(),
      tbservice_id: $('[name=tbservice_id]').val(),

      Category_Of_Treatment: $('[name="Category_Of_Treatment"]:checked').val(),
      Tb_Drugs_Taken: $('[name="Tb_Drugs_Taken[]"]:checked').serialize(),
      Month: $('[name="Month"]:checked').val(),
      Date_Examined: $('[name="Date_Examined"]').val(),
      Dssm_Result: $('[name="Dssm_Result"]').val()

    }

    $.ajax({
       headers: {
                'X-CSRF-TOKEN': $('span[name="_token"]').attr('content')
            },
        url: '<?php echo site_url(); ?>plugin/call/Tuberculosis/Tuberculosis/saveDssm/'+healthcareservice_id_hehe,
        type: "POST",
        data:data,
        error: function() {
          $('#info').html('<p>An error has occurred</p>');
        },
        success: function(data) {
          console.log(data);
          // alert("SAVED");
          window.location.reload();
        }
    });
  });

  $('#SaveDrugHistoryIntensiveButton').on('click', function(e){
    e.preventDefault();
    // alert("HEHHEHEH");
    // alert(jQuery("#hservice_id").val());
    var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
    var data={
      hservice_id: $('[name=hservice_id]').val(),
      tbservice_id: $('[name=tbservice_id]').val(),

      Adult_Intensive_Date_Of_Administration: $('[name="Adult_Intensive_Date_Of_Administration"]').val(),
      Drug_Intake_Type_Intensive: $('[name="Drug_Intake_Type_Intensive"]').val(),
      Adult_Intensive_Drug_Administrator: $('[name="Adult_Intensive_Drug_Administrator"]:checked').val()

    }

    $.ajax({
       headers: {
                'X-CSRF-TOKEN': $('span[name="_token"]').attr('content')
            },
        url: '<?php echo site_url(); ?>plugin/call/Tuberculosis/Tuberculosis/saveDrugIntakeIntensive/'+healthcareservice_id_hehe,
        type: "POST",
        data:data,
        error: function() {
          $('#info').html('<p>An error has occurred</p>');
        },
        success: function(data) {
          console.log(data);
          // alert("SAVED");
          window.location.reload();
        }
    });
  });

  $('#SaveDrugHistoryContinuousButton').on('click', function(e){
    e.preventDefault();
    // alert("HEHHEHEH");
    // alert(jQuery("#hservice_id").val());
    var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
    var data={
      hservice_id: $('[name=hservice_id]').val(),
      tbservice_id: $('[name=tbservice_id]').val(),

      Adult_Continuous_Date_Of_Administration: $('[name="Adult_Continuous_Date_Of_Administration"]').val(),
      Drug_Intake_Type_Continuous: $('[name="Drug_Intake_Type_Continuous"]').val(),
      Adult_Continuous_Drug_Administrator: $('[name="Adult_Continuous_Drug_Administrator"]:checked').val()

    }

    $.ajax({
       headers: {
                'X-CSRF-TOKEN': $('span[name="_token"]').attr('content')
            },
        url: '<?php echo site_url(); ?>plugin/call/Tuberculosis/Tuberculosis/saveDrugIntakeContinuous/'+healthcareservice_id_hehe,
        type: "POST",
        data:data,
        error: function() {
          $('#info').html('<p>An error has occurred</p>');
        },
        success: function(data) {
          console.log(data);
          // alert("SAVED");
          window.location.reload();
        }
    });
  });

  $('#SaveDosagesAndPreparationButton').on('click', function(e){
    e.preventDefault();
    // alert("HEHHEHEH");
    // alert(jQuery("#hservice_id").val());
    var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
    var data={
      hservice_id: $('[name=hservice_id]').val(),
      tbservice_id: $('[name=tbservice_id]').val(),

      Child_Isoniazid: $('[name="Child_Isoniazid"]').val(),
      Child_Rifampicin: $('[name="Child_Rifampicin"]').val(),
      Child_Pyrazinamide: $('[name="Child_Pyrazinamide"]').val(),
      Child_Ethambutol: $('[name="Child_Ethambutol"]').val(),
      Child_Streptomycin: $('[name="Child_Streptomycin"]').val()

    }

    $.ajax({
       headers: {
                'X-CSRF-TOKEN': $('span[name="_token"]').attr('content')
            },
        url: '<?php echo site_url(); ?>plugin/call/Tuberculosis/Tuberculosis/saveDrugs/'+healthcareservice_id_hehe,
        type: "POST",
        data:data,
        error: function() {
          $('#info').html('<p>An error has occurred</p>');
        },
        success: function(data) {
          console.log(data);
          // alert("SAVED");
          window.location.reload();
        }
    });
  });

  $('.circleButtonIntensive').on('click', function(e){
    e.preventDefault();
    // alert("HEHHEHEH");
    // alert(jQuery("#hservice_id").val());

    var id = $(this).attr('id');
    var answer = confirm("Would you like to delete the " + id + " entry?")
    // alert(id);
    if (answer) {
      var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
      // alert(healthcareservice_id_hehe);
      var data={
        hservice_id: $('[name=hservice_id]').val(),
        tbservice_id: $('[name=tbservice_id]').val(),

        Date_To_Be_Deleted: id
      }

      // alert(data);

      $.ajax({
         headers: {
                  'X-CSRF-TOKEN': $('span[name="_token"]').attr('content')
              },
          url: '<?php echo site_url(); ?>plugin/call/Tuberculosis/Tuberculosis/deleteDrugIntakeIntensive/'+healthcareservice_id_hehe,
          type: "POST",
          data:data,
          error: function() {
            $('#info').html('<p>An error has occurred</p>');
          },
          success: function(data) {
            console.log(data);
            // alert("SAVED");
            window.location.reload();
          }
      });
    }
  });

  $('.circleButtonContinuous').on('click', function(e){
    e.preventDefault();
    // alert("HEHHEHEH");
    // alert(jQuery("#hservice_id").val());

    var id = $(this).attr('id');
    var answer = confirm("Would you like to delete the " + id + " entry?")
    // alert(id);
    if (answer) {
      var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
      // alert(healthcareservice_id_hehe);
      var data={
        hservice_id: $('[name=hservice_id]').val(),
        tbservice_id: $('[name=tbservice_id]').val(),

        Date_To_Be_Deleted: id
      }

      // alert(data);

      $.ajax({
         headers: {
                  'X-CSRF-TOKEN': $('span[name="_token"]').attr('content')
              },
          url: '<?php echo site_url(); ?>plugin/call/Tuberculosis/Tuberculosis/deleteDrugIntakeContinuous/'+healthcareservice_id_hehe,
          type: "POST",
          data:data,
          error: function() {
            $('#info').html('<p>An error has occurred</p>');
          },
          success: function(data) {
            console.log(data);
            // alert("SAVED");
            window.location.reload();
          }
      });
    }
  });
});
</script>
@stop
