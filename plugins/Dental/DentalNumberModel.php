<?php

namespace Plugins\Dental;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class DentalNumberModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dental_number';
    protected static $table_name = 'dental_number';
    protected $primaryKey = 'dental_number_id';

    protected $fillable = [];
    protected $touches = array('dental');

    public function dental()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\Dental\DentalModel','dental_id','dental_id');
    }


}
