<?php
    $hservice_id = $data['healthcareservice_id'];
    $dental_id = $data['dental_id'];
    $dental_number = $data['dentalNumber'];
    $oral_condition = $data['oralHealthCondition'];
    $service = $data['dentalService'];

    $oral_health_remarks = NULL;
    if($oral_condition)
    {
      $oral_health_remarks = $oral_condition->oral_health_remarks;
    }
    $service_monitoring_remarks = NULL;
    if($service){
      $service_monitoring_remarks = $service->service_monitoring_remarks;
    }

    $patient_id = getPatientIDByHealthcareserviceID($hservice_id);
    $patient = findPatientByPatientID($patient_id);
    $gender = $patient->gender;

    //let us disable this form if this consult is already disposed
    if(empty($allData['disposition_record']['disposition'])) { $read = ''; }
    else { $read = 'disabled'; }

    $debuffs = explode(',',$data['dental_condition']);
    $names = ['Dental Caries',
              'Gingivitis',
              'Debris',
              'Calculus',
              'Abnormal Growth',
              'Cleft Lip',
              'Others'
            ];

    $numberNames = ['No. of Perm Teeth Present' => 'perm_teeth_present',
                    'No. of Perm. Sound Teeth' => 'perm_sound_teeth',
                    'No. of Decayed Teeth(D)' => 'decayed_teeth',
                    'No. of Missing Teeth(M)' => 'missing_teeth',
                    'No. of Filled Teeth(F)' => 'filled_teeth',
                    'Total DMF teeth' => 'DMF_teeth',
                    'No. of Temp. Teeth Present' => 'temp_teeth_present',
                    'No. of Temp. Sound Teeth' => 'temp_sound_teeth',
                    'No. of Decayed Teeth(d)' => 'temp_decayed_teeth',
                    'No. of Filled Teeth(m)' => 'temp_filled_teeth',
                    'Total df teeth' => 'DF_teeth'
                  ];

    $teethNumber1 = [55,54,53,52,51,61,62,63,64,65];
    $teethNumber2 = [18,17,16,15,14,13,12,11,21,22,23,24,25,26,27,28];
    $teethNumber3 = [48,47,46,45,44,43,42,41,31,32,33,34,35,36,37,38];
    $teethNumber4 = [85,84,83,82,81,71,72,73,74,75];

    $conditions = [" ",'D','d','F','f','M','m','Un','un','JC','jc','P','p'];
    $services = [" ",'S','PF','TF','X','O'];

    $legendOral = [[' ','D','F','M','Un','JC','P'],
                   ['Sound','Decayed','Filed','Missing','Unerupted','Jacket Crown','Pontic'],
                   [' ','d','f','m','un','jc','p']];

    $legendService = [['S','PF','TF','X','O'],
                      ['Sealant','Permanent Filling','Temporary Filling','Extraction','Others']];
?>
    <style type="text/css">
      #extTable{overflow-x: auto;}
      table.tftable {font-size:12px;color:#333333;height:100%;width:100%;border:none; empty-cells: hide;overflow:auto; text-align: center;}
      table.legend {font-size:14px;color:#333333;height:100%;width:20%;margin:0 auto;border:none; empty-cells: hide;overflow:auto; text-align: center;}
      .legend td {padding:8px;}
      .tftable th {font-size:12px;background-color:#285377;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:center;color: #ffffff;}
      .tftable td {font-size:12px;border: none;padding: 8px;}
      .leg {text-align: center;}
    </style>
    <fieldset>
    {!! Form::hidden('dental[dental_id]',$dental_id) !!}
    {!! Form::hidden('dental_number[dental_id]',$dental_id) !!}
    {!! Form::hidden('dental_condition[dental_id]',$dental_id) !!}
    {!! Form::hidden('dental_service[dental_id]',$dental_id) !!}

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#dentalCondition" data-toggle="tab">Dental Condition and Number of Teeth</a></li>
            <li class=""><a href="#oralCondition" data-toggle="tab">Oral Health Condition</a></li>
            <li class=""><a href="#serviceChart" data-toggle="tab">Services Monitoring Chart</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="dentalCondition">
                <fieldset>
                    <legend> Dental Condition and Number of Teeth</legend>
                    <div class="col-sm-12">
                      <label class="col-sm-2 control-label"> Date of oral examination </label>
                      <dl class="col-sm-4">
                          <div class="input-group">
                              <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                              </div>
                              <input id="datepicker" class="form-control" name="dental[date_of_oral_exam]" type="text" value={{ $data['date_of_oral_exam'] }} >
                          </div>
                      </dl>
                        <div class="col-sm-12">
                          <div class="icheck">
                            <dl class="col-sm-4">
                                <dt> Dental Condition </dt>
                                <dd>
                                  @foreach($names as $key)
                                      @if($key == 'Others')
                                          <input type="text" class="form-control" name="dental[dental_condition][]"  id="" placeholder="If Others, please specify."  value="{{ end($debuffs) }}">
                                      @else
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox" {{ $read }} name="dental[dental_condition][]" id="" value='{{$key}}'
                                                <?php
                                                  foreach($debuffs as $val){
                                                      if($val == $key){
                                                          echo "checked";
                                                      }
                                                  }
                                                ?>
                                               > {{ $key }}
                                            </label>
                                        </div>
                                      @endif
                                    @endforeach
                                </dd>
                            </dl>
                          </div>
                            <dl class="col-sm-8">
                                <dt> Indicate Number </dt>
                                <dd>
                                  @foreach($numberNames as $key => $value)
                                  <div class="col-sm-12">
                                      <label class="col-sm-3 control-label"> {{$key}} </label>
                                      @if($value == 'decayed_teeth' || $value == 'missing_teeth' || $value == 'filled_teeth')
                                        <div class="col-sm-6 form-group">
                                            <input type="number" class="form-control perm" name="dental[{{$value}}]" min="0" max="32" value={{ $dental_number[$value] }}>
                                        </div>
                                      @elseif($value == 'DMF_teeth')
                                        <div class="col-sm-6 form-group">
                                            <input type="text" class="form-control" id="DMF" name="dental[{{$value}}]" readonly value={{ $dental_number[$value] }}>
                                        </div>
                                      @elseif($value == 'temp_decayed_teeth' || $value == 'temp_filled_teeth')
                                        <div class="col-sm-6 form-group">
                                            <input type="number" class="form-control temp" name="dental[{{$value}}]" min="0" max="32" value={{ $dental_number[$value] }}>
                                        </div>
                                      @elseif($value == 'DF_teeth')
                                        <div class="col-sm-6 form-group">
                                            <input type="text" class="form-control" id="DF" name="dental[{{$value}}]" readonly value={{ $dental_number[$value] }}>
                                        </div>
                                      @else
                                        <div class="col-sm-6 form-group">
                                            <input type="number" class="form-control" name="dental[{{$value}}]" min="0" max="32" value={{ $dental_number[$value] }}>
                                        </div>
                                      @endif
                                  </div>
                                  @endforeach
                                </dd>
                            </dl>
                        </div>
                    </div>
                </fieldset>
            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="oralCondition">
              <fieldset>
                  <legend>Oral Health Condition</legend>
                  <div class="form-group">
                    <div class="col-md-12" id="extTable">
                      <table class="tftable" border="1">
                          <tr>
                            <td></td><td></td><td></td>
                            @foreach ($teethNumber1 as $key)
                              <th>{{$key}}</th>
                            @endforeach
                            <td></td><td></td><td></td>
                          </tr>
                          <tr>
                            <td></td><td></td><td></td>
                            @foreach ($teethNumber1 as $key)
                              <th><select class="form-control no-padding" name="dental[t{{$key}}_status]" value={{$oral_condition["t".$key."_status"]}}>
                                @foreach ($conditions as $value)
                                  <option value={{$value}}
                                    <?php
                                      if($oral_condition["t".$key."_status"] == $value)
                                        echo ("selected=selected");
                                    ?>
                                  >{{$value}}</option>
                                @endforeach
                              </select></th>
                            @endforeach
                            <td></td><td></td><td></td>
                          </tr>
                          <tr>
                            @foreach ($teethNumber2 as $key)
                              <th><select class="form-control no-padding" name="dental[t{{$key}}_status]" value={{$oral_condition["t".$key."_status"]}}>
                                @foreach ($conditions as $value)
                                  <option value={{$value}}
                                    <?php
                                      if($oral_condition["t".$key."_status"] == $value)
                                        echo ("selected=selected");
                                    ?>
                                  >{{$value}}</option>
                                @endforeach
                              </select></th>
                            @endforeach
                          </tr>
                          <tr>
                            @foreach ($teethNumber2 as $key)
                              <th>{{$key}}</th>
                            @endforeach
                          </tr>
                       </table>
                      <br>
                      <table class="tftable" border="1">
                          <tr>
                            @foreach ($teethNumber3 as $key)
                              <th>{{$key}}</th>
                            @endforeach
                          </tr>
                          <tr>
                            @foreach ($teethNumber3 as $key)
                              <th><select class="form-control no-padding" name="dental[t{{$key}}_status]" value={{$oral_condition["t".$key."_status"]}}>
                                @foreach ($conditions as $value)
                                  <option value={{$value}}
                                    <?php
                                      if($oral_condition["t".$key."_status"] == $value)
                                        echo ("selected=selected");
                                    ?>
                                  >{{$value}}</option>
                                @endforeach
                              </select></th>
                            @endforeach
                          </tr>
                          <tr>
                            <td></td><td></td><td></td>
                            @foreach ($teethNumber4 as $key)
                              <th><select class="form-control no-padding" name="dental[t{{$key}}_status]" value={{$oral_condition["t".$key."_status"]}}>
                                @foreach ($conditions as $value)
                                  <option value={{$value}}
                                    <?php
                                      if($oral_condition["t".$key."_status"] == $value)
                                        echo ("selected=selected");
                                    ?>
                                  >{{$value}}</option>
                                @endforeach
                              </select></th>
                            @endforeach
                            <td></td><td></td><td></td>
                          </tr>
                          <tr>
                            <td></td><td></td><td></td>
                            @foreach ($teethNumber4 as $key)
                              <th>{{$key}}</th>
                            @endforeach
                            <td></td><td></td><td></td>
                          </tr>
                       </table>
                       <br>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-2"> Remarks: </label>
                    <div class="col-md-10">
                      {!! Form::textarea('dental[oral_health_remarks]', $oral_health_remarks, ['class' => 'form-control noresize', 'placeholder' => 'Remarks', 'cols'=>'10', 'rows'=>'5', $read]) !!}
                    </div>
                  </div>
              </fieldset>
              <fieldset>
                <legend>Legend</legend>
                <div class="col-md-12" id="extTable">
                  <div class="col-md-4 col-md-offset-4">
                    <div class="box box-success box-solid">
                      <div class="box-header with-border">
                        <div class="row leg">
                          <div class="col-md-4">
                            <h3 class="box-title">Permanent</h3>
                          </div>
                          <div class="col-md-4">
                            <h3 class="box-title">Tooth Condition</h3>
                          </div>
                          <div class="col-md-4">
                            <h3 class="box-title">Temporary</h3>
                          </div>
                        </div>
                      </div>
                      <div class="box-body">
                        @for($i = 0; $i < sizeof($legendOral[0]);$i++)
                        <div class="row leg">
                          <div class="col-md-4">
                            <em>{{$legendOral[0][$i]}}</em>
                          </div>
                          <div class="col-md-4">
                            <em>{{$legendOral[1][$i]}}</em>
                          </div>
                          <div class="col-md-4">
                            <em>{{$legendOral[2][$i]}}</em>
                          </div>
                        </div>
                        @endfor
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
            </div><!-- /.tab-pane -->
            <div class="tab-pane " id="serviceChart">
              <fieldset>
                  <legend>Services Chart</legend>
                  <div class="form-group">
                    <div class="col-md-12" id="extTable">
                      <table class="tftable" border="1">
                          <tr>
                            <td></td><td></td><td></td>
                            @foreach ($teethNumber1 as $key)
                              <th>{{$key}}</th>
                            @endforeach
                            <td></td><td></td><td></td>
                          </tr>
                          <tr>
                            <td></td><td></td><td></td>
                            @foreach ($teethNumber1 as $key)
                              <th><select class="form-control no-padding" name="dental[t{{$key}}_service]" value={{$service["t".$key."_service"]}}>
                                @foreach ($services as $value)
                                  <option value={{$value}}
                                    <?php
                                      if($service["t".$key."_service"] == $value)
                                        echo ("selected=selected");
                                    ?>
                                  >{{$value}}</option>
                                @endforeach
                              </select></th>
                            @endforeach
                            <td></td><td></td><td></td>
                          </tr>
                          <tr>
                            @foreach ($teethNumber2 as $key)
                              <th><select class="form-control no-padding" name="dental[t{{$key}}_service]" value={{$service["t".$key."_service"]}}>
                                @foreach ($services as $value)
                                  <option value={{$value}}
                                    <?php
                                      if($service["t".$key."_service"] == $value)
                                        echo ("selected=selected");
                                    ?>
                                  >{{$value}}</option>
                                @endforeach
                              </select></th>
                            @endforeach
                          </tr>
                          <tr>
                            @foreach ($teethNumber2 as $key)
                              <th>{{$key}}</th>
                            @endforeach
                          </tr>
                       </table>
                      <br>
                      <table class="tftable" border="1">
                          <tr>
                            @foreach ($teethNumber3 as $key)
                              <th>{{$key}}</th>
                            @endforeach
                          </tr>
                          <tr>
                            @foreach ($teethNumber3 as $key)
                              <th><select class="form-control no-padding" name="dental[t{{$key}}_service]" value={{$service["t".$key."_service"]}}>
                                @foreach ($services as $value)
                                  <option value={{$value}}
                                    <?php
                                      if($service["t".$key."_service"] == $value)
                                        echo ("selected=selected");
                                    ?>
                                  >{{$value}}</option>
                                @endforeach
                              </select></th>
                            @endforeach
                          </tr>
                          <tr>
                            <td></td><td></td><td></td>
                            @foreach ($teethNumber4 as $key)
                              <th><select class="form-control no-padding" name="dental[t{{$key}}_service]" value={{$service["t".$key."_service"]}}>
                                @foreach ($services as $value)
                                  <option value={{$value}}
                                    <?php
                                      if($service["t".$key."_service"] == $value)
                                        echo ("selected=selected");
                                    ?>
                                  >{{$value}}</option>
                                @endforeach
                              </select></th>
                            @endforeach
                            <td></td><td></td><td></td>
                          </tr>
                          <tr>
                            <td></td><td></td><td></td>
                            @foreach ($teethNumber4 as $key)
                              <th>{{$key}}</th>
                            @endforeach
                            <td></td><td></td><td></td>
                          </tr>
                       </table>
                        <br>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-2"> Remarks: </label>
                    <div class="col-md-10">
                      {!! Form::textarea('dental[service_monitoring_remarks]', $service_monitoring_remarks, ['class' => 'form-control noresize', 'placeholder' => 'Remarks', 'cols'=>'10', 'rows'=>'5', $read]) !!}
                    </div>
                  </div>
              </fieldset>
              <fieldset>
                <legend>Legend</legend>
                <div class="col-md-12" id="extTable">
                  <div class="col-md-4 col-md-offset-4">
                    <div class="box box-success box-solid">
                      <div class="box-header with-border">
                        <div class="row leg">
                          <div class="col-md-6">
                            <h3 class="box-title">Value</h3>
                          </div>
                          <div class="col-md-6">
                            <h3 class="box-title">Service</h3>
                          </div>
                        </div>
                      </div>
                      <div class="box-body">
                        @for($i = 0; $i < sizeof($legendService[0]);$i++)
                        <div class="row leg">
                          <div class="col-md-6">
                            <em>{{$legendService[0][$i]}}</em>
                          </div>
                          <div class="col-md-6">
                            <em>{{$legendService[1][$i]}}</em>
                          </div>
                        </div>
                        @endfor
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
            </div>
        </div>
    </div>
    </fieldset>

    {!! HTML::script('public/dist/plugins/jQuery/jQuery-2.1.4.min.js') !!}
    <script type="text/javascript">
      $(document).ready(function(e){
        $(".form-group").on("input",".perm",function(){
            var totalSum = 0;
            $(".perm").each(function(){
                var inputVal = $(this).val();
                if($.isNumeric(inputVal))
                  totalSum += parseFloat(inputVal);
            });
            $('#DMF').attr('value',totalSum);
        });
        $(".form-group").on("input",".temp",function(){
            var totalSum = 0;
            $(".temp").each(function(){
                var inputVal = $(this).val();
                if($.isNumeric(inputVal))
                  totalSum += parseFloat(inputVal);
            });
            $('#DF').attr('value',totalSum);
        });
      });
    </script>
