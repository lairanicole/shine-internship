<?php

$plugin_id = 'Dental';                       //plugin ID
$plugin_module = 'healthcareservices';          //module owner
$plugin_location = 'dropdown';                  //UI location where plugin will be accessible
$plugin_primaryKey = 'dental_id';        //primary_key used to find data
$plugin_table = 'dental';            //plugintable default; table_name custom table
$plugin_relationship = array('dental_number', 'dental_oral_condition', 'dental_service');           //SUBTABLES
$plugin_tabs_child = array('addservice', 'complaints', 'dental_plugin', 'medicalorders', 'disposition');
$plugin_type = "program";
$plugin_gender = "all";
$plugin_age = "00-100";
//plugin maximum role to access this plugin (MANDATORY VALUE)
$plugin_role = 5;

$plugin_relationship = array();
$plugin_folder = 'Dental'; //module owner
$plugin_title = 'Dental';            //plugin title
$plugin_description = 'Dental';
$plugin_version = '1.0';
$plugin_developer = 'ShineLabs';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2018";

$plugin_tabs = [
    'addservice' => 'Basic Information',
    'disposition' => 'Disposition',
    'medicalorders' => 'Medical Orders',
    // 'vitals' => 'Vitals & Physical',
    // 'examinations' => 'Examination',
    // 'impanddiag' => 'Impressions and Diagnosis',
    'complaints' => 'Complaints',
    'dental_plugin' => 'Dental'
];

$plugin_tabs_models = [
    'complaints' => 'GeneralConsultation',
    // 'impanddiag' => 'Diagnosis',
    // 'examinations' => 'Examination',
    'disposition' => 'Disposition',
    'medicalorders' => 'MedicalOrder',
    // 'vitals' => 'VitalsPhysical',
    'dental_plugin' => 'DentalModel'
];
