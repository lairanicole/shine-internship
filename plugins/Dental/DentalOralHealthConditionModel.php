<?php

namespace Plugins\Dental;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class DentalOralHealthConditionModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dental_oral_condition';
    protected static $table_name = 'dental_oral_condition';
    protected $primaryKey = 'dental_oral_id';

    protected $fillable = [];
    protected $touches = array('dental');
    public function dental()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\Dental\DentalModel','dental_id','dental_id');
    }


}
