<?php

use Plugins\Dental\DentalModel as Dental;
use Plugins\Dental\DentalNumberModel as DentalNumber;
use Plugins\Dental\DentalOralHealthConditionModel as DentalOralHealthCondition;
use Plugins\Dental\DentalServicesModel as DentalServices;
use Modules\Helathcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

class DentalController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        //no index
    }

    public function save($data)
    {
        $dental_id = $data['dental_id'];
        if($data) {
            $dental = Dental::where('dental_id','=',$dental_id)->first();
            $dental_number = DentalNumber::where('dental_id','=',$dental_id)->first();
            $dental_condition = DentalOralHealthCondition::where('dental_id','=',$dental_id)->first();
            $dental_service = DentalServices::where('dental_id','=',$dental_id)->first();

            $dental->date_of_oral_exam = $data['date_of_oral_exam'];
            $dental_condition_string = "";
            if(isset($data['dental_condition'])){
                $condition = $data['dental_condition'];
                $dental_condition_string = implode(',', $condition);
            }
            $dental->dental_condition = $dental_condition_string;
            $dental->save();

            if(empty($dental_number)){
              $dental_number = new DentalNumber;
              $dental_number->dental_id = $data['dental_id'];
              $dental_number->dental_number_id = IdGenerator::generateId();
            }
            $dental_number->perm_teeth_present = $data['perm_teeth_present'];
            $dental_number->perm_sound_teeth = $data['perm_sound_teeth'];
            $dental_number->decayed_teeth = $data['decayed_teeth'];
            $dental_number->missing_teeth = $data['missing_teeth'];
            $dental_number->filled_teeth = $data['filled_teeth'];
            $dental_number->DMF_teeth = ($data['decayed_teeth']+$data['missing_teeth']+$data['filled_teeth']);
            $dental_number->temp_teeth_present = $data['temp_teeth_present'];
            $dental_number->temp_sound_teeth = $data['temp_sound_teeth'];
            $dental_number->temp_decayed_teeth = $data['temp_decayed_teeth'];
            $dental_number->temp_filled_teeth = $data['temp_filled_teeth'];
            $dental_number->DF_teeth = ($data['temp_decayed_teeth']+$data['temp_filled_teeth']);

            $dental_number->save();

            if(empty($dental_condition)){
              $dental_condition = new DentalOralHealthCondition;
              $dental_condition->dental_id = $data['dental_id'];
              $dental_condition->dental_oral_id = IdGenerator::generateId();
            }
            $dental_condition->t55_status = $data['t55_status'];
            $dental_condition->t54_status = $data['t54_status'];
            $dental_condition->t53_status = $data['t53_status'];
            $dental_condition->t52_status = $data['t52_status'];
            $dental_condition->t51_status = $data['t51_status'];
            $dental_condition->t61_status = $data['t61_status'];
            $dental_condition->t62_status = $data['t62_status'];
            $dental_condition->t63_status = $data['t63_status'];
            $dental_condition->t64_status = $data['t64_status'];
            $dental_condition->t65_status = $data['t65_status'];
            $dental_condition->t18_status = $data['t18_status'];
            $dental_condition->t17_status = $data['t17_status'];
            $dental_condition->t16_status = $data['t16_status'];
            $dental_condition->t15_status = $data['t15_status'];
            $dental_condition->t14_status = $data['t14_status'];
            $dental_condition->t13_status = $data['t13_status'];
            $dental_condition->t12_status = $data['t12_status'];
            $dental_condition->t11_status = $data['t11_status'];
            $dental_condition->t21_status = $data['t21_status'];
            $dental_condition->t22_status = $data['t22_status'];
            $dental_condition->t23_status = $data['t23_status'];
            $dental_condition->t24_status = $data['t24_status'];
            $dental_condition->t25_status = $data['t25_status'];
            $dental_condition->t26_status = $data['t26_status'];
            $dental_condition->t27_status = $data['t27_status'];
            $dental_condition->t28_status = $data['t28_status'];
            $dental_condition->t48_status = $data['t48_status'];
            $dental_condition->t47_status = $data['t47_status'];
            $dental_condition->t46_status = $data['t46_status'];
            $dental_condition->t45_status = $data['t45_status'];
            $dental_condition->t44_status = $data['t44_status'];
            $dental_condition->t43_status = $data['t43_status'];
            $dental_condition->t42_status = $data['t42_status'];
            $dental_condition->t41_status = $data['t41_status'];
            $dental_condition->t31_status = $data['t31_status'];
            $dental_condition->t32_status = $data['t32_status'];
            $dental_condition->t33_status = $data['t33_status'];
            $dental_condition->t34_status = $data['t34_status'];
            $dental_condition->t35_status = $data['t35_status'];
            $dental_condition->t36_status = $data['t36_status'];
            $dental_condition->t37_status = $data['t37_status'];
            $dental_condition->t38_status = $data['t38_status'];
            $dental_condition->t85_status = $data['t85_status'];
            $dental_condition->t84_status = $data['t84_status'];
            $dental_condition->t83_status = $data['t83_status'];
            $dental_condition->t82_status = $data['t82_status'];
            $dental_condition->t81_status = $data['t81_status'];
            $dental_condition->t71_status = $data['t71_status'];
            $dental_condition->t72_status = $data['t72_status'];
            $dental_condition->t73_status = $data['t73_status'];
            $dental_condition->t74_status = $data['t74_status'];
            $dental_condition->t75_status = $data['t75_status'];

            $dental_condition->oral_health_remarks = $data['oral_health_remarks'];

            $dental_condition->save();

            if(empty($dental_service)){
              $dental_service = new DentalServices;
              $dental_service->dental_id = $data['dental_id'];
              $dental_service->dental_service_id = IdGenerator::generateId();
            }
            $dental_service->t55_service = $data['t55_service'];
            $dental_service->t54_service = $data['t54_service'];
            $dental_service->t53_service = $data['t53_service'];
            $dental_service->t52_service = $data['t52_service'];
            $dental_service->t51_service = $data['t51_service'];
            $dental_service->t61_service = $data['t61_service'];
            $dental_service->t62_service = $data['t62_service'];
            $dental_service->t63_service = $data['t63_service'];
            $dental_service->t64_service = $data['t64_service'];
            $dental_service->t65_service = $data['t65_service'];
            $dental_service->t18_service = $data['t18_service'];
            $dental_service->t17_service = $data['t17_service'];
            $dental_service->t16_service = $data['t16_service'];
            $dental_service->t15_service = $data['t15_service'];
            $dental_service->t14_service = $data['t14_service'];
            $dental_service->t13_service = $data['t13_service'];
            $dental_service->t12_service = $data['t12_service'];
            $dental_service->t11_service = $data['t11_service'];
            $dental_service->t21_service = $data['t21_service'];
            $dental_service->t22_service = $data['t22_service'];
            $dental_service->t23_service = $data['t23_service'];
            $dental_service->t24_service = $data['t24_service'];
            $dental_service->t25_service = $data['t25_service'];
            $dental_service->t26_service = $data['t26_service'];
            $dental_service->t27_service = $data['t27_service'];
            $dental_service->t28_service = $data['t28_service'];
            $dental_service->t48_service = $data['t48_service'];
            $dental_service->t47_service = $data['t47_service'];
            $dental_service->t46_service = $data['t46_service'];
            $dental_service->t45_service = $data['t45_service'];
            $dental_service->t44_service = $data['t44_service'];
            $dental_service->t43_service = $data['t43_service'];
            $dental_service->t42_service = $data['t42_service'];
            $dental_service->t41_service = $data['t41_service'];
            $dental_service->t31_service = $data['t31_service'];
            $dental_service->t32_service = $data['t32_service'];
            $dental_service->t33_service = $data['t33_service'];
            $dental_service->t34_service = $data['t34_service'];
            $dental_service->t35_service = $data['t35_service'];
            $dental_service->t36_service = $data['t36_service'];
            $dental_service->t37_service = $data['t37_service'];
            $dental_service->t38_service = $data['t38_service'];
            $dental_service->t85_service = $data['t85_service'];
            $dental_service->t84_service = $data['t84_service'];
            $dental_service->t83_service = $data['t83_service'];
            $dental_service->t82_service = $data['t82_service'];
            $dental_service->t81_service = $data['t81_service'];
            $dental_service->t71_service = $data['t71_service'];
            $dental_service->t72_service = $data['t72_service'];
            $dental_service->t73_service = $data['t73_service'];
            $dental_service->t74_service = $data['t74_service'];
            $dental_service->t75_service = $data['t75_service'];

            $dental_service->service_monitoring_remarks = $data['service_monitoring_remarks'];

            $dental_service->save();

    }
  }
}
