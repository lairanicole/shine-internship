<?php

namespace Plugins\Dental;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class DentalServicesModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dental_service';
    protected static $table_name = 'dental_service';
    protected $primaryKey = 'dental_service_id';

    protected $fillable = [];
    protected $touches = array('dental');
    public function dental()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\Dental\DentalModel','dental_id','dental_id');
    }


}
