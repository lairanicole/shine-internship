<?php
namespace Plugins\Dental;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class DentalModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dental';
    protected static $table_name = 'dental';
    protected $primaryKey = 'dental_id';

    protected $touches = array('Healthcareservices');

    public function Healthcareservices() {
        return $this->belongsTo('ShineOS\Core\Healthcareservices\Entities\Healthcareservices', 'healthcareservice_id', 'healthcareservice_id');
    }

    public function dentalNumber()
    {
        DB::enableQueryLog();
        return $this->hasOne('Plugins\Dental\DentalNumberModel','dental_id','dental_id');
    }

    public function oralHealthCondition()
    {
        DB::enableQueryLog();
        return $this->hasOne('Plugins\Dental\DentalOralHealthConditionModel','dental_id','dental_id');
    }

    public function dentalService()
    {
        DB::enableQueryLog();
        return $this->hasOne('Plugins\Dental\DentalServicesModel','dental_id','dental_id');
    }

    public function scopeWithAndWhereHas($query, $relation, $constraint){
       return $query->whereHas($relation, $constraint)
                    ->with([$relation => $constraint]);
    }

}
