<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMigrationDental extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('dental')!=TRUE) {
            Schema::create('dental', function (Blueprint $table) {
                $table->increments('id');
                $table->string('dental_id', 32);
                $table->string('healthcareservice_id', 32);
                $table->string('date_of_oral_exam', 60);
                $table->text('dental_condition');
                $table->softDeletes();
                $table->timestamps();
                $table->unique('healthcareservice_id');
            });
        }

        if (Schema::hasTable('dental_number')!=TRUE) {
            Schema::create('dental_number', function (Blueprint $table) {
              $table->increments('id')->nullable();
              $table->string('dental_id', 32)->nullable();
              $table->string('dental_number_id', 60)->nullable();
              $table->integer('perm_teeth_present')->nullable();
              $table->integer('perm_sound_teeth')->nullable();
              $table->integer('decayed_teeth')->nullable();
              $table->integer('missing_teeth')->nullable();
              $table->integer('filled_teeth')->nullable();
              $table->integer('DMF_teeth')->nullable();
              $table->integer('temp_teeth_present')->nullable();
              $table->integer('temp_sound_teeth')->nullable();
              $table->integer('temp_decayed_teeth')->nullable();
              $table->integer('temp_filled_teeth')->nullable();
              $table->integer('DF_teeth')->nullable();

              $table->softDeletes();
              $table->timestamps();
            });
          }

          if (Schema::hasTable('dental_oral_condition')!=TRUE) {
              Schema::create('dental_oral_condition', function (Blueprint $table) {
                $table->increments('id')->nullable();
                $table->string('dental_id', 32)->nullable();
                $table->string('dental_oral_id', 60)->nullable();
                $table->string('t55_status',10)->nullable();
                $table->string('t54_status',10)->nullable();
                $table->string('t53_status',10)->nullable();
                $table->string('t52_status',10)->nullable();
                $table->string('t51_status',10)->nullable();
                $table->string('t61_status',10)->nullable();
                $table->string('t62_status',10)->nullable();
                $table->string('t63_status',10)->nullable();
                $table->string('t64_status',10)->nullable();
                $table->string('t65_status',10)->nullable();
                $table->string('t18_status',10)->nullable();
                $table->string('t17_status',10)->nullable();
                $table->string('t16_status',10)->nullable();
                $table->string('t15_status',10)->nullable();
                $table->string('t14_status',10)->nullable();
                $table->string('t13_status',10)->nullable();
                $table->string('t12_status',10)->nullable();
                $table->string('t11_status',10)->nullable();
                $table->string('t21_status',10)->nullable();
                $table->string('t22_status',10)->nullable();
                $table->string('t23_status',10)->nullable();
                $table->string('t24_status',10)->nullable();
                $table->string('t25_status',10)->nullable();
                $table->string('t26_status',10)->nullable();
                $table->string('t27_status',10)->nullable();
                $table->string('t28_status',10)->nullable();
                $table->string('t48_status',10)->nullable();
                $table->string('t47_status',10)->nullable();
                $table->string('t46_status',10)->nullable();
                $table->string('t45_status',10)->nullable();
                $table->string('t44_status',10)->nullable();
                $table->string('t43_status',10)->nullable();
                $table->string('t42_status',10)->nullable();
                $table->string('t41_status',10)->nullable();
                $table->string('t31_status',10)->nullable();
                $table->string('t32_status',10)->nullable();
                $table->string('t33_status',10)->nullable();
                $table->string('t34_status',10)->nullable();
                $table->string('t35_status',10)->nullable();
                $table->string('t36_status',10)->nullable();
                $table->string('t37_status',10)->nullable();
                $table->string('t38_status',10)->nullable();
                $table->string('t85_status',10)->nullable();
                $table->string('t84_status',10)->nullable();
                $table->string('t83_status',10)->nullable();
                $table->string('t82_status',10)->nullable();
                $table->string('t81_status',10)->nullable();
                $table->string('t71_status',10)->nullable();
                $table->string('t72_status',10)->nullable();
                $table->string('t73_status',10)->nullable();
                $table->string('t74_status',10)->nullable();
                $table->string('t75_status',10)->nullable();

                $table->text('oral_health_remarks');

                $table->softDeletes();
                $table->timestamps();
              });
            }

          if (Schema::hasTable('dental_service')!=TRUE) {
              Schema::create('dental_service', function (Blueprint $table) {
                $table->increments('id')->nullable();
                $table->string('dental_id', 32)->nullable();
                $table->string('dental_service_id', 60)->nullable();
                $table->string('t55_service',10)->nullable();
                $table->string('t54_service',10)->nullable();
                $table->string('t53_service',10)->nullable();
                $table->string('t52_service',10)->nullable();
                $table->string('t51_service',10)->nullable();
                $table->string('t61_service',10)->nullable();
                $table->string('t62_service',10)->nullable();
                $table->string('t63_service',10)->nullable();
                $table->string('t64_service',10)->nullable();
                $table->string('t65_service',10)->nullable();
                $table->string('t18_service',10)->nullable();
                $table->string('t17_service',10)->nullable();
                $table->string('t16_service',10)->nullable();
                $table->string('t15_service',10)->nullable();
                $table->string('t14_service',10)->nullable();
                $table->string('t13_service',10)->nullable();
                $table->string('t12_service',10)->nullable();
                $table->string('t11_service',10)->nullable();
                $table->string('t21_service',10)->nullable();
                $table->string('t22_service',10)->nullable();
                $table->string('t23_service',10)->nullable();
                $table->string('t24_service',10)->nullable();
                $table->string('t25_service',10)->nullable();
                $table->string('t26_service',10)->nullable();
                $table->string('t27_service',10)->nullable();
                $table->string('t28_service',10)->nullable();
                $table->string('t48_service',10)->nullable();
                $table->string('t47_service',10)->nullable();
                $table->string('t46_service',10)->nullable();
                $table->string('t45_service',10)->nullable();
                $table->string('t44_service',10)->nullable();
                $table->string('t43_service',10)->nullable();
                $table->string('t42_service',10)->nullable();
                $table->string('t41_service',10)->nullable();
                $table->string('t31_service',10)->nullable();
                $table->string('t32_service',10)->nullable();
                $table->string('t33_service',10)->nullable();
                $table->string('t34_service',10)->nullable();
                $table->string('t35_service',10)->nullable();
                $table->string('t36_service',10)->nullable();
                $table->string('t37_service',10)->nullable();
                $table->string('t38_service',10)->nullable();
                $table->string('t85_service',10)->nullable();
                $table->string('t84_service',10)->nullable();
                $table->string('t83_service',10)->nullable();
                $table->string('t82_service',10)->nullable();
                $table->string('t81_service',10)->nullable();
                $table->string('t71_service',10)->nullable();
                $table->string('t72_service',10)->nullable();
                $table->string('t73_service',10)->nullable();
                $table->string('t74_service',10)->nullable();
                $table->string('t75_service',10)->nullable();

                $table->text('service_monitoring_remarks');

                $table->softDeletes();
                $table->timestamps();
              });
            }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dental');
        Schema::drop('dental_number');
        Schema::drop('dental_oral_condition');
        Schema::drop('dental_service');
    }

}
