
<?php

    $consult_type = $allData['healthcareData']->consultation_type;

    $hservice_id = $data['healthcareservice_id'];
    $fpservice_id = $data['familyplanning_id'];
    $patient_id = getPatientIDByHealthcareserviceID($hservice_id);
    $patient = findPatientByPatientID($patient_id);
    $gender = $patient->gender;

    //let us disable this form if this consult is already disposed
    if(empty($allData['disposition_record']['disposition'])) { $read = ''; }
    else { $read = 'disabled'; }

    $methodArray = ["" => "",
                    "CON" => "Condom",
                    "FSTR/BTL" => "Female Sterilization/Bilateral Tubal",
                    "INJ" => "Depo-medroxy Progestine Acetate",
                    "IUD" => "Intra-Uterine Device",
                    "MSTR/VASECTOMY" => "Male Sterilization/Vasectomy",
                    "NFP-BBT" => "NFP-Basal Body Temperature",
                    "NFP-CM" => "NFP-Cervical Mucus Method",
                    "NFP-LAM" => "Lactational Amenorrhea Method",
                    "NFP-SDM" => "NFP-Standard Days Method",
                    "NFP-STM" => "NFP-Sympothermal Method",
                    "PILLS" => "Pills",
                    "IMPLANT" => "Implant"
                    ];

    $clientTypeArray = ["" => "", "CU" => "Current User", "NA" => "New Acceptor"];

    $clientSubTypeArray = ["" => "", "CC" => "Changing Clinic", "CM" => "Changing Method", "RS" => "Restart"];

    $dropoutReasonArray = [
            "" => "",
            "DR_A" => "Pregnant",
            "DR_B" => "Desire to become pregnant",
            "DR_C" => "Medical complications",
            "DR_D" => "Fear of side effects",
            "DR_E" => "Changed clinic",
            "DR_F" => "Husband disapproves",
            "DR_G" => "Menopause",
            "DR_H" => "Lost or moved out of the area or residence",
            "DR_I" => "Failed to get supply",
            "DR_J" => "IUD expelled",
            "DR_K" => "Lack of supply",
            "DR_L" => "Unknown",
        ];

?>

<div class="icheck">
    <fieldset>
    {!! Form::hidden('familyplanning[fpservice_id]',$fpservice_id) !!}

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#history" data-toggle="tab">History and Examination</a></li>
            <li class=""><a href="#familyplanning" data-toggle="tab">Family Planning</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="history">
                <fieldset>
                    <legend> Physical Examination </legend>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <dl class="col-sm-3">
                                <dt> Conjunctiva </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Conjunctiva][]" id="" value="Pale"
                                            <?php
                                                foreach(explode(",", $data['conjunctiva']) as $value)
                                                {
                                                    if($value=="Pale")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Pale
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Conjunctiva][]" id="" value="Yellowish"
                                            <?php
                                                foreach(explode(",", $data['conjunctiva']) as $value)
                                                {
                                                    if($value=="Yellowish")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Yellowish
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="col-sm-3">
                                <dt> Neck </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Neck][]" id="" value="Enlarged thyroid"
                                            <?php
                                                foreach(explode(",", $data['neck']) as $value)
                                                {
                                                    if($value=="Enlarged thyroid")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Enlarged thyroid
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Neck][]" id="" value="Enlarged lymph nodes"
                                            <?php
                                                foreach(explode(",", $data['neck']) as $value)
                                                {
                                                    if($value=="Enlarged lymph nodes")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Enlarged lymph nodes
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="col-sm-3">
                                <dt> Breast </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Breast][]" id="" value="Mass"
                                            <?php
                                                foreach(explode(",", $data['breast']) as $value)
                                                {
                                                    if($value=="Mass")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Mass
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Breast][]" id="" value="Nipple discharge"
                                            <?php
                                                foreach(explode(",", $data['breast']) as $value)
                                                {
                                                    if($value=="Nipple discharge")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Nipple discharge
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Breast][]" id="" value="Skin - orange peel or dimpling"
                                            <?php
                                                foreach(explode(",", $data['breast']) as $value)
                                                {
                                                    if($value=="Skin - orange peel or dimpling")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Skin - orange peel or dimpling
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Breast][]" id="" value="Enlarged axillary lymph nodes"
                                            <?php
                                                foreach(explode(",", $data['breast']) as $value)
                                                {
                                                    if($value=="Enlarged axillary lymph nodes")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Enlarged axillary lymph nodes
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="col-sm-3">
                                <dt> Thorax </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Thorax][]" id="" value="Abnormal heart sounds/cardiac rate"
                                            <?php
                                                foreach(explode(",", $data['thorax']) as $value)
                                                {
                                                    if($value=="Abnormal heart sounds/cardiac rate")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Abnormal heart sounds/cardiac rate
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Thorax][]" id="" value="Abnormal breath sounds/respiratory rate"
                                            <?php
                                                foreach(explode(",", $data['thorax']) as $value)
                                                {
                                                    if($value=="Abnormal breath sounds/respiratory rate")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Abnormal breath sounds/respiratory rate
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                        <div class="col-sm-12">
                            <dl class="col-sm-3">
                                <dt> Abdomen </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Abdomen][]" id="" value="Enlarged liver"
                                            <?php
                                                foreach(explode(",", $data['abdomen']) as $value)
                                                {
                                                    if($value=="Enlarged liver")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Enlarged liver
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Abdomen][]" id="" value="Mass"
                                            <?php
                                                foreach(explode(",", $data['abdomen']) as $value)
                                                {
                                                    if($value=="Mass")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Mass
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Abdomen][]" id="" value="Tenderness"
                                            <?php
                                                foreach(explode(",", $data['abdomen']) as $value)
                                                {
                                                    if($value=="Tenderness")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Tenderness
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                           <dl class="col-sm-3">
                                <dt> Extremities </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Extremities][]" id="" value="Edema"
                                            <?php
                                                foreach(explode(",", $data['extremities']) as $value)
                                                {
                                                    if($value=="Edema")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Edema
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Extremities][]" id="" value="Varicosities"
                                            <?php
                                                foreach(explode(",", $data['extremities']) as $value)
                                                {
                                                    if($value=="Varicosities")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Varicosities
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend> Pelvic Examination </legend>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <dl class="col-sm-3">
                                <dt> Perineum </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Perineum][]" id="" value="Scars"
                                            <?php
                                                foreach(explode(",", $data['perineum']) as $value)
                                                {
                                                    if($value=="Scars")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Scars
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Perineum][]" id="" value="Warts"
                                            <?php
                                                foreach(explode(",", $data['perineum']) as $value)
                                                {
                                                    if($value=="Warts")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Warts
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Perineum][]" id="" value="Reddish"
                                            <?php
                                                foreach(explode(",", $data['perineum']) as $value)
                                                {
                                                    if($value=="Reddish")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Reddish
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Perineum][]" id="" value="Laceration"
                                            <?php
                                                foreach(explode(",", $data['perineum']) as $value)
                                                {
                                                    if($value=="Laceration")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Laceration
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="col-sm-3">
                                <dt> Vagina </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Vagina][]" id="" value="Congested"
                                            <?php
                                                foreach(explode(",", $data['vagina']) as $value)
                                                {
                                                    if($value=="Congested")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Congested
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Vagina][]" id="" value="Bartholin's Cyst"
                                            <?php
                                                foreach(explode(",", $data['vagina']) as $value)
                                                {
                                                    if($value=="Bartholin's Cyst")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Bartholin's Cyst
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Vagina][]" id="" value="Warts"
                                            <?php
                                                foreach(explode(",", $data['vagina']) as $value)
                                                {
                                                    if($value=="Warts")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Warts
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Vagina][]" id="" value="Skene's Cland Discharge"
                                            <?php
                                                foreach(explode(",", $data['vagina']) as $value)
                                                {
                                                    if($value=="Skene's Cland Discharge")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Skene's Cland Discharge
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Vagina][]" id="" value="Rectocele"
                                            <?php
                                                foreach(explode(",", $data['vagina']) as $value)
                                                {
                                                    if($value=="Rectocele")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Rectocele
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Vagina][]" id="" value="Cystocele"
                                            <?php
                                                foreach(explode(",", $data['vagina']) as $value)
                                                {
                                                    if($value=="Cystocele")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Cystocele
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="col-sm-3">
                                <dt> Cervix </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Cervix][]" id="" value="Congested"
                                            <?php
                                                foreach(explode(",", $data['cervix']) as $value)
                                                {
                                                    if($value=="Congested")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Congested
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Cervix][]" id="" value="Erosion"
                                            <?php
                                                foreach(explode(",", $data['cervix']) as $value)
                                                {
                                                    if($value=="Erosion")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Erosion
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Cervix][]" id="" value="Discharge"
                                            <?php
                                                foreach(explode(",", $data['cervix']) as $value)
                                                {
                                                    if($value=="Discharge")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Discharge
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Cervix][]" id="" value="Polyps/Cysts"
                                            <?php
                                                foreach(explode(",", $data['cervix']) as $value)
                                                {
                                                    if($value=="Polyps/Cysts")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Polyps/Cysts
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Cervix][]" id="" value="Laceration"
                                            <?php
                                                foreach(explode(",", $data['cervix']) as $value)
                                                {
                                                    if($value=="Laceration")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Laceration
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="col-sm-3">
                                <dt> Consistency </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Consistency][]" id="" value="Firm"
                                            <?php
                                                foreach(explode(",", $data['consistency']) as $value)
                                                {
                                                    if($value=="Firm")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Firm
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Consistency][]" id="" value="Soft"
                                            <?php
                                                foreach(explode(",", $data['consistency']) as $value)
                                                {
                                                    if($value=="Soft")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Soft
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                        <div class="col-sm-12">
                            <dl class="col-sm-3">
                                <dt> Adnexa </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Adnexa][]" id="" value="Mass"
                                            <?php
                                                foreach(explode(",", $data['adnexa']) as $value)
                                                {
                                                    if($value=="Mass")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Mass
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Adnexa][]" id="" value="Tenderness"
                                            <?php
                                                foreach(explode(",", $data['adnexa']) as $value)
                                                {
                                                    if($value=="Tenderness")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                           > Tenderness
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="col-sm-3">
                                <dt> Uterus Position </dt>
                                <dd>
                                    <div class="radio">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[UterusPosition]" id=""  value="Mid" <?php if($data['uterus_position']=='Mid'){echo "checked";}?> > Mid
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[UterusPosition]" id="" value="Antiflexed" <?php if($data['uterus_position']=='Antiflexed'){echo "checked";}?> > Antiflexed
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[UterusPosition]" id="" value="Retroflexed" <?php if($data['uterus_position']=='Retroflexed'){echo "checked";}?> > Retroflexed
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="col-sm-3">
                                <dt> Uterus Size </dt>
                                <dd>
                                    <div class="radio">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[UterusSize]" id="" value="Normal" <?php if($data['uterus_size']=='Normal'){echo "checked";}?> > Normal
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[UterusSize]" id="" value="Small" <?php if($data['uterus_size']=='Small'){echo "checked";}?> > Small
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[UterusSize]" id="" value="Large" <?php if($data['uterus_size']=='Large'){echo "checked";}?> > Large
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[UterusSize]" id="" value="Mass" <?php if($data['uterus_size']=='Mass'){echo "checked";}?> > Mass
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="col-sm-3">
                                <dt> Uterine Depth (cm) <small> - for intended IUD users </small></dt>
                                <dd>
                                    <input type="text" name="familyplanning[UterusDepth]" id="" class="form-control" value={{ $data['uterus_depth'] }} >
                                </dd>
                            </dl>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend> Obsterical History </legend>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="col-sm-12 box no-border noboxshadow">
                                <label class="col-sm-2 control-label"> No. of Pregnancies: </label>
                                <dd class="col-sm-1">
                                </dd>
                                <dd class="col-sm-2">
                                    <small> Full Term </small>
                                    <input type="text" class="form-control" name="familyplanning[FullTerm]"  placeholder="" value={{ $data['full_term'] }}>
                                </dd>
                                <dd class="col-sm-2">
                                    <small> Abortions </small>
                                    <input type="text" class="form-control" name="familyplanning[Abortions]"  placeholder="" value={{ $data['abortions'] }}>
                                </dd>
                                <dd class="col-sm-2">
                                    <small> Premature </small>
                                    <input type="text" class="form-control" name="familyplanning[Premature]"  placeholder="" value={{ $data['premature'] }}>
                                </dd>
                                <dd class="col-sm-2">
                                    <small> Living Children </small>
                                    <input type="text" class="form-control" name="familyplanning[LivingChildren]"  placeholder="" value={{ $data['living_children'] }}>
                                </dd>
                            </div>
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label"> Date of last delivery </label>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <!-- <input type="text" class="form-control" name="familyplanning[DateOFLastDelivery"  placeholder="" value={{ $data['date_of_last_delivery'] }}> -->
                                        <input id="datepicker" class="form-control" name="familyplanning[DateOFLastDelivery]" type="text" value={{ $data['date_of_last_delivery'] }} >
                                    </div>

                                </div>
                                <label class="col-sm-2 control-label"> Type of last delivery </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="familyplanning[TypeOfLastDelivery]"  placeholder="" value={{ $data['type_of_last_delivery'] }}>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label"> Past menstrual period </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="familyplanning[PastMenstrualPeriod]"  placeholder="" value={{ $data['past_menstrual_period'] }}>
                                </div>
                                <label class="col-sm-2 control-label"> Last menstrual period </label>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <!-- <input type="text" class="form-control" name="familyplanning[LastMenstrualPeriod"  placeholder="" value={{ $data['last_menstrual_period'] }}> -->
                                        <input id="datepicker" class="form-control" name="familyplanning[LastMenstrualPeriod]" type="text" value={{ $data['last_menstrual_period'] }} >
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label"> Number of days menses </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="familyplanning[NumberOfDaysMenses]"  placeholder="" value={{ $data['number_of_days_menses'] }}>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="col-sm-2 control-label"> Character of menses</label>
                                <div class="col-sm-10">
                                    <div class="radio inline">
                                            <label>
                                              <input type="checkbox" {{ $read }} name="familyplanning[CharacterofMenses]" id="" value="Scanty" <?php if($data['character_of_menses']=="Scanty"){echo "checked";}?> > Scanty
                                            </label>
                                        </div>
                                        <div class="radio inline">
                                            <label>
                                              <input type="checkbox" {{ $read }} name="familyplanning[CharacterofMenses]" id="" value="Painful" <?php if($data['character_of_menses']=="Painful"){echo "checked";}?> > Painful
                                            </label>
                                        </div>
                                        <div class="radio inline">
                                            <label>
                                              <input type="checkbox" {{ $read }} name="familyplanning[CharacterofMenses]" id="" value="Moderate" <?php if($data['character_of_menses']=="Moderate"){echo "checked";}?> > Moderate
                                            </label>
                                        </div>
                                        <div class="radio inline">
                                            <label>
                                              <input type="checkbox" {{ $read }} name="familyplanning[CharacterofMenses]" id="" value="Regular" <?php if($data['character_of_menses']=="Regular"){echo "checked";}?> > Regular
                                            </label>
                                        </div>
                                        <div class="radio inline">
                                            <label>
                                              <input type="checkbox" {{ $read }} name="familyplanning[CharacterofMenses]" id="" value="Heavy" <?php if($data['character_of_menses']=="Heavy"){echo "checked";}?> > Heavy
                                            </label>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend> History of any of the following  </legend>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <dl class="col-sm-12">
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" {{ $read }} name="familyplanning[HistoryOfAnyOfTheFollowing][]" id="" value="Hydatidiform mole (within the last 12 months)"
                                                <?php
                                                    foreach(explode(",", $data['history_of_following']) as $value)
                                                    {
                                                        if($value=="Hydatidiform mole (within the last 12 months)")
                                                        {
                                                            echo "checked";
                                                        }
                                                    }
                                                ?>
                                            > Hydatidiform mole (within the last 12 months)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[HistoryOfAnyOfTheFollowing][]" id="" value="Ectopic Pregnancy"
                                                <?php
                                                    foreach(explode(",", $data['history_of_following']) as $value)
                                                    {
                                                        if($value=="Ectopic Pregnancy")
                                                        {
                                                            echo "checked";
                                                        }
                                                    }
                                                ?>
                                          > Ectopic Pregnancy
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend> STI Risks  </legend>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <dl class="col-sm-12">
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[WithHistoryOfMultiplePartners]" id="" value="Yes" <?php if($data['with_history_of_multiple_partners']=="Yes"){echo "checked";} ?> > With History of multiple partners
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            @if($gender == 'F')
                            <dl class="col-sm-6">
                                <dt> For Women </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[STIRisksWomen][]" id="" value="Unusual discharge from vagina"
                                                <?php
                                                    foreach(explode(",", $data['sti_risks_women']) as $value)
                                                    {
                                                        if($value=="Unusual discharge from vagina")
                                                        {
                                                            echo "checked";
                                                        }
                                                    }
                                                ?>
                                           > Unusual discharge from vagina
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[STIRisksWomen][]" id="" value="Itching or sores in or around vagina"
                                                <?php
                                                    foreach(explode(",", $data['sti_risks_women']) as $value)
                                                    {
                                                        if($value=="Itching or sores in or around vagina")
                                                        {
                                                            echo "checked";
                                                        }
                                                    }
                                                ?>
                                           > Itching or sores in or around vagina
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[STIRisksWomen][]" id="" value="Pain or burning sensation"
                                                <?php
                                                    foreach(explode(",", $data['sti_risks_women']) as $value)
                                                    {
                                                        if($value=="Pain or burning sensation")
                                                        {
                                                            echo "checked";
                                                        }
                                                    }
                                                ?>
                                           > Pain or burning sensation
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[STIRisksWomen][]" id="" value="Treated for STIs in the past"
                                                <?php
                                                    foreach(explode(",", $data['sti_risks_women']) as $value)
                                                    {
                                                        if($value=="Treated for STIs in the past")
                                                        {
                                                            echo "checked";
                                                        }
                                                    }
                                                ?>
                                          > Treated for STIs in the past
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            @endif
                            @if($gender=='M')
                            <dl class="col-sm-6">
                                <dt> For Men </dt>
                                <dd>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[STIRisksMen][]" id="" value="Pain or burning sensation"
                                                <?php
                                                    foreach(explode(",", $data['sti_risks_men']) as $value)
                                                    {
                                                        if($value=="Pain or burning sensation")
                                                        {
                                                            echo "checked";
                                                        }
                                                    }
                                                ?>
                                           > Pain or burning sensation
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[STIRisksMen][]" id="" value="Open sores anywhere in genital area"
                                                <?php
                                                    foreach(explode(",", $data['sti_risks_men']) as $value)
                                                    {
                                                        if($value=="Open sores anywhere in genital area")
                                                        {
                                                            echo "checked";
                                                        }
                                                    }
                                                ?>
                                           > Open sores anywhere in genital area
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[STIRisksMen][]" id="" value="Pus coming from penis"
                                                <?php
                                                    foreach(explode(",", $data['sti_risks_men']) as $value)
                                                    {
                                                        if($value=="Pus coming from penis")
                                                        {
                                                            echo "checked";
                                                        }
                                                    }
                                                ?>
                                           > Pus coming from penis
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[STIRisksMen][]" id="" value="Swollen testicles or penis"
                                                <?php
                                                    foreach(explode(",", $data['sti_risks_men']) as $value)
                                                    {
                                                        if($value=="Swollen testicles or penis")
                                                        {
                                                            echo "checked";
                                                        }
                                                    }
                                                ?>
                                           > Swollen testicles or penis
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[STIRisksMen][]" id="" value="Treated for STIs in the past"
                                                <?php
                                                    foreach(explode(",", $data['sti_risks_men']) as $value)
                                                    {
                                                        if($value=="Treated for STIs in the past")
                                                        {
                                                            echo "checked";
                                                        }
                                                    }
                                                ?>
                                           > Treated for STIs in the past
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                            @endif
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend> Risks for violence against women (VAW)  </legend>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox" {{ $read }} name="familyplanning[ViolenceAgainstWomen][]" id="" value="History of domestic violence or VAW"
                                        <?php
                                            foreach(explode(",", $data['violence_against_women']) as $value)
                                            {
                                                if($value=="History of domestic violence or VAW")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        ?>
                                   > History of domestic violence or VAW
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox" {{ $read }} name="familyplanning[ViolenceAgainstWomen][]" id="" value="Unpleasant relationship with partner"
                                        <?php
                                            foreach(explode(",", $data['violence_against_women']) as $value)
                                            {
                                                if($value=="Unpleasant relationship with partner")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        ?>
                                   > Unpleasant relationship with partner
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox" {{ $read }} name="familyplanning[ViolenceAgainstWomen][]" id="" value="Partner does not approve of the visit to Family Planning"
                                        <?php
                                            foreach(explode(",", $data['violence_against_women']) as $value)
                                            {
                                                if($value=="Partner does not approve of the visit to Family Planning")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        ?>
                                   > Partner does not approve of the visit to Family Planning
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox" {{ $read }} name="familyplanning[ViolenceAgainstWomen][]" id="" value="Partner disagrees to use Family Planning"
                                        <?php
                                            foreach(explode(",", $data['violence_against_women']) as $value)
                                            {
                                                if($value=="Partner disagrees to use Family Planning")
                                                {
                                                    echo "checked";
                                                }
                                            }
                                        ?>
                                   > Partner disagrees to use Family Planning
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <dl class="col-sm-12">
                                <dt> Referred to </dt>
                                <dd>
                                    <div class="checkbox inline">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Referredto][]" id="" value="DSWD"
                                            <?php
                                                foreach(explode(",", $data['referred_to']) as $value)
                                                {
                                                    if($value=="DSWD")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > DSWD
                                        </label>
                                    </div>
                                    <div class="checkbox inline">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Referredto][]" id="" value="WCPU"
                                            <?php
                                                foreach(explode(",", $data['referred_to']) as $value)
                                                {
                                                    if($value=="WCPU")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > WCPU
                                        </label>
                                    </div>
                                    <div class="checkbox inline">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Referredto][]" id="" value="NGOs"
                                            <?php
                                                foreach(explode(",", $data['referred_to']) as $value)
                                                {
                                                    if($value=="NGOs")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > NGOs
                                        </label>
                                    </div>
                                    <div class="checkbox inline">
                                        <label>
                                          <input type="checkbox" {{ $read }} name="familyplanning[Referredto][]" id="" value="Others"
                                            <?php
                                                foreach(explode(",", $data['referred_to']) as $value)
                                                {
                                                    if($value=="Others")
                                                    {
                                                        echo "checked";
                                                    }
                                                }
                                            ?>
                                          > Others
                                        </label>
                                    </div>
                                    <div class="checkbox inline">
                                        <label>
                                          <input type="text" class="form-control" name="familyplanning[ReferredtoOthers]"  placeholder="" value={{ $data['referred_to_others'] }}>
                                        </label>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </fieldset>
                <!-- // -->

            </div><!-- /.tab-pane -->

            <div class="tab-pane " id="familyplanning">
                <fieldset>
                    <legend>Family Planning Chart</legend>
                    <div class="form-group">
                    <label class="col-sm-3 control-label">Family Planning Started on</label>
                    @if($consult_type == 'FOLLO')
                    <label class="col-sm-5 control-label"> {{ $allData['plugindataall'][0]->planning_start }} </label>
                    @else
                    <label class="col-sm-5 control-label"> {{ $data['planning_start'] }} </label>
                    @endif
                    </div>

                    <div class ='col-md-12'>
                        <table class="table table-compact datatable">
                            <thead>
                                <tr>
                                    <th >Client Type</th>
                                    <th> Client Subtype </th>
                                    <th >Actual Visit Date</th>
                                    <th >Next Follow-up Date</th>
                                    <th >Previous Method</th>
                                    <th >Dropout Date</th>
                                    <th >Reason</th>
                                    <th >Current Method</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($allData['plugindataall'] as $key => $value)
                                    <tr>
                                        <td>{{ $clientTypeArray[$value->client_type] }}</td>
                                        <td>{{ $clientSubTypeArray[$value->client_sub_type] }} </td>
                                        <td>{{ $value->actual_visit_date }}</td>
                                        <td>{{ $value->followup_visit_date }}</td>
                                        <td>{{ $methodArray[$value->previous_method] }}</td>
                                        <td>{{ $value->dropout_date}} </td>
                                        <td>{{ $dropoutReasonArray[$value->dropout_reason]}} </td>
                                        <td>{{ $methodArray[$value->current_method] }} </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </fieldset>

                <!-- Family Planning -->
                <fieldset>
                    <legend>Family Planning</legend>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="col-sm-2 control-label"> Planning Start </label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                    </div>
                                    <input class="form-control datepicker" name="familyplanning[PlanningStart]" type="text" value="<?php if($consult_type == 'FOLLO') { echo $allData['plugindataall'][0]->planning_start; } else { echo $data['planning_start']; } ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-2 control-label"> No. of Living Children </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="familyplanning[NoOfLivingChildren]"  placeholder="" value={{ $data['no_of_living_children'] }}>
                            </div>
                            <label class="col-sm-2 control-label"> Plan more children? </label>
                            <div class="col-sm-4">
                                <div class="radio inline">
                                    <label>
                                      <input type="radio" {{ $read }} name="familyplanning[PlanMoreChildren]" id="" value="yes" <?php if($data['plan_more_children']=="yes"){echo "checked";} ?> > Yes
                                    </label>
                                </div>
                                <div class="radio inline">
                                    <label>
                                      <input type="radio" {{ $read }} name="familyplanning[PlanMoreChildren]" id="" value="no" <?php if($data['plan_more_children']=="no"){echo "checked";} ?> > No
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-2 control-label"> Reason for practicing FP </label>
                            <div class="col-sm-10">
                                <textarea class="form-control noresize" placeholder="Indicate reason for practicing family planning" name="familyplanning[ReasonForPracticingFP]">{{ $data['reason_for_practicing_fp'] }}</textarea>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-2 control-label"> Client Type </label>
                            <div class="col-sm-4">
                                {!!
                                    Form::select('ClientType',
                                    $clientTypeArray,
                                    $data['client_type'],
                                    array('name' => 'familyplanning[ClientType]', 'class' => 'form-control'))
                                !!}
                            </div>
                            <!-- if client type = CU -->
                            <label class="col-sm-2 control-label"> Client Sub Type </label>
                            <div class="col-sm-4">
                                {!!
                                    Form::select('ClientSubType',
                                    $clientSubTypeArray,
                                    $data['client_sub_type'],
                                    array('name' => 'familyplanning[ClientSubType]', 'class' => 'form-control'))
                                !!}
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-2 control-label"> Previous Method </label>
                            <div class="col-sm-4">
                                {!!
                                    Form::select('PreviousMethod',
                                    $methodArray,
                                    $data['previous_method'],
                                    array('name' => 'familyplanning[PreviousMethod]', 'class' => 'form-control'))
                                !!}
                            </div>
                            <label class="col-sm-2 control-label"> Current Method </label>
                            <div class="col-sm-4">
                                {!!
                                    Form::select('CurrentMethod',
                                    $methodArray,
                                    $data['current_method'],
                                    array('name' => 'familyplanning[CurrentMethod]', 'class' => 'form-control'))
                                !!}
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-2 control-label"> Dropout Date </label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                    </div>
                                    <input class="form-control datepicker" name="familyplanning[DropoutDate]" type="text" value="{{ $data['dropout_date'] }}" >
                                </div>
                            </div>
                            <label class="col-sm-2 control-label"> Dropout Reason </label>
                            <div class="col-sm-4">
                                {!!
                                    Form::select('DropoutReason',
                                    $dropoutReasonArray,
                                    $data['dropout_reason'],
                                    array('name' => 'familyplanning[DropoutReason]', 'class' => 'form-control'))
                                !!}
                            </div>
                        </div>
                        <div class="col-sm-12">

                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-2 control-label"> Actual Visit Date </label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input class="form-control datepicker" name="familyplanning[ActualVisitDate]" type="text" value={{ $data['actual_visit_date'] }} >
                                </div>
                            </div>
                            <label class="col-sm-2 control-label"> Next Follow-up Visit </label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input id="datepicker" class="form-control datepicker_future" name="familyplanning[FollowupVisitDate]" type="text" value={{ $data['followup_visit_date'] }} >
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-2 control-label"> Remarks </label>
                            <div class="col-sm-10">
                                <textarea class="form-control noresize" placeholder="" name="familyplanning[Remarks]" >{{ $data['remarks'] }}</textarea>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div><!-- /.tab-pane -->
        </div>
    </div>

    </fieldset>
</div>
