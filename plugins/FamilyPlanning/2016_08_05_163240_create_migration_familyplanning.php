<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMigrationFamilyPlanning extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('familyplanning_service')!=TRUE) { 
            Schema::create('familyplanning_service', function (Blueprint $table) {
                $table->increments('id');
                $table->string('familyplanning_id', 32);
                $table->string('healthcareservice_id', 32);
                $table->text('conjunctiva');
                $table->text('neck');
                $table->text('breast');
                $table->text('thorax');
                $table->text('abdomen');
                $table->text('extremities');
                $table->text('perineum');
                $table->text('vagina');
                $table->text('cervix');
                $table->text('consistency');
                $table->text('adnexa');
                $table->string('uterus_position', 60);
                $table->string('uterus_size', 60);
                $table->string('uterus_depth', 60);
                $table->string('full_term', 60);
                $table->string('abortions', 60);
                $table->string('premature', 60);
                $table->string('living_children', 60);
                $table->string('date_of_last_delivery', 60);
                $table->string('type_of_last_delivery', 60);
                $table->string('past_menstrual_period', 60);
                $table->string('last_menstrual_period', 60);
                $table->string('number_of_days_menses', 60);
                $table->string('character_of_menses', 60);
                $table->text('history_of_following');
                $table->string('with_history_of_multiple_partners', 15);
                $table->text('sti_risks_women');
                $table->text('sti_risks_men');
                $table->text('violence_against_women');
                $table->text('referred_to');
                $table->text('referred_to_others');
                $table->string('planning_start', 32);
                $table->string('no_of_living_children', 60);
                $table->string('plan_more_children', 15);
                $table->text('reason_for_practicing_fp');
                $table->string('client_type', 60);
                $table->string('client_sub_type', 60);
                $table->string('previous_method', 60);
                $table->string('dropout_date', 60);
                $table->string('dropout_reason', 60);
                $table->string('current_method', 60);
                $table->string('actual_visit_date', 60);
                $table->string('followup_visit_date', 60);
                $table->text('remarks');

                $table->softDeletes();
                $table->timestamps();
                $table->unique('healthcareservice_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('familyplanning_service');
    }

}
