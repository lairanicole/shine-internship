<?php

use Plugins\FamilyPlanning\FamilyPlanningModel as FamilyPlanning;
use Modules\Helathcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

class FamilyPlanningController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        //no index
    }

    public function save($data)
    {
        if($data) {
            $familyplanning_id = $data['fpservice_id'];

            $familyplanning_service = FamilyPlanning::where('familyplanning_id','=',$familyplanning_id)->first();

            // $familyplanning_service->conjunctiva = $data['Conjunctiva'];
            // $familyplanning_service->neck = $data['Neck'];
            // $familyplanning_service->breast = $data['Breast'];
            // $familyplanning_service->thorax = $data['Thorax'];
            // $familyplanning_service->abdomen = $data['Abdomen'];
            // $familyplanning_service->extremities = $data['Extremities'];
            // $familyplanning_service->perineum = $data['Perineum'];
            // $familyplanning_service->vagina = $data['Vagina'];
            // $familyplanning_service->cervix = $data['Cervix'];
            // $familyplanning_service->consistency = $data['Consistency'];
            // $familyplanning_service->adnexa = $data['Adnexa'];
            // $familyplanning_service->uterus_position = $data['UterusPosition'];
            // $familyplanning_service->uterus_size = $data['UterusSize'];
            $familyplanning_service->uterus_depth = $data['UterusDepth'];
            $familyplanning_service->full_term = $data['FullTerm'];
            $familyplanning_service->abortions = $data['Abortions'];
            $familyplanning_service->premature = $data['Premature'];
            $familyplanning_service->living_children = $data['LivingChildren'];
            $familyplanning_service->date_of_last_delivery = $data['DateOFLastDelivery'];
            $familyplanning_service->type_of_last_delivery = $data['TypeOfLastDelivery'];
            $familyplanning_service->past_menstrual_period = $data['PastMenstrualPeriod'];
            $familyplanning_service->last_menstrual_period = $data['LastMenstrualPeriod'];
            $familyplanning_service->number_of_days_menses = $data['NumberOfDaysMenses'];
            // $familyplanning_service->character_of_menses = $data['CharacterofMenses'];
            // $familyplanning_service->with_history_of_multiple_partners = $data['WithHistoryOfMultiplePartners'];
            $familyplanning_service->referred_to_others = $data['ReferredtoOthers'];
            $familyplanning_service->planning_start = $data['PlanningStart'];
            $familyplanning_service->no_of_living_children = $data['NoOfLivingChildren'];
            // $familyplanning_service->plan_more_children = $data['PlanMoreChildren'];
            $familyplanning_service->reason_for_practicing_fp = $data['ReasonForPracticingFP'];
            $familyplanning_service->client_type = $data['ClientType'];
            $familyplanning_service->client_sub_type = $data['ClientSubType'];
            $familyplanning_service->previous_method = $data['PreviousMethod'];
            $familyplanning_service->dropout_date = $data['DropoutDate'] ? $data['DropoutDate'] : NULL;
            $familyplanning_service->dropout_reason = $data['DropoutReason'];
            $familyplanning_service->current_method = $data['CurrentMethod'];
            $familyplanning_service->actual_visit_date = $data['ActualVisitDate'] ? $data['ActualVisitDate'] : NULL;
            $familyplanning_service->followup_visit_date = $data['FollowupVisitDate'] ? $data['FollowupVisitDate'] : NULL;
            $familyplanning_service->remarks = $data['Remarks'];

            if(isset($data['UterusPosition']))
            {
                $familyplanning_service->uterus_position = $data['UterusPosition'];
            }

            if(isset($data['UterusSize']))
            {
                $familyplanning_service->uterus_size = $data['UterusSize'];
            }

            if(isset($data['CharacterofMenses']))
            {
                $familyplanning_service->character_of_menses = $data['CharacterofMenses'];
            }

            if(isset($data['WithHistoryOfMultiplePartners']))
            {
                $familyplanning_service->with_history_of_multiple_partners = $data['WithHistoryOfMultiplePartners'];
            }

            if(isset($data['PlanMoreChildren']))
            {
                $familyplanning_service->plan_more_children = $data['PlanMoreChildren'];
            }

            $conjunctivaString = "";
            if(isset($data['Conjunctiva']))
            {
                $conjunctiva = $data['Conjunctiva'];

                $conjunctivaString = implode(',', $conjunctiva);
            }
            $familyplanning_service->conjunctiva = $conjunctivaString;

            $neckString = "";
            if(isset($data['Neck']))
            {
                $neck = $data['Neck'];

                $neckString = implode(',', $neck);
            }
            $familyplanning_service->neck = $neckString;

            $breastString = "";
            if(isset($data['Breast']))
            {
                $breast = $data['Breast'];

                $breastString = implode(',', $breast);
            }
            $familyplanning_service->breast = $breastString;

            $thoraxString = "";
            if(isset($data['Thorax']))
            {
                $thorax = $data['Thorax'];

                $thoraxString = implode(',', $thorax);
            }
            $familyplanning_service->thorax = $thoraxString;

            $abdomenString = "";
            if(isset($data['Abdomen']))
            {
                $abdomen = $data['Abdomen'];

                $abdomenString = implode(',', $abdomen);
            }
            $familyplanning_service->abdomen = $abdomenString;

            $extremitiesString = "";
            if(isset($data['Extremities']))
            {
                $extremities = $data['Extremities'];

                $extremitiesString = implode(',', $extremities);
            }
            $familyplanning_service->extremities = $extremitiesString;

            $perineumString = "";
            if(isset($data['Perineum']))
            {
                $perineum = $data['Perineum'];

                $perineumString = implode(',', $perineum);
            }
            $familyplanning_service->perineum = $perineumString;

            $vaginaString = "";
            if(isset($data['Vagina']))
            {
                $vagina = $data['Vagina'];

                $vaginaString = implode(',', $vagina);
            }
            $familyplanning_service->vagina = $vaginaString;

            $cervixString = "";
            if(isset($data['Cervix']))
            {
                $cervix = $data['Cervix'];

                $cervixString = implode(',', $cervix);
            }
            $familyplanning_service->cervix = $cervixString;

            $consistencyString = "";
            if(isset($data['Consistency']))
            {
                $consistency = $data['Consistency'];

                $consistencyString = implode(',', $consistency);
            }
            $familyplanning_service->consistency = $consistencyString;

            $adnexaString = "";
            if(isset($data['Adnexa']))
            {
                $adnexa = $data['Adnexa'];

                $adnexaString = implode(',', $adnexa);
            }
            $familyplanning_service->adnexa = $adnexaString;


            //HISTORY OF FOLLOWING
            $historyFollowing = "";
            if(isset($data['HistoryOfAnyOfTheFollowing']))
            {
                $historyOFFollowing = $data['HistoryOfAnyOfTheFollowing'];

                $historyFollowing = implode(',', $historyOFFollowing);
            }
            $familyplanning_service->history_of_following = $historyFollowing;

            //STI RISKS WOMEN
            $stiRisksWomen = "";
            if(isset($data['STIRisksWomen']))
            {
                $sti_women = ($data['STIRisksWomen'] != null) ? $data['STIRisksWomen'] : null;

                $stiRisksWomen = implode(',', $sti_women);
            }
            $familyplanning_service->sti_risks_women = $stiRisksWomen;


            //STI RISKS MEN
            $stiRisksMen = "";
            if(isset($data['STIRisksMen']))
            {
                $sti_men = $data['STIRisksMen'];

                $stiRisksMen = implode(',', $sti_men);
            }
            $familyplanning_service->sti_risks_men = $stiRisksMen;

            //VIOLENCE AGAINTS WOMEN
            $violenceWomen = "";
            if(isset($data['ViolenceAgainstWomen']))
            {
                $violenceAgainstWomen = $data['ViolenceAgainstWomen'];

                $violenceWomen = implode(',', $violenceAgainstWomen);
            }
            $familyplanning_service->violence_against_women = $violenceWomen;

            //REFERRED TO
            $referredTo = "";
            if(isset($data['Referredto']))
            {
                $referred = $data['Referredto'];

                $referredTo = implode(',', $referred);
            }
            $familyplanning_service->referred_to = $referredTo;

            // dd($familyplanning_service);
            $familyplanning_service->save();

            //$patient_id = getPatientIDByHealthcareserviceID($hservice_id);
            //header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
            //exit;
        }
    }
}
