<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration; 

class UpdateMigrationFamilyplanning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        if (Schema::hasTable('familyplanning_service')==TRUE) { 
            Schema::table('familyplanning_service', function (Blueprint $table) {
                $table->text('conjunctiva')->nullable()->change();
                $table->text('neck')->nullable()->change();
                $table->text('breast')->nullable()->change();
                $table->text('thorax')->nullable()->change();
                $table->text('abdomen')->nullable()->change();
                $table->text('extremities')->nullable()->change();
                $table->text('perineum')->nullable()->change();
                $table->text('vagina')->nullable()->change();
                $table->text('cervix')->nullable()->change();
                $table->text('consistency')->nullable()->change();
                $table->text('adnexa')->nullable()->change();
                $table->string('uterus_position', 60)->nullable()->change();
                $table->string('uterus_size', 60)->nullable()->change();
                $table->string('uterus_depth', 60)->nullable()->change();
                $table->string('full_term', 60)->nullable()->change();
                $table->string('abortions', 60)->nullable()->change();
                $table->string('premature', 60)->nullable()->change();
                $table->string('living_children', 60)->nullable()->change();
                $table->string('date_of_last_delivery', 60)->nullable()->change();
                $table->string('type_of_last_delivery', 60)->nullable()->change();
                $table->string('past_menstrual_period', 60)->nullable()->change();
                $table->string('last_menstrual_period', 60)->nullable()->change();
                $table->string('number_of_days_menses', 60)->nullable()->change();
                $table->string('character_of_menses', 60)->nullable()->change();
                $table->text('history_of_following')->nullable()->change();
                $table->string('with_history_of_multiple_partners', 15)->nullable()->change();
                $table->text('sti_risks_women')->nullable()->change();
                $table->text('sti_risks_men')->nullable()->change();
                $table->text('violence_against_women')->nullable()->change();
                $table->text('referred_to')->nullable()->change();
                $table->text('referred_to_others')->nullable()->change();
                $table->string('planning_start', 32)->nullable()->change();
                $table->string('no_of_living_children', 60)->nullable()->change();
                $table->string('plan_more_children', 15)->nullable()->change();
                $table->text('reason_for_practicing_fp')->nullable()->change();
                $table->string('client_type', 60)->nullable()->change();
                $table->string('client_sub_type', 60)->nullable()->change();
                $table->string('previous_method', 60)->nullable()->change();
                $table->string('dropout_date', 60)->nullable()->change();
                $table->string('dropout_reason', 60)->nullable()->change();
                $table->string('current_method', 60)->nullable()->change();
                $table->string('actual_visit_date', 60)->nullable()->change();
                $table->string('followup_visit_date', 60)->nullable()->change();
                $table->text('remarks')->nullable()->change();

                $conn = Schema::getConnection();
                $dbSchemaManager = $conn->getDoctrineSchemaManager();
                $doctrineTable = $dbSchemaManager->listTableDetails('familyplanning_service');
         
                if (! $doctrineTable->hasIndex('familyplanning_service_familyplanning_id_unique'))
                {
                    $table->unique('familyplanning_id');
                }
            }); 

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
