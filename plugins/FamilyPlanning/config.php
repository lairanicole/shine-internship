<?php

$plugin_id = 'FamilyPlanning';                       //plugin ID
$plugin_module = 'healthcareservices';          //module owner
$plugin_location = 'dropdown';                  //UI location where plugin will be accessible
$plugin_primaryKey = 'familyplanning_id';        //primary_key used to find data
$plugin_table = 'familyplanning_service';            //plugintable default; table_name custom table
$plugin_tabs_child = array('addservice', 'complaints', 'familyplanning_plugin', 'disposition');
$plugin_type = "program";
$plugin_gender = "all";
$plugin_age = "12-60";
//plugin maximum role to access this plugin (MANDATORY VALUE)
$plugin_role = 5;

$plugin_relationship = array();
$plugin_folder = 'FamilyPlanning'; //module owner
$plugin_title = 'Family Planning';            //plugin title
$plugin_description = 'Family Planning';
$plugin_version = '1.0';
$plugin_developer = 'ShineLabs';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2016";

$plugin_tabs = [
    'addservice' => 'Basic Information',
    'disposition' => 'Disposition',
    // 'medicalorders' => 'Medical Orders',
    // 'vitals' => 'Vitals & Physical',
    // 'examinations' => 'Examination',
    'impanddiag' => 'Impressions and Diagnosis',
    'complaints' => 'Complaints',
    'familyplanning_plugin' => 'Family Planning'
];

$plugin_tabs_models = [
    // 'impanddiag' => 'Diagnosis',
    // 'examinations' => 'Examination',
    'disposition' => 'Disposition',
    // 'medicalorders' => 'MedicalOrder',
    // 'vitals' => 'VitalsPhysical',
    'familyplanning_plugin' => 'FamilyPlanningModel'
];
