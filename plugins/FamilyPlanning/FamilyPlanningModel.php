<?php
namespace Plugins\FamilyPlanning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class FamilyPlanningModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'familyplanning_service';
    protected static $table_name = 'familyplanning_service';
    protected $primaryKey = 'familyplanning_id';

    protected $touches = array('Healthcareservices');

    public function Healthcareservices() {
        return $this->belongsTo('ShineOS\Core\Healthcareservices\Entities\Healthcareservices', 'healthcareservice_id', 'healthcareservice_id');
    }

    public function facilityPatientUser()
    {
        DB::enableQueryLog();
        return $this->belongsToMany('ShineOS\Core\Facilities\Entities\FacilityPatientUser','healthcare_services','facilitypatientuser_id', 'healthcareservice_id')->withPivot('created_at');
    }

}
