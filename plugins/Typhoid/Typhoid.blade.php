
<?php
    $hservice_id = $data['healthcareservice_id'];
    $tservice_id = $data['typhoid_id'];
?>

<?php
if($data['is_submitted'] == false) { $read = ''; }
else { $read = 'disabled'; }
?>

<div class="tab-content">

<div id="typhoid" class="tab-pane step active">
{!! Form::model($data, array('url' => 'plugin/call/Typhoid/Typhoid/save/'.$hservice_id,'class'=>'form-horizontal')) !!}
{!! Form::hidden('tservice_id',$tservice_id) !!}
{!! Form::hidden('hservice_id',$hservice_id) !!}
  <fieldset id="clinical_data">
    <legend>Clinical Data</legend>
    <div class="form-group col-md-6">
      <label class="col-md-4 control-label"><small>Was the patient diagnosed with typhoid fever?</small></label>
      <div class="col-sm-8">
        <div class="btn-group toggler" data-toggle="buttons">
          <label class="btn btn-default <?php if($data['diagnosed_typhoid'] == 'Y') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Diagnosed_Typhoid" value="Y" <?php if($data['diagnosed_typhoid'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
          </label>
          <label class="btn btn-default <?php if($data['diagnosed_typhoid'] == 'N') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Diagnosed_Typhoid" value="N" <?php if($data['diagnosed_typhoid'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
          </label>
          <label class="btn btn-default <?php if($data['diagnosed_typhoid'] == 'U') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Diagnosed_Typhoid" value="U" <?php if($data['diagnosed_typhoid'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
          </label>
        </div>
      </div>
    </div>
    <div class="form-group col-md-6">
      <label class="col-sm-4 control-label">Date of Onset of Symptoms</label>
      <div class="col-sm-8">
          {!! Form::text('Symptoms_Onset_Date', (empty($data['date_examined']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['symptoms_onset_date']))), array('class' => 'form-control required datepicker', $read)) !!}
      </div>
    </div>
    <div class="form-group col-md-6">
      <label class="col-md-4 control-label">Was the patient hospitalized?</label>
      <div class="col-sm-8">
        <div class="btn-group toggler" data-toggle="buttons">
          <label class="btn btn-default <?php if($data['patient_hospitalized'] == 'Y') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Patient_Hospitalized" value="Y" <?php if($data['patient_hospitalized'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
          </label>
          <label class="btn btn-default <?php if($data['patient_hospitalized'] == 'N') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Patient_Hospitalized" value="N" <?php if($data['patient_hospitalized'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
          </label>
          <label class="btn btn-default <?php if($data['patient_hospitalized'] == 'U') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Patient_Hospitalized" value="U" <?php if($data['patient_hospitalized'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
          </label>
        </div>
      </div>
    </div>
    <div class="form-group col-md-6">
      <label class="col-sm-4 control-label">How many days was the patient hospitalized?</label>
      <div class="col-md-8">
        <input type="number" class="form-control" name="Days_Patient_Hospitalized" placeholder="Duration (Days)" value="<?php echo $data['days_patient_hospitalized'] ?>" {{$read}}>
      </div>
    </div>
  </fieldset>

  <fieldset id="laboratory_data">
    <legend>Laboratory Data </legend>
    <div class="row">
      <div class="form-group col-md-6">
        <label class="col-sm-4 control-label">Date Salmonella First Isolated</label>
        <div class="col-sm-8">
            <?/*date('m/d/Y', (empty($data['date_examined']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['date_salmonella_first_isolated']))), array('class' => 'form-control required datepicker', $read))*/?>

            {!! Form::text('Date_Salmonella_First_Isolated', $data['date_salmonella_first_isolated'], array('class' => 'form-control required datepicker', $read )) !!}
        </div>
      </div>
      <div class="col-md-6">
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-12 icheck">
        <label class="col-sm-2 control-label">Sites of Isolation</label>
        <div class="checkbox col-sm-2">
          <label>
            <?php $sites_of_isolation = explode(',',$data['sites_of_isolation'],3); ?>
            <input type="checkbox" name="Sites_Of_Isolation_Blood" value="BLOOD" <?php if($sites_of_isolation[0] != null) { echo "checked = 'checked'"; } ?> {{$read}} /> Blood
          </label>
        </div>
        <div class="checkbox col-sm-2">
          <label>
            <input type="checkbox" name="Sites_Of_Isolation_Stool" value="STOOL" <?php if($sites_of_isolation[1] != null) { echo "checked = 'checked'"; } ?> {{$read}} /> Stool
          </label>
        </div>
        <div class="checkbox col-sm-2">
          <label>
            <input type="checkbox" name="Sites_Of_Isolation_Gall" value="GALL" <?php if($sites_of_isolation[2] != null) { echo "checked = 'checked'"; } ?> {{$read}} /> Gall Bladder
          </label>
        </div>

      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <label class="col-sm-2 control-label">Other Sites of Isolation</label>
        <div class="col-sm-10">
          {!! Form::textarea('Soi_Other_Comments', $data['soi_other_comments'], ['class' => 'form-control noresize', 'placeholder' => 'Others. e.g. Heart,Liver', 'cols'=>'5', 'rows'=>'2', $read]) !!}
        </div>
      </div>
    </div>
      <!-- CHECKBOX TO BE FIXED -->
    <div class="row">
      <div class="form-group col-md-12 icheck">
        <label class="col-sm-2 control-label">Serotype</label>
        <div class="checkbox col-sm-2">
          <label>
            <?php $serotype_avail = explode(',',$data['serotype'],4); ?>
            <input type="checkbox" name="Serotype_A" value="TYPHI" <?php if($serotype_avail[0] != null) { echo "checked = 'checked'"; } ?> {{$read}} /> Typhi
          </label>
        </div>
        <div class="checkbox col-sm-2">
          <label>
            <input type="checkbox" name="Serotype_B" value="PARATYPHIA" <?php if($serotype_avail[1] != null) { echo "checked = 'checked'"; } ?> {{$read}} /> Paratyphi A
          </label>
        </div>

        <div class="checkbox col-sm-2">
          <label>
            <input type="checkbox" name="Serotype_C" value="PARATYPHIB" <?php if($serotype_avail[2] != null) { echo "checked = 'checked'"; } ?> {{$read}} /> Paratyphi B
          </label>
        </div>

        <div class="checkbox col-sm-2">
          <label>
            <input type="checkbox" name="Serotype_D" value="PARATYPHIC" <?php if($serotype_avail[3] != null) { echo "checked = 'checked'"; } ?> {{$read}} /> Paratyphi C
          </label>
        </div>
      </div>
    </div>
  </fieldset>

  <fieldset id="epidemiologic_data">
    <legend>Epidemiologic Data</legend>
    <div class="form-group col-md-6">
      <label class="col-sm-4 control-label">Did this case occur as part of an outbreak?</label>
      <div class="col-sm-8">
        <div class="btn-group toggler" data-toggle="buttons">
          <label class="btn btn-default <?php if($data['part_of_an_outbreak'] == 'Y') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Part_Of_An_Outbreak" value="Y" <?php if($data['part_of_an_outbreak'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
          </label>
          <label class="btn btn-default <?php if($data['part_of_an_outbreak'] == 'N') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Part_Of_An_Outbreak" value="N" <?php if($data['part_of_an_outbreak'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
          </label>
          <label class="btn btn-default <?php if($data['part_of_an_outbreak'] == 'U') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Part_Of_An_Outbreak" value="U" <?php if($data['part_of_an_outbreak'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
          </label>
        </div>
      </div>
    </div>
    <div class="form-group col-md-6">
      <label class="col-sm-4 control-label"><small>Did the patient have typhoid vaccination within five years before onset?</small></label>
      <div class="col-sm-8">
        <div class="btn-group toggler" data-toggle="buttons">
          <label class="btn btn-default <?php if($data['patient_typhoid_vaccine_five_years'] == 'Y') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Patient_Typhoid_Vaccine_Five_Years" value="Y" <?php if($data['patient_typhoid_vaccine_five_years'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
          </label>
          <label class="btn btn-default <?php if($data['patient_typhoid_vaccine_five_years'] == 'N') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Patient_Typhoid_Vaccine_Five_Years" value="N" <?php if($data['patient_typhoid_vaccine_five_years'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
          </label>
          <label class="btn btn-default <?php if($data['patient_typhoid_vaccine_five_years'] == 'U') {echo 'active';} ?>" {{$read}}>
            <i class="fa fa-check"></i> <input type="radio" name="Patient_Typhoid_Vaccine_Five_Years" value="U" <?php if($data['patient_typhoid_vaccine_five_years'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
          </label>
        </div>
      </div>
    </div>
    <div class="form-group col-md-6">
      <label class="col-md-4 control-label">Oral Ty21a or Vivotif (Berna) four pill series</label>
      <div class="col-sm-5">
          <div class="btn-group toggler" data-toggle="buttons">
            <label class="btn btn-default <?php if($data['oral_ty21a_or_vivotif'] == 'Y') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Oral_Ty21a_Or_Vivotif" value="Y" <?php if($data['oral_ty21a_or_vivotif'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
            </label>
            <label class="btn btn-default <?php if($data['oral_ty21a_or_vivotif'] == 'N') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Oral_Ty21a_Or_Vivotif" value="N" <?php if($data['oral_ty21a_or_vivotif'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
            </label>
            <label class="btn btn-default <?php if($data['oral_ty21a_or_vivotif'] == 'U') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Oral_Ty21a_Or_Vivotif" value="U" <?php if($data['oral_ty21a_or_vivotif'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
            </label>
          </div>
      </div>
      <div class = "col-sm-3">
        <input type="number" class="form-control" name="Oral_Ty21a_Year_Received" placeholder="Year Received" value="<?php echo $data['oral_ty21a_year_received'] ?>" {{$read}}>
      </div>
    </div>
    <div class="form-group col-md-6">
      <label class="col-md-4 control-label">ViCPS or Typhim Vi shot (Pasteur Merieux)</label>
      <div class="col-sm-5">
          <div class="btn-group toggler" data-toggle="buttons">
            <label class="btn btn-default <?php if($data['vicps_or_typhim_shot'] == 'Y') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Vicps_Or_Typhim_Shot" value="Y" <?php if($data['vicps_or_typhim_shot'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
            </label>
            <label class="btn btn-default <?php if($data['vicps_or_typhim_shot'] == 'N') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Vicps_Or_Typhim_Shot" value="N" <?php if($data['vicps_or_typhim_shot'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
            </label>
            <label class="btn btn-default <?php if($data['vicps_or_typhim_shot'] == 'U') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Vicps_Or_Typhim_Shot" value="U" <?php if($data['vicps_or_typhim_shot'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
            </label>
          </div>
      </div>
      <div class = "col-sm-3">
        <input type="number" class="form-control" name="Vicps_Year_Received" placeholder="Year Received" {{$read}} value="<?php echo $data['vicps_year_received'] ?>">
      </div>
    </div>
  </fieldset>


  <fieldset id="epidemiologic_data">
    <legend>Other Data</legend>
    <div class = "col-md-6">
      <label class="col-md-4 control-label">Does the patient work as a food handler?</label>
      <div class="col-sm-8">
          <div class="btn-group toggler" data-toggle="buttons">
            <label class="btn btn-default <?php if($data['food_handler'] == 'Y') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Food_Handler" value="Y" <?php if($data['food_handler'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
            </label>
            <label class="btn btn-default <?php if($data['food_handler'] == 'N') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Food_Handler" value="N" <?php if($data['food_handler'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
            </label>
            <label class="btn btn-default <?php if($data['food_handler'] == 'U') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Food_Handler" value="U" <?php if($data['food_handler'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
            </label>
          </div>
      </div>
    </div>
    <div class="form-group col-md-12">
      <label class="col-md-2 control-label">Other Comments</label>
      <div class="col-md-8 ui-widget">
        {!! Form::textarea('Other_Comments', $data['other_comments'], ['class' => 'form-control noresize', 'placeholder' => 'Other Comments', 'cols'=>'10', 'rows'=>'2', $read]) !!}
      </div>
    </div>
  </fieldset>



  <?php if($data['is_submitted'] == false) { ?>
    <div class="form-group pull-right">
        <!--<button type="button" class="btn btn-primary" onclick="#'">Close</button>-->
          <button type="submit" value="submit" class="btn btn-success">Submit</button>
    </div>
  <?php } ?>

  {!! Form::close() !!}
  <br clear="all" />
</div>
</div>
