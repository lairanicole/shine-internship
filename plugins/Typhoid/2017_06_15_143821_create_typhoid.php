<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTyphoid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (Schema::hasTable('typhoid_record')!=TRUE) {
            Schema::create('typhoid_record', function (Blueprint $table) {
                $table->increments('id');
                $table->string('typhoid_id', 32);
                $table->string('healthcareservice_id', 32);
                $table->enum('diagnosed_typhoid', ['Y','N','U'])->default('U')->nullable();
                $table->dateTime('symptoms_onset_date')->nullable();
                $table->enum('patient_hospitalized', ['Y','N','U'])->default('U')->nullable();
                $table->integer('days_patient_hospitalized')->default(NULL)->nullable();
                $table->dateTime('date_salmonella_first_isolated')->nullable();
                $table->string('sites_of_isolation',40)->default(',,')->nullable();
                $table->text('soi_other_comments')->nullable();
                $table->string('serotype',40)->default(',,,')->nullable();
                $table->enum('part_of_an_outbreak', ['Y','N','U'])->default('U')->nullable();
                $table->enum('patient_typhoid_vaccine_five_years', ['Y','N','U'])->default('U')->nullable();
                $table->enum('oral_ty21a_or_vivotif', ['Y','N','U'])->default('U')->nullable();
                $table->integer('oral_ty21a_year_received')->default(NULL)->nullable();
                $table->enum('vicps_or_typhim_shot', ['Y','N','U'])->default('U')->nullable();
                $table->integer('vicps_year_received')->default(NULL)->nullable();
                $table->enum('food_handler', ['Y','N','U'])->default('U')->nullable();
                $table->text('other_comments')->nullable();
                $table->tinyInteger('is_submitted')->default(0)->nullable();
                
                $table->softDeletes();
                $table->timestamps();                            
                $table->unique('typhoid_id');
            });
        } 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
