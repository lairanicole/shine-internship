<?php

use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Plugins\Typhoid\TyphoidModel as Typhoid;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

class TyphoidController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        //no index
    }

    public function save()
    {
        $typhoid_id = Input::get('tservice_id');
        $hservice_id = Input::get('hservice_id');

        $typhoid_record = Typhoid::find($typhoid_id);

        $typhoid_record->diagnosed_typhoid = Input::get('Diagnosed_Typhoid');

        $symptomsOnsetDate = (new Datetime(Input::get('Symptoms_Onset_Date')))->format('Y-m-d H:i:s');

        $typhoid_record->symptoms_onset_date = $symptomsOnsetDate;
        // $typhoid_record->symptoms_onset_date = Input::get('Symptoms_Onset_Date');
        $typhoid_record->patient_hospitalized = Input::get('Patient_Hospitalized');

        if (Input::get('Days_Patient_Hospitalized') == NULL) {
            $typhoid_record->days_patient_hospitalized = NULL;
        }
        else {
            $typhoid_record->days_patient_hospitalized = Input::get('Days_Patient_Hospitalized');
        }

        $dateSalmonellaFirstIsolated = (new Datetime(Input::get('Date_Salmonella_First_Isolated')))->format('Y-m-d H:i:s');

        // $typhoid_record->date_salmonella_first_isolated = Input::get('Date_Salmonella_First_Isolated');
        $typhoid_record->date_salmonella_first_isolated = $dateSalmonellaFirstIsolated;
        $typhoid_record->sites_of_isolation = Input::get('Sites_Of_Isolation_Blood') . ',' . Input::get('Sites_Of_Isolation_Stool') . ',' . Input::get('Sites_Of_Isolation_Gall');
        $typhoid_record->soi_other_comments = Input::get('Soi_Other_Comments');
        $typhoid_record->serotype = Input::get('Serotype_A') . ',' . Input::get('Serotype_B') . ',' . Input::get('Serotype_C') . ',' . Input::get('Serotype_D');
        $typhoid_record->part_of_an_outbreak = Input::get('Part_Of_An_Outbreak');
        $typhoid_record->patient_typhoid_vaccine_five_years = Input::get('Patient_Typhoid_Vaccine_Five_Years');
        $typhoid_record->oral_ty21a_or_vivotif = Input::get('Oral_Ty21a_Or_Vivotif');

        if (Input::get('Oral_Ty21a_Year_Received') == NULL) {
            $typhoid_record->oral_ty21a_year_received = NULL;
        }
        else {
            $typhoid_record->oral_ty21a_year_received = Input::get('Oral_Ty21a_Year_Received');
        }

        $typhoid_record->vicps_or_typhim_shot = Input::get('Vicps_Or_Typhim_Shot');

        if (Input::get('Vicps_Year_Received') == NULL) {
            $typhoid_record->vicps_year_received = NULL;
        }
        else {
            $typhoid_record->vicps_year_received = Input::get('Vicps_Year_Received');
        }
        $typhoid_record->food_handler = Input::get('Food_Handler');
        $typhoid_record->other_comments = Input::get('Other_Comments');
        $typhoid_record->is_submitted = true;

        

        $typhoid_record->save();

        $flash_message = 'Typhoid Case Report was Saved!';

        $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

        header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
        exit;

        //return Redirect::back()
         //            ->with('flash_message', $flash_message)
           //          ->with('flash_type', 'alert-success')
             //        ->with('flash_tab', 'dengue');
    }

    public function view()
    {
        
    }

    public static function update()
    {

    }
}
