<?php

namespace Plugins\Typhoid;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class TyphoidModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'typhoid_record';
    protected static $table_name = 'typhoid_record';
    protected $primaryKey = 'typhoid_id';


}
