<?php

$plugin_module = 'patients';            //plugin parent module
$plugin_title = 'PhilHealth';            //plugin title
$plugin_id = 'Philhealth';              //plugin ID
$plugin_location = 'tab';           //UI location where plugin will be accessible
$plugin_primaryKey = 'patient_id';      //primary_key used to find data
$plugin_table = 'patient_philhealthinfo';           //plugintable default; table_name custom table
$plugin_description = 'Patient PhilHealth Information';
$plugin_version = '1.0';
$plugin_developer = 'ShineLabs';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2016";
$plugin_folder = 'Philhealth';

?>
