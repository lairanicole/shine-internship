<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTumorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (Schema::hasTable('tumor_registry_record')!=TRUE) {
        Schema::create('tumor_registry_record', function (Blueprint $table) {
          $table->increments('id');
          $table->string('tumor_registry_id', 60);
          $table->string('healthcareservice_id', 60);
          $table->string('tumor_registry_case_id', 60)->nullable()->change();
          $table->string('physical_activity_type', 60)->nullable()->change();
          $table->integer('minutes_per_exercise')->nullable()->change();
          $table->integer('year_of_diagnosis')->nullable()->change();
          $table->integer('number_of_children')->nullable()->change();
          $table->enum('physical_activity_frequency', ['D','W','M','Q','Y'])->nullable()->change();
          $table->enum('smoking_frequency', ['L','M'])->nullable()->change();
          $table->enum('second_hand_smoke_exposure', ['W','WO'])->nullable()->change();
          $table->integer('second_hand_smoke_years')->nullable()->change();
          $table->string('alcoholic_beverages_type', 60)->nullable()->change();
          $table->integer('alcoholic_beverages_amount')->nullable()->change();
          $table->enum('alcoholic_beverages_unit_of_measure', ['B','G','S'])->nullable()->change();
          $table->enum('alcoholic_beverages_frequency', ['D','W','M','Q','Y'])->nullable()->change();
          $table->integer('age_started_drinking_alcohol')->nullable()->change();
          $table->integer('no_of_years_drinking_alcohol')->nullable()->change();
          $table->string('typical_diet_meat', 60)->nullable()->change();
          $table->enum('typical_diet_meat_frequency', ['D','W','M','Q','Y'])->nullable()->change();
          $table->string('typical_diet_grains', 60)->nullable()->change();
          $table->enum('typical_diet_grains_frequency', ['D','W','M','Q','Y'])->nullable()->change();
          $table->string('typical_diet_fruits_vegetables', 60)->nullable()->change();
          $table->enum('typical_diet_fruits_vegetables_frequency', ['D','W','M','Q','Y'])->nullable()->change();
          $table->integer('number_of_sexual_partners')->nullable()->change();
          $table->string('onset_of_sexual_intercourse', 60)->nullable()->change();
          $table->string('contraceptives_used', 60)->nullable()->change();
          $table->integer('contraceptives_years_used')->nullable()->change();
          $table->enum('menstrual_cycle_regularity', ['R','IR'])->nullable()->change();
          $table->integer('menstrual_cycle_interval')->nullable()->change();
          $table->integer('menopausal_age')->nullable()->change();
          $table->integer('hpvi_year_examined')->nullable()->change();
          $table->integer('hpi_year_examined')->nullable()->change();
          $table->integer('hepa_b_year_examined')->nullable()->change();
          $table->string('cancer_family_member_1_name', 60)->nullable()->change();
          $table->string('cancer_family_member_1_relationship', 60)->nullable()->change();
          $table->string('cancer_family_member_1_type', 60)->nullable()->change();
          $table->longText('procedure_availed')->nullable()->change();
          $table->longText('procedure_result')->nullable()->change();
          $table->longText('procedure_date')->nullable()->change();
          $table->longText('laboratory_done_by')->nullable()->change();
          $table->string('referred_from', 60)->nullable()->change();
          $table->string('reason_for_referral', 60)->nullable()->change();
          $table->dateTime('date_of_consultation_admission')->nullable()->default(NULL)->nullable()->change();
          $table->string('chief_complaint', 60)->nullable()->change();
          $table->dateTime('date_of_diagnosis')->nullable()->default(NULL)->nullable()->change();
          $table->string('non_microscopic_basis_of_diagnosis', 60)->nullable()->change();
          $table->string('microscopic_basis_of_diagnosis', 60)->nullable()->change();
          $table->enum('treatment_purpose', ['CRC','CRI','PLL','OTH'])->nullable()->change();
          $table->string('primary_assistance_given', 60)->nullable()->change();
          $table->dateTime('rafi_ejacc_date_given')->default(NULL)->nullable()->change();
          $table->string('rafi_ejacc_treatment', 60)->nullable()->change();
          $table->string('other_source_treatment', 60)->nullable()->change();

          $table->softDeletes();
          $table->timestamps();
          $table->unique('tumor_registry_id');
        });
      }
      if (Schema::hasTable('tumor_registry_treatment_regiments_record')!=TRUE) {
        Schema::create('tumor_registry_treatment_regiments_record', function (Blueprint $table) {
          $table->increments('id');
          $table->string('tumor_registry_treatment_regiments_id', 60);
          $table->string('tumor_registry_case_id', 60);
          $table->string('tumor_registry_id', 60);
          $table->string('healthcareservice_id', 60)->nullable()->change();
          $table->enum('treatment_purpose', ['CRC','CRI','PLL','OTH'])->nullable()->change();
          $table->integer('primary_assistance_given')->nullable()->change();
          $table->dateTime('rafi_ejacc_date_given')->default(NULL)->nullable()->change();
          $table->longText('rafi_ejacc_treatment')->nullable()->change();
          $table->longText('other_source_treatment')->nullable()->change();
          

          $table->softDeletes();
          $table->timestamps();
          $table->unique('tumor_registry_treatment_regiments_id', 'tr_regiment_id');
        });
      }
      if (Schema::hasTable('tumor_registry_cancer_data_record')!=TRUE) {
        Schema::create('tumor_registry_cancer_data_record', function (Blueprint $table) {
          $table->increments('id');
          $table->string('tumor_registry_cancer_data_id', 60);
          $table->string('tumor_registry_case_id', 60);
          $table->string('tumor_registry_id', 60);
          $table->string('healthcareservice_id', 60);
          $table->enum('multiple_primary_levels', ['ZER','ONE','TWO','THR','FR'])->nullable()->change();
          $table->string('primary_sites', 60)->nullable()->change();
          $table->text('primary_sites_others', 60)->nullable()->change();
          $table->text('other_sites', 60)->nullable()->change();
          $table->string('laterality', 60)->nullable()->change();
          $table->string('histology_morphology', 60)->nullable()->change();
          $table->text('staging', 60)->nullable()->change();
          $table->text('distant_metastasis_sites', 60)->nullable()->change();

          $table->softDeletes();
          $table->timestamps();
          $table->unique('tumor_registry_cancer_data_id', "tr_cancer_id");
        });
      }

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
