<?php
$plugin_id = 'TumorRegistry';                       //plugin ID
$plugin_module = 'healthcareservices';          //module owner
$plugin_location = 'dropdown';                  //UI location where plugin will be accessible
$plugin_primaryKey = 'tumor_registry_id';        //primary_key used to find data
$plugin_table = 'tumor_registry_record';            //plugintable default; table_name custom table
$plugin_relationship = array();
$plugin_tabs_child = array('addservice', 'complaints', 'vitals', 'impanddiag', 'tumorregistry_plugin', 'medicalorders', 'disposition'); 
$plugin_type = 'program';
$plugin_gender = "all";

$plugin_folder = 'TumorRegistry'; //module owner
$plugin_title = 'Tumor Registry';            //plugin title
$plugin_description = 'Tumor Registry';
$plugin_version = '1.0';
$plugin_developer = 'ShineLabs';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2016";

$plugin_tabs = [
    'addservice' => 'Basic Information',
    'disposition' => 'Disposition',
    'medicalorders' => 'Medical Orders',
    'vitals' => 'Vitals & Physical',
    'impanddiag' => 'Impressions and Diagnosis',
    'complaints' => 'Complaints',
    'tumorregistry_plugin' => 'Tumor Registry'
];

$plugin_tabs_models = [
    'complaints' => 'GeneralConsultation',
    'disposition' => 'Disposition',
    'medicalorders' => 'MedicalOrder',
    'vitals' => 'VitalsPhysical',
    'tumorregistry_plugin' => 'TumorRegistryModel'
];

$tumor_registry_record_db = "CREATE TABLE IF NOT EXISTS `tumor_registry_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tumor_registry_id` varchar(32) NOT NULL,
  `healthcareservice_id` varchar(32) NOT NULL,
  `tumor_registry_case_id` varchar(32) NOT NULL,
  `physical_activity_type` varchar(32) DEFAULT NULL,
  `minutes_per_exercise` int(10) DEFAULT NULL,
  `year_of_diagnosis` int(10) DEFAULT NULL,
  `number_of_children` int(10) DEFAULT NULL,
  `physical_activity_frequency` enum('D','W','M','Q','Y') DEFAULT NULL,
  `smoking_frequency` enum('L','M') DEFAULT NULL,
  `second_hand_smoke_exposure` enum('W','WO') DEFAULT NULL,
  `second_hand_smoke_years` int(10) DEFAULT NULL,
  `alcoholic_beverages_type` varchar(32) DEFAULT NULL,
  `alcoholic_beverages_amount` int(10) DEFAULT NULL,
  `alcoholic_beverages_unit_of_measure` enum('B','G','S') DEFAULT NULL,
  `alcoholic_beverages_frequency` enum('D','W','M','Q','Y') DEFAULT NULL,
  `age_started_drinking_alcohol` int(10) DEFAULT NULL,
  `no_of_years_drinking_alcohol` int(10) DEFAULT NULL,
  `typical_diet_meat` varchar(32) DEFAULT NULL,
  `typical_diet_meat_frequency` enum('D','W','M','Q','Y') DEFAULT NULL,
  `typical_diet_grains` varchar(32) DEFAULT NULL,
  `typical_diet_grains_frequency` enum('D','W','M','Q','Y') DEFAULT NULL,
  `typical_diet_fruits_vegetables` varchar(32) DEFAULT NULL,
  `typical_diet_fruits_vegetables_frequency` enum('D','W','M','Q','Y') DEFAULT NULL,
  `number_of_sexual_partners` int(10) DEFAULT NULL,
  `onset_of_sexual_intercourse` varchar(32) DEFAULT NULL,
  `contraceptives_used` varchar(32) DEFAULT NULL,
  `contraceptives_years_used` int(10) DEFAULT NULL,
  
  `menstrual_cycle_regularity` enum('R','IR') DEFAULT NULL,
  `menstrual_cycle_interval` int(10) DEFAULT NULL,
  `menopausal_age` int(10) NOT NULL,
  `hpvi_year_examined` int(10) DEFAULT NULL,
  `hpi_year_examined` int(10) DEFAULT NULL,
  `hepa_b_year_examined` int(10) DEFAULT NULL,
  `cancer_family_member_1_name` varchar(2500) DEFAULT NULL,
  `cancer_family_member_1_relationship` varchar(2500) DEFAULT NULL,
  `cancer_family_member_1_type` varchar(2500) DEFAULT NULL,
  `procedure_availed` longtext,
  `procedure_result` longtext,
  `procedure_date` longtext,
  `laboratory_done_by` longtext,
  `referred_from` varchar(32) DEFAULT NULL,
  `reason_for_referral` varchar(32) DEFAULT NULL,
  `date_of_consultation_admission` datetime DEFAULT NULL,
  `chief_complaint` varchar(32) DEFAULT NULL,
  `date_of_diagnosis` datetime DEFAULT NULL,
  `non_microscopic_basis_of_diagnosis` varchar(32) DEFAULT NULL,
  `microscopic_basis_of_diagnosis` varchar(32) DEFAULT NULL,
  `treatment_purpose` enum('CRC','CRI','PLL','OTH') DEFAULT NULL,
  `primary_assistance_given` varchar(32) NOT NULL,
  `rafi_ejacc_date_given` datetime DEFAULT NULL,
  `rafi_ejacc_treatment` varchar(120) DEFAULT NULL,
  `other_source_treatment` varchar(120) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tumor_registry_id` (`tumor_registry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;";

$tumor_registry_treatment_regiments_record_db = "CREATE TABLE IF NOT EXISTS `tumor_registry_treatment_regiments_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tumor_registry_treatment_regiments_id` varchar(32) NOT NULL,
  `tumor_registry_case_id` varchar(32) NOT NULL,
  `tumor_registry_id` varchar(60) NOT NULL,
  `healthcareservice_id` varchar(32) NOT NULL,
  `treatment_purpose` enum('CRC','CRI','PLL','OTH') DEFAULT NULL,
  `primary_assistance_given` varchar(32) NOT NULL,
  `rafi_ejacc_date_given` datetime DEFAULT NULL,
  `rafi_ejacc_treatment` longtext,
  `other_source_treatment` longtext,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tumor_registry_treatment_regiments_id` (`tumor_registry_treatment_regiments_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;";

$tumor_registry_cancer_data_record_db = "CREATE TABLE IF NOT EXISTS `tumor_registry_cancer_data_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tumor_registry_cancer_data_id` varchar(32) NOT NULL,
  `tumor_registry_case_id` varchar(32) NOT NULL,
  `tumor_registry_id` varchar(60) NOT NULL,
  `healthcareservice_id` varchar(32) NOT NULL,
  `multiple_primary_levels` enum('ZER','ONE','TWO','THR','FR') DEFAULT NULL,
  `primary_sites` varchar(180) DEFAULT NULL,
  `primary_sites_others` text,
  `other_sites` text,
  `laterality` varchar(180) DEFAULT NULL,
  `histology_morphology` varchar(32) DEFAULT NULL,
  `staging` text,
  `distant_metastasis_sites` text,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tumor_registry_cancer_data_id` (`tumor_registry_cancer_data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;";

// DB::statement($tumor_registry_record_db);
// DB::statement($tumor_registry_treatment_regiments_record_db);
// DB::statement($tumor_registry_cancer_data_record_db);
