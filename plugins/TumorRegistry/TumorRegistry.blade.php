
<?php
    $hservice_id = $data['healthcareservice_id'];
    $trservice_id = $data['tumor_registry_id'];
    $patient_gender = ($allData['patient']['gender']);
    // dd($hservice_id);

    // if($allData['healthcareData']->consultation_type == "FOLLO" && $data['tumor_registry_case_id'] == NULL) {
    //   $ajaxIsDisabled = "disabled";
    // } else {
    //   $ajaxIsDisabled = "";
    // }

    // dd($isFollowUp);
    $isNewHealthcare = false;
    if($data['tumor_registry_case_id'] == NULL) {
      $isNewHealthcare = true;
    }

    if($pdata AND $data['tumor_registry_case_id'] == NULL) {
        // $pdata['tumor_registry_case_id'] = NULL;
        $data = $pdata;
        // dd($pdata);
    }


    // dd($patient_gender);
    if( isset($allData['pluginparentdata']) ) {
        $trcaseID = $allData['pluginparentdata']->tumor_registry_case_id;
    } elseif($data->tumor_registry_case_id) {
        $trcaseID = $data->tumor_registry_case_id;
    } else {
        $trcaseID = NULL;
    }
    // dd($trcaseID);

    if(!isset($allData['disposition_record'])) { $isDisabled = ''; }
    else { $isDisabled = 'disabled'; }

    if ($isNewHealthcare == false) {
      $ajaxIsDisabled = "";
    } else {
      $ajaxIsDisabled = "disabled";
    }
    // dd($ajaxIsDisabled);

?>


<div id="tumor_registry">

  {!! Form::hidden('trservice_id',$trservice_id) !!}
  {!! Form::hidden('hservice_id',$hservice_id,array('id'=>'hservice_id')) !!}
  {!! Form::hidden('trcase_id',$trcaseID,array('id'=>'trcaseID')) !!}

  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active">
        <a href="#patient_history_tab" data-toggle="tab">Patient Data</a>
      </li>
      <li class="">
        <a href="#family_history_tab" data-toggle="tab">Family History</a>
      </li>
      <li class="">
        <a href="#screening_history_tab" data-toggle="tab">Basis of Diagnosis and Screening History</a>
      </li>
      <li class="">
        <a href="#cancer_history_tab" data-toggle="tab">Cancer Data</a>
      </li>
      <li class="">
        <a href="#treatment_regiment_tab" data-toggle="tab">Treatment Regiment</a>
      </li>
    </ul>




    <div class="tab-content">
      <!-- Patient history I start -->
      <div class="tab-pane active" id="patient_history_tab">
        <div class = "row">
          <div class="col-md-12">
          <legend>Physical Activity</legend>
            <!-- <div class="form-group col-md-6"> -->
              <label class="col-md-2 control-label">Type</label>
              <div class="col-md-4">
               <input {{$isDisabled}} {{$isDisabled}} type="text" class="form-control" name="tumorregistry[PatientHistory][Physical_Activity_Type]" placeholder="Physical Activity Type" value="<?php echo $data['physical_activity_type'] ?>" >
              </div>
            <!-- </div> -->
            <!-- <div class="form-group col-md-6"> -->
              <label class="col-md-2 control-label">Minutes Per Exercise</label>
              <div class="col-md-4">
               <input {{$isDisabled}} {{$isDisabled}} type="number" class="form-control" name="tumorregistry[PatientHistory][Minutes_Per_Exercise]" placeholder="Minutes Per Exercise" value="<?php echo $data['minutes_per_exercise'] ?>">
              </div>
            <!-- </div> -->
          </div>
        </div>
        <div class = "row">
          <div class="col-md-12">
            <label class="col-md-2 control-label">Frequency</label>
            <div class="col-md-4">
              <select {{$isDisabled}} {{$isDisabled}} name="tumorregistry[PatientHistory][Physical_Activity_Frequency]" class="form-control" onchange='checkvalue(this.value)'>
                <option value="D" <?php if($data['physical_activity_frequency'] == 'D') {echo 'selected';} ?>>Daily</option>
                <option value="W" <?php if($data['physical_activity_frequency'] == 'W') {echo 'selected';} ?>>Weekly</option>
                <option value="M" <?php if($data['physical_activity_frequency'] == 'M') {echo 'selected';} ?>>Monthly</option>
                <option value="Q" <?php if($data['physical_activity_frequency'] == 'Q') {echo 'selected';} ?>>Quarterly</option>
                <option value="Y" <?php if($data['physical_activity_frequency'] == 'Y') {echo 'selected';} ?>>Yearly</option>
              </select>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
          <legend>Smoking</legend>
            <label class="col-md-2 control-label">Frequency</label>
            <div class="col-md-10">
              <div class="btn-group toggler" data-toggle="buttons">
                <label {{$isDisabled}} class="btn btn-default <?php if($data['smoking_frequency'] == 'L') {echo 'active';} ?>" >
                 <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tumorregistry[PatientHistory][Smoking_Frequency]" value="L" <?php if($data['smoking_frequency'] == 'L') {echo 'checked=\'checked\' ';} ?>> Less than / equal to 1 pack per day
                </label>
                <label {{$isDisabled}} class="btn btn-default <?php if($data['smoking_frequency'] == 'M') {echo 'active';} ?>" >
                 <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tumorregistry[PatientHistory][Smoking_Frequency]" value="M" <?php if($data['smoking_frequency'] == 'M') {echo 'checked=\'checked\' ';} ?>> More than 1 pack per day
                </label>
              </div>
            </div>

          </div>
        </div>
        <div class = "row">
          <div class="col-md-12">
            <label class="col-md-2 control-label">Second Hand Smoke</label>
            <div class="col-md-4">
              <div class="btn-group toggler" data-toggle="buttons">
                <label {{$isDisabled}} class="btn btn-default <?php if($data['second_hand_smoke_exposure'] == 'W') {echo 'active';} ?>" >
                 <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tumorregistry[PatientHistory][Second_Hand_Smoke_Exposure]" value="W" <?php if($data['second_hand_smoke_exposure'] == 'W') {echo 'checked=\'checked\' ';} ?>> With Exposure
                </label>
                <label {{$isDisabled}} class="btn btn-default <?php if($data['second_hand_smoke_exposure'] == 'WO') {echo 'active';} ?>" >
                 <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tumorregistry[PatientHistory][Second_Hand_Smoke_Exposure]" value="WO" <?php if($data['second_hand_smoke_exposure'] == 'WO') {echo 'checked=\'checked\' ';} ?>> No Exposure
                </label>
              </div>
            </div>
          <!-- </div> -->
          <!-- <div class="form-group col-md-6"> -->
              <label class="col-md-2 control-label">Number of Years Exposed</label>
              <div class="col-md-4">
               <input {{$isDisabled}} type="number" class="form-control" name="tumorregistry[PatientHistory][Second_Hand_Smoke_Years]" placeholder="Number of Years Exposed" value="<?php echo $data['second_hand_smoke_years'] ?>" >
              </div>
            </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
          <legend>Drinking of Alcoholic Beverages</legend>
            <label class="col-md-2 control-label">Types</label>
            <div class="col-md-4">
              <input {{$isDisabled}} type="text" class="form-control" name="tumorregistry[PatientHistory][Alcoholic_Beverages_Type]" placeholder="Alcholic Beverages Type" value="<?php echo $data['alcoholic_beverages_type'] ?>" >
            </div>
          <!-- </div> -->
          <!-- <div class="col-md-6"> -->
            <label class="col-md-2 control-label">Amount</label>
            <div class="col-md-4">
              <input {{$isDisabled}} type="Number" class="form-control" name="tumorregistry[PatientHistory][Alcoholic_Beverages_Amount]" placeholder="Amount" value="<?php echo $data['alcoholic_beverages_amount'] ?>" >
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <label class="col-md-2 control-label">Unit of Measure</label>
            <div class="col-md-4">
              <div class="btn-group toggler" data-toggle="buttons">
                <label {{$isDisabled}} class="btn btn-default <?php if($data['alcoholic_beverages_unit_of_measure'] == 'B') {echo 'active';} ?>" >
                 <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tumorregistry[PatientHistory][Alcoholic_Beverages_Unit_Of_Measure]" value="B" <?php if($data['alcoholic_beverages_unit_of_measure'] == 'B') {echo 'checked=\'checked\' ';} ?>> Bottle
                </label>
                <label {{$isDisabled}} class="btn btn-default <?php if($data['alcoholic_beverages_unit_of_measure'] == 'G') {echo 'active';} ?>" >
                 <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tumorregistry[PatientHistory][Alcoholic_Beverages_Unit_Of_Measure]" value="G" <?php if($data['alcoholic_beverages_unit_of_measure'] == 'G') {echo 'checked=\'checked\' ';} ?>> Glass
                </label><label {{$isDisabled}} class="btn btn-default <?php if($data['alcoholic_beverages_unit_of_measure'] == 'S') {echo 'active';} ?>" >
                 <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tumorregistry[PatientHistory][Alcoholic_Beverages_Unit_Of_Measure]" value="S" <?php if($data['alcoholic_beverages_unit_of_measure'] == 'S') {echo 'checked=\'checked\' ';} ?>> Shot
                </label>
              </div>
            </div>
          <!-- </div> -->
          <!-- <div class="col-md-6"> -->
            <label class="col-md-2 control-label">Frequency</label>

            <div class="col-md-4">
              <select {{$isDisabled}} name="tumorregistry[PatientHistory][Alcoholic_Beverages_Frequency]" class="form-control" onchange='checkvalue(this.value)'>
                <option value="D" <?php if($data['alcoholic_beverages_frequency'] == 'D') {echo 'selected';} ?>>Daily</option>
                <option value="W" <?php if($data['alcoholic_beverages_frequency'] == 'W') {echo 'selected';} ?>>Weekly</option>
                <option value="M" <?php if($data['alcoholic_beverages_frequency'] == 'M') {echo 'selected';} ?>>Monthly</option>
                <option value="Q" <?php if($data['alcoholic_beverages_frequency'] == 'Q') {echo 'selected';} ?>>Quarterly</option>
                <option value="Y" <?php if($data['alcoholic_beverages_frequency'] == 'Y') {echo 'selected';} ?>>Yearly</option>
              </select>
            </div>

          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <label class="col-md-2 control-label">Age Started Drinking Alcohol</label>
            <div class="col-md-4">
              <input {{$isDisabled}} type="number" class="form-control" name="tumorregistry[PatientHistory][Age_Started_Drinking_Alcohol]" placeholder="Age Started Drinking Alcohol" value="<?php echo $data['age_started_drinking_alcohol'] ?>" >
            </div>
          <!-- </div> -->
          <!-- <div class="col-md-6"> -->
            <label class="col-md-2 control-label">No. of Years Drinking Alcohol</label>
            <div class="col-md-4">
              <input {{$isDisabled}} type="Number" class="form-control" name="tumorregistry[PatientHistory][No_Of_Years_Drinking_Alcohol]" placeholder="No. of Years Drinking Alcohol" value="<?php echo $data['no_of_years_drinking_alcohol'] ?>" >
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
          <legend>Usual/Typical Diet</legend>
            <div class="form-group icheck">
              <?php
              if (json_decode($data['typical_diet_meat']) != null) {
                $meatArray = json_decode($data['typical_diet_meat']);
              } else {
                $meatArray = [];
              }
               ?>

              <label class="col-md-2 control-label">Meats</label>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Meat][]" value="F" <?php if (in_array("F", $meatArray)) { echo "checked='checked'"; } ?> > Fish
                  </label>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Meat][]" value="BP" <?php if (in_array("BP", $meatArray)) { echo "checked='checked'"; } ?>> Beef/Pork
                  </label>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Meat][]" value="C" <?php if (in_array("C", $meatArray)) { echo "checked='checked'"; } ?>> Chicken
                  </label>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Meat][]" value="E" <?php if (in_array("E", $meatArray)) { echo "checked='checked'"; } ?>> Egg
                  </label>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Meat][]" value="OTH" <?php if (in_array("OTH", $meatArray)) { echo "checked='checked'"; } ?>> Others
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <label class="col-md-2 control-label">Meat Diet Frequency</label>

            <div class="col-md-4">
              <select {{$isDisabled}} name="tumorregistry[PatientHistory][Typical_Diet_Meat_Frequency]" class="form-control" onchange='checkvalue(this.value)'>
                <option value="D" <?php if($data['typical_diet_meat_frequency'] == 'D') {echo 'selected';} ?>>Daily</option>
                <option value="W" <?php if($data['typical_diet_meat_frequency'] == 'W') {echo 'selected';} ?>>Weekly</option>
                <option value="M" <?php if($data['typical_diet_meat_frequency'] == 'M') {echo 'selected';} ?>>Monthly</option>
                <option value="Q" <?php if($data['typical_diet_meat_frequency'] == 'Q') {echo 'selected';} ?>>Quarterly</option>
                <option value="Y" <?php if($data['typical_diet_meat_frequency'] == 'Y') {echo 'selected';} ?>>Yearly</option>
              </select>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <div class="form-group icheck">
              <?php
              if (json_decode($data['typical_diet_grains']) != null) {
                $grainsArray = json_decode($data['typical_diet_grains']);
              } else {
                $grainsArray = [];
              }
               ?>
              <label class="col-md-2 control-label">Grains</label>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Grains][]" value="R" <?php if (in_array("R", $grainsArray)) { echo "checked='checked'"; } ?>> Rice
                  </label>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Grains][]" value="G" <?php if (in_array("G", $grainsArray)) { echo "checked='checked'"; } ?>> Grains
                  </label>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Grains][]" value="B" <?php if (in_array("B", $grainsArray)) { echo "checked='checked'"; } ?>> Bread
                  </label>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Grains][]" value="C" <?php if (in_array("C", $grainsArray)) { echo "checked='checked'"; } ?>> Cereals
                  </label>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Grains][]" value="RC" <?php if (in_array("RC", $grainsArray)) { echo "checked='checked'"; } ?>> Root Crops
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <label class="col-md-2 control-label">Grains Diet Frequency</label>
            <div class="col-md-4">
              <select {{$isDisabled}} name="tumorregistry[PatientHistory][Typical_Diet_Grains_Frequency]" class="form-control" onchange='checkvalue(this.value)'>
                <option value="D" <?php if($data['typical_diet_grains_frequency'] == 'D') {echo 'selected';} ?>>Daily</option>
                <option value="W" <?php if($data['typical_diet_grains_frequency'] == 'W') {echo 'selected';} ?>>Weekly</option>
                <option value="M" <?php if($data['typical_diet_grains_frequency'] == 'M') {echo 'selected';} ?>>Monthly</option>
                <option value="Q" <?php if($data['typical_diet_grains_frequency'] == 'Q') {echo 'selected';} ?>>Quarterly</option>
                <option value="Y" <?php if($data['typical_diet_grains_frequency'] == 'Y') {echo 'selected';} ?>>Yearly</option>
              </select>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <div class="form-group icheck">
              <?php
              if (json_decode($data['typical_diet_fruits_vegetables']) != null) {
                $fruitsVegeArray = json_decode($data['typical_diet_fruits_vegetables']);
              } else {
                $fruitsVegeArray = [];
              }
               ?>
              <label class="col-md-2 control-label">Fruits/Vegetables</label>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Fruits_Vegetables][]" value="F" <?php if (in_array("F", $fruitsVegeArray)) { echo "checked='checked'"; } ?>> Fruits
                  </label>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="checkbox">
                  <label>
                    <input {{$isDisabled}} type="checkbox" name="tumorregistry[PatientHistory][Typical_Diet_Fruits_Vegetables][]" value="V" <?php if (in_array("V", $fruitsVegeArray)) { echo "checked='checked'"; } ?>> Vegetables
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <label class="col-md-2 control-label">Fruits/Vegetables Diet Frequency</label>
            <div class="col-md-4">
              <select {{$isDisabled}} name="tumorregistry[PatientHistory][Typical_Diet_Fruits_Vegetables_Frequency]" class="form-control" onchange='checkvalue(this.value)'>
                <option value="D" <?php if($data['typical_diet_fruits_vegetables_frequency'] == 'D') {echo 'selected';} ?>>Daily</option>
                <option value="W" <?php if($data['typical_diet_fruits_vegetables_frequency'] == 'W') {echo 'selected';} ?>>Weekly</option>
                <option value="M" <?php if($data['typical_diet_fruits_vegetables_frequency'] == 'M') {echo 'selected';} ?>>Monthly</option>
                <option value="Q" <?php if($data['typical_diet_fruits_vegetables_frequency'] == 'Q') {echo 'selected';} ?>>Quarterly</option>
                <option value="Y" <?php if($data['typical_diet_fruits_vegetables_frequency'] == 'Y') {echo 'selected';} ?>>Yearly</option>
              </select>
            </div>
          </div>
        </div>


        <div class = "row">
          <div class="col-md-12">
          <legend>Sexual Activity</legend>
            <!-- <div class="form-group col-md-6"> -->
              <label class="col-md-2 control-label">Number of Sexual Partners</label>
              <div class="col-md-4">
               <input {{$isDisabled}} type="number" class="form-control" name="tumorregistry[PatientHistory][Number_Of_Sexual_Partners]" placeholder="Number of Sexual Partners" value="<?php echo $data['number_of_sexual_partners'] ?>" >
              </div>
            <!-- </div> -->
            <!-- <div class="form-group col-md-6"> -->
              <label class="col-md-2 control-label">Onset of Sexual Intercourse</label>
              <div class="col-md-4">
               <input {{$isDisabled}} type="text" class="form-control" name="tumorregistry[PatientHistory][Onset_Of_Sexual_Intercourse]" placeholder="Onset of Sexual Intercourse" value="<?php echo $data['onset_of_sexual_intercourse'] ?>">
              </div>
            <!-- </div> -->
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <!-- <div class="form-group col-md-6"> -->
              <label class="col-md-2 control-label">Contraceptives Used</label>
              <div class="col-md-4">
               <input {{$isDisabled}} type="text" class="form-control" name="tumorregistry[PatientHistory][Contraceptives_Used]" placeholder="Contraceptives Used" value="<?php echo $data['contraceptives_used'] ?>" >
              </div>
            <!-- </div> -->
            <!-- <div class="form-group col-md-6"> -->
              <label class="col-md-2 control-label">No. of Years Contraceptive was Used</label>
              <div class="col-md-4">
               <input {{$isDisabled}} type="number" class="form-control" name="tumorregistry[PatientHistory][Contraceptives_Years_Used]" placeholder="Number of Years" value="<?php echo $data['contraceptives_years_used'] ?>">
              </div>
            <!-- </div> -->
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <label class="col-md-2 control-label">No. of Children</label>
            <div class="col-md-4">
             <input {{$isDisabled}} type="text" class="form-control" name="tumorregistry[PatientHistory][Number_Of_Children]" placeholder="No. of Children" value="<?php echo $data['number_of_children'] ?>" >
            </div>
          </div>
        </div>

        @if ($patient_gender == "F")
        <div class = "row">
          <div class="col-md-12">
          <legend>Menstrual Activity</legend>
            <!-- <div class="form-group col-md-6"> -->
              <label class="col-md-2 control-label">Menstrual Cycles</label>
              <div class="col-md-4">
                <div class="btn-group toggler" data-toggle="buttons">
                  <label {{$isDisabled}} class="btn btn-default <?php if($data['menstrual_cycle_regularity'] == 'R') {echo 'active';} ?>" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tumorregistry[PatientHistory][Menstrual_Cycle_Regularity]" value="R" <?php if($data['menstrual_cycle_regularity'] == 'R') {echo 'checked=\'checked\' ';} ?>> Regular
                  </label>
                  <label {{$isDisabled}} class="btn btn-default <?php if($data['menstrual_cycle_regularity'] == 'IR') {echo 'active';} ?>" >
                   <i class="fa fa-check"></i> <input {{$isDisabled}} type="radio" name="tumorregistry[PatientHistory][Menstrual_Cycle_Regularity]" value="IR" <?php if($data['menstrual_cycle_regularity'] == 'IR') {echo 'checked=\'checked\' ';} ?>> Irregular
                  </label>
                </div>
              </div>
            <!-- </div> -->
            <!-- <div class="form-group col-md-6"> -->
              <label class="col-md-2 control-label">Interval/Cycle</label>
              <div class="col-md-4">
               <input {{$isDisabled}} type="number" class="form-control" name="tumorregistry[PatientHistory][Menstrual_Cycle_Interval]" placeholder="Interval/Cycle" value="<?php echo $data['menstrual_cycle_interval'] ?>">
              </div>
            <!-- </div> -->
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <!-- <div class="form-group col-md-6"> -->
              <label class="col-md-2 control-label">Menopausal Age</label>
              <div class="col-md-4">
               <input {{$isDisabled}} type="number" class="form-control" name="tumorregistry[PatientHistory][Menopausal_Age]" placeholder="Menopausal Age" value="<?php echo $data['menopausal_age'] ?>">
              </div>
            <!-- </div> -->
          </div>
        </div>
        @endif

        <div class = "row">
          <div class="col-md-12">
          <legend>Infections <small>(leave blank if not applicable)</small></legend>
            <!-- <div class="form-group"> -->
              <label class="col-md-2 control-label">Human Papilloma Virus Infection (HPVI)<small> (Year Examined)</small></label>
              <div class="col-md-4">
               <input {{$isDisabled}} type="number" class="form-control" name="tumorregistry[PatientHistory][Hpvi_Year_Examined]" placeholder="HPVI Year Examined" value="<?php echo $data['hpvi_year_examined'] ?>">
              </div>
            <!-- </div> -->
            <!-- <div class="form-group"> -->
              <label class="col-md-2 control-label">Helicobacter Pylori Infection (HPI)<small> (Year Examined)</small></label>
              <div class="col-md-4">
               <input {{$isDisabled}} type="number" class="form-control" name="tumorregistry[PatientHistory][Hpi_Year_Examined]" placeholder="HPI Year Examined" value="<?php echo $data['hpi_year_examined'] ?>">
              </div>
            <!-- </div> -->
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-12">
            <label class="col-md-2 control-label">Hepatitis B Virus Infection<small> (Year Examined)</small></label>
            <div class="col-md-4">
             <input {{$isDisabled}} type="number" class="form-control" name="tumorregistry[PatientHistory][Hepa_B_Year_Examined]" placeholder="Hepa B Year Examined" value="<?php echo $data['hepa_b_year_examined'] ?>">
            </div>
          </div>
        </div>
        <!-- <div class="row"> -->
          <div class = "row">
            <div class="col-md-12">
            <legend>Facility Details</legend>
              <!-- <div class="form-group col-md-6"> -->
                <label class="col-md-2 control-label">Facility Referred From</label>
                <div class="col-md-4">
                 <input {{$isDisabled}} type="text" class="form-control" name="tumorregistry[PatientHistory][Referred_From]" placeholder="Facility Referred From" value="<?php echo $data['referred_from'] ?>">
                </div>
              <!-- </div> -->
              <!-- <div class="form-group col-md-6"> -->
                <label class="col-md-2 control-label">Reason for Referral</label>
                <div class="col-md-4">
                 <input {{$isDisabled}} type="text" class="form-control" name="tumorregistry[PatientHistory][Reason_For_Referral]" placeholder="Brief Reason for Referral" value="<?php echo $data['reason_for_referral'] ?>">
                </div>
              <!-- </div> -->
            </div>
          </div>

          <div class = "row">
            <div class="col-md-12">
              <!-- <div class="form-group col-md-6"> -->
                <label class="col-sm-2 control-label">Date of Consultation/Admission</label>
                <div class="col-sm-4">
                  <div class="input-group">
                    <i class="fa fa-calendar inner-icon"></i>
                    {!! Form::text('tumorregistry[PatientHistory][Date_Of_Consultation_Admission]', (empty($data['date_of_consultation_admission']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['date_of_consultation_admission']))), ['class' => 'form-control', 'id'=>'datepicker', $read]); !!}
                  </div>
                </div>
              <!-- </div> -->
              <!-- <div class="form-group col-md-6"> -->
                <label class="col-md-2 control-label">Chief Complain</label>
                <div class="col-md-4">
                 <input {{$isDisabled}} type="text" class="form-control" name="tumorregistry[PatientHistory][Chief_Complaint]" placeholder="Chief Complain" value="<?php echo $data['chief_complaint'] ?>">
                </div>
              <!-- </div> -->
            </div>
          </div>
          <div class = "row">
            <div class="col-md-12">
              <!-- <div class="form-group col-md-6"> -->
                <label class="col-sm-2 control-label">Date of Diagnosis</label>
                <div class="col-sm-4">
                  <div class="input-group">
                    <i class="fa fa-calendar inner-icon"></i>
                    {!! Form::text('tumorregistry[PatientHistory][Date_Of_Diagnosis]', (empty($data['date_of_diagnosis']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['date_of_diagnosis']))), ['class' => 'form-control', 'id'=>'datepicker', $read]); !!}
                  </div>
                </div>
              <!-- </div> -->
            </div>
          </div>
        <!-- </div> -->

      </div>
      <!-- Patient history I end -->

      <!-- Family History start -->
      <div class="tab-pane" id="family_history_tab">

        <div class= "row ">
          <div class="col-md-12 parentFamilyMemberDiv">
            <legend>Family History of Cancer </legend>
            <?php
              if (json_decode($data['cancer_family_member_1_name']) != null) {
                $familyMemberNameArray = json_decode($data['cancer_family_member_1_name']);
                $familyMemberRelationshipArray = json_decode($data['cancer_family_member_1_relationship']);
                $familyMemberTypeArray = json_decode($data['cancer_family_member_1_type']);

                $arrayFamilyMemberCount = count($familyMemberNameArray);
              } else {
                $arrayFamilyMemberCount = -1;
              }
            ?>

            <?php
              for ($x = 0; $x < $arrayFamilyMemberCount; $x++) { ?>
                <div class="forLoopFamilyMemberDiv">
                  <div class="col-md-12">
                    <legend><small>Family Member</small></legend>
                    <div class = "col-md-10">
                      <div class = "row">
                        <div class="col-md-12">
                          <div class="form-group col-md-6">
                            <label class="col-md-4 control-label">Family Member Name</label>
                            <div class="col-md-8">
                             <input {{$isDisabled}} type="text" class="form-control" name="tumorregistry[FamilyHistory][Cancer_Family_Member_1_Name][]" placeholder="Family Member Name" value="<?php echo $familyMemberNameArray[$x] ?>">
                            </div>
                          </div>
                          <div class="form-group col-md-6">
                            <label class="col-md-4 control-label">Relationship</label>
                            <div class="col-md-8">
                             <input {{$isDisabled}} type="text" class="form-control" name="tumorregistry[FamilyHistory][Cancer_Family_Member_1_Relationship][]" placeholder="Family Member Relationship" value="<?php echo $familyMemberRelationshipArray[$x] ?>">
                            </div>
                          </div>
                        </div>
             col-md-8">
                       <input type="text" class="form-control" name="tumorregistry[FamilyHistory][Cancer_Family_Member_1_Relationship][]" placeholder="Family Member Relationship" value="">
                      </div>
                    </div>
                  </div>
                </div>
                <div class = "row">
                  <div class="col-md-12">
                    <div class="form-group col-md-6">
                      <label class="col-md-4 control-label">Type of Cancer</label>
                      <div class="col-md-8">
                       <input type="text" class="form-control" name="tumorregistry[FamilyHistory][Cancer_Family_Member_1_Type][]" placeholder="Family Member Type of Cancer" value="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <button id="removeFamilyMember-button" type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Remove</button>
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-success" id="addFamilyMember-button" style="padding-right:15px;" type="button">Add Another Family Member</button>
        </div>
      </div>
      <!-- family history end -->

      <!-- Screening History  start -->
      <div class="tab-pane" id="screening_history_tab">

        
        <div class = "row">
          <div class="col-md-12">
            <legend>Most Valid Basis of Diagnosis</legend>
            <div class="form-group col-md-6 icheck">
              <?php 
              if (json_decode($data['non_microscopic_basis_of_diagnosis']) != null) {
                $nonMicroscopicArray = json_decode($data['non_microscopic_basis_of_diagnosis']);
              } else {
                $nonMicroscopicArray = [];
              }
              ?>

              <label class="col-sm-4 control-label">Non-Microscopic</label>
              <div class="col-sm-8">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="tumorregistry[ScreeningHistory][Non_Microscopic_Basis_Of_Diagnosis][]" value="DHO" <?php if (in_array("DHO", $nonMicroscopicArray)) { echo "checked='checked'"; } ?>> Death Certificates Only
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="tumorregistry[ScreeningHistory][Non_Microscopic_Basis_Of_Diagnosis][]" value="CI" <?php if (in_array("CI", $nonMicroscopicArray)) { echo "checked='checked'"; } ?>> Clinical Investigation
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="tumorregistry[ScreeningHistory][Non_Microscopic_Basis_Of_Diagnosis][]" value="STM" <?php if (in_array("STM", $nonMicroscopicArray)) { echo "checked='checked'"; } ?>> Specific Tumor Markers
                  </label>
                </div>
              </div>
            </div>

            <div class="form-group col-md-6 icheck">
              <?php 
              if (json_decode($data['microscopic_basis_of_diagnosis']) != null) {
                $microscopicArray = json_decode($data['microscopic_basis_of_diagnosis']);
              } else {
                $microscopicArray = [];
              }
              ?>
              <label class="col-sm-4 control-label">Microscopic</label>
              <div class="col-sm-8">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="tumorregistry[ScreeningHistory][Microscopic_Basis_Of_Diagnosis][]" value="CM" <?php if (in_array("CM", $microscopicArray)) { echo "checked='checked'"; } ?>> Cytology/Hematology
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="tumorregistry[ScreeningHistory][Microscopic_Basis_Of_Diagnosis][]" value="HM" <?php if (in_array("HM", $microscopicArray)) { echo "checked='checked'"; } ?>> Histology of Metastasis
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="tumorregistry[ScreeningHistory][Microscopic_Basis_Of_Diagnosis][]" value="HP" <?php if (in_array("HP", $microscopicArray)) { echo "checked='checked'"; } ?>> Histology of Primary
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="tumorregistry[ScreeningHistory][Microscopic_Basis_Of_Diagnosis][]" value="U" <?php if (in_array("U", $microscopicArray)) { echo "checked='checked'"; } ?>> Unknown
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class = "row addButtonLocationDiv">
          <div class="col-md-12 parentDivScreening">
          <legend>Screening History</legend> 

            <?php
            if (json_decode($data['procedure_availed']) != null) {
              $procedureAvailedArray = json_decode($data['procedure_availed']);
              $procedureResultArray = json_decode($data['procedure_result']);
              $procedureDateArray = json_decode($data['procedure_date']);
              // dd(date('m/d/Y', strtotime(json_decode($procedureDateArray[0]))));
              // dd(strtotime(json_decode($procedureDateArray[0])));
              $laboratoryDoneByArray = json_decode($data['laboratory_done_by']);

              $arrayProcedureCount = count($procedureAvailedArray);
            } else {
              $arrayProcedureCount = -1;
            }
          ?>
          
          <?php 
          for ($x = 0; $x < $arrayProcedureCount; $x++) { ?>
            <div class="cloneDivForLoopScreening">
              <div class="row">
                <div class="col-md-12">
                  <legend><small>Procedure</small></legend>
                  <div class="col-md-10">
                    <div class="row">
                      <div class="col-md-6">
                        <label class="col-sm-4 control-label">Procedure Availed</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Procedure Availed" name="tumorregistry[ScreeningHistory][Procedure_Availed][]" value="<?php echo $procedureAvailedArray[$x] ?>"/>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label class="col-sm-4 control-label">Result</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Procedure Result" name="tumorregistry[ScreeningHistory][Procedure_Result][]" value="<?php echo $procedureResultArray[$x] ?>"/>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <label class="col-sm-4 control-label">Procedure Date</label>
                        <div class="col-sm-8">
                          <div class="input-group">
                            <i class="fa fa-calendar inner-icon"></i>
                            {!! Form::text('tumorregistry[ScreeningHistory][Procedure_Date][]',  date('m/d/Y', strtotime(json_decode($procedureDateArray[$x]))), ['class' => 'form-control datepicker', $read]); !!}
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label class="col-sm-4 control-label">Laboratory Done By</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Laboratory Done By" name="tumorregistry[ScreeningHistory][Laboratory_Done_By][]" value="<?php echo $laboratoryDoneByArray[$x] ?>"/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <button id="removeScreeningForLoop-button" type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Remove</button>
                  </div>
                </div>
              </div>              
            </div>
            <?php  } ?>
            <input type="hidden" value="0" id="hiddenScreeningCounter">

            <div class="cloneDivScreening hidden">
              <div class="row">
                <div class="col-md-12">
                  <legend><small>Procedure</small></legend>
                  <div class="col-md-10">
                    <div class="row">
                      <div class="col-md-6">
                        <label class="col-sm-4 control-label">Procedure Availed</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Procedure Availed" name="tumorregistry[ScreeningHistory][Procedure_Availed][]"/>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label class="col-sm-4 control-label">Result</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Procedure Result" name="tumorregistry[ScreeningHistory][Procedure_Result][]"/>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <label class="col-sm-4 control-label">Procedure Date</label>
                        <div class="col-sm-8">
                          <div class="input-group">
                            <i class="fa fa-calendar inner-icon"></i>
                            {!! Form::text('tumorregistry[ScreeningHistory][Procedure_Date][]',  getCurrentDate('m/d/Y'), ['class' => 'form-control datepicker', $read]); !!}
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label class="col-sm-4 control-label">Laboratory Done By</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Laboratory Done By" name="tumorregistry[ScreeningHistory][Laboratory_Done_By][]"/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <button id="removeScreening-button" type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Remove</button>
                  </div>
                </div>
              </div>              
            </div>
          </div>
        </div>
        <div class="box-footer">
          <button style= "padding-right:15px;" class="btn btn-success" id="addScreening-button" type="button">Add Screening</button> 
        </div>
      </div> 
      <!-- Patient History II end -->

      <!-- Cancer history start -->
      <div class="tab-pane" id="cancer_history_tab">
        <div class="row">
          <div class="col-md-12">
            <legend>Cancer Data History</legend>
            <label class="col-sm-2 control-label">Year of Diagnosis</label>
            <div class="col-sm-4">
              <input type="number" class="form-control" placeholder="Year of Diagnosis" name="tumorregistry[PatientHistory][Year_Of_Diagnosis]" value="<?php echo $data['year_of_diagnosis'] ?>"/>
            </div>
          </div>
        </div>

        &nbsp

        <div class="row">
          <div class="col-md-12">
          <table class="table table-striped table-bordered">
          <tr>
            <th style="width:11%;">Date Recorded</th>
            <th style="width:11%;">Primary Stage</th>
            <th style="width:11%;">Primary Site</th>
            <th style="width:11%;">Other Sites</th>
            <th style="width:11%;">Laterality</th>
            <th style="width:11%;">Histology</th>
            <th style="width:11%;">Staging</th>
            <th style="width:23%;">Sites of Distant Metastasis</th>
          </tr>
          <?php
          
          // dd($data['cancerData']);
          foreach ($data['cancerData'] as $key => $value) { 

            $multiplePrimaryLevelsValue = "N/A";
            switch ($value->multiple_primary_levels) {
              case "ZER":
                  $multiplePrimaryLevelsValue = "0";
                  break;
              case "ONE":
                  $multiplePrimaryLevelsValue = "1";
                  break;
              case "TWO":
                  $multiplePrimaryLevelsValue = "2";
                  break;
              case "THR":
                  $multiplePrimaryLevelsValue = "3";
                  break;
              case "FR":
                  $multiplePrimaryLevelsValue = "4";
                  break;
              default:
                  $multiplePrimaryLevelsValue = "N/A";
            }

            if($value->primary_sites != "Others") {
              $primarySitesValue = $value->primary_sites;
            }
            else {
              $primarySitesValue = $value->primary_sites_others;
            }

            // $other_source_treatment_array = "";
            parse_str($value->distant_metastasis_sites, $distant_metastasis_sites_array);

            // dd($other_source_treatment_initial_array['Distant_Metastasis_Sites']);
            // dd($value->other_source_treatment);

            $distant_metastasis_sites_value = "";
            if (isset($distant_metastasis_sites_array['Distant_Metastasis_Sites'])) {
              foreach ($distant_metastasis_sites_array['Distant_Metastasis_Sites'] as $site => $metastasis_value) {
              $distant_metastasis_sites_value .= $metastasis_value . ", ";
              }
            } else {
              $distant_metastasis_sites_value = "None";
            }
            
            // dd($distant_metastasis_sites_value);

            ?>
            <tr>
              <td style="width:11%;"><?php echo $value->created_at->format('F d\, Y'); ?></td>
              <td style="width:11%;"><?php echo "Stage " . $multiplePrimaryLevelsValue; ?></td>
              <td style="width:11%;"><?php echo $primarySitesValue; ?></td>
              <td style="width:11%;"><?php if($value->other_sites == null) {echo "None";} else {echo $value->other_sites;} ?></td>
              <td style="width:11%;"><?php echo $value->laterality; ?></td>
              <td style="width:11%;"><?php if($value->histology_morphology == null) {echo "None";} else {echo $value->histology_morphology;} ?></td>
              <td style="width:11%;"><?php if($value->staging == null) {echo "None";} else {echo $value->staging;} ?></td>
              <td style="width:23%;"><?php echo rtrim($distant_metastasis_sites_value, ', ') ?></td>
            </tr>
          <?php } ?>

        </table>
        <div class = "row">
          <div class="col-md-12">
          <legend>Multiple Primaries</legend>
            <label class="col-md-2 control-label">Primary Stage</label>
            <div class="col-md-4">
              <select name="Multiple_Primary_Levels" class="form-control" onchange='checkvalue(this.value)'>x
                <option value="ZER" <?php if($data['multiple_primary_levels'] == 'ZER') {echo 'selected';} ?>>Stage Zero</option>
                <option value="ONE" <?php if($data['multiple_primary_levels'] == 'ONE') {echo 'selected';} ?>>Stage One</option>
                <option value="TWO" <?php if($data['multiple_primary_levels'] == 'TWO') {echo 'selected';} ?>>Stage Two</option>
                <option value="THR" <?php if($data['multiple_primary_levels'] == 'THR') {echo 'selected';} ?>>Stage Three</option>
                <option value="FR" <?php if($data['multiple_primary_levels'] == 'FR') {echo 'selected';} ?>>Stage Four</option>
              </select>
            </div>
          </div>
        </div>
          <!-- </div> -->
        <div class="row">
          <div class="col-md-12">
          <!-- <div class="col-md-6"> -->
            <label class="col-md-2 control-label">Primary Site</label>
            <div class="col-md-4">
              <select name="Primary_Sites" class="form-control" onchange='checkvalue(this.value)'>
                <option value="None Selected" <?php if($data['primary_sites'] == 'None Selected') {echo 'selected';} ?>> --- Choose a Primary Site --- </option>
                <option <?php if($data['primary_sites'] == 'Bladder') {echo 'selected';} ?>>Bladder</option>
                <option <?php if($data['primary_sites'] == 'Blood') {echo 'selected';} ?>>Blood</option>
                <option <?php if($data['primary_sites'] == 'Brain') {echo 'selected';} ?>>Brain</option>
                <option <?php if($data['primary_sites'] == 'Breast') {echo 'selected';} ?>>Breast</option>
                <option <?php if($data['primary_sites'] == 'Colon') {echo 'selected';} ?>>Colon</option>
                <option <?php if($data['primary_sites'] == 'Corpus Uteri') {echo 'selected';} ?>>Corpus Uteri</option>
                <option <?php if($data['primary_sites'] == 'Esophagus') {echo 'selected';} ?>>Esophagus</option>
                <option <?php if($data['primary_sites'] == 'Gall') {echo 'selected';} ?>>Gall</option>
                <option <?php if($data['primary_sites'] == 'Kidney') {echo 'selected';} ?>>Kidney</option>
                <option <?php if($data['primary_sites'] == 'Liver') {echo 'selected';} ?>>Liver</option>
                <option <?php if($data['primary_sites'] == 'Lung') {echo 'selected';} ?>>Lung</option>
                <option <?php if($data['primary_sites'] == 'Nasopharnyx') {echo 'selected';} ?>>Nasopharnyx</option>
                <option <?php if($data['primary_sites'] == 'Oral Cavity') {echo 'selected';} ?>>Oral Cavity</option>
                <option <?php if($data['primary_sites'] == 'Ovary') {echo 'selected';} ?>>Ovary</option>
                <option <?php if($data['primary_sites'] == 'Pancreas') {echo 'selected';} ?>>Pancreas</option>
                <option <?php if($data['primary_sites'] == 'Prostate') {echo 'selected';} ?>>Prostate</option>
                <option <?php if($data['primary_sites'] == 'Rectum') {echo 'selected';} ?>>Rectum</option>
                <option <?php if($data['primary_sites'] == 'Skin') {echo 'selected';} ?>>Skin</option>
                <option <?php if($data['primary_sites'] == 'Stomach') {echo 'selected';} ?>>Stomach</option>
                <option <?php if($data['primary_sites'] == 'Testis') {echo 'selected';} ?>>Testis</option>
                <option <?php if($data['primary_sites'] == 'Thyroid') {echo 'selected';} ?>>Thyroid</option>
                <option <?php if($data['primary_sites'] == 'Urinary') {echo 'selected';} ?>>Urinary</option>
                <option <?php if($data['primary_sites'] == 'Uterine Cervix') {echo 'selected';} ?>>Uterine Cervix</option>
                <option <?php if($data['primary_sites'] == 'Others') {echo 'selected';} ?>>Others</option>
              </select>
            </div>
            <div class="col-md-4">
              <input type="text" class="form-control" placeholder = "Others" name="Primary_Sites_Others" id="Others_Field" style="display:none;">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <label class="col-sm-2 control-label">Other Sites</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" placeholder="Other Sites" name="Other_Sites" value="<?php echo $data['other_sites'] ?>"/>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
          <legend>Specifics</legend>
            <!-- <div class="form-group col-md-6"> -->
              <label class="col-md-2 control-label">Laterality</label>
              <div class="col-md-4">
                <select name="Laterality" class="form-control" onchange='checkvalue(this.value)'>
                  <option <?php if($data['laterality'] == 'LFT') {echo 'selected';} ?>>Left</option>
                  <option <?php if($data['laterality'] == 'RGT') {echo 'selected';} ?>>Right</option>
                  <option <?php if($data['laterality'] == 'NST') {echo 'selected';} ?>>Not Stated</option>
                  <option <?php if($data['laterality'] == 'BLT') {echo 'selected';} ?>>Bilateral</option>
                  <option <?php if($data['laterality'] == 'MLD') {echo 'selected';} ?>>Mild</option>
                </select>
              </div>
            <!-- </div> -->

            <!-- <div class="form-group col-md-6"> -->
              <label class="col-sm-2 control-label">Histology (Morphology)</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" placeholder="Histology (Morphology)" name="Histology_Morphology" value="<?php echo $data['histology_morphology'] ?>"/>
              </div>
            <!-- </div> -->
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <!-- <div class="form-group col-md-12"> -->
              <label class="col-md-2 control-label">Staging</label>
              <div class="col-md-10">
                <div class="btn-group toggler" data-toggle="buttons">
                  <label class="btn btn-default" onclick="CheckNone()">
                   <i class="fa fa-check"></i> <input value="In-Situ" type="radio" name="Staging" <?php if($data['staging'] == 'In-Situ') {echo 'checked=\'checked\' ';} ?>>In-Situ
                  </label>
                  <label class="btn btn-default" >
                   <i class="fa fa-check"></i> <input value="Localized" type="radio" name="Staging" <?php if($data['staging'] == 'Localized') {echo 'checked=\'checked\' ';} ?>>Localized
                  </label>   
                  <label class="btn btn-default" >
                   <i class="fa fa-check"></i> <input value="Direct Extension" type="radio" name="Staging" <?php if($data['staging'] == 'Direct Extension') {echo 'checked=\'checked\' ';} ?>>Direct Extension
                  </label>
                  <label class="btn btn-default" >
                   <i class="fa fa-check"></i> <input value="Regional Lymph Node" type="radio" name="Staging" <?php if($data['staging'] == 'Regional Lymph Node') {echo 'checked=\'checked\' ';} ?>>Regional Lymph Node
                  </label>
                  <label class="btn btn-default" >
                   <i class="fa fa-check"></i> <input value="3+4" type="radio" name="Staging" <?php if($data['staging'] == '3+4') {echo 'checked=\'checked\' ';} ?>>3+4
                  </label>       
                  <label class="btn btn-default" >
                   <i class="fa fa-check"></i> <input value="Distant Metastasis" type="radio" name="Staging" <?php if($data['staging'] == 'Distant Metastasis') {echo 'checked=\'checked\' ';} ?>>Distant Metastasis
                  </label>
                  <label class="btn btn-default" >
                   <i class="fa fa-check"></i> <input value="Unknown" type="radio" name="Staging" <?php if($data['staging'] == 'Unknown') {echo 'checked=\'checked\' ';} ?>>Unknown
                  </label>                         
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <div class="form-group col-md-12 icheck">
              <?php 
              if (json_decode($data['distant_metastasis_sites']) != null) {
                $distantSitesArray = json_decode($data['distant_metastasis_sites']);
              } else {
                $distantSitesArray = [];
              }
              ?>
              <label class="col-md-2 control-label">Sites of Distant Metastasis</label>
              <div class="col-sm-3">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="Distant_Metastasis_Sites[]" value="Unknown"> Unknown
                  </label>
                </div>     
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="Distant_Metastasis_Sites[]" value="Bone"> Bone
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="Distant_Metastasis_Sites[]" value="Liver"> Liver
                  </label>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="Distant_Metastasis_Sites[]" value="Lung (Pleura)"> Lung (Pleura)
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="Distant_Metastasis_Sites[]" value="Brain"> Brain
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="Distant_Metastasis_Sites[]" value="Ovary"> Ovary
                  </label>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="Distant_Metastasis_Sites[]" value="Skin"> Skin
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="Distant_Metastasis_Sites[]" value="Distant Lymph Node"> Distant Lymph Node
                  </label>
                </div>           
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
          
          <input type="button" id="CancerDataButton" class="btn btn-primary pull-left" value="Save Cancer Data">
        </div>

          </div>
        </div>
      </div> 
      <!-- Cancer history end -->
      <div class="tab-pane" id="treatment_regiment_tab">


        <legend>Treatment Regiment History</legend>
        <table class="table table-striped table-bordered">
          <tr>
            <th style="width:10%;">Treatment Number</th>
            <th style="width:12%;">Treatment Purpose</th>
            <th style="width:18%;">RAFI Primary Assistance</th>
            <th style="width:12%;">Date Given</th>
            <th style="width:24%;">Treatments received from RAFI</th>
            <th style="width:24%;">Treatments received from Other Sources</th>
          </tr>
          <?php
          // dd($data['treatmentRegiments']);
          foreach ($data['treatmentRegiments'] as $key => $value) { 

            $treatmentPurposeValue = "N/A";
            switch ($value->treatment_purpose) {
              case "CRC":
                  $treatmentPurposeValue = "Curative-Complete";
                  break;
              case "CRI":
                  $treatmentPurposeValue = "Curative-Incomplete";
                  break;
              case "PLL":
                  $treatmentPurposeValue = "Pallative";
                  break;
              case "OTH":
                  $treatmentPurposeValue = "Others";
                  break;
              default:
                  $treatmentPurposeValue = "N/A";
            }

          $rafi_ejacc_treatment_array = array();
          
          parse_str($value->rafi_ejacc_treatment, $rafi_ejacc_treatment_initial_array);

          $rafi_ejacc_treatment_value = "";
          if(isset($rafi_ejacc_treatment_initial_array['Rafi_Ejacc_Treatment'])) {
            foreach ($rafi_ejacc_treatment_initial_array['Rafi_Ejacc_Treatment'] as $rafi_treatment_key => $rafi_treatment_value) {
              $rafi_ejacc_treatment_value .= $rafi_treatment_value . ", ";
            }
          }
          else {
            $rafi_ejacc_treatment_value = "None";
          }

          parse_str($value->other_source_treatment, $other_source_treatment_initial_array);

          $other_source_treatment_value = "";
          if(isset($other_source_treatment_initial_array['Other_Source_Treatment'])) {
            foreach ($other_source_treatment_initial_array['Other_Source_Treatment'] as $other_treatment_key => $other_treatment_value) {
              $other_source_treatment_value .= $other_treatment_value . ", ";
            }
          }
          else {
            $other_source_treatment_value = "None";
          }

          if ($value->primary_assistance_given != null) {
            $primary_assistance_given_value = $value->primary_assistance_given;
          }
          else {
            $primary_assistance_given_value = "None";
          }

            ?>
            <tr>
              <td style="width:10%;"><?php echo $key+1; ?></td>
              <td style="width:12%;"><?php echo $treatmentPurposeValue; ?></td>
              <td style="width:18%;"><?php echo $primary_assistance_given_value; ?></td>
              <td style="width:12%;"><?php echo date('m/d/Y', strtotime($value->rafi_ejacc_date_given)); ?></td>
              <td style="width:24%; text-align:left;"><?php echo 
              rtrim($rafi_ejacc_treatment_value, ', '); ?></td>
              <td style="width:24%; text-align:left;"><?php echo rtrim($other_source_treatment_value, ', '); ?></td>
            </tr>
          <?php } ?>

        </table>

        <div class = "row">
          <div class="col-md-12">
            <legend>Treatment Regiment</legend>
            <!-- <div class="form-group col-md-12"> -->
              <label class="col-md-2 control-label">Treatment Purpose</label>
              <div class="col-md-10">
                <div class="btn-group toggler" data-toggle="buttons">
                  <label class="btn btn-default" >
                   <i class="fa fa-check"></i> <input type="radio" name="Treatment_Purpose" value="CRC" >Curative-Complete
                  </label>
                  <label class="btn btn-default" >
                   <i class="fa fa-check"></i> <input type="radio" name="Treatment_Purpose" value="CRI" >Curative-Incomplete
                  </label>   
                  <label class="btn btn-default" >
                   <i class="fa fa-check"></i> <input type="radio" name="Treatment_Purpose" value="PLL" >Pallative
                  </label>
                  <label class="btn btn-default" >
                   <i class="fa fa-check"></i> <input type="radio" name="Treatment_Purpose" value="OTH" >Others
                  </label>                
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <!-- <div class="form-group col-md-6"> -->
              <label class="col-sm-2 control-label">Primary Assistance Given by RAFI-EJACC</label>
              <div class="col-sm-4">
                  <input type="text" class="form-control" placeholder="Primary Assistance Given by RAFI-EJACC" name="Primary_Assistance_Given" value="<?php echo $data['primary_assistance_given'] ?>"/>
              </div>
            <!-- </div> -->
            <!-- div class="form-group col-md-6"> --> 
              <label class="col-sm-2 control-label">Date Given</label>
              <div class="col-sm-4">
                <div class="input-group">
                  <i class="fa fa-calendar inner-icon"></i>
                  {!! Form::text('Rafi_Ejacc_Date_Given', (empty($data['rafi_ejacc_date_given']) ? getCurrentDate('m/d/Y') : date('m/d/Y', strtotime($data['rafi_ejacc_date_given']))), ['class' => 'form-control', 'id'=>'datepicker', $read]); !!}
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <div class="form-group icheck">
              <?php 
              if (json_decode($data['rafi_ejacc_treatment']) != null) {
                $rafiEjaccArray = json_decode($data['rafi_ejacc_treatment']);
              } else {
                $rafiEjaccArray = [];
              }
              ?>
              <label class="col-md-2 control-label">Planned Additional/Adjuvant Treatment/s actually received from RAFI-EJACC</label>
              <div class="col-sm-3">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Rafi_Ejacc_Treatment[]" value="Surgery" > Surgery
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Rafi_Ejacc_Treatment[]" value="Radiotherapy" > Radiotherapy
                  </label>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Rafi_Ejacc_Treatment[]" value="Chemotherapy" > Chemotherapy
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Rafi_Ejacc_Treatment[]" value="Immunotherapy/Cyrotherapy" > Immunotherapy/Cyrotherapy
                  </label>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Rafi_Ejacc_Treatment[]" value="Hormonal" > Hormonal
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Rafi_Ejacc_Treatment[]" value="Unknown" > Unknown
                  </label>
                </div>       
              </div>
            </div>
          </div>
        </div>

        <div class = "row">
          <div class="col-md-12">
            <div class="form-group icheck">
              <?php 
              if (json_decode($data['other_source_treatment']) != null) {
                $otherSourceArray = json_decode($data['other_source_treatment']);
              } else {
                $otherSourceArray = [];
              }
              ?>
              <label class="col-md-2 control-label">Treatments received from Other Sources</label>
              <div class="col-sm-3">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Surgery"> Surgery
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Radiotherapy"> Radiotherapy
                  </label>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Chemotherapy"> Chemotherapy
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Immunotherapy/Cyrotherapy"> Immunotherapy/Cyrotherapy
                  </label>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Hormonal"> Hormonal
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Unknown"> Unknown
                  </label>
                </div>       
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
          
          <input type="button" id="TreatmentRegimentButton" class="btn btn-primary pull-left" value="Save Treatment Regiment">
        </div>

      </div>

      
    </div> <!-- tabcontent -->


  </div>
</div>

@section('scripts')

<script>
$(function(){
  $('#TreatmentRegimentButton').on('click', function(e){
    e.preventDefault();
    // alert("HEHHEHEH");
    // alert(jQuery("#hservice_id").val());
    var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
    var data={
      hservice_id: $('[name=hservice_id]').val(),
      trservice_id: $('[name=trservice_id]').val(),
      Treatment_Purpose: $('[name="Treatment_Purpose"]:checked').val(),
      Primary_Assistance_Given: $('[name="Primary_Assistance_Given"]').val(),
      Rafi_Ejacc_Date_Given: $('[name="Rafi_Ejacc_Date_Given"]').val(),
      Rafi_Ejacc_Treatment: $('[name="Rafi_Ejacc_Treatment[]"]').serialize(),
      Other_Source_Treatment: $('[name="Other_Source_Treatment[]"]').serialize()
    }

    $.ajax({
       headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
        url: '<?php echo site_url(); ?>plugin/call/TumorRegistry/TumorRegistry/saveTreatmentRegiment/'+healthcareservice_id_hehe,
        type: "POST",
        data:data,
        error: function() {
          $('#info').html('<p>An error has occurred</p>');
        },
        success: function(data) {
          console.log(data);
          // alert("SAVED");
          window.location.reload();
        }
    });
  });

  $('#CancerDataButton').on('click', function(e){
    e.preventDefault();

    var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
    var data={
      hservice_id: $('[name=hservice_id]').val(),
      trservice_id: $('[name=trservice_id]').val(),
      Multiple_Primary_Levels: $('[name="Multiple_Primary_Levels"]').val(),
      Primary_Sites: $('[name="Primary_Sites"]').val(),
      Primary_Sites_Others: $('[name="Primary_Sites_Others"]').val(),
      Other_Sites: $('[name="Other_Sites"]').val(),
      Laterality: $('[name="Laterality"]').val(),
      Histology_Morphology: $('[name="Histology_Morphology"]').val(),
      Staging: $('[name="Staging"]:checked').val(),
      Distant_Metastasis_Sites: $('[name="Distant_Metastasis_Sites[]"]:checked').serialize()
    }

    $.ajax({
       headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
        url: '<?php echo site_url(); ?>plugin/call/TumorRegistry/TumorRegistry/saveCancerDataAjax/'+healthcareservice_id_hehe,
        type: "POST",
        data:data,
        error: function() {
          $('#info').html('<p>An error has occurred</p>');
        },
        success: function(data) {
          console.log(data);
          // alert("SAVED");
          window.location.reload();
        }
    });
  });
});
</script>

{!! HTML::script('/plugins/TumorRegistry/Assets/TumorRegistry.js'); !!}
@stopjaxIsDisabled}} {{$isDisabled}} type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Surgery"> Surgery
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Radiotherapy"> Radiotherapy
                  </label>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Chemotherapy"> Chemotherapy
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Immunotherapy/Cyrotherapy"> Immunotherapy/Cyrotherapy
                  </label>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Hormonal"> Hormonal
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input {{$ajaxIsDisabled}} {{$isDisabled}} type="checkbox" id="H_Drug_Button" name="Other_Source_Treatment[]" value="Unknown"> Unknown
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
          @if ($isNewHealthcare === false && $isDisabled === "")
          <input type="button" id="TreatmentRegimentButton" class="btn btn-primary pull-left" value="Save Treatment Regiment">
          @elseif ($isNewHealthcare == true && $isDisabled === "")
          <input disabled type="button" class="btn btn-primary pull-left" value="Save Healthcare to Enable">
          @endif

        </div>

      </div>


    </div> <!-- tabcontent -->


  </div>
</div>

@section('plugin_jsscripts')

<script>
$(function(){
  $('#TreatmentRegimentButton').on('click', function(e){
    e.preventDefault();
    // alert("HEHHEHEH");
    // alert(jQuery("#hservice_id").val());
    var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
    var data={
      hservice_id: $('[name=hservice_id]').val(),
      trservice_id: $('[name=trservice_id]').val(),
      Treatment_Purpose: $('[name="Treatment_Purpose"]:checked').val(),
      Primary_Assistance_Given: $('[name="Primary_Assistance_Given"]').val(),
      Rafi_Ejacc_Date_Given: $('[name="Rafi_Ejacc_Date_Given"]').val(),
      Rafi_Ejacc_Treatment: $('[name="Rafi_Ejacc_Treatment[]"]').serialize(),
      Other_Source_Treatment: $('[name="Other_Source_Treatment[]"]').serialize()
    }

    $.ajax({
       headers: {
                'X-CSRF-TOKEN': $('span[name="_token"]').attr('content')
            },
        url: '<?php echo site_url(); ?>plugin/call/TumorRegistry/TumorRegistry/saveTreatmentRegiment/'+healthcareservice_id_hehe,
        type: "POST",
        data:data,
        error: function() {
          $('#info').html('<p>An error has occurred</p>');
        },
        success: function(data) {
          console.log(data);
          // alert("SAVED");
          window.location.reload();
        }
    });
  });

  $('#CancerDataButton').on('click', function(e){
    e.preventDefault();

    var healthcareservice_id_hehe = document.getElementById('hservice_id').value;
    var data={
      hservice_id: $('[name=hservice_id]').val(),
      trservice_id: $('[name=trservice_id]').val(),
      Multiple_Primary_Levels: $('[name="Multiple_Primary_Levels"]').val(),
      Primary_Sites: $('[name="Primary_Sites"]').val(),
      Primary_Sites_Others: $('[name="Primary_Sites_Others"]').val(),
      Other_Sites: $('[name="Other_Sites"]').val(),
      Laterality: $('[name="Laterality"]').val(),
      Histology_Morphology: $('[name="Histology_Morphology"]').val(),
      Staging: $('[name="Staging"]:checked').val(),
      Distant_Metastasis_Sites: $('[name="Distant_Metastasis_Sites[]"]:checked').serialize()
    }

    $.ajax({
       headers: {
                'X-CSRF-TOKEN': $('span[name="_token"]').attr('content')
            },
        url: '<?php echo site_url(); ?>plugin/call/TumorRegistry/TumorRegistry/saveCancerDataAjax/'+healthcareservice_id_hehe,
        type: "POST",
        data:data,
        error: function() {
          $('#info').html('<p>An error has occurred</p>');
        },
        success: function(data) {
          console.log(data);
          // alert("SAVED");
          window.location.reload();
        }
    });
  });
});
</script>

{!! HTML::script('/plugins/TumorRegistry/Assets/TumorRegistry.js'); !!}
@stop
