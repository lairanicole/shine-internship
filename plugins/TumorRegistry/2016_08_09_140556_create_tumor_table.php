<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTumorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (Schema::hasTable('tumor_registry_record')!=TRUE) {
        Schema::create('tumor_registry_record', function (Blueprint $table) {
          $table->increments('id');
          $table->string('tumor_registry_id', 60);
          $table->string('healthcareservice_id', 60);
          $table->string('tumor_registry_case_id', 60)->nullable();
          $table->string('physical_activity_type', 60)->nullable();
          $table->integer('minutes_per_exercise')->nullable();
          $table->integer('year_of_diagnosis')->nullable();
          $table->integer('number_of_children')->nullable();
          $table->enum('physical_activity_frequency', ['D','W','M','Q','Y'])->nullable();
          $table->enum('smoking_frequency', ['L','M'])->nullable();
          $table->enum('second_hand_smoke_exposure', ['W','WO'])->nullable();
          $table->integer('second_hand_smoke_years')->nullable();
          $table->string('alcoholic_beverages_type', 60)->nullable();
          $table->integer('alcoholic_beverages_amount')->nullable();
          $table->enum('alcoholic_beverages_unit_of_measure', ['B','G','S'])->nullable();
          $table->enum('alcoholic_beverages_frequency', ['D','W','M','Q','Y'])->nullable();
          $table->integer('age_started_drinking_alcohol')->nullable();
          $table->integer('no_of_years_drinking_alcohol')->nullable();
          $table->string('typical_diet_meat', 60)->nullable();
          $table->enum('typical_diet_meat_frequency', ['D','W','M','Q','Y'])->nullable();
          $table->string('typical_diet_grains', 60)->nullable();
          $table->enum('typical_diet_grains_frequency', ['D','W','M','Q','Y'])->nullable();
          $table->string('typical_diet_fruits_vegetables', 60)->nullable();
          $table->enum('typical_diet_fruits_vegetables_frequency', ['D','W','M','Q','Y'])->nullable();
          $table->integer('number_of_sexual_partners')->nullable();
          $table->string('onset_of_sexual_intercourse', 60)->nullable();
          $table->string('contraceptives_used', 60)->nullable();
          $table->integer('contraceptives_years_used')->nullable();
          $table->enum('menstrual_cycle_regularity', ['R','IR'])->nullable();
          $table->integer('menstrual_cycle_interval')->nullable();
          $table->integer('menopausal_age')->nullable();
          $table->integer('hpvi_year_examined')->nullable();
          $table->integer('hpi_year_examined')->nullable();
          $table->integer('hepa_b_year_examined')->nullable();
          $table->string('cancer_family_member_1_name', 60)->nullable();
          $table->string('cancer_family_member_1_relationship', 60)->nullable();
          $table->string('cancer_family_member_1_type', 60)->nullable();
          $table->longText('procedure_availed')->nullable();
          $table->longText('procedure_result')->nullable();
          $table->longText('procedure_date')->nullable();
          $table->longText('laboratory_done_by')->nullable();
          $table->string('referred_from', 60)->nullable();
          $table->string('reason_for_referral', 60)->nullable();
          $table->dateTime('date_of_consultation_admission')->nullable()->default(NULL)->nullable();
          $table->string('chief_complaint', 60)->nullable();
          $table->dateTime('date_of_diagnosis')->nullable()->default(NULL)->nullable();
          $table->string('non_microscopic_basis_of_diagnosis', 60)->nullable();
          $table->string('microscopic_basis_of_diagnosis', 60)->nullable();
          $table->enum('treatment_purpose', ['CRC','CRI','PLL','OTH'])->nullable();
          $table->string('primary_assistance_given', 60)->nullable();
          $table->dateTime('rafi_ejacc_date_given')->default(NULL)->nullable();
          $table->string('rafi_ejacc_treatment', 60)->nullable();
          $table->string('other_source_treatment', 60)->nullable();

          $table->softDeletes();
          $table->timestamps();
          $table->unique('tumor_registry_id');
        });
      }
      if (Schema::hasTable('tumor_registry_treatment_regiments_record')!=TRUE) {
        Schema::create('tumor_registry_treatment_regiments_record', function (Blueprint $table) {
          $table->increments('id');
          $table->string('tumor_registry_treatment_regiments_id', 60);
          $table->string('tumor_registry_case_id', 60);
          $table->string('tumor_registry_id', 60);
          $table->string('healthcareservice_id', 60)->nullable();
          $table->enum('treatment_purpose', ['CRC','CRI','PLL','OTH'])->nullable();
          $table->integer('primary_assistance_given')->nullable();
          $table->dateTime('rafi_ejacc_date_given')->default(NULL)->nullable();
          $table->longText('rafi_ejacc_treatment')->nullable();
          $table->longText('other_source_treatment')->nullable();
          

          $table->softDeletes();
          $table->timestamps();
          $table->unique('tumor_registry_treatment_regiments_id', 'tr_regiment_id');
        });
      }
      if (Schema::hasTable('tumor_registry_cancer_data_record')!=TRUE) {
        Schema::create('tumor_registry_cancer_data_record', function (Blueprint $table) {
          $table->increments('id');
          $table->string('tumor_registry_cancer_data_id', 60);
          $table->string('tumor_registry_case_id', 60);
          $table->string('tumor_registry_id', 60);
          $table->string('healthcareservice_id', 60);
          $table->enum('multiple_primary_levels', ['ZER','ONE','TWO','THR','FR'])->nullable();
          $table->string('primary_sites', 60)->nullable();
          $table->text('primary_sites_others', 60)->nullable();
          $table->text('other_sites', 60)->nullable();
          $table->string('laterality', 60)->nullable();
          $table->string('histology_morphology', 60)->nullable();
          $table->text('staging', 60)->nullable();
          $table->text('distant_metastasis_sites', 60)->nullable();

          $table->softDeletes();
          $table->timestamps();
          $table->unique('tumor_registry_cancer_data_id', "tr_cancer_id");
        });
      }

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
