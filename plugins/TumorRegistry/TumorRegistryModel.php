<?php

namespace Plugins\TumorRegistry;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class TumorRegistryModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tumor_registry_record';
    protected static $table_name = 'tumor_registry_record';
    protected $primaryKey = 'tumor_registry_id';

    protected $fillable = [];
    
    public function Healthcareservices() {
        return $this->belongsTo('ShineOS\Core\Healthcareservices\Entities\Healthcareservices', 'healthcareservice_id', 'healthcareservice_id');
    }

    public function treatmentRegiments()
    {
        DB::enableQueryLog();
        return $this->hasMany('Plugins\TumorRegistry\TumorRegistryTreatmentRegimentsModel','tumor_registry_case_id','tumor_registry_case_id');
    }

    public function cancerData()
    {
        DB::enableQueryLog();
        return $this->hasMany('Plugins\TumorRegistry\TumorRegistryCancerDataModel','tumor_registry_case_id','tumor_registry_case_id');
    }
}
