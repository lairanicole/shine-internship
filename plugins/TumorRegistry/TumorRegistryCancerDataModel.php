<?php

namespace Plugins\TumorRegistry;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TumorRegistryCancerDataModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tumor_registry_cancer_data_record';
    protected static $table_name = 'tumor_registry_cancer_data_record';
    protected $primaryKey = 'tumor_registry_cancer_data_id';

    protected $fillable = [];

    public function tumorRegistry()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\TumorRegistry\TumorRegistryModel','tumor_registry_case_id','tumor_registry_case_id');
    }


}
