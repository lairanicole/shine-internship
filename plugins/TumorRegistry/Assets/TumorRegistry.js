var TumorRegistry = {};


function CheckNone() {
    document.getElementById('Distant_Metastatis_Sites_None').prop('checked', true);
}

function checkvalue(val)
{
    if(val==="Others")
       document.getElementById('Others_Field').style.display='block';
    else
       document.getElementById('Others_Field').style.display='none'; 
}

TumorRegistry.cloneDiv = function ()
{
    $('.box-footer').on('click', '#addScreening-button',function (){
        // alert("HELLO");
        var count = $("#hiddenScreeningCounter").val();
        if (count == 0) {
            // alert(count);
            $("#hiddenScreeningCounter").val(++count);
            $('.cloneDivScreening').removeClass("hidden" );
        }
        else {
            $("#hiddenScreeningCounter").val(++count);
            //alert(count);
            Helper.cloneElement('.cloneDivScreening', $('.parentDivScreening'));
        };
        
    });

    $('.box-footer').on('click', '#addFamilyMember-button',function (){
        // alert("HELLO");
        var count = $("#hiddenFamilyCounter").val();
        if (count == 0) {
            // alert(count);
            $("#hiddenFamilyCounter").val(++count);
            $('.cloneDivFamilyMember').removeClass("hidden" );
        }
        else {
            $("#hiddenFamilyCounter").val(++count);
            //alert(count);
            Helper.cloneElement('.cloneDivFamilyMember', $('.parentFamilyMemberDiv'));
        };
        
    });
}

TumorRegistry.removeDiv = function ()
{
    //SCREENING REMOVE
    $('.parentDivScreening').on('click', '#removeScreening-button', function () {

        var count = $("#hiddenScreeningCounter").val();
        if (count == 1) {
            //alert(count);
            $("#hiddenScreeningCounter").val(--count);
            $('.cloneDivScreening').addClass( "hidden" );
        }
        else {
            $("#hiddenScreeningCounter").val(--count);
            //alert(count);
            $(this).closest('.cloneDivScreening').remove();
        };
    });

    $('.parentDivScreening').on('click', '#removeScreeningForLoop-button', function () {
        $(this).closest('.cloneDivForLoopScreening').remove();
    });


    //FAMILY MEMBER REMOVE
    $('.parentFamilyMemberDiv').on('click', '#removeFamilyMember-button', function () {

        var count = $("#hiddenFamilyCounter").val();
        if (count == 1) {
            //alert(count);
            $("#hiddenFamilyCounter").val(--count);
            $('.cloneDivFamilyMember').addClass( "hidden" );
        }
        else {
            $("#hiddenFamilyCounter").val(--count);
            //alert(count);
            $(this).closest('.cloneDivFamilyMember').remove();
        };
    });

    $('.parentFamilyMemberDiv').on('click', '#removeFamilyMemberForLoop-button', function () {
        $(this).closest('.forLoopFamilyMemberDiv').remove();
    });
}

$(function ()
{
   TumorRegistry.cloneDiv();
   TumorRegistry.removeDiv();
});
