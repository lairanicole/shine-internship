<?php

use src\ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Plugins\TumorRegistry\TumorRegistryModel as TumorRegistry;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

use Plugins\TumorRegistry\TumorRegistryTreatmentRegimentsModel as TumorRegistryTreatmentRegiments;
use Plugins\TumorRegistry\TumorRegistryCancerDataModel as TumorRegistryCancerData;
use Plugins\TumorRegistry\TumorRegistryModel as TumorRegistryMod;

class TumorRegistryController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
        $this->trcase_id = Input::get('trcase_id');
        $this->healthcareservice_id = Input::get('hservice_id');
        $this->trservice_id = Input::get('trservice_id');
        // $this->patient_id = Input::get('patient_id');
        // dd("HELLO");
    }

    public function index()
    {
        
    }

    public function save($data)
    {
        // dd($_POST);
      // dd($data);
      // $hello ="";
      $tumorregistry = TumorRegistryMod::where('healthcareservice_id', $this->healthcareservice_id)->first();

      if($this->trcase_id == NULL) //if there is no caseID given, this is a new maternal case
      {
          $tumorregistry->tumor_registry_case_id = $this->trcase_id =  IdGenerator::generateId();
          $tumorregistry->save();
      } else {
          //if this is a follow-up and caseID is given, assign caseID to this
          if($tumorregistry->tumor_registry_case_id == NULL)
          {
              $tumorregistry->tumor_registry_case_id = $this->trcase_id;
              $tumorregistry->save();
          }
      }

      foreach($data as $key=>$d)
      {
          // $hello .= $key . ", ";

          $EmptyTestArray = array_filter($d);
          // if($key == "ScreeningHistory") {
          //     $func = 'save'.ucfirst($key);
          //     // dd($d);
          //     $this->$func($d);
          // }
          if(!empty($EmptyTestArray) && $key != "TreatmentRegiment") {
              $func = 'save'.ucfirst($key);
              // dd($d);
              $this->$func($d);
          }
      }

      $hservice_id = Input::get('hservice_id');
      // dd($hello);
      $patient_id = getPatientIDByHealthcareserviceID($hservice_id);
      header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
      exit;
    }

    public function saveCancerDataAjax() {
      $hservice_id = Input::get('hservice_id');
      $tumor_registry_id = Input::get('trservice_id');


      // dd(Input::All());

      // dd(Input::get('Distant_Metastatis_Sites'));
      // dd(Input::get('Distant_Metastasis_Sites'));

      $tumor_registry_data = TumorRegistryMod::select('tumor_registry_case_id')->where('healthcareservice_id', $hservice_id)->first();
        // dd($tumor_registry_data->tumor_registry_case_id);
        $tr_cancer_data = TumorRegistryCancerData::where('tumor_registry_case_id', $this->trcase_id)->count();

      if ($tr_cancer_data > 0):
          $tumor_registry_cancer_data_record = TumorRegistryCancerData::where('tumor_registry_case_id', $this->tumor_registry_case_id)->first();
      else:
          $tumor_registry_cancer_data_record = new TumorRegistryCancerData();
          $tumor_registry_cancer_data_record->tumor_registry_cancer_data_id = IdGenerator::generateId();
          $tumor_registry_cancer_data_record->tumor_registry_case_id = $tumor_registry_data->tumor_registry_case_id;
      endif;


      // $tumor_registry_cancer_data_record = new TumorRegistryCancerData();
      // $tumor_registry_cancer_data_record->tumor_registry_cancer_data_id = IdGenerator::generateId();
      // $tumor_registry_cancer_data_record->tumor_registry_id = $tumor_registry_id;
      // dd(Input::get('Multiple_Primary_Levels'));
      $tumor_registry_cancer_data_record->tumor_registry_id = $tumor_registry_id;
      $tumor_registry_cancer_data_record->healthcareservice_id = $hservice_id;
      $tumor_registry_cancer_data_record->multiple_primary_levels = Input::get('Multiple_Primary_Levels');
      
      $tumor_registry_cancer_data_record->primary_sites = Input::get('Primary_Sites');
      $tumor_registry_cancer_data_record->other_sites = Input::get('Other_Sites');
      $tumor_registry_cancer_data_record->primary_sites_others = Input::get('Primary_Sites_Others');
      $tumor_registry_cancer_data_record->laterality = Input::get('Laterality');
      $tumor_registry_cancer_data_record->histology_morphology = Input::get('Histology_Morphology');
      $tumor_registry_cancer_data_record->staging = Input::get('Staging');

      
      $tumor_registry_cancer_data_record->distant_metastasis_sites = Input::get('Distant_Metastasis_Sites');

      $tumor_registry_cancer_data_record->save();

      $flash_message = 'Tumor Registry Case Report was Saved!';

      $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

      // dd($tumor_registry_record->smoking_frequency);

      header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
      exit;

    }

    public function saveTreatmentRegiment() {
      $hservice_id = Input::get('hservice_id');
        $tumor_registry_id = Input::get('trservice_id');
        // dd((new Datetime(Input::get('Rafi_Ejacc_Date_Given')))->format('Y-m-d H:i:s'));

        
        $tumor_registry_data = TumorRegistryMod::select('tumor_registry_case_id')->where('healthcareservice_id', $hservice_id)->first();
        // dd($tumor_registry_data->tumor_registry_case_id);
        $tr_treatment_data = TumorRegistryTreatmentRegiments::where('tumor_registry_case_id', $this->trcase_id)->count();

      if ($tr_treatment_data > 0):
          $tumor_registry_treatment_regiment_record = TumorRegistryTreatmentRegiments::where('tumor_registry_case_id', $this->tumor_registry_case_id)->first();
      else:
          $tumor_registry_treatment_regiment_record = new TumorRegistryTreatmentRegiments();
          $tumor_registry_treatment_regiment_record->tumor_registry_treatment_regiments_id = IdGenerator::generateId();
          $tumor_registry_treatment_regiment_record->tumor_registry_case_id = $tumor_registry_data->tumor_registry_case_id;
      endif;
        

        
      $tumor_registry_treatment_regiment_record->tumor_registry_id = $tumor_registry_id;
      $tumor_registry_treatment_regiment_record->healthcareservice_id = $hservice_id;


        $tumor_registry_treatment_regiment_record->treatment_purpose = Input::get('Treatment_Purpose');
        $tumor_registry_treatment_regiment_record->primary_assistance_given = Input::get('Primary_Assistance_Given');

        // dd((new Datetime(Input::get('Rafi_Ejacc_Date_Given')))->format('Y-m-d H:i:s'));
        
        $tumor_registry_treatment_regiment_record->rafi_ejacc_date_given = (new Datetime(Input::get('Rafi_Ejacc_Date_Given')))->format('Y-m-d H:i:s');
        $tumor_registry_treatment_regiment_record->rafi_ejacc_treatment =Input::get('Rafi_Ejacc_Treatment');
        $tumor_registry_treatment_regiment_record->other_source_treatment =Input::get('Other_Source_Treatment');
        // dd($tumor_registry_treatment_regiment_record);
        
        $tumor_registry_treatment_regiment_record->save();

      $flash_message = 'Tumor Registry Case Report was Saved!';

      $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

      // dd($tumor_registry_record->smoking_frequency);

      header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
      exit;

    }
    
    public function saveScreeningHistory($data)
    {
      $sHistory = $data;
      // dd($sHistory);
      //dd(Input::all());
      $tumor_registry_id = Input::get('trservice_id');
      $hservice_id = Input::get('hservice_id');

      $tumor_registry_record = TumorRegistry::find($tumor_registry_id);
      if(isset($sHistory['Non_Microscopic_Basis_Of_Diagnosis'])) {
        $tumor_registry_record->non_microscopic_basis_of_diagnosis = json_encode($sHistory['Non_Microscopic_Basis_Of_Diagnosis']);
      }

      if(isset($sHistory['Microscopic_Basis_Of_Diagnosis'])) {
        $tumor_registry_record->microscopic_basis_of_diagnosis = json_encode($sHistory['Microscopic_Basis_Of_Diagnosis']);
      }
      $procedureDateArray = array();

      // dd($sHistory);


      foreach ($sHistory['Procedure_Date'] as $key => $date) {
        $procedureDateArray[] = json_encode((new Datetime($date))->format('Y-m-d H:i:s'));
      }

      // dd($sHistory['Procedure_Result'][0]);

      $procedure_availed_to_save = [];
      foreach ($sHistory['Procedure_Availed'] as $key => $value) {
        if($sHistory['Procedure_Availed'][$key] != "" || $sHistory['Procedure_Result'][$key] != "" || $sHistory['Laboratory_Done_By'][$key] != "") {
          array_push($procedure_availed_to_save, $value);
        }
      }
      $tumor_registry_record->procedure_availed = json_encode($procedure_availed_to_save);

      $procedure_result_to_save = [];
      foreach ($sHistory['Procedure_Result'] as $key => $value) {
        if($sHistory['Procedure_Availed'][$key] != "" || $sHistory['Procedure_Result'][$key] != "" || $sHistory['Laboratory_Done_By'][$key] != "") {
          array_push($procedure_result_to_save, $value);
        }
      }
      $tumor_registry_record->procedure_result = json_encode($procedure_result_to_save);

      $procedure_date_to_save = [];
      foreach ($procedureDateArray as $key => $value) {
        if($sHistory['Procedure_Availed'][$key] != "" || $sHistory['Procedure_Result'][$key] != "" || $sHistory['Laboratory_Done_By'][$key] != "") {
          array_push($procedure_date_to_save, $value);
        }
      }
      $tumor_registry_record->procedure_date = json_encode($procedure_date_to_save);

      $laboratory_done_to_save = [];
      foreach ($sHistory['Laboratory_Done_By'] as $key => $value) {
        if($sHistory['Procedure_Availed'][$key] != "" || $sHistory['Procedure_Result'][$key] != "" || $sHistory['Laboratory_Done_By'][$key] != "") {
          array_push($laboratory_done_to_save, $value);
        }
      }
      $tumor_registry_record->laboratory_done_by = json_encode($laboratory_done_to_save);

      $tumor_registry_record->save();

      // $flash_message = 'Tumor Registry Case Report was Saved!';

      // $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

      // // dd($tumor_registry_record->smoking_frequency);

      // header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
      // exit;
    }

    public function saveFamilyHistory($data)
    {
      $fHistory = $data;
      // dd($fHistory);
      $tumor_registry_id = Input::get('trservice_id');
      $hservice_id = Input::get('hservice_id');

      $tumor_registry_record = TumorRegistry::find($tumor_registry_id);
      // dd(Input::get('Cancer_Family_Member_1_Name')[0]);

      $family_member_name_to_save = [];
      foreach ($fHistory['Cancer_Family_Member_1_Name'] as $key => $value) {
        if($fHistory['Cancer_Family_Member_1_Name'][$key] != "" || $fHistory['Cancer_Family_Member_1_Relationship'][$key] != "" || $fHistory['Cancer_Family_Member_1_Type'][$key] != "") {
          array_push($family_member_name_to_save, $value);
        }
      }
      $tumor_registry_record->cancer_family_member_1_name = json_encode($family_member_name_to_save);

      $family_member_relationship_to_save = [];
      foreach ($fHistory['Cancer_Family_Member_1_Relationship'] as $key => $value) {
        if($fHistory['Cancer_Family_Member_1_Name'][$key] != "" || $fHistory['Cancer_Family_Member_1_Relationship'][$key] != "" || $fHistory['Cancer_Family_Member_1_Type'][$key] != "") {
          array_push($family_member_relationship_to_save, $value);
        }
      }
      $tumor_registry_record->cancer_family_member_1_relationship = json_encode($family_member_relationship_to_save);

      $family_member_type_to_save = [];
      foreach ($fHistory['Cancer_Family_Member_1_Type'] as $key => $value) {
        if($fHistory['Cancer_Family_Member_1_Name'][$key] != "" || $fHistory['Cancer_Family_Member_1_Relationship'][$key] != "" || $fHistory['Cancer_Family_Member_1_Type'][$key] != "") {
          array_push($family_member_type_to_save, $value);
        }
      }
      $tumor_registry_record->cancer_family_member_1_type = json_encode($family_member_type_to_save);


      $tumor_registry_record->save();

      // $flash_message = 'Tumor Registry Case Report was Saved!';

      // $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

      // // dd($tumor_registry_record->smoking_frequency);

      // header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
      // exit;
    }

    public function savePatientHistory($data)
    {

      $pHistory = $data;
      $tumor_registry_id = Input::get('trservice_id');
      $hservice_id = Input::get('hservice_id');

      $tumor_registry_record = TumorRegistry::find($tumor_registry_id);

      $tumor_registry_record->physical_activity_type = $pHistory['Physical_Activity_Type'];
      
      if ($pHistory['Minutes_Per_Exercise'] == NULL) {
        $tumor_registry_record->minutes_per_exercise = NULL;
      }
      else {
        $tumor_registry_record->minutes_per_exercise = $pHistory['Minutes_Per_Exercise'];
      }

      
      $tumor_registry_record->physical_activity_frequency = $pHistory['Physical_Activity_Frequency'];

      if ($pHistory['Year_Of_Diagnosis'] != NULL) {
        $tumor_registry_record->year_of_diagnosis = $pHistory['Year_Of_Diagnosis'];
      }
      else {
        $tumor_registry_record->year_of_diagnosis = NULL;
      }
      
      if ($pHistory['Number_Of_Children'] != NULL) {
        $tumor_registry_record->number_of_children = $pHistory['Number_Of_Children'];
      }
      else {
        $tumor_registry_record->number_of_children = NULL;
      }

      if(isset($pHistory['Smoking_Frequency'])) {
        $tumor_registry_record->smoking_frequency = $pHistory['Smoking_Frequency'];
      }

      if(isset($pHistory['Second_Hand_Smoke_Exposure'])) {
        $tumor_registry_record->second_hand_smoke_exposure = $pHistory['Second_Hand_Smoke_Exposure'];
      }

      if ($pHistory['Second_Hand_Smoke_Years'] != NULL) {
        $tumor_registry_record->second_hand_smoke_years = $pHistory['Second_Hand_Smoke_Years'];
      }
      else {
        $tumor_registry_record->second_hand_smoke_years = NULL;
      }
      
      $tumor_registry_record->alcoholic_beverages_type = $pHistory['Alcoholic_Beverages_Type'];


      if ($pHistory['Alcoholic_Beverages_Amount'] != NULL) {
        $tumor_registry_record->alcoholic_beverages_amount = $pHistory['Alcoholic_Beverages_Amount'];
      }
      else {
        $tumor_registry_record->alcoholic_beverages_amount = NULL;
      }


      if(isset($pHistory['Alcoholic_Beverages_Unit_Of_Measure'])) {
        $tumor_registry_record->alcoholic_beverages_unit_of_measure = $pHistory['Alcoholic_Beverages_Unit_Of_Measure'];
      }

      if ($pHistory['Age_Started_Drinking_Alcohol'] != NULL) {
        $tumor_registry_record->age_started_drinking_alcohol = $pHistory['Age_Started_Drinking_Alcohol'];
      }
      else {
        $tumor_registry_record->age_started_drinking_alcohol = NULL;
      }

      if ($pHistory['No_Of_Years_Drinking_Alcohol'] != NULL) {
        $tumor_registry_record->no_of_years_drinking_alcohol = $pHistory['No_Of_Years_Drinking_Alcohol'];
      }
      else {
        $tumor_registry_record->no_of_years_drinking_alcohol = NULL;
      }

      $tumor_registry_record->alcoholic_beverages_frequency = $pHistory['Alcoholic_Beverages_Frequency'];
      
      

      if(isset($pHistory['Typical_Diet_Meat'])) {
        $tumor_registry_record->typical_diet_meat = json_encode($pHistory['Typical_Diet_Meat']);
      }

      $tumor_registry_record->typical_diet_meat_frequency = $pHistory['Typical_Diet_Meat_Frequency'];

      if(isset($pHistory['Typical_Diet_Grains'])) {
        $tumor_registry_record->typical_diet_grains = json_encode($pHistory['Typical_Diet_Grains']);
      }
      
      $tumor_registry_record->typical_diet_grains_frequency = $pHistory['Typical_Diet_Grains_Frequency'];

      if(isset($pHistory['Typical_Diet_Fruits_Vegetables'])) {
        $tumor_registry_record->typical_diet_fruits_vegetables = json_encode($pHistory['Typical_Diet_Fruits_Vegetables']);
      }

      $tumor_registry_record->typical_diet_fruits_vegetables_frequency = $pHistory['Typical_Diet_Fruits_Vegetables_Frequency'];
      
      if ($pHistory['Number_Of_Sexual_Partners'] != NULL) {
        $tumor_registry_record->number_of_sexual_partners = $pHistory['Number_Of_Sexual_Partners'];
      }
      else {
        $tumor_registry_record->number_of_sexual_partners = NULL;
      }

      
      $tumor_registry_record->onset_of_sexual_intercourse = $pHistory['Onset_Of_Sexual_Intercourse'];
      $tumor_registry_record->contraceptives_used = $pHistory['Contraceptives_Used'];
      
      if ($pHistory['Contraceptives_Years_Used'] != NULL) {
        $tumor_registry_record->contraceptives_years_used = $pHistory['Contraceptives_Years_Used'];
      }
      else {
        $tumor_registry_record->contraceptives_years_used = NULL;
      }

      

      if(isset($pHistory['Menstrual_Cycle_Regularity'])) {
        $tumor_registry_record->menstrual_cycle_regularity = $pHistory['Menstrual_Cycle_Regularity'];
      }
      
      if ($pHistory['Menstrual_Cycle_Interval'] != NULL) {
        $tumor_registry_record->menstrual_cycle_interval = $pHistory['Menstrual_Cycle_Interval'];
      }
      else {
        $tumor_registry_record->menstrual_cycle_interval = NULL;
      }

      if ($pHistory['Menopausal_Age'] != NULL) {
        $tumor_registry_record->menopausal_age = $pHistory['Menopausal_Age'];
      }
      else {
        $tumor_registry_record->menopausal_age = NULL;
      }


      if ($pHistory['Hpvi_Year_Examined'] != NULL) {
        $tumor_registry_record->hpvi_year_examined = $pHistory['Hpvi_Year_Examined'];
      }
      else {
        $tumor_registry_record->hpvi_year_examined = NULL;
      }

      if ($pHistory['Hpi_Year_Examined'] != NULL) {
        $tumor_registry_record->hpi_year_examined = $pHistory['Hpi_Year_Examined'];
      }
      else {
        $tumor_registry_record->hpi_year_examined = NULL;
      }

      if ($pHistory['Hepa_B_Year_Examined'] != NULL) {
        $tumor_registry_record->hepa_b_year_examined = $pHistory['Hepa_B_Year_Examined'];
      }
      else {
        $tumor_registry_record->hepa_b_year_examined = NULL;
      }

      $tumor_registry_record->referred_from = $pHistory['Referred_From'];
      $tumor_registry_record->reason_for_referral = $pHistory['Reason_For_Referral'];
      $tumor_registry_record->date_of_consultation_admission = (new Datetime($pHistory['Date_Of_Consultation_Admission']))->format('Y-m-d H:i:s');
      $tumor_registry_record->chief_complaint = $pHistory['Chief_Complaint'];
      $tumor_registry_record->date_of_diagnosis = (new Datetime($pHistory['Date_Of_Diagnosis']))->format('Y-m-d H:i:s');

      $tumor_registry_record->save();
    }

    

    public function view()
    {
        
    }

    public static function update()
    {

    }
}
