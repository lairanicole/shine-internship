<?php
$plugin_id = 'Dengue';                      //plugin ID
$plugin_module = 'healthcareservices';      //module owner
$plugin_location = 'dropdown';              //UI location where plugin will be accessible
$plugin_primaryKey = 'dengue_id';           //primary_key used to find data
$plugin_table = 'dengue_record';            //plugintable default; table_name custom table
$plugin_tabs_child = array('addservice', 'complaints', 'vitals', 'dengue_plugin', 'impanddiag', 'medicalorders', 'disposition'); //,
$plugin_type = 'program';
$plugin_gender = 'all';

$plugin_relationship = array();
$plugin_folder = 'Dengue'; //module owner
$plugin_title = 'Dengue';            //plugin title
$plugin_description = 'Dengue';
$plugin_version = '1.0';
$plugin_developer = 'ShineLabs';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2016";


$plugin_tabs = [
    'addservice' => 'Basic Information',
    'disposition' => 'Disposition',
    'medicalorders' => 'Medical Orders',
    'vitals' => 'Vitals & Physical',
    'dengue_plugin' => 'Dengue',
    'impanddiag' => 'Impressions & Diagnosis',
    'complaints' => 'Complaints'
];

$plugin_tabs_models = [
    'complaints' => 'GeneralConsultation',
    'disposition' => 'Disposition',
    'medicalorders' => 'MedicalOrder',
    'vitals' => 'VitalsPhysical',
    'dengue_plugin' => 'DengueModel'
];

 