<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMigrationDengue extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string', 'integer', 'datetime');
        
        if (Schema::hasTable('dengue_record')!=TRUE) {
            Schema::create('dengue_record', function (Blueprint $table) {
                $table->increments('id');
                $table->string('dengue_id', 32);
                $table->string('healthcareservice_id', 32);
                $table->enum('fever_lasting', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('fever_now', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('platelets_critical', ['Y','N','U'])->default('U')->nullable()->change();
                $table->integer('platelet_count')->default(NULL)->nullable()->change();
                $table->enum('rapid_weak_pulse', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('pallor_cool_skin', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('chills', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('rash', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('headache', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('eye_pain', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('body_pain', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('joint_pain', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('anorexia', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('tourniquet_test', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('petechiae', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('purpura_ecchymosis', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('vomit_with_blood', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('blood_in_stool', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('nasal_bleeding', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('vaginal_bleeding', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('positive_urinalysis', ['Y','N','U'])->default('U')->nullable()->change();
                $table->integer('lowest_hematocrit')->default(NULL)->nullable()->change();
                $table->integer('highest_hematocrit')->default(NULL)->nullable()->change();
                $table->integer('lowest_serum_albumin')->default(NULL)->nullable()->change();
                $table->integer('lowest_serum_protein')->default(NULL)->nullable()->change();
                $table->integer('lowest_pulse_pressure')->default(NULL)->nullable()->change();
                $table->integer('lowest_wbc_count')->default(NULL)->nullable()->change();
                $table->enum('persistent_vomiting', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('abdominal_pain_tenderness', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('mucosal_bleeding', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('lethargy_restlessness', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('liver_enlargement', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('pleural_or_abdominal_effusion', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('diarrhea', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('cough', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('conjunctivitis', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('nasal_congestion', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('sore_throat', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('jaundice', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('convulsion_or_coma', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('nausea_and_vomiting', ['Y','N','U'])->default('U')->nullable()->change();
                $table->enum('arthritis', ['Y','N','U'])->default('U')->nullable()->change();
                $table->tinyInteger('is_submitted')->default(0);
                
                $table->softDeletes();
                $table->timestamps();                            
                $table->unique('dengue_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }

}