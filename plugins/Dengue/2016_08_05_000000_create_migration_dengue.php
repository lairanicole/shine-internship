<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMigrationDengue extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string', 'integer', 'datetime');
        
        if (Schema::hasTable('dengue_record')!=TRUE) {
            Schema::create('dengue_record', function (Blueprint $table) {
                $table->increments('id');
                $table->string('dengue_id', 32);
                $table->string('healthcareservice_id', 32);
                $table->enum('fever_lasting', ['Y','N','U'])->default('U')->nullable();
                $table->enum('fever_now', ['Y','N','U'])->default('U')->nullable();
                $table->enum('platelets_critical', ['Y','N','U'])->default('U')->nullable();
                $table->integer('platelet_count')->default(NULL)->nullable();
                $table->enum('rapid_weak_pulse', ['Y','N','U'])->default('U')->nullable();
                $table->enum('pallor_cool_skin', ['Y','N','U'])->default('U')->nullable();
                $table->enum('chills', ['Y','N','U'])->default('U')->nullable();
                $table->enum('rash', ['Y','N','U'])->default('U')->nullable();
                $table->enum('headache', ['Y','N','U'])->default('U')->nullable();
                $table->enum('eye_pain', ['Y','N','U'])->default('U')->nullable();
                $table->enum('body_pain', ['Y','N','U'])->default('U')->nullable();
                $table->enum('joint_pain', ['Y','N','U'])->default('U')->nullable();
                $table->enum('anorexia', ['Y','N','U'])->default('U')->nullable();
                $table->enum('tourniquet_test', ['Y','N','U'])->default('U')->nullable();
                $table->enum('petechiae', ['Y','N','U'])->default('U')->nullable();
                $table->enum('purpura_ecchymosis', ['Y','N','U'])->default('U')->nullable();
                $table->enum('vomit_with_blood', ['Y','N','U'])->default('U')->nullable();
                $table->enum('blood_in_stool', ['Y','N','U'])->default('U')->nullable();
                $table->enum('nasal_bleeding', ['Y','N','U'])->default('U')->nullable();
                $table->enum('vaginal_bleeding', ['Y','N','U'])->default('U')->nullable();
                $table->enum('positive_urinalysis', ['Y','N','U'])->default('U')->nullable();
                $table->integer('lowest_hematocrit')->default(NULL)->nullable();
                $table->integer('highest_hematocrit')->default(NULL)->nullable();
                $table->integer('lowest_serum_albumin')->default(NULL)->nullable();
                $table->integer('lowest_serum_protein')->default(NULL)->nullable();
                $table->integer('lowest_pulse_pressure')->default(NULL)->nullable();
                $table->integer('lowest_wbc_count')->default(NULL)->nullable();
                $table->enum('persistent_vomiting', ['Y','N','U'])->default('U')->nullable();
                $table->enum('abdominal_pain_tenderness', ['Y','N','U'])->default('U')->nullable();
                $table->enum('mucosal_bleeding', ['Y','N','U'])->default('U')->nullable();
                $table->enum('lethargy_restlessness', ['Y','N','U'])->default('U')->nullable();
                $table->enum('liver_enlargement', ['Y','N','U'])->default('U')->nullable();
                $table->enum('pleural_or_abdominal_effusion', ['Y','N','U'])->default('U')->nullable();
                $table->enum('diarrhea', ['Y','N','U'])->default('U')->nullable();
                $table->enum('cough', ['Y','N','U'])->default('U')->nullable();
                $table->enum('conjunctivitis', ['Y','N','U'])->default('U')->nullable();
                $table->enum('nasal_congestion', ['Y','N','U'])->default('U')->nullable();
                $table->enum('sore_throat', ['Y','N','U'])->default('U')->nullable();
                $table->enum('jaundice', ['Y','N','U'])->default('U')->nullable();
                $table->enum('convulsion_or_coma', ['Y','N','U'])->default('U')->nullable();
                $table->enum('nausea_and_vomiting', ['Y','N','U'])->default('U')->nullable();
                $table->enum('arthritis', ['Y','N','U'])->default('U')->nullable();
                $table->tinyInteger('is_submitted')->default(0);
                
                $table->softDeletes();
                $table->timestamps();                            
                $table->unique('dengue_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dengue_record');
    }

}