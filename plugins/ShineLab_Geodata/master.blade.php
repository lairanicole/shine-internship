@extends('patients::layouts.inner')
@section('header-content') Facility GeoData @stop
@section('patient-title') Editing Profile @stop

@section('patient-content')
<div class="nav-tabs-custom">
    @include('pluginform::geodata')
</div>
@stop
