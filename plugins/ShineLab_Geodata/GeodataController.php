<?php

use ShineOS\Core\Facilities\Entities\Facilities;
use Plugins\ShineLab_Geodata\GeodataModel;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use Shine\Repositories\Contracts\UserRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\Libraries\Utils as Utils;
use Shine\User;
use Shine\Plugin; 

class GeodataController extends Controller
{

    protected $moduleName = 'Facilities';
    protected $modulePath = 'facilities';

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
        $this->middleware('auth');
    }

    public function index() { 
        //no index
    }

    public function save()
    {
        $id = Input::get('facility_id');
        $brgycode =Input::get('brgycode'); 
        $checks = geodataModel::where('facility_id', $id)->where('brgycode', $brgycode)->first();

        if($checks) {
            $geodata = $checks;
        } else {
            $geodata = new GeodataModel();
        }
        // dd(Input::all());
        
        $geodata->facility_geodata_id = IdGenerator::generateId();
        $geodata->facility_id = Input::get('facility_id');
        $geodata->brgycode = $brgycode;
        $geodata->population = Input::get('population');
        $geodata->barangay = Input::get('barangay');
        $geodata->households = Input::get('households');
        $geodata->health_centers = Input::get('health_centers');
        $geodata->bhs = Input::get('bhs');
        $geodata->households_water_1 = Input::get('households_water_1');
        $geodata->households_water_2 = Input::get('households_water_2');
        $geodata->households_water_3 = Input::get('households_water_3');
        $geodata->households_toilet = Input::get('households_toilet');
        $geodata->households_solid_waste = Input::get('households_solid_waste');
        $geodata->households_sanitary = Input::get('households_sanitary');
        $geodata->food_establishments = Input::get('food_establishments');
        $geodata->food_establishments_permit = Input::get('food_establishments_permit');
        $geodata->food_handlers = Input::get('food_handlers');
        $geodata->food_handlers_permit = Input::get('food_handlers_permit');
        $geodata->salt_samples = Input::get('salt_samples');
        $geodata->salt_samples_iodine = Input::get('salt_samples_iodine');
        $geodata->save();

        Session::flash('message', 'Geodata data has been successfully added or updated.');
        header('Location: '.url().'/facilities#Geodata');
        exit;
    } 

    public function view($id)
    {
        $facility = Facilities::where('facility_id','=', $id)->first();
        $plugdata = GeodataModel::where('facility_id',$id)->first();

        View::addNamespace('pluginform', plugins_path().'ShineLab_Geodata');
        echo View::make('pluginform::master', array('plugdata'=>$plugdata))->render();

    }

    public function del($brgy_code)
    {
        $thisfacility = Session::get('facility_details')->facility_id;
        $delete = geodataModel::where('facility_id', $thisfacility)->where('brgycode', $brgy_code)->delete();
        if($delete) {
            Session::flash('message', 'Geodata data has been successfully added or updated.');
        } else {
            Session::flash('error', 'Failed to delete geodata');
        }
        header('Location: '.url().'/facilities#Geodata');
        exit;
        // dd($brgy_code,$thisfacility);
    }
}
