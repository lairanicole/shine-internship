<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GeodataTableUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('facility_geodata')==TRUE) {

            if (Schema::hasColumn('facility_geodata', 'health_centers')!=TRUE) {
              Schema::table('facility_geodata', function (Blueprint $table) {
                $table->string('health_centers', 32)->nullable();
              });
            }

            if (Schema::hasColumn('facility_geodata', 'brgycode')!=TRUE) {
              Schema::table('facility_geodata', function (Blueprint $table) {
                $table->string('brgycode', 32)->nullable(); 
              });
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
