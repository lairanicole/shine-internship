<?php

namespace Plugins\ShineLab_Geodata;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class GeodataModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'facility_geodata';
    protected static $table_name = 'facility_geodata';
    protected $primaryKey = 'facility_id';

    protected $touches = array('Facility');

    public function Facility() {
        return $this->belongsTo('ShineOS\Core\Facilities\Entities\Facilities', 'facility_id', 'facility_id');
    }
}
