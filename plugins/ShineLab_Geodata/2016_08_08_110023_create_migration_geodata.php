<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMigrationGeodata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('facility_geodata')!=TRUE) {
            Schema::create('facility_geodata', function (Blueprint $table) {
                $table->increments('id');
                $table->string('facility_geodata_id', 32);
                $table->string('facility_id', 32);

                $table->string('population', 32)->nullable();
                $table->string('barangay', 32)->nullable();
                $table->string('households', 32)->nullable();
                $table->string('bhs', 32)->nullable();
                $table->string('households_water_1', 32)->nullable();
                $table->string('households_water_2', 32)->nullable();
                $table->string('households_water_3', 32)->nullable();   
                $table->string('households_toilet', 32)->nullable();
                $table->string('households_solid_waste', 32)->nullable();
                $table->string('households_sanitary', 32)->nullable();
                $table->string('food_establishments', 32)->nullable();
                $table->string('food_establishments_permit', 32)->nullable();
                $table->string('food_handlers', 32)->nullable();
                $table->string('food_handlers_permit', 32)->nullable();
                $table->string('salt_samples', 32)->nullable();
                $table->string('salt_samples_iodine', 32)->nullable();

                $table->softDeletes();
                $table->timestamps();
                $table->unique('facility_geodata_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('facility_geodata');       //
    }
}
