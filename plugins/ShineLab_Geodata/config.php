<?php

$plugin_module = 'facilities';            //plugin parent module
$plugin_title = 'Environmental Information';            //plugin title
$plugin_id = 'Geodata';              //plugin ID
$plugin_location = 'tab';           //UI location where plugin will be accessible
$plugin_primaryKey = 'facility_id';      //primary_key used to find data
$plugin_table = 'facility_geodata';           //plugintable default; table_name custom table
$plugin_description = 'The Facility Geodata Plugin. Capture geodata of a facility using this plugin. This is a simple example of data capture plugins. This plugin also creates its own DB Table.';
$plugin_version = '1.0';
$plugin_developer = 'ShineLabs';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2016";
$plugin_folder = 'ShineLab_Geodata';
