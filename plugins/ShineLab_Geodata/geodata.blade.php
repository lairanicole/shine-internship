<?php 
$population = $households = $barangay = $bhs = $health_centers = $households_water_1 = $households_water_2 = $households_water_3 = $households_toilet = $households_solid_waste = $households_sanitary = $food_establishments = $food_establishments_permit = $food_handlers = $food_handlers_permit = $salt_samples = $salt_samples_iodine = NULL; 
$geo_barangay = $geo_barangay_1 = $geo_barangay_2 = $geo_barangay_3= array();  
?>
<div class="tab-content">
<div id="Geodata" class="tab-pane step active">
<div class="box-body">
    <h4>Environmental Information</h4> <small><b>(For RHUs, CHOs, MHCs and MHOs:</b> Add/Update your covered barangay health stations in <b>Catchment Area tab</b> before filling out this area.)</small>

@if(count($currentFacility->facility_catchment_area)>0 AND $currentFacility->facility_catchment_area!=NULL) 
    @foreach($currentFacility->facility_catchment_area as $cffc_k => $cffc_v)
        <?php
        $geo_barangay_1[$cffc_k]['barangay_code'] = $cffc_v->bhs_brgy_code;
        $geo_barangay_1[$cffc_k]['barangay_name'] = getBrgyName($cffc_v->bhs_brgy_code);
        ?>
    @endforeach 

    <?php 
    if($brgy_data['geodatabarangay']!=NULL) {
        $geo_barangay_2 = array();
        foreach ($brgy_data['geodatabarangay'] as $key => $value) { 
            $geo_barangay_2[$key]['barangay_code'] = $value['brgycode'];
            $geo_barangay_2[$key]['barangay_name'] = getBrgyName($value['brgycode']);
        }
    }

    if($currentFacility->facility_contact->barangay!=NULL) {
        $geo_barangay_3[0]['barangay_code'] = $currentFacility->facility_contact->barangay;
        $geo_barangay_3[0]['barangay_name'] = getBrgyName($currentFacility->facility_contact->barangay);
    }
    $geo_barangay_merge = array_merge($geo_barangay_1,$geo_barangay_2,$geo_barangay_3);
    $geo_barangay = array_map("unserialize", array_unique(array_map("serialize", $geo_barangay_merge)));

    ?>
@else
    @if(array_key_exists('brgys', $brgy_data))
    @if($brgy_data['brgys']!=NULL)
        @foreach($brgy_data['brgys'] as $bdb_k => $bdb_v)
            <?php
            $geo_barangay[$bdb_k]['barangay_code'] = $bdb_v->barangay_code;
            $geo_barangay[$bdb_k]['barangay_name'] = $bdb_v->barangay_name;
            ?>
        @endforeach 
    @endif
    @endif
@endif
<?php //dd($geo_barangay); ?>
{!! Form::open(array('url' => 'facilities/', 'method'=>'POST') ) !!}
<fieldset>
    <div class="form-group">
        @if($geo_barangay!=NULL) 
        <label class="col-sm-2 control-label">Select Barangay</label>
        <div class="col-sm-4">
            <select name="geodatabarangay_select" class="form-control geodatabarangay_select" id="geodatabarangay_select">
                <option value="" selected="selected"> Select a Barangay </option>
                @foreach($geo_barangay as $brgy_k => $brgy_v)
                    <option value="{{$brgy_v['barangay_code']}}">{{ $brgy_v['barangay_name'] }}</option>
                @endforeach
            </select>
        </div>
        <!-- <input type="submit" class="btn btn-primary" value="View data"> -->
        @else
        <div class="alert alert-error"> 
            <p>No Barangay found. Please update your Facility contact address then try to logout and login again. Thank you!</p>
        </div>  
        @endif
    </div>
</fieldset>
{!! Form::close() !!}


<div id="geo_form_hidden" style="display: none;">
<!-- <hr> -->
<code> Showing barangay<i><label id="barangayName" class="control-label"></label></i>environmental information</code>
{!! Form::model($currentFacility, array('url' => 'plugin/call/ShineLab_Geodata/geodata/save/'.$currentFacility->facility_id,'class'=>'form-horizontal')) !!}
<fieldset>
    <input type="hidden" name="facility_id" id="facility_id" value="{{ $currentFacility->facility_id }}" />
    <input type="hidden" name="brgycode" id="brgycode" value="" />
    <legend>Population</legend>
    <div class="form-group">
        <label class="col-sm-2 control-label">Population</label>
        <div class="col-sm-4">
            <input type="number" class="form-control aplha" placeholder="Population" name="population" id="geo_population" value="{{ $population or NULL }}" />
        </div>
        <label class="col-sm-2 control-label">Barangay</label>
        <div class="col-sm-4">
            <input type="number" class="form-control aplha" placeholder="Barangay" name="barangay" id="geo_barangay" value="{{ $barangay or NULL }}" />
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Households</legend>
    <div class="form-group">
        <label class="col-sm-2 control-label">No. of Households</label>
        <div class="col-sm-4">
            <input type="number" class="form-control alphanumeric" placeholder="No. of Households" name="households" id="geo_households" value="{{ $households or NULL }}" />
        </div>
        <label class="col-sm-2 control-label">No. of BHS</label>
        <div class="col-sm-4">
            <input type="number" class="form-control alphanumeric" placeholder="No. of BHS" name="bhs" id="geo_bhs" value="{{ $bhs or NULL }}" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">No. of Health Centers</label>
        <div class="col-sm-4">
            <input type="number" class="form-control alphanumeric" placeholder="No. of Health Centers" name="health_centers" id="geo_health_centers" value="{{ $health_centers or NULL }}" />
        </div>
    </div>
    <br clear="all" />
    <br clear="all" />
    <div class="form-group">
        <div class="col-sm-6">
            <strong>Households with access to improved or safe water supply</strong><hr />
            <label class="control-label">Level I Point source</label>
            <div class="">
                <input type="number" class="form-control alphanumeric" placeholder="Number of households with level I Point source" name="households_water_1" id="geo_households_water_1" value="{{ $households_water_1 or NULL }}" />
            </div>
            <br clear="all" />
            <label class="control-label">Level II Communal Faucet System or Standpost</label>
            <div class="">
                <input type="number" class="form-control alphanumeric" placeholder="Number of households with level II Communal Faucet System or Standpost" name="households_water_2" id="geo_households_water_2" value="{{ $households_water_2 or NULL }}" />
            </div>
            <br clear="all" />
            <label class="control-label">Level III Waterworks System</label>
            <div class="">
                <input type="number" class="form-control alphanumeric" placeholder="Number of households with level III Waterworks System" name="households_water_3" id="geo_households_water_3" value="{{ $households_water_3 or NULL }}" />
            </div>
            <br clear="all" />
        </div>
        <div class="col-sm-6">
            <strong>Sanitation of households</strong><hr />
            <label class="control-label">Households with sanitary toilet facilities</label>
            <div class="">
                <input type="number" class="form-control alphanumeric" placeholder="Households with sanitary toilet facilities" name="households_toilet" id="geo_households_toilet" value="{{ $households_toilet or NULL }}" />
            </div>
            <br clear="all" />
            <label class="control-label">Households with satisfactory disposal of solid waste</label>
            <div class="">
                <input type="number" class="form-control alphanumeric" placeholder="Households with satisfactory disposal of solid waste" name="households_solid_waste" id="geo_households_solid_waste" value="{{ $households_solid_waste or NULL }}" />
            </div>
            <br clear="all" />
            <label class="control-label">Households with complete basic sanitary facilities</label>
            <div class="">
                <input type="number" class="form-control alphanumeric" placeholder="Households with complete basic sanitary facilities" name="households_sanitary" id="geo_households_sanitary" value="{{ $households_sanitary or NULL }}" />
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Food</legend>
    <div class="form-group">
        <div class="col-md-6">
        <label class="control-label">Food establishments</label>
        <div class="">
            <input type="number" class="form-control alphanumeric" placeholder="No. of food establishments" name="food_establishments" id="geo_food_establishments" value="{{ $food_establishments or NULL }}" />
        </div>
        </div>
        <div class="col-md-6">
        <label class="control-label">Food establishments with sanitary permit</label>
        <div class="">
            <input type="number" class="form-control alphanumeric" placeholder="No. of food establishments with sanitary permit" name="food_establishments_permit" id="geo_food_establishments_permit" value="{{ $food_establishments_permit or NULL }}" />
        </div>
        </div>
    </div>
    <br clear="all" />
    <div class="form-group">

        <div class="col-md-6">
        <label class="control-label">Food handlers</label>
        <div class="">
            <input type="number" class="form-control alphanumeric" placeholder="No. of food handlers" name="food_handlers" id="geo_food_handlers" value="{{ $food_handlers or NULL }}" />
        </div>
        </div>
        <div class="col-md-6">
        <label class="control-label">Food handlers with health certificate</label>
        <div class="">
            <input type="number" class="form-control alphanumeric" placeholder="No. of food handlers with health certificate" name="food_handlers_permit" id="geo_food_handlers_permit" value="{{ $food_handlers_permit or NULL }}" />
        </div>
        </div>
    </div>
    <br clear="all" />
    <div class="form-group">

        <div class="col-md-6">
        <label class="control-label">Salt samples tested</label>
        <div class="">
            <input type="number" class="form-control alphanumeric" placeholder="No. of salt samples tested" name="salt_samples" id="geo_salt_samples" value="{{ $salt_samples or NULL }}"/>
        </div>
        </div>
        <div class="col-md-6">
        <label class="control-label">Salt samples tested (+) for iodine</label>
        <div class="">
            <input type="number" class="form-control alphanumeric" placeholder="salt samples tested (+) for iodine" name="salt_samples_iodine" id="geo_salt_samples_iodine" value="{{ $salt_samples_iodine or NULL }}" />
        </div>
        </div>
    </div>
</fieldset>
<div class="form-group pull-right ">
    <button type="submit" value="submit" class="btn btn-success"> <i class="fa fa-save-o"></i>Save </button>
    <!-- <button type="button" class="btn btn-primary" onclick="location.href='{{ url('facilities') }}'">Close</button> -->
    <!-- <button type="button" class="btn btn-danger" onclick="linkDelete()">Delete</button> -->
    <a href="#" onclick="linkDelete()" type="button" class="btn btn-danger btn-flat"><i class="fa fa-trash-o"></i> Delete</a>
</div>
<br clear="all" />
{!! Form::close() !!}
</div>
</div>
</div>
@section('scripts') 
<script type="text/javascript">
var geodatabarangay = <?php echo json_encode($brgy_data['geodatabarangay']); ?>;
var url="{{ url('/plugin/call/ShineLab_Geodata/geodata/del/') }}";
var facility_id = $("#facility_id").val();

$(document).ready(function(){
    $('#geodatabarangay_select').on('change', function() {
        $("#geo_form_hidden").show();
        $("#brgycode").val($(this).find("option:selected").attr("value"));
        var val_selected = $(this).find("option:selected").attr("value");
        var index = geodatabarangay.contains(val_selected); 
        document.getElementById("barangayName").innerHTML = $(this).find("option:selected").text();
        if(val_selected=='') {
            $("#geo_form_hidden").hide();
        }
        
        if(index) { 
            $("#geo_population").val(geodatabarangay[index]["population"]);
            $("#geo_barangay").val(geodatabarangay[index]["barangay"]);
            $("#geo_households").val(geodatabarangay[index]["households"]);
            $("#geo_bhs").val(geodatabarangay[index]["bhs"]);
            $("#geo_health_centers").val(geodatabarangay[index]["health_centers"]);
            $("#geo_households_water_1").val(geodatabarangay[index]["households_water_1"]);
            $("#geo_households_water_2").val(geodatabarangay[index]["households_water_2"]);
            $("#geo_households_water_3").val(geodatabarangay[index]["households_water_3"]);
            $("#geo_households_toilet").val(geodatabarangay[index]["households_toilet"]);
            $("#geo_households_solid_waste").val(geodatabarangay[index]["households_solid_waste"]);
            $("#geo_households_sanitary").val(geodatabarangay[index]["households_sanitary"]);
            $("#geo_food_establishments").val(geodatabarangay[index]["food_establishments"]);
            $("#geo_food_establishments_permit").val(geodatabarangay[index]["food_establishments_permit"]);
            $("#geo_food_handlers").val(geodatabarangay[index]["food_handlers"]);
            $("#geo_food_handlers_permit").val(geodatabarangay[index]["food_handlers_permit"]);
            $("#geo_salt_samples").val(geodatabarangay[index]["salt_samples"]);
            $("#geo_salt_samples_iodine").val(geodatabarangay[index]["salt_samples_iodine"]); 
        } else {
            $("#geo_population").val('');
            $("#geo_barangay").val('');
            $("#geo_households").val('');
            $("#geo_bhs").val('');
            $("#geo_health_centers").val('');
            $("#geo_households_water_1").val('');
            $("#geo_households_water_2").val('');
            $("#geo_households_water_3").val('');
            $("#geo_households_toilet").val('');
            $("#geo_households_solid_waste").val('');
            $("#geo_households_sanitary").val('');
            $("#geo_food_establishments").val('');
            $("#geo_food_establishments_permit").val('');
            $("#geo_food_handlers").val('');
            $("#geo_food_handlers_permit").val('');
            $("#geo_salt_samples").val('');
            $("#geo_salt_samples_iodine").val(''); 
        }

        
        

    }); 

    //SORTING
    var options = $('select.geodatabarangay_select option');
    var arr = options.map(function(_, o) { return { t: $(o).text(), v: o.value }; }).get();
    arr.sort(function(o1, o2) { return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0; });
    options.each(function(i, o) {
      o.value = arr[i].v;
      $(o).text(arr[i].t);
    });
});
Array.prototype.contains = function (needle) {
   for (var i in this) {
       for(var key in this[i])  {
            if(this[i][key] == needle ){
                return i;
            }
        }
   }
   return false;
}

function linkDelete() {
  if (confirm('Are you sure you want to delete the data of this barangay?')) {
    window.location.href=url+'/'+$("#brgycode").val();
  }
  return false;
}
</script>
@stop