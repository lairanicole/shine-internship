<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeaslesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
         if (Schema::hasTable('measles_record')!=TRUE) {
            Schema::create('measles_record', function (Blueprint $table) {
                $table->increments('id');
                $table->string('measles_id', 32);
                $table->string('healthcareservice_id', 32);
                $table->enum('signs_rashes', ['Y','N','U'])->default('U')->nullable();
                $table->dateTime('rash_onset')->default(NULL)->nullable();
                $table->integer('rash_duration')->default(NULL)->nullable();
                $table->enum('generalized_rash', ['Y','N','U'])->default('U')->nullable();
                $table->string('origin_on_body', 40)->default(NULL)->nullable();
                $table->string('direction_of_spread', 40)->default(NULL)->nullable();
                $table->enum('signs_fever', ['Y','N','U'])->default('U')->nullable();
                $table->integer('temperature')->default(NULL)->nullable();
                $table->enum('temperature_skin', ['H','W','N'])->default('N');
                $table->enum('cough', ['Y','N','U'])->default('U')->nullable();
                $table->enum('runny_nose_coryza', ['Y','N','U'])->default('U')->nullable();
                $table->enum('conjunctivitis', ['Y','N','U'])->default('U')->nullable();
                $table->enum('kopliks_spots', ['Y','N','U'])->default('U')->nullable();
                $table->text('notes')->default(NULL)->nullable();
                $table->enum('hospitalized', ['Y','N','U'])->default('U')->nullable();
                $table->integer('days_hospitalized')->default(NULL)->nullable();
                $table->enum('pneumonia', ['Y','N','U'])->default('U')->nullable();
                $table->enum('encephalitis', ['Y','N','U'])->default('U')->nullable();
                $table->text('other_complications')->default(NULL)->nullable();
                $table->tinyInteger('is_submitted')->default(0);

                $table->softDeletes();
                $table->timestamps();                            
                $table->unique('measles_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
