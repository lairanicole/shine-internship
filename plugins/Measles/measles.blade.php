
<?php
    $hservice_id = $data['healthcareservice_id'];
    $mservice_id = $data['measles_id'];
    //dd($data);
?>

<?php
if($data['is_submitted'] == false) { $read = ''; }
else { $read = 'disabled'; }
?>

<div class="tab-content">

<div id="measles" class="tab-pane step active">
  <!-- if there's family planning background - make this uneditable proceed to Family Planning -->
{!! Form::model($data, array('url' => 'plugin/call/Measles/Measles/save/'.$hservice_id,'class'=>'form-horizontal')) !!}
{!! Form::hidden('mservice_id',$mservice_id) !!}
{!! Form::hidden('hservice_id',$hservice_id) !!}
  
  <div class="col-md-12">
      <legend>Signs and Symptoms</legend>

      <!-- RASHES -->
      <legend><small>Rashes</small></legend>
      <div class="row">
        <div class="form-group col-md-6">
          <label class="col-md-4 control-label">Does the patient show signs of rashes?</label>
          <div class="col-sm-8">
              <div class="btn-group toggler" data-toggle="buttons">
                <label class="btn btn-default <?php if($data['signs_rashes'] == 'Y') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Signs_Rashes" value="Y" <?php if($data['signs_rashes'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
                </label>
                <label class="btn btn-default <?php if($data['signs_rashes'] == 'N') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Signs_Rashes" value="N" <?php if($data['signs_rashes'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
                </label>
                <label class="btn btn-default <?php if($data['signs_rashes'] == 'U') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Signs_Rashes" value="U" <?php if($data['signs_rashes'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
                </label>
              </div>
          </div>
        </div>
      </div>

      <div id="rash_group" class="row">
        <div class="form-group col-md-6">
          <label class="col-sm-4 control-label">Rash Onset</label>
          <div class="col-sm-8">
              {!! Form::text('Rash_Onset', $data['rash_onset'], array('class' => 'form-control','placeholder'=>'Rash Onset', 'type' => 'date', $read)) !!}
          </div>
        </div>
        <div class="form-group col-md-6">
          <label class="col-md-4 control-label">Duration (Days)</label>
            <div class="col-md-8">
              <input type="number" class="form-control" name="Rash_Duration" placeholder="Duration (Days)" value="<?php echo $data['rash_duration'] ?>" {{$read}}>
            </div>
        </div>
        <div class="form-group col-md-6">
          <label class="col-md-4 control-label">Generalized Rash</label>
          <div class="col-sm-8">
              <div class="btn-group toggler" data-toggle="buttons">
                <label class="btn btn-default <?php if($data['generalized_rash'] == 'Y') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Generalized_Rash" value="Y" <?php if($data['generalized_rash'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
                </label>
                <label class="btn btn-default <?php if($data['generalized_rash'] == 'N') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Generalized_Rash" value="N" <?php if($data['generalized_rash'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
                </label>
                <label class="btn btn-default <?php if($data['generalized_rash'] == 'U') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Generalized_Rash" value="U" <?php if($data['generalized_rash'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
                </label>
              </div>
          </div>
        </div>
        <div class="form-group col-md-6">
          <label class="col-md-4 control-label">Origin on Body</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="Origin_On_Body" placeholder="Origin on Body" value="<?php echo $data['origin_on_body'] ?>" {{$read}}>
            </div>
        </div>
        <div class="form-group col-md-6">
          <label class="col-md-4 control-label">Direction of Spread</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="Direction_Of_Spread" placeholder="Direction of Spread" value="<?php echo $data['direction_of_spread'] ?>" {{$read}}>
            </div>
        </div>
      </div>


      <!-- Fever -->
      <legend><small>Fever</small></legend>
      <div class="row">
        <div class="form-group col-md-6">
          <label class="col-md-4 control-label">Does the patient show signs of fever?</label>
          <div class="col-sm-8">
              <div class="btn-group toggler" data-toggle="buttons">
                <label class="btn btn-default <?php if($data['signs_fever'] == 'Y') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Signs_Fever" value="Y" <?php if($data['signs_fever'] == 'Y') {echo 'checked=\'checked\' ';} ?> onClick="$('#rash_group').removeClass('hidden');"> Yes
                </label>
                <label class="btn btn-default <?php if($data['signs_fever'] == 'N') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Signs_Fever" value="N" <?php if($data['signs_fever'] == 'N') {echo 'checked=\'checked\' ';} ?> onClick="$('#rash_group').removeClass('hidden');"> No
                </label>
                <label class="btn btn-default <?php if($data['signs_fever'] == 'U') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Signs_Fever" value="U" <?php if($data['signs_fever'] == 'U') {echo "checked=\'checked\' ";} ?> onClick="$('#rash_group').removeClass('hidden');"> Unknown
                </label>
              </div>
          </div>
        </div>
        
        <div class="form-group col-md-6">
          <label class="col-md-4 control-label">If temperature is not taken, skin is</label>
          <div class="col-sm-8">
              <div class="btn-group toggler" data-toggle="buttons">
                <label class="btn btn-default <?php if($data['temperature_skin'] == 'H') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Temperature_Skin" value="H" <?php if($data['temperature_skin'] == 'H') {echo 'checked=\'checked\' ';} ?> onClick="$('#rash_group').removeClass('hidden');"> Hot
                </label>
                <label class="btn btn-default <?php if($data['temperature_skin'] == 'W') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Temperature_Skin" value="W" <?php if($data['temperature_skin'] == 'W') {echo 'checked=\'checked\' ';} ?> onClick="$('#rash_group').removeClass('hidden');"> Warm
                </label>
                <label class="btn btn-default <?php if($data['temperature_skin'] == 'N') {echo 'active';} ?>" {{$read}}>
                  <i class="fa fa-check"></i> <input type="radio" name="Temperature_Skin" value="N" <?php if($data['temperature_skin'] == 'N') {echo "checked=\'checked\' ";} ?> onClick="$('#rash_group').removeClass('hidden');"> Normal
                </label>
              </div>
          </div>          
        </div>
      </div>


        <!-- Other -->
      <legend><small>Other Symptoms</small></legend>
      <div class="form-group col-md-6">
        <label class="col-md-4 control-label">Cough</label>
        <div class="col-sm-8">
            <div class="btn-group toggler" data-toggle="buttons">
              <label class="btn btn-default <?php if($data['cough'] == 'Y') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Cough" value="Y" <?php if($data['cough'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
              </label>
              <label class="btn btn-default <?php if($data['cough'] == 'N') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Cough" value="N" <?php if($data['cough'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
              </label>
              <label class="btn btn-default <?php if($data['cough'] == 'U') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Cough" value="U" <?php if($data['cough'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
              </label>
            </div>
        </div>          
      </div>
      <div class="form-group col-md-6">
        <label class="col-md-4 control-label">Runny Nose (Coryza)</label>
        <div class="col-sm-8">
            <div class="btn-group toggler" data-toggle="buttons">
              <label class="btn btn-default <?php if($data['runny_nose_coryza'] == 'Y') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Runny_Nose_Coryza" value="Y" <?php if($data['runny_nose_coryza'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
              </label>
              <label class="btn btn-default <?php if($data['runny_nose_coryza'] == 'N') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Runny_Nose_Coryza" value="N" <?php if($data['runny_nose_coryza'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
              </label>
              <label class="btn btn-default <?php if($data['runny_nose_coryza'] == 'U') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Runny_Nose_Coryza" value="U" <?php if($data['runny_nose_coryza'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
              </label>
            </div>
        </div>          
      </div>
      <div class="form-group col-md-6">
        <label class="col-md-4 control-label">Conjunctivitis</label>
        <div class="col-sm-8">
            <div class="btn-group toggler" data-toggle="buttons">
              <label class="btn btn-default <?php if($data['conjunctivitis'] == 'Y') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Conjunctivitis" value="Y" <?php if($data['conjunctivitis'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
              </label>
              <label class="btn btn-default <?php if($data['conjunctivitis'] == 'N') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Conjunctivitis" value="N" <?php if($data['conjunctivitis'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
              </label>
              <label class="btn btn-default <?php if($data['conjunctivitis'] == 'U') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Conjunctivitis" value="U" <?php if($data['conjunctivitis'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
              </label>
            </div>
        </div>          
      </div>
      <div class="form-group col-md-6">
        <label class="col-md-4 control-label">Koplik's Spots</label>
        <div class="col-sm-8">
            <div class="btn-group toggler" data-toggle="buttons">
              <label class="btn btn-default <?php if($data['kopliks_spots'] == 'Y') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Kopliks_Spots" value="Y" <?php if($data['kopliks_spots'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
              </label>
              <label class="btn btn-default <?php if($data['kopliks_spots'] == 'N') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Kopliks_Spots" value="N" <?php if($data['kopliks_spots'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
              </label>
              <label class="btn btn-default <?php if($data['kopliks_spots'] == 'U') {echo 'active';} ?>" {{$read}}>
                <i class="fa fa-check"></i> <input type="radio" name="Kopliks_Spots" value="U" <?php if($data['kopliks_spots'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
              </label>
            </div>
        </div>          
      </div>
      <div class="form-group col-md-12">
        <label class="col-md-2 control-label">Notes</label>
        <div class="col-md-8 ui-widget">
          {!! Form::textarea('Notes', $data['notes'], ['class' => 'form-control noresize', 'placeholder' => 'Other Complaints/Symptoms found', 'cols'=>'10', 'rows'=>'5', $read]) !!}
        </div>
      </div>
  </div>

  <div class="col-md-12">
    <legend>Complications</legend>
    <div class="form-group col-md-6">
      <label class="col-md-4 control-label">Hospitalized</label>
      <div class="col-sm-8">
          <div class="btn-group toggler" data-toggle="buttons">
            <label class="btn btn-default <?php if($data['hospitalized'] == 'Y') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Hospitalized" value="Y" <?php if($data['hospitalized'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
            </label>
            <label class="btn btn-default <?php if($data['hospitalized'] == 'N') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Hospitalized" value="N" <?php if($data['hospitalized'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
            </label>
            <label class="btn btn-default <?php if($data['hospitalized'] == 'U') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Hospitalized" value="U" <?php if($data['hospitalized'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
            </label>
          </div>
      </div>          
    </div>
    <div class="form-group col-md-6">
      <label class="col-md-4 control-label">Days hospitalized</label>
        <div class="col-md-8">
          <input type="number" class="form-control" name="Days_Hospitalized" placeholder="Days Hospitalized" value="<?php echo $data['days_hospitalized'] ?>" {{$read}}>
        </div>
    </div>
    <div class="form-group col-md-6">
      <label class="col-md-4 control-label">Pneumonia</label>
      <div class="col-sm-8">
          <div class="btn-group toggler" data-toggle="buttons">
            <label class="btn btn-default <?php if($data['pneumonia'] == 'Y') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Pneumonia" value="Y" <?php if($data['pneumonia'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
            </label>
            <label class="btn btn-default <?php if($data['pneumonia'] == 'N') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Pneumonia" value="N" <?php if($data['pneumonia'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
            </label>
            <label class="btn btn-default <?php if($data['pneumonia'] == 'U') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Pneumonia" value="U" <?php if($data['pneumonia'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
            </label>
          </div>
      </div>          
    </div>
    <div class="form-group col-md-6">
      <label class="col-md-4 control-label">Encephalitis</label>
      <div class="col-sm-8">
          <div class="btn-group toggler" data-toggle="buttons">
            <label class="btn btn-default <?php if($data['encephalitis'] == 'Y') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Encephalitis" value="Y" <?php if($data['encephalitis'] == 'Y') {echo 'checked=\'checked\' ';} ?>> Yes
            </label>
            <label class="btn btn-default <?php if($data['encephalitis'] == 'N') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Encephalitis" value="N" <?php if($data['encephalitis'] == 'N') {echo 'checked=\'checked\' ';} ?>> No
            </label>
            <label class="btn btn-default <?php if($data['encephalitis'] == 'U') {echo 'active';} ?>" {{$read}}>
              <i class="fa fa-check"></i> <input type="radio" name="Encephalitis" value="U" <?php if($data['encephalitis'] == 'U') {echo "checked=\'checked\' ";} ?>> Unknown
            </label>
          </div>
      </div>          
    </div>
    <div class="form-group col-md-12">
        <label class="col-md-2 control-label">Other Complications</label>
        <div class="col-md-8 ui-widget">
          {!! Form::textarea('Other_Complications', $data['other_complications'], ['class' => 'form-control noresize', 'placeholder' => 'Describe other complications experienced by patient', 'cols'=>'10', 'rows'=>'5', $read]) !!}
        </div>
      </div>
  </div>
 <!--  <div class="row">
    <legend>Epidemiological Exposure History</legend>
    <div class="checkbox">
      <label>
        <input type="checkbox" name="alert[]" id="chk_disab" value="DISAB">
        Has Disability
      </label>
    </div>
  </div> -->

  <?php if($data['is_submitted'] == false) { ?>
    <div class="form-group pull-right">
        <!--<button type="button" class="btn btn-primary" onclick="#'">Close</button>-->
          <button type="submit" value="submit" class="btn btn-success">Submit</button>
    </div>
  <?php } ?>

  {!! Form::close() !!}
  <br clear="all" />
</div>
</div>
