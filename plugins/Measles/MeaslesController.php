<?php

use src\ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use Plugins\Measles\MeaslesModel as Measles;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

class MeaslesController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        //no index
    }

    public function save()
    {
        $measles_id = Input::get('mservice_id');
        $hservice_id = Input::get('hservice_id');

        $measles_record = Measles::find($measles_id);

        $measles_record->signs_rashes = Input::get('Signs_Rashes');
        $measles_record->rash_onset = Input::get('Rash_Onset');
        
        if (Input::get('Rash_Duration') == NULL) {
            $measles_record->rash_duration = NULL;
        }
        else {
            $measles_record->rash_duration = Input::get('Rash_Duration');
        }


        $measles_record->generalized_rash = Input::get('Generalized_Rash');
        $measles_record->origin_on_body = Input::get('Origin_On_Body');
        $measles_record->direction_of_spread = Input::get('Direction_Of_Spread');
        $measles_record->signs_fever = Input::get('Signs_Fever');

        if (Input::get('Temperature') == NULL) {
            $measles_record->temperature = NULL;
        }
        else {
            $measles_record->temperature = Input::get('Temperature');
        }


        $measles_record->temperature_skin = Input::get('Temperature_Skin');
        $measles_record->cough = Input::get('Cough');
        $measles_record->runny_nose_coryza = Input::get('Runny_Nose_Coryza');
        $measles_record->conjunctivitis = Input::get('Conjunctivitis');
        $measles_record->kopliks_spots = Input::get('Kopliks_Spots');
        $measles_record->notes = Input::get('Notes');
        $measles_record->hospitalized = Input::get('Hospitalized');
    
        if (Input::get('Days_Hospitalized') == NULL) {
            $measles_record->days_hospitalized = NULL;
        }
        else {
            $measles_record->days_hospitalized = Input::get('Days_Hospitalized');
        }
        

        $measles_record->pneumonia = Input::get('Pneumonia');
        $measles_record->encephalitis = Input::get('Encephalitis');
        $measles_record->other_complications = Input::get('Other_Complications');
        $measles_record->is_submitted = true;

        $measles_record->save();

        $flash_message = 'Measles Case Report was Saved!';

        $patient_id = getPatientIDByHealthcareserviceID($hservice_id);

        header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);
        exit;

        //return Redirect::back()
         //            ->with('flash_message', $flash_message)
           //          ->with('flash_type', 'alert-success')
             //        ->with('flash_tab', 'dengue');
    }

    public function view()
    {
        
    }

    public static function update()
    {

    }
}
