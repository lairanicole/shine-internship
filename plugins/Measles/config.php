<?php
$plugin_id = 'Measles';                       //plugin ID
$plugin_module = 'healthcareservices';          //module owner
$plugin_location = 'dropdown';                  //UI location where plugin will be accessible
$plugin_primaryKey = 'measles_id';        //primary_key used to find data
$plugin_table = 'measles_record';            //plugintable default; table_name custom table
$plugin_relationship = array();
$plugin_tabs_child = array('addservice', 'complaints', 'vitals', 'measles_plugin', 'impanddiag', 'medicalorders', 'disposition'); //,
$plugin_type = 'program';
$plugin_gender = 'all';

$plugin_folder = 'Measles'; //module owner
$plugin_title = 'Measles';            //plugin title
$plugin_description = 'Measles';
$plugin_version = '1.0';
$plugin_developer = 'ShineLabs';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2016";


$plugin_tabs = [
    'addservice' => 'Basic Information',
    'disposition' => 'Disposition',
    'medicalorders' => 'Medical Orders',
    'vitals' => 'Vitals & Physical',
    'measles_plugin' => 'Measles Reporting',
    'impanddiag' => 'Impressions and Diagnosis',
    'complaints' => 'Complaints'
];

$plugin_tabs_models = [
    'complaints' => 'GeneralConsultation',
    'disposition' => 'Disposition',
    'medicalorders' => 'MedicalOrder',
    'vitals' => 'VitalsPhysical',
    'measles_plugin' => 'MeaslesModel'
];


