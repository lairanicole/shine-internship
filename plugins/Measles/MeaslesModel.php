<?php

namespace Plugins\Measles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeaslesModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'measles_record';
    protected static $table_name = 'measles_record';
    protected $primaryKey = 'measles_id';


}
