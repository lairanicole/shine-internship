<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePatientEmployment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('patient_employmentinfo')==TRUE) {
            Schema::table('patient_employmentinfo', function (Blueprint $table) {
                if (Schema::hasColumn('patient_employmentinfo', 'facility_id')!=TRUE) {
                    $table->string('facility_id', 60);
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
