<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMigrationPediatrics extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('pediatrics_service')!=TRUE) { 
            Schema::create('pediatrics_service', function (Blueprint $table) {
                $table->increments('id');
                $table->string('pediatrics_id', 32);
                $table->string('healthcareservice_id', 32);
                $table->string('pediatricscase_id', 32);

                $table->string('newborn_screening_referral_date', 32);
                $table->string('newborn_screening_actual_date', 32);
                $table->string('child_protection_date', 32);
                $table->string('tt_status', 32);
                $table->double('birth_weight')->nullable();
                $table->string('vit_a_supp_first_date', 32);
                $table->integer('vit_a_first_age');
                $table->string('vit_a_supp_second_date', 32);
                $table->integer('vit_a_second_age');
                $table->string('iron_supp_start_date', 32);
                $table->string('iron_supp_end_date', 32);   
                $table->string('bcg_recommended_date', 32);
                $table->string('bcg_actual_date', 32);
                $table->string('dpt1_recommended_date', 32);
                $table->string('dpt1_actual_date', 32);
                $table->string('dpt2_recommended_date', 32);
                $table->string('dpt2_actual_date', 32);
                $table->string('dpt3_recommended_date', 32);
                $table->string('dpt3_actual_date', 32);
                $table->string('hepa_b1_recommended_date', 32);
                $table->string('hepa_b1_actual_date', 32);
                $table->string('hepa_b2_recommended_date', 32);
                $table->string('hepa_b2_actual_date', 32);
                $table->string('hepa_b3_recommended_date', 32);
                $table->string('hepa_b3_actual_date', 32);
                $table->string('measles_recommended_date', 32);
                $table->string('measles_actual_date', 32);
                $table->string('opv1_recommended_date', 32);
                $table->string('opv1_actual_date', 32);
                $table->string('opv2_recommended_date', 32);
                $table->string('opv2_actual_date', 32);
                $table->string('opv3_recommended_date', 32);
                $table->string('opv3_actual_date', 32);
                $table->tinyInteger('is_breastfed_first_month');
                $table->tinyInteger('is_breastfed_second_month');
                $table->tinyInteger('is_breastfed_third_month');
                $table->tinyInteger('is_breastfed_fourth_month');
                $table->tinyInteger('is_breastfed_fifth_month');
                $table->tinyInteger('is_breastfed_sixth_month');
                $table->tinyInteger('breastfeed_sixth_month');

                $table->softDeletes();
                $table->timestamps();
                $table->unique('healthcareservice_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pediatrics_service');
    }

}
