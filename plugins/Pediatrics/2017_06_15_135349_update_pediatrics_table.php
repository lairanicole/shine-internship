<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePediatricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('pediatrics_service')==TRUE) { 
            Schema::table('pediatrics_service', function(Blueprint $table) {
                $table->string('pediatrics_id', 60)->change();
                $table->string('healthcareservice_id', 60)->change();
                $table->string('pediatricscase_id', 60)->nullable()->change(); 
                $table->date('newborn_screening_referral_date')->nullable()->change();
                $table->date('newborn_screening_actual_date')->nullable()->change();
                $table->date('child_protection_date')->nullable()->change();
                $table->string('tt_status', 32)->nullable()->change();
                // $table->double('birth_weight')->nullable()->change();
                $table->integer('vit_a_first_age')->nullable()->change(); 
                $table->date('vit_a_supp_first_date')->nullable()->change();
                $table->date('vit_a_supp_second_date')->nullable()->change();
                $table->integer('vit_a_second_age')->nullable()->change();
                $table->date('iron_supp_start_date')->nullable()->change();
                $table->date('iron_supp_end_date')->nullable()->change();
                $table->date('bcg_recommended_date')->nullable()->change();
                $table->date('bcg_actual_date')->nullable()->change();
                $table->date('dpt1_recommended_date')->nullable()->change();
                $table->date('dpt1_actual_date')->nullable()->change();
                $table->date('dpt2_recommended_date')->nullable()->change();
                $table->date('dpt2_actual_date')->nullable()->change();
                $table->date('dpt3_recommended_date')->nullable()->change();
                $table->date('dpt3_actual_date')->nullable()->change();

                if (Schema::hasColumn('pediatrics_service', 'hepa_b1_recommended_date')==TRUE) {
                    $table->date('hepa_b1_recommended_date')->nullable()->change();
                } else {
                    $table->date('hepa_b1_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'hepa_b1_actual_date')==TRUE) {
                    $table->date('hepa_b1_actual_date')->nullable()->change();
                } else {
                    $table->date('hepa_b1_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'hepa_b2_recommended_date')==TRUE) {
                    $table->date('hepa_b2_recommended_date')->nullable()->change();
                } else {
                    $table->date('hepa_b2_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'hepa_b2_actual_date')==TRUE) {
                    $table->date('hepa_b2_actual_date')->nullable()->change();
                } else {
                    $table->date('hepa_b2_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'hepa_b3_recommended_date')==TRUE) {
                    $table->date('hepa_b3_recommended_date')->nullable()->change();
                } else {
                    $table->date('hepa_b3_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'hepa_b3_actual_date')==TRUE) {
                    $table->date('hepa_b3_actual_date')->nullable()->change();
                } else {
                    $table->date('hepa_b3_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'measles_recommended_date')==TRUE) {
                    $table->date('measles_recommended_date')->nullable()->change();
                } else {
                    $table->date('measles_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'measles_actual_date')==TRUE) {
                    $table->date('measles_actual_date')->nullable()->change();
                } else {
                    $table->date('measles_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'opv1_recommended_date')==TRUE) {
                    $table->date('opv1_recommended_date')->nullable()->change();
                } else {
                    $table->date('opv1_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'opv1_actual_date')==TRUE) {
                    $table->date('opv1_actual_date')->nullable()->change();
                } else {
                    $table->date('opv1_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'opv2_recommended_date')==TRUE) {
                    $table->date('opv2_recommended_date')->nullable()->change();
                } else {
                    $table->date('opv2_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'opv2_actual_date')==TRUE) {
                    $table->date('opv2_actual_date')->nullable()->change();
                } else {
                    $table->date('opv2_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'opv3_recommended_date')==TRUE) {
                    $table->date('opv3_recommended_date')->nullable()->change();
                } else {
                    $table->date('opv3_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'opv3_actual_date')==TRUE) {
                    $table->date('opv3_actual_date')->nullable()->change();
                } else {
                    $table->date('opv3_actual_date')->nullable();
                }
                
                DB::statement('ALTER TABLE `pediatrics_service` MODIFY `is_breastfed_first_month` SMALLINT UNSIGNED NULL;');
                DB::statement('ALTER TABLE `pediatrics_service` MODIFY `is_breastfed_second_month` SMALLINT UNSIGNED NULL;');
                DB::statement('ALTER TABLE `pediatrics_service` MODIFY `is_breastfed_third_month` SMALLINT UNSIGNED NULL;');
                DB::statement('ALTER TABLE `pediatrics_service` MODIFY `is_breastfed_fourth_month` SMALLINT UNSIGNED NULL;');
                DB::statement('ALTER TABLE `pediatrics_service` MODIFY `is_breastfed_fifth_month` SMALLINT UNSIGNED NULL;');
                DB::statement('ALTER TABLE `pediatrics_service` MODIFY `is_breastfed_sixth_month` SMALLINT UNSIGNED NULL;');
                DB::statement('ALTER TABLE `pediatrics_service` MODIFY `breastfeed_sixth_month` SMALLINT UNSIGNED NULL;');

                if (Schema::hasColumn('pediatrics_service', 'patient_id')!=TRUE) {
                    $table->string('patient_id', 60)->nullable();
                } 
                if (Schema::hasColumn('pediatrics_service', 'case_status')!=TRUE) {
                    $table->string('case_status', 10)->nullable();
                } 
                if (Schema::hasColumn('pediatrics_service', 'mnp_6_11')!=TRUE) {
                    $table->date('mnp_6_11')->nullable();
                } else {
                    $table->date('mnp_6_11')->nullable()->change();
                }
                if (Schema::hasColumn('pediatrics_service', 'mnp_12_23')!=TRUE) {
                    $table->date('mnp_12_23')->nullable();
                } else {
                    $table->date('mnp_12_23')->nullable()->change();
                }
                if (Schema::hasColumn('pediatrics_service', 'complimentary_food_sixth')!=TRUE) {
                    $table->date('complimentary_food_sixth')->nullable();
                } else {
                    $table->date('complimentary_food_sixth')->nullable()->change();
                }
                if (Schema::hasColumn('pediatrics_service', 'complimentary_food_seventh')!=TRUE) {
                    $table->date('complimentary_food_seventh')->nullable();
                } else {
                    $table->date('complimentary_food_seventh')->nullable()->change();
                }
                if (Schema::hasColumn('pediatrics_service', 'complimentary_food_eight')!=TRUE) {
                    $table->date('complimentary_food_eight')->nullable();
                } else {
                    $table->date('complimentary_food_eight')->nullable()->change();
                }
                if (Schema::hasColumn('pediatrics_service', 'hepa_b1_win24_recommended_date')!=TRUE) {
                    $table->date('hepa_b1_win24_recommended_date')->nullable();
                } else {
                    $table->date('hepa_b1_win24_recommended_date')->nullable()->change();
                }
                if (Schema::hasColumn('pediatrics_service', 'hepa_b1_win24_actual_date')!=TRUE) {
                    $table->date('hepa_b1_win24_actual_date')->nullable();
                } else {
                    $table->date('hepa_b1_win24_actual_date')->nullable()->change();
                }
                if (Schema::hasColumn('pediatrics_service', 'hepa_b1_aft24_actual_date')!=TRUE) {
                    $table->date('hepa_b1_aft24_actual_date')->nullable();
                } else {
                    $table->date('hepa_b1_aft24_actual_date')->nullable()->change();
                }
                if (Schema::hasColumn('pediatrics_service', 'penta1_recommended_date')==TRUE) {
                    $table->date('penta1_recommended_date')->nullable()->change();
                } else {
                    $table->date('penta1_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'penta1_actual_date')==TRUE) {
                    $table->date('penta1_actual_date')->nullable()->change();
                } else {
                    $table->date('penta1_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'penta2_recommended_date')==TRUE) {
                    $table->date('penta2_recommended_date')->nullable()->change();
                } else {
                    $table->date('penta2_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'penta2_actual_date')==TRUE) {
                    $table->date('penta2_actual_date')->nullable()->change();
                } else {
                    $table->date('penta2_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'penta3_recommended_date')==TRUE) {
                    $table->date('penta3_recommended_date')->nullable()->change();
                } else {
                    $table->date('penta3_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'penta3_actual_date')==TRUE) {
                    $table->date('penta3_actual_date')->nullable()->change();
                } else {
                    $table->date('penta3_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'rota1_recommended_date')==TRUE) {
                    $table->date('rota1_recommended_date')->nullable()->change();
                } else {
                    $table->date('rota1_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'rota1_actual_date')==TRUE) {
                    $table->date('rota1_actual_date')->nullable()->change();
                } else {
                    $table->date('rota1_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'hepa_b1_aft24_recommended_date')==TRUE) {
                    $table->date('hepa_b1_aft24_recommended_date')->nullable()->change();
                } else {
                    $table->date('hepa_b1_aft24_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'rota2_actual_date')==TRUE) {
                    $table->date('rota2_actual_date')->nullable()->change();
                 } else {
                    $table->date('rota2_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'rota2_recommended_date')==TRUE) {
                    $table->date('rota2_recommended_date')->nullable()->change();
                 } else {
                    $table->date('rota2_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'rota3_recommended_date')==TRUE) {
                    $table->date('rota3_recommended_date')->nullable()->change();
                 } else {
                    $table->date('rota3_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'rota3_actual_date')==TRUE) {
                    $table->date('rota3_actual_date')->nullable()->change();
                 } else {
                    $table->date('rota3_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'pcv1_recommended_date')==TRUE) {
                    $table->date('pcv1_recommended_date')->nullable()->change();
                 } else {
                    $table->date('pcv1_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'pcv1_actual_date')==TRUE) {
                    $table->date('pcv1_actual_date')->nullable()->change();
                 } else {
                    $table->date('pcv1_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'pcv2_recommended_date')==TRUE) {
                    $table->date('pcv2_recommended_date')->nullable()->change();
                 } else {
                    $table->date('pcv2_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'pcv2_actual_date')==TRUE) {
                    $table->date('pcv2_actual_date')->nullable()->change();
                 } else {
                    $table->date('pcv2_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'pcv3_recommended_date')==TRUE) {
                    $table->date('pcv3_recommended_date')->nullable()->change();
                 } else {
                    $table->date('pcv3_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'pcv3_actual_date')==TRUE) {
                    $table->date('pcv3_actual_date')->nullable()->change();
                 } else {
                    $table->date('pcv3_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'mcv1_recommended_date')==TRUE) {
                    $table->date('mcv1_recommended_date')->nullable()->change();
                 } else {
                    $table->date('mcv1_recommended_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'mcv1_actual_date')==TRUE) {
                    $table->date('mcv1_actual_date')->nullable()->change();
                 } else {
                    $table->date('mcv1_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'mcv2_actual_date')==TRUE) {
                    $table->date('mcv2_actual_date')->nullable()->change();
                 } else {
                    $table->date('mcv2_actual_date')->nullable();
                }
                if (Schema::hasColumn('pediatrics_service', 'mcv2_recommended_date')==TRUE) {
                    $table->date('mcv2_recommended_date')->nullable()->change();
                 } else {
                    $table->date('mcv2_recommended_date')->nullable();
                }
            });
        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
