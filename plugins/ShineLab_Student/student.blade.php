<?php
    $student_id = NULL;
    $year_level = NULL;
    $course_id = NULL;
    $department = NULL;
    $school = NULL;
    $days = new SplFixedArray(5);
    $days[0] = "M";
    $days[1] = "T";
    $days[2] = "W";
    $days[3] = "TH";
    $days[4] = "F";
    for($i=0;$i<5;$i++){
        $j = 700;
        $f = true;
        while($j <= 2100){
            $temp = $days[$i] . $j;
            $$temp = $temp;
            ($f) ? $j += 30 : $j += 70;
            $f = !$f;
        }
    }

    $timeslots = new SplFixedArray(29);
    $x = 700;
    $f = true;
    for($i=0;$i<29;$i++){
        if($f){
            $y = $x + 30;
        }
        else{
            $y = $x + 70;
        }
        $timeslots[$i] = $x . "-" . $y;
        $x = $y;
        $f = !$f;
    }

    if($plugdata) {
        $student_id = $plugdata->student_id;
        $year_level = $plugdata->year_level;
        $course_id = $plugdata->course_id;
        $department = $plugdata->department;
        $school = $plugdata->school;
        $days = new SplFixedArray(5);
        $days[0] = "M";
        $days[1] = "T";
        $days[2] = "W";
        $days[3] = "TH";
        $days[4] = "F";
        for($i=0;$i<5;$i++){
            $j = 700;
            $f = true;
            while($j <= 2100){
                $temp = $days[$i] . $j;
                $$temp = $plugdata->$temp;
                ($f) ? $j += 30 : $j += 70;
                $f = !$f;
            }
        }
    }

    $method = 'save';
    if($plugdata){
        $method = 'update';
    }

    $url = 'plugin/call/ShineLab_Student/student/' . $method . '/' . $patient->patient_id;
?>

<div class="tab-content">
    @if (Session::has('flash_message'))
        <div class="alert {{Session::get('flash_type') }}">{{ Session::get('flash_message') }}</div>
    @endif
 <div class="tab-pane step active" id="student">
        {!! Form::model($patient, array('url' => 'plugin/call/ShineLab_Student/student/'.$method.'/'.$patient->patient_id,'class'=>'form-horizontal')) !!}
        {{ csrf_field() }}
        <fieldset>
            <input type="hidden" name="id" value="{{ $plugdata->id or NULL }}" />
            <input type="hidden" name="patient_id" value="{{ $patient->patient_id }}" />

            <legend>Student Information</legend>
            <div class="form-group has-feedback">
                <label class="col-sm-2 control-label">Student ID</label>
                <div class="col-sm-4">
                    <input type="number" name="student_id" class="form-control alphanumeric" value="{{ $student_id }}">
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-2 control-label">Year Level</label>
                <div class="col-sm-4">
                    <input type="number" name="year_level" class="form-control alphanumeric" value="{{ $year_level }}">
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-2 control-label">Course ID</label>
                <div class="col-sm-4">
                    <input type="text" name="course_id" class="form-control alphanumeric" value="{{ $course_id }}">
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-2 control-label">Department</label>
                <div class="col-sm-4">
                    <input type="text" name="department" class="form-control alphanumeric" value="{{ $department }}">
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-2 control-label">School</label>
                <div class="col-sm-4">
                    <input type="text" name="school" class="form-control alphanumeric" value="{{ $school }}">
                </div>
            </div>
            <div class="form-group has-feedback">
                <label class="col-sm-2 control-label">Class Schedule</label>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-2"></div>
                    @foreach($days as $day)
                        <div class="col-sm-2">{{ $day }}</div>
                    @endforeach
                </div>
                
                @foreach ($timeslots as $slot)
                    <div class="row">
                        <div class="col-sm-2">{{ $slot }}</div>
                        @foreach ($days as $day)
                            <div class="col-sm-2">
                                <?php $split_string = explode("-", $slot); $day_time = $day . $split_string[0]; ?>
                                @if($$day_time == 1)
                                    <input type="checkbox" name={{ $day . $split_string[0] }} value="1" checked="checked">
                                @else
                                    <input type="checkbox" name={{ $day . $split_string[0] }} value="1">
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endforeach
                
            </div>

        </fieldset>
         <div class="form-group pull-right ">
            <button type="button" class="btn btn-primary" onclick="location.href='{{ url('patients/view/'.$patient->patient_id) }}'">Close</button>
            <button type="submit" value="submit" class="btn btn-success">{{ ucfirst($method) }}</button>
        </div>
        
        {!! Form::close() !!}
        <br clear="all" />
  </div>
</div>
