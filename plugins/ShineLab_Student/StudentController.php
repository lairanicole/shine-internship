<?php

use Illuminate\Http\Request;
use Plugins\ShineLab_Student\StudentModel;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\Repositories\Eloquent\UserRepository as UserRepository;
use ShineOS\Core\Patients\Entities\Patients;

class StudentController extends Controller
{
    protected $moduleName = 'Patients';
    protected $modulePath = 'patients';

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
        $this->middleware('auth');
        View::addNamespace('patients', 'src/ShineOS/Core/Patients/Resources/Views');
    }

    public function view($id){
        $patient = Patients::where('patient_id','=', $id)->first();
        $plugdata = StudentModel::where('patient_id',$id)->first();

        View::addNamespace('pluginform', plugins_path().'ShineLab_Student');
        echo View::make('pluginform::master', array('patient'=>$patient, 'plugdata'=>$plugdata))->render();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save()
    {   
        $id = Input::get('patient_id');

        // programmatically assign variable names of timeslots
        $student = new StudentModel();
        $days = new SplFixedArray(5);
        $days[0] = "M";
        $days[1] = "T";
        $days[2] = "W";
        $days[3] = "TH";
        $days[4] = "F";
        $cnt = 0;
        $times = new SplFixedArray(145);
        for($i=0;$i<5;$i++){
            $j = 700;
            $f = true;
            while($j <= 2100){
                $times[$cnt] = $days[$i] . $j;
                ($f) ? $j += 30 : $j += 70;
                $f = !$f;
                $cnt++;
            }
        }

        $student->patient_studentinfo_id = IdGenerator::generateId();
        $student->patient_id = Input::get('patient_id');
        $student->student_id = Input::get('student_id');
        $student->year_level = Input::get('year_level');
        $student->course_id = Input::get('course_id');
        $student->department = Input::get('department');
        $student->school = Input::get('school');

        foreach($times as $t){
            $student->$t = (Input::get($t) == NULL) ? 0 : 1;
        }

        $student->save();

        Session::flash('alert-class', 'alert-success');
        header('Location: '.site_url().'patients/view/'.$id);
        exit;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = Input::get('patient_id');

        // programmatically assign variable names of timeslots
        $days = new SplFixedArray(5);
        $days[0] = "M";
        $days[1] = "T";
        $days[2] = "W";
        $days[3] = "TH";
        $days[4] = "F";
        $cnt = 0;
        $times = new SplFixedArray(145);
        for($i=0;$i<5;$i++){
            $j = 700;
            $f = true;
            while($j <= 2100){
                $times[$cnt] = $days[$i] . $j;
                ($f) ? $j += 30 : $j += 70;
                $f = !$f;
                $cnt++;
            }
        }

        $student['student_id'] = Input::get('student_id');
        $student['year_level'] = Input::get('year_level');
        $student['course_id'] = Input::get('course_id');
        $student['department'] = Input::get('department');
        $student['school'] = Input::get('school');

        foreach($times as $t){
            $student[$t] = (Input::get($t) == 0 || Input::get($t) == NULL) ? 0 : 1;
        }

        $affectedRows = StudentModel::where('patient_id', $id)
            ->update($student);

        Session::flash('alert-class', 'alert-success');

        if($affectedRows > 0) {
            header('Location: '.site_url().'patients/view/'.$id);
            exit;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
