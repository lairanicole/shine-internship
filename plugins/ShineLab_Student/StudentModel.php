<?php

namespace Plugins\ShineLab_Student;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentModel extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $guarded = [];
    protected $table = 'patient_studentinfo';
    protected static $table_name = 'patient_studentinfo';
    protected $primaryKey = 'patient_id';
    protected $touches = array('patients');
    
    public function patients()
    {
        return $this->belongsTo('ShineOS\Core\Patients\Entities\Patients','patient_id','patient_id');
    }
}
