<?php

$plugin_module = 'patients';
$plugin_title = 'Student Record';
$plugin_id = 'Student';
$plugin_location = 'newdata';
$plugin_primaryKey = 'patient_id';
$plugin_table = 'patient_studentinfo';  
$plugin_description = 'The Student Plugin stores student data. It includes student schedules so the school infirmary may know when each student is available for consultation. The plugin uses its own DB table.';
$plugin_version = '1.0';
$plugin_developer = 'ShineLabs';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2019";
$plugin_folder = 'ShineLab_Student';
