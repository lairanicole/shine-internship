<?php
namespace Plugins\MaternalCare;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class MaternalCarePostpartumModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'maternalcare_postpartum';
    protected static $table_name = 'maternalcare_postpartum';
    protected $primaryKey = 'postpartum_id';

    protected $fillable = [];

    public function maternal()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\MaternalCare\MaternalCareModel','maternalcase_id','maternalcase_id');
    }

    public function visits()
    {
     DB::enableQueryLog();
        return $this->hasMany('Plugins\MaternalCare\MaternalCareVisitsModel','postpartum_id','postpartum_id');
    }

    public function supplements()
    {
     DB::enableQueryLog();
        return $this->hasMany('Plugins\MaternalCare\MaternalCareSupplementsModel','postpartum_id','postpartum_id');
    }
}
