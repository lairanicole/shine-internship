<?php
use Shine\Http\Controllers\Controller;

use Shine\Libraries\IdGenerator;
use Plugins\MaternalCare\MaternalCarePrenatalModel as MaternalPrenatal;
use Plugins\MaternalCare\MaternalCareVisitsModel as MaternalVisits;
use Plugins\MaternalCare\MaternalCareSupplementsModel as MaternalSupplements;
use Plugins\MaternalCare\MaternalCarePartographModel as MaternalPartograph;
use Plugins\MaternalCare\MaternalCareDeliveryModel as MaternalCareDelivery;
use Plugins\MaternalCare\MaternalCarePostpartumModel as MaternalPostpartum;
use Plugins\MaternalCare\MaternalCareImmunizationModel as MaternalImmunization;
use Plugins\MaternalCare\MaternalCareModel as MaternalCare;

class MaternalCareController extends Controller {
    public function __construct() {
        $this->healthcareservice_id = Input::get('hservice_id');
        $this->mccase_id = Input::get('mccase_id');
        $this->mcservice_id = Input::get('mcservice_id');
        $this->patient_id = Input::get('patient_id');
        $this->date_now = date('m/d/Y');
    }

    public function save($data)
    {
        $maternal = MaternalCare::where('healthcareservice_id', $this->healthcareservice_id)->first();

        //if this is a new maternal case, let us add a CaseID
        if($this->mccase_id == NULL) //if there is no caseID given, this is a new maternal case
        {
            $maternal->maternalcase_id = $this->mccase_id =  IdGenerator::generateId();
            $maternal->save();
        } else {
            //if this is a follow-up and caseID is given, assign caseID to this
            if($maternal->maternalcase_id == NULL)
            {
                $maternal->maternalcase_id = $this->mccase_id;
                $maternal->save();
            }
        }

        foreach($data as $key=>$d)
        {
            //check if $d is empty
            $EmptyTestArray = array_filter($d);
            if(!empty($EmptyTestArray)) {
                $func = 'save'.ucfirst($key);
                $this->$func($d);
            }
        }

        header('Location: '.site_url().'healthcareservices/edit/'.$this->patient_id.'/'.$this->healthcareservice_id);
        exit;
    }

    public function savePrenatal($data)
    {
        //Set imp variables
        $patient_id = getPatientIDByHealthcareserviceID($this->healthcareservice_id);
        $maternalcare = MaternalCare::select('maternalcase_id')->where('healthcareservice_id', $this->healthcareservice_id)->first();
        $prenatal_id = NULL;

        // Save to prenatal table
        $mp = MaternalPrenatal::where('maternalcase_id', $this->mccase_id)->count();
        if ($mp > 0):
            $prenatal = MaternalPrenatal::where('maternalcase_id', $this->mccase_id)->first();
        else:
            $prenatal = new MaternalPrenatal();
            $prenatal->prenatal_id = IdGenerator::generateId();
            $prenatal->maternalcase_id = $maternalcare->maternalcase_id;
        endif;

        $prenat = $data;
        $lastMenstruationPeriod = (new Datetime($prenat['lastMenstruationPeriod']))->format('Y-m-d H:i:s');
        $estimatedDateDelivery = (new Datetime($prenat['estimatedDateDelivery']))->format('Y-m-d H:i:s');
        $prenatal->last_menstruation_period = $lastMenstruationPeriod;
        $prenatal->expected_date_delivery = $estimatedDateDelivery;

        $prenatal->gravidity = $prenat['gravidity'];
        $prenatal->parity = $prenat['parity'];
        $prenatal->term = $prenat['term'];
        $prenatal->pre_term = $prenat['preterm'];
        $prenatal->abortion = $prenat['abortion'];
        $prenatal->living = $prenat['living'];
        $prenatal_id = $prenatal->prenatal_id;
        $prenatal->save();

        // Save Prenatal Visits
        $mVisits = MaternalVisits::where('prenatal_id', $prenatal_id)->get();
        // dd($mVisits);

        $trimester = $prenat['prenatalTrimester'];
        $schedVisit = $prenat['prenatalScheduledVisit'];
        $actualVisit = $prenat['prenatalActualVisit'];

        // Vaccinations and Supplements
        // NOTE:: DO NOT FORGET TO CREATE MIGRATION FOR IMMUNIZATIONS
        $supplementation = $prenat['prenatalSupplementation'];
        $date_given = $prenat['prenatalSupplementationDateGiven'];
        $quantity = $prenat['prenatalSupplementationQty'];

        //immunization
        $tetanus = $prenat['prenatalTetanus'];
        $schedTetanus = $prenat['prenatalTetanusScheduledVisit'];
        $actualTetanus = $prenat['prenatalTetanusActualVisit'];

        $insertVisitsAndSupplements = $this->insertVisitsAndSupplements($trimester, $schedVisit, $actualVisit, $supplementation, $date_given, $quantity, $prenatal_id, $tetanus, $schedTetanus, $actualTetanus, 'pre');

        // Get saved prenatal visits
        $prenatal_visits = NULL;
        if(isset($prenat['prenatalIdSaved']) && isset($prenat['actualVisitUpdate']))
        {    
            $prenatal_visits['prenatal_id_saved'] = $prenat['prenatalIdSaved'];
            $prenatal_visits['prenatal_act_vis_saved'] = $prenat['actualVisitUpdate'];
        }

        // Get saved prenatal tetanus toxoid visits
        $prenatal_tetanus = NULL;
        if(isset($prenat['prenatalTetanusIdSaved']) && isset($prenat['prenatalTetanusActualVisitSaved']))
        {
            $prenatal_tetanus['prenatal_tetanus_id_saved'] = $prenat['prenatalTetanusIdSaved'];
            $prenatal_tetanus['prenatal_tetanus_act_vis_saved'] = $prenat['prenatalTetanusActualVisitSaved'];
        }

        $updateVisitsAndSupplements = $this->updateVisitsAndSupplements($prenatal_visits,$prenatal_tetanus,NULL);
    }

    public function savePartograph()
    {
        //Set imp variables
        $patient_id = getPatientIDByHealthcareserviceID($this->healthcareservice_id);
        $maternalcare = MaternalCare::select('maternalcase_id')->where('healthcareservice_id', $this->healthcareservice_id)->first();
        $partograph_id = NULL;

        $mp = MaternalPartograph::where('maternalcase_id', $this->mccase_id)->count();
        /*if ($mp > 0):
            $partograph = MaternalPartograph::where('maternalcase_id', $this->mccase_id)->first();
        else:*/
            $partograph = new MaternalPartograph();
            $partograph->partograph_id = IdGenerator::generateId();
        //endif;

        $partograph->maternalcase_id =  $maternalcare->maternalcase_id;

        $partograph->date_examined = (new Datetime($_POST['partographDateExamined']))->format('Y-m-d H:i:s');
        $partograph->ruptured_membranes = $_POST['rupturedMembranes'];
        $partograph->rapid_assessment_result = $_POST['rapidAssessmentResult'];
        $partograph->time_taken = $_POST['partographTimeTaken'];
        $partograph->cervical_dilation = $_POST['cervicalDilation'];
        $partograph->fetal_station = isset($_POST['fetalStation']) ? $_POST['fetalStation'] : NULL;
        $partograph->amount_bleeding = isset($_POST['amountOfBleeding']) ? $_POST['amountOfBleeding'] : NULL;
        $partograph->no_contractions = $_POST['numContractions'];
        $partograph->fetal_heart_rate_per_min = $_POST['fetalHeartRatePM'];
        $partograph->bloodpressure_systolic = $_POST['BPSystolic'];
        $partograph->bloodpressure_diastolic = $_POST['BPDiastolic'];
        //$partograph->bloodpressure_assessment = $_POST['']; -- no input type for this
        $partograph->pulse = $_POST['Pulse'];
        $partograph->placenta_delivered = isset($_POST['placentaDelivered']) ? $_POST['placentaDelivered'] : 0;
        $partograph->amniotic_fluid_characteristic = isset($_POST['amnioticFluidChar']) ? $_POST['amnioticFluidChar'] : '';
        $partograph->save();

        $partographs = MaternalPartograph::where('maternalcase_id', $maternalcare->maternalcase_id)->get();
        $newtable = "";
        if (count($partographs) > 0) {
            $cnt = 0;
            foreach($partographs as $k=>$p) {
                $time_taken = isset($p->time_taken) ? $p->time_taken : NULL;
                $cervical_dilation = isset($p->cervical_dilation) ? $p->cervical_dilation : NULL;
                $contraction_count = isset($p->no_contractions) ? $p->no_contractions : NULL;
                $fetal_hr_permin = isset($p->fetal_heart_rate_per_min) ? $p->fetal_heart_rate_per_min : NULL;
                $systolic = isset($p->bloodpressure_systolic) ? $p->bloodpressure_systolic : NULL;
                $diastolic = isset($p->bloodpressure_diastolic) ? $p->bloodpressure_diastolic : NULL;
                $pr = isset($p->pulse) ? $p->pulse : NULL;
                $cnt++;
                $newtable .= "<tr>
                    <td><kbd>".$cnt."</kbd></td>
                    <td>".$time_taken."</td>
                    <td>".$cervical_dilation." CM</td>
                    <td>".$p->fetal_station."</td>
                    <td>".$p->amount_bleeding."</td>
                    <td>".$contraction_count."</td>
                    <td>".$fetal_hr_permin."</td>
                    <td>".$diastolic."/".$systolic."</td>
                    <td>".$pr."</td>
                </tr>";
            }
        }

        $response = array('status' => '100', 'resp' => 100, 'partos' => $newtable);

        echo json_encode($response);

        // NOTE:: Add prompt message
        //header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$this->healthcareservice_id);
        //exit;
    }

    public function savePostpartum($data)
    {
        //Set imp variables
        $patient_id = getPatientIDByHealthcareserviceID($this->healthcareservice_id);
        $maternalcare = MaternalCare::select('maternalcase_id')->where('healthcareservice_id', $this->healthcareservice_id)->first();
        $postpartum = NULL;

        $mp = MaternalPostpartum::where('maternalcase_id', $this->mccase_id)->count();
        if ($mp > 0):
            $postpartum = MaternalPostpartum::where('maternalcase_id', $this->mccase_id)->first();
        else:
            $postpartum = new MaternalPostpartum();
            $postpartum->postpartum_id = IdGenerator::generateId();
        endif;

        $postpartum->maternalcase_id = $maternalcare->maternalcase_id;
        $postpartum->breastfeeding_date = (new Datetime($data['postpartumBreastFeedingDate']))->format('Y-m-d H:i:s');
        $postpartum_id = $postpartum->postpartum_id;

        $postpartum->save();

        // Save Prenatal Visits

        $trimester = $data['postpartumTrimester'];
        $schedVisit = $data['postpartumScheduledVisit'];
        $actualVisit = $data['postpartumActualVisit'];

        // Vaccinations and Supplements
        // NOTE:: DO NOT FORGET TO CREATE MIGRATION FOR IMMUNIZATIONS
        $supplementation = NULL;
        $date_given = NULL;
        $quantity = NULL;
        if(isset($data['postpartumSupplementation'])) {
            $supplementation = $data['postpartumSupplementation'];
            $date_given = $data['postpartumSupplementationDateGiven'];
            $quantity = $data['postpartumSupplementationQty'];
        }

        $insertVisitsAndSupplements = $this->insertVisitsAndSupplements($trimester, $schedVisit, $actualVisit, $supplementation, $date_given, $quantity, $postpartum_id, NULL, NULL, NULL, 'post');

        $postpartum_visits = NULL;
        if(isset($data['postpartumVisitIdSaved']) && isset($data['postpartumActualVisitSaved']))
        {
            $postpartum_visits['postpartum_id_saved'] = $data['postpartumVisitIdSaved'];
            $postpartum_visits['postpartum_act_vis_saved'] = $data['postpartumActualVisitSaved'];
        }

        $updateVisitsAndSupplements = $this->updateVisitsAndSupplements(NULL,NULL,$postpartum_visits);

        //header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$this->healthcareservice_id);
        //exit;
    }

    public function updateVisitsAndSupplements($prenatal_visits=NULL,$prenatal_tetanus=NULL,$postpartum_visits=NULL)
    {
        if($prenatal_visits != NULL)
        {
            if(count($prenatal_visits['prenatal_id_saved']) == count($prenatal_visits['prenatal_act_vis_saved']))
            {
                for($i=0; $i<count($prenatal_visits['prenatal_id_saved']); $i++)
                {
                    if($prenatal_visits['prenatal_act_vis_saved'][$i] != '' && $prenatal_visits['prenatal_act_vis_saved'][$i] != NULL)
                    {
                        $visit_to_update = MaternalVisits::where('maternalvisits_id',$prenatal_visits['prenatal_id_saved'][$i])->first();
                        $visit_to_update->actual_visit = (new Datetime($prenatal_visits['prenatal_act_vis_saved'][$i]))->format('Y-m-d H:i:s');
                        $visit_to_update->save();
                    }
                }
            }
        }

        if($prenatal_tetanus != NULL)
        {
            if(count($prenatal_tetanus['prenatal_tetanus_id_saved']) == count($prenatal_tetanus['prenatal_tetanus_act_vis_saved']))
            {       
                for($i=0; $i<count($prenatal_tetanus['prenatal_tetanus_id_saved']); $i++)
                {
                    if($prenatal_tetanus['prenatal_tetanus_act_vis_saved'][$i] != '' && $prenatal_tetanus['prenatal_tetanus_act_vis_saved'][$i] != NULL)
                    {
                        $visit_to_update = MaternalImmunization::where('immunization_id',$prenatal_tetanus['prenatal_tetanus_id_saved'][$i])->first();
                        $visit_to_update->actual_date = (new Datetime($prenatal_tetanus['prenatal_tetanus_act_vis_saved'][$i]))->format('Y-m-d H:i:s');
                        $visit_to_update->save();
                    }
                }
            }
        }

        if($postpartum_visits != NULL)
        {
            if(count($postpartum_visits['postpartum_id_saved']) == count($postpartum_visits['postpartum_act_vis_saved']))
            {
                for($i=0; $i<count($postpartum_visits['postpartum_id_saved']); $i++)
                {
                    if($postpartum_visits['postpartum_act_vis_saved'][$i] != '' && $postpartum_visits['postpartum_act_vis_saved'][$i] != NULL)
                    {
                        $visit_to_update = MaternalVisits::where('maternalvisits_id',$postpartum_visits['postpartum_id_saved'][$i])->first();
                        $visit_to_update->actual_visit = (new Datetime($postpartum_visits['postpartum_act_vis_saved'][$i]))->format('Y-m-d H:i:s');
                        $visit_to_update->save();
                    }
                }
            }            
        }
    }

    public function saveDelivery($data)
    {
        //Set imp variables
        $patient_id = getPatientIDByHealthcareserviceID($this->healthcareservice_id);
        $maternalcare = MaternalCare::select('maternalcase_id')->where('healthcareservice_id', $this->healthcareservice_id)->first();

        // Save to prenatal table
        $mp = MaternalCareDelivery::where('maternalcase_id', $this->mccase_id)->count();
        if ($mp > 0):
            $delivery = MaternalCareDelivery::where('maternalcase_id', $this->mccase_id)->first();
        else:
            $delivery = new MaternalCareDelivery();
            $delivery->maternaldelivery_id = IdGenerator::generateId();
            $delivery->maternalcase_id = $maternalcare->maternalcase_id;
        endif;

        $deli = $data;
        //Save form values
        $delivery_date = ($deli['deliveryDate'] != '') ? $deli['deliveryDate'] : null;
        $delivery_time = ($deli['deliveryTime'] != '') ? $deli['deliveryTime'] : null;
        $delivery_type = ($deli['deliveryType'] != '') ? $deli['deliveryType'] : null;
        $delivery_type_mode = ($deli['deliveryTypeMode'] != '') ? $deli['deliveryTypeMode'] : null;
        $delivery_place_type = ($deli['deliveryPlaceType'] != '') ? $deli['deliveryPlaceType'] : null;

        $delivery_place = ( ($deli['deliveryplace_NID'] != NULL) ? $deli['deliveryplace_NID'] : $deli['deliveryplace_FB'] );

        $weight = ($deli['livebirthWeight'] != '') ? $deli['livebirthWeight'] : null;
        $outcome = ($deli['pregnancyOutcome'] != '') ? $deli['pregnancyOutcome'] : null;
        $gender = (isset($deli['childGender'])) ? $deli['childGender'] : null;
        $attendant = ($deli['deliveryAttended'] != '') ? $deli['deliveryAttended'] : null;
        $birth_multiplicity = ($deli['birth_multiplicity'] != '') ? $deli['birth_multiplicity'] : null;
        $multiple_births = ($deli['multiple_births'] != '') ? $deli['deliveryAttended'] : null;

        $delivery->termination_datetime =  $this->combineDateTime($delivery_date, $delivery_time);
        $delivery->child_gender = $gender;
        $delivery->livebirth_weight = $weight;
        $delivery->delivery_place_type = $delivery_place_type;
        $delivery->delivered_type = $delivery_type;
        $delivery->delivery_type_mode = $delivery_type_mode;
        $delivery->delivery_place = $delivery_place;
        $delivery->termination_outcome = $outcome;
        $delivery->attendant = $attendant;
        $delivery->birth_multiplicity = $birth_multiplicity;
        $delivery->multiple_births = $multiple_births;
        // dd($delivery);
        $delivery->save();

        // NOTE:: Add prompt message
        //header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$this->healthcareservice_id);
        //exit;
    }

    public function insertVisitsAndSupplements($trimester, $schedVisit, $actualVisit, $supplementation, $date_given, $quantity, $prepost_id, $tetanusi=NULL, $schedTetanus=NULL, $actualTetanus=NULL, $maternal_type) {

        for($i=0; $i < count($trimester); $i++) {
            if($trimester[$i] != "" AND $trimester[$i] != NULL) {
                $visits = new MaternalVisits();

                $visits->maternalvisits_id = IdGenerator::generateId();
                $visits->maternalcase_id = $this->mccase_id;
                if($maternal_type == 'pre') {
                    $visits->prenatal_id = $prepost_id;
                } else {
                    $visits->postpartum_id = $prepost_id;
                }

                $visits->trimester_period = $trimester[$i];
                $visits->scheduled_visit = (new Datetime($schedVisit[$i]))->format('Y-m-d H:i:s');
                $visits->actual_visit = (new Datetime($actualVisit[$i]))->format('Y-m-d H:i:s');
                $visits->save();
            }
        }

        for($i=0; $i < count($supplementation); $i++) {
            if($supplementation[$i] != "" AND $supplementation[$i] != NULL) {
                $supplements = new MaternalSupplements();

                $supplements->maternalsupplement_id = IdGenerator::generateId();
                $supplements->maternalcase_id = $this->mccase_id;
                if($maternal_type == 'pre') {
                    $supplements->prenatal_id = $prepost_id;
                } else {
                    $supplements->postpartum_id = $prepost_id;
                }
                $supplements->supplement_type = $supplementation[$i];

                $supplements->actual_date = (new Datetime($date_given[$i]))->format('Y-m-d H:i:s');
                $supplements->quantity_dispensed = $quantity[$i];
                $supplements->save();
            }
        }

        for($i=0; $i < count($tetanusi); $i++) {
            if($tetanusi[$i] != "" AND $tetanusi[$i] != NULL) {
                $tetanus = new MaternalImmunization();

                $tetanus->immunization_id = IdGenerator::generateId();
                if($maternal_type == 'pre') {
                    $tetanus->plugin_id = $prepost_id;
                    $tetanus->plugin = 'prenatal';
                }
                $tetanus->patient_id = $this->patient_id;
                $tetanus->healthcareservice_id = $this->healthcareservice_id;
                $tetanus->subservice_id = $this->mccase_id;
                $tetanus->immunization_code = "tetanus";
                $tetanus->immun_type = $tetanusi[$i];
                $tetanus->scheduled_date = (new Datetime($schedTetanus[$i]))->format('Y-m-d H:i:s');
                $tetanus->actual_date = (new Datetime($actualTetanus[$i]))->format('Y-m-d H:i:s');
                $tetanus->save();
            }
        }
    }

    // NOTE:: Transfer / Convert into a helper
    private function combineDateTime($date, $time)
    {
        return date('Y-m-d H:i:s', strtotime("$date $time"));
    }

    public function showPartograph($maternal_id)
    {

        $partographs = MaternalPartograph::where('maternalcase_id', $maternal_id)->get();

        //dd($partographs);

        // NOTE:: Add prompt message
        //header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$this->healthcareservice_id);
        //exit;
    }
}
