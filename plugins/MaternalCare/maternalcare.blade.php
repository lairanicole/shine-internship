<?php

    $healthcareservice_id = $data['healthcareservice_id'];

    //assign standard data
    // dd($data['prenatal']);
    $prenataldat = $data['prenatal'];
    $partographdat = $data['partograph'];
    $deliveriesdat = $data['deliveries'];
    $postpartumdat = $data['postpartum'];

    //if this is a follow-up
    //assign previous data
    // dd($pdata);
    if($pdata AND count($data['prenatal']) == 0) {
        $prenataldat = $pdata['prenatal'];
    }
    if($pdata AND count($data['partograph']) == 0) {
        $partographdat = $pdata['partograph'];
    }
    if($pdata AND count($data['deliveries']) == 0) {
        $deliveriesdat = $pdata['deliveries'];
    }
    if($pdata AND count($data['postpartum']) == 0) {
        $postpartumdat = $pdata['postpartum'];
    }

    /**
     * Prenatal set values
     * @var null
     */
    $lastMenstruationPeriod = date('m/d/Y');
    $estimatedDateDelivery = NULL;
    $gravidity = NULL;
    $parity = NULL;
    $term = NULL;
    $preterm = NULL;
    $abortion = NULL;
    $living = NULL;

    if (count($prenataldat) > 0)
    {
        $lastMenstruationPeriod = date('m/d/Y', strtotime($prenataldat[0]->last_menstruation_period));
        $estimatedDateDelivery = date('m/d/Y', strtotime($prenataldat[0]->expected_date_delivery));
        $gravidity = $prenataldat[0]->gravidity;
        $parity = $prenataldat[0]->parity;
        $term = $prenataldat[0]->term;
        $preterm = $prenataldat[0]->pre_term;
        $abortion = $prenataldat[0]->abortion;
        $living = $prenataldat[0]->living;
    }

    /**
     * Partograph set values
     * @var null
     */
    $partographDateExamined = NULL;
    $rupturedMembranes = NULL;
    $rapidAssessmentResult = NULL;
    $partographTimeTaken = NULL;
    $cervicalDilation = NULL;
    $fetalStation = NULL;
    $amountBleeding = NULL;
    $numContractions = NULL;
    $fetalHeartRatePM = NULL;
    $BPDiastolic = NULL;
    $BPSystolic = NULL;
    $Pulse = NULL;
    $placentaDelivered = NULL;
    $amnioticFluidCharacteristic = NULL;

    if (count($partographdat) > 0)
    {
        $partographDateExamined = date('m/d/Y', strtotime($partographdat[0]->date_examined));
        $rupturedMembranes = $partographdat[0]->ruptured_membranes;
        $rapidAssessmentResult = $partographdat[0]->rapid_assessment_result;
        $partographTimeTaken = date('h:i A', strtotime($partographdat[0]->time_taken));
        $cervicalDilation = $partographdat[0]->cervical_dilation;

        $fetalStation = $partographdat[0]->fetal_station;
        $amountBleeding = $partographdat[0]->amount_bleeding;

        $numContractions = $partographdat[0]->no_contractions;
        $fetalHeartRatePM = $partographdat[0]->fetal_heart_rate_per_min;
        $BPDiastolic = $partographdat[0]->bloodpressure_diastolic;
        $BPSystolic = $partographdat[0]->bloodpressure_systolic;
        $Pulse = $partographdat[0]->pulse;

        $placentaDelivered = $partographdat[0]->placenta_delivered;
        $amnioticFluidCharacteristic = $partographdat[0]->amniotic_fluid_characteristic;
    }

    /**
     * Pregnancy Termination set values
     * @var null
     */
    $deliveryDate = NULL;
    $deliveryTime = NULL;
    $termination_outcome = NULL;
    $childGender = 'U';
    $livebirthWeight = NULL;
    $birth_multiplicity = NULL;
    $multiple_births = NULL;
    $attendant = NULL;
    $delivery_type = NULL;
    $delivery_type_mode = NULL;
    $delivery_place_type = NULL;
    $delivery_place = NULL;

    if (count($deliveriesdat) > 0) {
        $deliveryDate = date('m/d/Y', strtotime($deliveriesdat[0]->termination_datetime));
        $deliveryTime = date('h:i A', strtotime($deliveriesdat[0]->termination_datetime));
        $termination_outcome = $deliveriesdat[0]->termination_outcome;
        $childGender = $deliveriesdat[0]->child_gender;
        $livebirthWeight = $deliveriesdat[0]->livebirth_weight;
        $birth_multiplicity = $deliveriesdat[0]->birth_multiplicity;
        $multiple_births = $deliveriesdat[0]->multiple_births;
        $attendant = $deliveriesdat[0]->attendant;
        $delivery_type = $deliveriesdat[0]->delivered_type;
        $delivery_type_mode = $deliveriesdat[0]->delivery_type_mode;
        $delivery_place_type = $deliveriesdat[0]->delivery_place_type;
        $delivery_place = $deliveriesdat[0]->delivery_place;
    }

    $postpartumBreastFeedingDate = NULL;
    if (count($postpartumdat) > 0) {
        $postpartumBreastFeedingDate = date('m/d/Y', strtotime($postpartumdat[0]->breastfeeding_date));
    }

    //new
    if( isset($allData['pluginparentdata']) ) {
        $mccaseID = $allData['pluginparentdata']->maternalcase_id;
    } elseif(isset($data->maternalcase_id) AND $data->maternalcase_id) {
        $mccaseID = $data->maternalcase_id;
    } else {
        $mccaseID = NULL;
    }
?>

<div class="box-body no-padding">
  <!-- Main content -->
    <div class="iCheck">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active">
               <a href="#maternalcare_prenatal" data-toggle="tab">Prenatal</a>
            </li>
            <li class="">
               <a href="#maternalcare_partograph" data-toggle="tab">Partograph</a>
            </li>
            <li class="">
               <a href="#maternalcare_delivery" data-toggle="tab">Maternal Delivery</a>
            </li>
            <li class="">
               <a href="#maternalcare_postpartum" data-toggle="tab">Postpartum</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="maternalcare_prenatal">

                <input type="hidden" name="hservice_id" value="{{ $healthcareservice_id }}">
                <input type="hidden" name="mcservice_id" value="{{ $data->maternalcare_id }}">
                <input type="hidden" name="mccase_id" value="{{ $mccaseID }}">
                <fieldset>
                    <legend> Prenatal </legend>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> Last Menstruation Period (LMP) </label>
                            <div class="col-sm-4">
                                 {!! Form::text('maternalcare[prenatal][lastMenstruationPeriod]', $lastMenstruationPeriod, ['class' => 'form-control', 'id'=>'datepicker', 'placeholder'=>'mm/dd/yyyy']); !!}                            
                            </div>
                            <label class="col-sm-2 control-label"> Estimated Date of Delivery (EDD) </label>
                            <div class="col-sm-4">
                                {!! Form::text('maternalcare[prenatal][estimatedDateDelivery]', $estimatedDateDelivery, ['class' => 'form-control datepicker_future_null', 'placeholder'=>'preterm', 'placeholder'=>'mm/dd/yyyy', 'id'=>'edd', 'onfocus'=>'setEDD();']); !!}
                            </div>
                        </div><!-- /.col-12 -->
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> Gravidity </label>
                            <div class="col-sm-4">
                                {!! Form::number('maternalcare[prenatal][gravidity]', $gravidity, ['class' => 'form-control', 'placeholder'=>'gravidity', 'min'=>'0']) !!}
                            </div>
                            <label class="col-sm-2 control-label"> Parity </label>
                            <div class="col-sm-4">
                                {!! Form::number('maternalcare[prenatal][parity]', $parity, ['class' => 'form-control', 'placeholder'=>'parity', 'min'=>'0']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> Term </label>
                            <div class="col-sm-4">
                                {!! Form::number('maternalcare[prenatal][term]', $term, ['class' => 'form-control', 'placeholder'=>'term', 'min'=>'0']) !!}
                            </div>
                            <label class="col-sm-2 control-label"> Pre-Term </label>
                            <div class="col-sm-4">
                                {!! Form::number('maternalcare[prenatal][preterm]', $preterm, ['class' => 'form-control', 'placeholder'=>'preterm', 'min'=>'0']) !!}
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> Abortion </label>
                            <div class="col-sm-4">
                                {!! Form::number('maternalcare[prenatal][abortion]', $abortion, ['class' => 'form-control', 'placeholder'=>'abortion', 'min'=>'0']) !!}
                            </div>
                            <label class="col-sm-2 control-label"> Living </label>
                            <div class="col-sm-4">
                                {!! Form::number('maternalcare[prenatal][living]', $living, ['class' => 'form-control', 'placeholder'=>'living', 'min'=>'0']) !!}
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend> Prenatal Visits </legend>
                        <div class="col-sm-12">
                            <table class="col-sm-12">
                                <tr>
                                    <td class="col-sm-4">
                                       <dt> Trimester </dt>
                                    </td>
                                    <td class="col-sm-3">
                                        <dt> Scheduled Visit </dt>
                                    </td>
                                    <td class="col-sm-3">
                                        <dt> Actual Visit </dt>
                                    </td>
                                    <td class="col-sm-2"> &nbsp; </td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-sm-12">
                             @if(!empty($prenataldat))
                                @foreach($prenataldat as $key => $value)
                                    @if(!empty($value->visits))
                                        @foreach($value->visits as $kvisit => $vvalue)
                                        {!! Form::hidden('maternalcare[prenatal][prenatalIdSaved][]',$vvalue->maternalvisits_id); !!}
                                        <table class="col-sm-12">
                                            <tbody>
                                                <tr class="prenatal">
                                                    <td class="col-sm-4">
                                                        <dl>
                                                            <dd>
                                                                {!! Form::select(NULL, array(NULL => '--Select--', '1' => 'First', '2' => 'Second', '3' => 'Third'), $vvalue->trimester_period, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-3">
                                                        <dl>
                                                            <dd>
                                                                {!!Form::text(NULL, date('m/d/Y', strtotime($vvalue->scheduled_visit)), ['class' => 'form-control datepicker_scheduled', 'placeholder'=>'mm/dd/yyyy', 'disabled'=>'disabled']); !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-3">
                                                        <dl>
                                                            <dd>
                                                                {!!Form::text('maternalcare[prenatal][actualVisitUpdate][]', date('m/d/Y', strtotime($vvalue->actual_visit)), ['class' => 'form-control datepicker', 'placeholder'=>'mm/dd/yyyy']); !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-2"> &nbsp; </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        @if($read == NULL AND count($prenataldat) < 3)
                        <?php /*AND count($value->visits) < 3 */ ?>
                        <div class="m_prenatalvisit col-sm-12">
                            <table class="col-sm-12">
                                <tbody>
                                    <tr class="prenatal">
                                        <td class="hidden"><input type="text" name="id[]" value="0" class="id" /></td>
                                        <td class="col-sm-4">
                                            <dl>
                                                <dd>
                                                    {!!Form::select('maternalcare[prenatal][prenatalTrimester][]', array(NULL=>'--Select--', '1' => 'First', '2' => 'Second', '3' => 'Third'), NULL, ['class' => 'form-control prenatalTrimester']) !!}
                                                </dd>
                                            </dl>
                                        </td>
                                        <td class="col-sm-3">
                                            <dl>
                                                <dd>
                                                    {!!Form::text('maternalcare[prenatal][prenatalScheduledVisit][]', NULL, ['class' => 'form-control datepicker_scheduled prenatalScheduledVisit', 'placeholder'=>'mm/dd/yyyy']); !!}
                                                </dd>
                                            </dl>
                                        </td>
                                        <td class="col-sm-3">
                                            <dl>
                                                <dd>
                                                    {!!Form::text('maternalcare[prenatal][prenatalActualVisit][]', NULL, ['class' => 'form-control datepicker prenatalActualVisit', 'placeholder'=>'mm/dd/yyyy']); !!}
                                                </dd>
                                            </dl>
                                        </td>
                                        <td class="col-sm-2">
                                            <input type='button' class="delRow btn btn-default btn-sm" value="Delete" disabled="disabled">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-sm-12">
                                <input type='button' class="add btn btn-default btn-sm" value="Add">
                            </div>
                        </div>
                        @endif
                </fieldset>
                <fieldset>
                    <legend> Tetanus Toxoid Vaccination </legend>
                        <div class="col-sm-12">
                            <table class="col-sm-12">
                                <tr>
                                    <td class="col-sm-4">
                                       <dt> Tetanus </dt>
                                    </td>
                                    <td class="col-sm-3">
                                        <dt> Scheduled Visit </dt>
                                    </td>
                                    <td class="col-sm-3">
                                        <dt> Actual Visit </dt>
                                    </td>
                                    <td class="col-sm-2"> &nbsp; </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-12">
                             @if(!empty($prenataldat))
                                @foreach($prenataldat as $key => $value)
                                    @if(!empty($value->immunizations))
                                        @foreach($value->immunizations as $kvisit => $vvalue)
                                        {!! Form::hidden('maternalcare[prenatal][prenatalTetanusIdSaved][]',$vvalue->immunization_id);  !!}
                                        <table class="col-sm-12">
                                            <tbody>
                                                <tr class="prenatal">
                                                    <td class="col-sm-4">
                                                        <dl>
                                                            <dd>
                                                                {!!Form::text(NULL, $vvalue->immun_type, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-3">
                                                        <dl>
                                                            <dd>
                                                                {!!Form::text(NULL, date('m/d/Y', strtotime($vvalue->scheduled_date)), ['class' => 'form-control datepicker_scheduled', 'placeholder'=>'mm/dd/yyyy', 'disabled'=>'disabled']); !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-3">
                                                        <dl>
                                                            <dd>
                                                                {!! Form::text('maternalcare[prenatal][prenatalTetanusActualVisitSaved][]', date('m/d/Y', strtotime($vvalue->actual_date)), ['class' => 'form-control datepicker', 'placeholder'=>'mm/dd/yyyy']); !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-2"> &nbsp; </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        @if($read == NULL)
                        <?php /* AND count($value->immunizations)<=4*/ ?>
                        <div class="m_prenataltetanus col-sm-12">
                            <table class="col-sm-12">
                                <tbody>
                                    <tr class="prenatal">
                                        <td class="hidden"><input type="text" name="id[]" value="0" class="id" /></td>
                                        <td class="col-sm-4">
                                            <dl>
                                                <dd>
                                                    {!!Form::select('maternalcare[prenatal][prenatalTetanus][]', array(NULL=>'--Select--','1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'), NULL, ['class' => 'form-control']) !!}
                                                </dd>
                                            </dl>
                                        </td>
                                        <td class="col-sm-3">
                                            <dl>
                                                <dd>
                                                    {!!Form::text('maternalcare[prenatal][prenatalTetanusScheduledVisit][]', NULL, ['class' => 'form-control datepicker_scheduled', 'placeholder'=>'mm/dd/yyyy']); !!}
                                                </dd>
                                            </dl>
                                        </td>
                                        <td class="col-sm-3">
                                            <dl>
                                                <dd>
                                                    {!!Form::text('maternalcare[prenatal][prenatalTetanusActualVisit][]', NULL, ['class' => 'form-control datepicker', 'placeholder'=>'mm/dd/yyyy']); !!}
                                                </dd>
                                            </dl>
                                        </td>
                                        <td class="col-sm-2">
                                            <input type='button' class="delRow btn btn-default btn-sm" value="Delete" disabled="disabled">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-sm-12">
                                <input type='button' class="add btn btn-default btn-sm" value="Add">
                            </div>
                        </div>
                        @endif
                </fieldset>

                <fieldset>
                    <legend> Micronutrient Supplementation </legend>
                        <div class="col-sm-12">
                            <table class="col-sm-12">
                                <tr>
                                    <td class="col-sm-4">
                                       <dt> Supplementation </dt>
                                    </td>
                                    <td class="col-sm-3">
                                        <dt> Date Given </dt>
                                    </td>
                                    <td class="col-sm-3">
                                        <dt> Quantity Dispensed </dt>
                                    </td>
                                    <td class="col-sm-2"> &nbsp; </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-12">
                             @if(!empty($prenataldat))
                                @foreach($prenataldat as $key => $value)
                                    @if(!empty($value->supplements))
                                        @foreach($value->supplements as $ksupplements => $vsupplements)
                                        <table class="col-sm-12">
                                            <tbody>
                                                <tr>
                                                    <td class="col-sm-4">
                                                        <dl>
                                                            <dd>
                                                                {!!Form::select(NULL, array(NULL=>'--Select--','1' => 'Vitamin A', '2' => 'Iron with Folic Acid'), $vsupplements->supplement_type, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-3">
                                                        <dl>
                                                            <dd>
                                                                {!!Form::text(NULL, date('m/d/Y', strtotime($vsupplements->actual_date)), ['class' => 'form-control datepicker', 'placeholder'=>'mm/dd/yyyy', 'disabled'=>'disabled']); !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-3">
                                                        <dl>
                                                            <dd>
                                                                {!! Form::number(NULL, $vsupplements->quantity_dispensed, ['class' => 'form-control', 'placeholder'=>'gravidity', 'disabled'=>'disabled']) !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-2"> &nbsp; </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        @if($read == NULL)
                        <?php /* AND count($value->supplements)<2*/ ?>
                        <div class="m_prenatalmicronutrient col-sm-12">
                            <table class="col-sm-12">
                                <tbody>
                                    <tr class="prenatal">
                                        <td class="hidden"><input type="text" name="id[]" value="0" class="id" /></td>
                                        <td class="col-sm-4">
                                            <dl>
                                                <dd>
                                                    {!!Form::select('maternalcare[prenatal][prenatalSupplementation][]', array(NULL=>'--Select--','1' => 'Vitamin A', '2' => 'Iron with Folic Acid'), NULL, ['class' => 'form-control']) !!}
                                                </dd>
                                            </dl>
                                        </td>
                                        <td class="col-sm-3">
                                            <dl>
                                                <dd>
                                                    {!!Form::text('maternalcare[prenatal][prenatalSupplementationDateGiven][]', NULL, ['class' => 'form-control datepicker',  'placeholder'=>'mm/dd/yyyy']); !!}
                                                </dd>
                                            </dl>
                                        </td>
                                        <td class="col-sm-3">
                                            <dl>
                                                <dd>
                                                    {!! Form::number('maternalcare[prenatal][prenatalSupplementationQty][]', null, ['class' => 'form-control', 'placeholder'=>'gravidity', 'min'=>'0']) !!}
                                                </dd>
                                            </dl>
                                        </td>
                                        <td class="col-sm-2">
                                            <input type='button' class="delRow btn btn-default btn-sm" value="Delete" disabled="disabled">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-sm-12">
                                <input type='button' class="add btn btn-default btn-sm" value="Add">
                            </div>

                        </div>
                        @endif
                </fieldset>


            </div><!-- /.tab-pane -->

            <div class="tab-pane" id="maternalcare_partograph">

                <input type="hidden" name="hservice_id" value="{{ $healthcareservice_id }}">
                <input type="hidden" name="mcservice_id" value="{{ $data->maternalcare_id }}">
                <input type="hidden" name="partograph_id" value="">
                <fieldset>
                    <legend>Partograph Chart @if (count($partographdat) > 0)<a href="{{ url('plugin/call/MaternalCare/MaternalCare/showPartograph/'.$data->maternalcare_id) }}"  class="btn btn-primary btn-sm pull-right hidden" data-toggle="modal" data-target="#myInfoModal"><i class="fa fa-bar-chart"></i> | View Chart</a>@endif</legend>
                    <table class="table table-condensed table-stripped">
                        <tr>
                            <th width="4%"><kbd>-</kbd></th>
                            <th width="12%">Time</th>
                            <th width="12%">CD</th>
                            <th width="12%">FS</th>
                            <th width="12%">B</th>
                            <th width="12%">C</th>
                            <th width="12%">FHR</th>
                            <th width="12%">S/D</th>
                            <th width="12%">P</th>
                        </tr>
                        <tbody id="partographTable">
                        <?php if (count($partographdat) > 0) {
                            $SELECTED = "selected = 'selected'";
                            $cnt = 0;
                            foreach($partographdat as $k=>$p) {
                                $time_taken = isset($p->time_taken) ? $p->time_taken : NULL;
                                $cervical_dilation = isset($p->cervical_dilation) ? $p->cervical_dilation : NULL;
                                $contraction_count = isset($p->no_contractions) ? $p->no_contractions : NULL;
                                $fetal_hr_permin = isset($p->fetal_heart_rate_per_min) ? $p->fetal_heart_rate_per_min : NULL;
                                $systolic = isset($p->bloodpressure_systolic) ? $p->bloodpressure_systolic : NULL;
                                $diastolic = isset($p->bloodpressure_diastolic) ? $p->bloodpressure_diastolic : NULL;
                                $pr = isset($p->pulse) ? $p->pulse : NULL;
                                $cnt++;
                                ?>
                                <tr>
                                    <td><kbd>{{ $cnt }}</kbd></td>
                                    <td>{{ $time_taken }}</td>
                                    <td>{{ $cervical_dilation }} CM</td>
                                    <td>{{ $p->fetal_station }}</td>
                                    <td>{{ $p->amount_bleeding }}</td>
                                    <td>{{ $contraction_count }}</td>
                                    <td>{{ $fetal_hr_permin }}</td>
                                    <td>{{ $systolic }}/{{ $diastolic }}</td>
                                    <td>{{ $pr }}</td>
                                </tr>
                            <?php
                            }
                        } ?>
                        </tbody>
                    </table>
                </fieldset>
                <fieldset>
                    <legend> Partograph </legend>
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> Date Examined </label>
                            <div class="col-sm-4">
                                {!!Form::text('partographDateExamined',  null, ['class' => 'form-control datepicker', 'placeholder'=>'mm/dd/yyyy']); !!}
                            </div>
                        </div><!-- /.col-12 -->
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> Ruptured Membranes </label>
                            <div class="col-sm-4">
                                {!!Form::number('rupturedMembranes', null, ['class' => 'form-control', 'placeholder'=>'Ruptured Membranes', 'min'=>'0']); !!}
                            </div>
                            <label class="col-sm-2 control-label"> Rapid Assessment Result </label>
                            <div class="col-sm-4">
                                {!!Form::number('rapidAssessmentResult', null, ['class' => 'form-control', 'placeholder'=>'Rapid Assessment Result', 'min'=>'0']); !!}
                            </div>
                        </div>
                </fieldset>

                <fieldset>
                    <legend> Partograph Entry </legend>
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> Time Taken </label>
                            <div class="col-sm-4 bootstrap-timepicker">
                                {!! Form::text('partographTimeTaken', null, ['class' => 'form-control timepicker', 'placeholder'=>'hh:mm']); !!}
                            </div>
                            <label class="col-sm-2 control-label"> Cervical Dilation (CM) </label>
                            <div class="col-sm-4">
                                {!!Form::number('cervicalDilation', null, ['class' => 'form-control', 'placeholder'=>'Cervical Dilation (CM)', 'min'=>'0']); !!}
                            </div>
                        </div> <!-- /.col-12 -->
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> Fetal Station </label>
                            <div class="col-sm-4">
                                {!!Form::select('fetalStation',
                                    array(  NULL =>'--Select--',
                                            '-2' => '-2',
                                            '-1' => '-1',
                                            '0' => '0',
                                            '+1' => '+1',
                                            '+2' => '+2',
                                            '+3' => '+3',
                                            '+4' => '+4'),
                                    null, ['class' => 'form-control']) !!}
                            </div>
                            <label class="col-sm-2 control-label"> Amount of Bleeding </label>
                            <div class="col-sm-4">
                                {!!Form::select('amountOfBleeding',
                                    array(  NULL =>'--Select--',
                                            '0' => '0',
                                            '+' => '+',
                                            '++' => '++',
                                            '+++' => '+++'),
                                    null, ['class' => 'form-control']) !!}
                            </div>
                        </div> <!-- /.col-12 -->
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> No. of Contractions </label>
                            <div class="col-sm-4">
                                {!! Form::number('numContractions', null, ['class' => 'form-control', 'placeholder'=>'No. of Contractions', 'min'=>'0']) !!}
                            </div>
                            <label class="col-sm-2 control-label"> Fetal Heart Rate per Min </label>
                            <div class="col-sm-4">
                                {!! Form::number('fetalHeartRatePM', null, ['class' => 'form-control', 'placeholder'=>'Fetal Heart Rate per Min', 'min'=>'0']) !!}
                            </div>
                        </div> <!-- /.col-12 -->
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> Blood Pressure Systolic/Diastolic </label>
                            <div class="col-sm-2">
                                {!! Form::number('BPSystolic', null, ['class' => 'form-control', 'placeholder'=>'Systolic', 'min'=>'0']) !!}
                            </div>
                            <div class="col-sm-2">
                                {!! Form::number('BPDiastolic', null, ['class' => 'form-control', 'placeholder'=>'Diastolic', 'min'=>'0']) !!}
                            </div>
                            <label class="col-sm-2 control-label"> Pulse </label>
                            <div class="col-sm-4">
                                {!! Form::number('Pulse', null, ['class' => 'form-control', 'placeholder'=>'Pulse', 'min'=>'0']) !!}
                            </div>
                        </div> <!-- /.col-12 -->
                </fieldset>
                <fieldset class="textcenter">
                    <button class="btn btn-info btn-lg" id="updateParto">Update Partograph Chart</button>
                </fieldset>

                <fieldset>
                    <legend> Delivery </legend>
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> Placenta Delivered </label>
                            <div class="col-sm-4">
                                <div class="radio inline">
                                    <label>
                                        {!! Form::radio('maternalcare[partograph][placentaDelivered]', 1, ''); !!} Yes
                                    </label>
                                </div>
                                <div class="radio inline">
                                    <label>
                                        {!! Form::radio('maternalcare[partograph][placentaDelivered]', 0, ''); !!} No
                                    </label>
                                </div>
                            </div>
                            <label class="col-sm-2 control-label"> Amniotic Fluid Characteristic </label>
                            <div class="col-sm-4">
                                {!!Form::select('maternalcare[partograph][amnioticFluidChar]',
                                    array(  NULL =>'--Select--',
                                            '1' => 'Absent',
                                            '2' => 'Bloody',
                                            '3' => 'Clear',
                                            '4' => 'Meconium Stained'),
                                    null, ['class' => 'form-control']) !!}
                            </div>
                        </div> <!-- /.col-12 -->
                </fieldset>

            </div><!-- /.tab-pane -->

            <div class="tab-pane" id="maternalcare_delivery">

                    <fieldset>
                        <legend> Pregnancy Delivery</legend>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label"> Date of Delivery </label>
                                <div class="col-sm-4">
                                    {!!Form::text('maternalcare[delivery][deliveryDate]', $deliveryDate, ['class' => 'form-control', 'id'=>'datepicker', 'placeholder'=>'mm/dd/yyyy']); !!}
                                </div>
                                <label class="col-sm-2 control-label"> Time of Delivery </label>
                                <div class="col-sm-4 bootstrap-timepicker">
                                    {!! Form::text('maternalcare[delivery][deliveryTime]', $deliveryTime, ['class' => 'form-control', 'id'=>'timepicker', 'placeholder'=>'hh:mm']); !!}
                                </div>
                            </div><!-- /.col-12 -->
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label"> Outcome </label>
                                <div class="col-sm-4">
                                    {!!Form::select('maternalcare[delivery][pregnancyOutcome]',
                                    array(  NULL =>'--Select--',
                                            '001' => 'Live Birth - Full Term',
                                            '002' => 'Live Birth - Preterm',
                                            '003' => 'Multiple',
                                            '004' => 'Fetal Death in-utero or stillbirth',
                                            '005' => 'Ectopic',
                                            '006' => 'H-mole',
                                            '007' => 'Abortion',
                                            '008' => 'Dead',
                                            '009' => 'Problematic',
                                            '999' => 'Others',
                                            ), $termination_outcome, ['class' => 'form-control']) !!}
                                </div>
                                <label class="col-sm-2 control-label"> Child Gender </label>
                                <div class="col-sm-4">
                                    <div class="radio inline">
                                        <label>
                                            {!! Form::radio('maternalcare[delivery][childGender]', 'F', (($childGender=='F') ? true : '')); !!} Female
                                        </label>
                                    </div>
                                    <div class="radio inline">
                                        <label>
                                            {!! Form::radio('maternalcare[delivery][childGender]', 'M', (($childGender=='M') ? true : '')); !!} Male
                                        </label>
                                    </div>
                                </div>
                            </div><!-- /.col-12 -->
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label"> Livebirth Weight (grams) </label>
                                <div class="col-sm-4">
                                    {!! Form::number('maternalcare[delivery][livebirthWeight]', $livebirthWeight, ['class' => 'form-control', 'placeholder'=>'Livebirth Weight (grams)', 'min'=>'0']) !!}
                                </div>
                                <label class="col-sm-2 control-label"> Delivery Attended By </label>
                                <div class="col-sm-4">
                                    {!!Form::select('maternalcare[delivery][deliveryAttended]',
                                    array(  NULL =>'--Select--',
                                            '001' => 'Physician/Doctor',
                                            '002' => 'Nurse',
                                            '003' => 'Midwife',
                                            '004' => 'Traditional Birth Attendant (Hilot)',
                                            '999' => 'Other'),
                                    $attendant, ['class' => 'form-control']) !!}
                                </div>
                            </div><!-- /.col-12 -->
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label"> Delivery Type </label>
                                <div class="col-sm-4">
                                    {!!Form::select('maternalcare[delivery][deliveryType]',
                                    array(  NULL =>'--Select--',
                                            'N' => 'Normal',
                                            'O' => 'Operative'),
                                    $delivery_type, ['class' => 'form-control', 'id' => 'deliveryType_select']) !!}
                                </div>
                                <label class="col-sm-2 control-label"> Type of Delivery Place </label>
                                <div class="col-sm-4">
                                    {!!Form::select('maternalcare[delivery][deliveryPlaceType]',
                                    array(  NULL =>'--Select--',
                                            'NID' => 'Non-Institutional Delivery',
                                            'FB' => 'Facility-Based'),
                                    $delivery_place_type, ['class' => 'form-control', 'id' => 'deliveryPlace_select']) !!}
                                </div>
                            </div><!-- /.col-12 -->
                            <div class="col-xs-12">
                                <div id="deliveryType_mode" class="hidden">
                                    <label class="col-sm-2 control-label"> Delivery Type Mode </label>
                                    <div class="col-sm-4">
                                        {!!Form::select('maternalcare[delivery][deliveryTypeMode]',
                                        array(  NULL =>'--Select--',
                                                '01' => 'Vacuum Delivery',
                                                '02' => 'Forceps Delivery',
                                                '03' => 'Caeserian Section'),
                                        $delivery_type_mode, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div id="deliveryPlaceHead" class="hidden">
                                    <label class="col-sm-2 control-label"> Delivery Place </label>
                                    <div class="col-sm-4">
                                        {!!Form::select('maternalcare[delivery][deliveryplace_FB]',
                                        array(  NULL =>'--Select--',
                                                'bhs' => 'Barangay Health Station',
                                                'rhu' => 'Rural Health Unit',
                                                'hp' => 'Hospital',
                                                'LBC' => 'Lying-in / Birthing Clinics'),
                                        $delivery_place, ['class' => 'form-control hidden', 'id' => 'deliveryplace_FB']) !!}

                                        {!!Form::select('maternalcare[delivery][deliveryplace_NID]',
                                        array(  NULL =>'--Select--',
                                                'hme' => 'Home',
                                                'hsp' => 'In Transit',
                                                'oth' => 'Others'),
                                        $delivery_place, ['class' => 'form-control hidden', 'id' => 'deliveryplace_NID']) !!}
                                    </div>
                                </div>
                            </div><!-- /.col-12 -->
                            <div class="col-xs-12">
                                <label class="col-sm-2 control-label"> Birth Multiplicity </label>
                                <div class="col-sm-4">
                                    {!!Form::select('maternalcare[delivery][birth_multiplicity]',
                                    array(  NULL =>'--Select--',
                                            'M' => 'Multiple Gestation',
                                            'S' => 'Singleton'),
                                    $birth_multiplicity, ['class' => 'form-control', 'id' => 'multipleBirth_select']) !!}
                                </div>
                                <div id="multipleBirth" class="hidden">
                                    <label class="col-sm-2 control-label"> Birth Count </label>
                                    <div class="col-sm-4">
                                        {!!Form::select('maternalcare[delivery][multiple_births]',
                                        array( NULL =>'--Select--',
                                                '02' => 'Twins',
                                                '03' => 'Triplets',
                                                '04' => 'Quadruplets',
                                                '05' => 'Quintuplets',
                                                '06' => 'Sextuplets',
                                                '07' => 'Septuplets',
                                                '08' => 'Octuplets',
                                                '09' => 'Nonuplets',
                                                '10' => 'Decaplets',
                                                '11' => 'Undecaplets',
                                                '12' => 'Duodecaplets'),
                                        $multiple_births, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div><!-- /.col-12 -->
                        </div>

                    </fieldset>

            </div>

            <div class="tab-pane" id="maternalcare_postpartum">

                <input type="hidden" name="hservice_id" value="{{ $healthcareservice_id }}">
                <input type="hidden" name="mcservice_id" value="{{ $data->maternalcare_id }}">
                    <fieldset>
                        <legend> Postpartum Visits </legend>
                            <div class="col-sm-12">
                                <table class="col-sm-12">
                                    <tr>
                                        <td class="col-sm-4">
                                           <dt> Trimester </dt>
                                        </td>
                                        <td class="col-sm-3">
                                            <dt> Scheduled Visit </dt>
                                        </td>
                                        <td class="col-sm-3">
                                            <dt> Actual Visit </dt>
                                        </td>
                                        <td class="col-sm-2"> &nbsp; </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-12">
                             @if(!empty($postpartumdat))
                                @foreach($postpartumdat as $kpostpartum => $vpostpartum)
                                    @if(!empty($vpostpartum->visits))
                                        @foreach($vpostpartum->visits as $kvisit1 => $vvalue1)
                                        {!! Form::hidden('maternalcare[postpartum][postpartumVisitIdSaved][]',$vvalue1->maternalvisits_id); !!}
                                        <table class="col-sm-12">
                                            <tbody>
                                                <tr class="prenatal">
                                                    <td class="col-sm-4">
                                                        <dl>
                                                            <dd>
                                                                {!!Form::select(NULL, array(NULL=>'--Select--', '1' => 'First', '2' => 'Second', '3' => 'Third'), $vvalue1->trimester_period, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-3">
                                                        <dl>
                                                            <dd>
                                                                {!!Form::text(NULL, date('m/d/Y', strtotime($vvalue1->scheduled_visit)), ['class' => 'form-control datepicker_scheduled', 'placeholder'=>'mm/dd/yyyy', 'disabled'=>'disabled']); !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-3">
                                                        <dl>
                                                            <dd>
                                                                {!!Form::text('maternalcare[postpartum][postpartumActualVisitSaved][]', date('m/d/Y', strtotime($vvalue1->actual_visit)), ['class' => 'form-control datepicker', 'placeholder'=>'mm/dd/yyyy']); !!}
                                                            </dd>
                                                        </dl>
                                                    </td>
                                                    <td class="col-sm-2"> &nbsp; </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </div>
                            <div class="m_postpartumvisit col-sm-12">
                                <table class="col-sm-12">
                                    <tbody>
                                        <tr class="postpartum">
                                            <td class="hidden"><input type="text" name="id[]" value="0" class="id" /></td>
                                            <td class="col-sm-4">
                                                <dl>
                                                    <dd>
                                                        {!!Form::select('maternalcare[postpartum][postpartumTrimester][]', array(NULL=>'--Select--', '1' => 'First', '2' => 'Second', '3' => 'Third'), NULL, ['class' => 'form-control']) !!}
                                                    </dd>
                                                </dl>
                                            </td>
                                            <td class="col-sm-3">
                                                <dl>
                                                    <dd>
                                                        {!!Form::text('maternalcare[postpartum][postpartumScheduledVisit][]', NULL, ['class' => 'form-control datepicker_scheduled', 'placeholder'=>'mm/dd/yyyy']); !!}
                                                    </dd>
                                                </dl>
                                            </td>
                                            <td class="col-sm-3">
                                                <dl>
                                                    <dd>
                                                        {!!Form::text('maternalcare[postpartum][postpartumActualVisit][]', NULL, ['class' => 'form-control datepicker', 'placeholder'=>'mm/dd/yyyy']); !!}
                                                    </dd>
                                                </dl>
                                            </td>
                                            <td class="col-sm-2">
                                                <input type='button' class="delRow btn btn-default btn-sm" value="Delete" disabled="disabled">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="col-sm-12">
                                    <input type='button' class="add_ppvisit btn btn-default btn-sm" value="Add">
                                </div>
                            </div>
                    </fieldset>

                    <fieldset>
                        <legend> Micronutrient Supplementation </legend>
                            <div class="col-sm-12">
                            <table class="col-sm-12">
                                <tr>
                                    <td class="col-sm-4">
                                       <dt> Supplementation </dt>
                                    </td>
                                    <td class="col-sm-3">
                                        <dt> Date Given </dt>
                                    </td>
                                    <td class="col-sm-3">
                                        <dt> Quantity Dispensed </dt>
                                    </td>
                                    <td class="col-sm-2"> &nbsp; </td>
                                </tr>
                            </table>
                            </div>
                            <div class="col-sm-12">
                                 @if(!empty($postpartumdat))
                                    @foreach($postpartumdat as $postpartumkey => $postpartumvalue)
                                        @if(!empty($postpartumvalue->supplements))
                                            @foreach($postpartumvalue->supplements as $ksupplements1 => $vsupplements1)
                                            <table class="col-sm-12">
                                                <tbody>
                                                    <tr>
                                                        <td class="col-sm-4">
                                                            <dl>
                                                                <dd>
                                                                    {!!Form::select(NULL, array(NULL=>'--Select--','1' => 'Vitamin A', '2' => 'Iron with Folic Acid'), $vsupplements1->supplement_type, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
                                                                </dd>
                                                            </dl>
                                                        </td>
                                                        <td class="col-sm-3">
                                                            <dl>
                                                                <dd>
                                                                    {!!Form::text(NULL, date('m/d/Y', strtotime($vsupplements1->actual_date)), ['class' => 'form-control datepicker', 'id'=>'', 'placeholder'=>'mm/dd/yyyy', 'disabled'=>'disabled']); !!}
                                                                </dd>
                                                            </dl>
                                                        </td>
                                                        <td class="col-sm-3">
                                                            <dl>
                                                                <dd>
                                                                    {!! Form::number(NULL, $vsupplements1->quantity_dispensed, ['class' => 'form-control', 'placeholder'=>'Qty given', 'disabled'=>'disabled']) !!}
                                                                </dd>
                                                            </dl>
                                                        </td>
                                                        <td class="col-sm-2"> &nbsp; </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                            <div class="m_postpartummicronutrient col-sm-12">
                                <table class="col-sm-12">
                                    <tbody>
                                        <tr class="postpartum">
                                            <td class="hidden"><input type="text" name="id[]" value="0" class="id" /></td>
                                            <td class="col-sm-4">
                                                <dl>
                                                    <dd>
                                                        {!!Form::select('maternalcare[postpartum][postpartumSupplementation][]', array(NULL=>'--Select--','1' => 'Vitamin A', '2' => 'Iron with Folic Acid'), NULL, ['class' => 'form-control']) !!}
                                                    </dd>
                                                </dl>
                                            </td>
                                            <td class="col-sm-3">
                                                <dl>
                                                    <dd>
                                                        {!!Form::text('maternalcare[postpartum][postpartumSupplementationDateGiven][]', NULL, ['class' => 'form-control datepicker', 'placeholder'=>'mm/dd/yyyy']); !!}
                                                    </dd>
                                                </dl>
                                            </td>
                                            <td class="col-sm-3">
                                                <dl>
                                                    <dd>
                                                        {!! Form::number('maternalcare[postpartum][postpartumSupplementationQty][]', NULL, ['class' => 'form-control', 'min'=>'0']) !!}
                                                    </dd>
                                                </dl>
                                            </td>
                                            <td class="col-sm-2">
                                                <input type='button' class="delRow btn btn-default btn-sm" value="Delete" disabled="disabled">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="col-sm-12">
                                    <input type='button' class="add_ppsupp btn btn-default btn-sm" value="Add">
                                </div>

                            </div>
                    </fieldset>

                    <fieldset>
                        <legend> Breastfeeding </legend>
                        <div class="col-xs-12">
                            <label class="col-sm-2 control-label"> Date </label>
                            <div class="col-sm-4">
                                {!! Form::text('maternalcare[postpartum][postpartumBreastFeedingDate]', $postpartumBreastFeedingDate, ['class' => 'form-control', 'id'=>'datepicker', 'placeholder'=>'preterm', 'placeholder'=>'mm/dd/yyyy']); !!}
                            </div>
                        </div><!-- /.col-12 -->
                    </fieldset>


            </div>

        </div>
      </div>
    </div>
</div>

@section('plugin_jsscripts')
    <script type="text/javascript">
        function setEDD() {
            var myDate = new Date($("[name='maternalcare[prenatal][lastMenstruationPeriod]']").val());
            var dayOfMonth = myDate.getDate();
            myDate.setDate(dayOfMonth + 280);
            nm = myDate.getMonth()+1;
            nd = myDate.getDate();
            ny = myDate.getFullYear();
            var sh = new padder(2);
            dday = sh.pad(nm)+"/"+sh.pad(nd)+"/"+ny;
            $("#edd").val(dday);
        }

        $('#deliveryType_select').change(function(e) {
              var value = $.trim( this.value );
              if(value=='O' || value=='O') {
                $("#deliveryType_mode").removeClass("hidden");
              } else {
                $("#deliveryType_mode").addClass("hidden");
                $('#deliveryType_mode :selected').removeAttr("selected");
              }
          });
        $('#multipleBirth_select').change(function(e) {
              var value = $.trim( this.value );

              if(value=='M' || value=='m') {
                $("#multipleBirth").removeClass("hidden");
              } else {
                $("#multipleBirth").addClass("hidden");
                $('#multipleBirth :selected').removeAttr("selected");
              }
          });
        $('#deliveryPlace_select').change(function(e) {
              var value = $.trim( this.value );

              if(value=='FB') {
                $("#deliveryPlaceHead").removeClass("hidden");
                $("#deliveryplace_FB").removeClass("hidden");
                $("#deliveryplace_NID").addClass("hidden");
                $('#deliveryplace_NID :selected').removeAttr("selected");

              } else if(value=='NID') {
                $("#deliveryPlaceHead").removeClass("hidden");
                $("#deliveryplace_NID").removeClass("hidden");
                $("#deliveryplace_FB").addClass("hidden");
                $("#deliveryplace_FB :selected").removeAttr("selected");

              } else {
                $("#deliveryPlaceHead").addClass("hidden");
                $("#deliveryplace_FB").addClass("hidden");
                $("#deliveryplace_NID").addClass("hidden");
              }
          });
        // Make input field with datepicker_scheduled class as datepicker
        function datePicker() {

            var mindate = new Date('2018-01-01');
            var maxdate = new Date();
            maxdate.setDate(maxdate.getDate() + 365);
            console.log(maxdate);
            // someDate.setDate(someDate.getDate() + 15); //number  of days to add, e.x. 15 days
            // var dateFormated = someDate.toISOString().substr(0,10);

            $("input.datepicker_scheduled").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                "minDate": mindate,
                "maxDate": maxdate
            });
            $('input.datepicker_scheduled').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY'));
                $(this).attr('value', $('input.datepicker_scheduled').val());
              });
        }

        $(document).ready(function () {

            datePicker();

            var deliveryType_select = document.getElementById("deliveryType_select").value;
            var multipleBirth_select = document.getElementById("multipleBirth_select").value;
            var deliveryPlace_select = document.getElementById("deliveryPlace_select").value;

            console.log('deliveryPlace_select '+deliveryPlace_select);
            if(deliveryType_select=='O' || deliveryType_select=='O') {
                $("#deliveryType_mode").removeClass("hidden");
              } else {
                $("#deliveryType_mode").addClass("hidden");
                $('#deliveryType_mode :selected').removeAttr("selected");
              }

          if(multipleBirth_select=='M' || multipleBirth_select=='m') {
            $("#multipleBirth").removeClass("hidden");
          } else {
            $("#multipleBirth").addClass("hidden");
            $('#multipleBirth :selected').removeAttr("selected");
          }

              if(deliveryPlace_select=='FB') {
                $("#deliveryPlaceHead").removeClass("hidden");
                $("#deliveryplace_FB").removeClass("hidden");
                $("#deliveryplace_NID").addClass("hidden");
                $("#deliveryplace_NID :selected").removeAttr("selected");

              } else if(deliveryPlace_select=='NID') {
                $("#deliveryPlaceHead").removeClass("hidden");
                $("#deliveryplace_NID").removeClass("hidden");
                $("#deliveryplace_FB").addClass("hidden");
                $("#deliveryplace_FB :selected").removeAttr("selected");

              } else {
                $("#deliveryPlaceHead").addClass("hidden");
                $("#deliveryplace_FB").addClass("hidden");
                $("#deliveryplace_NID").addClass("hidden");
                $("#deliveryplace_NID :selected").removeAttr("selected");
                $("#deliveryplace_FB :selected").removeAttr("selected");
              }

            var id = 0;

        var count_prenatalvisit = 0;

        // Add button functionality
        $("div.m_prenatalvisit .add").click(function () {
            count_prenatalvisit++;
            var master = $(this).parents("div.m_prenatalvisit");
            // Get a new row based on the prenatal row
            var prot = master.find(".prenatal").clone();
            prot.attr("class", "");
            master.find("tbody").append(prot);
            console.log("m_prenatalvisit add "+count_prenatalvisit);
            datePicker();
            DeleteButton('m_prenatalvisit', count_prenatalvisit);
        });

        // Remove button functionality
        $('div.m_prenatalvisit').on("click", ".delRow", document.getElementById("prenatal"), function() {
            if( count_prenatalvisit>0) {
                count_prenatalvisit--;
                $(this).parents("tr").remove();
            }
            DeleteButton('m_prenatalvisit', count_prenatalvisit);
            console.log("m_prenatalvisit remove "+count_prenatalvisit);
        });

        function DeleteButton(divClone, type) {
            if(type>0) {
                $("div."+divClone+" .delRow").removeAttr("disabled");
            } else {
                $("div."+divClone+" .delRow").attr("disabled","disabled");
            }
        }

        var count_prenataltetanus = 0;

        $("div.m_prenataltetanus .add").click(function () {
            count_prenataltetanus++;
            var master = $(this).parents("div.m_prenataltetanus");
            // Get a new row based on the prenatal row
            var prot = master.find(".prenatal").clone();
            prot.attr("class", "");
            master.find("tbody").append(prot);
            console.log("m_prenataltetanus add "+count_prenataltetanus);
            datePicker();
            DeleteButton('m_prenataltetanus', count_prenataltetanus);
        });

        // Remove button functionality
        $('div.m_prenataltetanus').on("click", ".delRow", document.getElementById("prenatal"), function() {
            if( count_prenataltetanus>0) {
                count_prenataltetanus--;
                $(this).parents("tr").remove();
            }
            DeleteButton('m_prenataltetanus', count_prenataltetanus);
            console.log("m_prenataltetanus remove "+count_prenataltetanus);
        });

        var count_prenatalmicronutrient = 0;

        $("div.m_prenatalmicronutrient .add").click(function () {
            count_prenatalmicronutrient++;
            var master = $(this).parents("div.m_prenatalmicronutrient");
            // Get a new row based on the prenatal row
            var prot = master.find(".prenatal").clone();
            prot.attr("class", "");
            master.find("tbody").append(prot);

            console.log("m_prenatalmicronutrient add "+count_prenatalmicronutrient);
            datePicker();
            DeleteButton('m_prenatalmicronutrient', count_prenatalmicronutrient);
        });

        // Remove button functionality
        $('div.m_prenatalmicronutrient').on("click", ".delRow", document.getElementById("prenatal"), function() {
            if( count_prenatalmicronutrient>0) {
                count_prenatalmicronutrient--;
                $(this).parents("tr").remove();
            }
            DeleteButton('m_prenatalmicronutrient', count_prenatalmicronutrient);
            console.log("m_prenatalmicronutrient remove "+count_prenatalmicronutrient);
        });

         var count_postpartumvisit = 0;
        // Add button functionality
        $("div.m_postpartumvisit .add_ppvisit").click(function () {
            count_postpartumvisit++;
            var master = $(this).parents("div.m_postpartumvisit");
            // Get a new row based on the postpartum row
            var prot = master.find(".postpartum").clone();
            prot.attr("class", "");
            master.find("tbody").append(prot);
            console.log("m_postpartumvisit add "+count_postpartumvisit);
            datePicker();
            DeleteButton('m_postpartumvisit', count_postpartumvisit);
        });

        // Remove button functionality
        $('div.m_postpartumvisit').on("click", ".delRow", document.getElementById("postpartum"), function() {
            if( count_postpartumvisit>0) {
                count_postpartumvisit--;
                $(this).parents("tr").remove();
            }
            DeleteButton('m_postpartumvisit', count_postpartumvisit);
            console.log("m_postpartumvisit remove "+count_postpartumvisit);
        });

        var count_postpartummicronutrient = 0;
        $("div.m_postpartummicronutrient .add_ppsupp").click(function () {
            count_postpartummicronutrient++;
            var master = $(this).parents("div.m_postpartummicronutrient");
            // Get a new row based on the prenatal row
            var prot = master.find(".postpartum").clone();
            prot.attr("class", "");
            master.find("tbody").append(prot);

            console.log("m_postpartummicronutrient add "+count_postpartummicronutrient);
            datePicker();
            DeleteButton('m_postpartummicronutrient', count_postpartummicronutrient);
        });

        // Remove button functionality
        $('div.m_postpartummicronutrient').on("click", ".delRow", document.getElementById("postpartum"), function() {
            if( count_postpartummicronutrient>0) {
                count_postpartummicronutrient--;
                $(this).parents("tr").remove();
            }
            DeleteButton('m_postpartummicronutrient', count_postpartummicronutrient);
            console.log("m_postpartummicronutrient remove "+count_postpartummicronutrient);
        });

            $("#updateParto").click(function () {
                var partographDateExamined = $('input[name=partographDateExamined]').val();
                var rupturedMembranes = $('input[name=rupturedMembranes]').val();
                var rapidAssessmentResult = $('input[name=rapidAssessmentResult]').val();
                var partographTimeTaken = $('input[name=partographTimeTaken]').val();
                var cervicalDilation = $('input[name=cervicalDilation]').val();
                var fetalStation = $('select[name=fetalStation]').val();
                var amountOfBleeding = $('select[name=amountOfBleeding]').val();
                var numContractions = $('input[name=numContractions]').val();
                var fetalHeartRatePM = $('input[name=fetalHeartRatePM]').val();
                var BPDiastolic = $('input[name=BPDiastolic]').val();
                var BPSystolic = $('input[name=BPSystolic]').val();
                var Pulse = $('input[name=Pulse]').val();
                //check for valid form

                if(partographDateExamined == '' || rupturedMembranes == '' || rapidAssessmentResult == '' || partographTimeTaken == '' || cervicalDilation == '' || fetalStation == '' || amountOfBleeding == '' || numContractions == '' || fetalHeartRatePM == '' || BPDiastolic == '' || BPSystolic == '' || Pulse == '') {
                    alert('Please complete the form before updating.');
                } else {
                    $.ajax({
                        url : "{{ url('plugin/call/MaternalCare/MaternalCare/savePartograph/'.$healthcareservice_id) }}",
                        data : {
                            '_token' : $('input[name=_token]').val(),
                            hservice_id : $('input[name=hservice_id]').val(),
                            mcservice_id : $('input[name=mcservice_id]').val(),
                            partographDateExamined : partographDateExamined,
                            rupturedMembranes : rupturedMembranes,
                            rapidAssessmentResult : rapidAssessmentResult,
                            partographTimeTaken : partographTimeTaken,
                            cervicalDilation : cervicalDilation,
                            fetalStation : fetalStation,
                            amountOfBleeding : amountOfBleeding,
                            numContractions : numContractions,
                            fetalHeartRatePM : fetalHeartRatePM,
                            BPDiastolic : BPDiastolic,
                            BPSystolic : BPSystolic,
                            Pulse : Pulse
                        },
                        method: 'post',
                        async : false,
                        dataType : "json",
                        success : function( data ){
                            if(data.resp == '100'){
                                //let us update the table
                                $('#partographTable').html(data.partos);

                                //let us empty the fields
                                $('input[name=partographDateExamined]').val('');
                                $('input[name=rupturedMembranes]').val('');
                                $('input[name=rapidAssessmentResult]').val('');
                                $('input[name=partographTimeTaken]').val('');
                                $('input[name=cervicalDilation]').val('');
                                $('select[name=fetalStation]').val('');
                                $('select[name=amountOfBleeding]').val('');
                                $('input[name=numContractions]').val('');
                                $('input[name=fetalHeartRatePM]').val('');
                                $('input[name=BPDiastolic]').val('');
                                $('input[name=BPSystolic]').val('');
                                $('input[name=Pulse]').val('');
                            } else {
                                alert("Oops. Something went wrong.")
                            }
                        }
                    });
                }
            });
        });
    </script>
    @stop
