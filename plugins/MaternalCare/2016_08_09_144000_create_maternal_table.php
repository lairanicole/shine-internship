<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaternalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    
        if (Schema::hasTable('maternalcare')!=TRUE) { 
            Schema::create('maternalcare', function (Blueprint $table) {
                $table->increments('id');
                $table->string('maternalcare_id', 60);
                $table->string('maternalcase_id', 60);
                $table->string('healthcareservice_id', 60);
                
                $table->softDeletes();
                $table->timestamps();
                $table->unique('maternalcare_id');
            });
        }

        if (Schema::hasTable('maternalcare_prenatal')!=TRUE) { 
            Schema::create('maternalcare_prenatal', function (Blueprint $table) {
                $table->increments('id');
                $table->string('prenatal_id', 60);
                $table->string('maternalcare_id', 60);
                $table->string('maternalcase_id', 60);
                $table->dateTime('last_menstruation_period');
                $table->dateTime('expected_date_delivery');
                $table->integer('gravidity');
                $table->integer('parity');
                $table->integer('term');
                $table->integer('pre_term');
                $table->integer('abortion');
                $table->integer('living');

                $table->softDeletes();
                $table->timestamps();
                $table->unique('prenatal_id');
            });
        }

        if (Schema::hasTable('maternalcare_postpartum')!=TRUE) { 
            Schema::create('maternalcare_postpartum', function (Blueprint $table) {
                $table->increments('id');
                $table->string('postpartum_id', 60);
                $table->string('maternalcare_id', 60);
                $table->string('maternalcase_id', 60);
                $table->dateTime('breastfeeding_date');

                $table->softDeletes();
                $table->timestamps();
                $table->unique('postpartum_id');
            });
        }

        if (Schema::hasTable('maternalcare_delivery')!=TRUE) { 
            Schema::create('maternalcare_delivery', function (Blueprint $table) {
                $table->increments('id');
                $table->string('maternaldelivery_id', 60);
                $table->string('maternalcare_id', 60);
                $table->string('maternalcase_id', 60);

                $table->dateTime('termination_datetime');
                $table->enum('child_gender', ['F','M','U'])->default('U'); 
                $table->integer('livebirth_weight')->nullable();
                $table->string('termination_outcome', 60)->nullable();
                $table->string('delivered_type', 60)->nullable();
                $table->string('delivery_type_mode', 60)->nullable();
                $table->string('delivery_place_type', 60)->nullable();
                $table->string('delivery_place', 60)->nullable();
                $table->string('attendant', 60)->nullable();
                $table->string('birth_multiplicity', 60)->nullable();
                $table->string('multiple_births', 60)->nullable();

                $table->softDeletes();
                $table->timestamps();
                $table->unique('maternaldelivery_id');
            });
        }

        if (Schema::hasTable('maternalcare_partograph')!=TRUE) { 
            Schema::create('maternalcare_partograph', function (Blueprint $table) {
                $table->increments('id');
                $table->string('partograph_id', 60);
                $table->string('maternalcare_id', 60);
                $table->string('maternalcase_id', 60);

                $table->dateTime('date_examined');
                $table->string('ruptured_membranes', 100);
                $table->string('rapid_assessment_result', 100);
                $table->time('time_taken');
                $table->string('cervical_dilation', 100);
                $table->string('fetal_station', 100);
                $table->integer('amount_bleeding');
                $table->integer('no_contractions');
                $table->integer('fetal_heart_rate_per_min');
                $table->integer('bloodpressure_systolic');
                $table->integer('bloodpressure_diastolic');
                $table->string('bloodpressure_assessment', 100);
                $table->string('pulse', 100);
                $table->string('placenta_delivered', 100);
                $table->string('amniotic_fluid_characteristic', 100);
                
                $table->softDeletes();
                $table->timestamps();
                $table->unique('partograph_id');
            });
        }

        if (Schema::hasTable('maternalcare_supplements')!=TRUE) { 
            Schema::create('maternalcare_supplements', function (Blueprint $table) {
                $table->increments('id');
                $table->string('maternalsupplement_id', 60);
                $table->string('prenatal_id', 60);
                $table->string('postpartum_id', 60);
                $table->string('maternalcase_id', 60);

                $table->integer('pregnancy_type');
                $table->integer('supplement_type');
                $table->dateTime('scheduled_date');
                $table->dateTime('actual_date');
                $table->integer('quantity_dispensed');
                
                $table->softDeletes();
                $table->timestamps();
                $table->unique('maternalsupplement_id');
            });
        }

        if (Schema::hasTable('maternalcare_visits')!=TRUE) { 
            Schema::create('maternalcare_visits', function (Blueprint $table) {
                $table->increments('id');
                $table->string('maternalvisits_id', 60);
                $table->string('prenatal_id', 60);
                $table->string('postpartum_id', 60);
                $table->string('maternalcase_id', 60);

                $table->integer('trimester_period');
                $table->dateTime('scheduled_visit');
                $table->dateTime('actual_visit');

                $table->softDeletes();
                $table->timestamps();
                $table->unique('maternalvisits_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('maternalcare');
        Schema::drop('maternalcare_prenatal');
        Schema::drop('maternalcare_postpartum');
        Schema::drop('maternalcare_delivery');
        Schema::drop('maternalcare_partograph');
        Schema::drop('maternalcare_supplements');
        Schema::drop('maternalcare_visits');
    }
}
