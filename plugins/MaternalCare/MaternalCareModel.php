<?php
namespace Plugins\MaternalCare;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class MaternalCareModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'maternalcare';
    protected static $table_name = 'maternalcare';
    protected $primaryKey = 'maternalcare_id';

    protected $fillable = [];
    protected $touches = array('Healthcareservices');

    public function Healthcareservices() {
        return $this->belongsTo('ShineOS\Core\Healthcareservices\Entities\Healthcareservices', 'healthcareservice_id', 'healthcareservice_id');
    }

    public function prenatal()
    {
        DB::enableQueryLog();
        return $this->hasMany('Plugins\MaternalCare\MaternalCarePrenatalModel','maternalcase_id','maternalcase_id')->with('visits','supplements','immunizations');
    }

    public function partograph()
    {
        DB::enableQueryLog();
        return $this->hasMany('Plugins\MaternalCare\MaternalCarePartographModel','maternalcase_id','maternalcase_id');
    }

    public function deliveries()
    {
        DB::enableQueryLog();
        return $this->hasMany('Plugins\MaternalCare\MaternalCareDeliveryModel','maternalcase_id','maternalcase_id');
    }

    public function postpartum()
    {
        DB::enableQueryLog();
        return $this->hasMany('Plugins\MaternalCare\MaternalCarePostpartumModel','maternalcase_id','maternalcase_id')->with('visits','supplements');
    }

    public function scopeWithAndWhereHas($query, $relation, $constraint){
        return $query->whereHas($relation, $constraint)
                     ->with([$relation => $constraint]);
    }

}
