<?php
namespace Plugins\MaternalCare;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaternalCareImmunizationModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'patient_immunization';
    protected static $table_name = 'patient_immunization';
    protected $primaryKey = 'immunization_id';

    protected $fillable = [];

    public function maternal()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\MaternalCare\MaternalCareModel','maternalcase_id','subservice_id');
    }

    public function prenatal()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\MaternalCare\MaternalCarePrenatalModel','maternalcase_id','subservice_id');
    }

}
