<?php
namespace Plugins\MaternalCare;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaternalCareSupplementsModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'maternalcare_supplements';
    protected static $table_name = 'maternalcare_supplements';
    protected $primaryKey = 'maternalsupplement_id';

    protected $fillable = [];

    public function maternal()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\MaternalCare\MaternalCareModel','maternalcase_id','maternalcase_id');
    }
}
