<?php
namespace Plugins\MaternalCare;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class MaternalCareDeliveryModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'maternalcare_delivery';
    protected static $table_name = 'maternalcare_delivery';
    protected $primaryKey = 'maternaldelivery_id';

    protected $fillable = [];

    public function maternal()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\MaternalCare\MaternalCareModel','maternalcase_id','maternalcase_id');
    }

    public function scopeGender($query, $type)
    {
        if ($type != null) :
            $query->where('child_gender',$type);
        endif;

        return false;
    }

    public function scopeTerminationOutcome($query, $type)
    {
        if ($type != null) :
            $query->whereIn('termination_outcome',$type);
        endif;

        return false;
    }

    public function scopeDeliveryType($query, $type)
    {
        if ($type != null) :
            $query->where('delivered_type',$type);
        endif;

        return false;
    }

    public function scopeWeight($query, $type)
    {
        if ($type != null) :
            if($type == 'greater') :
                $query->where('livebirth_weight', '>=',$type);
            elseif($type == 'lesser') :
                $query->where('livebirth_weight', '<',$type);
            else :
                $query->where('livebirth_weight', '=', NULL);
            endif;
        endif;

        return false;
    }

    public function scopeAttendedBy($query, $type)
    {
        if ($type != null) :
            $query->where('attendant',$type);
        endif;

        return false;
    }

    public function scopeTypePlace($query, $type)
    {
        if ($type != null) :
            $query->where('delivery_place_type',$type);
        endif;

        return false;
    }

    public function scopePlace($query, $type)
    {
        if ($type != null) :
            if($type == 'hme'):
                $query->where('delivery_place',$type);
            else :
                $query->where('delivery_place','!=','hme');
            endif;
        endif;

        return false;
    }
}
