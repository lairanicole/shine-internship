<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMaternalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string', 'integer', 'datetime');
        
        if (Schema::hasTable('maternalcare')==TRUE) { 
            Schema::table('maternalcare', function(Blueprint $table) {
                $table->string('maternalcase_id', 60)->nullable()->change();
            });
        }
        
        if (Schema::hasTable('maternalcare_prenatal')==TRUE) { 
            Schema::table('maternalcare_prenatal', function(Blueprint $table) {
                if (Schema::hasColumn('maternalcare_prenatal', 'postpartum_id')!=TRUE) {
                    $table->string('postpartum_id', 60)->nullable();
                } 
                DB::statement('ALTER TABLE `maternalcare_prenatal` MODIFY `gravidity` INTEGER  NULL;');
                DB::statement('ALTER TABLE `maternalcare_prenatal` MODIFY `parity` INTEGER  NULL;');
                DB::statement('ALTER TABLE `maternalcare_prenatal` MODIFY `term` INTEGER  NULL;');
                DB::statement('ALTER TABLE `maternalcare_prenatal` MODIFY `pre_term` INTEGER  NULL;');
                DB::statement('ALTER TABLE `maternalcare_prenatal` MODIFY `abortion` INTEGER  NULL;');
                DB::statement('ALTER TABLE `maternalcare_prenatal` MODIFY `living` INTEGER  NULL;');
                DB::statement('ALTER TABLE `maternalcare_prenatal` MODIFY `last_menstruation_period` DATETIME  NULL;');
                DB::statement('ALTER TABLE `maternalcare_prenatal` MODIFY `expected_date_delivery` DATETIME  NULL;'); 
                
            });
        }
        
        if (Schema::hasTable('maternalcare_postpartum')==TRUE) { 
            Schema::table('maternalcare_postpartum', function(Blueprint $table) {
                $table->string('maternalcase_id', 60)->nullable()->change(); 
                DB::statement('ALTER TABLE `maternalcare_postpartum` MODIFY `breastfeeding_date` DATETIME  NULL;');  
            });
        }

        if (Schema::hasTable('maternalcare_delivery')==TRUE) { 
            Schema::table('maternalcare_delivery', function(Blueprint $table) {
                $table->string('maternalcase_id', 60)->nullable()->change(); 
                DB::statement('ALTER TABLE `maternalcare_delivery` MODIFY `termination_datetime` DATETIME  NULL;');
            });
        }

        if (Schema::hasTable('maternalcare_partograph')==TRUE) { 
            Schema::table('maternalcare_partograph', function(Blueprint $table) {
                $table->string('maternalcase_id', 60)->nullable()->change(); 
                DB::statement('ALTER TABLE `maternalcare_partograph` MODIFY `date_examined` DATETIME  NULL;');
                $table->string('ruptured_membranes', 100)->nullable()->change();
                $table->string('rapid_assessment_result', 100)->nullable()->change(); 
                DB::statement('ALTER TABLE `maternalcare_partograph` MODIFY `time_taken` TIME  NULL;');
                $table->string('cervical_dilation', 100)->nullable()->change();
                $table->string('fetal_station', 100)->nullable()->change();
                
                DB::statement('ALTER TABLE `maternalcare_partograph` MODIFY `amount_bleeding` INTEGER NULL;');
                DB::statement('ALTER TABLE `maternalcare_partograph` MODIFY `no_contractions` INTEGER NULL;');
                DB::statement('ALTER TABLE `maternalcare_partograph` MODIFY `fetal_heart_rate_per_min` INTEGER NULL;');
                DB::statement('ALTER TABLE `maternalcare_partograph` MODIFY `bloodpressure_systolic` INTEGER NULL;');
                DB::statement('ALTER TABLE `maternalcare_partograph` MODIFY `bloodpressure_diastolic` INTEGER NULL;');

                $table->string('bloodpressure_assessment', 100)->nullable()->change();
                $table->string('pulse', 100)->nullable()->change();
                $table->string('placenta_delivered', 100)->nullable()->change();
                $table->string('amniotic_fluid_characteristic', 100)->nullable()->change();

            });
        }

        if (Schema::hasTable('maternalcare_supplements')==TRUE) { 
            Schema::table('maternalcare_supplements', function(Blueprint $table) {
                $table->string('maternalcase_id', 60)->nullable()->change(); 

                DB::statement('ALTER TABLE `maternalcare_supplements` MODIFY `pregnancy_type` INTEGER  NULL;');
                DB::statement('ALTER TABLE `maternalcare_supplements` MODIFY `supplement_type` INTEGER  NULL;');
                DB::statement('ALTER TABLE `maternalcare_supplements` MODIFY `scheduled_date` DATETIME  NULL;');
                DB::statement('ALTER TABLE `maternalcare_supplements` MODIFY `actual_date` DATETIME  NULL;');

                $table->integer('quantity_dispensed')->nullable()->change();

            });
        }

        if (Schema::hasTable('maternalcare_visits')==TRUE) { 
            Schema::table('maternalcare_visits', function(Blueprint $table) {
                $table->string('maternalcase_id', 60)->nullable()->change(); 

                DB::statement('ALTER TABLE `maternalcare_visits` MODIFY `trimester_period` INTEGER NULL;');
                DB::statement('ALTER TABLE `maternalcare_visits` MODIFY `scheduled_visit` DATETIME  NULL;');
                DB::statement('ALTER TABLE `maternalcare_visits` MODIFY `actual_visit` DATETIME  NULL;');
            });
        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
