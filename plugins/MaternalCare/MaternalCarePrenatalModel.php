<?php
namespace Plugins\MaternalCare;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class MaternalCarePrenatalModel extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'maternalcare_prenatal';
    protected static $table_name = 'maternalcare_prenatal';
    protected $primaryKey = 'prenatal_id';

    protected $fillable = [];

    public function maternal()
    {
        DB::enableQueryLog();
        return $this->belongsTo('Plugins\MaternalCare\MaternalCareModel','maternalcase_id','maternalcase_id');
    }

    public function visits()
    {
     DB::enableQueryLog();
        return $this->hasMany('Plugins\MaternalCare\MaternalCareVisitsModel','prenatal_id','prenatal_id');
    }

    public function supplements()
    {
     DB::enableQueryLog();
        return $this->hasMany('Plugins\MaternalCare\MaternalCareSupplementsModel','prenatal_id','prenatal_id');
    }

    public function immunizations()
    {
     DB::enableQueryLog();
        return $this->hasMany('Plugins\MaternalCare\MaternalCareImmunizationModel','subservice_id','maternalcase_id');
    }
}
