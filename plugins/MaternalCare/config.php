<?php
//plugin ID (MANDATORY VALUE)
$plugin_id = 'MaternalCare';
//module owner (MANDATORY VALUE)
$plugin_module = 'healthcareservices';
//module folder (MANDATORY VALUE)
$plugin_folder = 'MaternalCare';
//UI location where plugin will be accessible (MANDATORY VALUE)
$plugin_location = 'dropdown';
//primary_key used to find data (MANDATORY VALUE)
$plugin_primaryKey = 'maternalcare_id';
//plugintable default; table_name custom table (MANDATORY VALUE)
$plugin_table = 'maternalcare';
//plugin relationship models
$plugin_relationship = array('prenatal','postpartum','partograph', 'deliveries');
//plugin tab list - arrangement used on tabs (MANDATORY VALUE)
$plugin_tabs_child = array('addservice', 'complaints', 'vitals', 'maternalcare_plugin', 'impanddiag' ,'medicalorders',  'disposition');
//plugin type (MANDATORY VALUE)
$plugin_type = "consultation";
//plugin gender condition (MANDATORY VALUE; use 'all' to denote all gender)
$plugin_gender = "F";
//plugin age condition (OPTIONAL; if range use 'min-max')
$plugin_age = "12";
//plugin maximum role to access this plugin (MANDATORY VALUE)
$plugin_role = 5;

//Descriptive definitions that will appear on the extensions page.
//plugin title (MANDATORY VALUE)
$plugin_title = 'OB-Gyn/Maternal Care';
//plugin description (MANDATORY VALUE)
$plugin_description = 'Maternal Care';
//plugin version (MANDATORY VALUE)
$plugin_version = '1.0';
//plugin developer (MANDATORY VALUE)
$plugin_developer = 'ShineLabs';
//plugin developer website
$plugin_url = 'http://www.shine.ph';
//plugin copyright
$plugin_copy = "2016";

//plugin tabs => title array (MANDATORY VALUE)
$plugin_tabs = [
    'addservice' => 'Basic Information',
    'disposition' => 'Disposition',
    'medicalorders' => 'Medical Orders',
    'vitals' => 'Vitals & Physical',
    'impanddiag' => 'Impressions and Diagnosis',
    'complaints' => 'Complaints',
    'maternalcare_plugin' => 'Maternal Care' //list the plugin last
];

//plugins tabs => models array (MANDATORY VALUE)
$plugin_tabs_models = [
    'complaints' => 'GeneralConsultation',
    'disposition' => 'Disposition',
    'medicalorders' => 'MedicalOrder',
    'vitals' => 'VitalsPhysical',
    'maternalcare_plugin' => 'MaternalCareModel' //list the plugin last
];
