<?php
$plugin_id = 'MentalHealth';                       //plugin ID
$plugin_module = 'healthcareservices';          //module owner
$plugin_location = 'dropdown';                  //UI location where plugin will be accessible
$plugin_primaryKey = 'mentalhealth_id';        //primary_key used to find data
$plugin_table = 'mentalhealth_service';            //plugintable default; table_name custom table
$plugin_tabs_child = array('addservice', 'complaints', 'vitals', 'mentalhealth_plugin', 'impanddiag', 'medicalorders', 'disposition'); //,
$plugin_type = 'program';
$plugin_gender = "all";
//plugin maximum role to access this plugin (MANDATORY VALUE)
$plugin_role = 5;

$plugin_relationship = array();
$plugin_folder = 'MentalHealth'; //module owner
$plugin_title = 'Mental Health';            //plugin title
$plugin_description = 'Mental Health';
$plugin_version = '1.0';
$plugin_developer = 'ShineLabs';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2018";

$plugin_tabs = [
    'addservice' => 'Basic Information',
    'complaints' => 'Complaints',
    'impanddiag' => 'Impressions & Diagnosis',
    'disposition' => 'Disposition',
    'medicalorders' => 'Medical Orders',
    'vitals' => 'Vitals & Physical',
    'mentalhealth_plugin' => 'Mental Health'
];

$plugin_tabs_models = [
    'complaints' => 'GeneralConsultation',
    'disposition' => 'Disposition',
    'medicalorders' => 'MedicalOrder',
    'vitals' => 'VitalsPhysical',
    'mentalhealth_plugin' => 'MentalHealthModel'
];