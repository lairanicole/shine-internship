<?php

use Plugins\MentalHealth\MentalHealthModel as MentalHealth;
use Modules\Helathcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

class MentalHealthController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        //no index
    }

    public function save($data)
    {
        $mentalHealth_id = $data['fpservice_id'];
        $hservice_id = $data['hservice_id'];
        $patient_id = $data['patient_id'];
        $mentalhealth_service = MentalHealth::where('mentalHealth_id','=',$mentalHealth_id)->first();

        if($mentalhealth_service == NULL) {
            $mentalhealth_service =  new MentalHealth;
            $mentalhealth_service->healthcareservice_id = $hservice_id;
            $mentalhealth_service->patient_id = $patient_id;
        
        }

        
        
        
        $mentalhealth_service->hallucinations = isset($data['hallucinations']) ? $data['hallucinations'] : NULL;
        $mentalhealth_service->illusions = isset($data['illusions']) ? $data['illusions'] : NULL;
        $mentalhealth_service->flight = isset($data['flight']) ? $data['flight'] : NULL;
        $mentalhealth_service->circumstantiality = isset($data['circumstantiality']) ? $data['circumstantiality'] : NULL;
        $mentalhealth_service->delusion = isset($data['delusion']) ? $data['delusion'] : NULL;
        $mentalhealth_service->suicidal = isset($data['suicidal']) ? $data['suicidal'] : NULL;
        $mentalhealth_service->looseness_of_associations = isset($data['looseness_of_associations']) ? $data['looseness_of_associations'] : NULL;
        $mentalhealth_service->tangentiality = isset($data['tangentiality']) ? $data['tangentiality'] : NULL;
        $mentalhealth_service->preoccupations = isset($data['preoccupations']) ? $data['preoccupations'] : NULL;
        $mentalhealth_service->oriented_to_time = isset($data['oriented_to_time']) ? $data['oriented_to_time'] : NULL;
        $mentalhealth_service->oriented_to_person = isset($data['oriented_to_person']) ? $data['oriented_to_person'] : NULL;
        $mentalhealth_service->oriented_to_place = isset($data['oriented_to_place']) ? $data['oriented_to_place'] : NULL;
        $mentalhealth_service->confabulation = isset($data['confabulation']) ? $data['confabulation'] : NULL;
        $mentalhealth_service->judgment = isset($data['judgment']) ? $data['judgment'] : NULL;

        $mentalhealth_service->general_description = Input::get('general_description');
        $mentalhealth_service->mood_and_affinity = Input::get('mood_and_affinity');
        $mentalhealth_service->speech_characteristics = Input::get('speech_characteristics');

        $mentalhealth_service->remote_memory = $data['remote_memory'];
        $mentalhealth_service->recent_past_memory = $data['recent_past_memory'];
        $mentalhealth_service->recent_memory = $data['recent_memory'];
        $mentalhealth_service->immediate_retention_and_recall = $data['immediate_retention_and_recall'];

        $mentalhealth_service->reading_and_writing = $data['reading_and_writing'];
        $mentalhealth_service->visual_spatial_ability = $data['visual_spatial_ability'];
        $mentalhealth_service->abstract_thought = $data['abstract_thought'];
        $mentalhealth_service->serial_7s = $data['serial_7s'];

        $mentalhealth_service->conciousness = $data['conciousness'];
        $mentalhealth_service->insight = $data['insight'];


        $mentalhealth_service->impulse_control = $data['impulse_control'];
        
        $mentalhealth_service->remarks = Input::get('remarks');

        $mentalhealth_service->save();
        header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);

        exit;
    }
}
?>