<?php
    function procDate($date) {
        if($date) {
            $d = date('m/d/Y', strtotime($date));
            return $d;
        } else {
            return NULL;
        }
    }
    $hservice_id = $allData['healthcareserviceid'];
    $fpservice_id = $data['mentalHealth_id'];
    $patient_id = $allData['patient']->patient_id;
    $currentdate = date("m/d/Y");

    //let us disable this form if this consult is already disposed
    if(empty($allData['disposition_record']['disposition'])) {
       $read = '';
     }
    else {
      $read = 'disabled';

    }

?>
{!! Form::hidden('mentalhealth[fpservice_id]',$fpservice_id) !!}
{!! Form::hidden('mentalhealth[hservice_id]',$hservice_id) !!}
{!! Form::hidden('mentalhealth[patient_id]',$patient_id) !!}
<legend>Mental Status Examination Form</legend>

<!-- First Textfield -->
<fieldset>
    <div class="form-group">
        <label class="col-md-2">General Description</label>
        <div class="col-md-10 ui-widget">
          {!! Form::textarea('general_description', $data['general_description'], ['class' => 'form-control noresize', 'placeholder' => 'General Description', 'cols'=>'10', 'rows'=>'5', $read]) !!}
        </div>
    </div>
</fieldset>

<!-- Second Textfield -->
<fieldset>
    <div class="form-group">
        <label class="col-md-2">Mood and Affectivity</label>
        <div class="col-md-10 ui-widget">
        {!! Form::textarea('mood_and_affinity', $data['mood_and_affinity'], ['class' => 'form-control noresize', 'placeholder' => 'Mood and Affectivity', 'cols'=>'10', 'rows'=>'5', $read]) !!}
        </div>
    </div>
</fieldset>

<!-- Third Textfield -->
<fieldset>
    <div class="form-group">
        <label class="col-md-2">Speech Characteristics</label>
        <div class="col-md-10 ui-widget">
        {!! Form::textarea('speech_characteristics', $data['speech_characteristics'], ['class' => 'form-control noresize', 'placeholder' => 'Speech Characteristics', 'cols'=>'10', 'rows'=>'5', $read]) !!}
        </div>
    </div>
</fieldset>

<hr>

<div class="icheck">
    <fieldset>
        <div class="form-group">

            <div class="col-md-12">
<!--Beginning or First Row -->
                <div class="col-md-12">
                <dt class="row">Perception</dt>

                    <!--First Column-->
                    <dl class="col-md-6">
                        <dd>

                            <!--First item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Hallucinations:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[hallucinations]" type="radio" value="1" <?php if($data['hallucinations']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[hallucinations]" type="radio" value="0" <?php if($data['hallucinations']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>
                        </dd>
                    </dl>

                    <!--Second Column-->
                    <dl class="col-md-6">
                        <dd>

                            <!--Second item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Illusions:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[illusions]" type="radio" value="1" <?php if($data['illusions']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[illusions]" type="radio" value="0" <?php if($data['illusions']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                        </dd>

                    </dl>
                </div>

<!-- Beginning of Second Row -->
                <div class="col-md-12">
                <dt class="row">Thought Content And Mental Trends</dt>
                    <!--First Column-->
                    <dl class="col-md-6">
                        <dd>

                            <!--First item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Flight:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[flight]" type="radio" value="1" <?php if($data['flight']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[flight]" type="radio" value="0" <?php if($data['flight']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                            <!--Third item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Circumstantiality:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[circumstantiality]" type="radio" value="1" <?php if($data['circumstantiality']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[circumstantiality]" type="radio" value="0" <?php if($data['circumstantiality']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                            <!--Fifth item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Delusion:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[delusion]" type="radio" value="1" <?php if($data['delusion']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[delusion]" type="radio" value="0" <?php if($data['delusion']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                            <!--Seventh item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Suicidal:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[suicidal]" type="radio" value="1" <?php if($data['suicidal']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[suicidal]" type="radio" value="0" <?php if($data['suicidal']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                        </dd>
                    </dl>

                    <!--Second Column-->
                    <dl class="col-md-6">
                        <dd>
                            <!--Second item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Looseness of Associations:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[looseness_of_associations]" type="radio" value="1" <?php if($data['looseness_of_associations']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[looseness_of_associations]" type="radio" value="0" <?php if($data['looseness_of_associations']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                            <!--Fourth item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Tangentiality:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[tangentiality]" type="radio" value="1" <?php if($data['tangentiality']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[tangentiality]" type="radio" value="0" <?php if($data['tangentiality']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                            <!--Sixth item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Preoccupations:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[preoccupations]" type="radio" value="1" <?php if($data['preoccupations']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[preoccupations]" type="radio" value="0" <?php if($data['preoccupations']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                        </dd>
                    </dl>

                </div>

<!-- Beginning of Third Row -->
                <div class="col-md-12">
                <dt class="row">Sensorium And Cognition</dt>
                    <!--First Column-->
                    <dl class="col-md-6">
                        <dd>

                            <!--First item -->
                            <div class="col-sm-12">
                            <label class="col-sm-4 control-label">Conciousness:</label>
                            <div class="col-sm-8 ">

                                <select class="form-control" name="mentalhealth[conciousness]" id="conciousness">
                                    <option value="">-- Choose Degree of Consciousness --</option>
                                    <option value="alert" <?php if(isset($data['conciousness']) && $data['conciousness'] == "alert" ) {echo "selected";} ?> >Alert</option>
                                    <option value="fugue" <?php if(isset($data['conciousness']) && $data['conciousness'] == "fugue" ) {echo "selected";} ?> >Fugue</option>
                                    <option value="cloudy" <?php if(isset($data['conciousness']) && $data['conciousness'] == "cloudy" ) {echo "selected";} ?> >Cloudy</option>
                                    <option value="somnolence" <?php if(isset($data['conciousness']) && $data['conciousness'] == "somnolence" ) {echo "selected";} ?> >Somnolence</option>
                                    <option value="coma" <?php if(isset($data['conciousness']) && $data['conciousness'] == "coma" ) {echo "selected";} ?> >Coma</option>

                                </select>
                            </div>
                            </div>
                            <!--Third item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Oriented to time:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[oriented_to_time]" type="radio" value="1" <?php if($data['oriented_to_time']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[oriented_to_time]" type="radio" value="0" <?php if($data['oriented_to_time']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                            <!--Fifth item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Oriented to Person:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[oriented_to_person]" type="radio" value="1" <?php if($data['oriented_to_person']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[oriented_to_person]" type="radio" value="0" <?php if($data['oriented_to_person']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                            <!--Seventh item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Oriented to Place:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[oriented_to_place]" type="radio" value="1" <?php if($data['oriented_to_place']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[oriented_to_place]" type="radio" value="0" <?php if($data['oriented_to_place']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                            <!--Ninth item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Confabulation:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[confabulation]" type="radio" value="1" <?php if($data['confabulation']=="1"){echo "checked";}?> {{$read}}>Yes
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[confabulation]" type="radio" value="0" <?php if($data['confabulation']=="0"){echo "checked";}?> {{$read}}>No
                                </label>
                                </div>
                            </div>

                        </dd>

                    </dl>

                    <!--Second Column-->
                    <dl class="col-md-6">
                        <dd>
                            <!--Second item -->
                            <div class="col-md-12">
                                <label class="col-md-4 control-label ">Remote Memory:</label>
                                    <div class="col-md-8">
                                        <input name="mentalhealth[remote_memory]" type="text" value="{{ $data['remote_memory'] or NULL }}" placeholder ="Intact/Impaired" class="form-control">
                                    </div>
                            </div>

                            <!--Fourth item -->
                            <div class="col-md-12">
                                <label class="col-md-4 control-label ">Recent Past Memory:</label>
                                    <div class="col-md-8">
                                        <input name="mentalhealth[recent_past_memory]" type="text" value="{{ $data['recent_past_memory'] }}" placeholder ="Intact/Impaired" class="form-control">
                                    </div>
                            </div>

                            <!--Sixth item -->
                            <div class="col-md-12">
                                <label class="col-md-4 control-label ">Recent Memory:</label>
                                    <div class="col-md-8">
                                        <input name="mentalhealth[recent_memory]" type="text" value="{{ $data['recent_memory'] }}" placeholder ="Intact/Impaired" class="form-control">
                                    </div>
                            </div>

                            <!--Eigth item -->
                            <div class="col-md-12">
                                <label class="col-md-4 control-label ">Immediate Retention and Recall:</label>
                                    <div class="col-md-8">
                                        <input name="mentalhealth[immediate_retention_and_recall]" type="text" value="{{ $data['immediate_retention_and_recall'] }}" placeholder ="Intact/Impaired" class="form-control">
                                    </div>
                            </div>

                        </dd>
                    </dl>

                </div>
<!-- Beginning of Fourth Row -->
            <div class="col-md-12">
                <dt class="row">Concentration and Attention</dt>
                    <!--First Column-->
                    <dl class="col-md-6">
                        <dd>

                            <!--First item -->
                            <div class="col-md-12">
                                <label class="col-md-4 control-label ">Reading and Writing:</label>
                                    <div class="col-md-8">
                                        <input name="mentalhealth[reading_and_writing]" type="text" value="{{ $data['reading_and_writing'] }}" placeholder ="Intact/Impaired" class="form-control">
                                    </div>
                            </div>
                            <!--Third item -->
                            <div class="col-md-12">
                                <label class="col-md-4 control-label ">Visual Spatial Ability:</label>
                                    <div class="col-md-8">
                                        <input name="mentalhealth[visual_spatial_ability]" type="text" value="{{ $data['visual_spatial_ability'] }}" placeholder ="Intact/Impaired" class="form-control">
                                    </div>
                            </div>

                        </dd>

                    </dl>
                    <!--Second Column-->
                    <dl class="col-md-6">
                        <dd>

                            <!--Second item -->
                            <div class="col-md-12">
                                <label class="col-md-4 control-label ">Abstract Thought:</label>
                                    <div class="col-md-8">
                                        <input name="mentalhealth[abstract_thought]" type="text" value="{{ $data['abstract_thought'] }}" placeholder ="Intact/Impaired" class="form-control">
                                    </div>
                            </div>
                            <!--Fourth item -->
                            <div class="col-md-12">
                                <label class="col-md-4 control-label ">Serial 7's:</label>
                                    <div class="col-md-8">
                                        <input name="mentalhealth[serial_7s]" type="text" value="{{ $data['serial_7s'] }}" placeholder ="Intact/Impaired" class="form-control">
                                    </div>
                            </div>

                        </dd>

                    </dl>
            </div>

<!-- Beginning of Fifth Row -->
            <div class="col-md-12">
                <dt class="row">Impulsivity</dt>
                    <!--First Column-->
                    <dl class="col-md-6">
                        <dd>

                            <!--First item -->
                            <div class="col-sm-12">
                                <label class="col-md-4 control-label">Impulse Control:</label>
                                    <div class="col-md-8">
                                        <input name="mentalhealth[impulse_control]" type="text" value="{{ $data['impulse_control'] }}" placeholder ="" class="form-control">
                                    </div>
                            </div>

                        </dd>

                    </dl>
            </div>

<!-- Beginning of Sixth Row -->
            <div class="col-md-12">
                <dt class="row">Judgment and Insight</dt>
                    <!--First Column-->
                    <dl class="col-md-6">
                        <dd>

                            <!--First item -->
                            <div class="col-sm-12">
                            <label class="col-sm-5 control-label">Judgment:</label>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[judgment]" type="radio" value="1" <?php if($data['judgment']=="1"){echo "checked";}?> {{$read}}>Good
                                </label>
                                </div>
                                <div class="radio-inline">
                                <label {{$read}}>
                                    <input name="mentalhealth[judgment]" type="radio" value="0" <?php if($data['judgment']=="0"){echo "checked";}?> {{$read}}>Poor
                                </label>
                                </div>
                            </div>

                        </dd>

                    </dl>

                    <!--Second Column-->
                    <dl class="col-md-6">
                        <dd>

                            <!--Second item -->
                            <div class="col-sm-12">
                            <label class="col-sm-4 control-label">Insight:</label>
                            <div class="col-sm-8 ">

                                    <select class="form-control" name="mentalhealth[insight]" id="insight">
                                    <option value="">-- Choose Degree of Insight --</option>
                                    <option value="denial" <?php if(isset($data['insight']) && $data['insight'] == "denial" ) {echo "selected";} ?> >Denial</option>
                                    <option value="aware" <?php if(isset($data['insight']) && $data['insight'] == "aware" ) {echo "selected";} ?> >Aware</option>
                                    <option value="intellectual_insight" <?php if(isset($data['insight']) && $data['insight'] == "intellectual_insight" ) {echo "selected";} ?> >Intellectual Insight</option>
                                    <option value="true_emotional_insight" <?php if(isset($data['insight']) && $data['insight'] == "true_emotional_insight" ) {echo "selected";} ?> >True Emotional Insight</option>

                                </select>
                              </select>
                            </div>
                            </div>

                        </dd>

                    </dl>
            </div>
        </div>
    </fieldset>
</div>
<hr>
<!-- Fourth Textfield -->
<fieldset>
    <div class="form-group">
        <label class="col-md-2">Remarks</label>
        <div class="col-md-10 ui-widget">
        {!! Form::textarea('remarks', $data['remarks'], ['class' => 'form-control noresize', 'placeholder' => 'Remarks', 'cols'=>'10', 'rows'=>'5', $read]) !!}
        </div>
    </div>
</fieldset>
