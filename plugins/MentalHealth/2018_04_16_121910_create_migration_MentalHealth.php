<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMigrationMentalHealth extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        if (Schema::hasTable('mentalhealth_service')!=TRUE) { 
            Schema::create('mentalhealth_service', function (Blueprint $table) {
                $table->increments('id');
                $table->string('mentalHealth_id', 32);
                $table->string('healthcareservice_id', 32);
              
                $table->text('general_description')->nullable();
                $table->text('mood_and_affinity')->nullable();
                $table->text('speech_characteristics')->nullable();

                $table->tinyInteger('hallucinations')->nullable();
                $table->tinyInteger('illusions')->nullable();

                $table->tinyInteger('flight')->nullable();
                $table->tinyInteger('looseness_of_associations')->nullable();
                $table->tinyInteger('circumstantiality')->nullable();
                $table->tinyInteger('tangentiality')->nullable();
                $table->tinyInteger('delusion')->nullable();
                $table->tinyInteger('preoccupations')->nullable();
                $table->tinyInteger('suicidal')->nullable();

                $table->text('conciousness')->nullable();
                $table->tinyInteger('oriented_to_time')->nullable();
                $table->tinyInteger('oriented_to_person')->nullable();
                $table->tinyInteger('oriented_to_place')->nullable();
                $table->string('remote_memory',8)->nullable();
                $table->string('recent_past_memory',8)->nullable();
                $table->string('recent_memory',8)->nullable();
                $table->string('immediate_retention_and_recall',8)->nullable();
                $table->tinyInteger('confabulation')->nullable();
                
                $table->string('reading_and_writing',8)->nullable();
                $table->string('visual_spatial_ability',8)->nullable();
                $table->string('abstract_thought',8)->nullable();
                $table->string('serial_7s',8)->nullable();

                $table->text('impulse_control')->nullable();

                $table->tinyInteger('judgment')->nullable();
                $table->text('insight')->nullable();

                $table->text('remarks')->nullable();

                $table->softDeletes();
                $table->timestamps();
                $table->unique('healthcareservice_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mentalhealth_service');
    }

}
