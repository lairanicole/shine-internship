<?php

namespace Shine\Http\Middleware; 

use Closure, Cache, Session, Auth;
        
class PatientAuth {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) { 
        $uri = $request->path();
        
        if (Session::has('roles')) {
            return $next($request);
        }

        Auth::logout();
        $request->session()->flush();
        return redirect('logout/111');
    }
}
