<?php

namespace Shine\Http\Middleware; 

use Closure;
use ShineOS\Core\API\Entities\ApiUserAccount;
use Illuminate\Http\JsonResponse;
use ShineOS\Core\API\Entities\APITrail;
use Shine\Libraries\IdGenerator;
        
class apiauth {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) { 
        $uri = $request->path();
        $method = $request->method();
        $input = $request->input();

        $api_key = $request->header('ShineKey');
        $api_secret = $request->header('ShineSecret');

        if(!empty($api_key) AND !empty($api_secret)) {
            $result = ApiUserAccount::where('api_key',$api_key)->where('api_secret',$api_secret)->first();
            if(!is_null($result)) {
                /**
                 * Save to Trail
                 */
                $apitrail = new APITrail(); 
                $apitrail->apitrail_id = IdGenerator::generateId();
                $apitrail->apiuseraccount_id = $result->apiuseraccount_id;
                $apitrail->api_key = $api_key;
                $apitrail->api_secret = $api_secret;
                $apitrail->path = $uri;
                $apitrail->method = $method;
                $apitrail->input = json_encode($input);
                $apitrail->save();

                return $next($request);
            }
        }

        return new JsonResponse(array('message'=>'Unauthorized Access'), 401);
    }
}
