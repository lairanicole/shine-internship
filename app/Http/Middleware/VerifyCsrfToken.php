<?php

namespace Shine\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Session, CLosure;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = ['api*'];

    public function handle($request, Closure $next){
		if($request->input('_token')) {
			if (Session::getToken() != $request->input('_token')) {
				return redirect()->guest('login')->with('warning', 'Your session has expired. Please reload and try logging in again.');
			}
		}
		return parent::handle($request, $next);
	}

}
