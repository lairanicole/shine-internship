<?php namespace Shine\Libraries;

use Modules\MDX\Entities\Medix;

class IdGenerator
{
	
	function __construct () {
		
	}
	
	/**
	 * Generates unique ID based on a given set of random numbers and timestamp
	 *
	 * @var int
	 */
	public static function generateId ( $prefix_length = 7 )
	{
		return self::randomNumber($prefix_length).date('yymmddhis');
	}

	/**
	 * Generates smaller unique ID based on a given set of random numbers and timestamp
	 *
	 * @var int
	 */
	public static function generateSmallId ( $prefix_length = 8 )
	{
		return self::randomNumber($prefix_length).date('ymdhis');
	}
	
	
	/**
	 * Generates random numbers
	 *
	 * @var int
	 */
	private static function randomNumber( $length = 7 ) {
		$result = '';

		for($i = 0; $i < $length; $i++) {
			$result .= mt_rand(0, 9);
		}

		return $result;
	}

	/**
	 * Generates random numbers
	 *
	 * @var int
	 */
	public static function generateMedixId($country_code, $app_instance_id, $app_ai = NULL) 
	{
		$medixid = '';

		$first = $country_code;
		if(strlen($country_code) < 4)
		{
			$first = sprintf("%04d", $country_code);
		}

		$third = $app_instance_id;
		if(strlen($app_instance_id) < 4);
		{
			$third = sprintf("%04d", $app_instance_id);
		}

		$countmedix = Medix::count();
		$app_id = sprintf("%08d", $countmedix+1);
		if($app_ai != NULL)
		{
			$app_id = sprintf("%08d", $app_ai);
		}

		$second = substr($app_id, 0, 4);
		$fourth = substr($app_id, -4);

		$nocheck = $medixid = $first.$second.$third.$fourth;
		$odd = $even = 0;
		$rev = strrev($nocheck);
		for ($i=0; $i < strlen($rev); $i++) { 
			if($i+1 % 2 == 0)
			{
				$even += $rev[$i];
			}
			else
			{
				$odd += $rev[$i];
			}
		}

		$remainder = ($odd + $even) % 10;
		if($remainder == 0)
		{
			$check = 0;
		}
		else
		{
			$check = 10 - $remainder;
		}
		$medixid[0] = $check;

		return $medixid;
	}
}
