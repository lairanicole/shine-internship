<?php
namespace Shine\Libraries;
use \App;

/**
 * Class SemephoreSMS Class handles the methods and properties of sending or receiving an SMS message.
 * The main inspiration of this class was from Nexmo PHP Library
 *
 * Usage: $var = new NexoMessage ( $account_key, $account_password );
 * Methods:
 *
 *      sendText($requestId, $to, $message)
 *      receiveTxt()
 *      reply()
 *      receiveNotifications()
 *
 */

class SemaphoreSMS {

    //authorization
    protected $clientId = '';
    protected $secretKey = '';
    protected $shortCode = '';
    protected $sslVerify = false;

    //Chikka's default URI for sending SMS
    protected $semaphoreSendURL = 'http://api.semaphore.co/api/v4/messages';

    protected $sendRequest = 'send';
    protected $receiveRequest = 'incoming';
    protected $replyRequest = 'reply';
    protected $notificationRequest = 'outgoing';

    private $requestCost = array(
        'free' => 'FREE',
        '1' =>1,
        '2.5'=> 2.5,
        '5'=> 5,
        '10' => 10,
        '15' => 15
        );


    private $expectedChikkaResponse = array(
        'message_type'=>'',
        'short_code' => '',
        'message_id' => '',
        'status' => '',
        'credits_cost' => '',
        'timestamp' => '');

    private $responseAccepted = array(
        'status' => 'Accepted',
        'message' => 'Message has been successfully processed.',
        'code' => 202
        );

    private $responseDenied = array(
        'status' => 'Error',
        'message' => 'Message has not been processed.',
        'code' => 400
        );

    /**
     * [__construct description]
     * @param [type] $clientId  [description]
     * @param [type] $secretKey [description]
     * @param [type] $shortCode [description]
     */
    public function __construct(){

    }


    /**
     * SendText allows sending of SMS message to Chikka API
     * @param type $requestId This identifier should be unique or your message will not be sent and you will be deducted
     * @param type $to  The mobile number you are sending an SMS
     * @param type $message The SMS message
     */

    public function sendText($messageID, $to, $message) {
        $messageID = strip_tags($messageID);

        //Request ID should not be blank
        if(strlen($messageID) < 1){
            trigger_error('Message ID is required');
            return false;
        }

        // Making sure strings are UTF-8 encoded
        if (!is_numeric($to) && !mb_check_encoding($to, 'UTF-8')) {
            trigger_error('TO needs to be a valid UTF-8 encoded string');
            return false;
        }

        if (!mb_check_encoding($message, 'UTF-8')) {
            trigger_error('Message needs to be a valid UTF-8 encoded string');
            return false;
        }

        //urlencode
        //$message = urlencode($message);

        //sendText post params
        $sendData = array(
            'number' => $to,
            'message' => $message,
            'sendername' => getenv('SEMAPHORE_SENDER_NAME')
            );

        //send Api request to Chikka and process it
        // dd($sendData);
        return $this->sendApiRequest($sendData,'sent');
    }

    /**
     * sendApiRequest - the functionality that sends request to Chikka API endpoint
     * @param  [array] $data post params
     * @return [object]
     */
    private function sendApiRequest($data,$messageType){
        $data = array_merge($data, array('apikey'=>getenv('SEMAPHORE_API_KEY')));
        // dd($data);
        //  build a request query from arrays of data
        $post = http_build_query($data);

        // If available, use CURL
        if (function_exists('curl_version')) {

            $to_semaphore = curl_init( $this->semaphoreSendURL );
            curl_setopt( $to_semaphore, CURLOPT_POST, true );
            curl_setopt( $to_semaphore, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $to_semaphore, CURLOPT_POSTFIELDS, $post );

            if (!$this->sslVerify) {
                curl_setopt( $to_semaphore, CURLOPT_SSL_VERIFYPEER, false);
            }

            $from_semaphore = curl_exec( $to_semaphore );
            curl_close ( $to_semaphore );

        } elseif (ini_get('allow_url_fopen')) {
            // No CURL available so try the awesome file_get_contents
            $opts = array('http' =>
                array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $post
                )
            );
            $context = stream_context_create($opts);
            $from_semaphore = file_get_contents($this->semaphoreSendURL, false, $context);

        } else {
            // No way of sending a HTTP post :(
            return false;
        }

        // dd($from_semaphore);
        return $this->parseApiResponse($from_semaphore, $messageType);
    }

    /**
     * parseApiResponse - process and handle Chikka api responses
     * @param  [array] $response    Response from Chikka API
     * @param  [string] $requestType This is the message type of the sms
     * @return [type]
     */
    private function parseApiResponse($response, $requestType = null){
        //combine the current response from Chikka and the message type that was requested
        var_dump($response);
        $response = json_decode($response,true);

        $final_response['responses'] = $response;
        if($requestType)
        {
            $final_response['request_type'] = $requestType;
        }           
        return json_decode(json_encode($final_response));
    }

}

?>
