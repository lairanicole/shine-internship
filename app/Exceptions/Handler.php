<?php

namespace Shine\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            // ajax 404 json feedback
            if ($request->ajax()) {
                return response()->json(['error' => 'Not Found'], 404);
            }
            // normal 404 view page feedback
            return response()->view('errors.404', [], 404);
        }

        if ($e instanceof HttpResponseException) {
            // ajax 404 json feedback
            if ($request->ajax()) {
                return response()->json(['error' => 'Not Found'], 404);
            }
            // normal 404 view page feedback
            return response()->view('errors.404', [], 404);
        }

        if ($e instanceof AuthenticationException) {
            // ajax 404 json feedback
            if ($request->ajax()) {
                return response()->json(['error' => 'Not Found'], 404);
            }
            // normal 404 view page feedback
            return response()->view('errors.404', [], 404);
        }

        if ($e instanceof InvalidArgumentException) {
            // ajax 404 json feedback
            if ($request->ajax()) {
                return response()->json(['error' => 'Not Found'], 404);
            }
            // normal 404 view page feedback
            return response()->view('errors.404', [], 404);
        }

        if ($e instanceof AuthorizationException) {
            // ajax 404 json feedback
            if ($request->ajax()) {
                return response()->json(['error' => 'Not Found'], 404);
            }
            // normal 404 view page feedback
            return response()->view('errors.404', [], 404);
        }

        if ($e instanceof TokenMismatchException){
            //Redirect to login form if session expires
            return redirect('login')->with('warning',"The page has expired, please try again. In the future, reload the page if it has been open for several hours.");
        }

        if ($e instanceof FatalErrorException){
            //Redirect to login with error
            $request->session()->flush();
            return redirect('login')->with('warning',"Fatal Error Exception. Please report this to the Administrator.");
        }

        return parent::render($request, $e);
    }
}
