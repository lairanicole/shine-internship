<?php

namespace Shine\Console\Commands;

use Illuminate\Console\Command; 
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Reports\Entities\M2;
use DB;

class MorbidityDataCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronReports:MorbidityDataCreate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron for collecting data reports for morbidity';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $m2 = NULL;
        //$facility = Session::get('facility_details');
        $facility = new Facilities;
        $fac = $facility::get()->pluck('facility_id');

        foreach($fac as $f) {
            //run datawarehouse chores
            //fhsis_m2
            $month = date('n');
            $year = date('Y');

            //process today's fhsis M2
            $diagnoses = DB::select( DB::raw(
            "SELECT TIMESTAMPDIFF(YEAR,b.birthdate,a.encounter_datetime) as 'age',
                b.gender,
                a.diagnosislist_id,
                a.code,
                MONTH(a.encounter_datetime) as 'diagnosisMonth',
                YEAR(a.encounter_datetime) as 'diagnosisYear',
                YEAR(a.datetime_death) as 'deathYear',
                a.facility_id,
                a.barangay,
                count(*) as 'count'
            FROM diagnosis_view a
            JOIN patients b ON b.patient_id = a.patient_id
            WHERE a.facility_id = '".$f."'
            AND a.barangay != ''
            AND a.diagnosislist_id != ''
            AND DATE(a.encounter_datetime) = '".date('Y-m-d')."'
            GROUP BY a.diagnosislist_id, a.code, age, b.gender, a.facility_id, a.barangay
            ORDER BY a.encounter_datetime ASC") );

            if($diagnoses) {
                foreach($diagnoses as $diagnosis) {
                    //check if this month is present
                    $rec = M2::where('diagnosislist_id', $diagnosis->diagnosislist_id)
                        ->where('age', $diagnosis->age)
                        ->where('gender', $diagnosis->gender)
                        ->where('diagnosisMonth', $diagnosis->diagnosisMonth)
                        ->where('diagnosisYear', $diagnosis->diagnosisYear)
                        ->where('facility_id', $diagnosis->facility_id)
                        ->where('brgycode', $diagnosis->barangay)
                        ->where('updated_at', '<', date('Y-m-d H:i:s'))
                        ->first();

                    if($rec) {
                        $rec->count = $rec->count + $diagnosis->count;
                        $rec->save();
                    } else {
                        $m2 = new M2();
                        $m2->age = $diagnosis->age;
                        $m2->gender = $diagnosis->gender;
                        $m2->diagnosislist_id = $diagnosis->diagnosislist_id;
                        $m2->icd10_code = $diagnosis->code;
                        $m2->diagnosisMonth = $diagnosis->diagnosisMonth;
                        $m2->diagnosisYear = $diagnosis->diagnosisYear;
                        $m2->deathYear = $diagnosis->deathYear;
                        $m2->facility_id = $diagnosis->facility_id;
                        $m2->brgycode = $diagnosis->barangay;
                        $m2->count = $diagnosis->count;
                        $m2->save();
                    }
                }
            }
        }

        if ($m2) :
            $this->comment(PHP_EOL."Done".PHP_EOL);
        else:
            $this->comment(PHP_EOL."No Data Found".PHP_EOL);
        endif;

    }
}
