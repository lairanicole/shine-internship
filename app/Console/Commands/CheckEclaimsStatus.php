<?php

namespace Shine\Console\Commands;

use Illuminate\Console\Command;

use Modules\Eclaims\Entities\Eclaims;
use Modules\Eclaims\Http\Controllers\ClaimController;

class CheckEclaimsStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eclaims:getstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to get status of all eclaims submission.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $claims = Eclaims::whereNotNull('eclaim_file_date')->orderBy('eclaim_file_date','DESC')->lists('eclaims_id');

        $bar = $this->output->createProgressBar(count($claims));
        $claimController = new ClaimController;
        foreach ($claims as $key => $claim) {
            try{
                $claimController->getClaimsStatus($claim,true);
            } catch (Exception $e) {
                $this->error('Error on Claim # '.$claim);
            }
            $bar->advance();
        }
        $bar->finish();
    }
}
