<?php

namespace Shine\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Shine\Console\Commands\Inspire::class,
        \Shine\Console\Commands\SavePlugin::class,
        \Shine\Console\Commands\FamilyPlanningData::class,
        \Shine\Console\Commands\MorbidityDataUpdate::class,
        \Shine\Console\Commands\MorbidityDataCreate::class,
        \Shine\Console\Commands\CheckEclaimsStatus::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //https://laravel.com/docs/5.4/scheduling
        // $filePath_updateM2 = base_path().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'cron_status_updateM2.txt';
        // $filePath_getDailyM2 = base_path().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'cron_status_getDailyM2.txt';
        // $filePath_getM1_FP = base_path().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'cron_status_getM1_FP.txt';
        // $email = 'desiree.alviento@gmail.com';

        // $getM2 = $schedule->command('cronReports:MorbidityDataCreate')
        //          // ->yearly()   //Run the task every year
        //          ->daily()   //Run the task every day at midnight
        //          ->sendOutputTo($filePath_updateM2)
        //          // ->emailOutputTo($email);
        //          ->withoutOverlapping();
                 

        // $getDailyM2 = $schedule->command('cronReports:MorbidityDataUpdate')

        //          // ->everyThirtyMinutes()   //Run the task every minute
        //          ->daily()         //Run the task every day at midnight
        //          ->sendOutputTo($filePath_getDailyM2)
        //          // ->emailOutputTo($email);
        //          ->withoutOverlapping();
                 

        // $getM1_FP = $schedule->command('cronReports:FamilyPlanningData')
        //          // ->everyThirtyMinutes()   //Run the task every minute
        //          ->daily()         //Run the task every day at midnight
        //          ->sendOutputTo($filePath_getM1_FP)
        //          // ->emailOutputTo($email);
        //          ->withoutOverlapping();


        $eclaimsGetStatusFile = base_path().DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'cron_status_eclaims_getstatus.txt';
        $eclaimsGetStatus = $schedule->command('eclaims:getstatus')
                 ->daily()         //Run the task every day at midnight
                 ->sendOutputTo($eclaimsGetStatusFile)
                 ->withoutOverlapping();

        // $this->comment(PHP_EOL.$getM2.PHP_EOL);
        // $this->comment(PHP_EOL.$getDailyM2.PHP_EOL);
        // $this->comment(PHP_EOL.$getM1_FP.PHP_EOL);

    }
}
