<?php

return [
	'name' => 'portal',
	'icon' => 'fa-user',
	'roles' => '["Patient"]',
	'version' => '1.0',
	'title' => 'Patient Portal Module',
	'folder' => 'Portal',
	'table' => '',
	'description' => 'The patient portal module is a module for patient facing application.',
	'module_user' => NULL,
	'developer' => 'MedixServe',
	'copy' => '2018',
	'url' => 'www.medixserve.com',
	'debug' => 1
];