<?php

# forgot password
Route::group(['prefix' => 'portal/forgotpassword', 'namespace' => 'Modules\Portal\Http\Controllers'], function()
{
    Route::get('/', 'PortalController@forgotpassword');
    Route::post('/send', 'PortalController@forgotpasswordSend');
    Route::get('/changepassword/{password_code}', 'PortalController@changepassword');
    Route::post('/changepassword_request', 'PortalController@changepassword_request');
});

Route::group(['prefix' => 'portal', 'namespace' => 'Modules\Portal\Http\Controllers', 'middleware' => 'auth.patient'], function()
{
    Route::get('/{id}', 'PortalController@dashboard');
    Route::get('/view/{id}', 'PortalController@view');
    Route::get('/hc/{id}', 'PortalController@hc');
    Route::get('/rx/{id}', 'PortalController@rx');
    Route::get('/labs/{id}', 'PortalController@labs');
    Route::get('/procedures/{id}', 'PortalController@proc');

    /*Route::post('/portal/{id}/update', 'PortalController@update')*/
    //New route for updates that sends data to blockchain
    Route::post('/{id}/update', function($id)
    {
        //insert track call here temporarily
        if($_POST) {
            //send post data to hyperledger
            sendBlock($_POST);
        }

        $controllerName = 'Modules\Portal\Http\Controllers\PortalController@update';
        return App::call($controllerName, [$id]);
    });
});

Route::group(['prefix' => 'portal/plugin', 'namespace' => 'ShineOS\Controllers', 'middleware' => 'auth.patient'], function()
{
    Route::any('/{plugin_name}/{all}', 'PluginController@index');
    Route::any('/call/{parent}/{plugin_name}/{all}/{ID}', 'PluginController@call');
    Route::any('/saveBlob/{parent}/{plugin_name}/{ID}', 'PluginController@saveBlob');
});