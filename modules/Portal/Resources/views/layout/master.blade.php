<!DOCTYPE html>
<html>
  <head>
      @include('partials.head')
  </head>
  <body class="skin-blue {{ substr(Request::path(), 0, strpos(Request::path(), '/') ) }} {{ $pagename }}" >
    <div class="wrapper">
        <header class="main-header">
            @include('portal::partials.header')
        </header>
        <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
            @include('portal::partials.sidebar')
      </aside>
      <!-- Content Wrapper. Contains page content -->
      
      <div class="content-wrapper">
        @yield('page-header')

        <section class="content">
            <div class="" id="patient-dashboard">
                @yield('list-content')
            </div>
        </section>
      </div>
    </div>

    @include('partials.footer')
  </body>
</html>
