<?php
  //$patient = Session::get('_global_user');
  //print_r($patient);
?>

<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
  <span class="sr-only">Toggle navigation</span>
</a>
<a href="{{ url('/')}}" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><img src="{{ asset('public/dist/img/shine-logo.png') }}"></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><img src="{{ asset('public/dist/img/shine-logo.png') }}"></span>
</a>

<nav class="navbar navbar-static-top" role="navigation">
      <div class="navbar-custom-menu">

      </div>
</nav>

<?php /*
<ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">
                <?php /*@if( Auth::check() )/ ?>
                    {{ $patient->first_name }} {{ $patient->last_name }}
                <?php /*@endif/ ?>
              </span>&nbsp;
              <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <div class="navimg_container">
                <?php
                    $userPhoto = $patient->photo_url;
                ?>
                @if ($userPhoto != '' AND is_file(userfiles_path().'uploads/profile_picture/'.$userPhoto) )
                    <img src="{{ url('public/uploads/patients/'.$userPhoto) }}" class="profile-img" />
                @else
                    <img src="{{ asset('public/dist/img/noimage_male.png') }}" class="profile-img" />
                @endif
                </div>
                <p>
                <?php /*@if( Auth::check() )/ ?>
                    {{ $patient->first_name }} {{ $patient->last_name }}
                    <?php $id = $patient->patient_id; //Auth::user()->user_id; ?>
                    <small>{{ date("F d, Y", strtotime($patient->birthdate)) }}</small>
                <?php /*@endif/ ?>
                </p>
              </li>
              <?php /*@if(isset($userFacilities))
                  <li class="user-body">
                    @foreach($userFacilities as $facilities)
                    <a href="#" class="user-facilities">{{ $facilities->facility_name }}</a>
                    @endforeach
                  </li>
              @endif/ ?>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="{{ URL::to('logout/'.$id) }}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
        */ ?>