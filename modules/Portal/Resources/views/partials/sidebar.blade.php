<section class="sidebar">
  <div class="user-panel">
    <div class="pull-left image hidden">
      <img src="{{ asset( 'public/dist/img/no_logo.png' ) }}" class="profile-img img-circle" />
    </div>
    <div class="pull-left info">
      <p>
        Patient Portal
      </p>
      <hr />
    </div>
  </div>

  <ul class="sidebar-menu">
    <li class="divider-bottom @if($pagename == 'dashboard') active @endif">
      <a href="{{ url('/portal/'.$patient->patient_id)}}" class="{{ Request::is('/') ? 'active' : '' }}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
      </a>
    </li>
    <li class="@if($pagename == 'view') active @endif">
      <a href="{{ url('/portal/view/'.$patient->patient_id)}}" class="{{ Request::is('/') ? 'active' : '' }}">
        <i class="fa fa-user-circle-o"></i> <span>Edit Profile</span>
      </a>
    </li>
    <li class="@if($pagename == 'consultations') active @endif">
      <a href="{{ url('/portal/hc/'.$patient->patient_id)}}" class="{{ Request::is('/') ? 'active' : '' }}">
        <i class="fa fa-stethoscope"></i> <span>Consultation History</span>
      </a>
    </li>
    <li class="@if($pagename == 'prescriptions') active @endif">
      <a href="{{ url('/portal/rx/'.$patient->patient_id)}}" class="{{ Request::is('/') ? 'active' : '' }}">
        <i class="fa fa-medkit"></i> <span>Prescription History</span>
      </a>
    </li>
    <li class="@if($pagename == 'laboratories') active @endif">
      <a href="{{ url('/portal/labs/'.$patient->patient_id)}}" class="{{ Request::is('/') ? 'active' : '' }}">
        <i class="fa fa-heartbeat"></i> <span>Laboratory History</span>
      </a>
    </li>
    <li class="@if($pagename == 'procedures') active @endif">
      <a href="{{ url('/portal/procedures/'.$patient->patient_id)}}" class="{{ Request::is('/') ? 'active' : '' }}">
        <i class="fa fa-h-square"></i> <span>Procedure History</span>
      </a>
    </li>
    <li class="divider-top">
        <a href="{{ URL::to('/logout/'.$patient->patient_id) }}">
          <i class="fa fa-power-off"></i> <span>Sign out</span>
        </a>
    </li>
  </ul>
</section>