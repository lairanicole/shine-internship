@section('heads')
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.css') !!}
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.skinFlat.css') !!}
{!! HTML::style('public/dist/css/flaticon.css') !!}
@stop

@extends('portal::layout.master')

@section('header-content') Dashboard @stop

@section('page-header')
  <section class="content-header">
      <h1 class="text-overflow">
          <i class="fa fa-fw fa-user-circle-o"></i> @yield('header-content')
      </h1>
      <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><a href="../records">Records</a></li>
      </ol>
  </section>
@stop

@section('list-content')
    <div class="col-md-3 padright0">
      <div class="box box-primary">
        <div class="box-header">

        </div><!-- /.box-header -->
        <div class="box-body box-profile">
          <!--do not forget to add style for profile-user-img-->
          <?php
              $imgsrc = asset('public/dist/img/noimage_male.png');

              if($patient->photo_url) {
                  if(is_file('public/uploads/patients/'.$patient->photo_url)) {
                    $imgsrc = asset('public/uploads/patients/'.$patient->photo_url);
                  }
              }
          ?>
          <div class="img_container" style="background: #FFFFFF url('{{ $imgsrc }}') center center no-repeat; background-size:cover"></div>

          <h3 class="text-center">{{ $patient->first_name }} {{ $patient->moddile_name}} {{ $patient->last_name}} </h3>
          <p align="center">
              <a href="{{ url('/portal/view', [$patient->patient_id]) }}" class="btn btn-xs bg-yellow black">Edit my Profile <i class="fa fa-external-link-square"></i></a>
          </p>
          <ul class="nav">
            <li>
              <label>Status</label>
              <p class="text-muted green">
                @if (!empty($patient->deleted_at))
                  Inactive
                @else
                  Active
                @endif
              </p>
            </li>
            <li>
              <label>Recorded on </label>
              <p class="text-muted">
                {{ $patient->created_at }}
              </p>
            </li>
            <li>
              <label>Created by </label>
              <p class="text-muted">
                @if($creator)
                {{ $creator->first_name." ".$creator->last_name }}
                @endif
              </p>
            </li>
            <?php if(isset($plugs)) {
              foreach($plugs as $k=>$p) {
                  if($p['plugin'] == 'Philhealth' AND $p['pdata']) { ?>
                      <li>
                          <label>PhilHealth No. </label>
                          <p class="text-muted">
                            {{ getPHICMemberType($plugs[$k]['pdata']->member_type) }} : {{ $plugs[$k]['pdata']->philhealth_id }}
                            <br />{{ returnPhilhealthMemberCategory($plugs[$k]['pdata']->philhealth_category) }}
                            @if($plugs[$k]['pdata']->indigenous_group == 1)
                              <br />Indigineous <i class="fa fa-check red"></i>
                            @endif
                          </p>
                      </li>
                  <?php }
              }
            } ?>
          </ul>
        </div><!-- /.box-body -->
      </div>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title blue"><i class="fa fa-coins margin-r-5"></i> MDX Tokens</h3>
        </div>
        <div class="box-body">
            <label> MDX ID: </label> <p> {{ $medix->medix_id or '3016502001234' }} </p>
            <label> Token Points: </label> <p> {{ $medix->tokens or '400.00' }} </p>
        </div>
      </div>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title blue"><i class="fa fa-vcard margin-r-5"></i> Patient Information</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <label>Sex</label>
          <p class="text-muted">
            {{ getGender($patient->gender) }}
          </p>

          <label>Birth Date</label>
          <p class="text-muted">{{ dateFormat($patient->birthdate,"M. d, Y") }}</p>

          <label>Age</label>
          <p class="text-muted">{{ getAge($patient->birthdate) }} years old</p>

          @if($patient->patientContact->email)
          <label>Email Address</label>
          <p class="text-muted">{{ $patient->patientContact->email }}</p>
          @endif
          @if($patient->patientContact->mobile)
          <label>Mobile No.</label>
          <p class="text-muted">{{ $patient->patientContact->mobile }}</p>
          @endif
          <label>Address/Barangay</label>
          <p>{{ $patient->patientContact->street_address }}
             {{ getBrgyName($patient->patientContact->barangay) }}</p>

          <label>City/Province</label>
          <p>{{ getCityName($patient->patientContact->city) }} @if($patient->patientContact->province), {{ getProvinceName($patient->patientContact->province) }}@endif</p>

          <label>Region</label>
          <p>{{ getRegionName($patient->patientContact->region) }}</p>

          <label>ZIP Code</label>
          <p>{{ $patient->patientContact->zip }}</p>

          <label>Country</label>
          <p>{{ $patient->patientContact->country or "n/a" }}</p>
        </div><!-- /.box-body -->
      </div>
    </div>

    <div class="col-md-9">
      <div class="row">
        <div class="col-md-12">
          @if (Session::has('message'))
            <div class="alert alert-dismissible alert-success">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <p>{{ Session::get('message') }}</p>
            </div>
          @endif
        </div>
      </div>

      @if($patient->patientDeathInfo != null)
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title blue"><i class="fa fa-certificate"></i> Death Info</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-12">
          <label>Immediate Cause of Death: <small>(Disease, injury or complication that led directly to death)</small></label><p>{{ $patient->patientDeathInfo->Immediate_Cause_of_Death }}</p>
          <label>Antecedent Cause of Death: <small>(Conditions giving rise to the immediate cause of death)</small></label><p>{{ $patient->patientDeathInfo->Antecedent_Cause_of_Death }}</p>
          <label>Underlying Cause of Death: <small>(Disease or injury that initiated the train of events leading directly to death)</small></label><p>{{ $patient->patientDeathInfo->Underlying_Cause_of_Death }}</p>
          </div>
          <div class="col-md-4">
          <label>Place:</label>
          <p>
              @if($patient->patientDeathInfo->PlaceDeath == 'FB')
                Facility-Based
              @else
                Non-Institutional
              @endif
          </p>
          </div>
          <div class="col-md-4">
          <label>Date:</label><p>{{ date('m/d/Y', strtotime($patient->patientDeathInfo->datetime_death)) }}</p>
          </div>
          <div class="col-md-12">
          <label>Notes:</label><pre>{{ $patient->patientDeathInfo->Remarks }}</pre>
          </div>
        </div>
      </div>
      @endif

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title blue"><i class="fa fa-stethoscope"></i> Recent Consultation</h3>
        </div><!-- /.box-header -->

        <?php if(isset($currentConsultation)) {
                $childConsultation = findHealthRecordChild($currentConsultation->healthcareservice_id);
            ?>
        <div class="box-body">
              
              <div class="row">
                <div class="col-lg-6">
                  <label>Consultation Date:</label>
                  <p class="text-muted">{{ date("F d, Y", strtotime($currentConsultation->encounter_datetime)) }}</p>

                  <?php
                      $previousConsultation = findHealthRecordByServiceID($currentConsultation->parent_service_id);
                    ?>
                  @if($currentConsultation->parent_service_id AND $previousConsultation)
                    <label>Followup from:</label>
                    <p class="text-muted">{{ date("F d, Y", strtotime($previousConsultation->encounter_datetime)) }}</p>
                  @endif

                  <label>Consultation Type:</label>
                  <p class="text-muted">
                      {{ getHealthcareServiceName($currentConsultation->healthcareservicetype_id) }}
                      @if($currentConsultation->healthcareservicetype_id == 'GeneralConsultation')
                       -
                      {{ getMedicalCategoryName($currentConsultationData->medicalcategory_id) }}
                      @endif
                  </p>

                  <label>Complaint:</label>
                  <p class="text-muted">{{ $currentConsultationData->complaint or "No complaint given" }}</p>

                  <label>Attending Physician:</label>
                  <p class="text-muted">
                      @if($dr = getUserFullNameByFacilityUserID($currentConsultation->seen_by) AND $dr  != 'User does not exist')
                      {{ $dr }} [{{ getFacilityNameByFacilityUserID($currentConsultation->seen_by) }}]
                      @endif
                  </p>
                </div>

                <div class="col-lg-6">
                  <label>Diagnosis:</label>
                  <p class="text-muted">{{ getDiagnosisByHealthServiceID($currentConsultation->healthcareservice_id) }}</p>

                  <label>Medical Order:</label>
                  <p class="text-muted">
                      <?php
                        $ord = NULL;
                        $orders = getMedicalOrdersByHealthServiceID($currentConsultation->healthcareservice_id);
                        foreach($orders as $order) {
                            if($ord != $order->medicalorder_type) {
                                switch($order->medicalorder_type) {
                                    case "MO_MED_PRESCRIPTION": echo "Prescription"; break;
                                    case "MO_LAB_TEST": echo "Laboratory Examination"; break;
                                    case "MO_PROCEDURE": echo "Medical Procedure"; break;
                                    case "MO_IMMUNIZATION": echo "Immunization"; break;
                                    case "MO_OTHER": echo "Other"; break;
                                }
                                echo ", ";
                                $ord = $order->medicalorder_type;
                            }
                        }
                      ?>
                  </p>
                  @if($disposition AND $disposition->disposition)
                  <label>Disposition:</label>
                  <p class="text-muted">{{ getDispositionName($disposition->disposition) }}<br> </p>
                  @endif
                </div>
              </div>

              <hr />

              <h4 class="blue">Recent Vitals</h4>
              <div class="">
                <div class="col-md-3 summary-data height-box">
                    <b class="wt">{{ $currentVitals->height or "n/a" }}</b>
                </div>
                <div class="col-md-6 summary-data" style="min-height:230px;">
                    <br />
                    <div class="item wtb"><b>{{ $currentVitals->weight or "n/a" }}</b><label>Weight (kgs)</label></div>
                    <div class="lborder item wtb"><b>{{ $currentVitals->temperature or "n/a" }}&deg;</b><label>Temp (C)</label></div>
                    <div class="lborder item wtb"><b>{{ $patient->blood_type or "n/a" }}</b><label>Blood Type</label></div>
                    <br clear="all" />
                    <hr />
                    <div class="col-sm-12">
                        <input id="bmi_range" type="text" name="bmi_range" value="0;100" data-type="single" data-step="2" data-from="23" data-slider="false" data-hasgrid="true" disabled: disabled !important;/>
                    </div>
                    <label>BMI</label>
                </div>
                <div class="col-md-3 summary-data centered">
                    <h4 class="leftaligned">Heart</h4>
                    <i class="flaticon-heart27 font50 red"></i><br />
                    <b class="font30" style="width:130px;"><span style="border-bottom:2px solid #333;">{{ $currentVitals->bloodpressure_systolic or "n/a" }}</span><br/>{{ $currentVitals->bloodpressure_diastolic or "n/a" }}</b>
                    <br />
                    <div class="item centered paddingl-10 fullw">
                        <span><span class="slabel centered">hr</span>{{ $currentVitals->heart_rate or "n/a" }}</span>
                        <span><span class="slabel centered">pr</span>{{ $currentVitals->pulse_rate or "n/a" }}</span>
                        <span><span class="slabel centered">rr</span>{{ $currentVitals->respiratory_rate or "n/a" }}</span>
                    </div>
                </div>
                <br clear="all" />
              </div>

              @if($RecentMedicalOrderLabExam)
              <hr />
              <h4 class="blue">Recent Lab Requests</h4>
              <div class="box-body table-responsive no-padding no-more-tables" id="">
                    <table class="table table-hover">
                        <thead class="cf">
                          <tr>
                            <th>Laboratory</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($RecentMedicalOrderLabExam as $lab_key => $lab_value)
                            <tr>
                              <td data-title="Laboratory">
                                  @if( isset($lab_value->laboratory_test_type) )
                                  {{ $labb = getLabName($lab_value->laboratory_test_type) }}
                                  @else
                                  {{ $labb = $lov_laboratories[$lab_value->laboratory_test_type] }}
                                  @endif

                                  @if($labb == 'Others')
                                  {{ ($lab_value->laboratory_test_type_others != "" OR $lab_value->laboratory_test_type_others != NULL) ? " : ".$lab_value->laboratory_test_type_others : " : Not specified" }}
                                  @endif
                              </td>
                              <td data-title="Status">
                                @if($lab_value->LaboratoryResult!=NULL)
                                  <?php $disabled_upload = TRUE; ?>
                                  <span class="label label-success">Uploaded: {{ date('M d, Y h:i A', strtotime($lab_value->LaboratoryResult->created_at)) }}
                                      </span>
                                @else
                                  <?php $disabled_upload = FALSE; ?>
                                  <span class="label label-warning">Pending: {{ date('M d, Y h:i A', strtotime($lab_value->created_at)) }}
                                      </span>
                                @endif
                              </td>
                              <td data-title="Action">
                                @if($lab_value->laboratory_test_type =='BR')
                                  <?php $Modal_labType = "lab_completebloodcount"; ?>
                                @elseif($lab_value->laboratory_test_type =='UR')
                                  <?php $Modal_labType = "lab_urinalysis"; ?>
                                @elseif($lab_value->laboratory_test_type =='FE')
                                  <?php $Modal_labType = "lab_fecalysis"; ?>
                                @else
                                  <?php $Modal_labType = "labModal"; ?>
                                @endif

                                @if(!$disabled_upload)
                                  <a href="#" class="btn btn-block btn-default btn-sm labClick" data-toggle="modal" data-id="{{ $lab_value->medicalorderlaboratoryexam_id }}" data-target="#{{ $Modal_labType }}" > Upload result </a>
                                @else
                                  <a href="{{ URL::to('laboratory/modal/'.$lab_value->laboratory_test_type.'/'.$lab_value->medicalorderlaboratoryexam_id) }}" class="btn btn-block btn-default btn-sm" data-toggle="modal" data-target="#myInfoModal"> View </a>
                                @endif
                              </td>
                            </tr>
                          @endforeach
                      </tbody>
                    </table>
                </div><!-- /.box-body -->
              @endif

              @if(!$RecentMedicalOrderPrescription->isEmpty())
              <hr />
              <h4 class="blue">Recent Prescriptions</h4>
              <div class="box-body table-responsive no-padding no-more-tables" id="">
                <table class="table table-hover">
                    <thead class="cf">
                      <tr>
                        <th>#</th>
                        <th>Prescription Date</th>
                        <th>Generic</th>
                        <th>Dosage</th>
                        <th>Intake</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $c = 0; ?>
                      @foreach($RecentMedicalOrderPrescription as $prescription)
                        <tr>
                          <td data-title="#">
                              <?php $c++; ?> {{ $c }}
                          </td>
                          <td data-title="Prescription Date">
                             {{ date('M d, Y', strtotime($prescription->created_at)) }}
                          </td>
                          <td data-title="Generic">
                             {{ $prescription->generic_name }}
                          </td>
                          <td data-title="Dosage">
                             {{ $prescription->dose_quantity }}
                          </td>
                          <td data-title="Intake">
                            <?php $Duration_Intake = explode(" ", $prescription->duration_of_intake); ?>
                            <?php
                                $di = isset($Duration_Intake[0]) ? $Duration_Intake[0] : NULL;
                                $dio = isset($Duration_Intake[1]) ? $Duration_Intake[1] : NULL;
                                $din = isset($Duration_Intake[1]) ? getIntakeName($Duration_Intake[1]) : NULL;
                            ?>
                              @if($prescription->duration_of_intake != ' C')
                                {{ $di." ".$din }}
                                @else
                                {{ $di." ".$din }}
                                @endif
                             {{ " - ".getRegimenName($prescription->dosage_regimen) }}
                          </td>
                        </tr>

                      @endforeach
                  </tbody>
                </table>
              </div><!-- /.box-body -->
              @endif

            </div><!-- /.box-body -->
        <?php } else { ?>
          <div class="box-body">
            <h4 class="orange">No recent consultation visit</h4>
          </div>
        <?php } ?>

      </div>

      @if(count($patients_monitoring))
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title blue"><i class="fa fa-heartbeat"></i> Health Tracker</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
          </div><!-- /.box-header -->
          <div class="box-body no-border table-responsive no-padding overflowx-hidden no-more-tables">
              <table class="table table-hover table-responsive datatable no-more-tables">
                  <thead>
                    <tr>
                      <th>Blood Pressure</th>
                      <th>Blood Pressure Assessment</th>
                      <th>Created At</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach ($patients_monitoring as $mkey => $mvalue)
                      <tr>
                        <td data-table="Blood Pressure"> {{ $mvalue->bloodpressure_systolic }} / {{ $mvalue->bloodpressure_diastolic }}</td>
                        <td data-table="BP Assessment"> {{ $mvalue->bloodpressure_assessment_name }} </td>
                        <td data-table="Date">{{ date('M. d, Y', strtotime($mvalue->created_at)) }}</td>
                      </tr>
                    @endforeach
                </tbody>
              </table>
          </div><!-- /.box-body -->
        </div>
      @endif

      @if($patient->patientMedicalHistory)
        <?php $medhistory = FALSE; ?>
        @foreach($patient->patientMedicalHistory as $history)
            @if($history->disease_status != NULL OR $history->disease_status != "")
                <?php $medhistory = TRUE; ?>
            @endif
        @endforeach
        @if($medhistory == TRUE)
      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title blue"><i class="fa fa-history"></i> Medical History</h3>
            </div>
            <div class="box-body">
                @foreach($patient->patientMedicalHistory as $history)
                    @if($history->disease_status != NULL OR $history->disease_status != "")
                    <p><label>{{ Shine\Libraries\Utils\Lovs::getValueOfFieldBy('diseases', 'disease_name', 'disease_id', $history->disease_id) }}</label> : {{ $history->disease_status }}</p>
                    @endif
                @endforeach
            </div>
      </div>
        @endif
      @endif

      @if($hc_history)
      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title blue"><i class="fa fa-calendar-check-o"></i> Consultation History</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body no-border table-responsive no-padding overflowx-hidden">
                  <table class="table table-hover table-heading table-datatable table-align-top table-responsive no-more-tables">
                    <thead class="cf">
                      <tr>
                        <th>Consultation Date</th>
                        <th>Clinical Service</th>
                        <th>Diagnosis</th>
                        <th>Type</th>
                        <th>Orders </th>
                        <th>Status</th>
                        <th>Physician</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach($hc_history as $history) { ?>
                    <tr class='row-clicker' onclick="location.href='{{ url('healthcareservices/edit', [$patient->patient_id, $history['healthcareservice_id']] ) }}'">
                      <td data-title="Consultation Date">{{ date('m/d/y', $history['dater']) }}</td>
                      <td data-title="Clinical Service">{{ getHealthcareServiceName($history['type']) }}</td>
                      <td data-title="Diagnosis">{{ $history['diagnosis_type'] }}</td>
                      <td data-title="Type">{{ getConsultTypeName($history['consultype']) }}</td>
                      <td align="center" data-title="Orders">
                        <?php
                          $ord = NULL;
                          if($history['orders']) {
                              foreach($history['orders'] as $order) {
                                  if($ord != $order->medicalorder_type) {
                                      switch($order->medicalorder_type){
                                          case "MO_MED_PRESCRIPTION": echo "<span class='fa fa-pencil'></span> "; break;
                                          case "MO_LAB_TEST": echo "<span class='fa fa-flask'></span> "; break;
                                      }
                                      $ord = $order->medicalorder_type;
                                  }
                              }
                          }
                        ?>
                      </td>
                      <td align="center" data-title="Status">
                          <?php
                            if($history['disposition'] AND $history['disposition']->disposition):
                                echo "<span class='fa fa-lock'></span>";
                            else:
                                echo "<span class='fa fa-unlock'></span>";
                            endif;
                          ?>
                      </td>

                        <?php if($history['seen']):
                            $seenby = $history['seen']->first_name." ".$history['seen']->last_name;
                        else:
                            $seenby = NULL;
                        endif; ?>
                      <td data-title="Physician">{{ $seenby }}</td>
                    </tr>
                    <?php } ?>
                  </tbody></table>
                </div><!-- /.box-body -->
          </div>
      @endif

      @if(count($MedicalOrderLabExam))
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title blue"><i class="fa fa-flask"></i> Patient Lab History</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body no-border table-responsive no-padding overflowx-hidden">
                    <table class="table table-hover table-responsive no-more-tables">
                        <thead class="cf">
                          <tr>
                            <th>Laboratory</th>
                            <th>Date Requested</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($MedicalOrderLabExam as $lab_key => $lab_value)
                            <tr>
                              <td data-title="Laboratory">
                                  @if( isset($lab_value->laboratory_test_type) )
                                  {{ $labb = getLabName($lab_value->laboratory_test_type) }}
                                  @else
                                  {{ $labb = $lov_laboratories[$lab_value->laboratory_test_type] }}
                                  @endif

                                  @if($labb == 'Others')
                                  {{ ($lab_value->laboratory_test_type_others != "" OR $lab_value->laboratory_test_type_others != NULL) ? " : ".$lab_value->laboratory_test_type_others : " : Not specified" }}
                                  @endif
                              </td>
                              <td data-title="Date Requested">
                                 {{ date('M d, Y', strtotime($lab_value->created_at)) }}
                              </td>
                              <td data-title="Status">
                                @if($lab_value->LaboratoryResult!=NULL)
                                  <?php $disabled_upload = TRUE; ?>
                                  <span class="label label-success">Uploaded: {{ date('M d, Y h:i A', strtotime($lab_value->LaboratoryResult->created_at)) }}
                                      </span>
                                @else
                                  <?php $disabled_upload = FALSE; ?>
                                  <span class="label label-warning">Pending: {{ date('M d, Y h:i A', strtotime($lab_value->created_at)) }}
                                      </span>
                                @endif
                              </td>
                              <td data-title="Action">
                                @if($lab_value->laboratory_test_type =='BR')
                                  <?php $Modal_labType = "lab_completebloodcount"; ?>
                                @elseif($lab_value->laboratory_test_type =='UR')
                                  <?php $Modal_labType = "lab_urinalysis"; ?>
                                @elseif($lab_value->laboratory_test_type =='FE')
                                  <?php $Modal_labType = "lab_fecalysis"; ?>
                                @else
                                  <?php $Modal_labType = "labModal"; ?>
                                @endif

                                @if(!$disabled_upload)
                                  <a href="#" class="btn btn-block btn-default btn-sm labClick" data-toggle="modal" data-id="{{ $lab_value->medicalorderlaboratoryexam_id }}" data-target="#{{ $Modal_labType }}" > Upload result </a>
                                @else
                                  <a href="{{ URL::to('laboratory/modal/'.$lab_value->laboratory_test_type.'/'.$lab_value->medicalorderlaboratoryexam_id) }}" class="btn btn-block btn-default btn-sm" data-toggle="modal" data-target="#myInfoModal"> View </a>
                                @endif
                              </td>
                            </tr>
                          @endforeach
                      </tbody>
                    </table>
                </div><!-- /.box-body -->
          </div>
      @endif

      @if(count($MedicalOrderPrescription))
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title blue"><i class="fa fa-shield"></i> Prescription History</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body no-border table-responsive no-padding overflowx-hidden">
                <table class="table table-hover datatable table-responsive no-more-tables">
                    <thead class="cf">
                      <tr>
                        <th>#</th>
                        <th>Prescription Date</th>
                        <th>Generic</th>
                        <th>Qty to<br />Dispense</th>
                        <th>Dosage</th>
                        <th>Intake</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $c = 0; ?>
                      @foreach($MedicalOrderPrescription as $prescription)
                        <tr>
                          <td data-title="#">
                              <?php $c++; ?> {{ $c }}
                          </td>
                          <td data-title="Prescription Date">
                             {{ date('M d, Y', strtotime($prescription->created_at)) }}
                          </td>
                          <td data-title="Generic">
                             {{ $prescription->generic_name }}
                          </td>
                          <td data-title="Qty">
                             {{ $prescription->total_quantity }}
                          </td>
                          <td data-title="Dosage">
                             {{ $prescription->dose_quantity }}
                          </td>
                          <td data-title="Intake">
                            <?php $Duration_Intake = explode(" ", $prescription->duration_of_intake); ?>
                            <?php
                                $di = isset($Duration_Intake[0]) ? $Duration_Intake[0] : NULL;
                                $dio = isset($Duration_Intake[1]) ? $Duration_Intake[1] : NULL;
                                $din = isset($Duration_Intake[1]) ? getIntakeName($Duration_Intake[1]) : NULL;
                            ?>
                              @if($prescription->duration_of_intake != ' C')
                                {{ $di." ".$din }}
                                @else
                                {{ $di." ".$din }}
                                @endif
                             {{ " - ".getRegimenName($prescription->dosage_regimen) }}
                          </td>
                        </tr>

                      @endforeach
                  </tbody>
                </table>
            </div><!-- /.box-body -->
          </div>
      @endif
    </div>

@include('laboratory::modal.modal_lab_completebloodcount')
@include('laboratory::modal.modal_lab_urinalysis')
@include('laboratory::modal.modal_lab_fecalysis')
@include('laboratory::modal.modal_laboratory_result')
@stop

@section('scripts')
<div class="modal fade" id="myInfoModal" tabindex="-1" role="dialog" aria-labelledby="myInfoModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myInfoModalLabel"> Healthcare Record Preview </h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

{!! HTML::script('public/dist/plugins/ionslider/ion.rangeSlider.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $("#bmi_range").ionRangeSlider({
            disable: true
        });
    });

    $("#myInfoModal").on("show.bs.modal", function(e) {
        $(this).find(".modal-content").html("");
        $(this).find(".modal-content").attr("style", "");
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });

    $('#iframeButton').on('click', function(){
        $("#printable").get(0).contentWindow.print();
    })

    $(document).on("click", ".labClick", function () {
      var lab_dataTarget = $(this).data('target');
      var medicalorderlaboratoryexam_id = $(this).data('id');
      $(lab_dataTarget+" #medicalorderlaboratoryexam_id").val( medicalorderlaboratoryexam_id );

    });
</script>
@stop
