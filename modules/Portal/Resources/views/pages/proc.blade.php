@section('heads')
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.css') !!}
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.skinFlat.css') !!}
{!! HTML::style('public/dist/css/flaticon.css') !!}
@stop

@extends('portal::layout.master')

@section('header-content') Procedures @stop

@section('page-header')
  <section class="content-header">
      <h1 class="text-overflow">
          <i class="fa fa-fw fa-h-square"></i> @yield('header-content')
      </h1>
      <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><a href="../records">Records</a></li>
      </ol>
  </section>
@stop

@section('list-content')
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title blue">Procedures History</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div><!-- /.box-header -->
    <div class="box-body no-border table-responsive no-padding overflowx-hidden">
      @if(count($MedicalOrderProcedures))
        <table class="table table-hover datatable table-responsive no-more-tables">
            <thead class="cf">
              <tr>
                <th>#</th>
                <th>Procedure Date</th>
                <th>Procedure Order</th>
                <th>Results</th>
              </tr>
            </thead>
            <tbody>
                <?php $c = 0; ?>
              @foreach($MedicalOrderProcedures as $procedure)
                <tr>
                  <td data-title="#">
                      <?php $c++; ?> {{ $c }}
                  </td>
                  <td data-title="Procedure Date">
                      {{ date('M d, Y', strtotime($procedure->created_at)) }}
                  </td>
                  <td data-title="Procedure Order">
                      {{ $procedure->procedure_order }}
                  </td>
                  <td data-title="Results">
                      &nbsp;
                  </td>
                </tr>

              @endforeach
          </tbody>
        </table>
      @endif
    </div><!-- /.box-body -->

  </div>

@stop

@section('scripts')

@stop
