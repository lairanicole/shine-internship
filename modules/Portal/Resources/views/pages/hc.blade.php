@section('heads')
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.css') !!}
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.skinFlat.css') !!}
{!! HTML::style('public/dist/css/flaticon.css') !!}
@stop

@extends('portal::layout.master')

@section('header-content') Consultations @stop

@section('page-header')
  <section class="content-header">
      <h1 class="text-overflow">
          <i class="fa fa-fw fa-stethoscope"></i> @yield('header-content')
      </h1>
      <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><a href="../records">Records</a></li>
      </ol>
  </section>
@stop

@section('list-content')
      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title blue">Consultation History</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div><!-- /.box-header -->
            @if($hc_history)
              <div class="box-body no-border table-responsive no-padding overflowx-hidden">
                <table class="table table-hover table-heading table-datatable table-align-top table-responsive no-more-tables">
                    <thead class="cf">
                      <tr>
                        <th>Consultation Date</th>
                        <th>Clinical Service</th>
                        <th>Diagnosis</th>
                        <th>Type</th>
                        <th>Orders </th>
                        <th>Status</th>
                        <th>Physician</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach($hc_history as $history) { ?>
                    <tr class='row-clicker' onclick="location.href='{{ url('healthcareservices/edit', [$patient->patient_id, $history['healthcareservice_id']] ) }}'">
                      <td data-title="Consultation Date">{{ date('m/d/y', $history['dater']) }}</td>
                      <td data-title="Clinical Service">{{ getHealthcareServiceName($history['type']) }}</td>
                      <td data-title="Diagnosis">{{ $history['diagnosis_type'] }}</td>
                      <td data-title="Type">{{ getConsultTypeName($history['consultype']) }}</td>
                      <td align="center" data-title="Orders">
                        <?php
                          $ord = NULL;
                          if($history['orders']) {
                              foreach($history['orders'] as $order) {
                                  if($ord != $order->medicalorder_type) {
                                      switch($order->medicalorder_type){
                                          case "MO_MED_PRESCRIPTION": echo "<span class='fa fa-pencil'></span> "; break;
                                          case "MO_LAB_TEST": echo "<span class='fa fa-flask'></span> "; break;
                                      }
                                      $ord = $order->medicalorder_type;
                                  }
                              }
                          }
                        ?>
                      </td>
                      <td align="center" data-title="Status">
                          <?php
                            if($history['disposition'] AND $history['disposition']->disposition):
                                echo "<span class='fa fa-lock'></span>";
                            else:
                                echo "<span class='fa fa-unlock'></span>";
                            endif;
                          ?>
                      </td>

                        <?php if($history['seen']):
                            $seenby = $history['seen']->first_name." ".$history['seen']->last_name;
                        else:
                            $seenby = NULL;
                        endif; ?>
                      <td data-title="Physician">{{ $seenby }}</td>
                    </tr>
                    <?php } ?>
                  </tbody></table>
              </div><!-- /.box-body -->
            @else
              <div class="box-body no-border overflowx-hidden">
                <h4>You have no healthcare history yet.</h4>
                <p>When you visit your doctor for consultation, data will appear here. Right now it seems you are a healthy person. Congratulations!!</p>
              </div>
            @endif
          </div>

    </div>

@stop

@section('scripts')

@stop
