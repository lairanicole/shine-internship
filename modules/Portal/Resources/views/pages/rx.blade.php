@section('heads')
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.css') !!}
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.skinFlat.css') !!}
{!! HTML::style('public/dist/css/flaticon.css') !!}
@stop

@extends('portal::layout.master')

@section('header-content') Prescriptions @stop

@section('page-header')
  <section class="content-header">
      <h1 class="text-overflow">
          <i class="fa fa-fw fa-medkit"></i> @yield('header-content')
      </h1>
      <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><a href="../records">Records</a></li>
      </ol>
  </section>
@stop

@section('list-content')
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title blue">Prescription History</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div><!-- /.box-header -->
    <div class="box-body no-border table-responsive no-padding overflowx-hidden">
      @if(count($MedicalOrderPrescription))
        <table class="table table-hover datatable table-responsive no-more-tables">
            <thead class="cf">
              <tr>
                <th>#</th>
                <th>Prescription Date</th>
                <th>Generic</th>
                <th>Qty to<br />Dispense</th>
                <th>Dosage</th>
                <th>Intake</th>
              </tr>
            </thead>
            <tbody>
                <?php $c = 0; ?>
              @foreach($MedicalOrderPrescription as $prescription)
                <tr>
                  <td data-title="#">
                      <?php $c++; ?> {{ $c }}
                  </td>
                  <td data-title="Prescription Date">
                      {{ date('M d, Y', strtotime($prescription->created_at)) }}
                  </td>
                  <td data-title="Generic">
                      {{ $prescription->generic_name }}
                  </td>
                  <td data-title="Qty">
                      {{ $prescription->total_quantity }}
                  </td>
                  <td data-title="Dosage">
                      {{ $prescription->dose_quantity }}
                  </td>
                  <td data-title="Intake">
                    <?php $Duration_Intake = explode(" ", $prescription->duration_of_intake); ?>
                    <?php
                        $di = isset($Duration_Intake[0]) ? $Duration_Intake[0] : NULL;
                        $dio = isset($Duration_Intake[1]) ? $Duration_Intake[1] : NULL;
                        $din = isset($Duration_Intake[1]) ? getIntakeName($Duration_Intake[1]) : NULL;
                    ?>
                      @if($prescription->duration_of_intake != ' C')
                        {{ $di." ".$din }}
                        @else
                        {{ $di." ".$din }}
                        @endif
                      {{ " - ".getRegimenName($prescription->dosage_regimen) }}
                  </td>
                </tr>

              @endforeach
          </tbody>
        </table>
      @endif
    </div><!-- /.box-body -->

  </div>

@stop

@section('scripts')

@stop
