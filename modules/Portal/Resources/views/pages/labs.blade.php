@section('heads')
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.css') !!}
{!! HTML::style('public/dist/plugins/ionslider/ion.rangeSlider.skinFlat.css') !!}
{!! HTML::style('public/dist/css/flaticon.css') !!}
@stop

@extends('portal::layout.master')

@section('header-content') Laboratories @stop

@section('page-header')
  <section class="content-header">
      <h1 class="text-overflow">
          <i class="fa fa-fw fa-heartbeat"></i> @yield('header-content')
      </h1>
      <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><a href="../records">Records</a></li>
      </ol>
  </section>
@stop

@section('list-content')

    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title blue">Prescription History</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div><!-- /.box-header -->
    <div class="box-body no-border table-responsive no-padding overflowx-hidden">
      @if(count($MedicalOrderLabExam))
        <table class="table table-hover table-responsive no-more-tables">
          <thead class="cf">
            <tr>
              <th>Laboratory</th>
              <th>Date Requested</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($MedicalOrderLabExam as $lab_key => $lab_value)
              <tr>
                <td data-title="Laboratory">
                    @if( isset($lab_value->laboratory_test_type) )
                    {{ $labb = getLabName($lab_value->laboratory_test_type) }}
                    @else
                    {{ $labb = $lov_laboratories[$lab_value->laboratory_test_type] }}
                    @endif

                    @if($labb == 'Others')
                    {{ ($lab_value->laboratory_test_type_others != "" OR $lab_value->laboratory_test_type_others != NULL) ? " : ".$lab_value->laboratory_test_type_others : " : Not specified" }}
                    @endif
                </td>
                <td data-title="Date Requested">
                    {{ date('M d, Y', strtotime($lab_value->created_at)) }}
                </td>
                <td data-title="Status">
                  @if($lab_value->LaboratoryResult!=NULL)
                    <?php $disabled_upload = TRUE; ?>
                    <span class="label label-success">Uploaded: {{ date('M d, Y h:i A', strtotime($lab_value->LaboratoryResult->created_at)) }}
                        </span>
                  @else
                    <?php $disabled_upload = FALSE; ?>
                    <span class="label label-warning">Pending: {{ date('M d, Y h:i A', strtotime($lab_value->created_at)) }}
                        </span>
                  @endif
                </td>
                <td data-title="Action">
                  @if($lab_value->laboratory_test_type =='BR')
                    <?php $Modal_labType = "lab_completebloodcount"; ?>
                  @elseif($lab_value->laboratory_test_type =='UR')
                    <?php $Modal_labType = "lab_urinalysis"; ?>
                  @elseif($lab_value->laboratory_test_type =='FE')
                    <?php $Modal_labType = "lab_fecalysis"; ?>
                  @else
                    <?php $Modal_labType = "labModal"; ?>
                  @endif

                  @if(!$disabled_upload)
                    <a href="#" class="btn btn-block btn-default btn-sm labClick" data-toggle="modal" data-id="{{ $lab_value->medicalorderlaboratoryexam_id }}" data-target="#{{ $Modal_labType }}" > Upload result </a>
                  @else
                    <a href="{{ URL::to('laboratory/modal/'.$lab_value->laboratory_test_type.'/'.$lab_value->medicalorderlaboratoryexam_id) }}" class="btn btn-block btn-default btn-sm" data-toggle="modal" data-target="#myInfoModal"> View </a>
                  @endif
                </td>
              </tr>
            @endforeach
        </tbody>
      </table>
      @endif
    </div><!-- /.box-body -->

  </div>

@stop

@section('scripts')

@stop
