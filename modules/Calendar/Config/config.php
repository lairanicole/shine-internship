<?php

return [
    'name' => 'calendar',
    'icon' => 'fa-calendar',
    'roles' => '["Admin","Doctor Admin","Doctor","Nurse","Midwife"]',
    'version' => '2.0',
    'title' => 'Shine Calendar Module',
    'folder' => 'Calendar',
    'table' => 'calendar',
    'description' => 'A simple calendar module to manage your appointments.',
    'module_user' => NULL,
    'developer' => 'Ateneo ShineLabs',
    'copy' => '2016-2018',
    'url' => 'www.shine.ph'
];
