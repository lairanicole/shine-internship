<?php

Route::group(['prefix' => 'laboratory', 'namespace' => 'Modules\Laboratory\Http\Controllers', 'middleware' => 'auth.access:laboratory'], function()
{
    Route::get('/', 'LaboratoryController@index');
    Route::post('/add', 'LaboratoryController@add');
    Route::post('/update', 'LaboratoryController@update');
    Route::get('/view/{laboratory_test_type}/{medicalorderlaboratoryexam_id}', 'LaboratoryController@add_view');
    Route::get('/modal/{laboratory_test_type}/{medicalorderlaboratoryexam_id}', 'LaboratoryController@modal_view');
    Route::get('/delete_attachment/{labid}/{filename}', 'LaboratoryController@delete_attachment');

});
