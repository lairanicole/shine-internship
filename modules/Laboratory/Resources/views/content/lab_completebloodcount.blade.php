<?php

  $data['BR_hemoglobin'] = $data['BR_hematosrit'] = $data['BR_erythrolytes'] = $data['BR_leukocytes'] = $data['BR_Segmenters'] = $data['BR_Juvenile'] = $data['BR_Eosinophils'] = $data['BR_Basophils'] = $data['BR_Lymphocytes'] = $data['BR_Stabs'] = $data['BR_Monocytes'] = $data['BR_Platelet_Count'] = $lab_filename = $disabled = NULL;
  $lab_id = $medlab_id = NULL;

  if(isset($lab['BR']) AND $lab['BR']) {
    if($disposition) {
      $disabled = 'disabled'; //disable entry when
    }
    $json_lab_data = json_decode($lab['BR']->lab_data);
    $lab_filename = $lab['BR']->filename;
    $lab_id = $lab['BR']->laboratoryresult_id;
    foreach ($json_lab_data as $key => $value) {
      $data[$key] = $value;
    }
  }
 if(isset($lab['BR'])) {
    $medlab_id = $lab['BR']->medicalorderlaboratoryexam_id;
  }
  if(isset($medicalorderlaboratoryexam_id)) {
    $medlab_id = $medicalorderlaboratoryexam_id;
  }

?>
<input type="hidden" name="medicalorderlaboratoryexam_id" id="medicalorderlaboratoryexam_id" value="{{ $medlab_id }}"/>

@if(isset($lab['BR']->laboratoryresult_id))
<input type="hidden" name="laboratoryresult_id" id="laboratoryresult_id" value="{{ $lab['BR']->laboratoryresult_id }}"/>
@endif
<div class="row">
  <div class="box no-border">
    <div class="col-sm-12">
      <div class="box-header with-border">
        <h3 class="box-title control-label"> Complete Blood Count </h3>
      </div>
    </div>

    <div class="col-sm-12">
        <div class="col-sm-12">
          <h4>Add Attachments</h4>
          <p>Upload an attachment or fill out the form below.</p>
          <input type="file" name="lab_file_upload" id="lab_file_upload">
        </div>
        <div class="clearfix"></div>
        <hr />
    </div>

    <div class="col-sm-12">
        <div class="col-sm-12">
          <div class="box-header with-border">
            <h3 class="box-title control-label">Results</h3>
          </div>
        </div>
        <div class="col-md-6">
            <div class="col-sm-12">
              <label class="col-sm-12">Result:</label>
            </div>

            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Hemoglobin</label>
              <div class="col-sm-8">
                {!!Form::text('BR_hemoglobin', $data['BR_hemoglobin'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label">g/L</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Hematosrit</label>
              <div class="col-sm-8">
                {!!Form::text('BR_hematosrit', $data['BR_hematosrit'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label">L/L</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Erythrolytes, numc</label>
              <div class="col-sm-8">
                {!!Form::text('BR_erythrolytes', $data['BR_erythrolytes'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label">1012/L</label>
            </div>
            <div class="col-sm-12">
              <label class="col-sm-4 control-label">Leukocytes, numc</label>
              <div class="col-sm-8">
                {!!Form::text('BR_leukocytes', $data['BR_leukocytes'], ['class' => 'form-control', $disabled]); !!}
              </div>
              <label class="control-label in-control-label">x109/L</label>
            </div>
        </div>
        <div class="col-md-6">
          <div class="col-sm-12">
            <label class="col-sm-12">Normal Values:</label>
          </div>
          <div class="col-sm-12">
            <div class="col-sm-4">
              <p> Hemoglobin </p>
            </div>
            <div class="col-sm-8">
              <p> Female: 120-160 g/L </p>
              <p> Male: 120-160 g/L </p>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="col-sm-4">
              <p> Hematosrit </p>
            </div>
            <div class="col-sm-8">
              <p> Female: 0.37-0.40L/L </p>
              <p> Male: 0.40-0.54L/L </p>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="col-sm-4">
              <p> Erythrolytes, numc </p>
            </div>
            <div class="col-sm-8">
              <p> Female: 4.2-5.4 x 1012/L </p>
              <p> Male: 4.6-6.2 x 1012/L </p>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="col-sm-4">
              <p> Leukocytes, numc </p>
            </div>
            <div class="col-sm-8">
              <p> Value: 4.5-11.0 x 109/L </p>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
      <div class="col-sm-12">
        <label class="col-sm-12">DIFFERENTIAL COUNT:</label>
      </div>
      <div class="col-sm-12">
        <label class="col-sm-12">Neurophils:</label>
      </div>
      <div class="col-sm-12">
        <label class="col-sm-1 control-label"></label>
        <label class="col-sm-2 control-label">Segmenters:</label>
        <div class="col-sm-7">
          {!!Form::text('BR_Segmenters', $data['BR_Segmenters'], ['class' => 'form-control', $disabled]); !!}
        </div>
        <label class="col-sm-2 control-label">(0.50-0.70)</label>
      </div>
      <div class="col-sm-12">
        <label class="col-sm-1 control-label"></label>
        <label class="col-sm-2 control-label">Stabs:</label>
        <div class="col-sm-7">
          {!!Form::text('BR_Stabs', $data['BR_Stabs'], ['class' => 'form-control', $disabled]); !!}
        </div>
        <label class="col-sm-2 control-label">(0.02-0.05)</label>
      </div>
      <div class="col-sm-12">
        <label class="col-sm-1 control-label"></label>
        <label class="col-sm-2 control-label">Juvenile:</label>
        <div class="col-sm-7">
          {!!Form::text('BR_Juvenile', $data['BR_Juvenile'], ['class' => 'form-control', $disabled]); !!}
        </div>
        <label class="col-sm-2 control-label">(0.00-0.01)</label>
      </div>
      <div class="col-sm-12">
        <label class="col-sm-3 control-label">Eosinophils:</label>
        <div class="col-sm-7">
          {!!Form::text('BR_Eosinophils', $data['BR_Eosinophils'], ['class' => 'form-control', $disabled]); !!}
        </div>
        <label class="col-sm-2 control-label">(0.01-0.04)</label>
      </div>
      <div class="col-sm-12">
        <label class="col-sm-3 control-label">Basophils:</label>
        <div class="col-sm-7">
          {!!Form::text('BR_Basophils', $data['BR_Basophils'], ['class' => 'form-control', $disabled]); !!}
        </div>
        <label class="col-sm-2 control-label">(0.00-0.01)</label>
      </div>
      <div class="col-sm-12">
        <label class="col-sm-3 control-label">Lymphocytes:</label>
        <div class="col-sm-7">
          {!!Form::text('BR_Lymphocytes', $data['BR_Lymphocytes'], ['class' => 'form-control', $disabled]); !!}
        </div>
        <label class="col-sm-2 control-label">(0.22-0.40)</label>
      </div>
      <div class="col-sm-12">
        <label class="col-sm-3 control-label">Monocytes:</label>
        <div class="col-sm-7">
          {!!Form::text('BR_Monocytes', $data['BR_Monocytes'], ['class' => 'form-control', $disabled]); !!}
        </div>
        <label class="col-sm-2 control-label">(0.03-0.08)</label>
      </div>
      <div class="col-sm-12">
        <label class="col-sm-3 control-label">Platelet Count:</label>
        <div class="col-sm-7">
        {!!Form::text('BR_Platelet_Count', $data['BR_Platelet_Count'], ['class' => 'form-control', $disabled]); !!}
        </div>
        <label class="col-sm-2 control-label">(150 350x109/L)</label>
      </div>
    </div>
        <div class="clearfix"></div>
        <hr />
    </div>
  </div>
</div>
<br>

@if($lab_filename!=NULL OR $lab_filename!="")
<div class="row">
  <div class="col-sm-12">
    <div class="box no-border">
        <div class="col-sm-12">
          <div class="box-header with-border">
            <h3 class="box-title control-label"> Attachments</h3>
          </div>
        </div>
        <div class="col-sm-12" style="overflow:hidden;margin-top:20px;">
          <div id="image-gallery" class="col-md-12 cf">
            <?php
              $filenames = explode(';', trim($lab_filename,';'));
              foreach($filenames as $filename) {
              $ext = substr($filename, -3);
              if($ext == 'png' OR $ext == 'jpg' OR $ext == 'gif') {
            ?>
                <p><a href="#" data-target="{{ url('laboratory/delete_attachment/'.$lab_id.'/'.$filename) }}" data-mod="{{ url('laboratory/modal/BR/'.$lab['BR']->medicalorderlaboratoryexam_id) }}" class="kbd kbd-sm bg-red deletebutton"><i class="fa fa-times"></i></a> {{ $filename }}</p>
                <p><img src="{{ url(uploads_url().'/lab_results/'.$filename) }}"  data-high-res-src="{{ url(uploads_url().'/lab_results/'.$filename) }}" alt="" class="pannable-image col-sm-12"></p>
                <?php } else { ?>
                <p><a href="#" data-target="{{ url('laboratory/delete_attachment/'.$lab_id.'/'.$filename) }}" data-mod="{{ url('laboratory/modal/BR/'.$lab['BR']->medicalorderlaboratoryexam_id) }}" class="kbd kbd-sm bg-red deletebutton"><i class="fa fa-times"></i></a> <a href="{{ url(uploads_url().'/lab_results/'.$filename) }}" target="_blank">{{ $filename }}</a></p>
                <?php } ?>
            <?php } ?>
          </div>
        </div>
      </div>
  </div>
</div>
@endif
