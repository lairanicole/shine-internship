
@extends('layout.master')
@section('heads')
{!! HTML::style('public/dist/css/imageviewer.css') !!}
@stop
@section('page-header')

  <section class="content-header">
    <!-- edit by RJBS -->
    <h1>
      <i class="fa fa-flask"></i>
      Laboratory
    </h1>
    <!-- end edit RJBS -->
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> <a href="{{ url('/laboratory') }}"> Laboratory </a></li>
    </ol>
  </section>
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Laboratory Results</h3>

      <div class="box-tools pull-right">
          <a href="{{ URL::previous() }}" class="btn btn-app">
            <i class="fa fa-arrow-left"></i> Back
          </a>
      </div>
    </div>

  <div class="box-body">
      @if(isset($lab['UR']))
        @include('laboratory::content.lab_urinalysis')
    @elseif(isset($lab['FE']))
        @include('laboratory::content.lab_fecalysis')
    @elseif(isset($lab['BR']))
        @include('laboratory::content.lab_completebloodcount')
    @else
        @include('laboratory::content.lab_default')
    @endif
  </div>
</div>
<!-- /.box -->
@stop



@section('before_validation_scripts')
{!! HTML::script('public/dist/js/imageviewer.min.js') !!}
<script type="text/javascript">
    //Example 3
    $(function () {
        $('.pannable-image').ImageViewer();
    });
</script>
@stop
