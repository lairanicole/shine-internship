
<div class="box">
    <div class="box-header with-border bg-yellow">
      <h3 class="box-title"><i class="fa fa-flask"></i> Laboratory Result</h3>

      <div class="box-tools pull-right">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
    </div>

    @if($lab[$testType])
    {!! Form::open(array('url' => 'laboratory/update', 'enctype'=>'multipart/form-data')) !!}
    <div class="box-body">
        @if($testType!= 'OT')
            <h3 class="box-title control-label">{{ getLabName($testType) }}</h3>
        @else
            <h3 class="box-title control-label">{{$medicalorder_laboratoryexam->laboratory_test_type_others}}</h3>
        @endif

        @if(isset($lab['UR']))
            @include('laboratory::content.lab_urinalysis')
        @elseif(isset($lab['FE']))
            @include('laboratory::content.lab_fecalysis')
        @elseif(isset($lab['BR']))
            @include('laboratory::content.lab_completebloodcount')
        @else
            @include('laboratory::content.lab_default')
        @endif
    </div>
    <div class="modal-footer" id="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-success">Submit</button>
    </div>
    {!! Form::close() !!}
    @else
    {!! Form::open(array('url' => 'laboratory/add', 'enctype'=>'multipart/form-data')) !!}
        <div class="box-body">
            <div class="col-sm-12">
              <div class="box-header no-border">
                @if($testType!= 'OT')
                    <h3 class="box-title control-label">{{ getLabName($testType) }}</h3>
                @else
                    <h3 class="box-title control-label">{{$medicalorder_laboratoryexam->laboratory_test_type_others}}</h3>
                @endif
              </div>
            </div>
            <h4>No laboratory result yet.</h4>
            <p>This laboratory request do not have results encoded yet. You can add them now below:</p>
            @if($testType  == 'UR')
                @include('laboratory::content.lab_urinalysis')
            @elseif($testType  == 'FE')
                @include('laboratory::content.lab_fecalysis')
            @elseif($testType  == 'BR')
                @include('laboratory::content.lab_completebloodcount')
            @else
                @include('laboratory::content.lab_default')
            @endif
        </div>
        <div class="modal-footer" id="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
        {!! Form::close() !!}
    @endif

</div>
<!-- /.box -->

@if(isset($lab[$testType]->LaboratoryResult) AND $lab[$testType]->LaboratoryResult->filename != NULL)
{!! HTML::script('public/dist/js/imageviewer.min.js') !!}
<script type="text/javascript">
    /*$(function () {
        if($('.pannable-image').length > 0) {
            $('.pannable-image').ImageViewer();
        }
    });*/
</script>
@endif
