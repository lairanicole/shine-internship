<?php namespace Modules\Eclaims\Entities;
   
use Illuminate\Database\Eloquent\Model;

class EclaimsFacility extends Model {

    protected $table = 'eclaims_facility';
    protected static $static_table = 'eclaims_facility';
    protected $primaryKey = 'id';
    protected $fillable = [];

}