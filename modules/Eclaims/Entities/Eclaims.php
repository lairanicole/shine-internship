<?php namespace Modules\Eclaims\Entities;

use Illuminate\Database\Eloquent\Model;
use DB;

use Illuminate\Database\Eloquent\SoftDeletes;

class Eclaims extends Model {

    use SoftDeletes;

    protected $table = 'eclaims';
    protected static $static_table = 'eclaims';
    protected $primaryKey = 'id';
    protected $fillable = [];

    public static function collectQualifiers($facid)
    {
        $inClaims = self::whereNotNull('healthcareservice_id')->where('healthcareservice_id','<>','')->lists('healthcareservice_id');

        //get all healthcare services eligible for e-claims
        $qualifiersall = DB::table('healthcare_view')
        ->leftJoin('diagnosis_view','healthcare_view.healthcareservice_id', '=', 'diagnosis_view.healthcareservice_id')
        ->join('disposition','healthcare_view.healthcareservice_id', '=', 'disposition.healthcareservice_id')
        ->select('healthcare_view.healthcareservice_id','healthcare_view.patient_id','healthcare_view.first_name','healthcare_view.last_name','healthcare_view.healthcareservicetype_id','healthcare_view.consultation_type','diagnosis_view.diagnosislist_id','disposition.disposition','healthcare_view.created_at')
        ->whereIn('healthcare_view.healthcareservicetype_id',["Tuberculosis","AnimalBite","NewBorn","Malaria"])
        ->where('healthcare_view.facility_id', $facid)
        ->whereNull('healthcare_view.deleted_at')
        ->whereNotIn('healthcare_view.healthcareservice_id',$inClaims)
        ->groupBy('healthcare_view.healthcareservice_id')
        ->get();

        //for MCP
        $qualifiersmcp = DB::table('maternalcare')
        ->join('healthcare_view','maternalcare.healthcareservice_id', '=', 'healthcare_view.healthcareservice_id')
        ->join('diagnosis_view','healthcare_view.healthcareservice_id', '=', 'diagnosis_view.healthcareservice_id')
        ->join('disposition','healthcare_view.healthcareservice_id', '=', 'disposition.healthcareservice_id')
        ->select('healthcare_view.healthcareservice_id','healthcare_view.patient_id','healthcare_view.first_name','healthcare_view.last_name','healthcare_view.healthcareservicetype_id','healthcare_view.consultation_type','diagnosis_view.diagnosislist_id','disposition.disposition','healthcare_view.created_at')
        ->where('healthcare_view.healthcareservicetype_id',"MaternalCare")
        ->where('healthcare_view.facility_id', $facid)
        ->whereNotIn('healthcare_view.healthcareservice_id',$inClaims)
        ->whereNull('healthcare_view.deleted_at')
        ->groupBy('maternalcare.maternalcase_id','healthcare_view.healthcareservice_id')
        ->get();
        //merge the arrays
        $qualifiers = array_merge($qualifiersall, $qualifiersmcp);
        //sort it according to created date
        usort($qualifiers, function($a, $b)
        {
            return -1 * strcmp($a->created_at, $b->created_at);
        });

        return $qualifiers;
    }

    public static function collectEligibles($facid, $eligibles)
    {
        /*$array = DB::table('eclaims')
            ->leftJoin('healthcare_view','healthcare_view.healthcareservice_id', '=', 'eclaims.healthcareservice_id')
            ->leftJoin('diagnosis_view','healthcare_view.healthcareservice_id', '=', 'diagnosis_view.healthcareservice_id')
            ->join('disposition','healthcare_view.healthcareservice_id', '=', 'disposition.healthcareservice_id')
            ->select('healthcare_view.healthcareservice_id','healthcare_view.patient_id','healthcare_view.first_name','healthcare_view.last_name','healthcare_view.healthcareservicetype_id','diagnosis_view.diagnosislist_id','disposition.disposition','healthcare_view.created_at', 'eclaims.*')
            ->whereIn('healthcare_view.healthcareservicetype_id',["Tuberculosis","AnimalBite","NewBorn","Malaria","MaternalCare"])
            ->where('healthcare_view.facility_id', $facid)
            ->whereIn('eclaims.healthcareservice_id', $eligibles)
            ->whereNull('eclaims.eclaim_file_date')
            ->get();*/

        $array = DB::table('eclaims')
            ->whereNull('pbef')
            ->whereNull('pbef_details')
            ->where('pbef_status', "YES")
            ->get();

        return $array;
    }

    public static function collectNonEligibles($facid, $eligibles)
    {
        /*$array = DB::table('eclaims')
            ->leftJoin('healthcare_view','healthcare_view.healthcareservice_id', '=', 'eclaims.healthcareservice_id')
            ->leftJoin('diagnosis_view','healthcare_view.healthcareservice_id', '=', 'diagnosis_view.healthcareservice_id')
            ->join('disposition','healthcare_view.healthcareservice_id', '=', 'disposition.healthcareservice_id')
            ->select('healthcare_view.healthcareservice_id','healthcare_view.patient_id','healthcare_view.first_name','healthcare_view.last_name','healthcare_view.healthcareservicetype_id','diagnosis_view.diagnosislist_id','disposition.disposition','healthcare_view.created_at', 'eclaims.*')
            ->whereIn('healthcare_view.healthcareservicetype_id',["Tuberculosis","AnimalBite","NewBorn","Malaria","MaternalCare"])
            ->where('healthcare_view.facility_id', $facid)
            ->whereIn('eclaims.healthcareservice_id', $eligibles)
            ->whereNull('eclaims.eclaim_file_date')
            ->get();*/

        $array = DB::table('eclaims')
            ->whereNull('pbef')
            ->whereNull('pbef_details')
            ->where('pbef_status', "NO")
            ->get();

        return $array;
    }

    public static function collectSubmitted($facid, $submitted)
    {
        $array = DB::table('eclaims')
            ->leftJoin('healthcare_view','healthcare_view.healthcareservice_id', '=', 'eclaims.healthcareservice_id')
            ->leftJoin('diagnosis_view','healthcare_view.healthcareservice_id', '=', 'diagnosis_view.healthcareservice_id')
            ->join('disposition','healthcare_view.healthcareservice_id', '=', 'disposition.healthcareservice_id')
            ->select('healthcare_view.healthcareservice_id','healthcare_view.patient_id','healthcare_view.first_name','healthcare_view.last_name','healthcare_view.healthcareservicetype_id','diagnosis_view.diagnosislist_id','disposition.disposition','healthcare_view.created_at', 'eclaims.*')
            ->where('healthcare_view.facility_id', $facid)
            ->whereIn('eclaims.healthcareservice_id', $submitted)
            ->whereNotNull('eclaims.eclaim_file_date')
            ->get();


        return $array;
    }

}
