<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEclaimsFacilityTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eclaims_facility', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('eclaimsfacility_id',60);
            $table->string('facility_id',60);

            $table->string('hci_accrename',100);
            $table->string('hci_userid',100);
            $table->string('hci_password',100);
            $table->string('hci_hospitalcode',100);
            $table->string('hci_accreditation',100);
            $table->string('hci_cipherkey',100);
            $table->tinyInteger('selected');

            $table->timestamps();
            $table->unique('eclaimsfacility_id');
            $table->unique('hci_hospitalcode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eclaims_facilities');
    }

}
