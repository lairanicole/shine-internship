<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEclaims extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('eclaims')!=TRUE) {

            Schema::create('eclaims', function (Blueprint $table) {
                $table->increments('id');
                $table->string('eclaims_id',60);
                $table->string('patient_id',60)->nullable();

                $table->string('patient_pin',60)->nullable();
                $table->string('healthcareservice_id',60)->nullable();

                $table->string('HOSPITAL_CODE',60);

                $table->string('csf',60)->nullable();
                $table->longText('csf_details')->nullable();
                $table->dateTime('csf_date')->nullable();

                $table->string('cf1',60)->nullable();
                $table->longText('cf1_details')->nullable();
                $table->longText('cf1_sdata')->nullable();
                $table->dateTime('cf1_date')->nullable();
                $table->string('pbef',60)->nullable();
                $table->longText('pbef_details')->nullable();
                $table->string('pbef_status',5)->nullable(); //Yes or No
                $table->dateTime('pbef_date')->nullable();
                $table->string('pTrackingNumber', 60)->nullable();

                $table->string('cf2',60)->nullable();
                $table->longText('cf2_details')->nullable();
                $table->dateTime('cf2_date')->nullable();
                $table->string('cf3',60)->nullable();
                $table->longText('cf3_details')->nullable();
                $table->dateTime('cf3_date')->nullable();

                $table->string('cf4',60)->nullable();
                $table->longText('cf4_details')->nullable();
                $table->dateTime('cf4_date')->nullable();

                $table->longText('receipts')->nullable();
                $table->longText('particulars')->nullable();
                
                $table->dateTime('eclaim_file_date')->nullable();
                $table->dateTime('eclaim_refile_date')->nullable();
                $table->longText('eclaim_xmlarray')->nullable(); //submitted data
                $table->longtext('eclaim_response')->nullable(); //submission response
                $table->string('pHospitalTransmittalNo', 20)->nullable();
                $table->string('pTransmissionControlNumber', 20)->nullable();
                $table->string('pReceiptTicketNumber', 100)->nullable(); //response
                $table->text('pClaimNumber')->nullable(); //response
                $table->string('pClaimSeriesLhio', 100)->nullable(); //response

                $table->text('eclaim_submit_status')->nullable();
                $table->longText('eclaim_status')->nullable(); //after checking stat

                $table->text('attachments')->nullable();

                $table->string('facility_id',60);
                $table->string('user_id',60);

                $table->softDeletes();
                $table->timestamps();
                $table->unique('eclaims_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eclaims');
    }

}
