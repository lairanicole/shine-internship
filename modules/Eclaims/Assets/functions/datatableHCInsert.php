<?php

//This is the script for dataTable Actions menu after loading data
//for healthcare listing
//Inserts E-Claims menu item if record is eclaims acceptable
//and records has disposition

echo "
//add E-Claims menu item on the healthcare Actions menu
if( $('ul.pathcrec').length > 1 ) { 
    var opts = $('ul.pathcrec');
    opts.each(function() {
        //check if service is Maternal
        var s = $(this).parents('tr').find('td:nth-child(3)').text();
        var t = $('li:first-child a', $(this) ).text(); 
        console.log(s);
        var u = $(this).parent().next('a').attr('href');
        //let us get the patid and careid
        ids = u.split('/');
        if( s == 'MaternalCare' && t != 'This record is not yet complete.Please give your Disposition.' ){
            $(this).append(\"<li role='separator' class='divider'></li>\");
            
            $(this).append(\"<li><a href='#' data-load-url='". url('eclaims/checkEligibility') ."/\"+ids[6]+\"/\"+ids[7]+\"' data-toggle='modal' data-backdrop='static' data-type='check' data-target='#eclaimsModal' data-loading='Checking benefit eligibility. Please wait...'>Check Claim Eligibility</a></li>\");

            $(this).append(\"<li><a href='#' data-load-url='". url('eclaims/generateClaim/') ."/\"+ids[6]+\"/\"+ids[7]+\"' data-toggle='modal' data-backdrop='static' data-type='generate' data-target='#eclaimsModal' data-loading='Generating e-claim. Please wait...'>Generate Claim</a></li>\");
        }
    })
}";

?>