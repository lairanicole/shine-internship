<?php
    //This is the module javascript functions
?>

<div class="modal fade" id="eclaimsModal" tabindex="-1" role="dialog" aria-labelledby="eclaimsModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-yellow">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myInfoModalLabel"> E-Claims Module </h4>
            </div>
            <div class="modal-body">
                <p class="result"></p>
            </div>
            <div class="modal-footer eclaims-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    //insert this Module JS at the end of the file
    $("#eclaimsModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        var loadurl = link.data('load-url');
        var type = link.data('type');
        var loader = "<i class='fa fa-spinner fa-pulse fa-fw'></i> " + link.data('loading');
        $(this).find(".result").html(loader);
        $(this).find(".result").load(loadurl, function( response, status, xhr ){
            if( response ) {
                //close progress modal
                if(type == 'check') {
                    $("#eclaimsModal")
                    //.modal('hide')
                    .on('hidden.bs.modal', function (e) {
                        //load the appropriate form with result
                        $("#eclaimsModal").find('.eclaims-footer').html('');
                        //$('#myInfoModal').modal('show');
                        //$(this).off('hidden.bs.modal'); // Remove the 'on' event binding
                        //$('#myInfoModal').find(".modal-content").load(response);
                    });
                }
                if ( type == 'map') {
                    $("#eclaimsModal").find('.eclaims-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button class="btn btn-success" onclick="$(\'.result\').printThis();">Print Claims Map</button>');
                } else {
                    $("#eclaimsModal").find('.eclaims-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button class="btn btn-success" onclick="$(\'.result\').printThis();">Print CF1</button> <button onclick="$(\'#form_eclaims\').submit();" class="btn btn-warning">Submit CF1</button>');
                }
            } else if ( status == "error" ) {
                var msg = "Sorry but there was an error: ";
                $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
            };
        });
        
    });
</script>