<?php

//This is the script for dataTable Actions menu after loading data
//for patients listing
//Inserts E-Claims menu item if record is eclaims acceptable
//and records has disposition

echo "
//add E-Claims menu item on the patient Records Actions menu
if( $('ul.pathprec').length > 1 ) { 
    var opts = $('ul.pathprec');
    opts.each(function() {
        //find the insertion point
        var ins = $(this).find('.enditem'); //enditem should be the last item on the dropdown
        
        //let us get the patid
        var u = $(this).parent().next('a').attr('href');
        ids = u.split('/'); console.log(ids);
        //if( s == 'MaternalCare' && t != 'This record is not yet complete.Please give your Disposition.' ){
            ins.before(\"<li role='separator' class='divider'></li>\");
            
            ins.before(\"<li><a href='#' data-load-url='". url('eclaims/checkEligibility') ."/\"+ids[6]+\"' data-toggle='modal' data-backdrop='static' data-type='check' data-target='#eclaimsModal' data-loading='Checking benefit eligibility. Please wait...'>Check Claim Eligibility</a></li>\");
        //}
    })
}";

?>