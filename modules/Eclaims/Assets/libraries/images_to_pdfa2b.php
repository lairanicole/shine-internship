<?php
/* $Id: images_to_pdfa2b.php,v 1.3 2016/12/29 11:36:44 rp Exp $
 * Images to PDF/A:
 * Convert grayscale, CMYK or RGB image files in TIFF or JPEG formats to 
 * PDF/A-2b, taking care of color space issues.
 *
 * Required software: PDFlib/PDFlib+PDI/PPS 9
 * Required data: image files
 */

/* This is where the data files are. Adjust as necessary. */
$searchpath = dirname(dirname(dirname(__FILE__)))."/input";
$outfile = "";
$title = "Images to PDF/A";

$imagefileRGB = "nesrin.jpg";
$imagefileCMYK = "kraxi_header.tif";
$imagefileGray = "nesrin_gray.jpg";

try {
    $p = new PDFlib();

    $p->set_option("searchpath={" . $searchpath . "}");

    /* This means we must check return values of load_font() etc. */
    $p->set_option("errorpolicy=return");
    $p->set_option("stringformat=utf8");

    /* Output all contents conforming to PDF/A-2b */
    if ($p->begin_document($outfile, "pdfa=PDF/A-2b") == 0)
	throw new Exception("Error: " . $p->get_errmsg());
    
    $p->set_info("Creator", "PDFlib Cookbook");
    $p->set_info("Title", $title . ' $Revision: 1.3 $');

    /* Use sRGB as output intent since it allows the color spaces ICC-based,
     * grayscale, and RGB. CMYK images must be tagged with a suitable CMYK
     * ICC profile.
     */
    $p->load_iccprofile("sRGB", "usage=outputintent");
    
    /* Load an ICC profile for CMYK images */
    $icc = $p->load_iccprofile("ISOcoated_v2_eci.icc", "");
    if ($icc == 0)
	throw new Exception("Error: " . $p->get_errmsg());

    $p->begin_page_ext(595, 842, "");

    /* We can use an RGB image without any further options since we
     * supplied an RGB output intent profile.
     */
    $image = $p->load_image("auto", $imagefileRGB, "");
    if ($image == 0)
	throw new Exception("Error: " . $p->get_errmsg());

    $p->fit_image($image, 100, 550, "scale=0.5");
    $p->close_image($image);
    
    /* Similarly, we can use a grayscale image without any further options
     * since we supplied an RGB output intent profile.
     */
    $image = $p->load_image("auto", $imagefileGray, "");
    if ($image == 0)
	throw new Exception("Error: " . $p->get_errmsg());

    $p->fit_image($image, 100, 250, "scale=0.5");
    $p->close_image($image);
    
    /* For CMYK images we explicitly assign a CMYK ICC profile */
    $image = $p->load_image("auto", $imagefileCMYK, "iccprofile=" . $icc);
    if ($image == 0)
	throw new Exception("Error: " . $p->get_errmsg());

    $p->fit_image($image, 100, 80, "");
    $p->close_image($image);

    $p->end_page_ext("");

    $p->end_document("");
    $buf = $p->get_buffer();
    $len = strlen($buf);

    header("Content-type: application/pdf");
    header("Content-Length: $len");
    header("Content-Disposition: inline; filename=images_to_pdfa.pdf");
    print $buf;
}
catch (PDFlibException $e) {
    echo("PDFlib exception occurred:\n" . 
        "[" . $e->get_errnum() . "] " . $e->get_apiname() .
        ": " . $e->get_errmsg() . "\n");
    exit(1);
}
catch (Exception $e) {
    echo($e->getMessage());
    exit(1);
}

$p = 0;
?>
