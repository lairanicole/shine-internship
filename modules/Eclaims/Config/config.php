<?php

return [
    'name' => 'eclaims',
    'icon' => 'fa-universal-access',
    'roles' => '["Admin","Doctor Admin","Doctor","Nurse","Midwife"]',
    'version' => '1.0',
    'title' => 'PhilHealth EClaims',
    'folder' => 'Eclaims',
    'table' => 'eclaims',
    'description' => 'The E-Claims Modules for submission of claims to Philhealth.',
    'module_user' => NULL,
    'developer' => 'mediXserve',
    'copy' => '2018',
    'url' => 'www.shine.ph|mylifeemr.com',
    'debug' => 1
];
