<div class="box box-solid">
    <div class="box-body">
        <h3>Eligible Claims</h3>
        <p>The following HealthCare records has generated claims. You can perform submission.</p>
        <table id="eligible" class="table table-bordered datatable">
            <thead>
                <tr>
                    <th>Patient Name</th>
                    <th>ICD10 / RVS Code</th>
                    <th>Eligiblity Checked on</th>
                    <th class="nosort" style="width:200px;">Action</th>
                </tr>
            </thead>
            <tbody>

            @foreach($eligibles as $claim)
                <?php $pat = json_decode($claim->cf1_details); ?>
                <tr>
                    <td>{{ $pat->pMemberFirstName." ".$pat->pMemberLastName }}</td>
                    <td>{{ $pat->pRVS }}</td>
                    <td>{{ date("M. d, Y", strtotime($claim->pbef_date)) }}</td>
                    <td style="text-align:center;">
                    <div class="btn-group" style="white-space: nowrap; width:279px;">
                        <div class="btn-group">
                            <a href="#" type="button" class="btn btn-warning btn-sm btn-flat dropdown-toggle" data-toggle="dropdown"> Forms <span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="myTabDrop1" id="myDrop-'.$claim->healthcareservice_id.'">
                                <li><a href="{{ url('eclaims/show/eligible/'.$claim->eclaims_id) }}" title="View Claim"> View Eligibility </a></li>
                                <li role='separator' class='divider'></li>
                                @if(isset($claim->csf) AND $claim->csf)
                                <li><a href="{{ url('eclaims/show/csf/'.$claim->eclaims_id) }}" title="View CSF">View CSF </a></li>
                                @else
                                <li><a href="{{ url('eclaims/fillform/'.$claim->eclaims_id.'/csf') }}" title="Fill CSF">Fill CSF </a></li>
                                @endif

                                @if($claim->cf1)
                                <li><a href="{{ url('eclaims/show/cf1/'.$claim->eclaims_id) }}" title="View CF1">View CF1 </a></li>
                                @else
                                <li><a href="{{ url('eclaims/fillform/'.$claim->eclaims_id.'/cf1') }}" title="Fill CF1">Fill CF1 </a></li>
                                @endif

                                @if($claim->cf2)                             
                                <li><a href="{{ url('eclaims/show/cf2/'.$claim->eclaims_id) }}" title="View CF1">View CF2 </a></li>
                                @else
                                <li><a href="{{ url('eclaims/fillform/'.$claim->eclaims_id.'/cf2') }}" title="Fill CF2">Fill CF2 </a></li>
                                @endif

                                @if($claim->cf3)
                                <li><a href="{{ url('eclaims/show/cf3/'.$claim->eclaims_id) }}" title="View CF1">View CF3 </a></li>
                                @else
                                <li><a href="{{ url('eclaims/fillform/'.$claim->eclaims_id.'/cf3') }}" title="FILL CF3">Fill CF3 </a></li>
                                @endif 
                                @if($claim->cf4)
                                <li><a href="{{ url('eclaims/show/cf4/'.$claim->eclaims_id) }}" title="View CF1">View CF4 </a></li>
                                @else
                                <li><a href="{{ url('eclaims/fillform/'.$claim->eclaims_id.'/cf4') }}" title="FILL CF4">Fill CF4 </a></li>
                                @endif                                       
                            </ul>
                        </div>

                        @if($claim->cf1 AND $claim->cf2)                              
                        <div class="btn-group">
                            <a href="#" type="button" class="btn btn-primary btn-sm btn-flat dropdown-toggle" data-toggle="dropdown"> Add Attachment <span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="myTabDrop1" id="myDrop-'.$claim->healthcareservice_id.'">
                                <li><a href="#" title="Upload Documents" data-toggle="modal" data-id="{{ $claim->eclaims_id }}" data-target="#uploadModal" data-type="upload">Upload Document</a></li>
                                <li><a href="#" data-load-url="{{ url('eclaims/attachments/'.$claim->eclaims_id) }}" data-loading="Loading... Please wait..." data-toggle="modal" data-type="listing" data-target="#eclaimsNotify" title="View Attachments">View Attachments </a></li>
                                <li role='separator' class='divider'></li>
                                <li><a href="{{ url('eclaims/fillform/'.$claim->eclaims_id.'/receipts') }}" title="Add Receipts and Particulars"> Add Receipts and Particulars </a></li>
                                @if($claim->cf4)
                                <li role='separator' class='divider'></li>
                                <li><a href="#" title="Upload Documents for CF4" data-toggle="modal" data-id="{{ $claim->eclaims_id }}" data-target="#uploadModal" data-type="upload"> Upload Documents for CF4 </a></li>
                                @endif
                            </ul>
                        </div>

                        <a href="#" data-load-url="{{ url('eclaims/submitClaim/'.$claim->eclaims_id) }}" class="btn btn-sm btn-success btn-flat smalhide" title="Submit Claim" data-toggle="modal" data-target="#eclaimsNotify" 
                        data-type="submit" data-loading="Submitting Claim. Please wait...">Submit Claim </a>
                        @else
                        <a href="#" type="button" class="btn btn-default btn-sm btn-flat dropdown-toggle" data-toggle="dropdown" disabled> Add Attachment <span class="caret"></span></a>
                        <a href="#" class="btn btn-sm btn-default btn-flat smalhide" title="Submit Claim" disabled>Submit Claim </a>
                        @endif
                    </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>