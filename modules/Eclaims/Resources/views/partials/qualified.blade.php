<div class="box box-solid">
    <div class="box-body">
        <h3>Qualified HealthCare Records for Claims</h3>
        <p>The following HealthCare records are qualified for claims. You can check the eligibility of the patient for Claims using the Check Eligibility. After checking the availability, your claim will be moved to the Eligible Claims tab.</p>
        <p>If your intended claim is not listed below, you can submit a manual claim but clicking on the <kbd>Submit Manual Claim</kbd> button.</p>
        <table id="qualified" class="table table-bordered qualified">
            <thead>
                <tr>
                    <th>Patient Name</th>
                    <th>Healthcare Service</th>
                    <th>Diagnosis</th>
                    <th>Disposition</th>
                    <th>Date</th>
                    <th class="nosort">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($claims as $claim)
                <tr>
                    <td>{{ $claim->first_name." ".$claim->last_name }}</td>
                    <td>{{ $claim->healthcareservicetype_id }}</td>
                    <td>{{ $claim->diagnosislist_id }}</td>
                    <td>{{ $claim->disposition }}</td>
                    <td>{{ date("M. d, Y", strtotime($claim->created_at)) }}</td>
                    <td style="text-align:center;">
                    <div class="btn-group" style="white-space: nowrap; width:170px;">
                        <a href="#" data-load-url="{{ url('healthcareservices/qview/'.$claim->patient_id.'/'.$claim->healthcareservice_id) }}" class="btn btn-sm btn-success btn-flat smalhide" title="View Claim"  data-toggle="modal" data-target="#myInfoModal"><i class="fa fa-eye"></i> View </a>
                        <a href="{{ url('eclaims/checkEligibility/'.$claim->patient_id.'/'.$claim->healthcareservice_id) }}" class="btn btn-sm btn-danger btn-flat smalhide" title="Check Eligibility">Check Eligibility </a>
                    </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>