<div class="box box-solid">
    <div class="box-body">
        <h3>Non-Eligible Claims</h3>
        <p>The following submitted claims was not eligible form submission.</p>
        <table id="noneligible" class="table table-bordered datatable">
            <thead>
                <tr>
                    <th>Patient Name</th>
                    <th>Healthcare Service</th>
                    <th>Reason for decline</th>
                    <th>Date Checked</th>
                    <th class="nosort">Action</th>
                </tr>
            </thead>
            <tbody>
            
            @foreach($noneligibles as $claim)
            <?php
                $reason = NULL;
                $pbef = json_decode($claim->pbef_details);
                if(is_array($pbef->DOCUMENTS->DOCUMENT)) {
                    $reason = implode("; ", $pbef->DOCUMENTS->DOCUMENT);
                } else {
                    $reason = $pbef->DOCUMENTS->DOCUMENT;
                }
            ?>
                <tr>
                    <td>{{ $claim->first_name." ".$claim->last_name }}</td>
                    <td>{{ $claim->healthcareservicetype_id }}</td>
                    <td>{{ $reason }}</td>
                    <td>{{ date("M. d, Y", strtotime($claim->pbef_date)) }}</td>
                    <td style="text-align:center;">
                    <div class="btn-group" style="white-space: nowrap; width:200px;">
                        <a href="{{ url('eclaims/show/eligible/'.$claim->eclaims_id) }}" class="btn btn-sm btn-success btn-flat smalhide" title="View Claim"><i class="fa fa-eye"></i> View Eligibility </a>
                        <a href="{{ url('eclaims/show/cf1/'.$claim->eclaims_id) }}" class="btn btn-sm btn-info btn-flat smalhide" title="View CF1">View CF1 </a>
                    </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>