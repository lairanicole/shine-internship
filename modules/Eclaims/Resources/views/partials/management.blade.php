<div class="box box-solid">
    <div class="box-body">
        <h3>eClaims Management</h3>
        <p>Management functions for miscellaneous web services. Use these to check additional eClaims information from PhilHealth.</p>
        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="#MemPIN" aria-controls="MemPIN" role="tab" data-toggle="tab">Get Member PIN</a></li>
                <li role="presentation"><a href="#EmpPEN" aria-controls="EmpPEN" role="tab" data-toggle="tab">Search Employer</a></li>
                <li role="presentation"><a href="#DocPAN" aria-controls="DocPAN" role="tab" data-toggle="tab">Get Doctor PAN</a></li>
                <li role="presentation"><a href="#DocAccree" aria-controls="DocAccree" role="tab" data-toggle="tab">Check Doctor Accreditation</a></li>
                
                <li role="presentation"><a href="#SearchCaseRate" aria-controls="SearchCaseRate" role="tab" data-toggle="tab">Search for Case Rate</a></li>
                <li role="presentation"><a href="#SearchHospital" aria-controls="SearchHospital" role="tab" data-toggle="tab">Search Hospital</a></li>

                <li role="presentation"><a href="#GetVoucher" aria-controls="GetVoucher" role="tab" data-toggle="tab">Get Claims Voucher</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" id="eclaimsManagementPanels">
                
                <div role="tabpanel" class="tab-pane" id="MemPIN">
                    <form class="form-inline" id="getMemberPIN">
                        <h4 class="title">Get Member PIN</h4>
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" id="pMemberFirstName" placeholder="" name="pMemberFirstName">
                        </div>
                        <div class="form-group">
                            <label>Middle Name</label>
                            <input type="text" class="form-control" id="pMemberMiddleName" placeholder="" name="pMemberMiddleName">
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" id="pMemberLastName" placeholder="" name="pMemberLastName">
                        </div>
                        <div class="form-group">
                            <label>Name Suffix</label>
                            <input type="text" class="form-control" id="pMemberSufix" placeholder="" name="pMemberSufix">
                        </div><br />
                        <div class="form-group">
                            <label>Birthdate</label>
                            <input type="date" class="form-control" id="pMemberBirthDate" placeholder="" name="pMemberBirthDate">
                        </div>
                        <div class="form-group" id="ECWSPIN">
                            <label></label>
                        </div><br />
                        <button id="getPIN" type="submit" class="btn btn-default">Get Member PIN</button>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="EmpPEN">
                    <form class="form-inline" id="getEmployerPEN">
                        <h4 class="title">Search Employer</h4>
                        <div class="form-group">
                            <label>Employer PEN</label>
                            <input type="text" class="form-control" id="empPen" placeholder="" name="empPen">
                        </div>
                        <div class="form-group">
                            <label>Employer Name</label>
                            <input type="text" class="form-control" id="empName" placeholder="" name="empName">
                        </div>
                        <div class="form-group" id="ECWSPEN" style="width:50%;">
                            <label></label>
                        </div><br />
                        <button id="searchEmployer" type="submit" class="btn btn-default">Search Employer</button>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="DocPAN">
                    <form class="form-inline" id="getDoctorPan">
                        <h4 class="title">Get Doctor PAN</h4>
                        <div class="form-group">
                            <label>Doctor TIN</label>
                            <input type="text" class="form-control" id="pDoctorTIN" placeholder="" name="pDoctorTIN">
                        </div>
                        <div class="form-group" id="ECWS">
                            <label></label>
                        </div><br />
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" id="pDoctorFirstName" placeholder="" name="pDoctorFirstName">
                        </div>
                        <div class="form-group">
                            <label>Middle Name</label>
                            <input type="text" class="form-control" id="pDoctorMiddleName" placeholder="" name="pDoctorMiddleName">
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" id="pDoctorLastName" placeholder="" name="pDoctorLastName">
                        </div>
                        <div class="form-group">
                            <label>Name Suffix</label>
                            <input type="text" class="form-control" id="pDoctorSufix" placeholder="" name="pDoctorSufix">
                        </div><br />
                        <div class="form-group">
                            <label>Birthdate</label>
                            <input type="date" class="form-control" id="pDoctorBirthDate" placeholder="" name="pDoctorBirthDate">
                        </div><br />
                        <button type="submit" class="btn btn-default">Get Doctor PAN</button>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="DocAccree">
                    <form class="form-inline" id="docAccreditation">
                        <h4 class="title">Doctor Accreditation Check</h4>
                        <div class="form-group">
                            <label>Doctor Accredication Code</label>
                            <input type="text" class="form-control" id="pDoctorAccreCode" placeholder="" name="pDoctorAccreCode">
                        </div>
                        <div class="form-group">
                            <label>Admission Date</label>
                            <input type="date" class="form-control" id="pAdmissionDate" placeholder="" name="pAdmissionDate">
                        </div>
                        <div class="form-group">
                            <label>Discharge Date</label>
                            <input type="date" class="form-control" id="pDischargeDate" placeholder="" name="pDischargeDate">
                        </div>
                        <div class="form-group" id="ECWSDOCACCRE" style="width:50%;">
                            <label></label>
                        </div><br />
                        <button type="submit" class="btn btn-default">Check Doctor</button>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="GetVoucher">
                    <form class="form-inline" id="getVoucher" style="float:left;width:30%">
                        <h4 class="title">Get Claims Voucher</h4>
                        <div class="form-group" style="width:90%">
                            <label>Voucher No</label>
                            <input type="text" class="form-control" id="pVoucherNo" placeholder="" name="pVoucherNo">
                        </div>
                        <br />
                        <button type="submit" class="btn btn-default">Get Voucher</button>
                    </form>
                    <div class="form-group" style="float:left;width:65%;margin-top:30px;">
                        <div id="voucherresult"></div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="SearchCaseRate">
                    <form class="form-inline" id="searchCaseRate" style="float:left;width:30%">
                        <h4 class="title">Search for Case Rate</h4>
                        <div class="form-group" style="width:90%">
                            <label>ICD10 Code</label>
                            <input type="text" class="form-control" id="pICD" placeholder="" name="pICD">
                        </div><br />
                        <label style="width:90%"> or </label>
                        <div class="form-group" style="width:90%">
                            <label>RVS Code</label>
                            <input type="text" class="form-control" id="pRVS" placeholder="" name="pRVS">
                        </div><br />
                        <div class="form-group hidden" style="width:90%">
                            <label>Description</label>
                            <input type="text" class="form-control" id="pDescription" placeholder="" name="pDescription">
                        </div><br />
                        <button type="submit" class="btn btn-default">Find Case Rate</button>
                    </form>
                    <div class="form-group" style="float:left;width:65%;margin-top:30px;">
                        <div id="searchcaseresult"></div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="SearchHospital">
                    <form class="form-inline" id="seaarchHospital" style="float:left;width:30%">
                        <h4 class="title">Search Hospital</h4>
                        <div class="form-group" style="width:90%">
                            <label>Hospital Code</label>
                            <input type="text" class="form-control" id="pHospitalCode" placeholder="" name="pHospitalCode">
                        </div><br />
                        <div class="form-group" style="width:90%">
                            <label>Hospital Name</label>
                            <input type="text" class="form-control" id="pHospitalName" placeholder="" name="pHospitalName">
                        </div><br />
                        <button type="submit" class="btn btn-default">Find Hospital</button>
                    </form>
                    
                    <div class="form-group" style="float:left;width:65%;margin-top:30px;">
                        <div id="searchhospitalresult"></div>
                    </div>
                </div>
            </div>

        </div>
        
    </div>
</div>