<div class="box box-solid">
    <div class="box-body">
        <h3>Submitted Claims</h3>
        {!! AsyncWidget::run('Modules\Eclaims\Widgets\SubmittedReport\SubmittedReport') !!}
        <p>The following claims are already submitted. To details about your claim, under <kbd style="background-color: #337ab7;">Actions</kbd> click: 
            <ul>
                <li><kbd>Get Claims Map</kbd> to Claim the LHIO Series No</li>
                <li><kbd>Inquire Claim Status</kbd> to get the current status of your claim</li>
                <li><kbd>Upload Additional Document</kbd> to add attachments to your claim</li>
                <li><kbd>Resubmit Claim</kbd> to resubmit your claim</li>
            </ul>
        </p>
        <table id="submitted" class="table table-bordered table-responsive datatable">
            <thead>
                <tr>
                    <th>Patient Name</th>
                    <th>Code</th>
                    <th>Date Submitted</th>
                    <th>Status</th>
                    <th class="nosort">Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach($submitted as $claim)
                <?php $pat = json_decode($claim->cf1_details); ?>
                <?php $cf2 = json_decode($claim->cf2_details); ?>
                <tr>
                    <td>{{ $pat->pPatientFirstName." ".$pat->pPatientLastName }}</td>
                    <td>{{ $cf2->FirstCaseRateCode ?? NULL }} @if(isset($cf2->SecondCaseRateCode) AND $cf2->SecondCaseRateCode != '') / {{ $cf2->SecondCaseRateCode ?? NULL }}@endif  </td>
                    <td>{{ date("M. d, Y", strtotime($claim->eclaim_file_date)) }}</td>
                    <td>{{ $claim->eclaim_status }} </td>
                    <td style="text-align:center;">
                    <div class="btn-group" style="white-space: nowrap;">
                        <div class="btn-group">
                            <a href="#" type="button" class="btn btn-primary btn-sm btn-flat dropdown-toggle" data-toggle="dropdown"> Actions <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="myTabDrop1" id="myDrop-'.$claim->healthcareservice_id.'">
                                <li><a href="#" data-load-url="{{ url('eclaims/getClaimsMap/'.$claim->eclaims_id) }}" title="Get Claims Map" data-type="map" data-toggle="modal" data-target="#eclaimsNotify" data-type="inquire" data-loading="Fetching Claims Map. Please wait...">Get Claims Map </a></li>
                                <li><a href="#" data-load-url="{{ url('eclaims/getClaimsStatus/'.$claim->eclaims_id) }}" title="Inquire Claim Status" data-toggle="modal" data-target="#eclaimsNotify" data-type="inquire" data-loading="Inquiring claim status. Please wait...">Inquire Claim Status </a></li>                                              
                                <li role='separator' class='divider'></li>
                                <li><a href="#" title="Upload Documents" data-toggle="modal" data-id="{{ $claim->eclaims_id }}" data-target="#addUploadModal" data-type="submit">Upload Additional Document </a></li>
                                <li role='separator' class='divider'></li>
                                <li><a href="#" data-load-url="{{ url('eclaims/resubmitClaim/'.$claim->eclaims_id) }}" title="Check Eligibility" data-toggle="modal" data-target="#eclaimsNotify" data-type="submit" data-loading="Resubmitting claim. Please wait...">Resubmit Claim </a></li>
                                </ul>
                        </div>
                        <div class="btn-group">
                            <a href="#" type="button" class="btn btn-warning btn-sm btn-flat dropdown-toggle" data-toggle="dropdown"> Forms <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="myTabDrop1" id="myDrop-'.$claim->healthcareservice_id.'">
                                <li><a href="{{ url('eclaims/show/eligible/'.$claim->eclaims_id) }}" title="View Claim"> View Eligibility (PBEF) </a></li>
                                <li role='separator' class='divider'></li>
                                <li><a href="{{ url('eclaims/show/cf1/'.$claim->eclaims_id) }}" title="View CF1">View CF1 </a></li>
                                <li><a href="{{ url('eclaims/show/cf2/'.$claim->eclaims_id) }}" title="View CF2">View CF2 </a></li>
                                @if($claim->cf3_details)
                                <li><a href="{{ url('eclaims/show/cf3/'.$claim->eclaims_id) }}" title="View CF3">View CF3 </a></li>
                                @else
                                <li><a href="{{ url('eclaims/show/cf3/'.$claim->eclaims_id) }}" title="View CF3">Fill CF3 </a></li>
                                @endif
                                @if($claim->cf4_details)
                                <li><a href="{{ url('eclaims/show/cf4/'.$claim->eclaims_id) }}" title="View CF4">View CF4 </a></li>
                                @else
                                <li><a href="{{ url('eclaims/show/cf4/'.$claim->eclaims_id) }}" title="View CF4">Fill CF4 </a></li>
                                @endif
                                <li role='separator' class='divider'></li>
                                <li><a href="#" data-load-url="{{ url('eclaims/attachments/'.$claim->eclaims_id) }}" data-loading="Loading... Please wait..." data-toggle="modal" data-type="listing" data-target="#eclaimsNotify" title="View Attachments">View Attachments </a></li>
                                <li role='separator' class='divider'></li>
                                <li><a href="{{ url('eclaims/fillform/'.$claim->eclaims_id.'/receipts') }}" title="View Receipts and Particulars"> View Receipts and Particulars </a></li>
                                </ul>
                        </div>
                        <a href="#" data-load-url="{{ url('eclaims/viewSubmitted/'.$claim->eclaims_id) }}" class="btn btn-sm btn-success btn-flat smalhide" title="View Claim"  data-toggle="modal" data-type="viewsub" data-target="#eclaimsNotify" data-loading="Retrieving Submitted Claim. Please wait..."><i class="fa fa-eye"></i> View Claim </a>
                    </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>