<div id="drug-base-form" hidden>
	<div class="single-drug-form">
		<div class="col-sm-12">
			<fieldset>
				<legend>
					<div class="form-inline">
						<label>Drug/Medicine </label>
						<button type="button" class="btn btn-default btn-sm pull-right remove-drug-form"><i class="fa fa-close"></i></button>
					</div>
				</legend>
				<div class="col-sm-12">
					<label class="control-label col-sm-2"> Purchase Date </label>
					<div class="col-sm-4">
						{!! Form::text('PARTICULARS[DRGMED][1][pPurchaseDate]',NULL,['class'=>'form-control datepicker','id'=>'datepicker','placeholder'=>'MM/DD/YYYY']) !!}
					</div>
				</div>
				<div class="col-sm-12">
					<label class="control-label col-sm-2"> Hospital Drug Code </label>
					<div class="col-sm-4">
						{!! Form::text('PARTICULARS[DRGMED][1][pDrugCode]',NULL,['class'=>'form-control']) !!}
					</div>
					<label class="control-label col-sm-2"> PNDF Code </label>
					<div class="col-sm-4">
						{!! Form::text('PARTICULARS[DRGMED][1][pPNDFCode]',NULL,['class'=>'form-control']) !!}
					</div>
				</div>
				<div class="col-sm-12">
					<label class="control-label col-sm-2"> Generic Name </label>
					<div class="col-sm-4">
						{!! Form::text('PARTICULARS[DRGMED][1][pGenericName]',NULL,['class'=>'form-control']) !!}
					</div>
					<label class="control-label col-sm-2"> Brand Name </label>
					<div class="col-sm-4">
						{!! Form::text('PARTICULARS[DRGMED][1][pBrandName]',NULL,['class'=>'form-control']) !!}
					</div>
				</div>
				<div class="col-sm-12">
					<label class="control-label col-sm-2"> Preparation </label>
					<div class="col-sm-4">
						{!! Form::text('PARTICULARS[DRGMED][1][pPreparation]',NULL,['class'=>'form-control']) !!}
					</div>
					<label class="control-label col-sm-2"> Quantity </label>
					<div class="col-sm-4">
						{!! Form::text('PARTICULARS[DRGMED][1][pQuantity]',NULL,['class'=>'form-control']) !!}
					</div>
				</div>
			</fieldset>
		</div>
	</div>
</div>