<div id="receipt-base-form" hidden>
	<div class="single-receipt-form">
		<div class="col-sm-12">
			<fieldset>
				<legend>
					<div class="form-inline">
						<label>Receipt # </label>
						{!! Form::text('RECEIPT[1][pReceiptNumber]',NULL,['class'=>'form-control']) !!}
						<button type="button" class="btn btn-default btn-sm pull-right remove-receipt-form"><i class="fa fa-close"></i></button>
					</div>
				</legend>
				<div class="col-sm-12">
					<label class="control-label col-sm-2"> Receipt Date </label>
					<div class="col-sm-4">
						{!! Form::text('RECEIPT[1][pReceiptDate]',NULL,['class'=>'form-control datepicker','id'=>'datepicker','placeholder'=>'MM/DD/YYYY']) !!}
					</div>
					<label class="control-label col-sm-2"> Company Name </label>
					<div class="col-sm-4">
						{!! Form::text('RECEIPT[1][pCompanyName]',NULL,['class'=>'form-control']) !!}
					</div>
				</div>
				<div class="col-sm-12">
					<label class="control-label col-sm-2"> Company TIN </label>
					<div class="col-sm-4">
						{!! Form::text('RECEIPT[1][pCompanyTIN]',NULL,['class'=>'form-control']) !!}
					</div>
					<label class="control-label col-sm-2"> BIR Permit Number </label>
					<div class="col-sm-4">
						{!! Form::text('RECEIPT[1][pBIRPermitNumber]',NULL,['class'=>'form-control']) !!}
					</div>
				</div>
				<div class="col-sm-12">
					<label class="control-label col-sm-2"> VAT Exempt Sale </label>
					<div class="col-sm-4">
						{!! Form::text('RECEIPT[1][pVATExemptSale]',NULL,['class'=>'form-control']) !!}
					</div>
					<label class="control-label col-sm-2"> VAT </label>
					<div class="col-sm-4">
						{!! Form::text('RECEIPT[1][pVAT]',NULL,['class'=>'form-control']) !!}
					</div>
				</div>
				<div class="col-sm-12">
					<label class="control-label col-sm-2"> Total </label>
					<div class="col-sm-4">
						{!! Form::text('RECEIPT[1][pTotal]',NULL,['class'=>'form-control']) !!}
					</div>
				</div>
				<div class="col-sm-12">
					<h4> Items </h4>
				</div>
				<div class="col-sm-12">
					<div class="item-table">
						<div class="table-responsive">
						  <table class="table">
						    <thead>
						    	<tr>
						    		<th><label class="control-label">Quantity</label></th>
						    		<th><label class="control-label">Unit Price</label></th>
						    		<th><label class="control-label">Description</label></th>
						    		<th><label class="control-label">Amount</label></th>
						    		<th></th>
						    	</tr>
						    </thead>
						    <tbody>
						    	<tr class="item-row">
						    		<td>{!! Form::text('RECEIPT[1][ITEM][pQuantity][]',NULL,['class'=>'form-control']) !!}</td>
						    		<td>{!! Form::text('RECEIPT[1][ITEM][pUnitPrice][]',NULL,['class'=>'form-control']) !!}</td>
						    		<td>{!! Form::text('RECEIPT[1][ITEM][pDescription][]',NULL,['class'=>'form-control']) !!}</td>
						    		<td>{!! Form::text('RECEIPT[1][ITEM][pAmount][]',NULL,['class'=>'form-control']) !!}</td>
						    		<td><button type="button" class="btn btn-warning btn-sm delete-item-btn"><i class="fa fa-close"></i></button></td>
						    	</tr>
						    </tbody>
						  </table>
						</div>
						<div class="col-sm-12">
							<div class="pull-right">
								<button type="button" class="btn btn-success btn-sm add-item-btn" ><i class="fa fa-plus"></i> Add Item</button>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>
</div>