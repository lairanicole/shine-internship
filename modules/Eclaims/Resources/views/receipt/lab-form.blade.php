<div id="lab-base-form" hidden>
	<div class="single-lab-form">
		<div class="col-sm-12">
			<fieldset>
				<legend>
					<div class="form-inline">
						<label>Laboratory </label>
						<button type="button" class="btn btn-default btn-sm pull-right remove-lab-form"><i class="fa fa-close"></i></button>
					</div>
				</legend>
				<div class="col-sm-12">
					<label class="control-label col-sm-2"> Diagnostic Date </label>
					<div class="col-sm-4">
						{!! Form::text('PARTICULARS[XLSO][1][pDiagnosticDate]',NULL,['class'=>'form-control datepicker','id'=>'datepicker','placeholder'=>'MM/DD/YYYY']) !!}
					</div>
					<label class="control-label col-sm-2"> Diagnostic Type  </label>
					<div class="col-sm-4">
						{!! Form::select('PARTICULARS[XLSO][1][pDiagnosticType]',
						['IMAGING'=>'IMAGING','LABORATORY'=>'LABORATORY','SUPPLIES'=>'SUPPLIES','OTHERS'=>'OTHERS']
						,NULL,['class'=>'form-control','placeholder'=>'Select diagnostic type...']) !!}
					</div>
				</div>
				<div class="col-sm-12">
					<label class="control-label col-sm-2"> Diagnostic Name </label>
					<div class="col-sm-4">
						{!! Form::text('PARTICULARS[XLSO][1][pDiagnosticName]',NULL,['class'=>'form-control']) !!}
					</div>
					<label class="control-label col-sm-2"> Quantity </label>
					<div class="col-sm-4">
						{!! Form::text('PARTICULARS[XLSO][1][pQuantity]',NULL,['class'=>'form-control']) !!}
					</div>
				</div>
			</fieldset>
		</div>
	</div>
</div>