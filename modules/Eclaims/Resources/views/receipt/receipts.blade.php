<?php

	$receipts = json_decode($claim->receipts,TRUE)['RECEIPTS'];
	$drugs = json_decode($claim->particulars,TRUE)['PARTICULARS']['DRGMED'];
	$labs = json_decode($claim->particulars,TRUE)['PARTICULARS']['XLSO'];
?>

@extends('eclaims::layouts.master')

@section('heads')
@stop

@section('content')

	<div class="col-sm-12">
		<div class="box box-primary">
		  <div class="box-header with-border">
		    <h3 class="box-title">Manage Claim Receipts and Particulars</h3>
		  </div>
		  <div class="box-body">
		  	<div class="col-sm-12">
		  		<h5>
		  			<div class="col-sm-2">	Patient Name: 
		  			</div> 
		  			<div class="col-sm-10">
		  				{{$cf1->pMemberLastName}}, {{$cf1->pMemberFirstName}} {{$cf1->pMemberMiddleName}}
		  			</div>
		  		</h5>
		  	</div>
		  	<div class="col-sm-12">
		  		<h5>
		  			<div class="col-sm-2">	Patient PIN: 
		  			</div> 
		  			<div class="col-sm-10">
		  				{{$claim->patient_pin}}
		  			</div>
		  		</h5>
		  	</div>
			
			@include('eclaims::receipt.drug-form')
			@include('eclaims::receipt.lab-form')
			@include('eclaims::receipt.receipt-form')
			{!! Form::open(array('url'=>'eclaims/addreceipt/'.$claim->eclaims_id)) !!}
		  	<div id="particulars-nav" style="padding-top: 75px;">
				<ul class="nav nav-tabs">
				  <li class="active"><a data-toggle="tab" href="#drugs">Drugs/Medicines</a></li>
				  <li><a data-toggle="tab" href="#labs">Laboratories</a></li>
				  <li><a data-toggle="tab" href="#receipts">Receipts</a></li>
				</ul>

				<div class="tab-content">
					{!! Form::hidden('eclaims_id',$eclaims_id) !!}
					<div id="drugs" class="tab-pane fade in active">
						<div class="page-header">
							Drugs/Medicines
							<button type="button" class="btn btn-primary pull-right" id="add-drug-btn">Add Drugs/Medicines</button>
						</div>
						<div id="drugs-div" style="padding-top: 10px;">
							@if($drugs AND count($drugs)>0)	
								@foreach($drugs as $key => $drug)
									<?php $namekey = $key+1; ?>
									<div class="single-drug-form">
										<div class="col-sm-12">
											<fieldset>
												<legend>
													<div class="form-inline">
														<label>Drug/Medicine </label>
														<button type="button" class="btn btn-default btn-sm pull-right remove-drug-form"><i class="fa fa-close"></i></button>
													</div>
												</legend>
												<div class="col-sm-12">
													<label class="control-label col-sm-2"> Purchase Date </label>
													<div class="col-sm-4">
														{!! Form::text('PARTICULARS[DRGMED]['.$namekey.'][pPurchaseDate]',isset($drug['pPurchaseDate']) ? str_replace('-','/',$drug['pPurchaseDate']) : NULL,['class'=>'form-control datepicker','id'=>'datepicker','placeholder'=>'MM/DD/YYYY']) !!}
													</div>
												</div>
												<div class="col-sm-12">
													<label class="control-label col-sm-2"> Hospital Drug Code </label>
													<div class="col-sm-4">
														{!! Form::text('PARTICULARS[DRGMED]['.$namekey.'][pDrugCode]',isset($drug['pDrugCode']) ? $drug['pDrugCode'] : NULL,['class'=>'form-control']) !!}
													</div>
													<label class="control-label col-sm-2"> PNDF Code </label>
													<div class="col-sm-4">
														{!! Form::text('PARTICULARS[DRGMED]['.$namekey.'][pPNDFCode]',isset($drug['pPNDFCode']) ? $drug['pPNDFCode'] : NULL,['class'=>'form-control']) !!}
													</div>
												</div>
												<div class="col-sm-12">
													<label class="control-label col-sm-2"> Generic Name </label>
													<div class="col-sm-4">
														{!! Form::text('PARTICULARS[DRGMED]['.$namekey.'][pGenericName]',isset($drug['pGenericName']) ? $drug['pGenericName'] : NULL,['class'=>'form-control']) !!}
													</div>
													<label class="control-label col-sm-2"> Brand Name </label>
													<div class="col-sm-4">
														{!! Form::text('PARTICULARS[DRGMED]['.$namekey.'][pBrandName]',isset($drug['pBrandName']) ? $drug['pBrandName'] : NULL,['class'=>'form-control']) !!}
													</div>
												</div>
												<div class="col-sm-12">
													<label class="control-label col-sm-2"> Preparation </label>
													<div class="col-sm-4">
														{!! Form::text('PARTICULARS[DRGMED]['.$namekey.'][pPreparation]',isset($drug['pPreparation']) ? $drug['pPreparation'] : NULL,['class'=>'form-control']) !!}
													</div>
													<label class="control-label col-sm-2"> Quantity </label>
													<div class="col-sm-4">
														{!! Form::text('PARTICULARS[DRGMED]['.$namekey.'][pQuantity]',isset($drug['pQuantity']) ? $drug['pQuantity'] : NULL,['class'=>'form-control']) !!}
													</div>
												</div>
											</fieldset>
										</div>
									</div>
								@endforeach
							@endif
						</div>
					</div>
					<div id="labs" class="tab-pane fade in">
						<div class="page-header">
							Laboratories
							<button type="button" class="btn btn-primary pull-right" id="add-lab-btn">Add Laboratories</button>
						</div>	
						<div id="labs-div" style="padding-top: 10px;">
							@if($labs AND count($labs)>0)	
								@foreach($labs as $key => $lab)
									<?php $namekey = $key+1; ?>
									<div class="single-lab-form">
										<div class="col-sm-12">
											<fieldset>
												<legend>
													<div class="form-inline">
														<label>Laboratory </label>
														<button type="button" class="btn btn-default btn-sm pull-right remove-lab-form"><i class="fa fa-close"></i></button>
													</div>
												</legend>
												<div class="col-sm-12">
													<label class="control-label col-sm-2"> Diagnostic Date </label>
													<div class="col-sm-4">
														{!! Form::text('PARTICULARS[XLSO][1][pDiagnosticDate]',isset($lab['pDiagnosticDate']) ? str_replace('-','/',$lab['pDiagnosticDate']) : NULL,['class'=>'form-control datepicker','id'=>'datepicker','placeholder'=>'MM/DD/YYYY']) !!}
													</div>
													<label class="control-label col-sm-2"> Diagnostic Type  </label>
													<div class="col-sm-4">
														{!! Form::select('PARTICULARS[XLSO][1][pDiagnosticType]',
														['IMAGING'=>'IMAGING','LABORATORY'=>'LABORATORY','SUPPLIES'=>'SUPPLIES','OTHERS'=>'OTHERS']
														,isset($lab['pDiagnosticType']) ? $lab['pDiagnosticType'] : NULL,['class'=>'form-control','placeholder'=>'Select diagnostic type...']) !!}
													</div>
												</div>
												<div class="col-sm-12">
													<label class="control-label col-sm-2"> Diagnostic Name </label>
													<div class="col-sm-4">
														{!! Form::text('PARTICULARS[XLSO][1][pDiagnosticName]',isset($lab['pDiagnosticName']) ? $lab['pDiagnosticName'] : NULL,['class'=>'form-control']) !!}
													</div>
													<label class="control-label col-sm-2"> Quantity </label>
													<div class="col-sm-4">
														{!! Form::text('PARTICULARS[XLSO][1][pQuantity]',isset($lab['pQuantity']) ? $lab['pQuantity'] : NULL,['class'=>'form-control']) !!}
													</div>
												</div>
											</fieldset>
										</div>
									</div>
								@endforeach
							@endif
						</div>				
					</div>
					<div id="receipts" class="tab-pane fade in">
						<div class="page-header">
							Receipts
							<button type="button" class="btn btn-primary pull-right" id="add-receipt-btn">Add Receipts</button>
						</div>
						<div id="receipt-div" style="padding-top: 10px;">
							@if($receipts AND count($receipts)>0)	
								@foreach($receipts as $key => $receipt)
									<?php $namekey = $key+1; ?>
									<div class="single-receipt-form">
										<div class="col-sm-12">
											<fieldset>
												<legend>
													<div class="form-inline">
														<label>Receipt # </label>
														{!! Form::text('RECEIPT['.$namekey.'][pReceiptNumber]',isset($receipt['pReceiptNumber']) ? $receipt['pReceiptNumber'] : NULL,['class'=>'form-control']) !!}
														<button type="button" class="btn btn-default btn-sm pull-right remove-receipt-form"><i class="fa fa-close"></i></button>
													</div>
												</legend>
												<div class="col-sm-12">
													<label class="control-label col-sm-2"> Receipt Date </label>
													<div class="col-sm-4">
														{!! Form::text('RECEIPT['.$namekey.'][pReceiptDate]',isset($receipt['pReceiptDate']) ? str_replace('-','/',$receipt['pReceiptDate']) : NULL,['class'=>'form-control datepicker','id'=>'datepicker','placeholder'=>'MM/DD/YYYY']) !!}
													</div>
													<label class="control-label col-sm-2"> Company Name </label>
													<div class="col-sm-4">
														{!! Form::text('RECEIPT['.$namekey.'][pCompanyName]',isset($receipt['pCompanyName']) ? $receipt['pCompanyName'] : NULL,['class'=>'form-control']) !!}
													</div>
												</div>
												<div class="col-sm-12">
													<label class="control-label col-sm-2"> Company TIN </label>
													<div class="col-sm-4">
														{!! Form::text('RECEIPT['.$namekey.'][pCompanyTIN]',isset($receipt['pCompanyTIN']) ? $receipt['pCompanyTIN'] : NULL,['class'=>'form-control']) !!}
													</div>
													<label class="control-label col-sm-2"> BIR Permit Number </label>
													<div class="col-sm-4">
														{!! Form::text('RECEIPT['.$namekey.'][pBIRPermitNumber]',isset($receipt['pBIRPermitNumber']) ? $receipt['pBIRPermitNumber'] : NULL,['class'=>'form-control']) !!}
													</div>
												</div>
												<div class="col-sm-12">
													<label class="control-label col-sm-2"> VAT Exempt Sale </label>
													<div class="col-sm-4">
														{!! Form::text('RECEIPT['.$namekey.'][pVATExemptSale]',isset($receipt['pVATExemptSale']) ? $receipt['pVATExemptSale'] : NULL,['class'=>'form-control']) !!}
													</div>
													<label class="control-label col-sm-2"> VAT </label>
													<div class="col-sm-4">
														{!! Form::text('RECEIPT['.$namekey.'][pVAT]',isset($receipt['pVAT']) ? $receipt['pVAT'] : NULL,['class'=>'form-control']) !!}
													</div>
												</div>
												<div class="col-sm-12">
													<label class="control-label col-sm-2"> Total </label>
													<div class="col-sm-4">
														{!! Form::text('RECEIPT['.$namekey.'][pTotal]',isset($receipt['pTotal']) ? $receipt['pTotal'] : NULL,['class'=>'form-control']) !!}
													</div>
												</div>
												<div class="col-sm-12">
													<h4> Items </h4>
												</div>
												<div class="col-sm-12">
													<div class="item-table">
														<div class="table-responsive">
														  <table class="table">
														    <thead>
														    	<tr>
														    		<th><label class="control-label">Quantity</label></th>
														    		<th><label class="control-label">Unit Price</label></th>
														    		<th><label class="control-label">Description</label></th>
														    		<th><label class="control-label">Amount</label></th>
														    		<th></th>
														    	</tr>
														    </thead>
														    <tbody>
													    		@if(isset($receipt['ITEM']) AND count($receipt['ITEM']) > 0)
														    		@foreach($receipt['ITEM'] as $item_key => $item_value)
															    	<tr class="item-row">
															    		<td>{!! Form::text('RECEIPT['.$namekey.'][ITEM][pQuantity][]',isset($item_value['pQuantity']) ? $item_value['pQuantity'] : NULL,['class'=>'form-control']) !!}</td>
															    		<td>{!! Form::text('RECEIPT['.$namekey.'][ITEM][pUnitPrice][]',isset($item_value['pUnitPrice']) ? $item_value['pUnitPrice'] : NULL,['class'=>'form-control']) !!}</td>
															    		<td>{!! Form::text('RECEIPT['.$namekey.'][ITEM][pDescription][]',isset($item_value['pDescription']) ? $item_value['pDescription'] : NULL,['class'=>'form-control']) !!}</td>
															    		<td>{!! Form::text('RECEIPT['.$namekey.'][ITEM][pAmount][]',isset($item_value['pAmount']) ? $item_value['pAmount'] : NULL,['class'=>'form-control']) !!}</td>
															    		<td><button type="button" class="btn btn-warning btn-sm delete-item-btn"><i class="fa fa-close"></i></button></td>
															    	</tr>
														    		@endforeach
														    	@else
																	<tr class="item-row">
															    		<td>{!! Form::text('RECEIPT['.$namekey.'][ITEM][pQuantity][]',NULL,['class'=>'form-control']) !!}</td>
															    		<td>{!! Form::text('RECEIPT['.$namekey.'][ITEM][pUnitPrice][]',NULL,['class'=>'form-control']) !!}</td>
															    		<td>{!! Form::text('RECEIPT['.$namekey.'][ITEM][pDescription][]',NULL,['class'=>'form-control']) !!}</td>
															    		<td>{!! Form::text('RECEIPT['.$namekey.'][ITEM][pAmount][]',NULL,['class'=>'form-control']) !!}</td>
															    		<td><button type="button" class="btn btn-warning btn-sm delete-item-btn"><i class="fa fa-close"></i></button></td>
															    	</tr>													    		
													    		@endif
														    </tbody>
														  </table>
														</div>
														<div class="col-sm-12">
															<div class="pull-right">
																<button type="button" class="btn btn-success btn-sm add-item-btn" ><i class="fa fa-plus"></i> Add Item</button>
															</div>
														</div>
													</div>
												</div>
											</fieldset>
										</div>
									</div>							
								@endforeach
							@endif
						</div>
					</div>
			    </div>
			</div>

			<div class="mainbuttons col-md-12 textcenter">
			    <button type="submit" class="btn btn-success">Submit</button>
			    <a type="button" class="btn btn-default" href='{{ url("/eclaims") }}'>Close</a>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop

@section('plugin_jsscripts')
	<script type="text/javascript">
		$("#add-drug-btn").click(function() {
			var drug_form_count = $('.single-drug-form').size();
			console.log($("#drug-base-form").html());
			var drugform = $("#drug-base-form").html().replace(/\[DRGMED\]\[1\]/g,'[DRGMED]['+drug_form_count+']');
			$("#drugs-div").append(drugform);

			$("#drugs-div").find('input.datepicker').daterangepicker({
	            singleDatePicker: true,
	            showDropdowns: true,
	            "minDate": "01/01/1900",
	            "maxDate": "{{ date('m/d/Y') }}"
	        });
		});
		$("#drugs").on('click','.remove-drug-form',function() {
			var formdiv = $(this).closest("div.single-drug-form");
			formdiv.remove();
		});

		$("#add-lab-btn").click(function() {
			var lab_form_count = $('.single-lab-form').size();
			var labform = $("#lab-base-form").html().replace(/\[XLSO\]\[1\]/g,'[XLSO]['+lab_form_count+']');
			$("#labs-div").append(labform);

			$("#labs-div").find('input.datepicker').daterangepicker({
	            singleDatePicker: true,
	            showDropdowns: true,
	            "minDate": "01/01/1900",
	            "maxDate": "{{ date('m/d/Y') }}"
	        });
		});
		$("#labs").on('click','.remove-lab-form',function() {
			var formdiv = $(this).closest("div.single-lab-form");
			formdiv.remove();
		});


		$("#add-receipt-btn").click(function() {
			var receipt_form_count = $('.single-receipt-form').size();
			var receiptform = $("#receipt-base-form").html().replace(/RECEIPT\[1\]/g,'RECEIPT['+receipt_form_count+']');
			$("#receipt-div").append(receiptform);

			$("#receipt-div").find('input.datepicker').daterangepicker({
	            singleDatePicker: true,
	            showDropdowns: true,
	            "minDate": "01/01/1900",
	            "maxDate": "{{ date('m/d/Y') }}"
	        });
		});
		$("#receipts").on('click','.remove-receipt-form',function() {
			var formdiv = $(this).closest("div.single-receipt-form");
			formdiv.remove();
		});

		$('#receipts').on('click','.add-item-btn',function(){
			var parentTable = $(this).closest(".item-table");
			var prot = parentTable.find("tr.item-row:first").clone(true);
	        prot.find('input').val('');
	        var lastRow = parentTable.find("tr.item-row:last");
	        prot.insertAfter(lastRow);
		});
		$('#receipts').on('click','.delete-item-btn',function(){
			var master = $(this).closest('tr');
    		master.remove();
		});
	</script>
@stop