<h4>Voucher Details</h4>
<div class="row">
	<div class="col-sm-offset-1 col-sm-6"><strong> Voucher No: </strong></div>
	<div class="col-sm-5" style="text-align: right"> {{ $voucher['@attributes']['pVoucherNo'] }} </div>
	<div class="col-sm-offset-1 col-sm-6"><strong> Voucher Date: </strong></div>
	<div class="col-sm-5" style="text-align: right"> {{ $voucher['@attributes']['pVoucherDate'] }} </div>
</div>


<h4>Voucher Summary</h4>
<div class="row">
	<div class="col-sm-offset-1 col-sm-6"><strong> Total Amount: </strong></div>
	<div class="col-sm-5" style="text-align: right"> {{ $voucher['SUMMARY']['@attributes']['pTotalAmount'] }} </div>
	<div class="col-sm-offset-1 col-sm-6"><strong> Number of Claims: </strong></div>
	<div class="col-sm-5" style="text-align: right"> {{ $voucher['SUMMARY']['@attributes']['pNumberOfClaims'] }} </div>
</div>


<h4>Payee Details</h4>
<table class="table table-striped table-condensed">
    <thead>
      <tr>
      	<th>#</th>
        <th>Type</th>
        <th>Code</th>
        <th>Name</th>
        <th>Gross</th>
        <th>Tax</th>
        <th>Net</th>
        <th>Check No.</th>
        <th>Check Date</th>
      </tr>
    </thead>
    <tbody>
    	@if(count($voucher['SUMMARY']['PAYEE'] > 1))
			@foreach($voucher['SUMMARY']['PAYEE'] as $key => $payee)
				<tr>
					<td>{{ $key+1 }}</td>
					<td>{{ $payee['@attributes']['pPayeeType'] }}</td>
					<td>{{ $payee['@attributes']['pPayeeCode'] }}</td>
					<td>{{ $payee['@attributes']['pPayeeName'] }}</td>
					<td style="text-align: right">{{ $payee['@attributes']['pGrossAmount'] }}</td>
					<td style="text-align: right">{{ $payee['@attributes']['pTaxAmount'] }}</td>
					<td style="text-align: right">{{ $payee['@attributes']['pNetAmount'] }}</td>
					<td style="text-align: right">{{ $payee['@attributes']['pCheckNo'] }}</td>
					<td style="text-align: right">{{ $payee['@attributes']['pCheckDate'] }}</td>
				</tr>
			@endforeach
		@else
			<tr>
				<td>1</td>
				<td>{{ $voucher['SUMMARY']['PAYEE']['@attributes']['pPayeeType'] }}</td>
				<td>{{ $voucher['SUMMARY']['PAYEE']['@attributes']['pPayeeCode'] }}</td>
				<td>{{ $voucher['SUMMARY']['PAYEE']['@attributes']['pPayeeName'] }}</td>
				<td style="text-align: right">{{ $voucher['SUMMARY']['PAYEE']['@attributes']['pGrossAmount'] }}</td>
				<td style="text-align: right">{{ $voucher['SUMMARY']['PAYEE']['@attributes']['pTaxAmount'] }}</td>
				<td style="text-align: right">{{ $voucher['SUMMARY']['PAYEE']['@attributes']['pNetAmount'] }}</td>
				<td style="text-align: right">{{ $voucher['SUMMARY']['PAYEE']['@attributes']['pCheckNo'] }}</td>
				<td style="text-align: right">{{ $voucher['SUMMARY']['PAYEE']['@attributes']['pCheckDate'] }}</td>
			</tr>			
		@endif
    </tbody>
</div>

