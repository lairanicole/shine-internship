<?php
	$facilities = array(
		'pCheckFacilityH1'=>'Level 1 Hospital',
		'pCheckFacilityH2'=>'Level 2 Hospital',
		'pCheckFacilityH3'=>'Level 3 Hospital',
		'pCheckFacilityASC'=>'Ambulatory Surgical Clinic',
		'pCheckFacilityPCF'=>'Primary Care Facility',
		'pCheckFacilityMAT'=>'Maternity Care Package Provider',
		'pCheckFacilityFSDC'=>'Freestanding Dialysis Clinics',
	);

	$columns = ['pPrimaryHCIFee','pPrimaryProfFee','pPrimaryCaseRate','pSecondaryHCIFee','pSecondaryProfFee','pSecondaryCaseRate'];

$results = []

?>


<legend>Case Rates</legend>

@if(isset($caserates['@attributes']) && isset($caserates['AMOUNT']))
	<?php $results[] = $caserates; ?>
@else
	<?php $results = $caserates; ?>
@endif
@foreach($results as $key => $caserate)
	<?php $values = []; ?>
	<div class="row">
		<div class="col-sm-5"><strong> Case Rate Code: </strong></div>
		<div class="col-sm-7"> {{ $caserate['@attributes']['pCaseRateCode'] }} </div>
		<div class="col-sm-5"><strong> Case Rate Description: </strong></div>
		<div class="col-sm-7"> {{ $caserate['@attributes']['pCaseRateDescription'] }} </div>
		<div class="col-sm-5"><strong> Item Code: </strong></div>
		<div class="col-sm-7"> {{ $caserate['@attributes']['pItemCode'] }} </div>
		<div class="col-sm-5"><strong> Item Description: </strong></div>
		<div class="col-sm-7"> {{ $caserate['@attributes']['pItemDescription'] }} </div>
		<div class="col-sm-5"><strong> Effectivity Date: </strong></div>
		<div class="col-sm-7"> {{ $caserate['@attributes']['pEffectivityDate'] }} </div>
		<div class="col-sm-5"> <strong> Amounts </strong> </div>
		<table class="table table-condensed table-bordered">
		    <thead>
		      <tr>
		      	<th>Health Facility Type</th>
		        <th style="text-align: center;">Primary HCI Fee </th>
		        <th style="text-align: center;">Primary Professional Fee</th>
		        <th style="text-align: center;">Primary Case Rate</th>
		        <th style="text-align: center;">Secondary HCI Fee </th>
		        <th style="text-align: center;">Secondary Professional Fee</th>
		        <th style="text-align: center;">Secondary Case Rate</th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach($facilities as $facility_key => $facility)
		    	<tr>
	    			<td><strong>{{ $facility }}</strong></td>
	    			@foreach($columns as $column )
    					@if(count($caserate['AMOUNT']) > 1)
	    					@foreach($caserate['AMOUNT'] as $amount_key => $amount) 
	    						@if($amount['@attributes'][$facility_key] == 'T') 
	    							<?php $values[$facility_key][$column] = number_format($amount['@attributes'][$column], 2, '.', ''); ?>
	    						@endif 
	    					@endforeach
						@else
    						@if($caserate['AMOUNT']['@attributes'][$facility_key] == 'T') 
    							<?php $values[$facility_key][$column] = number_format($caserate['AMOUNT']['@attributes'][$column], 2, '.', ''); ?>
    						@endif 	    							
    					@endif
	    				<td style="text-align: right;">
							{{ $values[$facility_key][$column] or 'NA' }}
	    				</td>
	    			@endforeach
		    	</tr>
	    		@endforeach
		    </tbody>
		</table>
	</div>
	<div class="row">
		<legend></legend>
	</div>
@endforeach