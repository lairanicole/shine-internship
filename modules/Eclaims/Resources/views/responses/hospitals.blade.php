<h4> Hospitals </h4>

<table class="table table-striped table-condensed">
    <thead>
      <tr>
      	<th>#</th>
        <th>Accreditation Code</th>
        <th>PMCC No.</th>
        <th>Hospital Name</th>
      </tr>
    </thead>
    <tbody>
    @if(count($hospitals) > 1)
  		@foreach($hospitals as $key => $hospital)
  			<tr>
  				<td>{{ $key+1 }}</td>
  				<td>{{ $hospital['@attributes']['pAccreCode'] }}</td>
  				<td>{{ $hospital['@attributes']['pPMCCNo'] }}</td>
  				<td>{{ $hospital['@attributes']['pHospitalName'] }}</td>
  			</tr>
  		@endforeach
    @else
        <tr>
          <td>1</td>
          <td>{{ $hospitals['@attributes']['pAccreCode'] }}</td>
          <td>{{ $hospitals['@attributes']['pPMCCNo'] }}</td>
          <td>{{ $hospitals['@attributes']['pHospitalName'] }}</td>
        </tr>      
    @endif
    </tbody>
</table>
