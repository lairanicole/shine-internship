<?php
$documents = array(
    'CAB' => "Clinical Abstract",
    'CAE' => "Certification of Approval/Agreement from the Employer",
    'CF1' => "Claim Form 1",
    'CF2' => "Claim Form 2",
    'CF3' => "Claim Form 3",
    'COE' => "Certificate of Eligibility",
    'CSF' => "Claim Signature Form",
    'CTR' => "Confirmatory Test Results by SACCL or RITM",
    'DTR' => "Diagnostic Test Result",
    'MBC' => "Member's Birth Certificate",
    'MDR' => "Proof of MDR with Payment Details",
    'MEF' => "Member Empowerment Form",
    'MMC' => "Member's Marriage Contract",
    'MSR' => "Malarial Smear Results",
    'MWV' => "Waiver for Consent for Release of Confidential Patient Health Information",
    'NTP' => "NTP Registry Card",
    'OPR' => "Operative Record",
    'ORS' => "Official Receipts",
    'PAC' => "Pre-Authorization Clearance",
    'PBC' => "Patient's Birth Certificate",
    'PIC' => "Valid Philhealth Indigent ID",
    'POR' => "PhilHealth Official Receipts",
    'SOA' => "Statement of Account",
    'STR' => "HIV Screening Test Result",
    'TCC' => "TB-Diagnostic Committee Certification (-) Sputum",
    'TYP' => "Three Years Payment of (2400 x 3 years of proof of payment)",
    'MRF' => "PhilHealth Member Registration Form",
    'ANR' => "Anesthesia Record",
    'HDR' => "Hemodialysis Record",
    'OTH' => "Other documents",
);

?>

@extends('eclaims::layouts.master')

@section('heads')
<style>
    .tab-pane .box {
        background: #FFF !important;
    }
    .modal-header {
        border-radius: 6px 6px 0 0 !important;
    }
    .dl-horizontal dt {
        width: 200px;
        padding-right: 15px;
        text-align: left;
    }
    dl, hr {
        margin: 0;
    }
    h5 {
        font-weight: 900;
        margin:7px 0;
        padding:0;
    }
    hr.sign {
        margin-top: 120px;
        border: 1px solid #222;
    }
    .pad10 {
        padding: 10px;
    }
    .bordered {
        background-color:#999;
        height:1px;
    }
    table, tr, td {
        padding:0;
    }
    table th {
        width: auto !important;
    }
    #eclaimsManagementPanels .form-group {
        width: 20%;
    }
    #eclaimsManagementPanels .form-group label {
        width: 100%;
    }
    .nav>li>a {
        padding: 10px 10px !important;
    }
    @media print {
        #printForm hr {
            margin:0;
            padding:0;
            height:3px;
        }
        #printForm td h5 {
            margin:0;
            padding:0;
            font-size:8px;
        }
        #printForm td, #printForm td p {
            font-size: 8px;
            line-height:1;
            margin:0;
            padding:0;
        }
        #printForm .bordered {
            border-bottom: 2px solid #333;
        }
        #printForm small, #printForm .small {
            font-size: 6px;
        }

    }
    div.box-header.with-border {
        padding: 25px 0px !important;
    }

    #save-fac-button, #change-fac-close {
        margin-top: 38px;
        /*width: 100px;*/
    }

    .change-buttons {
        margin-top: 35px;
    }
</style>
@stop

@section('eclaims-content')

<!-- Main content -->
<div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <div id="with-fac">
                @if($selected_facility && $selected_accreditation)
                    <div class="col-sm-12">
                        <div class="col-sm-5">
                            <h4> Selected Facility </h4>
                            <label class="control-label">{{ $eclaims_facility[$selected_facility] }}</label>
                        </div>
                        <div class="col-sm-5">
                            <h4> Selected Accreditation </h4>
                            <label class="control-label">{{ $accreditation_types[$selected_accreditation] }}</label>
                        </div>
                        <div class="col-sm-2 change-buttons">
                            <button class="btn btn-sm btn-warning" id="change-fac-button"> Change Facility </button>
                        </div>
                    </div>
                @endif
            </div>
            <div id="change-fac" class="@if($selected) hidden @endif">
                <div class="col-sm-12">
                    {!! Form::open(array('url'=>'eclaims/saveaccreditation')) !!}
                        <div class="col-sm-5">
                            <h4> Select Facility </h4>
                            {!! Form::select('hci_hospitalcode',$eclaims_facility,isset($selected_facility) ? $selected_facility : NULL, ['class'=>'form-control','placeholder'=>'Please select an accredited facility...','required'=>'required']) !!}
                        </div>
                        <div class="col-sm-5">
                            <h4> Select Accreditation </h4>
                            {!! Form::select('hci_accreditation',$accreditation_types,isset($selected_accreditation) ? $selected_accreditation : NULL, ['class'=>'form-control','placeholder'=>'Please select an accreditation type...','required'=>'required']) !!}
                        </div>
                        <div class="col-sm-2">
                            <input type="submit" class="btn btn-success" id="save-fac-button" value="Select"/>
                            <button class="btn btn-danger" id="change-fac-close"> Cancel </button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @if($selected_facility && $selected_accreditation)
        <div class="box-body">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#eligibility" aria-controls="eligibility" role="tab" data-toggle="tab">Qualified For Claims</a></li>
                <li role="presentation"><a href="#eligibilitycheck" aria-controls="eligibilitycheck" role="tab" data-toggle="tab">Eligibility Check</a></li>
                <li role="presentation"><a href="#submitted" aria-controls="submitted" role="tab" data-toggle="tab">Submitted Claims</a></li>
                <li role="presentation"><a href="#management" aria-controls="management" role="tab" data-toggle="tab">eClaims Management</a></li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="eligibility">
                    @include('eclaims::partials.qualified')
                </div>

                <div role="tabpanel" class="tab-pane" id="eligibilitycheck">
                    @include('eclaims::partials.eligibilitycheck')
                </div>

                <div role="tabpanel" class="tab-pane" id="submitted">
                    @include('eclaims::partials.submitted')
                </div>

                <div role="tabpanel" class="tab-pane" id="management">
                    @include('eclaims::partials.management')
                </div>
            </div>
        </div>
        @endif
    </div>
</div>

@stop

@section('scripts')
<div class="modal fade" id="myInfoModal" tabindex="-1" role="dialog" aria-labelledby="myInfoModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-yellow">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myInfoModalLabel"> Healthcare Record Preview </h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="eclaimsNotify" tabindex="-1" role="dialog" aria-labelledby="eclaimsNotify">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-yellow">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myInfoModalLabel"> E-Claims Module </h4>
            </div>
            <div class="modal-body">
                <p class="result"></p>
            </div>
            <div class="modal-footer eclaims-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-yellow">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="uploadModalLabel">Upload a Document for Claim Requirement</h4>
			</div>
			<form id="uploadProject">
				<div class="modal-body">
					<div class="form-group text-center hide" id="uploadwait">
						<i class='fa fa-sync fa-spin fa-fw'></i> Please wait. Uploading your Document/s.
					</div>
					<div class="form-group" id="uploadForm">
                        <label class="control-label">Select Type of File: </label>
                        {!! Form::select('send_doc_type',$documents,NULL,['class'=>'form-control','required'=>'required','placeholder'=>'Please select one...']) !!}
                        <label class="control-label">Document File: </label>
                        <input type="file" placeholder="Document File" name="send_doc" id="sendfile" class="form-control" />
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="hidden" name="eclaimid" id="eclaimid" value="">
					</div>
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-success" id="docuploadbutton">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="addUploadModal" tabindex="-1" role="dialog" aria-labelledby="addUploadModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-yellow">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="uploadModalLabel">Upload Additional Document for Claim Requirement</h4>
            </div>
            <form id="uploadAdditionalProject">
                <div class="modal-body">
                    <div class="form-group text-center hide" id="uploadwait">
                        <i class='fa fa-sync fa-spin fa-fw'></i> Please wait. Uploading your Document/s.
                    </div>
                    <div class="form-group" id="uploadForm">
                        <label class="control-label">Select Type of File: </label>
                        {!! Form::select('add_doc_type',$documents,NULL,['class'=>'form-control','required'=>'required','placeholder'=>'Please select one...']) !!}
                        <label class="control-label">Document File: </label>
                        <input type="file" placeholder="Document File" name="add_doc" id="addfile" class="form-control" />
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <input type="hidden" name="eclaimid" id="eclaimid" value="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="docsubmitbutton">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#change-fac-button").click(function(){
        $("#change-fac").removeClass("hidden");
        $("#with-fac").addClass("hidden");
    });

    $("#change-fac-close").click(function(){
        $("#change-fac").addClass("hidden");
        $("#with-fac").removeClass("hidden");
    });


    //modal script for viewing the healthcare record
    $("#myInfoModal").on("show.bs.modal", function(e) {
        $(this).find(".modal-content").html("");
        var link = $(e.relatedTarget);
        var loadurl = link.data('load-url');
        $(this).find(".modal-content").load(loadurl);
    });
    $("#myInfoModal").on("hide.bs.modal", function(e) {
        location.reload();
    });

    $("#eclaimsNotify").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        var loadurl = link.data('load-url');
        var type = link.data('type');
        var loader = "<i class='fa fa-spinner fa-pulse fa-fw'></i> " + link.data('loading');
        $(this).find(".result").html(loader);
        $(this).find(".result").load(loadurl, function( response, status, xhr ){
            console.log(response)

            if( response && response.match(/Claim is successfully submitted/) == 'Claim is successfully submitted') {
                $('#eclaimsNotify').modal('hide');
                swal({
                    title: 'Success!',
                    text: response,
                    type: 'success'
                }).then(function() {
                    location.reload();
                });
            } 
            else if ( response && response.match(/E-Claim Status Inquiry/) == 'E-Claim Status Inquiry') {
                // $('#eclaimsNotify').on('hide.bs.modal', function (e) {
                //     location.reload();
                // });
            }
            else if ( status == "error" ) {
                var msg = "Sorry but there was an error: ";
                $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
            }
            else {
                // alert(response.match(/E-Claim Status Inquiry/));
            }
        });
        if(type == 'viewsub') {
            $("#eclaimsNotify").find('.eclaims-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button class="btn btn-success" onclick="$(\'.result\').printThis();">Print Claim Receipt</button>');
        }
    });

    $("#uploadModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        var eclaimid = link.data('id');
        $(this).find("#eclaimid").val(eclaimid);
    });

    $("#addUploadModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        var eclaimid = link.data('id');
        $(this).find("#eclaimid").val(eclaimid);
    });

    $(document).ready(function() {

        //datatable for qualified claims
        $('table#qualified').DataTable({
            "Paginate": true,
            "LengthChange": true,
            "Filter": true,
            "Sort": true,
            "Info": true,
            "AutoWidth": true,
            "columnDefs": [
                { "orderable": false, "targets": 'nosort' }
            ],
            dom: "<'row'<'col-sm-5 col-xs-5 hidden-xs'l><'col-sm-5 col-xs-12'f><'col-sm-2 col-xs-2 hidden-xs'B>>" +
                "<'row'<'col-sm-12 col-xs-12'tr>>" +
                "<'row'<'col-sm-5 col-xs-12'i><'col-sm-7 col-xs-12'p>>",
            buttons: [
                {
                    text: '<span>Submit Manual Claim</span>',
                    action: function ( e, dt, node, config ) {
                        location="{{ url('eclaims/checkEligibility') }}";
                    },
                    className: 'btn btn-sm btn-warning xsbtn'
                }
            ]
        });

        $('#getMemberPIN').on('submit', function(e){
            e.preventDefault();
            $("#ECWSPIN label").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Member PIN...");

            $.ajax({
                url: "{{ url('eclaims/getPIN') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "pMemberFirstName": $('#pMemberFirstName').val(), 
                    "pMemberMiddleName": $('#pMemberMiddleName').val(), 
                    "pMemberLastName": $('#pMemberLastName').val(), 
                    "pMemberSuffix": $('#pMemberSuffix').val(), 
                    "pMemberBirthDate": $('#pMemberBirthDate').val()
                }, 
                type: "post",
                dataType: "json"
            })
            .done(function(data) {
                console.log(data);

                if(data.error && data.error=='validation_error')
                {
                    swal("Error!", "Validation error!", 'error');                 
                    $("#ECWSPIN label").html(data.message.join("<br>"));
                }

                if(data.ISOK && data.ISOK=='error')
                {
                    swal("Error!", data.MESSAGE, 'error');                 
                    $("#ECWSPIN label").html("");
                }
                if(data.indexOf('NO RECORDS') > -1) {
                    swal("Member not found!", data, "warning");
                    $("#ECWSPIN label").html("");
                } else {
                    swal("Member found!", "The Member PIN will be displayed on this page.", "success");
                    $('#ECWSPIN label').html("Member PIN: "+data);
                }
                $('#getPIN').removeAttr('disabled');
            })
            .fail( function() {
                swal("Oops!", 'Something went wrong. Cannot retrieve PAN Information. Please try again.', 'error');
                $("#ECWSPIN label").html("");
                $('#getPIN').removeAttr('disabled');
            });
        })

        $('#getEmployerPEN').on('submit', function(e){
            e.preventDefault();
            $("#ECWSPEN label").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Searching Employer...");

            $.ajax({
                url: "{{ url('eclaims/searchEmployer') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "empPen": $('#empPen').val(), 
                    "empName": $('#empName').val(), 
                }, 
                type: "post",
                dataType: "json"
            })
            .done(function(data) {
                //console.log(data);
                if(data.error && data.error=='validation_error')
                {
                    swal("Error!", "Validation error!", 'error');                 
                    $("#ECWSPEN label").html(data.message.join("<br>"));
                }

                if(data.employer) {
                    swal("Member found!", "The Member Employer information will be displayed on this page.", "success");
                    $('#ECWSPEN label').html("Member Name: "+data.employer.node.pEmployerName+"<br>Member PEN: "+data.employer.node.pPEN+"<br>Member Address: "+data.employer.node.pEmployerAddress);
                } else {
                    swal("Employer not found!", data, "warning");
                    $("#ECWSPEN label").html("");
                }
                $('#searchEmployer').removeAttr('disabled');
            })
            .fail( function() {
                swal("Oops!", 'Something went wrong. Cannot retrieve Employer information. Please try again.', 'error');
                $("#ECWSPEN label").html("");
                $('#searchEmployer').removeAttr('disabled');
            });
        })

        $('#getDoctorPan').on('submit', function(e){
            e.preventDefault();
            $("#ECWSPAN label").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Doctor PAN...");

            $.ajax({
                url: "{{ url('eclaims/getPAN') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "pDoctorTIN": $('#pDoctorTIN').val(), 
                    "pDoctorFirstName": $('#pDoctorFirstName').val(), 
                    "pDoctorMiddleName": $('#pDoctorMiddleName').val(), 
                    "pDoctorLastName": $('#pDoctorLastName').val(), 
                    "pDoctorSufix": $('#pDoctorSufix').val(), 
                    "pDoctorBirthDate": $('#pDoctorBirthDate').val()
                }, 
                type: "post",
                dataType: "json"
            })
            .done(function(data) {
                if(data.error && data.error=='validation_error')
                {
                    swal("Error!", "Validation error!", 'error');                 
                    $("#ECWSPAN label").html(data.message.join("<br>"));
                }

                if(data && data.ISOK == "OK") {
                    swal("Doctor found!", "Your ECWS Number will be displayed on this page.", "success");
                    $('#ECWSPAN label').html("Doctor PAN: "+data.ECWS);  
                }
                if(data && data.ISOK == "NODOC") {
                    swal("Doctor not found!", "Your Account may not be setup properly. Please complete your professional data and try again.", "warning");
                    $("#ECWSPAN label").html("");
                } else {
                    swal("Doctor not found!", "Doctor was not found in Philhealth records. Please check your data and try again.", "warning");
                    $("#ECWSPAN label").html("");
                }
                $('#getDoctorPan button[type=submit]').removeAttr('disabled');
            })
            .fail( function() {
                swal("Oops!", 'Something went wrong. Cannot retrieve PAN Information. Please try again.', 'error');
                $("#ECWSPAN label").html("");
                $('#getDoctorPan button[type=submit]').removeAttr('disabled');
            });
        })

        $('#docAccreditation').on('submit', function(e){
            e.preventDefault();
            $("#ECWSDOCACCRE label").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Checking Doctor Accreditation...");

            $.ajax({
                url: "{{ url('eclaims/checkAccr') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "pHospitalCode": $('#pHospitalCode').val(), 
                    "pDoctorAccreCode": $('#pDoctorAccreCode').val(), 
                    "pAdmissionDate": $('#pAdmissionDate').val(), 
                    "pDischargeDate": $('#pDischargeDate').val()
                }, 
                type: "post",
                dataType: "json"
            })
            .done(function(data) {
                if(data.error && data.error=='validation_error')
                {
                    swal("Error!", "Validation error!", 'error');                 
                    $("#ECWSDOCACCRE label").html(data.message.join("<br>"));
                }

                if(data && data.ISOK == "error") {
                    $('#ECWSDOCACCRE label').html('');
                    swal("Warning!", data.MESSAGE, "warning");
                }
                if(data && data.ISOK == "NODOC") {
                    $('#ECWSDOCACCRE label').html('');
                    swal("Doctor not found!", "Your Account may not be setup properly. Please complete your professional data and try again.", "warning");
                }
                if(data && data.node && data.node.IsAccredited == "YES") {
                    swal("Doctor is accredited!", "The Doctor Accreditation details will be displayed on this page!", "success");
                    $('#ECWSDOCACCRE label').html("Accreditation Code: "+data.node.pDoctorAccreCode+"<br>Admission Date: "+data.node.pAdmissionDate+"<br>Discharge Date: "+data.node.pDischargeDate+"<br>Accreditation Start: "+data.node.pAccreditationStart+"<br>Accreditation End: "+data.node.pAccreditationEnd);
                }
                $('#docAccreditation button[type=submit]').removeAttr('disabled');
            })
            .fail( function() {
                $('#ECWSDOCACCRE label').html('');
                swal("Oops!", 'Something went wrong. Cannot retrieve PAN Information. Please try again.', 'error');
                $('#docAccreditation button[type=submit]').removeAttr('disabled');
            });
        })

        $('#getVoucher').on('submit', function(e){
            e.preventDefault();
            $("#voucherresult").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Claims Voucher...");

            $.ajax({
                url: "{{ url('eclaims/inquireVoucher') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "pVoucherNo": $('#pVoucherNo').val()
                }, 
                type: "post",
                dataType: "json"
            })
            .done(function(data) {
                console.log(data);
                if(data.error && data.error=='validation_error')
                {
                    swal("Error!", "Validation error!", 'error');                 
                    $("#voucherresult").html(data.message.join("<br>"));
                }

                if(data && data.ISOK == "success") {
                    console.log(data.MESSAGE);
                    swal("Success!", "Voucher details found." ,"success");
                    $('#voucherresult').html(data.MESSAGE);
                } else {
                    $('#voucherresult').html("<h4>"+data.MESSAGE+"</h4>");
                    swal("Voucher not found!", 'You search did not return any results.', 'warning');
                }
                $('#getVoucher button[type=submit]').removeAttr('disabled');
            })
            .fail( function() {
                $('#voucherresult').html('');
                swal("Oops!", 'Something went wrong. Cannot retrieve PAN Information. Please try again.', 'error');
                $('#getVoucher button[type=submit]').removeAttr('disabled');
            });
        })

        $('#searchCaseRate').on('submit', function(e){
            e.preventDefault();
            $("#searchcaseresult").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Searching Case Rate...");

            $.ajax({
                url: "{{ url('eclaims/searchCaseRate') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "pICD": $('#pICD').val(),
                    "pRVS": $('#pRVS').val(),
                    "pDescription": $('#pDescription').val()
                }, 
                type: "post",
                dataType: "json"
            })
            .done(function(data) {
                console.log(data);
                if(data) {

                    if(data.error && data.error=='validation_error')
                    {
                        swal("Error!", "Validation error!", 'error');                 
                        $("#searchcaseresult").html(data.message.join("<br>"));
                    }

                    if(data.ISOK == 'success') {
                        console.log(data.MESSAGE);
                        swal("Success!", "Case rates found." ,"success");
                        $('#searchcaseresult').html(data.MESSAGE);
                    } else {
                        swal("Case rates not found!", 'You search did not return any results.', 'warning');
                        $('#searchcaseresult').html("<h4>"+data.MESSAGE+"</h4>");
                    }
                } else {
                    $('#searchcaseresult').html('Case rates not found! Please try another code.');
                    swal("Nothing found!", 'Your search did not return any results.', 'warning');
                }
                /*if(data && data.ISOK == "NODOC") {
                    $('#voucherresult').html('');
                    swal("Doctor not found!", "Your Account may not be setup properly. Please complete your professional data and try again.", "warning");
                }*/
                $('#searchCaseRate button[type=submit]').removeAttr('disabled');
            })
            .fail( function() {
                $('#searchcaseresult').html('');
                swal("Oops!", 'Something went wrong. Cannot retrieve Case Rate Information. Please try again.', 'error');
                $('#searchCaseRate button[type=submit]').removeAttr('disabled');
            });
        })

        $('#SearchHospital').on('submit', function(e){
            e.preventDefault();
            $("#searchhospitalresult").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Searching Hospital...");

            $.ajax({
                url: "{{ url('eclaims/searchHospital') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "pHospitalCode": $('#pHospitalCode').val(),
                    "pHospitalName": $('#pHospitalName').val()
                }, 
                type: "post",
                dataType: "json"
            })
            .done(function(data) {
                if(data.error && data.error=='validation_error')
                {
                    swal("Error!", "Validation error!", 'error');                 
                    $("#searchhospitalresult").html(data.message.join("<br>"));
                }

                if(data.ISOK == 'success') {
                    console.log(data.MESSAGE);
                    swal("Success!", "Hospitals found." ,"success");
                    $('#searchhospitalresult').html(data.MESSAGE);
                } else {
                    $('#searchhospitalresult').html("<h4>"+data.MESSAGE+"</h4>");
                    swal("No hospitals found!", 'You search did not return any results.', 'warning');
                }
                $('#SearchHospital button[type=submit]').removeAttr('disabled');
            })
            .fail( function() {
                $('#searchhospitalresult').html('');
                swal("Oops!", 'Something went wrong. Cannot find information. Please try again.', 'error');
                $('#SearchHospital button[type=submit]').removeAttr('disabled');
            });
        })

        //Document Upload function
        $("#docuploadbutton").click(function(){
            //e.preventDefault();
            //let us check token
            var file = $('form#uploadProject')[0]; console.log(file);
            var formdata = new FormData(file);
            
            var eid = $('form#uploadProject').find('#eclaimid').val();
            //console.log(formdata);
            $('#uploadProject div.modal-body #uploadwait').removeClass('hide');
            $('#uploadProject div.modal-body #uploadForm').hide();
            $('#uploadProject div.modal-footer button').attr('disabled','disabled');
            $.ajax({
                url: "{{ url('eclaims/uploadDocument') }}/"+eid,
                data: formdata,
                type: "post",
                dataType: "json",
                contentType: false,
                processData:false
            })
            .done(function(data) {
                    //console.log(data);
                    if(data && data.status == "OK") {
                        //console.log(data);
                        swal("Success!" , "EClaims Document uploaded successfully.", "success");
                        location.reload();
                    } else {
                        alert(data.message[0]);
                    }
            })
            .fail(function() {
                swal("Oops" , "Error uploading your EClaim Document file. Please try again later. Or you can contact Shine is the problem persists.", "error").then(function() {
                    //location.reload();
                    $('#uploadModal').find('#sendfile').val("");
                    $('#uploadModal').modal('hide');
                    $('#uploadProject div.modal-body #uploadwait').addClass('hide');
                    $('#uploadProject div.modal-body #uploadForm').show();
                    $('#uploadProject div.modal-footer button').removeAttr('disabled');
                });
            });
        });

        $("#docsubmitbutton").click(function(){
            //e.preventDefault();
            //let us check token
            var file = $('form#uploadAdditionalProject')[0]; //console.log(file);
            var formdata = new FormData(file);
            
            var eid = $('form#uploadAdditionalProject').find('#eclaimid').val();
            //console.log(formdata);
            $('#uploadAdditionalProject div.modal-body #uploadwait').removeClass('hide');
            $('#uploadAdditionalProject div.modal-body #uploadForm').hide();
            $('#uploadAdditionalProject div.modal-footer button').attr('disabled','disabled');
            $.ajax({
                url: "{{ url('eclaims/addDocument') }}/"+eid,
                data: formdata,
                type: "post",
                dataType: "json",
                contentType: false,
                processData:false
            })
            .done(function(data) {
                    //console.log(data);
                    if(data && data.ISOK != "error") {
                        //console.log(data);
                        swal("Success!" , "EClaims Document uploaded successfully.", "success");
                        location.reload();
                    } else {
                        swal("Error!" , data.MESSAGE, "warning");
                        location.reload();
                    }
            })
            .fail(function() {
                swal("Oops" , "Error uploading your EClaim Document file. Please try again later. Or you can contact Shine is the problem persists.", "error").then(function() {
                    //location.reload();
                    $('#addUploadModal').find('#addfile').val("");
                    $('#addUploadModal').modal('hide');
                    $('#uploadAdditionalProject div.modal-body #uploadwait').addClass('hide');
                    $('#uploadAdditionalProject div.modal-body #uploadForm').show();
                    $('#uploadAdditionalProject div.modal-footer button').removeAttr('disabled');
                });
            });
        });

        //Notification for CF4 generation
        @if(session('isok'))
            swal({
                title: 'Success!',
                text: '{{ session("isok") }}',
                type: 'success'
            });
        @endif
        @if(session('notok'))
            swal({
                title: 'Success!',
                text: '{{ session("notok") }}',
                type: 'success'
            });
        @endif

    });
</script>
@stop
