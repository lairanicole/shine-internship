@extends('layout.master')
@section('title') SHINE OS+ | E-Claims Module @stop

@section('heads')
{!! HTML::style('modules/Eclaims/Assets/eclaims.css') !!}
@stop

@section('page-header')
<section class="content-header">
  <!-- edit by RJBS -->
  <h1>
	<i class="fa fa-universal-access"></i>
	E-Claims
  </h1>
  <!-- end edit RJBS -->
  <ol class="breadcrumb">
	<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
	<li class="active"> <a href="{{ url('/eclaims') }}"> E-Claims </a></li>
  </ol>
</section>
@stop

@section('content')
	<div class="row">
		<!-- Main content -->
		@yield('eclaims-content')
	</div><!-- /.row -->
@stop

@section('page_scripts')
<scriptff src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></scriptff>
{!! HTML::script('modules/Eclaims/Assets/plugins/sweetalert2.js') !!}
@stop