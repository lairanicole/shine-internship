<?php // dd($cf2Form); ?>
<div id="printFormCF2">
<style>
  .formcf2 {
    padding:0;
    height:1480px;
    width:961px;
    @if(isset($page))
    position:relative;
    margin:0 auto;
    @else
    margin:3px 0 0 3px;
    @endif
  }
  .formcf21 {
    background: #FFF url('{!! asset("modules/Eclaims/Assets/images/cf21.png") !!}') center top no-repeat !important;
    background-size: contain !important;
    margin-bottom: 20px;
  }
  .formcf22 {
    background: #FFF url('{!! asset("modules/Eclaims/Assets/images/cf22.png") !!}') center top no-repeat !important;
    background-size: contain !important;
    height:1469px;
  }
  @if(isset($page))
    #formcf21, #formcf22 {
      margin-top: 25px;
      position: relative;
      margin-left: 0px;
    }
  @endif
  .formgroup {
    position: absolute;
  }
  .formcf2 input, .formcf2 textarea {
    background-color: rgba(200,200,200,.2);
    border: none;
    height: 22px;
    padding: 1px 5px;
    max-width: 900px;
  }
  #Pan { top: 257px;left: 498px; }
    #Pan #pan.box-rect {
      width: 180px;
      background-color: rgba(200,200,200,.2);
      text-align: center;
      letter-spacing: 11.5px; }
  #Provider { top: 280px; left: 253px; }
  #PanAddress { top:306px; left:106px; }
  #Admitted { top: 517px; left: 290px; }
    #Admitted input {
        background-color: rgba(200,200,200,.2);
        text-align: center;
        letter-spacing: 11px;
        padding-left: 6px;
      }
      #Admitted input[name=ad_mon] { width: 38px; }
      #Admitted input[name=ad_dd] { width: 39px; }
      #Admitted input[name=ad_yy] { width: 76px; }
      #Admitted input[name=ad_hh] { width: 41px; }
      #Admitted input[name=ad_min] { width: 41px; }
  #AdmittedTime { top: 523px; left: 721px; }
  #Discharged { top: 543px;left: 289px; }
    #Discharged input {
        background-color: rgba(200,200,200,.2);
        text-align: center;
        letter-spacing: 11px;
        padding-left: 6px;
      }
      #Discharged input[name=dc_mon] { width: 38px; }
      #Discharged input[name=dc_dd] { width: 39px; }
      #Discharged input[name=dc_yy] { width: 76px; }
      #Discharged input[name=dc_hh] { width: 41px; }
      #Discharged input[name=dc_min] { width: 41px; }
  #DischargedTime { top: 547px; left: 721px; }
  #Name { top: 382px;left: 175px; }
  #WasReferred { top: 474px;left: 32px; }
  #Disposition { top: 597px; left: 32px; }
  #ExpiredDate { top: 590px;left: 386px; }
    #ExpiredDate input {
        background-color: rgba(200,200,200,.2);
        text-align: center;
        letter-spacing: 11px;
        padding-left: 6px;
      }
      #ExpiredDate input[name=expr_mon] { width: 39px; }
      #ExpiredDate input[name=expr_dd] { width: 37px; }
      #ExpiredDate input[name=expr_yy] { width: 78px; }
      #ExpiredDate input[name=expr_hh] { width: 38px; }
      #ExpiredDate input[name=expr_min] { width: 38px; }
  #ExpiredTime { top: 598px;left: 756px; }
  
  #HCIAddress { top: 470px;left: 155px; }
  #TransferredHCI { top: 618px;left: 437px; }
  #AccomType { top: 694px;left: 207px; }
  #AdmissionDiagnosis { top: 719px;left: 202px; }
  #DischargeDiagnosis { top: 816px;left: 46px; }
  #DischargeDiagnosis .DD { margin-top: 0;height: 21px; }
  #Considerations { top: 996px;left: 46px; }
  #ZBenefit { top: 1095px;left: 414px; }
  #MCP { top: 1142px;left: 57px; }
  #TBDots { top: 1169px;left: 185px; }
  #AnimalBite { top: 1214px;left: 105px; }
  #NewBornCare { top: 1239px;left: 215px; }
  #NewBornEssential { top: 1289px;left: 46px; }
  #ICD10 { top: 1378px;left: 250px; }

  #ACCNoA { top: 51px;left: 69px; }
      #ACCNoA #coPay01 {
        top: 32px;
        position: absolute;
        left: 434px;
        width: 400px;
      }
      #ACCNoA #coPay01 input:nth-child(1) {
        display: inline-block;
        /*width: 400px;*/
        /*height: 19px;*/
      }
      #ACCNoA #coPay01 spacer {
        width: 228px;
      }
      #ACCNoA #coPay01 input[type=text] {
        margin-top: -19px;
      }
  #ACCNoB { top: 158px;left: 69px; }
      #ACCNoB #coPay02 {
        top: 24px;
        position: absolute;
        left: 434px;
        width: 400px;
      }
      #ACCNoB #coPay02 input:nth-child(1) {
        display: inline-block;
        /*width: 400px;*/
        /*height: 19px;*/
      }
      #ACCNoB #coPay02 spacer {
        width: 228px;
      }
      #ACCNoB #coPay02 input[type=text] {
        margin-top: -19px;
      }
  #ACCNoC { top: 255px;left: 69px; }
    #ACCNoC #coPay03 {
        top: 24px;
        position: absolute;
        left: 434px;
        width: 400px;
      }
      #ACCNoC #coPay03 input:nth-child(1) {
        display: inline-block;
        /*width: 400px;*/
        /*height: 19px;*/
      }
      #ACCNoC #coPay03 spacer {
        width: 228px;
      }
      #ACCNoC #coPay03 input[type=text] {
        margin-top: -19px;
      }

  #CertificationA { top: 438px;left: 35px; z-index:100; }
  #CertificationB { top: 570px;left: 35px; }
  #CertificationB2 { top: 769px;left: 189px; }
  #TotalCost { top: 875px;left: 544px; }

  #Consent { top: 1082px;left: 30px; }
    #Consent input {
        background-color: rgba(200,200,200,.2);
        text-align: center;
        padding-left: 6px;
      }
      #Consent input[name=apr_signed_mon] { width: 38px; letter-spacing: 11px; }
      #Consent input[name=apr_signed_dd] { width: 39px; letter-spacing: 11px; }
      #Consent input[name=apr_signed_yy] { width: 76px; letter-spacing: 11px; }
  #SOA { top: 1393px;left: 32px; }

  input {
    text-transform: uppercase !important;
  }
  input.box-input {
    width: 20px;
    text-align:center;
    vertical-align: middle;
    margin-left: 0.1em;
  }

  input.pan {
    width: 17px;
    text-align:center;
    vertical-align: middle;
  }
  input.rect-input {
    width: 172px;
    vertical-align: middle;
    margin-left: 0.1em;
  }
  input.rect-input.name_field {
    margin-right:10px;
  }
  input.opt-input {
    width: 23px;
    margin: 0.5px 0 0 0;
  }
  input[type='date'] {
    font-size: 10px;
    padding: 1px 0px 1px 4px;
  }
  spacer {
    width: 12px;
    display: inline-block;
  }
  spacer.lab {
    width: 41px;
    display: inline-block;
  }
  spacer.vert {
    height: 0px;
    display: block;
    margin-top: -1px;
  }
  .w165 {
    width: 165px !important;
  }
  .w158 {
    width: 158px !important;
  }
  .w115 {
    width: 115px !important;
  }
  .w100 {
    width: 100px !important;
  }
  .w85 {
    width: 85px !important;
  }
  .w70 {
    width: 70px !important;
  }
  .w50 {
    width: 52px !important;
  }
  .w18 {
    width: 18px !important;
  }
  .w12 {
    width: 12px !important;
  }
  .w11 {
    width: 11px !important;
  }
  .alert-absolute {
    position: absolute;
    width: 93%;
  }

  @media print {
    @page {
      size: 8.5in 13in;
      margin: 0.12in;
    }
    @if(isset($page))
      #formcf21 {
        margin-top: 24px;
        position: relative;
        margin-left: 14px;
      }
      #formcf22 {
        margin-top: 32px;
        position: relative;
        margin-left: 6px;
      }
    @endif
    .formcf2 {
      width: 990px !important;
    } 
    #Pan { top: 257px;left: 498px; }
      #Pan #pan.box-rect {
        width: 180px;
        background-color: rgba(200,200,200,.2);
        text-align: center;
        letter-spacing: 11.5px; }
    #Provider { top: 280px; left: 253px; }
    #PanAddress { top:306px; left:106px; }
    #Admitted { top: 517px; left: 290px; }
      #Admitted input {
          background-color: rgba(200,200,200,.2);
          text-align: center;
          letter-spacing: 11px;
          padding-left: 6px;
        }
        #Admitted input[name=ad_mon] { width: 38px; }
        #Admitted input[name=ad_dd] { width: 39px; }
        #Admitted input[name=ad_yy] { width: 76px; }
        #Admitted input[name=ad_hh] { width: 41px; }
        #Admitted input[name=ad_min] { width: 41px; }
    #AdmittedTime { top: 523px; left: 721px; }
    #Discharged { top: 543px;left: 289px; }
      #Discharged input {
          background-color: rgba(200,200,200,.2);
          text-align: center;
          letter-spacing: 11px;
          padding-left: 6px;
        }
        #Discharged input[name=dc_mon] { width: 38px; }
        #Discharged input[name=dc_dd] { width: 39px; }
        #Discharged input[name=dc_yy] { width: 76px; }
        #Discharged input[name=dc_hh] { width: 41px; }
        #Discharged input[name=dc_min] { width: 41px; }
    #DischargedTime { top: 547px; left: 721px; }
    #Name { top: 382px;left: 175px; }
    #WasReferred { top: 474px;left: 32px; }
    #Disposition { top: 598px; left: 32px; }
    #ExpiredDate { top: 590px;left: 386px; }
      #ExpiredDate input {
          background-color: rgba(200,200,200,.2);
          text-align: center;
          letter-spacing: 11px;
          padding-left: 6px;
        }
        #ExpiredDate input[name=expr_mon] { width: 39px; }
        #ExpiredDate input[name=expr_dd] { width: 37px; }
        #ExpiredDate input[name=expr_yy] { width: 78px; }
        #ExpiredDate input[name=expr_hh] { width: 38px; }
        #ExpiredDate input[name=expr_min] { width: 38px; }
    #ExpiredTime { top: 598px;left: 756px; }
    
    #HCIAddress { top: 470px;left: 155px; }
    #TransferredHCI { top: 618px;left: 437px; }
    #AccomType { top: 694px;left: 207px; }
    #AdmissionDiagnosis { top: 719px;left: 202px; }
    #DischargeDiagnosis { top: 816px;left: 46px; }
    #DischargeDiagnosis .DD { margin-top: 0;height: 21px; }
    #Considerations { top: 996px;left: 46px; }
    #ZBenefit { top: 1095px;left: 414px; }
    #MCP { top: 1142px;left: 57px; }
    #TBDots { top: 1169px;left: 185px; }
    #AnimalBite { top: 1214px;left: 105px; }
    #NewBornCare { top: 1239px;left: 215px; }
    #NewBornEssential { top: 1289px;left: 46px; }
    #ICD10 { top: 1378px;left: 250px; }

    #ACCNoA { top: 46px;left: 76px; }
      #ACCNoA #coPay01 {
        top: 30px;
        position: absolute;
        left: 438px;
        width: 400px;
      }
      #ACCNoA #coPay01 input:nth-child(1) {
        display: inline-block;
        width: 400px;
        height: 19px;
      }
      #ACCNoA #coPay01 spacer {
        width: 228px;
      }
      #ACCNoA #coPay01 input[type=text] {
        margin-top: -19px;
      }
    #ACCNoB { top: 152px;left: 78px; }
      #ACCNoB #coPay02 {
        top: 22px;
        position: absolute;
        left: 434px;
        width: 400px;
      }
      #ACCNoB #coPay02 input:nth-child(1) {
        display: inline-block;
        width: 400px;
        height: 19px;
      }
      #ACCNoB #coPay02 spacer {
        width: 228px;
      }
      #ACCNoB #coPay02 input[type=text] {
        margin-top: -19px;
      }
    #ACCNoC { top: 250px;left: 78px; }
      #ACCNoC #coPay03 {
        top: 21px;
        position: absolute;
        left: 433px;
        width: 400px;
      }
      #ACCNoC #coPay03 input:nth-child(1) {
        display: inline-block;
        width: 400px;
        height: 19px;
      }
      #ACCNoC #coPay03 spacer {
        width: 228px;
      }
      #ACCNoC #coPay03 input[type=text] {
        margin-top: -19px;
      }

    #CertificationA { top: 430px;left: 43px; z-index:100; }
    #CertificationB { top: 562px;left: 43px; }
    #CertificationB2 { top: 761px;left: 197px; }
    #TotalCost { top: 866px;left: 553px; }

    #Consent { top: 1075px;left: 37px; }
      #Consent input {
          background-color: rgba(200,200,200,.2);
          text-align: center;
          padding-left: 6px;
        }
        #Consent input[name=apr_signed_mon] { width: 38px; letter-spacing: 11px; }
        #Consent input[name=apr_signed_dd] { width: 39px; letter-spacing: 11px; }
        #Consent input[name=apr_signed_yy] { width: 76px; letter-spacing: 11px; }
    #SOA { top: 1393px;left: 32px; }

    input.box-input {
      margin-left: 0.1em;
    }
    spacer {
      width: 15px;
    }
  }
</style>

  <form id="form_eclaims" method="post" action="{{ url('eclaims/saveform') }}">
    <div class="container-fluid pad20 formcf2 formcf21">
      <div class="roww pad20"> 
        <div id="formcf21">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="form" value="cf2">
          <input type="hidden" name="eclaims_id" value="{{ $eclaims_id }}">
          @if(isset($cf2->cf2))
          <input type="hidden" name="cf2" value="{{ $cf2->cf2 }}">
          @endif
          <div id="Pan" class="formgroup">
            <?php
              //pReferredIHCPAccreCode
              $pan = NULL;
              if(isset($cf2Form->pan)) {
                //this is a fix for previous version data
                if(is_array($cf2Form->pan)) {
                  $pan = implode("", $cf2Form->pan);
                } else {
                  $pan = $cf2Form->pan;
                }
              }
            ?>
            <input type="text" class="box-rect" id="pan" name="pan" value="{{ $pan or NULL }}" maxlength="9" />
          </div>

          <div id="Provider" class="formgroup">
            <input type="text" class="rect-input" name="hci_name" value="{{ $cf2Form->hci_name }}" style="width:651px;" />
          </div>

          <div id="PanAddress" class="formgroup">
            <input type="text" class="rect-input" name="hci_building" style="width: 323px;margin-right: 15px;" value="{{ $cf2Form->hci_building }}" /><input type="text" class="rect-input" name="hci_city" style="width: 225px;margin-right: 17px;" value="{{ $cf2Form->hci_city }}" /><input type="text" class="rect-input" name="hci_province" style="width:215px;" value="{{ $cf2Form->hci_province }}" />
          </div>
          
          <div id="Name" class="formgroup">
            <input type="text" class="rect-input name_field" name="pPatientLastName" value="{{ $cf2Form->pPatientLastName }}" /><input type="text" class="rect-input name_field" name="pPatientFirstName" value="{{ $cf2Form->pPatientFirstName }}" style="width:198px;" /><input type="text" class="rect-input name_field" name="pPatientSuffix" value="{{ $cf2Form->pPatientSuffix }}" style="width: 131px;margin-right: 17px;" /><input type="text" class="rect-input name_field" name="pPatientMiddleName" value="{{ $cf2Form->pPatientMiddleName }}" style="width:188px;" />
          </div>

          <div id="WasReferred" class="formgroup">
            <input type="radio" class="opt-input" name="pPatientReferred" value="N" @if(isset($cf2Form->pPatientReferred) AND $cf2Form->pPatientReferred == 'N') checked @endif /><spacer style="width:36px;"></spacer><input type="radio" class="opt-input" name="pPatientReferred" value="Y" @if(isset($cf2Form->pPatientReferred) AND $cf2Form->pPatientReferred == 'Y') checked @endif />
          </div>
          
          <div id="HCIAddress" class="formgroup">

            <input type="hidden" name="pReferralIHCPAccreCode" value="{{ isset($cf2Form->pReferralIHCPAccreCode) ? $cf2Form->pReferralIHCPAccreCode : NULL }}" />
            <input type="text" class="rect-input" name="referring_hci_name" style="width:232px;" value="{{ isset($cf2Form->referring_hci_name) ? $cf2Form->referring_hci_name : NULL }}" /><spacer style="width:17px;"></spacer><input type="text" class="rect-input" style="width: 178px;margin-right: 16px;"  name="referring_hci_street_name" value="{{ isset($cf2Form->referring_hci_street_name) ? $cf2Form->referring_hci_street_name : NULL }}" /><input type="text" class="rect-input" style="    margin-right: 13px;width: 102px;" name="referring_hci_city" value="{{ isset($cf2Form->referring_hci_city) ? $cf2Form->referring_hci_city : NULL }}" /><input type="text" class="rect-input" name="referring_hci_province" value="{{ isset($cf2Form->referring_hci_province) ? $cf2Form->referring_hci_province : NULL }}" style="width: 114px; margin-right: 13px;" /><input type="text" class="rect-input" style="width:60px;" name="referring_hci_zipcode" value="{{ isset($cf2Form->referring_hci_zipcode) ? $cf2Form->referring_hci_zipcode : NULL }}" />
            
          </div>
          
          <div id="Admitted" class="formgroup">
            <input type="text" class="box-rect" name="ad_mon" id="ad_mon" value="{{ $cf2Form->ad_mon }}" /><spacer></spacer><input type="text" class="box-rect" name="ad_dd" id="ad_dd" value="{{ $cf2Form->ad_dd }}" /><spacer></spacer><input type="text" class="box-rect" name="ad_yy" id="ad_yy" value="{{ $cf2Form->ad_yy }}" /><spacer style="width: 126px;"></spacer><input type="text" class="box-rect" name="ad_hh" value="{{ $cf2Form->ad_hh }}" /><spacer></spacer><input type="text" class="box-rect" name="ad_min" value="{{ $cf2Form->ad_min }}" />
          </div>
          <div id="AdmittedTime" class="formgroup">
            <input type="radio" class="opt-input" value="AM" name="ad_ampm" @if(isset($cf2Form->ad_ampm) AND $cf2Form->ad_ampm == 'AM') checked @endif /><spacer style="width: 42px !important;"></spacer><input type="radio" class="opt-input" value="PM" name="ad_ampm" @if(isset($cf2Form->ad_ampm) AND $cf2Form->ad_ampm == 'PM') checked @endif />
          </div>

          <div id="Discharged" class="formgroup">
            <?php
            //$pDischargeDate
            ?>
            <input type="text" class="box-rect" name="dc_mon" id="dc_mon" value="{{ $cf2Form->dc_mon }}" /><spacer></spacer><input type="text" class="box-rect" name="dc_dd" id="dc_dd" value="{{ $cf2Form->dc_dd }}" /><spacer></spacer><input type="text" class="box-rect" name="dc_yy" id="dc_yy" value="{{ $cf2Form->dc_yy }}" />
            <spacer style="width: 126px;"></spacer><input type="text" class="box-rect" name="dc_hh" value="{{ $cf2Form->dc_hh }}" /><spacer></spacer><input type="text" class="box-rect" name="dc_min" value="{{ $cf2Form->dc_min }}" />
          </div>
          <div id="DischargedTime" class="formgroup">
            <input type="radio" class="opt-input" value="AM" name="dc_ampm" @if(isset($cf2Form->dc_ampm) AND $cf2Form->dc_ampm == 'AM') checked @endif /><spacer style="width: 42px !important;"></spacer><input type="radio" class="opt-input" value="PM" name="dc_ampm" @if(isset($cf2Form->dc_ampm) AND $cf2Form->dc_ampm == 'PM') checked @endif />
          </div>

          <div id="Disposition" class="formgroup">
            <input type="radio" class="opt-input" name="pDisposition" value="I" @if(isset($cf2Form->pDisposition) AND $cf2Form->pDisposition == "I") checked @endif /><spacer style="width:225px;"></spacer><input type="radio" class="opt-input" name="pDisposition" value="E" @if(isset($cf2Form->pDisposition) AND $cf2Form->pDisposition == "E") checked @endif />
            <spacer class="vert"></spacer>
            <input type="radio" class="opt-input" name="pDisposition" value="R" @if(isset($cf2Form->pDisposition) AND $cf2Form->pDisposition == "R") checked @endif /><spacer style="width:225px;"></spacer><input type="radio" class="opt-input" name="pDisposition" value="T" style="top: 0; position: relative;" @if(isset($cf2Form->pDisposition) AND $cf2Form->pDisposition == "T") checked @endif />
            <spacer class="vert"></spacer>
            <input type="radio" class="opt-input" name="pDisposition" value="H" @if(isset($cf2Form->pDisposition) AND $cf2Form->pDisposition == "H") checked @endif />
            <spacer class="vert"></spacer>
            <input type="radio" class="opt-input" name="pDisposition" value="A" @if(isset($cf2Form->pDisposition) AND $cf2Form->pDisposition == "A") checked @endif />
          </div>

          <div id="ExpiredDate" class="formgroup">
            <input type="text" class="box-rect" name="expr_mon" value="{{ isset($cf2Form->expr_mon) ? $cf2Form->expr_mon : NULL }}" /><spacer></spacer><input type="text" class="box-rect" name="expr_dd" value="{{ isset($cf2Form->expr_dd) ? $cf2Form->expr_dd : NULL }}" /><spacer></spacer><input type="text" class="box-rect" name="expr_yy" value="{{ isset($cf2Form->expr_yy) ? $cf2Form->expr_yy : NULL }}" />
            <spacer style="width: 72px;"></spacer><input type="text" class="box-rect" name="expr_hh" value="{{ isset($cf2Form->expr_hh) ? $cf2Form->expr_hh : NULL }}" /><spacer></spacer><input type="text" class="box-rect" name="expr_min" value="{{ isset($cf2Form->expr_min) ? $cf2Form->expr_min : NULL }}" />
          </div>
          <div id="ExpiredTime" class="formgroup">
            <input type="radio" class="opt-input" value="AM" name="expr_ampm" @if(isset($cf2Form->expr_ampm) AND $cf2Form->expr_ampm == "AM") checked @endif /><spacer style="width: 27px !important;"></spacer><input type="radio" class="opt-input" value="PM" name="expr_ampm" @if(isset($cf2Form->expr_ampm) AND $cf2Form->expr_ampm == "PM") checked @endif />
          </div>

          <div id="TransferredHCI" class="formgroup">
            @if(isset($cf2Form->pDisposition) AND $cf2Form->pDisposition == "T")
              <input type="hidden" name="pReferralIHCPAccreCode" value="{{ isset($cf2Form->pReferralIHCPAccreCode) ? $cf2Form->pReferralIHCPAccreCode : NULL }}" />
            @endif
            <input type="text" class="rect-input" name="trans_hci_name" style="width:300px;" placeholder="FACILITY NAME" value="{{ isset($cf2Form->trans_hci_name) ? $cf2Form->trans_hci_name : NULL }}" /><input type="text" class="rect-input" name="pReferralIHCPAccreCode" style="width:164px;" placeholder="IHCP ACCRE CODE" value="{{ isset($cf2Form->pReferralIHCPAccreCode) ? $cf2Form->pReferralIHCPAccreCode : NULL }}" />
            <spacer class="vert" style="height: 0px !important;"></spacer><input type="text" class="rect-input" style="width: 177px;margin-left: -30px;margin-right: 18px;" name="trans_hci_street_name" value="{{ isset($cf2Form->trans_hci_street_name) ? $cf2Form->trans_hci_street_name : NULL }}" /><input type="text" class="rect-input" name="trans_hci_city" value="{{ isset($cf2Form->trans_hci_city) ? $cf2Form->trans_hci_city : NULL }}" style="margin-right:18px;width:97px;" /><input type="text" class="rect-input" name="trans_hci_province" value="{{ isset($cf2Form->trans_hci_province) ? $cf2Form->trans_hci_province : NULL }}" style="width:111px;margin-right:17px;" /><input type="text" class="rect-input" style="width:54px;" name="trans_hci_zipcode" value="{{ isset($cf2Form->trans_hci_zipcode) ? $cf2Form->trans_hci_zipcode : NULL }}" />
            <spacer class="vert" style="height: 11px !important;"></spacer><input type="text" class="rect-input" style="width:420px;margin-left:43px;" name="pReferralReasons" value="{{ isset($cf2Form->pReferralReasons) ? $cf2Form->pReferralReasons : NULL }}" />
          </div>

          <div id="AccomType" class="formgroup">
            <input type="radio" class="opt-input" name="pAccommodationType" value="P" @if(isset($cf2Form->pAccommodationType) AND $cf2Form->pAccommodationType == "P") checked @endif /><spacer style="width:62px;"></spacer><input type="radio" class="opt-input" name="pAccommodationType" value="N" @if(isset($cf2Form->pAccommodationType) AND $cf2Form->pAccommodationType == "N") checked @endif />
          </div>

          <div id="AdmissionDiagnosis" class="formgroup">
            <textarea name="pAdmissionDiagnosis" style="width:711px;height:54px;">{{ isset($cf2Form->pAdmissionDiagnosis) ? $cf2Form->pAdmissionDiagnosis : NULL }}</textarea>
          </div>

          <div id="DischargeDiagnosis" class="formgroup">
            <div class="DD DDa">
              <input type="text" class="rect-input" name="pDischargeDiagnosis[]" style="width:110px;" value="{{ isset($cf2Form->pDischargeDiagnosis[0]) ? $cf2Form->pDischargeDiagnosis[0] : NULL }}" />
              <spacer class="w11"></spacer>
              <input type="text" class="rect-input" style="width:86px;"  name="pICDCode[]" value="{{ isset($cf2Form->pICDCode[0]) ? $cf2Form->pICDCode[0] : NULL }}" />
              <spacer style="width:25px;"></spacer>
              <input type="text" class="rect-input" style="width:179px;" name="pRelatedProcedure[]" value="{{ isset($cf2Form->pRelatedProcedure[0]) ? $cf2Form->pRelatedProcedure[0] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="text" class="rect-input" style="width:92px;" name="pRVSCode[]" value="{{ isset($cf2Form->pRVSCode[0]) ? $cf2Form->pRVSCode[0] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="date" class="rect-input" style="width:102px;" name="pProcedureDate[]" value="{{ isset($cf2Form->pProcedureDate[0]) ? $cf2Form->pProcedureDate[0] : NULL }}" />
              <spacer style="width:14px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[0]" value="L" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[0]) AND $cf2Form->pLaterality[0] == "L") checked @endif />
              <spacer style="width:26px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[0]" value="R" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[0]) AND $cf2Form->pLaterality[0] == "R") checked @endif />
              <spacer style="width:32px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[0]" value="B" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[0]) AND $cf2Form->pLaterality[0] == "B") checked @endif />
            </div>
            <div class="DD DDb">
              <input type="text" class="rect-input" name="pDischargeDiagnosis[]" style="width:110px;" value="{{ isset($cf2Form->pDischargeDiagnosis[1]) ? $cf2Form->pDischargeDiagnosis[1] : NULL }}" />
              <spacer class="w11"></spacer>
              <input type="text" class="rect-input" style="width:86px;"  name="pICDCode[]" value="{{ isset($cf2Form->pICDCode[1]) ? $cf2Form->pICDCode[1] : NULL }}" />
              <spacer style="width:25px;"></spacer>
              <input type="text" class="rect-input" style="width:179px;" name="pRelatedProcedure[]" value="{{ isset($cf2Form->pRelatedProcedure[1]) ? $cf2Form->pRelatedProcedure[1] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="text" class="rect-input" style="width:92px;" name="pRVSCode[]" value="{{ isset($cf2Form->pRVSCode[1]) ? $cf2Form->pRVSCode[1] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="date" class="rect-input" style="width:102px;" name="pProcedureDate[]" value="{{ isset($cf2Form->pProcedureDate[1]) ? $cf2Form->pProcedureDate[1] : NULL }}" />
              <spacer style="width:14px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[1]" value="L" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[1]) AND $cf2Form->pLaterality[1] == "L") checked @endif />
              <spacer style="width:26px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[1]" value="R" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[1]) AND $cf2Form->pLaterality[1] == "R") checked @endif />
              <spacer style="width:32px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[1]" value="B" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[1]) AND $cf2Form->pLaterality[1] == "B") checked @endif />
            </div>
            <div class="DD DDc">
              <input type="text" class="rect-input" name="pDischargeDiagnosis[]" style="width:110px;" value="{{ isset($cf2Form->pDischargeDiagnosis[2]) ? $cf2Form->pDischargeDiagnosis[2] : NULL }}" />
              <spacer class="w11"></spacer>
              <input type="text" class="rect-input" style="width:86px;"  name="pICDCode[]" value="{{ isset($cf2Form->pICDCode[2]) ? $cf2Form->pICDCode[2] : NULL }}" />
              <spacer style="width:25px;"></spacer>
              <input type="text" class="rect-input" style="width:179px;" name="pRelatedProcedure[]" value="{{ isset($cf2Form->pRelatedProcedure[2]) ? $cf2Form->pRelatedProcedure[2] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="text" class="rect-input" style="width:92px;" name="pRVSCode[]" value="{{ isset($cf2Form->pRVSCode[2]) ? $cf2Form->pRVSCode[2] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="date" class="rect-input" style="width:102px;" name="pProcedureDate[]" value="{{ isset($cf2Form->pProcedureDate[2]) ? $cf2Form->pProcedureDate[2] : NULL }}" />
              <spacer style="width:14px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[2]" value="L" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[2]) AND $cf2Form->pLaterality[2] == "L") checked @endif />
              <spacer style="width:26px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[2]" value="R" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[2]) AND $cf2Form->pLaterality[2] == "R") checked @endif />
              <spacer style="width:32px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[2]" value="B" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[2]) AND $cf2Form->pLaterality[2] == "B") checked @endif />
            </div>
            <div class="DD DDd">
              <input type="text" class="rect-input" name="pDischargeDiagnosis[]" style="width:110px;" value="{{ isset($cf2Form->pDischargeDiagnosis[3]) ? $cf2Form->pDischargeDiagnosis[3] : NULL }}" />
              <spacer class="w11"></spacer>
              <input type="text" class="rect-input" style="width:86px;"  name="pICDCode[]" value="{{ isset($cf2Form->pICDCode[3]) ? $cf2Form->pICDCode[3] : NULL }}" />
              <spacer style="width:25px;"></spacer>
              <input type="text" class="rect-input" style="width:179px;" name="pRelatedProcedure[]" value="{{ isset($cf2Form->pRelatedProcedure[3]) ? $cf2Form->pRelatedProcedure[3] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="text" class="rect-input" style="width:92px;" name="pRVSCode[]" value="{{ isset($cf2Form->pRVSCode[3]) ? $cf2Form->pRVSCode[3] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="date" class="rect-input" style="width:102px;" name="pProcedureDate[]" value="{{ isset($cf2Form->pProcedureDate[3]) ? $cf2Form->pProcedureDate[3] : NULL }}" />
              <spacer style="width:14px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[3]" value="L" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[3]) AND $cf2Form->pLaterality[3] == "L") checked @endif />
              <spacer style="width:26px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[3]" value="R" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[3]) AND $cf2Form->pLaterality[3] == "R") checked @endif />
              <spacer style="width:32px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[3]" value="B" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[3]) AND $cf2Form->pLaterality[3] == "B") checked @endif />
            </div>
            <div class="DD DDe">
              <input type="text" class="rect-input" name="pDischargeDiagnosis[]" style="width:110px;" value="{{ isset($cf2Form->pDischargeDiagnosis[4]) ? $cf2Form->pDischargeDiagnosis[4] : NULL }}" />
              <spacer class="w11"></spacer>
              <input type="text" class="rect-input" style="width:86px;"  name="pICDCode[]" value="{{ isset($cf2Form->pICDCode[4]) ? $cf2Form->pICDCode[4] : NULL }}" />
              <spacer style="width:25px;"></spacer>
              <input type="text" class="rect-input" style="width:179px;" name="pRelatedProcedure[]" value="{{ isset($cf2Form->pRelatedProcedure[4]) ? $cf2Form->pRelatedProcedure[4] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="text" class="rect-input" style="width:92px;" name="pRVSCode[]" value="{{ isset($cf2Form->pRVSCode[4]) ? $cf2Form->pRVSCode[4] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="date" class="rect-input" style="width:102px;" name="pProcedureDate[]" value="{{ isset($cf2Form->pProcedureDate[4]) ? $cf2Form->pProcedureDate[4] : NULL }}" />
              <spacer style="width:14px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[4]" value="L" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[4]) AND $cf2Form->pLaterality[4] == "L") checked @endif />
              <spacer style="width:26px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[4]" value="R" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[4]) AND $cf2Form->pLaterality[4] == "R") checked @endif />
              <spacer style="width:32px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[4]" value="B" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[4]) AND $cf2Form->pLaterality[4] == "B") checked @endif />
            </div>
            <div class="DD DDf">
              <input type="text" class="rect-input" name="pDischargeDiagnosis[]" style="width:110px;" value="{{ isset($cf2Form->pDischargeDiagnosis[5]) ? $cf2Form->pDischargeDiagnosis[5] : NULL }}" />
              <spacer class="w11"></spacer>
              <input type="text" class="rect-input" style="width:86px;"  name="pICDCode[]" value="{{ isset($cf2Form->pICDCode[5]) ? $cf2Form->pICDCode[5] : NULL }}" />
              <spacer style="width:25px;"></spacer>
              <input type="text" class="rect-input" style="width:179px;" name="pRelatedProcedure[]" value="{{ isset($cf2Form->pRelatedProcedure[5]) ? $cf2Form->pRelatedProcedure[5] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="text" class="rect-input" style="width:92px;" name="pRVSCode[]" value="{{ isset($cf2Form->pRVSCode[5]) ? $cf2Form->pRVSCode[5] : NULL }}" />
              <spacer style="width:9px;"></spacer>
              <input type="date" class="rect-input" style="width:102px;" name="pProcedureDate[]" value="{{ isset($cf2Form->pProcedureDate[5]) ? $cf2Form->pProcedureDate[5] : NULL }}" />
              <spacer style="width:14px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[5]" value="L" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[5]) AND $cf2Form->pLaterality[5] == "L") checked @endif />
              <spacer style="width:26px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[5]" value="R" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[5]) AND $cf2Form->pLaterality[5] == "R") checked @endif />
              <spacer style="width:32px;"></spacer>
              <input type="radio" class="opt-input" name="pLaterality[5]" value="B" style="top: 8px; position: relative;" @if(isset($cf2Form->pLaterality[5]) AND $cf2Form->pLaterality[5] == "B") checked @endif />
            </div>
            
          </div>

          <div id="Considerations" class="formgroup">
            <input type="checkbox" class="opt-input" name="HEMODIALYSIS" value="Y" style="top: 8px; position: relative;" @if(isset($cf2Form->HEMODIALYSIS) AND $cf2Form->HEMODIALYSIS == 'Y') checked @endif /><spacer style="width:146px;"></spacer><input type="date" class="rect-input" name="HEMODIALYSIS_pSessionDate" style="width:236px;" value="{{ isset($cf2Form->HEMODIALYSIS_pSessionDate) ? $cf2Form->HEMODIALYSIS_pSessionDate : NULL }}" /><spacer style="width:28px;"></spacer><input type="checkbox" class="opt-input" name="BLOODTRANSFUSION" value="Y" style="top: 8px; position: relative;" @if(isset($cf2Form->BLOODTRANSFUSION) AND $cf2Form->BLOODTRANSFUSION == 'Y') checked @endif /><spacer style="width:140px;"></spacer><input type="date" class="rect-input" name="BLOODTRANSFUSION_pSessionDate" style="width:235px;"" value="{{ isset($cf2Form->BLOODTRANSFUSION_pSessionDate) ? $cf2Form->BLOODTRANSFUSION_pSessionDate : NULL }}" />
            <spacer class="vert" style="height: 0px;margin-top: -5px;"></spacer>
            <input type="checkbox" class="opt-input" name="PERITONEALDIALYSIS" value="Y" style="top: 8px; position: relative;" @if(isset($cf2Form->PERITONEALDIALYSIS) AND $cf2Form->PERITONEALDIALYSIS == 'Y') checked @endif /><spacer style="width:146px;"></spacer><input type="date" class="rect-input" name="PERITONEALDIALYSIS_pSessionDate" style="width:236px;" value="{{ isset($cf2Form->PERITONEALDIALYSIS_pSessionDate) ? $cf2Form->PERITONEALDIALYSIS_pSessionDate : NULL }}" /><spacer style="width:28px;"></spacer><input type="checkbox" class="opt-input" name="BRACHYTHERAPY" value="Y" style="top: 8px; position: relative;" @if(isset($cf2Form->BRACHYTHERAPY) AND $cf2Form->BRACHYTHERAPY == 'Y') checked @endif /><spacer style="width:140px;"></spacer><input type="date" class="rect-input" name="BRACHYTHERAPY_pSessionDate" style="width:235px;"" value="{{ isset($cf2Form->BRACHYTHERAPY_pSessionDate) ? $cf2Form->BRACHYTHERAPY_pSessionDate : NULL }}" />
            <spacer class="vert" style="height: 0px;margin-top: -5px;"></spacer>
            <input type="checkbox" class="opt-input" name="LINAC" value="Y" style="top: 8px; position: relative;" @if(isset($cf2Form->LINAC) AND $cf2Form->LINAC == 'Y') checked @endif /><spacer style="width:146px;"></spacer><input type="date" class="rect-input" name="LINAC_pSessionDate" style="width:236px;" value="{{ isset($cf2Form->LINAC_pSessionDate) ? $cf2Form->LINAC_pSessionDate : NULL }}" /><spacer style="width:28px;"></spacer><input type="checkbox" class="opt-input" name="CHEMOTHERAPY" value="Y" style="top: 8px; position: relative;" @if(isset($cf2Form->CHEMOTHERAPY) AND $cf2Form->CHEMOTHERAPY == 'Y') checked @endif /><spacer style="width:140px;"></spacer><input type="date" class="rect-input" name="CHEMOTHERAPY_pSessionDate" style="width:235px;"" value="{{ isset($cf2Form->CHEMOTHERAPY_pSessionDate) ? $cf2Form->CHEMOTHERAPY_pSessionDate : NULL }}" />
            <spacer class="vert" style="height: 0px;margin-top: -5px;"></spacer>
            <input type="checkbox" class="opt-input" name="COBALT" value="Y" style="top: 8px; position: relative;" @if(isset($cf2Form->COBALT) AND $cf2Form->COBALT == 'Y') checked @endif /><spacer style="width:146px;"></spacer><input type="date" class="rect-input" name="COBALT_pSessionDate" style="width:236px;" value="{{ isset($cf2Form->COBALT_pSessionDate) ? $cf2Form->COBALT_pSessionDate : NULL }}" /><spacer style="width:28px;"></spacer><input type="checkbox" class="opt-input" name="SIMPLEDEBRIDEMENT" value="Y" style="top: 8px; position: relative;" @if(isset($cf2Form->SIMPLEDEBRIDEMENT) AND $cf2Form->SIMPLEDEBRIDEMENT == 'Y') checked @endif /><spacer style="width:140px;"></spacer><input type="date" class="rect-input" name="SIMPLEDEBRIDEMENT_pSessionDate" style="width:235px;"" value="{{ isset($cf2Form->SIMPLEDEBRIDEMENT_pSessionDate) ? $cf2Form->SIMPLEDEBRIDEMENT_pSessionDate : NULL }}" />
          </div>

          <div id="ZBenefit" class="formgroup">
            <input type="text" class="rect-input" name="pZBenefitCode" style="width:252px;" value="{{ isset($cf2Form->pZBenefitCode) ? $cf2Form->pZBenefitCode : NULL }}" />
          </div>  

          <div id="MCP" class="formgroup">
            <input type="date" class="rect-input" name="pCheckUpDate1" value="{{ isset($cf2Form->pCheckUpDate1) ? $cf2Form->pCheckUpDate1 : NULL }}" style="width:203px;" /><spacer style="width:31px;"></spacer><input type="date" class="rect-input" name="pCheckUpDate2" value="{{ isset($cf2Form->pCheckUpDate2) ? $cf2Form->pCheckUpDate2 : NULL }}" style="width:185px;" /><spacer style="width:26px;"></spacer><input type="date" class="rect-input" name="pCheckUpDate3" value="{{ isset($cf2Form->pCheckUpDate3) ? $cf2Form->pCheckUpDate3 : NULL }}" style="width:166px;" /><spacer style="width:28px;"></spacer><input type="date" class="rect-input" name="pCheckUpDate4" value="{{ isset($cf2Form->pCheckUpDate4) ? $cf2Form->pCheckUpDate4 : NULL }}" style="width:202px;" />
          </div>

          <div id="TBDots" class="formgroup">
            <input type="radio" class="opt-input" name="pTBType" value="I" @if(isset($cf2Form->pTBType) AND $cf2Form->pTBType == 'I') checked @endif /><spacer style="width:127px;"></spacer><input type="radio" class="opt-input" name="pTBType" value="M" @if(isset($cf2Form->pTBType) AND $cf2Form->pTBType == 'M') checked @endif /><spacer style="width: 208px;text-align: right;padding-bottom: 10px;vertical-align: middle;">NTP Card No:</spacer><input type="text" class="rect-input" name="pNTPCardNo" value="{{ isset($cf2Form->pNTPCardNo) ? $cf2Form->pNTPCardNo : NULL }}" style="width: 156px;margin-top: -10px;" />
          </div>

          <div id="AnimalBite" class="formgroup">
            <input type="date" class="rect-input" name="pDay0ARV" value="{{ isset($cf2Form->pDay0ARV) ? $cf2Form->pDay0ARV : NULL }}" style="width:105px;" />
            <spacer style="width:60px;"></spacer>
            <input type="date" class="rect-input" name="pDay3ARV" value="{{ isset($cf2Form->pDay3ARV) ? $cf2Form->pDay3ARV : NULL }}" style="width:105px;" />
            <spacer style="width:75px;"></spacer>
            <input type="date" class="rect-input" name="pDay7ARV" value="{{ isset($cf2Form->pDay7ARV) ? $cf2Form->pDay7ARV : NULL }}" style="width:105px;" />
            <spacer style="width:18px;"></spacer>
            <input type="date" class="rect-input" name="pRIG" value="{{ isset($cf2Form->pRIG) ? $cf2Form->pRIG : NULL }}" style="width:105px;" />
            <spacer style="width:100px;"></spacer>
            <input type="date" class="rect-input" name="pABPOthers" value="{{ isset($cf2Form->pABPOthers) ? $cf2Form->pABPOthers : NULL }}" style="width:105px;" />
          </div>
          <div id="NewBornCare" class="formgroup">
            <input type="checkbox" class="opt-input" name="pEssentialNewbornCare" value="Y" @if(isset($cf2Form->pEssentialNewbornCare) AND $cf2Form->pEssentialNewbornCare == 'Y') checked @endif/><spacer style="width:126px;"/></spacer><input type="checkbox" class="opt-input" name="pNewbornHearingScreeningTest" value="Y" @if(isset($cf2Form->pNewbornHearingScreeningTest) AND $cf2Form->pNewbornHearingScreeningTest == 'Y') checked @endif /><spacer style="width:176px;"></spacer><input type="checkbox" class="opt-input" name="pNewbornScreeningTest" value="Y" @if(isset($cf2Form->pNewbornScreeningTest) AND $cf2Form->pNewbornScreeningTest == 'Y') checked @endif />
            <div id="FilterCardNo" class="no-print"><spacer style="width:150px;"></spacer>Filter Card No: <input type="text" class="rect-input" name="pFilterCardNo" value="{{ isset($cf2Form->pFilterCardNo) ? $cf2Form->pFilterCardNo : NULL }}" style="width: 156px;margin-top: -10px;" /></div>
          </div>

          <div id="NewBornEssential" class="formgroup">
            <input type="checkbox" class="opt-input" name="pDrying" value="Y" @if(isset($cf2Form->pDrying) AND $cf2Form->pDrying == 'Y') checked @endif /><spacer style="width:167px;"></spacer><input type="checkbox" class="opt-input" name="pCordClamping" value="Y" @if(isset($cf2Form->pCordClamping) AND $cf2Form->pCordClamping == 'Y') checked @endif /><spacer style="width:137px;"></spacer><input type="checkbox" class="opt-input" name="pWeighing" value="Y" @if(isset($cf2Form->pWeighing) AND $cf2Form->pWeighing == 'Y') checked @endif /><spacer style="width:153px;"></spacer><input type="checkbox" class="opt-input" name="pBCG" value="Y" @if(isset($cf2Form->pBCG) AND $cf2Form->pBCG == 'Y') checked @endif /><spacer style="width:126px;"></spacer><input type="checkbox" class="opt-input" name="pHepatitisB" value="Y" @if(isset($cf2Form->pHepatitisB) AND $cf2Form->pHepatitisB == 'Y') checked @endif />
            <br />
            <input type="checkbox" class="opt-input" name="pSkinToSkin" value="Y" style="top: -3px; position: relative;" @if(isset($cf2Form->pSkinToSkin) AND $cf2Form->pSkinToSkin == 'Y') checked @endif /><spacer style="width:167px;"></spacer><input type="checkbox" class="opt-input" name="pProphylaxis" value="Y" style="top: -3px; position: relative;" @if(isset($cf2Form->pProphylaxis) AND $cf2Form->pProphylaxis == 'Y') checked @endif /><spacer style="width:137px;"></spacer><input type="checkbox" class="opt-input" name="pVitaminK" value="Y" style="top: -3px; position: relative;" @if(isset($cf2Form->pVitaminK) AND $cf2Form->pVitaminK == 'Y') checked @endif /><spacer style="width:153px;"></spacer><input type="checkbox" class="opt-input" name="pNonSeparation" value="Y" style="top: -3px; position: relative;" @if(isset($cf2Form->pNonSeparation) AND $cf2Form->pNonSeparation == 'Y') checked @endif />
            <br />
            <input type="text" class="rect-input" name="pLaboratoryNumber" value="{{ isset($cf2Form->pLaboratoryNumber) ? $cf2Form->pLaboratoryNumber : NULL }}" style="top: -10px;position: relative;left: 391px;width: 220px;" />
          </div>

          <div id="ICD10" class="formgroup">
            <input type="text" class="rect-input" name="FirstCaseRateCode" value="{{ isset($cf2Form->FirstCaseRateCode) ? $cf2Form->FirstCaseRateCode : NULL }}" style="width:270px;" /><spacer style="width:127px;"></spacer><input type="text" class="rect-input" name="SecondCaseRateCode" value="{{ isset($cf2Form->SecondCaseRateCode) ? $cf2Form->SecondCaseRateCode : NULL }}" style="width:257px;" />
          </div>

        </div>
      </div>
    </div>
    
    <div class="container-fluid pad20 formcf2 formcf22">
      <div class="roww pad20"> 
        <div id="formcf22">
          
          <div id="ACCNoA" class="formgroup">
            <?php
              //pDoctorAccreCode
              $doc_accr_1 = NULL;
              if(isset($cf2Form->doc_accr_1) AND $cf2Form->doc_accr_1) {
                $doc_accr_1 = $cf2Form->doc_accr_1;
              }
            ?>
            <input type="text" class="box-rect" id="doc_accr_1" name="doc_accr_1" value="{{ $doc_accr_1 }}" style="width: 254px;margin-left: 68px;letter-spacing: 11px;" />@if(!isset($doc_accr_1))<spacer></spacer><button id="getPANButton_1" class="btn btn-danger btn-xs btn-arrow-left no-print getPANButton" type="button" onclick="getPAN(1);" > Check Accreditation</button>@endif
            <br />
            <div id="doc_1" style="left: 0px;margin-top: -8px;">
            <input type="text" class="rect-input" name="doctor_firstname_1" placeholder="First" style="width: 100px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_firstname_1) ? $cf2Form->doctor_firstname_1 : NULL }}" /><input type="text" class="rect-input" name="doctor_middlename_1" placeholder="Middle" style="width: 100px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_middlename_1) ? $cf2Form->doctor_middlename_1 : NULL }}" /><input type="text" class="rect-input" name="doctor_lastname_1" placeholder="Last" style="width: 100px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_lastname_1) ? $cf2Form->doctor_lastname_1 : NULL }}" /><input type="text" class="rect-input" name="doctor_suffix_1" placeholder="Suffix" style="width: 55px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_suffix_1) ? $cf2Form->doctor_suffix_1 : NULL }}" />
            <spacer class="vert" style="height:30px;"></spacer>
            <?php
              //pDoctorSignDate
              $signed_mon_1 = "";
              $signed_yy_1 = "";
              $signed_dd_1 = "";
              if(isset($cf2Form->signed_yy_1)) {
                $signed_yy_1 = $cf2Form->signed_yy_1;
              }
              if(isset($cf2Form->signed_mon_1)) {
                $signed_mon_1 = $cf2Form->signed_mon_1;
              }
              if(isset($cf2Form->signed_dd_1)) {
                $signed_dd_1 = $cf2Form->signed_dd_1;
              }
            ?>
            <spacer style="width:108px;"></spacer><input type="text" class="box-rect double" name="signed_mon_1" value="{{ $signed_mon_1 }}" style="width: 39px;margin-left: 0px;letter-spacing: 11px;" /><spacer></spacer><input type="text" class="box-rect double" name="signed_dd_1" value="{{ $signed_dd_1 }}" style="width: 37px;letter-spacing: 11px;" /><spacer></spacer><input type="text" class="box-rect quadruple" name="signed_yy_1" value="{{ $signed_yy_1 }}" style="width: 76px;letter-spacing: 11px;" />
            </div>
            
            <div id="coPay01">
            <input type="checkbox" class="opt-input" name="pWithCoPay_1" value="N" @if(isset($cf2Form->pWithCoPay_1) AND $cf2Form->pWithCoPay_1 == 'N') checked @endif />
            <br />
            <input type="checkbox" class="opt-input" name="pWithCoPay_1" value="Y" @if(isset($cf2Form->pWithCoPay_1) AND $cf2Form->pWithCoPay_1 == 'Y') checked @endif /><spacer></spacer><input type="text" class="rect-input" style="width: 135px;" name="pDoctorCoPay_1" value="{{ isset($cf2Form->pDoctorCoPay_1) ? $cf2Form->pDoctorCoPay_1 : NULL }}" />
            </div>

          </div>

          <div id="ACCNoB" class="formgroup">
            <?php
              $doc_accr_2 = NULL;
              if(isset($cf2Form->doc_accr_2) AND $cf2Form->doc_accr_2) {
                $doc_accr_2 = $cf2Form->doc_accr_2;
              }
            ?>
            <input type="text" class="box-rect" id="doc_accr_2" name="doc_accr_2" value="{{ $doc_accr_2 }}" style="width: 254px;margin-left:68px; letter-spacing: 11px;" />@if(!isset($doc_accr_2))<spacer></spacer><button id="getPANButton_2" class="btn btn-danger btn-xs btn-arrow-left no-print getPANButton" type="button" onclick="getPAN(2);">Check Accreditation</button>@endif
            <br />
            <div id="doc_2" style="left: 0px;margin-top:-15px;">
            <input type="text" class="rect-input" name="doctor_firstname_2" placeholder="First" style="width: 100px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_firstname_2) ? $cf2Form->doctor_firstname_2 : NULL }}" /><input type="text" class="rect-input" name="doctor_middlename_2" placeholder="Middle" style="width: 100px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_middlename_2) ? $cf2Form->doctor_middlename_2 : NULL }}" /><input type="text" class="rect-input" name="doctor_lastname_2" placeholder="Last" style="width: 100px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_lastname_2) ? $cf2Form->doctor_lastname_2 : NULL }}" /><input type="text" class="rect-input" name="doctor_suffix_2" placeholder="Suffix" style="width: 55px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_suffix_2) ? $cf2Form->doctor_suffix_2 : NULL }}" />
            <spacer class="vert" style="height:30px;"></spacer>
            <?php
              $signed_mon_2 = "";
              $signed_yy_2 = "";
              $signed_dd_2 = "";
              if(isset($cf2Form->signed_yy_2)) {
                $signed_yy_2 = $cf2Form->signed_yy_2;
              }
              if(isset($cf2Form->signed_mon_2)) {
                $signed_mon_2 = $cf2Form->signed_mon_2;
              }
              if(isset($cf2Form->signed_dd_2)) {
                $signed_dd_2 = $cf2Form->signed_dd_2;
              }
            ?>
            <spacer style="width:108px;"></spacer><input type="text" class="box-rect double" name="signed_mon_2" value="{{ $signed_mon_2 }}" style="width: 39px;margin-left: 0px;letter-spacing: 11px;" /><spacer></spacer><input type="text" class="box-rect double" name="signed_dd_2" value="{{ $signed_dd_2 }}" style="width: 37px;letter-spacing: 11px;" /><spacer></spacer><input type="text" class="box-rect quadruple" name="signed_yy_2" value="{{ $signed_yy_2 }}" style="width: 76px;letter-spacing: 11px;" />
            </div>
            
            <div id="coPay02">
            <input type="checkbox" class="opt-input" name="pWithCoPay_2" value="N" @if(isset($cf2Form->pWithCoPay_2) AND $cf2Form->pWithCoPay_2 == 'N') checked @endif />
            <br />
            <input type="checkbox" class="opt-input" name="pWithCoPay_2" value="Y" @if(isset($cf2Form->pWithCoPay_2) AND $cf2Form->pWithCoPay_2 == 'Y') checked @endif /><spacer></spacer><input type="text" class="rect-input" style="width: 135px;" name="pDoctorCoPay_2" value="{{ isset($cf2Form->pDoctorCoPay_2) ? $cf2Form->pDoctorCoPay_2 : NULL }}" />
            </div>

          </div>

          <div id="ACCNoC" class="formgroup">
            <?php
              $doc_accr_3 = NULL;
              if(isset($cf2Form->doc_accr_3) AND $cf2Form->doc_accr_3) {
                $doc_accr_3 = $cf2Form->doc_accr_3;
              }
            ?>
            <input type="text" class="box-rect" id="doc_accr_3" name="doc_accr_3" value="{{ $doc_accr_3 }}" style="width: 254px;margin-left:68px; letter-spacing: 11px;" />@if(!isset($doc_accr_3))<spacer></spacer><button id="getPANButton_3" class="btn btn-danger btn-xs btn-arrow-left no-print getPANButton" type="button" onclick="getPAN(3);">Check Accreditation</button>@endif
            <br />
            <div id="doc_3" style="left: 0px;margin-top:-15px;">
            <input type="text" class="rect-input" name="doctor_firstname_3" placeholder="First" style="width:100px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_firstname_3) ? $cf2Form->doctor_firstname_3 : NULL }}" />
            <input type="text" class="rect-input" name="doctor_middlename_3" placeholder="Middle" style="width:100px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_middlename_3) ? $cf2Form->doctor_middlename_3 : NULL }}" />
            <input type="text" class="rect-input" name="doctor_lastname_3" placeholder="Last" style="width:100px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_lastname_3) ? $cf2Form->doctor_lastname_3 : NULL }}" />
            <input type="text" class="rect-input" name="doctor_suffix_3" placeholder="Suffix" style="width:55px;top: 12px;position: relative;" value="{{ isset($cf2Form->doctor_suffix_3) ? $cf2Form->doctor_suffix_3 : NULL }}" />
            <spacer class="vert" style="height:25px;"></spacer>
            <?php
              $signed_mon_3 = "";
              $signed_yy_3 = "";
              $signed_dd_3 = "";
              if(isset($cf2Form->signed_yy_3)) {
                $signed_yy_3 = $cf2Form->signed_yy_3;
              }
              if(isset($cf2Form->signed_mon_3)) {
                $signed_mon_3 = $cf2Form->signed_mon_3;
              }
              if(isset($cf2Form->signed_dd_3)) {
                $signed_dd_3 = $cf2Form->signed_dd_3;
              }
            ?>
            <spacer style="width:108px;"></spacer><input type="text" class="box-rect double" name="signed_mon_3" value="{{ $signed_mon_3 }}" style="width: 39px;margin-left: 0px;letter-spacing: 11px;" /><spacer></spacer><input type="text" class="box-rect double" name="signed_dd_3" value="{{ $signed_dd_3 }}" style="width: 37px;letter-spacing: 11px;" /><spacer></spacer><input type="text" class="box-rect quadruple" name="signed_yy_3" value="{{ $signed_yy_3 }}" style="width: 76px;letter-spacing: 11px;" />
            </div>
            
            <div id="coPay03">
            <input type="checkbox" class="opt-input" name="pWithCoPay_3" value="N" @if(isset($cf2Form->pWithCoPay_3) AND $cf2Form->pWithCoPay_3 == 'N') checked @endif />
            <br />
            <input type="checkbox" class="opt-input" name="pWithCoPay_3" value="Y" @if(isset($cf2Form->pWithCoPay_3) AND $cf2Form->pWithCoPay_3 == 'Y') checked @endif /><spacer></spacer><input type="text" class="rect-input" style="width: 135px;" name="pDoctorCoPay_3" value="{{ isset($cf2Form->pDoctorCoPay_3) ? $cf2Form->pDoctorCoPay_3 : NULL }}" />
            </div>
          </div>

          <div id="CertificationA" class="formgroup">
            <input type="radio" class="opt-input" name="pEnoughBenefits" value="Y" @if(isset($cf2Form->pEnoughBenefits) AND $cf2Form->pEnoughBenefits == 'Y') checked @endif />
            <input type="text" class="rect-input" name="pTotalHCIFees" style="left: 424px;position: relative;top: 51px;width: 371px;" value="{{ isset($cf2Form->pTotalHCIFees) ? $cf2Form->pTotalHCIFees : NULL }}" /><br />
            <input type="text" class="rect-input" name="pTotalProfFees" style="left: 450px;position: relative;top: 52px;width: 371px;" value="{{ isset($cf2Form->pTotalProfFees) ? $cf2Form->pTotalProfFees : NULL }}" /><br />
            <input type="text" class="rect-input" name="pGrandTotal" style="left: 450px;position: relative;top: 54px;width: 371px;" value="{{ isset($cf2Form->pGrandTotal) ? $cf2Form->pGrandTotal : NULL }}" />
          </div>

          <div id="CertificationB" class="formgroup">
            <input type="radio" class="opt-input" name="pEnoughBenefits" value="N" @if(isset($cf2Form->pEnoughBenefits) AND $cf2Form->pEnoughBenefits == 'N') checked @endif />
            <div style="left:42px;top:97px;position: relative;">
              <textarea name="hci_pTotalActualCharges" style="left: 111px;position: relative;top:0;width: 129px;height: 74px;">{{ isset($cf2Form->hci_pTotalActualCharges) ? $cf2Form->hci_pTotalActualCharges : NULL}}</textarea><textarea name="hci_pDiscount" style="left: 112px;position: relative;top:0;width: 167px;height: 74px;">{{ isset($cf2Form->hci_pDiscount) ? $cf2Form->hci_pDiscount : NULL}}</textarea><textarea name="hci_pPhilhealthBenefit" style="left: 113px;position: relative;top: 0;width: 151px;height: 74px;">{{ isset($cf2Form->hci_pPhilhealthBenefit) ? $cf2Form->hci_pPhilhealthBenefit : NULL}}</textarea><input type="text" class="rect-input" name="hci_pTotalAmount" style="left: 180px;position: relative;top: -60px;width: 143px;height: 19px;" value="{{ isset($cf2Form->hci_pTotalAmount) ? $cf2Form->hci_pTotalAmount : NULL}}" /><input type="checkbox" class="opt-input" name="hci_pMemberPatient" value="Y" style="position: relative;top: -20px;left: -24px;" @if(isset($cf2Form->hci_pMemberPatient) AND $cf2Form->hci_pMemberPatient == 'Y') checked @endif /><input type="checkbox" class="opt-input" name="hci_pHMO" value="Y" style="position: relative;top: -20px;left: 70px;" @if(isset($cf2Form->hci_pHMO) AND $cf2Form->hci_pHMO == 'Y') checked @endif /><input type="checkbox" class="opt-input" name="hci_pOthers" value="Y" style="position: relative;top: -3px;left: -70px;" @if(isset($cf2Form->hci_pOthers) AND $cf2Form->hci_pOthers == 'Y') checked @endif />
            </div>
          </div>
          <div id="CertificationB2" class="formgroup">
            <textarea name="pf_pTotalActualCharges" style="width: 128px;height: 74px;float: left;">{{ isset($cf2Form->pf_pTotalActualCharges) ? $cf2Form->pf_pTotalActualCharges : NULL}}</textarea><textarea name="pf_pDiscount" style="width: 167px;height: 74px;float: left;margin-left: 1px;">{{ isset($cf2Form->pf_pDiscount) ? $cf2Form->pf_pDiscount : NULL}}</textarea><textarea name="pf_pPhilhealthBenefit" style="width: 151px;height: 74px;float: left;margin-left: 1px;">{{ isset($cf2Form->pf_pPhilhealthBenefit) ? $cf2Form->pf_pPhilhealthBenefit : NULL}}</textarea><input type="text" class="rect-input" name="pf_pTotalAmount" style="width: 141px;height: 19px;position: relative;margin-left: 69px;top: -4px;" value="{{ isset($cf2Form->pf_pTotalAmount) ? $cf2Form->pf_pTotalAmount : NULL }}"  /><input type="checkbox" class="opt-input" name="pf_pMemberPatient" value="Y" style="left:455px;position: absolute;top: 32px;" @if(isset($cf2Form->pf_pMemberPatient) AND $cf2Form->pf_pMemberPatient == 'Y') checked @endif /><input type="checkbox" class="opt-input" name="pf_pOthers" value="Y" style="position: absolute;top: 48px;left: 455px;" @if(isset($cf2Form->pf_pOthers) AND $cf2Form->pf_pOthers == 'Y') checked @endif /><input type="checkbox" class="opt-input" name="pf_pHMO" value="Y" style="position: absolute;top: 32px;left: 572px;" @if(isset($cf2Form->pf_pHMO) AND $cf2Form->pf_pHMO == 'Y') checked @endif />
          </div>

          <div id="TotalCost" class="formgroup">
            <input type="radio" class="opt-input" name="pDrugsMedicinesSupplies" value="N" @if(isset($cf2Form->pDrugsMedicinesSupplies) AND $cf2Form->pDrugsMedicinesSupplies == 'N') checked @endif /><spacer style="width:71px;"></spacer><input type="radio" class="opt-input" name="pDrugsMedicinesSupplies" value="Y" @if(isset($cf2Form->pDrugsMedicinesSupplies) AND $cf2Form->pDrugsMedicinesSupplies == 'Y') checked @endif /><spacer style="width: 43px;"></spacer><input type="text" class="rect-input" name="pDMSTotalAmount" style="position: relative;width:96px;top: -9px;left: 45px;" value="{{ isset($cf2Form->pDMSTotalAmount) ? $cf2Form->pDMSTotalAmount : NULL }}" />
            <spacer class="vert" style="height:15px;"></spacer>
            <input type="radio" class="opt-input" name="pExaminations" value="N" @if(isset($cf2Form->pExaminations) AND $cf2Form->pExaminations == 'N') checked @endif /><spacer style="width:71px;"></spacer><input type="radio" class="opt-input" name="pExaminations" value="Y" @if(isset($cf2Form->pExaminations) AND $cf2Form->pExaminations == 'Y') checked @endif /><spacer style="width: 43px;"></spacer><input type="text" class="rect-input" name="pExamTotalAmount" style="position: relative;width:96px;top: -9px;left: 45px;" value="{{ isset($cf2Form->pExamTotalAmount) ? $cf2Form->pExamTotalAmount : NULL }}" />
          </div>

          <div id="Consent" class="formgroup">
            <?php
              //APRBYPATREPSIG pDateSigned
              $apr_signed_yy = date("Y");
              $apr_signed_mon = date("m");
              $apr_signed_dd = date("d");
              if(isset($cf2Form->apr_signed_yy)) {
                $apr_signed_yy = $cf2Form->apr_signed_yy;
              }
              if(isset($cf2Form->apr_signed_mon)) {
                $apr_signed_mon = $cf2Form->apr_signed_mon;
              }
              if(isset($cf2Form->apr_signed_dd)) {
                $apr_signed_dd = $cf2Form->apr_signed_dd;
              }
            ?>
            <input type="text" class="rect-input" name="apr_name" style="width: 375px;text-align: center;" value="{{ isset($cf2Form->apr_name) ? $cf2Form->apr_name : NULL }}" />
            <spacer class="vert" style="height:24px;"></spacer>
            <spacer style="width:147px;"></spacer><input type="text" class="box-rect" name="apr_signed_mon" value="{{ $apr_signed_mon }}" /><spacer></spacer><input type="text" class="box-rect" name="apr_signed_dd" value="{{ $apr_signed_dd }}" /><spacer></spacer><input type="text" class="box-rect" name="apr_signed_yy" value="{{ $apr_signed_yy }}" />
            <spacer class="vert" style="height:27px;"></spacer>
            <spacer style="width:199px;"></spacer><input type="radio" class="opt-input" name="pRelCode" value="S" @if(isset($cf2Form->pRelCode) AND $cf2Form->pRelCode == 'S') checked @endif /><spacer style="width: 50px;"></spacer><input type="radio" class="opt-input" name="pRelCode" value="C" @if(isset($cf2Form->pRelCode) AND $cf2Form->pRelCode == 'C') checked @endif /><spacer style="width: 42px;"></spacer><input type="radio" class="opt-input" name="pRelCode" value="P" @if(isset($cf2Form->pRelCode) AND $cf2Form->pRelCode == 'P') checked @endif />
            <spacer class="vert" style="height:0px;margin-top:-7px;"></spacer>
            <spacer style="width:199px;"></spacer><input type="radio" class="opt-input" name="pRelCode" value="I" @if(isset($cf2Form->pRelCode) AND $cf2Form->pRelCode == 'I') checked @endif /><spacer style="width: 50px;"></spacer><input type="radio" class="opt-input" name="pRelCode" value="O" @if(isset($cf2Form->pRelCode) AND $cf2Form->pRelCode == 'O') checked @endif /><spacer style="width: 43px;"></spacer><input type="text" class="rect-input" name="pRelDesc" style="position: relative;width: 118px;top: -10px;left: 36px;" value="{{ isset($cf2Form->pRelDesc) ? $cf2Form->pRelDesc : NULL }}" />
            <spacer class="vert" style="height:0px;margin-top:-7px;"></spacer>
            <spacer style="width:199px;"></spacer><input type="radio" class="opt-input" name="pReasonCode" value="I" @if(isset($cf2Form->pReasonCode) AND $cf2Form->pReasonCode == 'I') checked @endif />
            <spacer class="vert" style="height:0px;margin-top:-8px;"></spacer>
            <spacer style="width:200px;"></spacer><input type="radio" class="opt-input" name="pReasonCode" value="O" @if(isset($cf2Form->pReasonCode) AND $cf2Form->pReasonCode == 'O') checked @endif /><spacer style="width: 43px;"></spacer><input type="text" class="rect-input" name="pReasonDesc" style="position: relative;width: 192px;top: -10px;left: 34px;" value="{{ isset($cf2Form->pReasonDesc) ? $cf2Form->pReasonDesc : NULL }}" />
            <div style="position: absolute;left: 509px;top: 133px;">
            <input type="radio" class="opt-input" name="pThumbmarkedBy" value="P" @if(isset($cf2Form->pThumbmarkedBy) AND $cf2Form->pThumbmarkedBy == 'P') checked @endif /><input type="radio" class="opt-input" name="thumb-by" value="R" @if(isset($cf2Form->pThumbmarkedBy) AND $cf2Form->pThumbmarkedBy == 'R') checked @endif style="margin-top: -7px;" />
            </div>
          </div>

          <div id="SOA" class="formgroup no-print">
            Has attached SOA: <input type="checkbox" class="opt-input" name="pHasAttachedSOA" value="Y" style="vertical-align: bottom;" @if(isset($cf2Form->pHasAttachedSOA) AND $cf2Form->pHasAttachedSOA == 'Y') checked @endif />
          </div>


        </div>
      </div>
    </div>
  </form>
</div>

<script>
  function getPAN(num) {
    // var button_string = "#getPANButton_"+num;

    if(num == 1){
      $('#getPANButton_1').html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Doctor PAN...");
      var dr_accre_code = $('#doc_accr_1').val();
    }
    else if(num == 2){
      $('#getPANButton_2').html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Doctor PAN...");
      $(button_string).html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Doctor PAN...");
      var dr_accre_code = $('#doc_accr_2').val();
    }
    else{
      $('#getPANButton_3').html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Doctor PAN...");
      $(button_string).html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Doctor PAN...");
      var dr_accre_code = $('#doc_accr_3').val();
    }

    var pdacode_str_0 = '#doc_accr_' + num;

    $.ajax({
        url: "{{ url('eclaims/checkAccr/') }}",
        data: {
          "_token": "{{ csrf_token() }}",
          "pDoctorAccreCode": dr_accre_code,
          "pAdmissionDate": $('#ad_dd').val()+'/'+$('#ad_mon').val()+'/'+$('#ad_yy').val(),
          "pDischargeDate": $('#dc_dd').val()+'/'+$('#dc_mon').val()+'/'+$('#dc_yy').val(),
        }, 
        type: "post",
        dataType: "json"
    })
    .done(function(data) {
      if(data && data.node.IsAccredited == "YES")
      {
        $('.getPANButton').html("Check Accreditation");
        swal("Success!", 'Doctor is accredited!', 'success');
      }
      if (data && data.ISOK == "error")
      {
        $('.getPANButton').html("Check Accreditation");
        swal("Warning!", data.MESSAGE, 'error');        
      }
    })
    .fail( function() {
      $('.getPANButton').html("Check Accreditation");
      swal("Oops!", 'Something went wrong. Please try again.', 'error');
    });
  }

  $(document).ready(function() {
    $("input.box-input").attr("maxlength", 1);
    $(".double").attr("maxlength", 2);
    $(".quadruple").attr("maxlength", 4)
  });
</script>
