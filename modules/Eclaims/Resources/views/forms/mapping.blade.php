
<div class="container-fluid pad20" style="background-color:#FFF;">
    <div class="roww pad20" id="printForm">
        <table width="100%">
            <tr>
                <td width="20%" align="center"><img src="{{ asset('public/dist/img/phic01-logo.png') }}" height="100" /></td>
                <td align="center">
                    <small>Republic of the Philippines</small>
                    <p class="lead no-margin-bottom">Philippine Health Insurance Corporation</p>
                    <small>Citystate Centre Building, 709 Shaw Boulevard, Pasig City<br />
                    Healthline: 441-7444 Website: http://www.philhealth.gov.ph</small>
                </td>
                <td width="20%" align="center"><img src="{{ asset('public/dist/img/phic02-logo.jpg') }}" height="100" /></td>
            </tr>
        </table>
        <div class="text-center">
            <small><em>Bawat Pilipino Miembro. Bawat Miembro Protektado. Kalusugan Natin Sigurado</em></small>
        </div>
        <div class="col-md-12 bg-yellow pad10">
            <h2 class="text-black text-center">HOSPITAL AND PHILHEALTH CLAIMS MAPPING</h2>
        </div>
    
        <table width=“100%“>
           <tr><td colspan=“2”>&nbsp;</td></tr>
           <tr><td colspan=“2"><h5>CONFIRMATION</h5></td></tr>
           <tr>
               <td width=“30%“>Receipt Ticket Number : </td>
               <td>{{ $dat['attributes']['pReceiptTicketNumber'] }}</td>
           </tr>
           <tr>
               <td width=“30%“>Hospital Code : </td>
               <td>{{ $dat['attributes']['pHospitalCode'] }}</td>
           </tr>
           <tr>
               <td width=“30%“>Hospital Transmittal Number : </td>
               <td>{{ $dat['attributes']['pHospitalTransmittalNo'] }}</td>
           </tr>
           <tr>
               <td width=“30%“>Total Claims : </td>
               <td>{{ $dat['attributes']['pTotalClaims'] }}</td>
           </tr>
           <tr>
               <td width=“30%“>Received Date : </td>
               <td>{{ $dat['attributes']['pReceivedDate'] }}</td>
           </tr>
           <tr><td class=“bordered” colspan=“2"></td></tr>
           <tr><td colspan=“2”><h5>CLAIMS MAPPING</h5></td></tr>
           @if(count($dat['MAPPING']) > 1) 
             @foreach($dat['MAPPING'] as $key => $map)
              <tr>
                <td colspan="2"><hr /></td>
              </tr>
               <tr>
                   <td width=“30%“>Claim Number : </td>
                   <td>{{ $map['attributes']['pClaimNumber'] }}</td>
               </tr>
               <tr>
                   <td width=“30%“>Patient Last Name : </td>
                   <td>{{ $map['attributes']['pPatientLastName'] }}</td>
               </tr>
               <tr>
                   <td width=“30%“>Patient First Name : </td>
                   <td>{{ $map['attributes']['pPatientFirstName'] }}</td>
               </tr>
               <tr>
                   <td width=“30%“>Patient Middle Name : </td>
                   <td>{{ $map['attributes']['pPatientMiddleName'] }}</td>
               </tr>
               <tr>
                   <td width=“30%“>Patient Suffix : </td>
                   <td>{{ $map['attributes']['pPatientSuffix'] }}</td>
               </tr>
               <tr><td class=“bordered” colspan=“2"></td></tr>
               <tr>
                   <td width=“30%“>Admission Date : </td>
                   <td>{{ $map['attributes']['pAdmissionDate'] }}</td>
               </tr>
               <tr>
                   <td width=“30%“>Discharge Date : </td>
                   <td>{{ $map['attributes']['pDischargeDate'] }}</td>
               </tr>
               <tr><td class=“bordered” colspan=“2"></td></tr>
               <tr>
                   <td width=“30%“>Claims LHIO Series : </td>
                   <td>{{ $map['attributes']['pClaimSeriesLhio'] }}</td>
               </tr>
              @endforeach
            @else
                <tr>
                  <td colspan="2"><hr /></td>
                </tr>
                 <tr>
                     <td width=“30%“>Claim Number : </td>
                     <td>{{ $dat['MAPPING']['attributes']['pClaimNumber'] }}</td>
                 </tr>
                 <tr>
                     <td width=“30%“>Patient Last Name : </td>
                     <td>{{ $dat['MAPPING']['attributes']['pPatientLastName'] }}</td>
                 </tr>
                 <tr>
                     <td width=“30%“>Patient First Name : </td>
                     <td>{{ $dat['MAPPING']['attributes']['pPatientFirstName'] }}</td>
                 </tr>
                 <tr>
                     <td width=“30%“>Patient Middle Name : </td>
                     <td>{{ $dat['MAPPING']['attributes']['pPatientMiddleName'] }}</td>
                 </tr>
                 <tr>
                     <td width=“30%“>Patient Suffix : </td>
                     <td>{{ $dat['MAPPING']['attributes']['pPatientSuffix'] }}</td>
                 </tr>
                 <tr><td class=“bordered” colspan=“2"></td></tr>
                 <tr>
                     <td width=“30%“>Admission Date : </td>
                     <td>{{ $dat['MAPPING']['attributes']['pAdmissionDate'] }}</td>
                 </tr>
                 <tr>
                     <td width=“30%“>Discharge Date : </td>
                     <td>{{ $dat['MAPPING']['attributes']['pDischargeDate'] }}</td>
                 </tr>
                 <tr><td class=“bordered” colspan=“2"></td></tr>
                 <tr>
                     <td width=“30%“>Claims LHIO Series : </td>
                     <td>{{ $dat['MAPPING']['attributes']['pClaimSeriesLhio'] }}</td>
                 </tr>              
            @endif
       </table>
        
        <table width="100%">
            <tr><td class="bordered" colspan="2"></td></tr>
        </table>
        
        <p clear="all">&nbsp;</p>
        <div class="roww">
            <div class="col-md-12 bg-yellow pad10 text-center">
                <p class="text-black text-center no-margin-bottom">Philippine Health Insurance Corporation</p>
                <small class="text-black">Citystate Centre Building, 709 Shaw Boulevard, Pasig City 
            Healthline: 441-7444</small>
            </div>
        </div>
    </div>
</div>

<script>
  $(document).ready(function() {
    $("#buttonGroup").remove();
  });
</script>