<div id="printFormCF1">
<style>
  .formcf1 {
    background: #FFF url('{!! asset("modules/Eclaims/Assets/images/cf1.png") !!}') center top no-repeat !important;
    background-size: contain !important;
    padding:0;
    margin:0 auto;
    height:1620px;
    width:961px;
    @if(isset($page))
    position:relative;
    @endif
  }
  @if(isset($page))
    #form_eclaims {
      margin-top: 12px;
      position: relative;
      margin-left: -36px;
    }
  @endif
  .formgroup {
    position: absolute;
  }
  .formcf1 input, .formcf1 select {
    background-color: rgba(200,200,200,.5);
    border: none;
    height: 22px;
    padding: 1px 2px 1px 5px;
    max-width: 900px;
  }
  #Pin { top: 306px;left: 422px; }
    #Pin #pin.box-rect { width: 257px;
      background: #EEE;
      text-align: center;
      letter-spacing: 11px; }
  #Birthdate { top: 351px;left: 734px; }
    #Birthdate input {
      background: #EEE;
      text-align: center;
      letter-spacing: 5px;}
    #Birthdate input[name=mon] { width: 41px; }
    #Birthdate input[name=dd] { width: 41px; }
    #Birthdate input[name=yy] { width: 79px; }
  #Name { top:347px; left:54px; }
    #Name input.name_field { margin-left: 17px; }
  #Gender { top: 413px;left: 775px; }
  #Address1 { top:437px; left:54px; }
  #Address2 { top:485px; left:54px; }
  #Contact { top: 556px;left: 52px; }
  #IsMember { top: 611px;left: 236px; }
  #Dependent-Pin { top: 671px;left: 440px; }
    #Dependent-Pin #deppin.box-rect { width: 257px;
      background: #EEE;
      text-align: center;
      letter-spacing: 11px; }
  #Dependent-Birthdate { top:720px; left:734px; }
    #Dependent-Birthdate input {
      background: #EEE;
      text-align: center;
      letter-spacing: 5px;}
    #Dependent-Birthdate input[name=dep_mon] { width: 41px; }
    #Dependent-Birthdate input[name=dep_dd] { width: 41px; }
    #Dependent-Birthdate input[name=dep_yy] { width: 79px; }
  #Dependent-Name { top:713px; left:39px; }
    #Dependent-Name input.rect-input {
      margin-left:17px;
    }
  #Dependent-Relationship { top:773px; left:246px; }
  #Dependent-Gender { top:774px; left:774px; }
  #ePEN { top: 1096px;left: 323px; }
    #ePEN #epen.box-rect { 
      width: 257px;
      background: #EEE;
      text-align: center;
      letter-spacing: 11px; }
  #eContact { top: 1088px;left: 733px; }
  #eBusi { top: 1141px;left: 124px; }
    #eBusi input { text-align: center; }
  #eAdd { top: 1482px;left: 63px; }
    #eAdd input { 
      border: 1px solid #000;
    }

  input {
    text-transform: uppercase !important;
  }
  input.box-input {
    width: 20px;
    text-align:center;
    vertical-align: middle;
    margin-left: 0.1em;
    height: 18px;
  }
  input.rect-input {
    width: 156px;
    vertical-align: middle;
    margin-left: 0.1em;
  }
  input.opt-input {
    width: 23px;
    margin: 2px 0 0;
  }
  select.select-input {
    margin-left: 10px;
    vertical-align: top;
  }
  input[type="date"], input[type="time"], input[type="datetime-local"], input[type="month"] {
    line-height: 14px;
  }
  spacer {
    width: 9px;
    display: inline-block;
  }
  spacer.lab {
    width: 33px;
    display: inline-block;
  }
  .w165 {
    width: 165px !important;
  }
  .w158 {
    width: 158px !important;
  }
  .w115 {
    width: 115px !important;
  }
  .w70 {
    width: 70px !important;
  }
  .w50 {
    width: 52px !important;
  }
  @media print {
    @page {
      size: 8.5in 13in;
      margin: 0.12in 0.3in;
    }
    @if(isset($page))
      #form_eclaims {
        margin-top: 20px;
        position: relative;
        margin-left: -23px;
      }
    @endif
    .formcf1 {
      width: 990px !important;
      position: unset;
      @if(isset($page))
      position:relative;
      @endif
    }
    #form_eclaims div.formgroup {
      margin-top: 5px;
    }
    input.box-input {
      margin-left: 0.1em;
    }
    spacer {
      width: 10px;
    }
    spacer.lab {
      width: 35px;
    }
    .no-print {
      display: none;
    }
    #Pin { top: 304px;left: 422px; }
      #Pin #pin.box-rect { width: 264px;
        background: #EEE;
        text-align: center;
        letter-spacing: 11px; }
    #Birthdate { top: 351px;left: 744px; }
      #Birthdate input {
        background: #EEE;
        text-align: center;
        letter-spacing: 5px;}
      #Birthdate input[name=mon] { width: 41px; }
      #Birthdate input[name=dd] { width: 41px; }
      #Birthdate input[name=yy] { width: 80px; }
    #Name { top:347px; left:54px; }
      #Name input.name_field { margin-left: 17px; }
    #Gender { top: 413px;left: 785px; }
    #Address1 { top:439px; left:54px; }
    #Address2 { top:487px; left:54px; }
    #Contact { top: 560px;left: 52px; }
    #IsMember { top: 618px;left: 230px; }
      #IsMember spacer.lab { width: 127px !important; }
    #Dependent-Pin { top: 681px;left: 440px; }
      #Dependent-Pin #deppin.box-rect { width: 264px;
        background: #EEE;
        text-align: center;
        letter-spacing: 11px; }
    #Dependent-Birthdate { top:730px; left:742px; }
      #Dependent-Birthdate input {
        background: #EEE;
        text-align: center;
        letter-spacing: 5px;}
      #Dependent-Birthdate input[name=dep_mon] { width: 42px; }
      #Dependent-Birthdate input[name=dep_dd] { width: 42px; }
      #Dependent-Birthdate input[name=dep_yy] { width: 81px; }
    #Dependent-Name { top:725px; left:39px; }
      #Dependent-Name input.rect-input {
        margin-left:17px;
      }
    #Dependent-Relationship { top:785px; left:239px; }
      #Dependent-Relationship spacer.lab1 { width: 39px !important; }
      #Dependent-Relationship spacer.lab2 { width: 49px !important; }
    #Dependent-Gender { top:786px; left:784px; }
    #ePEN { top: 1118px;left: 320px; }
      #ePEN #epen.box-rect { width: 264px;
        background: #EEE;
        text-align: center;
        letter-spacing: 11px; }
    #eContact { top: 1112px;left: 740px; }
    #eBusi { top: 1163px;left: 124px; }
        #eBusi input { text-align: center; }
    #eAdd { top: 1482px;left: 63px; }
  }
</style>

<div class="container-fluid pad20 formcf1">
    <div class="roww pad20"> 
      <?php 
        if(isset($eclaims_id) AND isset($claim) AND $claim->pbef_status == 'YES') {
          $action = 'saveform';
        } elseif(isset($eclaims_id) AND isset($cf1->patient_id) AND isset($cf1->heatlhcareservice_id)) {
          $action = 'checkEligibility/'.$cf1->patient_id.'/'.$cf1->heatlhcareservice_id.'/cf1';
        } else
        {
          $action = 'checkEligibility';          
        }
      ?>
      <form id="form_eclaims" method="post" action="{{ url('eclaims/'.$action) }}">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <input type="hidden" name="form" value="cf1">
      @if(isset($cf1->cf1))
        <input type="hidden" name="cf1" value="{{ $cf1->cf1 }}">
      @endif
      @if(isset($cf1Form->pHospitalCode))
        <input type="hidden" name="pHospitalCode" value="{{ $cf1Form->pHospitalCode }}">
      @endif
      @if(isset($eclaims_id))
      <input type="hidden" name="eclaims_id" value="{{ $eclaims_id }}">
      @endif
      <input type="hidden" name="patid" value="{{ $cf1->patient_id ?? NULL }}">
      <input type="hidden" name="careid" value="{{ $cf1->heatlhcareservice_id ?? NULL }}">
      <input type="hidden" name="pPatientType" value="{{ $cf1Form->pPatientType }}">
      
      <div id="Pin" class="formgroup">
        <?php
        $phic = NULL;
        if($cf1Form AND $cf1Form->pin != "") {
          $phic = $cf1Form->pin;
        } ?>
        <input type="text" class="box-rect" id="pin" name="pin" value="{{ $cf1Form->pin }}" />
        <spacer></spacer><button id="getPINButton" class="btn btn-danger btn-xs btn-arrow-left no-print" type="button" onclick="getPIN('{{ $cf1->patient_id }}');">Check Member PIN</button>
      </div>
      
      <div id="Birthdate" class="formgroup">
        <input type="text" class="box-rect" id="membirthmon" name="mon" value="{{ $cf1Form->mon }}" /><spacer></spacer><input type="text" class="box-rect" id="membirthdd" name="dd" value="{{ $cf1Form->dd }}" /><spacer></spacer><input type="text" class="box-rect" id="membirthyy" name="yy" value="{{ $cf1Form->yy }}" />
      </div>
      
      <div id="Name" class="formgroup">
        <input type="text" class="rect-input" name="pMemberLastName" value="{{ $cf1Form->pMemberLastName }}" /><input type="text" class="rect-input name_field" name="pMemberFirstName" value="{{ $cf1Form->pMemberFirstName }}" /><input type="text" class="rect-input name_field" name="pMemberSuffix" value="{{ $cf1Form->pMemberSuffix }}" /><input type="text" class="rect-input name_field" name="pMemberMiddleName" value="{{ $cf1Form->pMemberMiddleName }}" style="width: 128px;" />
      </div>

      <div id="Gender" class="formgroup">
        <input type="radio" class="opt-input" name="pMemberSex" value="M" @if(isset($cf1Form->pMemberSex) AND $cf1Form->pMemberSex == 'M') checked @endif /><spacer class="lab"></spacer><input type="radio" class="opt-input" name="pMemberSex" value="F" @if(isset($cf1Form->pMemberSex) AND $cf1Form->pMemberSex == 'F') checked @endif />
      </div>

      <div id="Address1" class="formgroup">
        <input type="text" class="rect-input" name="unit" value="{{ $cf1Form->unit }}" style="width:157px;margin-right:16px;" /><input type="text" class="rect-input" name="building" value="{{ $cf1Form->building }}" style="width:157px;margin-right: 14px;" /><input type="text" class="rect-input" name="housenumber" value="{{ $cf1Form->housenumber }}" style="width:156px;margin-right: 16px;" /><input type="text" class="rect-input" name="street" value="{{ $cf1Form->street }}" style="width:129px;margin-right: 18px;" /><input type="text" class="rect-input" name="village" value="{{ $cf1Form->village }}" style="width:216px;" />
      </div>

      <div id="Address2" class="formgroup">
        <input type="text" class="rect-input" name="barangay" value="{{ $cf1Form->barangay }}" style="width:157px;margin-right:16px;" /><input type="text" class="rect-input" name="city" value="{{ $cf1Form->city }}" style="width:157px;margin-right:14px;" /><input type="text" class="rect-input"  style="width:156px;margin-right:16px;" name="province" value="{{ $cf1Form->province }}" /><input type="text" class="rect-input"  style="width:129px;margin-right:18px;" name="country" value="{{ $cf1Form->country }}" /><input type="text" class="rect-input" style="width:216px;" name="pZipCode" value="{{ $cf1Form->pZipCode }}" />
      </div>

      <div id="Contact" class="formgroup">
        <input type="text" class="rect-input" name="pLandlineNo" value="{{ $cf1Form->pLandlineNo }}" style="width:296px;" /><spacer class="lab" style="width:15px;"></spacer><input type="text" class="rect-input" name="pMobileNo" value="{{ $cf1Form->pMobileNo }}" style="width:253px;" /><spacer class="lab" style="width:17px;"></spacer><input type="text" class="rect-input" name="pEmailAddress" value="{{ $cf1Form->pEmailAddress }}" style="width:301px;" />
      </div>

      <div id="IsMember" class="formgroup">
        <input type="radio" class="opt-input" name="pPatientIs" value="MM" @if( isset($cf1Form) AND isset($cf1Form->pPatientIs) AND $cf1Form->pPatientIs == 'MM' ) checked @endif /><spacer class="lab" style="width:123px;"></spacer><input type="radio" class="opt-input" name="pPatientIs" value="DD" @if(isset($cf1Form) AND isset($cf1Form->pPatientIs) AND $cf1Form->pPatientIs == 'DD') checked @endif  />
        <label style="margin-left: 136px;vertical-align: bottom;">MemberShip Type</label>
        <select name="pMemberShipType" class="select-input">
          <option @if(!$cf1Form OR (isset($cf1Form->pMemberShipType) AND $cf1Form->pMemberShipType == '')) selected @endif disabled>Choose Membership Type</option>
          <option value="S" @if(isset($cf1Form->pMemberShipType) AND $cf1Form->pMemberShipType == 'S') selected @endif>Employed Private</option>
          <option value="G" @if(isset($cf1Form->pMemberShipType) AND $cf1Form->pMemberShipType == 'G') selected @endif>Employer Government</option>
          <option value="I" @if(isset($cf1Form->pMemberShipType) AND $cf1Form->pMemberShipType == 'I') selected @endif>Indigent</option>
          <option value="NS" @if(isset($cf1Form->pMemberShipType) AND $cf1Form->pMemberShipType == 'NS') selected @endif>Individually Paying</option>
          <option value="NO" @if(isset($cf1Form->pMemberShipType) AND $cf1Form->pMemberShipType == 'NO') selected @endif>OFW</option>
          <option value="PS" @if(isset($cf1Form->pMemberShipType) AND $cf1Form->pMemberShipType == 'PS') selected @endif>Non Paying Private</option>
          <option value="PG" @if(isset($cf1Form->pMemberShipType) AND $cf1Form->pMemberShipType == 'PG') selected @endif>Non Paying Government</option>
          <option value="P" @if(isset($cf1Form->pMemberShipType) AND $cf1Form->pMemberShipType == 'P') selected @endif>Lifetime member</option>

        </select>
      </div>

      <div id="Dependent-Pin" class="formgroup">
        <input type="text" class="box-rect" id="deppin" name="deppin" value="{{ $cf1Form->deppin }}" /><spacer></spacer><button class="btn btn-danger btn-xs btn-arrow-left no-print" id="getDepPINButton" type="button" onclick="getDepPIN('{{ $cf1->patient_id }}');">Check Dependent PIN</button>
      </div>

      <div id="Dependent-Birthdate" class="formgroup">
        <input type="text" class="box-rect" id="patbirth_mon" name="dep_mon" value="{{ $cf1Form->dep_mon }}" /><spacer></spacer><input type="text" class="box-rect" id="patbirth_dd" name="dep_dd" value="{{ $cf1Form->dep_dd }}" /><spacer></spacer><input type="text" class="box-rect" id="patbirth_yy" name="dep_yy" value="{{ $cf1Form->dep_yy }}" />
      </div>

      <div id="Dependent-Name" class="formgroup">
        <input type="text" class="rect-input" name="pPatientLastName" value="{{ $cf1Form->pPatientLastName }}" /><input type="text" class="rect-input" name="pPatientFirstName" value="{{ $cf1Form->pPatientFirstName }}" /><input type="text" class="rect-input" name="pPatientSuffix" value="{{ $cf1Form->pPatientSuffix }}" /><input type="text" class="rect-input" name="pPatientMiddleName" value="{{ $cf1Form->pPatientMiddleName }}" style="width: 129px;" />
      </div>

      <div id="Dependent-Relationship" class="formgroup">
        <input type="radio" class="opt-input" value="C" @if(isset($cf1Form->dep_relationship) AND $cf1Form->dep_relationship == 'C') checked @endif name="dep_relationship" /><spacer class="lab lab1" style="width:37px;"></spacer><input type="radio" class="opt-input" value="P" @if(isset($cf1Form->dep_relationship) AND $cf1Form->dep_relationship == 'P') checked @endif name="dep_relationship" /><spacer class="lab lab2" style="width:46px;"></spacer><input type="radio" class="opt-input" value="S" @if(isset($cf1Form->dep_relationship) AND $cf1Form->dep_relationship == 'S') checked @endif name="dep_relationship" />
      </div>

      <div id="Dependent-Gender" class="formgroup">
        <input type="radio" class="opt-input" name="pPatientSex" value="M" @if(isset($cf1Form->pPatientSex) AND $cf1Form->pPatientSex == 'M') checked @endif /><spacer class="lab"></spacer><input type="radio" class="opt-input" name="pPatientSex" value="F" @if(isset($cf1Form->pPatientSex) AND $cf1Form->pPatientSex == 'F') checked @endif />
      </div>

      <div id="ePEN" class="formgroup">
        <?php
          $epen = NULL;
          if(isset($cf1Form->epen) AND $cf1Form->epen != "") {
            $epen = $cf1Form->epen;
          } 
        ?>
        <input type="text" class="box-rect" id="epen" name="epen" value="{{ $epen }}" />
        <br />
        <spacer class="vert" style="height:10px;width:274px;"></spacer><button class="btn btn-danger btn-xs btn-arrow-left no-print" type="button" id="getPENButton" onclick="getPEN();">Get Employer PEN</button>
      </div>

      <div id="eContact" class="formgroup">
        <input type="text" class="rect-input" name="pEmployerContact" value="{{ isset($cf1Form->pEmployerContact) ? $cf1Form->pEmployerContact : NULL }}" style="width: 206px;height: 17px;" />
      </div>

      <div id="eBusi" class="formgroup">
        <input type="text" class="rect-input" name="pEmployerName" value="{{ isset($cf1Form->pEmployerName) ? $cf1Form->pEmployerName : NULL }}" style="width:742px;" />
      </div>

      <div id="eAdd" class="formgroup no-print">
        Admission Date: <input type="date" class="rect-input" name="pAdmissionDate" value="{{ isset($cf1Form->pAdmissionDate) ? $cf1Form->pAdmissionDate : NULL }}" style="width:150px;" /><spacer></spacer>Discharge Date: <input type="date" class="rect-input" name="pDischargeDate" value="{{ isset($cf1Form->pDischargeDate) ? $cf1Form->pDischargeDate : NULL }}" style="width:150px;" /><br><br>RVS Code: <input type="text" class="rect-input" name="pRVS" value="{{ isset($cf1Form->pRVS) ? $cf1Form->pRVS : NULL }}" style="width:100px;" /><spacer></spacer>Total Amount Actual: <input type="text" class="rect-input" name="pTotalAmountActual" value="{{ isset($cf1Form->pTotalAmountActual) ? $cf1Form->pTotalAmountActual : NULL }}" style="width:100px;" /><spacer></spacer>Total Amount Claimed: <input type="text" class="rect-input" name="pTotalAmountClaimed" value="{{ isset($cf1Form->pTotalAmountClaimed) ? $cf1Form->pTotalAmountClaimed : NULL }}" style="width:100px;" /><spacer class="lab"></spacer>Call<input type="radio" class="opt-input" name="pIsFinal" value="0" @if(isset($cf1Form->pIsFinal) AND $cf1Form->pIsFinal == '0') checked @endif /> Initial <spacer></spacer><input type="radio" class="opt-input" name="pIsFinal" value="1" @if(isset($cf1Form->pIsFinal) AND $cf1Form->pIsFinal == '1') checked @endif /> Final
      </div>

      </form>
    </div>
</div>
</div>
<script>
  function getPIN(id) {
    $("#getPINButton").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Member PIN&hellip;");
    
    $.ajax({
        url: "{{ url('eclaims/getPIN/'.$cf1->patient_id) }}",
        data: {
          "_token": "{{ csrf_token() }}",
          "pMemberFirstName": $('input[name=pMemberFirstName]').val(),
          "pMemberLastName": $('input[name=pMemberLastName]').val(),
          "pMemberMiddleName": $('input[name=pMemberMiddleName]').val(),
          "pMemberSuffix": $('input[name=pMemberSuffix]').val(),
          "pMemberBirthDate": $('#membirthyy').val()+"-"+$('#membirthmon').val()+"-"+$('#membirthdd').val()
        }, 
        type: "post",
        dataType: "json"
    })
    .done(function(data) {
      console.log(data);
        if(data && data.length == 12) {
          swal("Member found!", data, 'success');
          $("#getPINButton").hide();
          var pin = data.replace(/-/g,"");
          /*var expin = pin.split("");
          expin.forEach(function(item, index) {
            $('#pin'+index).val(item);
          })*/
          $('#pin').val(pin);
        } 
        else if(data && data == "NO RECORDS FOUND. PLEASE PROCEED TO ANY PHILHEALTH OFFICE FOR PIN REGISTRATION.") {
          $("#getPINButton").html("Check Member PIN");
          swal("Warning!", data, 'warning');          
        }
        else {
          $("#getPINButton").html("Check Member PIN");
          swal("Warning!", data.error, 'warning');
        }
    })
    .fail( function() {
      $("#getPINButton").html("Check Member PIN");
      swal("Oops!", 'Something went wrong. Cannot retrieve PIN Information. Please try again.',"error");
    });
  }
  
  function getDepPIN(id) {
    $("#getDepPINButton").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Dependent PIN...");
    
    $.ajax({
        url: "{{ url('eclaims/getDepPIN') }}",
        data: {
          "_token": "{{ csrf_token() }}",
          "first_name": $('input[name=pPatientFirstName]').val(),
          "last_name": $('input[name=pPatientLastName]').val(),
          "middle_name": $('input[name=pPatientMiddleName]').val(),
          "suffix": $('input[name=pPatientSuffix]').val(),
          "birthdate": $('#patbirth_yy').val()+"-"+$('#patbirth_mon').val()+"-"+$('#patbirth_dd').val()
        }, 
        type: "post",
        dataType: "json"
    })
    .done(function(data) {
      console.log(data);
        if(data && data.ISOK == "OK") {
          $("#getDepPINButton").hide();
          var pin = data.PHILHEALTHNUMBER.replace(/-/g,"");
          /*var expin = pin.split("");
          expin.forEach(function(item, index) {
            $('#deppin'+index).val(item);
          })*/
          $('#deppin').val(pin);
        } else {
          $("#getDepPINButton").html("Check Dependent PIN");
          swal("Warning!", data, 'warning');
        }
    })
    .fail( function() {
      $("#getDepPINButton").html("Check Dependent PIN");
      swal("Oops!", 'Something went wrong. Cannot retrieve PIN Information. Please try again.', "error");
    });
  }

  function getPEN() {
    $("#getPENButton").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching PEN...");
    
    $.ajax({
        url: "{{ url('eclaims/getEmployer') }}",
        data: {
          "_token": "{{ csrf_token() }}",
          "pPEN": $('#epen0').val()+$('#epen1').val()+$('#epen2').val()+$('#epen3').val()+$('#epen4').val()+$('#epen5').val()+$('#epen6').val()+$('#epen7').val()+$('#epen8').val()+$('#epen9').val()+$('#epen10').val()+$('#epen11').val(),
          "pEmployerName": $('input[name=pEmployerName]').val()
        }, 
        type: "post",
        dataType: "json"
    })
    .done(function(data) {
        console.log(data);
        if(data) {
          if(data.employer) {
            // var pin = data['@attributes']['employer']['@attributes']['pPEN'].replace(/-/g,"");
            // var expin = pin.split("");
            // expin.forEach(function(item, index) {
            //   $('#epen'+index).val(item);
            // })
            // $('input[name=pEmployerContact').val(data['@attributes']['employer']['@attributes']['pEmployerAddress']);
            // $('input[name=pEmployerName').val(data.node.employer.pEmployerName);
            swal("Success!", "Employer found!", 'success');
            $("#getPENButton").hide();
          } else {
            $("#getPENButton").html("Get Employer PEN");
            swal("Warning!", "Employer info not found.", 'warning');
          }
        } else {
          $("#getPENButton").html("Get Employer PEN");
          swal("Warning!", "Employer not available.", 'warning');
        }
        
        if(data && data['error'] == 'validation_error') {
          $("#getPENButton").html("Get Employer PEN");
          swal("Oops!", data['message'][0], "error");
        }
    })
    .fail( function() {
      $("#getPENButton").html("Get Employer PEN");
      swal("Oops!", 'Something went wrong. Cannot retrieve PEN Information. Please try again.', "error");
    });
  }
  
  $(document).ready(function() {
    $("input.box-input").attr("maxlength", 1)

    /*$('#form_eclaims').on('submit', function(){
      var doc = new jsPDF();
      doc.fromHTML($('#form_eclaims').html(), 15, 15);
      doc.save('sample-file.pdf');
    })*/
  });
</script>
