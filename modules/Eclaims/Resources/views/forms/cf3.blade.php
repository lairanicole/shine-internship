<div id="printFormCF3">
<style>
  .formcf3 {
    padding:0;
    height:1581px;
    width:961px;
    position:relative;
    margin:0 auto;
  }
  .formcf31 {
    background: #FFF url('{!! asset("modules/Eclaims/Assets/images/cf31.png") !!}') center top no-repeat !important;
    background-size: contain !important;
    margin-bottom: 20px;
  }
  .formcf32 {
    background: #FFF url('{!! asset("modules/Eclaims/Assets/images/cf32.png") !!}') center top no-repeat !important;
    background-size: contain !important;
  }
  #formcf31, #formcf32 {
    margin-top: -38px;
    position: relative;
    margin-left: -38px;
  }
  .formgroup {
    position: absolute;
  }
  .formcf3 input, .formcf3 textarea {
    background-color: rgba(200,200,200,.2);
    border: none;
    height: 23px;
    padding: 1px 5px;
    max-width: 900px;
  }
  
  #Pan {top: 267px;left: 464px;}
  #Name {top: 339px;left: 55px;}
    #Name input {
      width: 179px;
    }
    #Name input[name=suffix] {
      width: 35px;
    }
  #Admitted {top: 394px;left: 181px;}
  #Discharged {top: 444px;left: 181px;}
  #Complaint {top: 339px;left: 695px;}
  #History {top: 533px;left: 47px;}
  #Vitals {top: 796px;left: 197px;}
  #HEENT {top: 843px;left: 166px;}
  #Chest {top: 889px;left: 166px;}
  #CVS {top: 933px;left: 166px;}
  #CourseWards {top: 1063px;left: 46px;}
  #Findings {top: 1309px;left: 46px;}
  #Disposition {top: 1325px;left: 242px;}

  #Initial {top: 134px;left: 323px;}
  #PE-Vitals {top: 188px;left: 360px;}
  #PE-Risks {top: 224px;left: 360px;}
  #OBRiskFactors {top: 271px;left: 218px;}
  #SurgicalRiskFactors {top: 361px;left: 218px;}
  #AdmittingDiagnosis {top: 448px;left: 218px;}
  #DeliveryPlan {top: 502px;left: 369px;}
  #ConsultationNo {top: 573px;left: 249px;}
  #DataOfVisit {top: 603px;left: 246px;}
  #AOG {top: 628px;left: 254px;}
  #Delivery {top: 821px;left: 322px;}
  #MaternalOutcome {top: 874px;left: 218px;}
  #PostPartumCare {top: 1098px;left: 544px;}
  #Certification {top: 1376px;left: 580px;}

  input {
    text-transform: uppercase !important;
  }
  input.box-input {
    width: 25px;
    text-align:center;
    vertical-align: middle;
    margin-right: 0.12em;
    padding: 1px 0px;
  }

  input.cf3-pan {
    width: 238px;
    text-align: center;
    vertical-align: middle;
    padding: 1px 0px 0 9px;
    letter-spacing: 19px;
  }
  
  input.box-input[name*=hh_am], input.box-input[name*=hh_pm] {
    font-size: 11px;
  }

  #Certification input.box-input {
    letter-spacing: 4px;
  }

  #Admitted input.box-input[name^=ad_],
  #Discharged input.box-input[name^=dc_] {
    width: 34px;
    /*letter-spacing: 6px;*/
    padding-left: 0px;
  }
  #Admitted input.box-input[name*=_yy],
  #Discharged input.box-input[name*=_yy] {
    width: 69px;
    letter-spacing: 8px;
    padding-left: 3px;
  }
  #Admitted input.box-input.double,
  #Discharged input.box-input.double {
    width: 34px;
    letter-spacing: 0;
    padding-left: inherit;
  }

  input.box-input.double {
    width: 25px;
  }

  input.box-input.quadruple {
    width: 50px;
  }

  #Initial input.box-input {
    width: 26px;
    padding:1px 0;
  }

  #Delivery input.box-input,
  #MaternalOutcome input.box-input,
  #Certification input.box-input {
    width: 27px;
    margin-right: 0.13em;
  }
  #Initial input.box-input[name*=_yy],
  #PE-Vitals input.box-input[name*=_yy],
  #Delivery input.box-input[name*=_yy],
  #DeliveryPlan input.box-input[name*=_yy],
  #MaternalOutcome input.box-input[name*=_yy],
  #Certification input.box-input[name*=_yy] {
    width: 52px;
    letter-spacing: 4px;
    padding-left: 3px;
  }
  input.rect-input {
    width: 143px;
    vertical-align: middle;
    margin-left: 0.1em;
  }
  input.rect-md-input {
    width: 55px;
  }
  #DataOfVisit input.rect-input {
    width: 19.25px;
  }
  #DataOfVisit input.rect-input.double {
    font-size:12px;
    padding: 0px;
    text-align: center;
  }
  #AOG input.rect-input {
    width: 45px;
    margin: 0px 12px 0px 1px;
    height: 15px;
  }
  #PostPartumCare input.rect-input {
    height: 21px;
  }
  input.opt-input {
    width: 23px;
    margin: 2px 0 0;
  }
  input[type='date'] {
    font-size: 10px;
    padding: 1px 0px 1px 4px;
  }
  spacer {
    width: 12px;
    display: inline-block;
  }
  spacer.lab {
    width: 41px;
    display: inline-block;
  }
  spacer.vert {
    height:7px;
    display: block;
  }
  #Vitals spacer {
    width: 25px;
  }
  .w165 {
    width: 165px !important;
  }
  .w158 {
    width: 158px !important;
  }
  .w115 {
    width: 115px !important;
  }
  .w100 {
    width: 100px !important;
  }
  .w85 {
    width: 85px !important;
  }
  .w70 {
    width: 70px !important;
  }
  .w50 {
    width: 52px !important;
  }
  .w13 {
    width: 13px !important;
  }
  .w12 {
    width: 12px !important;
  }
  .alert-absolute {
    position: absolute;
    width: 93%;
  }

  @media print {
    @page {
      size: 8.5in 13in;
      margin: 0in;
    }
    @if(isset($page))
      #formcf31, #formcf32 {
        margin-top: -38px;
        position: relative;
        margin-left: -25px;
      }
    @endif
    .formcf3 {
      width: 990px !important;
    } 
    
    input.box-input {
      margin-left: 0.1em;
    }
    spacer {
      width: 15px;
    }
  }
</style>
  <form id="form_eclaims" method="post" action="{{ url('eclaims/saveform') }}">
    <div class="container-fluid pad20 formcf3 formcf31">
      <div class="roww pad20"> 
        <div id="formcf31">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="form" value="cf3">
          <input type="hidden" name="eclaims_id" value="{{ $eclaims_id }}">
          @if(isset($cf3->cf3))
            <input type="hidden" name="cf3" value="{{ $cf3->cf3 }}">
          @endif
          <div id="Pan" class="formgroup">
            <input type="text" class="box-input cf3-pan" id="pan0" name="pan" value="{{ $cf3Form->pan or NULL }}" />
          </div>

          <div id="Name" class="formgroup">
            <input type="text" class="rect-input" name="lastname" value="{{ $cf3Form->lastname }}" /><input type="text" class="rect-input" name="firstname" value="{{ $cf3Form->firstname }}" /><input type="text" class="rect-input" name="suffix" value="{{ $cf3Form->suffix }}" /><input type="text" class="rect-input" name="middlename" value="{{ $cf3Form->middlename }}" />
          </div>

          <div id="Admitted" class="formgroup">
            <?php
              //pAdmissionDate
              $ad_mon = NULL;
              $ad_dd = NULL;
              $ad_yy = NULL;
              $ad_hh_am = NULL;
              $ad_hh_pm = NULL;
              
              if(isset($cf3Form->ad_mon)) {
                $ad_mon = $cf3Form->ad_mon;
              }
              if(isset($cf3Form->ad_dd)) {
                $ad_dd = $cf3Form->ad_dd;
              }
              if(isset($cf3Form->ad_yy)) {
                $ad_yy = $cf3Form->ad_yy;
              }

              if(isset($cf3Form->ad_hh_am)) {
                $ad_hh_am = $cf3Form->ad_hh_am;
              }
              if(isset($cf3Form->ad_hh_pm)) {
                $ad_hh_pm = $cf3Form->ad_hh_pm;
              }
              
            ?>
            <input type="text" class="box-input double" name="ad_mon" value="{{ $ad_mon }}" /><spacer style="width:27px;"></spacer><input type="text" class="box-input double" name="ad_dd" value="{{ $ad_dd }}" /><spacer style="width:27px;"></spacer><input type="text" class="box-input quadruple" name="ad_yy" value="{{ $ad_yy }}" /><spacer style="width: 132px;"></spacer><input type="text" class="box-input quintuple" name="ad_hh_am" value="{{ $ad_hh_am }}" /><spacer style="width:45px;"></spacer><input type="text" class="box-input quintuple" name="ad_hh_pm" value="{{ $ad_hh_pm }}" />
          </div>

          <div id="Discharged" class="formgroup">
            <?php
              //pDischargeDate
              $dc_mon = NULL;
              $dc_dd = NULL;
              $dc_yy = NULL;
              $dc_hh_am = NULL;
              $dc_min_am = NULL;
              $dc_hh_pm = NULL;
              $dc_min_pm = NULL;
              if(isset($cf3Form->dc_mon)) {
                $dc_mon = $cf3Form->dc_mon;
              }
              if(isset($cf3Form->dc_dd)) {
                $dc_dd = $cf3Form->dc_dd;
              }
              if(isset($cf3Form->dc_yy)) {
                $dc_yy = $cf3Form->dc_yy;
              }

              if(isset($cf3Form->dc_hh_am)) {
                $dc_hh_am = $cf3Form->dc_hh_am;
              }
              if(isset($cf3Form->dc_hh_pm)) {
                $dc_hh_pm = $cf3Form->dc_hh_pm;
              }
            ?>
            <input type="text" class="box-input double" name="dc_mon" value="{{ $dc_mon }}" /><spacer style="width:27px;"></spacer><input type="text" class="box-input double" name="dc_dd" value="{{ $dc_dd }}" /><spacer style="width:27px;"></spacer><input type="text" class="box-input quadruple" name="dc_yy" value="{{ $dc_yy }}" /><spacer style="width:132px;"></spacer><input type="text" class="box-input quintuple" name="dc_hh_am" value="{{ $dc_hh_am }}" /><spacer style="width:45px;"></spacer><input type="text" class="box-input quintuple" name="dc_hh_pm" value="{{ $dc_hh_pm }}" />
          </div>

          <div id="Complaint" class="formgroup">
            <textarea name="pChiefComplaint" style="position: relative;width: 244px;height: 145px;">{{ isset($cf3Form->pChiefComplaint) ? $cf3Form->pChiefComplaint : NULL }}</textarea>
          </div>

          <div id="History" class="formgroup">
            <textarea name="pBriefHistory" style="position: relative;width:894px;height: 194px;">{{ isset($cf3Form->pBriefHistory) ? $cf3Form->pBriefHistory : NULL }}</textarea>
          </div>

          <div id="Vitals" class="formgroup">
            <input type="text" class="rect-md-input" name="pBP" value="{{ isset($cf3Form->pBP) ? $cf3Form->pBP : NULL }}" /><spacer></spacer><input type="text" style="width:63px;" class="rect-md-input" name="pCR" value="{{ isset($cf3Form->pCR) ? $cf3Form->pCR : NULL }}" /><spacer></spacer><input type="text" class="rect-md-input" name="pRR" value="{{ isset($cf3Form->pRR) ? $cf3Form->pRR : NULL }}" /><spacer style="width:86px;"></spacer><input type="text" style="width:64px;" class="rect-input" name="pTemp" value="{{ isset($cf3Form->pTemp) ? $cf3Form->pTemp : NULL }}" /><spacer style="width:169px;"></spacer><input type="text" style="width:198px;" class="rect-input" name="pAbdomen" value="{{ isset($cf3Form->pAbdomen) ? $cf3Form->pAbdomen : NULL }}" />
          </div>

          <div id="HEENT" class="formgroup">
            <input type="text" style="width:406px;" class="rect-md-input" name="pHEENT" value="{{ isset($cf3Form->pHEENT) ? $cf3Form->pHEENT : NULL }}" /><spacer style="width:169px;"></spacer><input type="text" style="width:198px;" class="rect-input" name="pGUIE" value="{{ isset($cf3Form->pGUIE) ? $cf3Form->pGUIE : NULL }}" />
          </div>

          <div id="Chest" class="formgroup">
            <input type="text" style="width:406px;" class="rect-md-input" name="pChestLungs" value="{{ isset($cf3Form->pChestLungs) ? $cf3Form->pChestLungs : NULL }}" /><spacer style="width:169px;"></spacer><input type="text" style="width:198px;" class="rect-input" name="pSkinExtremities" value="{{ isset($cf3Form->pSkinExtremities) ? $cf3Form->pSkinExtremities : NULL }}" />
          </div>

          <div id="CVS" class="formgroup">
            <input type="text" style="width:406px;" class="rect-md-input" name="pCVS" value="{{ isset($cf3Form->pCVS) ? $cf3Form->pCVS : NULL }}" /><spacer style="width:169px;"></spacer><input type="text" style="width:198px;" class="rect-input" name="pNeuroExam" value="{{ isset($cf3Form->pNeuroExam) ? $cf3Form->pNeuroExam : NULL }}" />
          </div>

          <div id="CourseWards" class="formgroup">
            <textarea name="pCourseWard" style="position: relative;width:894px;height:210px;">{{ isset($cf3Form->pCourseWard) ? $cf3Form->pCourseWard : NULL }}</textarea>
          </div>

          <div id="Findings" class="formgroup">
            <textarea name="pPertinentFindings" style="position: relative;width:894px;height: 142px;">{{ isset($cf3Form->pPertinentFindings) ? $cf3Form->pPertinentFindings : NULL }}</textarea>
          </div>

          <div id="Disposition" class="formgroup">
            <input type="radio" class="opt-input" name="pDisposition" value="I" style="position: relative;top: 159px;left: -9px;" @if(isset($cf3Form->pDisposition) AND $cf3Form->pDisposition == 'I') checked @endif /><spacer style="width:110px;"></spacer><input type="radio" class="opt-input" name="pDisposition" value="T" style="position: relative;top: 159px;left: -9px;" @if(isset($cf3Form->pDisposition) AND $cf3Form->pDisposition == 'T') checked @endif /><spacer style="width:118px;"></spacer><input type="radio" class="opt-input" name="pDisposition" value="H" style="position: relative;top: 159px;left: -9px;" @if(isset($cf3Form->pDisposition) AND $cf3Form->pDisposition == 'H') checked @endif /><spacer style="width:110px;"></spacer><input type="radio" class="opt-input" name="pDisposition" value="A" style="position: relative;top: 159px;left: -9px;" @if(isset($cf3Form->pDisposition) AND $cf3Form->pDisposition == 'A') checked @endif /><spacer style="width:93px;"></spacer><input type="radio" class="opt-input" name="pDisposition" value="E" style="position: relative;top: 159px;left: -9px;" @if(isset($cf3Form->pDisposition) AND $cf3Form->pDisposition == 'E') checked @endif />
          </div>

        </div>
      </div>
    </div>
    
    <div class="container-fluid pad20 formcf3 formcf32">
      <div class="roww pad20"> 
        <div id="formcf32">
          
          <div id="Initial" class="formgroup">
            <?php
              //pPrenatalConsultation
              $ini_mon = NULL;
              $ini_dd = NULL;
              $ini_yy = NULL;
              if(isset($cf3Form->ini_mon)) {
                $ini_mon = $cf3Form->ini_mon;
              }
              if(isset($cf3Form->ini_dd)) {
                $ini_dd = $cf3Form->ini_dd;
              }
              if(isset($cf3Form->ini_yy)) {
                $ini_yy = $cf3Form->ini_yy;
              }
            ?>
            <input type="text" class="box-input double" name="ini_mon" value="{{ $ini_mon }}" /><spacer style="width:20px;"></spacer><input type="text" class="box-input double" name="ini_dd" value="{{ $ini_dd }}" /><spacer style="width:21px;"></spacer><input type="text" class="box-input quadruple" name="ini_yy" value="{{ $ini_yy }}" />
          </div>

          <div id="PE-Vitals" class="formgroup">
            <?php
              //pLMP
              $lmp_mon = NULL;
              $lmp_dd = NULL;
              $lmp_yy = NULL;
              if(isset($cf3Form->lmp_mon)) {
                $lmp_mon = $cf3Form->lmp_mon;
              }
              if(isset($cf3Form->lmp_dd)) {
                $lmp_dd = $cf3Form->lmp_dd;
              }
              if(isset($cf3Form->lmp_yy)) {
                $lmp_yy = $cf3Form->lmp_yy;
              }
            ?>
              <input type="checkbox" class="opt-input" name="pVitalSigns" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pVitalSigns) AND $cf3Form->pVitalSigns == 'Y') checked @endif /><spacer style="width:170px;"></spacer><input type="text" class="box-input double" name="lmp_mon" value="{{ $lmp_mon }}" /><spacer style="width:20px;"></spacer><input type="text" class="box-input double" name="lmp_dd" value="{{ $lmp_dd }}" /><spacer style="width:19px;"></spacer><input type="text" class="box-input quadruple" name="lmp_yy" value="{{ $lmp_yy }}" /><spacer style="width:102px;"></spacer><input type="text" style="width:60px;" class="rect-input" name="pMenarcheAge" value="{{ isset($cf3Form->pMenarcheAge) ? $cf3Form->pMenarcheAge : NULL }}" />
          </div>
          
          <div id="PE-Risks" class="formgroup">
              <input type="checkbox" class="opt-input" name="pPregnancyLowRisk" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pPregnancyLowRisk) AND  $cf3Form->pPregnancyLowRisk == 'Y') checked @endif /><spacer style="width:162px;"></spacer><input style="width:35px;" type="text" class="rect-input" name="pObstetricG" value="{{ isset($cf3Form->pObstetricG) ? $cf3Form->pObstetricG : NULL }}" /><spacer style="width:25px;"></spacer><input type="text" style="width:41px;" class="rect-input" name="pObstetricP" value="{{ isset($cf3Form->pObstetricP) ? $cf3Form->pObstetricP : NULL }}" /><spacer style="width:5px;"></spacer><input style="width:27px;" type="text" class="rect-input" name="pObstetric_T" value="{{ isset($cf3Form->pObstetric_T) ? $cf3Form->pObstetric_T : NULL }}" /><spacer style="width:5px;"></spacer><input type="text" style="width:27px;" class="rect-input" name="pObstetric_P" value="{{ isset($cf3Form->pObstetric_P) ? $cf3Form->pObstetric_P : NULL }}" /><spacer style="width:5px;"></spacer><input type="text" style="width:27px;" class="rect-input" name="pObstetric_A" value="{{ isset($cf3Form->pObstetric_A) ? $cf3Form->pObstetric_A : NULL }}" /><spacer style="width:5px;"></spacer><input type="text" style="width:27px;" class="rect-input" name="pObstetric_L" value="{{ isset($cf3Form->pObstetric_L) ? $cf3Form->pObstetric_L : NULL }}" />
          </div>

          <div id="OBRiskFactors" class="formgroup">
            <input type="checkbox" class="opt-input" name="pMultiplePregnancy" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pMultiplePregnancy) AND $cf3Form->pMultiplePregnancy == 'Y') checked @endif /><spacer style="width:194px;"></spacer><input type="checkbox" class="opt-input" name="pPlacentaPrevia" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pPlacentaPrevia) AND $cf3Form->pPlacentaPrevia == 'Y') checked @endif /><spacer style="width:180px;"></spacer><input type="checkbox" class="opt-input" name="pPreEclampsia" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pPreEclampsia) AND $cf3Form->pPreEclampsia == 'Y') checked @endif />
            <spacer class="vert" style="height: 0px;margin-top: -6px;"></spacer>
            <input type="checkbox" class="opt-input" name="pOvarianCyst" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pOvarianCyst) AND $cf3Form->pOvarianCyst == 'Y') checked @endif /><spacer style="width:194px;"></spacer><input type="checkbox" class="opt-input" name="pMiscarriages" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pMiscarriages) AND $cf3Form->pMiscarriages == 'Y') checked @endif /><spacer style="width:180px;"></spacer><input type="checkbox" class="opt-input" name="pEclampsia" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pEclampsia) AND $cf3Form->pEclampsia == 'Y') checked @endif />
            <spacer class="vert" style="height: 0px;margin-top: -6px;"></spacer>
            <input type="checkbox" class="opt-input" name="pMyomaUteri" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pMyomaUteri) AND $cf3Form->pMyomaUteri == 'Y') checked @endif /><spacer style="width:194px;"></spacer><input type="checkbox" class="opt-input" name="pStillBirth" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pStillBirth) AND $cf3Form->pStillBirth == 'Y') checked @endif /><spacer style="width:180px;"></spacer><input type="checkbox" class="opt-input" name="pPrematureContraction" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pPrematureContraction) AND $cf3Form->pPrematureContraction == 'Y') checked @endif />
          </div>

          <div id="SurgicalRiskFactors" class="formgroup">
            <input type="checkbox" class="opt-input" name="pHypertension" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pPrematureContraction) AND $cf3Form->pPrematureContraction == 'Y') checked @endif /><spacer style="width:194px;"></spacer><input type="checkbox" class="opt-input" name="pThyroidDisaster" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pThyroidDisaster) AND $cf3Form->pThyroidDisaster == 'Y') checked @endif /><spacer style="width:180px;"></spacer><input type="checkbox" class="opt-input" name="pEpilepsy" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pEpilepsy) AND $cf3Form->pEpilepsy == 'Y') checked @endif /><spacer style="width:235px;"></spacer><input type="checkbox" class="opt-input" name="pPreviousCS" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pPreviousCS) AND $cf3Form->pPreviousCS == 'Y') checked @endif />
            <spacer class="vert" style="height: 0px;margin-top: -6px;"></spacer>
            <input type="checkbox" class="opt-input" name="pHeartDisease" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pHeartDisease) AND $cf3Form->pHeartDisease == 'Y') checked @endif /><spacer style="width:194px;"></spacer><input type="checkbox" class="opt-input" name="pObesity" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pObesity) AND $cf3Form->pObesity == 'Y') checked @endif /><spacer style="width:180px;"></spacer><input type="checkbox" class="opt-input" name="pRenalDisease" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pRenalDisease) AND $cf3Form->pRenalDisease == 'Y') checked @endif /><spacer style="width:235px;"></spacer><input type="checkbox" class="opt-input" name="pUrineMyomectomy" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pUrineMyomectomy) AND $cf3Form->pUrineMyomectomy == 'Y') checked @endif />
            <spacer class="vert" style="height: 0px;margin-top: -6px;"></spacer>
            <input type="checkbox" class="opt-input" name="pDiabetes" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pDiabetes) AND $cf3Form->pDiabetes == 'Y') checked @endif /><spacer style="width:194px;"></spacer><input type="checkbox" class="opt-input" name="pAsthma" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pAsthma) AND $cf3Form->pAsthma == 'Y') checked @endif /><spacer style="width:180px;"></spacer><input type="checkbox" class="opt-input" name="pBleedingDisorders" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pBleedingDisorders) AND $cf3Form->pBleedingDisorders == 'Y') checked @endif />
          </div>

          <div id="AdmittingDiagnosis" class="formgroup">
            <input type="text" style="width:700px;" class="rect-input" name="pAdmittingDiagnosis" value="{{ isset($cf3Form->pAdmittingDiagnosis) ? $cf3Form->pAdmittingDiagnosis : NULL}}" />
          </div>

          <div id="DeliveryPlan" class="formgroup">
            <?php
              $edate_delv_mon = NULL;
              $edate_delv_dd = NULL;
              $edate_delv_yy = NULL;
              if(isset($cf3Form->edate_delv_mon)) {
                $edate_delv_mon = $cf3Form->edate_delv_mon;
              }
              if(isset($cf3Form->edate_delv_dd)) {
                $edate_delv_dd = $cf3Form->edate_delv_dd;
              }
              if(isset($cf3Form->edate_delv_yy)) {
                $edate_delv_yy = $cf3Form->edate_delv_yy;
              }
            ?>
            <input type="radio" class="opt-input" name="pMCPOrientation" value="Y" style="position: relative;top: 9px;" @if(isset($cf3Form->pMCPOrientation) AND $cf3Form->pMCPOrientation == 'Y') checked @endif /><spacer style="width:4px;"></spacer><input type="radio" class="opt-input" name="pMCPOrientation" value="N" style="position: relative;top: 9px;" @if(isset($cf3Form->pMCPOrientation) AND $cf3Form->pMCPOrientation == 'N') checked @endif /><spacer style="width:231px;"></spacer><input type="text" class="box-input double" name="edate_delv_mon" value="{{ $edate_delv_mon }}" /><spacer style="width:20px;"></spacer><input type="text" class="box-input double" name="edate_delv_dd" value="{{ $edate_delv_dd }}" /><spacer style="width:19px;"></spacer><input type="text" class="box-input quadruple" name="edate_delv_yy" value="{{ $edate_delv_yy }}" />

          </div>

          <div id="DataOfVisit" class="formgroup">
            <input type="text" class="rect-input double" name="pVisitDate_mon[]" value="{{ isset($cf3Form->pVisitDate_mon[0]) ? $cf3Form->pVisitDate_mon[0] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_dd[]" value="{{ isset($cf3Form->pVisitDate_dd[0]) ? $cf3Form->pVisitDate_dd[0] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_yy[]" value="{{ isset($cf3Form->pVisitDate_yy[0]) ? $cf3Form->pVisitDate_yy[0] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_mon[]" value="{{ isset($cf3Form->pVisitDate_mon[1]) ? $cf3Form->pVisitDate_mon[1] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_dd[]" value="{{ isset($cf3Form->pVisitDate_dd[1]) ? $cf3Form->pVisitDate_dd[1] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_yy[]" value="{{ isset($cf3Form->pVisitDate_yy[1]) ? $cf3Form->pVisitDate_yy[1] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_mon[]" value="{{ isset($cf3Form->pVisitDate_mon[2]) ? $cf3Form->pVisitDate_mon[2] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_dd[]" value="{{ isset($cf3Form->pVisitDate_dd[2]) ? $cf3Form->pVisitDate_dd[2] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_yy[]" value="{{ isset($cf3Form->pVisitDate_yy[2]) ? $cf3Form->pVisitDate_yy[2] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_mon[]" value="{{ isset($cf3Form->pVisitDate_mon[3]) ? $cf3Form->pVisitDate_mon[3] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_dd[]" value="{{ isset($cf3Form->pVisitDate_dd[3]) ? $cf3Form->pVisitDate_dd[3] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_yy[]" value="{{ isset($cf3Form->pVisitDate_yy[3]) ? $cf3Form->pVisitDate_yy[3] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_mon[]" value="{{ isset($cf3Form->pVisitDate_mon[4]) ? $cf3Form->pVisitDate_mon[4] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_dd[]" value="{{ isset($cf3Form->pVisitDate_dd[4]) ? $cf3Form->pVisitDate_dd[4] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_yy[]" value="{{ isset($cf3Form->pVisitDate_yy[4]) ? $cf3Form->pVisitDate_yy[4] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_mon[]" value="{{ isset($cf3Form->pVisitDate_mon[5]) ? $cf3Form->pVisitDate_mon[5] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_dd[]" value="{{ isset($cf3Form->pVisitDate_dd[5]) ? $cf3Form->pVisitDate_dd[5] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_yy[]" value="{{ isset($cf3Form->pVisitDate_yy[5]) ? $cf3Form->pVisitDate_yy[5] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_mon[]" value="{{ isset($cf3Form->pVisitDate_mon[6]) ? $cf3Form->pVisitDate_mon[6] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_dd[]" value="{{ isset($cf3Form->pVisitDate_dd[6]) ? $cf3Form->pVisitDate_dd[6] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_yy[]" value="{{ isset($cf3Form->pVisitDate_yy[6]) ? $cf3Form->pVisitDate_yy[6] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_mon[]" value="{{ isset($cf3Form->pVisitDate_mon[7]) ? $cf3Form->pVisitDate_mon[7] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_dd[]" value="{{ isset($cf3Form->pVisitDate_dd[7]) ? $cf3Form->pVisitDate_dd[7] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_yy[]" value="{{ isset($cf3Form->pVisitDate_yy[7]) ? $cf3Form->pVisitDate_yy[7] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_mon[]" value="{{ isset($cf3Form->pVisitDate_mon[8]) ? $cf3Form->pVisitDate_mon[8] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_dd[]" value="{{ isset($cf3Form->pVisitDate_dd[8]) ? $cf3Form->pVisitDate_dd[8] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_yy[]" value="{{ isset($cf3Form->pVisitDate_yy[8]) ? $cf3Form->pVisitDate_yy[8] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_mon[]" value="{{ isset($cf3Form->pVisitDate_mon[9]) ? $cf3Form->pVisitDate_mon[9] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_dd[]" value="{{ isset($cf3Form->pVisitDate_dd[9]) ? $cf3Form->pVisitDate_dd[9] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_yy[]" value="{{ isset($cf3Form->pVisitDate_yy[9]) ? $cf3Form->pVisitDate_yy[9] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_mon[]" value="{{ isset($cf3Form->pVisitDate_mon[10]) ? $cf3Form->pVisitDate_mon[10] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_dd[]" value="{{ isset($cf3Form->pVisitDate_dd[10]) ? $cf3Form->pVisitDate_dd[10] : NULL}}" /><input type="text" class="rect-input double" name="pVisitDate_yy[]" value="{{ isset($cf3Form->pVisitDate_yy[10]) ? $cf3Form->pVisitDate_yy[10] : NULL}}" />
          </div>

          <div id="AOG" class="formgroup">
            <input type="text" class="rect-input" name="pAOGWeeks[]" value="{{ $cf3Form->pAOGWeeks[0] or NULL }}" />
            <input type="text" class="rect-input" name="pAOGWeeks[]" value="{{ $cf3Form->pAOGWeeks[1] or NULL }}" />
            <input type="text" class="rect-input" name="pAOGWeeks[]" value="{{ $cf3Form->pAOGWeeks[2] or NULL }}" />
            <input type="text" class="rect-input" name="pAOGWeeks[]" value="{{ $cf3Form->pAOGWeeks[3] or NULL }}" />
            <input type="text" class="rect-input" name="pAOGWeeks[]" value="{{ $cf3Form->pAOGWeeks[4] or NULL }}" />
            <input type="text" class="rect-input" name="pAOGWeeks[]" value="{{ $cf3Form->pAOGWeeks[5] or NULL }}" />
            <input type="text" class="rect-input" name="pAOGWeeks[]" value="{{ $cf3Form->pAOGWeeks[6] or NULL }}" />
            <input type="text" class="rect-input" name="pAOGWeeks[]" value="{{ $cf3Form->pAOGWeeks[7] or NULL }}" />
            <input type="text" class="rect-input" name="pAOGWeeks[]" value="{{ $cf3Form->pAOGWeeks[8] or NULL }}" />
            <input type="text" class="rect-input" name="pAOGWeeks[]" value="{{ $cf3Form->pAOGWeeks[9] or NULL }}" />
            <input type="text" class="rect-input" name="pAOGWeeks[]" value="{{ $cf3Form->pAOGWeeks[10] or NULL }}" />
            <spacer class="vert" style="height:18px;"></spacer>
            <input type="text" class="rect-input" name="pWeight[]" value="{{ $cf3Form->pWeight[0] or NULL }}" />
            <input type="text" class="rect-input" name="pWeight[]" value="{{ $cf3Form->pWeight[1] or NULL }}" />
            <input type="text" class="rect-input" name="pWeight[]" value="{{ $cf3Form->pWeight[2] or NULL }}" />
            <input type="text" class="rect-input" name="pWeight[]" value="{{ $cf3Form->pWeight[3] or NULL }}" />
            <input type="text" class="rect-input" name="pWeight[]" value="{{ $cf3Form->pWeight[4] or NULL }}" />
            <input type="text" class="rect-input" name="pWeight[]" value="{{ $cf3Form->pWeight[5] or NULL }}" />
            <input type="text" class="rect-input" name="pWeight[]" value="{{ $cf3Form->pWeight[6] or NULL }}" />
            <input type="text" class="rect-input" name="pWeight[]" value="{{ $cf3Form->pWeight[7] or NULL }}" />
            <input type="text" class="rect-input" name="pWeight[]" value="{{ $cf3Form->pWeight[8] or NULL }}" />
            <input type="text" class="rect-input" name="pWeight[]" value="{{ $cf3Form->pWeight[9] or NULL }}" />
            <input type="text" class="rect-input" name="pWeight[]" value="{{ $cf3Form->pWeight[10] or NULL }}" />
            <spacer class="vert" style="height:2px;"></spacer>
            <input type="text" class="rect-input" name="pCardiacRate[]" value="{{ $cf3Form->pCardiacRate[0] or NULL }}" />
            <input type="text" class="rect-input" name="pCardiacRate[]" value="{{ $cf3Form->pCardiacRate[1] or NULL }}" />
            <input type="text" class="rect-input" name="pCardiacRate[]" value="{{ $cf3Form->pCardiacRate[2] or NULL }}" />
            <input type="text" class="rect-input" name="pCardiacRate[]" value="{{ $cf3Form->pCardiacRate[3] or NULL }}" />
            <input type="text" class="rect-input" name="pCardiacRate[]" value="{{ $cf3Form->pCardiacRate[4] or NULL }}" />
            <input type="text" class="rect-input" name="pCardiacRate[]" value="{{ $cf3Form->pCardiacRate[5] or NULL }}" />
            <input type="text" class="rect-input" name="pCardiacRate[]" value="{{ $cf3Form->pCardiacRate[6] or NULL }}" />
            <input type="text" class="rect-input" name="pCardiacRate[]" value="{{ $cf3Form->pCardiacRate[7] or NULL }}" />
            <input type="text" class="rect-input" name="pCardiacRate[]" value="{{ $cf3Form->pCardiacRate[8] or NULL }}" />
            <input type="text" class="rect-input" name="pCardiacRate[]" value="{{ $cf3Form->pCardiacRate[9] or NULL }}" />
            <input type="text" class="rect-input" name="pCardiacRate[]" value="{{ $cf3Form->pCardiacRate[10] or NULL }}" />
            <spacer class="vert" style="height:2px;"></spacer>
            <input type="text" class="rect-input" name="pRespiratoryRate[]" value="{{ $cf3Form->pRespiratoryRate[0] or NULL }}" />
            <input type="text" class="rect-input" name="pRespiratoryRate[]" value="{{ $cf3Form->pRespiratoryRate[1] or NULL }}" />
            <input type="text" class="rect-input" name="pRespiratoryRate[]" value="{{ $cf3Form->pRespiratoryRate[2] or NULL }}" />
            <input type="text" class="rect-input" name="pRespiratoryRate[]" value="{{ $cf3Form->pRespiratoryRate[3] or NULL }}" />
            <input type="text" class="rect-input" name="pRespiratoryRate[]" value="{{ $cf3Form->pRespiratoryRate[4] or NULL }}" />
            <input type="text" class="rect-input" name="pRespiratoryRate[]" value="{{ $cf3Form->pRespiratoryRate[5] or NULL }}" />
            <input type="text" class="rect-input" name="pRespiratoryRate[]" value="{{ $cf3Form->pRespiratoryRate[6] or NULL }}" />
            <input type="text" class="rect-input" name="pRespiratoryRate[]" value="{{ $cf3Form->pRespiratoryRate[7] or NULL }}" />
            <input type="text" class="rect-input" name="pRespiratoryRate[]" value="{{ $cf3Form->pRespiratoryRate[8] or NULL }}" />
            <input type="text" class="rect-input" name="pRespiratoryRate[]" value="{{ $cf3Form->pRespiratoryRate[9] or NULL }}" />
            <input type="text" class="rect-input" name="pRespiratoryRate[]" value="{{ $cf3Form->pRespiratoryRate[10] or NULL }}" />
            <spacer class="vert" style="height:1px;"></spacer>
            <input type="text" class="rect-input" name="pBloodPressure[]" value="{{ $cf3Form->pBloodPressure[0] or NULL }}" />
            <input type="text" class="rect-input" name="pBloodPressure[]" value="{{ $cf3Form->pBloodPressure[1] or NULL }}" />
            <input type="text" class="rect-input" name="pBloodPressure[]" value="{{ $cf3Form->pBloodPressure[2] or NULL }}" />
            <input type="text" class="rect-input" name="pBloodPressure[]" value="{{ $cf3Form->pBloodPressure[3] or NULL }}" />
            <input type="text" class="rect-input" name="pBloodPressure[]" value="{{ $cf3Form->pBloodPressure[4] or NULL }}" />
            <input type="text" class="rect-input" name="pBloodPressure[]" value="{{ $cf3Form->pBloodPressure[5] or NULL }}" />
            <input type="text" class="rect-input" name="pBloodPressure[]" value="{{ $cf3Form->pBloodPressure[6] or NULL }}" />
            <input type="text" class="rect-input" name="pBloodPressure[]" value="{{ $cf3Form->pBloodPressure[7] or NULL }}" />
            <input type="text" class="rect-input" name="pBloodPressure[]" value="{{ $cf3Form->pBloodPressure[8] or NULL }}" />
            <input type="text" class="rect-input" name="pBloodPressure[]" value="{{ $cf3Form->pBloodPressure[9] or NULL }}" />
            <input type="text" class="rect-input" name="pBloodPressure[]" value="{{ $cf3Form->pBloodPressure[10] or NULL }}" />
            <spacer class="vert" style="height:0px;"></spacer>
            <input type="text" class="rect-input" name="pTemperature[]" value="{{ $cf3Form->pTemperature[0] or NULL }}" />
            <input type="text" class="rect-input" name="pTemperature[]" value="{{ $cf3Form->pTemperature[1] or NULL }}" />
            <input type="text" class="rect-input" name="pTemperature[]" value="{{ $cf3Form->pTemperature[2] or NULL }}" />
            <input type="text" class="rect-input" name="pTemperature[]" value="{{ $cf3Form->pTemperature[3] or NULL }}" />
            <input type="text" class="rect-input" name="pTemperature[]" value="{{ $cf3Form->pTemperature[4] or NULL }}" />
            <input type="text" class="rect-input" name="pTemperature[]" value="{{ $cf3Form->pTemperature[5] or NULL }}" />
            <input type="text" class="rect-input" name="pTemperature[]" value="{{ $cf3Form->pTemperature[6] or NULL }}" />
            <input type="text" class="rect-input" name="pTemperature[]" value="{{ $cf3Form->pTemperature[7] or NULL }}" />
            <input type="text" class="rect-input" name="pTemperature[]" value="{{ $cf3Form->pTemperature[8] or NULL }}" />
            <input type="text" class="rect-input" name="pTemperature[]" value="{{ $cf3Form->pTemperature[9] or NULL }}" />
            <input type="text" class="rect-input" name="pTemperature[]" value="{{ $cf3Form->pTemperature[10] or NULL }}" />
          </div>

          <div id="Delivery" class="formgroup">
            <?php
              $deliv_mon = NULL;
              $deliv_dd = NULL;
              $deliv_yy = NULL;
              $deliv_hh_am = NULL;
              $deliv_hh_pm = NULL;

              if(isset($cf3Form->deliv_mon)) {
                $deliv_mon = $cf3Form->deliv_mon;
              }
              if(isset($cf3Form->deliv_dd)) {
                $deliv_dd = $cf3Form->deliv_dd;
              }
              if(isset($cf3Form->deliv_yy)) {
                $deliv_yy = $cf3Form->deliv_yy;
              }
              if(isset($cf3Form->deliv_hh_am)) {
                $deliv_hh_am = $cf3Form->deliv_hh_am;
              }
              if(isset($cf3Form->deliv_hh_pm)) {
                $deliv_hh_pm = $cf3Form->deliv_hh_pm;
              }
            ?>
            <input type="text" class="box-input double" name="deliv_mon" value="{{ $deliv_mon }}" /><spacer style="width:20px;"></spacer><input type="text" class="box-input double" name="deliv_dd" value="{{ $deliv_dd }}" /><spacer style="width:20px;"></spacer><input type="text" class="box-input quadruple" name="deliv_yy" value="{{ $deliv_yy }}" /><spacer style="width: 73px;"></spacer><input type="text" class="box-input quintuple" name="deliv_hh_am" value="{{ $deliv_hh_am }}" /><spacer style="width:27px;"></spacer><input type="text" class="box-input quintuple" name="deliv_hh_pm" value="{{ $deliv_hh_pm }}" />
          </div>

          <div id="MaternalOutcome" class="formgroup">
            <input type="text" style="width: 76px;" class="rect-input" name="pObstetricIndex" value="{{ isset($cf3Form->pObstetricIndex) ? $cf3Form->pObstetricIndex : NULL }}" /><spacer style="width: 100px;"></spacer><input type="text" style="width:170px;" class="rect-input" name="pAOGLMP" value="{{ isset($cf3Form->pAOGLMP) ? $cf3Form->pAOGLMP : NULL }}" /><spacer style="width:11px;"></spacer><input type="text" class="rect-input" name="pDeliveryManner" value="{{ isset($cf3Form->pDeliveryManner) ? $cf3Form->pDeliveryManner : NULL }}" style="width:163px;" /><spacer style="width:12px;"></spacer><input type="text" style="width:164px;" class="rect-input" name="pPresentation" value="{{ isset($cf3Form->pPresentation) ? $cf3Form->pPresentation : NULL }}" />
            <spacer class="vert" style="height:30px;"></spacer>
            <input type="text" style="width: 164px;" class="rect-input" name="pFetalOutcome" value="{{ isset($cf3Form->pFetalOutcome) ? $cf3Form->pFetalOutcome : NULL }}" /><spacer style="width: 12px;"></spacer><input type="text" style="width:170px;" class="rect-input" name="pSex" value="{{ isset($cf3Form->pSex) ? $cf3Form->pSex : NULL }}" /><spacer style="width:11px;"></spacer><input type="text" class="rect-input" name="pBirthWeight" value="{{ isset($cf3Form->pBirthWeight) ? $cf3Form->pBirthWeight : NULL }}" style="width:163px;" /><spacer style="width:12px;"></spacer><input type="text" style="width:164px;" class="rect-input" name="pAPGARScore" value="{{ isset($cf3Form->pAPGARScore) ? $cf3Form->pAPGARScore : NULL }}" />
            <spacer class="vert" style="height:21px"></spacer>
            <?php
              //pPostpartum
              $ppartum_mon = NULL;
              $ppartum_dd = NULL;
              $ppartum_yy = NULL;
              if(isset($cf3Form->ppartum_mon)) {
                $ppartum_mon = $cf3Form->ppartum_mon;
              }
              if(isset($cf3Form->ppartum_dd)) {
                $ppartum_dd = $cf3Form->ppartum_dd;
              }
              if(isset($cf3Form->ppartum_yy)) {
                $ppartum_yy = $cf3Form->ppartum_yy;
              }
            ?>
            <spacer style="width:328px;"></spacer><input type="text" class="box-input double" name="ppartum_mon" value="{{ $ppartum_mon }}" /><spacer style="width:20px;"></spacer><input type="text" class="box-input double" name="ppartum_dd" value="{{ $ppartum_dd }}" /><spacer style="width:20px;"></spacer><input type="text" class="box-input quadruple" name="ppartum_yy" value="{{ $ppartum_yy }}" />
            <spacer class="vert" style="height:13px"></spacer>
            <?php
              //DischargeDate
              $deliv_dc_mon = NULL;
              $deliv_dc_dd = NULL;
              $deliv_dc_yy = NULL;
              $deliv_dc_hh_am = NULL;
              $deliv_dc_hh_pm = NULL;

              if(isset($cf3Form->deliv_dc_mon)) {
                $deliv_dc_mon = $cf3Form->deliv_dc_mon;
              }
              if(isset($cf3Form->deliv_dc_dd)) {
                $deliv_dc_dd = $cf3Form->deliv_dc_dd;
              }
              if(isset($cf3Form->deliv_dc_yy)) {
                $deliv_dc_yy = $cf3Form->deliv_dc_yy;
              }
              if(isset($cf3Form->deliv_dc_hh_am)) {
                $deliv_dc_hh_am = $cf3Form->deliv_dc_hh_am;
              }
              if(isset($cf3Form->deliv_dc_hh_pm)) {
                $deliv_dc_hh_pm = $cf3Form->deliv_dc_hh_pm;
              }
              
            ?>
            <spacer style="width:104px;"></spacer><input type="text" class="box-input double" name="deliv_dc_mon" value="{{ $deliv_dc_mon }}" /><spacer style="width:20px;"></spacer><input type="text" class="box-input double" name="deliv_dc_dd" value="{{ $deliv_dc_dd }}" /><spacer style="width:20px;"></spacer><input type="text" class="box-input quadruple" name="deliv_dc_yy" value="{{ $deliv_dc_yy }}" /><spacer style="width:80px;"></spacer><input type="text" class="box-input quintuple" name="deliv_dc_hh_am" value="{{ $deliv_dc_hh_am }}" /><spacer style="width:27px;"></spacer><input type="text" class="box-input quintuple" name="deliv_dc_hh_pm" value="{{ $deliv_dc_hh_pm }}" />
          </div>

          <div id="PostPartumCare" class="formgroup">
            <input type="checkbox" class="opt-input" name="pPerinealWoundCare" value="Y" style="position: relative;top:9px;" @if(isset($cf3Form->pPerinealWoundCare) AND $cf3Form->pPerinealWoundCare == 'Y') checked @endif /><spacer style="width:5px;"></spacer><input style="width:347px;" type="text" class="rect-input" name="pPerinealRemarks" value="{{ isset($cf3Form->pPerinealRemarks) ? $cf3Form->pPerinealRemarks : NULL }}" />
            <spacer class="vert" style="height:3px;margin-top:-10px;"></spacer>
            <input type="checkbox" class="opt-input" name="pMaternalComplications" value="Y" style="position: relative;top:7px;" @if(isset($cf3Form->pMaternalComplications) AND $cf3Form->pMaternalComplications == 'Y') checked @endif /><spacer style="width:5px;"></spacer><input style="width:347px;" type="text" class="rect-input" name="pMaternalRemarks" value="{{ isset($cf3Form->pMaternalRemarks) ? $cf3Form->pMaternalRemarks : NULL }}" />
            <spacer class="vert" style="height:23px;margin-top:-10px;"></spacer>
            <input type="checkbox" class="opt-input" name="pBreastFeeding" value="Y" style="position: relative;top:7px;" @if(isset($cf3Form->pBreastFeeding) AND $cf3Form->pBreastFeeding == 'Y') checked @endif /><spacer style="width:5px;"></spacer><input style="width:347px;" type="text" class="rect-input" name="pBreastFeedingRemarks" value="{{ isset($cf3Form->pBreastFeedingRemarks) ? $cf3Form->pBreastFeedingRemarks : NULL }}" />
            <spacer class="vert" style="height:3px;margin-top:-12px;"></spacer>
            <input type="checkbox" class="opt-input" name="pFamilyPlanning" value="Y" style="position: relative;top:7px;" @if(isset($cf3Form->pFamilyPlanning) AND $cf3Form->pFamilyPlanning == 'Y') checked @endif /><spacer style="width:5px;"></spacer><input style="width:347px;" type="text" class="rect-input" name="pFamilyPlanningRemarks" value="{{ isset($cf3Form->pFamilyPlanningRemarks) ? $cf3Form->pFamilyPlanningRemarks : NULL }}" />
            <spacer class="vert" style="height:3px;margin-top:-12px;"></spacer>
            <input type="checkbox" class="opt-input" name="pPlanningService" value="Y" style="position: relative;top:7px;" @if(isset($cf3Form->pPlanningService) AND $cf3Form->pPlanningService == 'Y') checked @endif /><spacer style="width:5px;"></spacer><input style="width:347px;" type="text" class="rect-input" name="pPlanningServiceRemarks" value="{{ isset($cf3Form->pPlanningServiceRemarks) ? $cf3Form->pPlanningServiceRemarks : NULL }}" />
            <spacer class="vert" style="height:3px;margin-top:-12px;"></spacer>
            <input type="checkbox" class="opt-input" name="pSurgicalSterilization" value="Y" style="position: relative;top:7px;" @if(isset($cf3Form->pSurgicalSterilization) AND $cf3Form->pSurgicalSterilization == 'Y') checked @endif /><spacer style="width:5px;"></spacer><input style="width:347px;" type="text" class="rect-input" name="pSterilizationRemarks" value="{{ isset($cf3Form->pSterilizationRemarks) ? $cf3Form->pSterilizationRemarks : NULL }}" />
            <spacer class="vert" style="height:3px;margin-top:-12px;"></spacer>
            <input type="checkbox" class="opt-input" name="pFollowupSchedule" value="Y" style="position: relative;top:7px;" @if(isset($cf3Form->pFollowupSchedule) AND $cf3Form->pFollowupSchedule == 'Y') checked @endif /><spacer style="width:5px;"></spacer><input style="width:347px;" type="text" class="rect-input" name="pFollowupScheduleRemarks" value="{{ isset($cf3Form->pFollowupScheduleRemarks) ? $cf3Form->pFollowupScheduleRemarks : NULL }}" />
          </div>

            <?php
              //pPostpartum
              $signed_mon = NULL;
              $signed_dd = NULL;
              $signed_yy = NULL;
              if(isset($cf3Form->signed_mon)) {
                $signed_mon = $cf3Form->signed_mon;
              }
              if(isset($cf3Form->signed_dd)) {
                $signed_dd = $cf3Form->signed_dd;
              }
              if(isset($cf3Form->signed_yy)) {
                $signed_yy = $cf3Form->signed_yy;
              }
            ?>

          <div id="Certification" class="formgroup">
            <input type="text" class="box-input double" name="signed_mon" value="{{ $signed_mon }}" /><spacer style="width:20px;"></spacer><input type="text" class="box-input double" name="signed_dd" value="{{ $signed_dd }}" /><spacer style="width:20px;"></spacer><input type="text" class="box-input quadruple" name="signed_yy" value="{{ $signed_yy }}" />
          </div>

        </div>
      </div>
    </div>
  </form>
</div>

<script>
  $(document).ready(function() {
    $("input.box-input").attr("maxlength", 1);
    $("input.box-input.double").attr("maxlength", 2);
    $("input.rect-input.double").attr("maxlength", 2);
    $("input.box-input.quadruple").attr("maxlength", 4);
    $("input.box-input.quintuple").attr("maxlength", 5);
    $("input.box-input.cf3-pan").attr("maxlength", 9);
  });
</script>
