<div class="container-fluid pad20" style="background-color:#FFF;">
    <div class="roww pad20" id="printForm">
        <table width="100%">
            <tr>
                <td width="20%" align="center"><img src="{{ asset('public/dist/img/phic01-logo.png') }}" height="100" /></td>
                <td align="center">
                    <small>Republic of the Philippines</small>
                    <p class="lead no-margin-bottom">Philippine Health Insurance Corporation</p>
                    <small>Citystate Centre Building, 709 Shaw Boulevard, Pasig City<br />
                    Healthline: 441-7444 Website: http://www.philhealth.gov.ph</small>
                </td>
                <td width="20%" align="center"><img src="{{ asset('public/dist/img/phic02-logo.jpg') }}" height="100" /></td>
            </tr>
        </table>
    
        <div class="col-md-12 bg-yellow pad10">
            <h2 class="text-black text-center">PhilHealth Benefit Eligibility Form</h2>
        </div>
        <div class="text-center">
            <small><em>Bawat Pilipino Miembro. Bawat Miembro Protektado. Kalusugan Natin Sigurado</em></small>
        </div>
    
        <table width="100%">
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td width="30%">Date &amp; Time of Generation : </td>
                <td>{{ date("F d, Y h:i:s A", strtotime($date_time)) }}</td>
            </tr>
            <tr>
                <td width="30%">HCI Portal Reference No. : </td>
                <td>{{ $hci_portal_ref }}</td>
            </tr>
            <tr><td class="bordered" colspan="2"></td></tr>
            <tr><td colspan="2"><h5>HEALTH CARE INSTITUTION (HCI) INFORMATION</h5></td></tr>
            <tr>
                <td width="30%">Name of Institution : </td>
                <td>{{ $hci_name }}</td>
            </tr>
            <tr>
                <td width="30%">Accreditation No. : </td>
                <td>{{ $hci_accr }}</td>
            </tr>
            <tr><td class="bordered" colspan="2"></td></tr>
            <tr><td colspan="2"><h5>MEMBER INFORMATION</h5></td></tr>
            <tr>
                <td width="30%">PhilHealth Information No. : </td>
                <td>{{ $pin }}</td>
            </tr>
            <tr>
                <td width="30%">Name of Member : </td>
                <td>{{ $name }}</td>
            </tr>
            <tr>
                <td width="30%">Sex : </td>
                <td>{{ $sex }}</td>
            </tr>
            <tr>
                <td width="30%">Date of Birth : </td>
                <td>{{ $birthdate }}</td>
            </tr>
            <tr>
                <td width="30%">Member Category : </td>
                <td>{{ $category }}</td>
            </tr>
            <tr><td class="bordered" colspan="2"></td></tr>
            <tr><td colspan="2"><h5>PATIENT INFORMATION</h5></td></tr>
            <tr>
                <td width="30%">Name of Patient : </td>
                <td>{{ $patient }}</td>
            </tr>
            <tr>
                <td width="30%">Date Admitted : </td>
                <td>{{ $admitted }}</td>
            </tr>
            <tr>
                <td width="30%">Date Discharged : </td>
                <td>{{ $discharge }}</td>
            </tr>
            <tr>
                <td width="30%">Sex : </td>
                <td>{{ $patsex }}</td>
            </tr>
            <tr>
                <td width="30%">Date of Birth : </td>
                <td>{{ $patbirthdate }}</td>
            </tr>
        </table>
        
        <table width="100%">
            <tr><td class="bordered" colspan="2"></td></tr>
        </table>
        
        <h5>ELIGIBILITY INFORMATION</h5>
        <h4 class="text-center">ELIGIBLE TO AVAIL OF PHILHEALTH BENEFITS? = {{ $isok }}</h4>
        <p style="margin-top:20px;">Reason/s: {{ $reasons }}</p>
        
        <table width="100%">
        <tr><td class="bordered" colspan="2"></td></tr>
        </table>
        
        <h5>Attached Documents:</h5>
        <p>NA</p>
        
        <table width="100%">
        <tr><td class="bordered" colspan="2"></td></tr>
        </table>
        
        <h5>Important Reminders:</h5>
        <ol>
            <li class="small">Generation and printing of this form is FREE for all PhilHealth Beneficiaries.</li>
            <li class="small">This form shall be submitted aling with the required PhilHealth claims form and is valid only for the confinement/admission stated above.</li>
            <li class="small">This does not include eligibility to the rule of SINGLE PERIOD OF CONFINEMENT (SPC). It shall be established when the claim is processed by PhilHealth.<br />Non-qualification to the rule of SPC shall result to the denial of this claim.</li>
        </ol>
        
        <table width="100%">
        <tr><td class="bordered" colspan="2"></td></tr>
        </table>
        
        <div class="roww clearfix">
            <table width="100%">
                <tr>
                    <td width="40%" align="center" class="pad20">
                        <hr class="sign" />
                        <small>Member/Representative</small><br />
                        <small>Signature Over Printed Name / Thumbmark</small>
                    </td>
                    <td width="40%" align="center" class="pad20">
                        <hr class="sign" />
                        <small>HCI Portal User</small><br />
                        <small>Signature Over Printed Name / Thumbmark</small>
                    </td>
                    <td width="20%" align="center" valign="bottom">
                        <small>Visit www.philhealth.gov.pg</small><br />
                        <img src="{{ asset('public/dist/img/phic-qrcode.jpg') }}" height="100" />
                    </td>
                </tr>
            </table>
        </div>
        <p clear="all">&nbsp;</p>
        <div class="roww">
            <div class="col-md-12 bg-yellow pad10 text-center">
                <p class="text-black text-center no-margin-bottom">Philippine Health Insurance Corporation</p>
                <small class="text-black">Citystate Centre Building, 709 Shaw Boulevard, Pasig City 
            Healthline: 441-7444</small>
            </div>
        </div>
    </div>

    <div class="roww">
        <div class="col-md-12 pad10 text-center">
            <a href="{{ url('eclaims') }}" type="button" class="btn btn-default" >Close</a>
            <button class="btn btn-success" onclick="$('#printForm').printThis();">Print Form</button> <button onclick="topdf();" class="btn btn-info" >Save to PDF</button>
        </div>
    </div>
</div>

<script>
  $(document).ready(function() {
    $("#buttonGroup").remove();
  });
</script>