<div id="printFormCF4">
<style>
  .formcf4 {
    padding:0;
    height:1807px;
    width:961px;
    @if(isset($page))
    position:relative;
    margin:0 auto;
    @else
    margin:3px 0 0 3px;
    @endif
  }
  .formcf41 {
    background: #FFF url('{!! asset("modules/Eclaims/Assets/images/cf41.png") !!}') center top no-repeat !important;
    background-size: contain !important;
    margin-bottom: 20px;
  }
  .formcf42 {
    background: #FFF url('{!! asset("modules/Eclaims/Assets/images/cf42.png") !!}') center top no-repeat !important;
    background-size: contain !important;
    height:1576px;
  }
  @if(isset($page))
    #formcf41, #formcf42 {
      margin-top: 25px;
      position: relative;
      margin-left: 0px;
    }
  @endif
  .formgroup {
    position: absolute;
  }
  .formcf4 input, .formcf4 textarea {
    background-color: rgba(200,200,200,.8);
    border: none;
    height: 22px;
    padding: 1px 5px;
    max-width: 1500px;
  }
  

  input {
    text-transform: uppercase !important;
  }
  input.box-input {
    width: 20px;
    text-align:center;
    vertical-align: middle;
    margin-left: 0.1em;
  }

  input.pan {
    width: 17px;
    text-align:center;
    vertical-align: middle;
  }
  input.rect-input {
    width: 172px;
    vertical-align: middle;
    margin-left: 0.1em;
  }
  input.rect-input.name_field {
    margin-right:10px;
  }
  input.opt-input {
    width: 23px;
    margin: 0.5px 0 0 0;
  }
  input[type='date'] {
    font-size: 10px;
    padding: 1px 0px 1px 4px;
  }
  input[name^='ad_'],
  input[name^='dc_'],
  .pin-number {
    letter-spacing: 10px;
    padding-left: 8px !important;
  }
  spacer {
    width: 12px;
    display: inline-block;
  }
  spacer.lab {
    width: 41px;
    display: inline-block;
  }
  spacer.vert {
    height: 0px;
    display: block;
    margin-top: -1px;
  }
  .w165 {
    width: 165px !important;
  }
  .w158 {
    width: 158px !important;
  }
  .w115 {
    width: 115px !important;
  }
  .w100 {
    width: 100px !important;
  }
  .w85 {
    width: 85px !important;
  }
  .w70 {
    width: 70px !important;
  }
  .w50 {
    width: 52px !important;
  }
  .w18 {
    width: 18px !important;
  }
  .w12 {
    width: 12px !important;
  }
  .alert-absolute {
    position: absolute;
    width: 93%;
  }
  #HCIName {
    position: absolute;
    top: 204px;
    left: -2px;
  }
    #HCIName input {
      width: 552px;
      height: 33px;
    }
  #PAN {
    position: absolute;
    top: 204px;
    left: 552px;
  }
    #PAN input {
      width: 366px;
      height: 33px;
    }
  #HCIAddress {
    position: absolute;
    top: 256px;
    left: -2px;
  }
    #HCIAddress input {
      height: 33px;
      margin-right:1px;
    }
    #HCIAddress input[name=hci_bldg] {
      width: 140px;
    }
    #HCIAddress input[name=hci_street] {
      width: 127px;
    }
    #HCIAddress input[name=pPatientAddbrgy] {
      width: 118.5px;
      margin-left: 0px;
    }
    #HCIAddress input[name=pPatientAddmun] {
      width: 119.5px;
      margin-left: .5px;
    }
    #HCIAddress input[name=pPatientAddprov] {
      width: 308px;
    }
    #HCIAddress input[name=pPatientAddzipcode] {
      width: 97px;
    }
  #PatName {
    position: absolute;
    top: 352px;
    left: -2px;
  }
    #PatName input {
      height: 23px;
      margin-right: 1px;
    }
    #PatName input[name=pPatientLname] {
      width: 203px;
    }
    #PatName input[name=pPatientFname] {
      width: 206px;
    }
    #PatName input[name=pPatientMname] {
      width: 250px;
    }
  #PatPIN {
    position: absolute;
    top: 352px;
    left: 662px;
  }
    #PatPIN input {
      height: 23px;
      margin-right: 1px;
      width: 257px;
    }
  #PatAge {
    position: absolute;
    top: 397px;
    left: 662px;
  }
    #PatAge input {
      height: 23px;
      margin-right: 1px;
      width: 257px;
    }
  #Compliant {
    position: absolute;
    top: 420px;
    left: -1px;
  }
    #Compliant textarea {
      height: 44px;
      margin-right: 1px;
      width: 665px;
    }
  #PatGender {
    position: absolute;
    top: 420px;
    left: 664px;
  }
    #PatGender input:nth-child(1) {
      margin-top: 6px;
      margin-left: 93px;
    }
    #PatGender input:nth-child(2) {
      margin-top: 6px;
      margin-left: 34px;
    }
  #AdmitDiagnosis {
    position: absolute;
    top: 484px;
    left: -1px;
  }
    #AdmitDiagnosis textarea {
      height: 69px;
      margin-right: 1px;
      width: 328px;
    }
  #DischDiagnosis {
    position: absolute;
    top: 484px;
    left: 328px;
  }
    #DischDiagnosis textarea {
      height: 69px;
      margin-right: 1px;
      width: 335px;
    }
  #firstCaseRate {
    position: absolute;
    top: 486px;
    left: 662px;
  }
    #firstCaseRate input {
      height: 23px;
      margin-right: 1px;
      width: 257px;
    }
  #secondCaseRate {
    position: absolute;
    top: 530px;
    left: 662px;
  }
    #secondCaseRate input {
      height: 23px;
      margin-right: 1px;
      width: 257px;
    }
  #Admitted {
    position: absolute;
    top: 563px;
    left: 146px;
  }
    #Admitted input {
      width:44px;
    }
    #Admitted input.yearbox {
      width:88px;
    }
    #Admitted spacer {
      width: 14px;
    }
  #admittedTime {
    position: absolute;
    top: 560px;
    left: 648px;
  }
    #admittedTime input {
      width:44px;
    }
    #admittedTime input[type=radio] {
      width:22px;
    }
  #Discharge {
    position: absolute;
    top: 607px;
    left: 146px;
  }
    #Discharge input {
      width:44px;
    }
    #Discharge input.yearbox {
      width:88px;
    }
    #Discharge spacer {
      width: 14px;
    }
  #dischargeTime {
    position: absolute;
    top: 604px;
    left: 648px;
  }
    #dischargeTime input {
      width:44px;
    }
    #dischargeTime input[type=radio] {
      width:22px;
    }
  #HistoryOfIllness {
    position: absolute;
    top: 687px;
    left: -1px;
  }
    #HistoryOfIllness textarea {
      height: 185px;
      margin-right: 1px;
      width: 921px;
    }
  #PastMedicalHistory {
    position: absolute;
    top: 896px;
    left: -1px;
  }
    #PastMedicalHistory textarea {
      height: 101px;
      margin-right: 1px;
      width: 921px;
    }
  #ObGynHistory {
    position: absolute;
    top: 1019px;
    left: 21px;
  }
    #ObGynHistory input {
      width:31px;
    }

  #pSignsSymptoms {
    position: absolute;
    top: 1102px;
    left: 16px;
  }
    #pSignsSymptoms br {
      display: block;
      margin: 3px 0 3.5px;
    }

    #pSignsSymptoms input.opt-input, #PeAdmissionHEENT input.opt-input {
      margin-bottom: 9px;
    }


  #Referred {
    position: absolute;
    top: 1424px;
    left: 334px;
  }
    #Referred input {
      width:31px;
    }
  #PeAdmissionGS {
    position: absolute;
    top: 1544px;
    left: 148px;
  }
  #PeAdmissionVS {
    position: absolute;
    top: 1580px;
    left: 175px;
  }
  #PeAdmissionHEENT {
    position: absolute;
    top: 1611px;
    left: 148px;
  }
  #PeChest {
    position: absolute;
    top: 14px;
    left: 150px;
  }
  #PeCVS {
    position: absolute;
    top: 98px;
    left: 150px;
  }
  #PeAbdomen {
    position: absolute;
    top: 182px;
    left: 150px;
  }
  #PeGU {
    position: absolute;
    top: 270px;
    left: 150px;
  }
  #PeSkin {
    position: absolute;
    top: 329px;
    left: 150px;
  }
  #PeNeuro {
    position: absolute;
    top: 437px;
    left: 150px;
  }
  #CourseInWard {
    position: absolute;
    top: 595px;
    left: 4px;
  }
    #CourseInWard span {
      display:block;
      height: 25px;
      margin-bottom: 4.3px;
    }
    #CourseInWard span input {
      height:27px;
    }
    #CourseInWard span input:nth-child(1) {
      width: 166px;
      margin-right: 2px;
      top: -2px;
      position: relative;
    }
    #CourseInWard span input:nth-child(2) {
      width: 747px;
      position: relative;
      top: 0px;
    }
  #SurgicalProcedure {
    position: absolute;
    top: 1033px;
    left: 386px;
  }
    #SurgicalProcedure input {
      width: 533px;
      height: 30px;
    }
  #DrugsMeds {
    position: absolute;
    top: 1124px;
    left: 3px;
  }
    #DrugsMeds span {
      padding:3px 0 0;
      display:block;
    }
    #DrugsMeds span input {
      height:24px;
      width: 145px;
      margin-right:2px;
    }
    #DrugsMeds span input:nth-child(2) {
      width: 208px;
    }
    #DrugsMeds span input:nth-child(3) {
      width: 94px;
    }
    #DrugsMeds span input:nth-child(4) {
      width: 148px;
    }
    #DrugsMeds span input:nth-child(5) {
      width: 208px;
    }
    #DrugsMeds span input:nth-child(6) {
      width: 102px;
    }
  #Outcome {
    position: absolute;
    top: 1351px;
    left: 18px;
  }

  @-moz-document url-prefix() {
    #pSignsSymptoms input.opt-input, #PeAdmissionHEENT input.opt-input {
      margin-bottom: 0px;
    }
    #pSignsSymptoms {
      position: absolute;
      top: 1102px;
      left: 14px;
    }
    #Referred {
      position: absolute;
      top: 1424px;
      left: 329px;
    }
    #CourseInWard span input:nth-child(1) {
      width: 166px;
      margin-right: 2px;
      top: -2px;
      position: relative;
    }
    #CourseInWard span input:nth-child(2) {
      width: 747px;
      position: relative;
      top: -5px;
    }
  }
    
  @media print {
    @page {
      size: 8.5in 14in;
      margin: 0in;
    }
    @if(isset($page))
      #formcf41 {
        margin-top: 24px;
        position: relative;
        margin-left: 14px;
      }
      #formcf42 {
        margin-top: 25px;
        position: relative;
        margin-left: 15px;
      }
    @endif
    .formcf4 {
      width: 990px !important;
    } 
    

    input.box-input {
      margin-left: 0.1em;
    }
    spacer {
      width: 15px;
    }
  }
</style>

  <form id="form_eclaims" method="post" action="{{ url('eclaims/saveform') }}">
    <div class="container-fluid pad20 formcf4 formcf41">
      <div class="roww pad20"> 
        <div id="formcf41">
          <input type="hidden" name="submitCF4" value="0">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="form" value="cf4">
          <input type="hidden" name="eclaims_id" value="{{ $eclaims_id }}">
          <input type="hidden" name="pEffYear" value="{{ isset($cf4Form->pEffYear) ? $cf4Form->pEffYear : NULL }}">
          <input type="hidden" name="pHciTransNo" value="{{ isset($cf4Form->pHciTransNo) ? $cf4Form->pHciTransNo : NULL }}">
          <input type="hidden" name="pHciCaseNo" value="{{ isset($cf4Form->pHciCaseNo) ? $cf4Form->pHciCaseNo : NULL }}">

          @if(isset($cf4->cf4))
          <input type="hidden" name="cf4" value="{{ $cf4->cf4 }}">
          @endif

          <div id="HCIName" class="formgroup">
            <input type="text" class="rect-input" name="hci_name" value="{{ $cf4Form->hci_name ? $cf4Form->hci_name : '' }}" />
          </div>
          <div id="PAN" class="formgroup">
            <input type="text" class="rect-input pin-number" name="pHciAccreNo" value="{{ $cf4Form->pHciAccreNo ? $cf4Form->pHciAccreNo : '' }}" />
          </div>
          <div id="HCIAddress" class="formgroup">
            <input type="text" class="rect-input" name="hci_bldg" value="{{ $cf4Form->hci_bldg ? $cf4Form->hci_bldg : '' }}" /><input type="text" class="rect-input" name="hci_street" value="{{ $cf4Form->hci_street ? $cf4Form->hci_street : '' }}" /><input type="text" class="rect-input" name="pPatientAddbrgy" value="{{ isset($cf4Form->pPatientAddbrgy) ? $cf4Form->pPatientAddbrgy : '' }}" /><input type="text" class="rect-input" name="pPatientAddmun" value="{{ isset($cf4Form->pPatientAddmun) ? $cf4Form->pPatientAddmun : '' }}" /><input type="text" class="rect-input" name="pPatientAddprov" value="{{ isset($cf4Form->pPatientAddprov) ? $cf4Form->pPatientAddprov : '' }}" /><input type="text" class="rect-input" name="pPatientAddzipcode" value="{{ isset($cf4Form->pPatientAddzipcode) ? $cf4Form->pPatientAddzipcode : '' }}" />
          </div>
          <div id="PatName" class="formgroup">
            <input type="text" class="rect-input" name="pPatientLname" value="{{ isset($cf4Form->pPatientLname) ? $cf4Form->pPatientLname : '' }}" /><input type="text" class="rect-input" name="pPatientFname" value="{{ isset($cf4Form->pPatientFname) ? $cf4Form->pPatientFname : '' }}" /><input type="text" class="rect-input" name="pPatientMname" value="{{ isset($cf4Form->pPatientMname) ? $cf4Form->pPatientMname : '' }}" />
          </div>
          <div id="PatPIN" class="formgroup">
            <input type="text" class="rect-input pin-number masked" name="pPatientPin" value="{{ $cf4Form->pPatientPin ? $cf4Form->pPatientPin : '' }}" data-mask='' data-inputmask="'mask': '99-999999999-9'" />
          </div>
          <div id="PatAge" class="formgroup">
            <input type="text" class="rect-input" name="patAge" value="{{ $cf4Form->patAge ? $cf4Form->patAge : '' }}" />
          </div>
          <div id="Compliant" class="formgroup">
            <textarea type="text" class="rect-input" name="pChiefComplaint">{{ $cf4Form->pChiefComplaint ? $cf4Form->pChiefComplaint : '' }}</textarea>
          </div>
          <div id="PatGender" class="formgroup">
            <input type="radio" class="opt-input" name="pPatientSex" @if(isset($cf4Form->pPatientSex) AND $cf4Form->pPatientSex == 'M') checked @endif value="M" />
            <input type="radio" class="opt-input" name="pPatientSex" @if(isset($cf4Form->pPatientSex) AND $cf4Form->pPatientSex == 'F') checked @endif value="F" />
          </div>
          <div id="AdmitDiagnosis" class="formgroup">
            <textarea type="text" class="rect-input" name="pIcdCode">{{ isset($cf4Form->pAdmissionDiagnosis) ? $cf4Form->pAdmissionDiagnosis : '' }}</textarea>
          </div>
          <div id="DischDiagnosis" class="formgroup">
            <textarea type="text" class="rect-input" name="pIcdCode">{{ $cf4Form->pIcdCode ? $cf4Form->pIcdCode : '' }}</textarea>
          </div>
          <div id="firstCaseRate" class="formgroup">
            <input type="text" class="rect-input" name="pFirstCaseRate" value="{{ $cf4Form->pFirstCaseRate ? $cf4Form->pFirstCaseRate : '' }}" />
          </div>
          <div id="secondCaseRate" class="formgroup">
            <input type="text" class="rect-input" name="pSecondCaseRate" value="{{ $cf4Form->pSecondCaseRate ? $cf4Form->pSecondCaseRate : '' }}" />
          </div>
          <div id="Admitted" class="formgroup">
            <input type="hidden" name="pAdmissionDate" value="{{ isset($cf4Form->pAdmissionDate) ? $cf4Form->pAdmissionDate : NULL }}" />
            <input type="text" class="box-rect" name="ad_mon" id="ad_mon" value="{{ $cf4Form->ad_mon ? $cf4Form->ad_mon : '' }}" /><spacer></spacer><input type="text" class="box-rect" name="ad_dd" id="ad_dd" value="{{ $cf4Form->ad_dd ? $cf4Form->ad_dd : '' }}" /><spacer></spacer><input type="text" class="box-rect yearbox" name="ad_yy" id="ad_yy" value="{{ $cf4Form->ad_yy ? $cf4Form->ad_yy : '' }}" />
          </div>
          <div id="admittedTime" class="formgroup">
            <input type="text" class="box-rect" name="ad_hh" value="{{ $cf4Form->ad_hh ? $cf4Form->ad_hh : '' }}" /><spacer></spacer><input type="text" class="box-rect" name="ad_min" value="{{ $cf4Form->ad_min ? $cf4Form->ad_min : '' }}" /><spacer style="margin: 0 0 0 -4px;"></spacer><input type="radio" class="opt-input" value="AM" name="ad_ampm" @if(isset($cf4Form->ad_ampm) AND $cf4Form->ad_ampm == "AM") checked @endif /><spacer style="width: 23px !important;"></spacer><input type="radio" class="opt-input" value="PM" name="ad_ampm" @if(isset($cf4Form->ad_ampm) AND $cf4Form->ad_ampm == "PM") checked @endif />
          </div>
          <div id="Discharge" class="formgroup">
            <input type="hidden" name="pDischargeDate" value="{{ isset($cf4Form->DischargeDate) ? $cf4Form->DischargeDate : NULL }}" />
            <input type="text" class="box-rect" name="dc_mon" id="dc_mon" value="{{ $cf4Form->dc_mon ? $cf4Form->dc_mon : '' }}" /><spacer></spacer><input type="text" class="box-rect" name="dc_dd" id="dc_dd" value="{{ $cf4Form->dc_dd ? $cf4Form->dc_dd : '' }}" /><spacer></spacer><input type="text" class="box-rect yearbox" name="dc_yy" id="dc_yy" value="{{ $cf4Form->dc_yy ? $cf4Form->dc_yy : '' }}" />
          </div>
          <div id="dischargeTime" class="formgroup">
            <input type="text" class="box-rect" name="dc_hh" value="{{ $cf4Form->dc_hh ? $cf4Form->dc_hh : '' }}" /><spacer></spacer><input type="text" class="box-rect" name="dc_min" value="{{ $cf4Form->dc_min ? $cf4Form->dc_min : '' }}" /><spacer style="margin: 0 0 0 -4px;"></spacer><input type="radio" class="opt-input" value="AM" name="dc_ampm" @if(isset($cf4Form->dc_ampm) AND $cf4Form->dc_ampm == "AM") checked @endif /><spacer style="width: 23px !important;"></spacer><input type="radio" class="opt-input" value="PM" name="dc_ampm" @if(isset($cf4Form->dc_ampm) AND $cf4Form->dc_ampm == "PM") checked @endif />
          </div>

          <div id="HistoryOfIllness" class="formgroup">
            <textarea type="text" class="rect-input" name="pIllnessHistory">{{ $cf4Form->pIllnessHistory ? $cf4Form->pIllnessHistory : '' }}</textarea>
          </div>
          <div id="PastMedicalHistory" class="formgroup">
            <textarea type="text" class="rect-input" name="pSpecificDesc">{{ isset($cf4Form->pSpecificDesc) ? $cf4Form->pSpecificDesc : '' }}</textarea>
          </div>

          <div id="ObGynHistory" class="formgroup">
            <input type="text" class="box-rect" name="pObstetricG" value="{{ $cf4Form->pObstetricG ? $cf4Form->pObstetricG : '' }}" /><spacer style="width: 21px;"></spacer><input type="text" class="box-rect" name="pObstetricP" value="{{ $cf4Form->pObstetricP ? $cf4Form->pObstetricP : '' }}" /><spacer style="width: 19px;"></spacer><input type="text" class="box-rect" name="pObstetric_T" value="{{ $cf4Form->pObstetric_T ? $cf4Form->pObstetric_T : '' }}" /><spacer style="width: 15px;"></spacer><input type="text" class="box-rect" name="pObstetric_P" value="{{ $cf4Form->pObstetric_P ? $cf4Form->pObstetric_P : '' }}" /><spacer style="width: 17px;"></spacer><input type="text" class="box-rect" name="pObstetric_A" value="{{ $cf4Form->pObstetric_A ? $cf4Form->pObstetric_A : '' }}" /><spacer style="width: 16px;"></spacer><input type="text" class="box-rect" name="pObstetric_L" value="{{ $cf4Form->pObstetric_L ? $cf4Form->pObstetric_L : '' }}" /><spacer style="width: 47px;"></spacer><input type="date" class="box-rect" name="pObstetric_lmp" value="{{ $cf4Form->pObstetric_lmp ? $cf4Form->pObstetric_lmp : '' }}" style="width: 118px;" /><spacer style="width: 15px;"></spacer><input type="checkbox" class="opt-input" value="Y" name="pObstetric_NA" @if(isset($cf4Form->pObstetric_NA) AND isset($cf4Form->pObstetric_NA) == "1") checked @endif />
          </div>


          <?php if(isset($cf4Form->pSignsSymptoms)) {
            $cf4Form->pSignsSymptoms = (array)$cf4Form->pSignsSymptoms; 
          } ?>
          <div id="pSignsSymptoms" class="formgroup">
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Sensorium']) AND $cf4Form->pSignsSymptoms['Sensorium'] == "1") checked @endif value="1" name="pSignsSymptoms[Sensorium]" style="margin-right: 182px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Diarrhea']) AND $cf4Form->pSignsSymptoms['Diarrhea'] == "10") checked @endif value="10" name="pSignsSymptoms[Diarrhea]" style="margin-right: 170px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Hematemesis']) AND $cf4Form->pSignsSymptoms['Hematemesis'] == "19") checked @endif value="19" name="pSignsSymptoms[Hematemesis]" style="margin-right: 175px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Palpitations']) AND $cf4Form->pSignsSymptoms['Palpitations'] == "28") checked @endif value="28" name="pSignsSymptoms[Palpitations]" />
            <br />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['AbCramps']) AND $cf4Form->pSignsSymptoms['AbCramps'] == "2") checked @endif value="2" name="pSignsSymptoms[AbCramps]" style="margin-right: 182px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Dizziness']) AND $cf4Form->pSignsSymptoms['Dizziness'] == "11") checked @endif value="11" name="pSignsSymptoms[Dizziness]" style="margin-right: 170px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Hemturia']) AND $cf4Form->pSignsSymptoms['Hemturia'] == "20") checked @endif value="20" name="pSignsSymptoms[Hemturia]" style="margin-right: 175px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Seizures']) AND $cf4Form->pSignsSymptoms['Seizures'] == "33") checked @endif value="33" name="pSignsSymptoms[Seizures]" />
            <br />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Anorexia']) AND $cf4Form->pSignsSymptoms['Anorexia'] == "3") checked @endif value="3" name="pSignsSymptoms[Anorexia]" style="margin-right: 182px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Dysphagia']) AND $cf4Form->pSignsSymptoms['Dysphagia'] == "12") checked @endif value="12" name="pSignsSymptoms[Dysphagia]" style="margin-right: 170px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Hemoptysis']) AND $cf4Form->pSignsSymptoms['Hemoptysis'] == "21") checked @endif value="21" name="pSignsSymptoms[Hemoptysis]" style="margin-right: 175px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['SkinRashes']) AND $cf4Form->pSignsSymptoms['SkinRashes'] == "29") checked @endif value="29" name="pSignsSymptoms[SkinRashes]" />
            <br />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['BleedingGums']) AND $cf4Form->pSignsSymptoms['BleedingGums'] == "4") checked @endif value="4" name="pSignsSymptoms[BleedingGums]" style="margin-right: 182px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Dyspnea']) AND $cf4Form->pSignsSymptoms['Dyspnea'] == "13") checked @endif value="13" name="pSignsSymptoms[Dyspnea]" style="margin-right: 170px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Irritability']) AND $cf4Form->pSignsSymptoms['Irritability'] == "22") checked @endif value="22" name="pSignsSymptoms[Irritability]" style="margin-right: 175px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['BloodyStool']) AND $cf4Form->pSignsSymptoms['BloodyStool'] == "30") checked @endif value="30" name="pSignsSymptoms[BloodyStool]" />
            <br />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['BodyWeakness']) AND $cf4Form->pSignsSymptoms['BodyWeakness'] == "5") checked @endif value="5" name="pSignsSymptoms[BodyWeakness]" style="margin-right: 182px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Dysuria']) AND $cf4Form->pSignsSymptoms['Dysuria'] == "14") checked @endif value="14" name="pSignsSymptoms[Dysuria]" style="margin-right: 170px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Jaundice']) AND $cf4Form->pSignsSymptoms['Jaundice'] == "23") checked @endif value="23" name="pSignsSymptoms[Jaundice]" style="margin-right: 175px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Sweating']) AND $cf4Form->pSignsSymptoms['Sweating'] == "32") checked @endif value="32" name="pSignsSymptoms[Sweating]" />
            <br />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['BlurryVision']) AND $cf4Form->pSignsSymptoms['BlurryVision'] == "6") checked @endif value="6" name="pSignsSymptoms[BlurryVision]" style="margin-right: 182px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Epistaxis']) AND $cf4Form->pSignsSymptoms['Epistaxis'] == "15") checked @endif value="15" name="pSignsSymptoms[Epistaxis]" style="margin-right: 170px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['LowerEdema']) AND $cf4Form->pSignsSymptoms['LowerEdema'] == "25") checked @endif value="25" name="pSignsSymptoms[LowerEdema]" style="margin-right: 175px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Urgency']) AND $cf4Form->pSignsSymptoms['Urgency'] == "34") checked @endif value="34" name="pSignsSymptoms[Urgency]" />
            <br />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['ChestPain']) AND $cf4Form->pSignsSymptoms['ChestPain'] == "7") checked @endif value="7" name="pSignsSymptoms[ChestPain]" style="margin-right: 182px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Fever']) AND $cf4Form->pSignsSymptoms['Fever'] == "37") checked @endif value="37" name="pSignsSymptoms[Fever]" style="margin-right: 170px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Myaglia']) AND $cf4Form->pSignsSymptoms['Myaglia'] == "26") checked @endif value="26" name="pSignsSymptoms[Myaglia]" style="margin-right: 175px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Vomiting']) AND $cf4Form->pSignsSymptoms['Vomiting'] == "35") checked @endif value="35" name="pSignsSymptoms[Vomiting]" />
            <br />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Constipation']) AND $cf4Form->pSignsSymptoms['Constipation'] == "8") checked @endif value="8" name="pSignsSymptoms[Constipation]" style="margin-right: 182px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['FrequentUrination']) AND $cf4Form->pSignsSymptoms['FrequentUrination'] == "17") checked @endif value="17" name="pSignsSymptoms[FrequentUrination]" style="margin-right: 170px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Orthopnea']) AND $cf4Form->pSignsSymptoms['Orthopnea'] == "27") checked @endif value="27" name="pSignsSymptoms[Orthopnea]" style="margin-right: 175px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['WeightLoss']) AND $cf4Form->pSignsSymptoms['WeightLoss'] == "36") checked @endif value="36" name="pSignsSymptoms[WeightLoss]" />
            <br />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Cough']) AND $cf4Form->pSignsSymptoms['Cough'] == "9") checked @endif value="9" name="pSignsSymptoms[Cough]" style="margin-right: 182px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Headache']) AND $cf4Form->pSignsSymptoms['Headache'] == "18") checked @endif value="18" name="pSignsSymptoms[Headache]" style="margin-right: 170px;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Pain']) AND $cf4Form->pSignsSymptoms['Pain'] == "38") checked @endif value="38" name="pSignsSymptoms[Pain]" style="margin-right: 30px;" />
            <input type="text" class="box-rect" name="pPainSite" value="{{ $cf4Form->pPainSite ? $cf4Form->pPainSite : '' }}" style="width: 89px;margin: -15px 53px 0 0; vertical-align: text-top;" />
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->pSignsSymptoms['Others']) AND $cf4Form->pSignsSymptoms['Others'] == "X") checked @endif value="X" name="pSignsSymptoms[Others]" />
            <input type="text" class="box-rect" name="pOtherComplaint" value="{{ $cf4Form->pOtherComplaint ? $cf4Form->pOtherComplaint : '' }}" style="width: 217px;margin-left: 48px;" />
            <br />
          </div>

          <div id="Referred" class="formgroup">
            <input type="radio" class="opt-input" value="Y" name="was_referred" @if(isset($cf4Form->was_referred) AND $cf4Form->was_referred == 'Y') checked @endif /><spacer style="width: 28px !important;"></spacer><input type="radio" class="opt-input" value="N" name="was_referred" @if(isset($cf4Form->was_referred) AND $cf4Form->was_referred == 'N') checked @endif /><spacer style="width: 105px !important;"></spacer><input type="text" class="box-rect" name="YesReason" value="{{ isset($cf4Form->YesReason) ? $cf4Form->YesReason : '' }}" style="width: 379px;" />
            <br>
            <spacer style="width: 218px !important;"></spacer><input type="text" class="box-rect" name="OrigHCI" value="{{ isset($cf4Form->NameOrigHCI) ? $cf4Form->NameOrigHCI : '' }}" style="width: 357px;margin-top: 13px;" />
          </div>

          <div id="PeAdmissionGS" class="formgroup">
            <input type="radio" class="opt-input" value="1" name="GS" @if(isset($cf4Form->GS) AND $cf4Form->GS == "1") checked @endif  /><spacer style="width: 158px !important;"></spacer><input type="radio" class="opt-input" @if(isset($cf4Form->GS) AND $cf4Form->GS == "2") checked @endif value="2" name="GS" /><spacer style="width: 116px !important;"></spacer><input type="text" class="box-rect" name="GSAltered" style="width: 217px;" value="{{ $cf4Form->GSAltered ? $cf4Form->GSAltered : '' }}" />
          </div>
          <div id="PeAdmissionVS" class="formgroup">
            <input type="text" class="box-rect" name="VSBP" placeholder="SBP/DBP" style="width: 117px;" value="{{ $cf4Form->VSBP ? $cf4Form->VSBP : '' }}" /><spacer style="width: 64px !important;"></spacer><input type="text" class="box-rect" name="VSHR" style="width: 117px;" value="{{ $cf4Form->VSHR ? $cf4Form->VSHR : '' }}" /><spacer style="width: 36px !important;"></spacer><input type="text" class="box-rect" name="VSRR" style="width: 117px;" value="{{ $cf4Form->VSRR ? $cf4Form->VSRR : '' }}" /><spacer style="width: 64px !important;"></spacer><input type="text" class="box-rect" name="VSTemp" style="width: 117px;" value="{{ $cf4Form->VSTemp ? $cf4Form->VSTemp : '' }}" />
          </div>
          <div id="PeAdmissionHEENT" class="formgroup">
            <?php $cf4Form->HEENT = array($cf4Form->HEENT); ?>
            <input type="checkbox" class="opt-input" value="11" name="HEENT[Normal]" @if(isset($cf4Form->HEENT[0]->Normal) AND $cf4Form->HEENT[0]->Normal == "11") checked @endif /><spacer style="width: 159px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->HEENT[0]->AbnPupillary) AND $cf4Form->HEENT[0]->AbnPupillary == "12") checked @endif value="12" name="HEENT[AbnPupillary]" /><spacer style="width: 176px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->HEENT[0]->Lymphade) AND $cf4Form->HEENT[0]->Lymphade == "13") checked @endif value="13" name="HEENT[Lymphade]" /><spacer style="width: 161px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->HEENT[0]->DryMucus) AND $cf4Form->HEENT[0]->DryMucus == "14") checked @endif value="14" name="HEENT[DryMucus]" />
            <br style="margin: 6px 0 5px;" />
            <input type="checkbox" class="opt-input" value="15" name="HEENT[Icteric]" @if(isset($cf4Form->HEENT[0]->Icteric) AND $cf4Form->HEENT[0]->Icteric == "15") checked @endif /><spacer style="width: 159px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->HEENT[0]->Pale) AND $cf4Form->HEENT[0]->Pale == "16") checked @endif value="16" name="HEENT[Pale]" /><spacer style="width: 176px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->HEENT[0]->SunkenEyeballs) AND $cf4Form->HEENT[0]->SunkenEyeballs == "17") checked @endif value="17" name="HEENT[SunkenEyeballs]" /><spacer style="width: 161px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->HEENT[0]->Fontanelle) AND $cf4Form->HEENT[0]->Fontanelle == "18") checked @endif value="18" name="HEENT[Fontanelle]" />
            <br />
            <input type="text" class="box-rect" name="HEENT[pHeentRem]" value="{{ isset($cf4Form->HEENT[0]->pHeentRem) ? $cf4Form->HEENT[0]->pHeentRem : (isset($care->Examination[0]) ? $care->Examination[0]->heent_others : '') }}" style="width: 213px;margin: 3px 0 0 52px;" />
          </div>
        </div>
      </div>
    </div>
    
    <div class="container-fluid pad20 formcf4 formcf42">
      <div class="roww pad20"> 
        <div id="formcf42">
          <div id="PeChest" class="formgroup">
            <?php $cf4Form->PeChest = array($cf4Form->PeChest); ?>
            <input type="checkbox" class="opt-input" value="6" name="PeChest[EssentialNormal]" @if(isset($cf4Form->PeChest[0]->EssentialNormal) AND $cf4Form->PeChest[0]->EssentialNormal == "6") checked @endif /><spacer style="width: 158px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeChest[0]->AsymmetricalChest) AND $cf4Form->PeChest[0]->AsymmetricalChest == "7") checked @endif value="7" name="PeChest[AsymmetricalChest]" /><spacer style="width: 175px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeChest[0]->DecreasedBreath) AND $cf4Form->PeChest[0]->DecreasedBreath == "8") checked @endif value="8" name="PeChest[DecreasedBreath]" /><spacer style="width: 160px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeChest[0]->Wheezes) AND $cf4Form->PeChest[0]->Wheezes == "5") checked @endif value="5" name="PeChest[Wheezes]" />
            <br style="margin: 5px 0 2px;" />
            <input type="checkbox" class="opt-input" value="10" name="PeChest[LumpsOverBreast]" @if(isset($cf4Form->PeChest[0]->LumpsOverBreast) AND $cf4Form->PeChest[0]->LumpsOverBreast == "10") checked @endif /><spacer style="width: 158px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeChest[0]->RalesCrackles) AND $cf4Form->PeChest[0]->RalesCrackles == "4") checked @endif value="4" name="PeChest[RalesCrackles]" /><spacer style="width: 175px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeChest[0]->IntercostalRibs) AND $cf4Form->PeChest[0]->IntercostalRibs == "1") checked @endif value="1" name="PeChest[IntercostalRibs]" />
            <br />
            <input type="text" class="box-rect" name="PeChest[pChestRem]" value="{{ isset($cf4Form->PeChest[0]->pChestRem) ? $cf4Form->PeChest[0]->pChestRem : (isset($care->Examination[0]) ? $care->Examination[0]->chest_others : '') }}" style="width: 213px;margin: 3px 0 0 52px;" />
          </div>

          <div id="PeCVS" class="formgroup">
            <?php $cf4Form->PeCVS = array($cf4Form->PeCVS); ?>
            <input type="checkbox" class="opt-input" value="5" name="PeCVS[CVSEssentialNormal]" @if(isset($cf4Form->PeCVS[0]->CVSEssentialNormal) AND $cf4Form->PeCVS[0]->CVSEssentialNormal) AND $cf4Form->PeCVS[0]->CVSEssentialNormal == "5") checked @endif /><spacer style="width: 158px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeCVS[0]->DisplacedApex) AND $cf4Form->PeCVS[0]->DisplacedApex) AND $cf4Form->PeCVS[0]->DisplacedApex == "6") checked @endif value="6" name="PeCVS[DisplacedApex]" /><spacer style="width: 175px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeCVS[0]->Heaves) AND $cf4Form->PeCVS[0]->Heaves AND $cf4Form->PeCVS[0]->Heaves == "3") checked @endif value="3" name="PeCVS[Heaves]" /><spacer style="width: 160px !important;"></spacer><input type="checkbox" class="opt-input"  @if( (isset($cf4Form->PeCVS[0]->PericardialBulge) AND $cf4Form->PeCVS[0]->PericardialBulge) AND $cf4Form->PeCVS[0]->PericardialBulge == "9") checked @endif value="9" name="PeCVS[PericardialBulge]" />
            <br style="margin: 5px 0 2px;" />
            <input type="checkbox" class="opt-input" value="7" name="PeCVS[IrregularRhythm]" @if(isset($cf4Form->PeCVS[0]->IrregularRhythm) AND $cf4Form->PeCVS[0]->IrregularRhythm) AND $cf4Form->PeCVS[0]->IrregularRhythm == "7") checked @endif /><spacer style="width: 158px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeCVS[0]->MuffledHeart) AND $cf4Form->PeCVS[0]->MuffledHeart) AND $cf4Form->PeCVS[0]->MuffledHeart == "8") checked @endif value="8" name="PeCVS[MuffledHeart]" /><spacer style="width: 175px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeCVS[0]->Murmur) AND $cf4Form->PeCVS[0]->Murmur) AND $cf4Form->PeCVS[0]->Murmur == "4") checked @endif value="4" name="PeCVS[Murmur]" />
            <br />
            <input type="text" class="box-rect" name="PeCVS[pHeartRem]" value="{{ isset($cf4Form->PeCVS[0]->pHeartRem) ? $cf4Form->PeCVS[0]->pHeartRem : (isset($care->Examination[0]) ? $care->Examination[0]->heart_others : '') }}" style="width: 213px;margin: 3px 0 0 52px;" />
          </div>

          <div id="PeAbdomen" class="formgroup">
            <?php $cf4Form->PeAbdomen = array($cf4Form->PeAbdomen); ?>
            <input type="checkbox" class="opt-input" value="7" name="PeAbdomen[AbdomenEssentialNormal]" @if(isset($cf4Form->PeAbdomen[0]->AbdomenEssentialNormal) AND $cf4Form->PeAbdomen[0]->AbdomenEssentialNormal == "7") checked @endif /><spacer style="width: 158px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeAbdomen[0]->AbdomenalRigid) AND $cf4Form->PeAbdomen[0]->AbdomenalRigid == "8") checked @endif value="8" name="PeAbdomen[AbdomenalRigid]" /><spacer style="width: 175px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeAbdomen[0]->AbdomenalTender) AND $cf4Form->PeAbdomen[0]->AbdomenalTender == "9") checked @endif value="9" name="PeAbdomen[AbdomenalTender]" /><spacer style="width: 160px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeAbdomen[0]->HyperactiveBowel) AND $cf4Form->PeAbdomen[0]->HyperactiveBowel == "10") checked @endif value="10" name="PeAbdomen[HyperactiveBowel]" />
            <br style="margin: 5px 0 2px;" />
            <input type="checkbox" class="opt-input" value="11" @if(isset($cf4Form->PeAbdomen[0]->PalpableMass) AND $cf4Form->PeAbdomen[0]->PalpableMass == "11") checked @endif name="PeAbdomen[PalpableMass]" /><spacer style="width: 158px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeAbdomen[0]->Tympanitic) AND $cf4Form->PeAbdomen[0]->Tympanitic == "12") checked @endif value="12" name="PeAbdomen[Tympanitic]" /><spacer style="width: 175px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeAbdomen[0]->Uterine) AND $cf4Form->PeAbdomen[0]->Uterine == "13") checked @endif value="13" name="PeAbdomen[Uterine]" />
            <br />
            <input type="text" class="box-rect" name="PeAbdomen[pAbdomenRem]" value="{{ isset($cf4Form->PeAbdomen[0]->pAbdomenRem) ? $cf4Form->PeAbdomen[0]->pAbdomenRem : (isset($care->Examination[0]) ? $care->Examination[0]->abdomen_others : '') }}" style="width: 213px;margin: 5px 0 0 52px;" />
          </div>

          <div id="PeGU" class="formgroup">
            <?php $cf4Form->PeGU = array($cf4Form->PeGU); ?>
            <input type="checkbox" class="opt-input" value="1" name="PeGU[GUEssentialNormal]" @if(isset($cf4Form->PeGU[0]->GUEssentialNormal) AND $cf4Form->PeGU[0]->GUEssentialNormal == "1") checked @endif /><spacer style="width: 158px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeGU[0]->BloodStain) AND $cf4Form->PeGU[0]->BloodStain == "2") checked @endif value="2" name="PeGU[BloodStain]" /><spacer style="width: 175px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeGU[0]->CervicalDilation) AND $cf4Form->PeGU[0]->CervicalDilation == "3") checked @endif value="3" name="PeGU[CervicalDilation]" /><spacer style="width: 160px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeGU[0]->AbnormalDischarge) AND $cf4Form->PeGU[0]->AbnormalDischarge == "4") checked @endif value="4" name="PeGU[AbnormalDischarge]" />
            <br />
            <input type="text" class="box-rect" name="PeGU[pGuRem]" value="{{ isset($cf4Form->PeGU[0]->pGuRem) ? $cf4Form->PeGU[0]->pGuRem : '' }}" style="width: 213px;margin: 4px 0 0 52px;" />
          </div>

          <div id="PeSkin" class="formgroup">
            <?php $cf4Form->PeSkin = array($cf4Form->PeSkin); ?>
            <input type="checkbox" class="opt-input" value="1" name="PeSkin[SkinEssentialNormal]" @if(isset($cf4Form->PeSkin[0]->SkinEssentialNormal) AND $cf4Form->PeSkin[0]->SkinEssentialNormal == "1") checked @endif /><spacer style="width: 158px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeSkin[0]->Clubbing) AND $cf4Form->PeSkin[0]->Clubbing == "2") checked @endif value="2" name="PeSkin[Clubbing]" /><spacer style="width: 175px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeSkin[0]->ClammySkin) AND $cf4Form->PeSkin[0]->ClammySkin == "3") checked @endif value="3" name="PeSkin[ClammySkin]" /><spacer style="width: 160px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeSkin[0]->Cyanosis) AND $cf4Form->PeSkin[0]->Cyanosis == "4") checked @endif value="4" name="PeSkin[Cyanosis]" />
            <br style="margin: 5px 0 2px;" />
            <input type="checkbox" class="opt-input" value="5" name="PeSkin[Edima]" @if(isset($cf4Form->PeSkin[0]->Edima) AND $cf4Form->PeSkin[0]->Edima == "5") checked @endif /><spacer style="width: 158px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeSkin[0]->DecreaseMobility) AND $cf4Form->PeSkin[0]->DecreaseMobility == "6") checked @endif value="6" name="PeSkin[DecreaseMobility]" /><spacer style="width: 175px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeSkin[0]->PaleNailBeds) AND $cf4Form->PeSkin[0]->PaleNailBeds == "7") checked @endif value="7" name="PeSkin[PaleNailBeds]" /><spacer style="width: 160px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeSkin[0]->PoorSkinTurgor) AND $cf4Form->PeSkin[0]->PoorSkinTurgor == "8") checked @endif value="8" name="PeSkin[PoorSkinTurgor]" />
            <br style="margin: 5px 0 2px;" />
            <input type="checkbox" class="opt-input" value="9" name="PeSkin[Rashes]" @if(isset($cf4Form->PeSkin[0]->Rashes) AND $cf4Form->PeSkin[0]->Rashes == "9") checked @endif /><spacer style="width: 158px !important;"></spacer><input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeSkin[0]->WeakPulses) AND $cf4Form->PeSkin[0]->WeakPulses == "10") checked @endif value="10" name="PeSkin[WeakPulses]" />
            <br />
            <input type="text" class="box-rect" name="PeSkin[pSkinRem]" value="{{ isset($cf4Form->PeSkin[0]->pSkinRem) ? $cf4Form->PeSkin[0]->pSkinRem : (isset($care->Examination[0]) ? $care->Examination[0]->skin_others : '') }}" style="width: 213px;margin: 1px 0 0 52px;" />
          </div>

          <div id="PeNeuro" class="formgroup">
            <?php $cf4Form->PeNeuro = array($cf4Form->PeNeuro); ?>
            <input type="checkbox" class="opt-input" value="6" name="PeNeuro[NeuroEssentialNormal]" @if(isset($cf4Form->PeNeuro[0]->NeuroEssentialNormal) AND $cf4Form->PeNeuro[0]->NeuroEssentialNormal == "6") checked @endif />
            <spacer style="width: 151px !important;"></spacer>
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeNeuro[0]->AbnormalGait) AND $cf4Form->PeNeuro[0]->AbnormalGait == "7") checked @endif value="7" name="PeNeuro[AbnormalGait]" /><spacer style="width: 171px !important;"></spacer>
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeNeuro[0]->AbnormalPosition) AND $cf4Form->PeNeuro[0]->AbnormalPosition == "8") checked @endif value="8" name="PeNeuro[AbnormalPosition]" />
            <spacer style="width: 154px !important;"></spacer>
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeNeuro[0]->AbnormalSensation) AND $cf4Form->PeNeuro[0]->AbnormalSensation == "9") checked @endif value="9" name="PeNeuro[AbnormalSensation]" />
            <br style="margin: 5px 0 2px;" />
            <input type="checkbox" class="opt-input" value="10" name="PeNeuro[AbnormalReflexes]" @if(isset($cf4Form->PeNeuro[0]->AbnormalReflexes) AND $cf4Form->PeNeuro[0]->AbnormalReflexes == "10") checked @endif />
            <spacer style="width: 151px !important;"></spacer>
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeNeuro[0]->PoorMemory) AND $cf4Form->PeNeuro[0]->PoorMemory == "11") checked @endif value="11" name="PeNeuro[PoorMemory]" /><spacer style="width: 171px !important;"></spacer>
            <input type="checkbox" class="opt-input"  @if(isset($cf4Form->PeNeuro[0]->PoorMuscleTone) AND $cf4Form->PeNeuro[0]->PoorMuscleTone == "12") checked @endif value="12" name="PeNeuro[PoorMuscleTone]" />
            <spacer style="width: 154px !important;"></spacer>
            <input type="checkbox" class="opt-input" @if(isset($cf4Form->PeNeuro[0]->PoorCoordination) AND $cf4Form->PeNeuro[0]->PoorCoordination == "13") checked @endif value="13" name="PeNeuro[PoorCoordination]" />
            <br />
            <input type="text" class="box-rect" name="PeNeuro[pNeuroRem]" value="{{ isset($cf4Form->PeNeuro[0]->pNeuroRem) ? $cf4Form->PeNeuro[0]->pNeuroRem : '' }}" style="width: 213px;margin: 5px 0 0 52px;" />
          </div>

          <div id="CourseInWard" class="formgroup">
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[0] ? $cf4Form->CourseInWardDate[0] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[0] ? $cf4Form->CourseInWordOrder[0] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[1] ? $cf4Form->CourseInWardDate[1] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[1] ? $cf4Form->CourseInWordOrder[1] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[2] ? $cf4Form->CourseInWardDate[2] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[2] ? $cf4Form->CourseInWordOrder[2] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[3] ? $cf4Form->CourseInWardDate[3] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[3] ? $cf4Form->CourseInWordOrder[3] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[4] ? $cf4Form->CourseInWardDate[4] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[4] ? $cf4Form->CourseInWordOrder[4] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[5] ? $cf4Form->CourseInWardDate[5] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[5] ? $cf4Form->CourseInWordOrder[5] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[6] ? $cf4Form->CourseInWardDate[6] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[6] ? $cf4Form->CourseInWordOrder[6] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[7] ? $cf4Form->CourseInWardDate[7] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[7] ? $cf4Form->CourseInWordOrder[7] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[8] ? $cf4Form->CourseInWardDate[8] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[8] ? $cf4Form->CourseInWordOrder[8] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[9] ? $cf4Form->CourseInWardDate[9] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[9] ? $cf4Form->CourseInWordOrder[9] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[10] ? $cf4Form->CourseInWardDate[10] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[10] ? $cf4Form->CourseInWordOrder[10] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[11] ? $cf4Form->CourseInWardDate[11] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[11] ? $cf4Form->CourseInWordOrder[11] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[12] ? $cf4Form->CourseInWardDate[12] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[12] ? $cf4Form->CourseInWordOrder[12] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[13] ? $cf4Form->CourseInWardDate[13] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[13] ? $cf4Form->CourseInWordOrder[13] : '' }}" /></span>
            <span><input type="date" name="CourseInWardDate[]" value="{{ $cf4Form->CourseInWardDate[14] ? $cf4Form->CourseInWardDate[14] : '' }}" /><input type="text" name="CourseInWordOrder[]" value="{{ $cf4Form->CourseInWordOrder[14] ? $cf4Form->CourseInWordOrder[14] : '' }}" /></span>
          </div>

          <div id="SurgicalProcedure" class="formgroup">
            <input type="text" name="SurgicalProcedureCode" value="{{ isset($cf4Form->SurgicalProcedureCode) ? $cf4Form->SurgicalProcedureCode : '' }}" />
          </div>

          <div id="DrugsMeds" class="formgroup">
            <span style="padding: 2px 0 0;"><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[0]) ? $cf4Form->generic[0] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[0]) ? $cf4Form->qtyDose[0] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[0]) ? $cf4Form->total[0] : '' }}" /><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[1]) ? $cf4Form->generic[1] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[1]) ? $cf4Form->qtyDose[1] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[1]) ? $cf4Form->total[1] : '' }}" /></span>
            <span><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[2]) ? $cf4Form->generic[2] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[2]) ? $cf4Form->qtyDose[2] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[2]) ? $cf4Form->total[2] : '' }}" /><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[3]) ? $cf4Form->generic[3] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[3]) ? $cf4Form->qtyDose[3] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[3]) ? $cf4Form->total[3] : '' }}" /></span>
            <span><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[4]) ? $cf4Form->generic[4] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[4]) ? $cf4Form->qtyDose[4] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[4]) ? $cf4Form->total[4] : '' }}" /><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[5]) ? $cf4Form->generic[5] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[5]) ? $cf4Form->qtyDose[5] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[5]) ? $cf4Form->total[5] : '' }}" /></span>
            <span><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[6]) ? $cf4Form->generic[6] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[6]) ? $cf4Form->qtyDose[6] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[6]) ? $cf4Form->total[6] : '' }}" /><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[7]) ? $cf4Form->generic[7] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[7]) ? $cf4Form->qtyDose[7] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[7]) ? $cf4Form->total[7] : '' }}" /></span>
            <span><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[8]) ? $cf4Form->generic[8] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[8]) ? $cf4Form->qtyDose[8] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[8]) ? $cf4Form->total[8] : '' }}" /><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[9]) ? $cf4Form->generic[9] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[9]) ? $cf4Form->qtyDose[9] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[9]) ? $cf4Form->total[9] : '' }}" /></span>
            <span><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[10]) ? $cf4Form->generic[10] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[10]) ? $cf4Form->qtyDose[10] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[10]) ? $cf4Form->total[10] : '' }}" /><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[11]) ? $cf4Form->generic[11] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[11]) ? $cf4Form->qtyDose[11] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[11]) ? $cf4Form->total[11] : '' }}" /></span>
            <span><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[12]) ? $cf4Form->generic[12] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[12]) ? $cf4Form->qtyDose[12] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[12]) ? $cf4Form->total[12] : '' }}" /><input type="text" class="medicines" name="generic[]" value="{{ isset($cf4Form->generic[13]) ? $cf4Form->generic[13] : '' }}" /><input type="text" name="qtyDose[]" value="{{ isset($cf4Form->qtyDose[13]) ? $cf4Form->qtyDose[13] : '' }}" /><input type="text" name="total[]" value="{{ isset($cf4Form->total[13]) ? $cf4Form->total[13] : '' }}" /></span>
          </div>

          <div id="Outcome" class="formgroup">
            <input type="radio" class="opt-input" value="Improved" name="OutcomeOpt" @if(isset($cf4Form->OutcomeOpt) AND $cf4Form->OutcomeOpt == "Improved") checked @endif /><spacer style="width: 92px !important;"></spacer><input type="radio" class="opt-input"  @if(isset($cf4Form->OutcomeOpt) AND $cf4Form->OutcomeOpt == "HAMA") checked @endif value="HAMA" name="OutcomeOpt" /><spacer style="width: 83px !important;"></spacer><input type="radio" class="opt-input"  @if(isset($cf4Form->OutcomeOpt) AND $cf4Form->OutcomeOpt == "Expire") checked @endif value="Expire" name="" /><spacer style="width: 97px !important;"></spacer><input type="radio" class="opt-input"  @if(isset($cf4Form->OutcomeOpt) AND $cf4Form->OutcomeOpt == "Absconded") checked @endif value="Absconded" name="OutcomeOpt" /><spacer style="width: 112px !important;"></spacer><input type="radio" class="opt-input"  @if(isset($cf4Form->OutcomeOpt) AND $cf4Form->OutcomeOpt == "Transferred") checked @endif value="Transferred" name="OutcomeOpt" /><spacer style="width: 178px !important;"></spacer><input type="text" class="rect-input" name="ReasOutcomeOptReasonon" style="width: 160px;margin-top: -16px;" value="{{ isset($cf4Form->OutcomeOptReason) ? $cf4Form->OutcomeOptReason : ''  }}" />
          </div>

        </div>
      </div>
    </div>
  </form>
</div>

<script>
  $(".masked").inputmask();

  @if(isset($cf4->cf4_xmlarray))
  $(document).ready(function() {
    $("button.genButton").addClass('hidden');
    $("button.reGenButton").removeClass('hidden');
  });
  @else
  $(document).ready(function() {
    $("button.genButton").removeClass('hidden');
    $("button.reGenButton").addClass('hidden');
  });
  @endif

  var medicines_arr = <?php echo json_encode($medicines); ?>;
  console.log()
  $(".medicines" ).autocomplete({
    source: medicines_arr
  });
</script>
