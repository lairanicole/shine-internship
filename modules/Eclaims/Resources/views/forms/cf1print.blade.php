<div id="printFormCF1">
{!! HTML::style('public/dist/css/bootstrap.min.css') !!}
{!! HTML::style('public/dist/css/ShineOS.css') !!}
<style>
  body {
    background-color:#FFF;
    padding:15px;
  }
  .formcf1 {
    background: #FFF url('{!! asset("modules/Eclaims/Assets/images/cf1.png") !!}') center top no-repeat !important;
    background-size: cover !important;
    padding:0;
    margin:0;
    height:1700px;
    width:960px;
  }
  .formgroup {
    position: absolute;
  }
  .formcf1 span {
    background-color: rgba(200,200,200,.2);
    border: none;
    display: inline-block;
    height: 23px;
    padding: 1px 5px;
    max-width: 900px;
  }
  span.box-input {
    width: 20px;
    text-align:center;
    vertical-align: middle;
    margin-left: 0.09em;
  }
  span.rect-input {
    width: 145px;
    vertical-align: middle;
    margin-left: 0.09em;
  }
  span.opt-input {
    width: 23px;
    margin: 2px 0 0;
  }
  spacer {
    width: 13px;
    display: inline-block;
  }
  spacer.lab {
    width: 41px;
    display: inline-block;
  }
  .w165 {
    width: 165px !important;
  }
  .w158 {
    width: 158px !important;
  }
  .w115 {
    width: 115px !important;
  }
  .w70 {
    width: 70px !important;
  }
  .w50 {
    width: 52px !important;
  }
</style>

<div class="container-fluid pad20 formcf1">
    <div class="roww pad20">
      <div id="Pin" class="formgroup" style="top:308px; left:351px;">
        <?php
        $phic = NULL;
        if($philhealth AND $philhealth->philhealth_id != NULL) {
          $phic = str_split($philhealth->philhealth_id);
        } ?>
        <span contenteditable="true" class="box-input">{{ $phic[0] }}</span><span contenteditable="true" class="box-input">{{ $phic[1] }}</span><spacer></spacer><span contenteditable="true" class="box-input">{{ $phic[2] }}</span><span contenteditable="true" class="box-input">{{ $phic[3] }}</span><span contenteditable="true" class="box-input">{{ $phic[4] }}</span><span contenteditable="true" class="box-input">{{ $phic[5] }}</span><span contenteditable="true" class="box-input">{{ $phic[6] }}</span><span contenteditable="true" class="box-input">{{ $phic[7] }}</span><span contenteditable="true" class="box-input">{{ $phic[8] }}</span><span contenteditable="true" class="box-input">{{ $phic[9] }}</span><span contenteditable="true" class="box-input">{{ $phic[10] }}</span><spacer></spacer><span contenteditable="true" class="box-input">{{ $phic[11] }}</span>
      </div>
      
      <div id="Birthdate" class="formgroup" style="top:342px; left:738px;">
        <?php
          $birth = str_split($patient->birthdate);
        ?>
        <span contenteditable="true" class="box-input">{{ $birth[5] }}</span><span contenteditable="true" class="box-input">{{ $birth[6] }}</span><spacer></spacer><span contenteditable="true" class="box-input">{{ $birth[8] }}</span><span contenteditable="true" class="box-input">{{ $birth[9] }}</span><spacer></spacer><span contenteditable="true" class="box-input">{{ $birth[0] }}</span><span contenteditable="true" class="box-input">{{ $birth[1] }}</span><span contenteditable="true" class="box-input">{{ $birth[2] }}</span><span contenteditable="true" class="box-input">{{ $birth[3] }}</span>
      </div>
      
      <div id="Name" class="formgroup" style="top:366px; left:54px;">
      <span contenteditable="true" type="text" class="rect-input" name="lastname">{{ $patient->last_name }}</span><span contenteditable="true" type="text" class="rect-input" name="firstname">{{ $patient->first_name }}</span><span contenteditable="true" type="text" class="rect-input" name="suffix">{{ $patient->name_suffix }}</span><span contenteditable="true" type="text" class="rect-input" name="middlename">{{ $patient->middle_name }}</span>
      </div>

      <div id="Gender" class="formgroup" style="top:406px; left:691px;">
      <span contenteditable="true" type="radio" class="opt-input" name="sex">@if($patient->gender == 'M') X @else &nbsp; @endif</span><spacer class="lab"></spacer><span contenteditable="true" type="radio" class="opt-input" name="sex">@if($patient->gender == 'F') X @else &nbsp; @endif</span>
      </div>

      <div id="Address1" class="formgroup" style="top:432px; left:54px;">
      <span contenteditable="true" type="text" class="rect-input w50" name="unit"></span><span contenteditable="true" type="text" class="rect-input w158" name="building"></span><span contenteditable="true" type="text" class="rect-input w50" name="housenumber"></span><span contenteditable="true" type="text" class="rect-input w158" name="street"></span><span contenteditable="true" type="text" class="rect-input w158" name="village"></span>
      </div>

      <div id="Address2" class="formgroup" style="top:473px; left:54px;">
      <?php
        $brgy = getBrgyName($contact->barangay);
        $city = getCityNameReturn($contact->city);
        $prov = getProvinceNameReturn($contact->province);
        $zip = $contact->zip;
      ?>
      <span contenteditable="true" type="text" class="rect-input w115" name="barangay">{{ $brgy }}</span><span contenteditable="true" type="text" class="rect-input w115" name="city">{{ $city }}</span><span contenteditable="true" type="text" class="rect-input" style="width:200px;" name="province">{{ $prov }}</span><span contenteditable="true" type="text" class="rect-input" style="width:60px;" name="country">PHL</span><span contenteditable="true" type="text" class="rect-input" style="width:88px;" name="zipcode">{{ $zip }}</span>
      </div>

      <div id="Contact" class="formgroup" style="top:547px; left:246px;">
      <span contenteditable="true" type="text" class="rect-input" name="landline" style="width:156px;">{{ $contact->phone }}</span><spacer class="lab w70"></spacer><span contenteditable="true" type="text" class="rect-input" name="mobile" style="width:177px;">{{ $contact->mobile }}</span><spacer class="lab" style="width:88px;"></spacer><span contenteditable="true" type="text" class="rect-input" name="email" style="width:205px;">{{ $patient->email }}</span>
      </div>

      <div id="IsMember" class="formgroup" style="top:586px; left:200px;">
      <span contenteditable="true" type="radio" class="opt-input" name="is_member">@if( isset($philhealth) AND isset($philhealth->member_type) AND $philhealth->member_type == 'MM' ) X @else &nbsp; @endif </span><spacer class="lab" style="width:134px;"></spacer><span contenteditable="true" type="radio" class="opt-input" name="is_member">@if(isset($philhealth) AND isset($philhealth->member_type) AND $philhealth->member_type == 'DD') X @else &nbsp; @endif</span>
      </div>

      <div id="Dependent-Pin" class="formgroup" style="top:686px; left:371px;">
        <span contenteditable="true" type="text" class="box-input" name="deppin[]"></span><span contenteditable="true" type="text" class="box-input" name="deppin[]"></span><spacer></spacer><span contenteditable="true" type="text" class="box-input" name="deppin[]"></span><span contenteditable="true" type="text" class="box-input" name="deppin[]"></span><span contenteditable="true" type="text" class="box-input" name="deppin[]"></span><span contenteditable="true" type="text" class="box-input" name="deppin[]"></span><span contenteditable="true" type="text" class="box-input" name="deppin[]"></span><span contenteditable="true" type="text" class="box-input" name="deppin[]"></span><span contenteditable="true" type="text" class="box-input" name="deppin[]"></span><span contenteditable="true" type="text" class="box-input" name="deppin[]"></span><span contenteditable="true" type="text" class="box-input" name="deppin[]"></span><spacer></spacer><span contenteditable="true" type="text" class="box-input" name="deppin[]"></span>
      </div>

      <div id="Dependent-Birthdate" class="formgroup" style="top:720px; left:738px;">
        <span contenteditable="true" type="text" class="box-input" name="dep_mon[]"></span><span contenteditable="true" type="text" class="box-input" name="dep_mon[]"></span><spacer></spacer><span contenteditable="true" type="text" class="box-input" name="dep_dd[]"></span><span contenteditable="true" type="text" class="box-input" name="dep_dd[]"></span><spacer></spacer><span contenteditable="true" type="text" class="box-input" name="dep_yy[]"></span><span contenteditable="true" type="text" class="box-input" name="dep_yy[]"></span><span contenteditable="true" type="text" class="box-input" name="dep_yy[]"></span><span contenteditable="true" type="text" class="box-input" name="dep_yy[]"></span>
      </div>

      <div id="Dependent-Name" class="formgroup" style="top:745px; left:54px;">
      <span contenteditable="true" type="text" class="rect-input" name="dep_lastname"></span><span contenteditable="true" type="text" class="rect-input" name="dep_firstname"></span><span contenteditable="true" type="text" class="rect-input" name="dep_suffix"></span><span contenteditable="true" type="text" class="rect-input" name="dep_middlename"></span>
      </div>

      <div id="Dependent-Relationship" class="formgroup" style="top:790px; left:199px;">
      <span contenteditable="true" type="radio" class="opt-input" name="dep_relationship"></span><spacer class="lab" style="width:44px;"></spacer><span contenteditable="true" type="radio" class="opt-input" name="dep_relationship"></span><spacer class="lab" style="width:53px;"></spacer><span contenteditable="true" type="radio" class="opt-input" name="dep_relationship"></span>
      </div>

      <div id="Dependent-Gender" class="formgroup" style="top:789px; left:688px;">
      <span contenteditable="true" type="radio" class="opt-input" name="dep_sex"></span><spacer class="lab"></spacer><span contenteditable="true" type="radio" class="opt-input" name="dep_sex"></span>
      </div>

      <div id="ePEN" class="formgroup" style="top:1194px; left:238px;">
        <span contenteditable="true" type="text" class="box-input" name="epen[]"></span><span contenteditable="true" type="text" class="box-input" name="epen[]"></span><spacer></spacer><span contenteditable="true" type="text" class="box-input" name="epen[]"></span><span contenteditable="true" type="text" class="box-input" name="epen[]"></span><span contenteditable="true" type="text" class="box-input" name="epen[]"></span><span contenteditable="true" type="text" class="box-input" name="epen[]"></span><span contenteditable="true" type="text" class="box-input" name="epen[]"></span><span contenteditable="true" type="text" class="box-input" name="epen[]"></span><span contenteditable="true" type="text" class="box-input" name="epen[]"></span><span contenteditable="true" type="text" class="box-input" name="epen[]"></span><span contenteditable="true" type="text" class="box-input" name="epen[]"></span><spacer></spacer><span contenteditable="true" type="text" class="box-input" name="epen[]"></span>
      </div>

      <div id="eContact" class="formgroup" style="top:1189px; left:655px;">
        <span contenteditable="true" type="text" class="rect-input" name="pEmployerContact" style="width:182px;"></span>
      </div>

      <div id="eBusi" class="formgroup" style="top:1266px; left:136px;">
        <span contenteditable="true" type="text" class="rect-input" name="pEmployerName" style="width:715px;"></span>
      </div>

    </div>
</div>
</div>

<script>
  function removeDD() {
    $("#Dependent-Pin").hide();
    $("#Dependent-Birthdate").hide();
    $("#Dependent-Name").hide();
    $("#Dependent-Relationship").hide();
    $("#Dependent-Gender").hide();
  }
  function showDD() {
    $("#Dependent-Pin").show();
    $("#Dependent-Birthdate").show();
    $("#Dependent-Name").show();
    $("#Dependent-Relationship").show();
    $("#Dependent-Gender").show();
  }
  $(document).ready(function() {
    @if( isset($philhealth) AND isset($philhealth->member_type) AND $philhealth->member_type == 'MM' )
      removeDD();
    @endif
  });
</script>