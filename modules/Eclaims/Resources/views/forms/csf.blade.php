<div id="printFormCSF">
  <style>
    .formcsf {
      padding:0;
      height:1500px;
      width:974px;
      @if(isset($page))
      position:relative;
      margin:0 auto;
      @else
      margin:3px 0 0 3px;
      @endif
    }
    .formcsf1 {
      background: #FFF url('{!! asset("modules/Eclaims/Assets/images/csf.png") !!}') center top no-repeat !important;
      background-size: contain !important;
      margin-bottom: 20px;
    }
    
    @if(isset($page))
      #formcsf1, #formcsf2 {
        margin-top: -70px;
        position: relative;
        margin-left: -40px;
      }
    @endif
    .formgroup {
      position: absolute;
    }
    .formcf2 input, .formcf2 textarea {
      background-color: rgba(200,200,200,.2);
      border: none;
      height: 23px;
      padding: 1px 5px;
      max-width: 900px;
    }
    input {
      text-transform: uppercase !important;
      height: 22px;
      background-color: rgba(0,0,0,.05);
      border: none;
      vertical-align: sub;
    }
    input.box-input {
      width: 13px;
      text-align:center;
      vertical-align: sub;
      margin-left: 0.1em;
    }
    #Admitted-Date input.box-input {
      margin-left: 0.13em;
    }
    #ePEN input.box-input {
      width: 21px;
      margin-left: 0.05em;
    }
    input.rect-input {
      width: 159px;
      vertical-align: sub;
      margin-left: 0.1em;
      padding: 1px 4px 1px 4px;
    }
    input.opt-input {
      width: 23px;
      margin: 2px 0 0;
    }
    input[type='date'] {
      font-size: 10px;
      padding: 1px 0px 1px 4px;
    }
    
    spacer {
      width: 9px;
      display: inline-block;
    }
    spacer.lab {
      width: 41px;
      display: inline-block;
    }
    spacer.vert {
      height:7px;
      display: block;
    }
    .w165 {
      width: 165px !important;
    }
    .w158 {
      width: 158px !important;
    }
    .w115 {
      width: 115px !important;
    }
    .w100 {
      width: 100px !important;
    }
    .w85 {
      width: 85px !important;
    }
    .w70 {
      width: 70px !important;
    }
    .w50 {
      width: 52px !important;
    }
    .w13 {
      width: 13px !important;
    }
    .w12 {
      width: 12px !important;
    }
    .alert-absolute {
      position: absolute;
      width: 93%;
    }
    #Pin { top: 308px;left: 432px; }
      #Pin #pin.box-rect { width: 260px;
        background: #EEE;
        text-align: center;
        letter-spacing: 11px; }
    #Birthdate { top: 357px;left: 749px; }
      #Birthdate input {
        background: #EEE;
        text-align: center;
        letter-spacing: 5px;}
      #Birthdate input[name=mon] { width: 41px; }
      #Birthdate input[name=dd] { width: 41px; }
      #Birthdate input[name=yy] { width: 79px; }
    #Name { top:349px; left:59px; }
      #Name input.name_field { margin-left: 17px; }
    #Gender { top:406px; left:693px; }
    #Address1 { top:432px; left:54px; }
    #Address2 { top:473px; left:54px; }
    #Contact { top:548px; left:247px; }
    #IsMember { top:586px; left:203px; }
    #Dependent-Pin { top:414px; left:450px; }
      #Dependent-Pin #deppin.box-rect {
        width: 260px;
        background: #EEE;
        text-align: center;
        letter-spacing: 11px; }
    #Admitted-Date { top: 539px; left: 162px; }
      #Admitted-Date input {
        background: #EEE;
        text-align: center;
        letter-spacing: 5px;}
      #Admitted-Date input[name=admit_mon] { width: 41px; }
      #Admitted-Date input[name=admit_dd] { width: 41px; }
      #Admitted-Date input[name=admit_yy] { width: 79px; }
    #Discharge-Date { top: 538px;left: 486px; }
      #Discharge-Date input {
        background: #EEE;
        text-align: center;
        letter-spacing: 5px;}
      #Discharge-Date input[name=disch_mon] { width: 41px; }
      #Discharge-Date input[name=disch_dd] { width: 41px; }
      #Discharge-Date input[name=disch_yy] { width: 79px; }
    #Patient-Birthdate { top: 538px;left: 749px; }
      #Patient-Birthdate input {
        background: #EEE;
        text-align: center;
        letter-spacing: 5px;}
      #Patient-Birthdate input[name=patbirth_mon] { width: 41px; }
      #Patient-Birthdate input[name=patbirth_dd] { width: 41px; }
      #Patient-Birthdate input[name=patbirth_yy] { width: 79px; }
    #Patient-Name { top:454px; left:59px; }
      #Patient-Name input.name_field { margin-left: 17px; }
    #Patient-Relationship { top:460px; left:734px; }
    #Dependent-Gender { top:789px; left:691px; }
    #ePEN { top: 822px;left: 326px; }
      #ePEN #epen.box-rect { 
        width: 263px;
        background: #EEE;
        text-align: center;
        letter-spacing: 11px; }
    #eContact { top: 815px;left: 749px; }
    #eBusi { top: 840px;left: 215px; }
      #eBusi input { text-align: center; }
    #ICD10 { top: 1410px;left: 466px; }
      #ICD10 input { text-align: center; }

    
    @media print {
      @page {
        size: 8.5in 13in;
        margin: 0.12in;
      }
      @if(isset($page))
        #formcsf1 {
          margin-top: -70px;
          position: relative;
          margin-left: -35px;
        }
      @endif
      .formcf1 {
        width: 990px !important;
      } 
      input.box-input {
        margin-left: 0.1em;
      }
      spacer {
        width: 15px;
      }
      #Pin { top: 308px;left: 432px; }
        #Pin #pin.box-rect { width: 260px;
          background: #EEE;
          text-align: center;
          letter-spacing: 11px; }
      #Birthdate { top: 357px;left: 746px; }
        #Birthdate input {
          background: #EEE;
          text-align: center;
          letter-spacing: 5px;}
        #Birthdate input[name=mon] { width: 41px; }
        #Birthdate input[name=dd] { width: 41px; }
        #Birthdate input[name=yy] { width: 79px; }
      #Name { top:349px; left:59px; }
        #Name input.name_field { margin-left: 17px; }
      #Gender { top:406px; left:693px; }
      #Address1 { top:432px; left:54px; }
      #Address2 { top:473px; left:54px; }
      #Contact { top:548px; left:247px; }
      #IsMember { top:586px; left:203px; }
      #Dependent-Pin { top:414px; left:450px; }
        #Dependent-Pin #deppin.box-rect {
          width: 260px;
          background: #EEE;
          text-align: center;
          letter-spacing: 11px; }
      #Admitted-Date { top: 539px; left: 159px; }
        #Admitted-Date input {
          background: #EEE;
          text-align: center;
          letter-spacing: 5px;}
        #Admitted-Date input[name=admit_mon] { width: 41px; }
        #Admitted-Date input[name=admit_dd] { width: 41px; }
        #Admitted-Date input[name=admit_yy] { width: 79px; }
      #Discharge-Date { top: 538px;left: 483px; }
        #Discharge-Date input {
          background: #EEE;
          text-align: center;
          letter-spacing: 5px;}
        #Discharge-Date input[name=disch_mon] { width: 41px; }
        #Discharge-Date input[name=disch_dd] { width: 41px; }
        #Discharge-Date input[name=disch_yy] { width: 79px; }
      #Patient-Birthdate { top: 538px;left: 746px; }
        #Patient-Birthdate input {
          background: #EEE;
          text-align: center;
          letter-spacing: 5px;}
        #Patient-Birthdate input[name=patbirth_mon] { width: 41px; }
        #Patient-Birthdate input[name=patbirth_dd] { width: 41px; }
        #Patient-Birthdate input[name=patbirth_yy] { width: 79px; }
      #Patient-Name { top:454px; left:59px; }
        #Patient-Name input.name_field { margin-left: 17px; }
      #Patient-Relationship { top:460px; left:730px; }
      #Dependent-Gender { top:789px; left:691px; }
      #ePEN { top: 822px;left: 326px; }
        #ePEN #epen.box-rect { 
          width: 263px;
          background: #EEE;
          text-align: center;
          letter-spacing: 11px; }
      #eContact { top: 815px;left: 749px; }
      #eBusi { top: 840px;left: 215px; }
        #eBusi input { text-align: center; }
      #ICD10 { top: 1410px;left: 466px; }
        #ICD10 input { text-align: center; }
      
    }
  </style>

  <form id="form_eclaims" method="post" action="{{ url('eclaims/saveform') }}">
    <div class="container-fluid pad20 formcsf formcsf1">
      <div class="roww pad20"> 
        <div id="formcsf1">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="eclaims_id" value="{{ $eclaims_id }}">
          <input type="hidden" name="form" value="csf">
          <input type="hidden" name="pMemberSex" value="{{ $csfForm->pMemberSex }}">
          <input type="hidden" name="pMemberShipType" value="{{ $csfForm->pMemberShipType }}">
          @if(isset($csfForm->pHospitalCode))
          <input type="hidden" name="pHospitalCode" value="{{ $csfForm->pHospitalCode }}">
          @endif
          <input type="hidden" name="pPatientType" value="{{ $csfForm->pPatientType }}">
          <div id="Pin" class="formgroup">
            <?php
            $phic = NULL;
            if($csfForm AND $csfForm->pin != "") {
              $phic = $csfForm->pin;
            } ?>
            <input type="text" class="box-rect" id="pin" name="pin" value="{{ isset($phic) ? $phic : NULL }}" />@if(!isset($phic[1]))<spacer></spacer>
            <button id="getPINButton" class="btn btn-danger btn-xs btn-arrow-left no-print hidden" style="margin-top: 3px;" type="button" onclick="">Get Member PIN</button>@endif
          </div>
      
          <div id="Birthdate" class="formgroup">
            <input type="text" class="box-rect" name="mon" value="{{ $csfForm->mon }}" /><spacer></spacer><input type="text" class="box-rect" name="dd" value="{{ $csfForm->dd }}" /><spacer></spacer><input type="text" class="box-rect" name="yy" value="{{ $csfForm->yy }}" />
          </div>
      
          <div id="Name" class="formgroup">
            <input type="text" class="rect-input" name="pMemberLastName" value="{{ $csfForm->pMemberLastName }}" /><input type="text" class="rect-input name_field" name="pMemberFirstname" value="{{ $csfForm->pMemberFirstname }}" /><input type="text" class="rect-input name_field" name="pMemberSuffix" value="{{ $csfForm->pMemberSuffix }}" /><input type="text" class="rect-input name_field" name="pMemberMiddleName" value="{{ $csfForm->pMemberMiddleName }}" style="width: 128px;" />
          </div>

          <div id="IsMember" class="formgroup hidden">
            <input type="radio" class="opt-input" name="is_member" value="MM" @if( isset($csfForm) AND isset($csfForm->is_member) AND $csfForm->is_member == 'MM' ) checked @endif /><spacer class="lab" style="width:134px;"></spacer><input type="radio" class="opt-input" name="is_member" value="DD" @if(isset($csfForm) AND isset($csfForm->is_member) AND $csfForm->is_member == 'DD') checked @endif  />
          </div>

          <div id="Dependent-Pin" class="formgroup">
            <input type="text" class="box-rect" id="deppin" name="deppin" value="{{ $csfForm->deppin }}" />@if($csfForm->deppin == NULL)<spacer></spacer>
            <button class="btn btn-danger btn-xs btn-arrow-left no-print hidden" id="getDepPINButton" type="button" onclick="">Get Dependent PIN</button>@endif
          </div>

          <div id="Admitted-Date" class="formgroup">
            <input type="text" class="box-rect" name="admit_mon" value="{{ $csfForm->admit_mon }}" /><spacer></spacer><input type="text" class="box-rect" name="admit_dd" value="{{ $csfForm->admit_dd }}" /><spacer></spacer><input type="text" class="box-rect" name="admit_yy" value="{{ $csfForm->admit_yy }}" />
          </div>

          <div id="Discharge-Date" class="formgroup">
            <input type="text" class="box-rect" name="disch_mon" value="{{ $csfForm->disch_mon }}" /><spacer></spacer><input type="text" class="box-rect" name="disch_dd" value="{{ $csfForm->disch_dd }}" /><spacer></spacer><input type="text" class="box-rect" name="disch_yy" value="{{ $csfForm->disch_yy }}" />
          </div>

          <div id="Patient-Birthdate" class="formgroup">
            <input type="text" class="box-rect" id="patbirth_mon" name="patbirth_mon" value="{{ $csfForm->patbirth_mon }}" /><spacer></spacer><input type="text" class="box-rect" id="patbirth_dd" name="patbirth_dd" value="{{ $csfForm->patbirth_dd }}" /><spacer></spacer><input type="text" class="box-rect" id="patbirth_yy" name="patbirth_yy" value="{{ $csfForm->patbirth_yy }}" />
          </div>

          <div id="Patient-Name" class="formgroup">
            <input type="text" class="rect-input" name="pPatientLastName" value="{{ $csfForm->pPatientLastName }}" /><input type="text" class="rect-input name_field" name="pPatientFirstName" value="{{ $csfForm->pPatientFirstName }}" /><input type="text" class="rect-input name_field" name="pPatientSuffix" value="{{ $csfForm->pPatientSuffix }}" /><input type="text" class="rect-input name_field" name="pPatientMiddleName" value="{{ $csfForm->pPatientMiddleName }}" style="width: 128px;" />
          </div>

          <div id="Patient-Relationship" class="formgroup">
            <input type="radio" class="opt-input" value="C" @if(isset($csfForm->pat_relationship) AND $csfForm->pat_relationship == 'C') checked @endif name="pat_relationship" /><spacer class="lab" style="width:44px;"></spacer><input type="radio" class="opt-input" value="P" @if(isset($csfForm->pat_relationship) AND $csfForm->pat_relationship == 'P') checked @endif name="pat_relationship" /><spacer class="lab" style="width:49px;"></spacer><input type="radio" class="opt-input" value="S" @if(isset($csfForm->pat_relationship) AND $csfForm->pat_relationship == 'S') checked @endif name="pat_relationship" />
          </div>

          <div id="Dependent-Gender" class="formgroup hidden">
            <input type="radio" class="opt-input" name="dep_sex" value="M" @if(isset($csfForm->dep_sex) AND $csfForm->dep_sex == 'M') selected @endif /><spacer class="lab"></spacer><input type="radio" class="opt-input" name="dep_sex" value="F" @if(isset($csfForm->dep_sex) AND $csfForm->dep_sex == 'F') selected @endif />
          </div>

          <div id="ePEN" class="formgroup">
            <input type="text" class="box-rect" id="epen" name="epen" value="{{ $csfForm->epen }}" />
            @if($csfForm->epen == NULL)<spacer style="width:11px;"></spacer><button class="btn btn-danger btn-xs btn-arrow-left no-print hidden" type="button" id="getPENButton" onclick="">Get PEN</button>@endif
          </div>

          <div id="eContact" class="formgroup">
            <input type="text" class="rect-input" name="pEmployerContact" value="{{ $csfForm->pEmployerContact }}" style="width:145px;" />
          </div>

          <div id="eBusi" class="formgroup">
            <input type="text" class="rect-input" name="pEmployerName" value="{{ $csfForm->pEmployerName }}" style="width: 543px;height: 16px;" />
          </div>

          <div id="ICD10" class="formgroup">
            <input type="text" class="rect-input" name="FirstCaseRateCode" value="{{ isset($csfForm->FirstCaseRateCode) ? $csfForm->FirstCaseRateCode : NULL }}" style="width:164px;" /><spacer style="width:127px;"></spacer><input type="text" class="rect-input" name="SecondCaseRateCode" value="{{ isset($csfForm->SecondCaseRateCode) ? $csfForm->SecondCaseRateCode : NULL }}" style="width:196px;" />
          </div>

        </div>
      </div>
    </div>

  </form>
</div>

<script>
  function getPIN(id) {
    $("#getPINButton").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Member PIN...");
    
    $.ajax({
        url: "{{ url('eclaims/getPIN/') }}"+id,
        data: {
          "_token": "{{ csrf_token() }}"
        }, 
        type: "post",
        dataType: "json"
    })
    .done(function(data) {
        if(data && data.ISOK == "OK") {
          $("#getPINButton").hide();
          var pin = data.PHILHEALTHNUMBER.replace(/-/g,"");
          var expin = pin.split("");
          expin.forEach(function(item, index) {
            $('#pin'+index).val(item);
          })
        }
    })
    .fail( function() {
      $("#getPINButton").html("Get Member PIN");
      swal("Oops!", 'Something went wrong. Cannot retrieve PIN Information. Please try again.',"error");
    });
  }
  
  function getDepPIN(id) {
    $("#getDepPINButton").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching Dependent PIN...");
    
    $.ajax({
        url: "{{ url('eclaims/getDepPIN') }}",
        data: {
          "_token": "{{ csrf_token() }}",
          "first_name": $('input[name=pat_firstname]').val(),
          "last_name": $('input[name=pat_lastname]').val(),
          "middle_name": $('input[name=pat_middlename]').val(),
          "suffix": $('input[name=pat_suffix]').val(),
          "birthdate": $('#patbirth_yy0').val()+$('#patbirth_yy1').val()+$('#patbirth_yy2').val()+$('#patbirth_yy3').val()+"-"+$('#patbirth_mon0').val()+$('#patbirth_mon1').val()+"-"+$('#patbirth_dd0').val()+$('#patbirth_dd1').val()
        }, 
        type: "post",
        dataType: "json"
    })
    .done(function(data) {
        if(data && data.ISOK == "OK") {
          $("#getDepPINButton").hide();
          // var pin = data.PHILHEALTHNUMBER.replace(/-/g,"");
          // var expin = pin.split("");
          // expin.forEach(function(item, index) {
          //   $('#deppin'+index).val(item);
          // })
        }
    })
    .fail( function() {
      $("#getPINButton").html("Get Dependent PIN");
      swal("Oops!", 'Something went wrong. Cannot retrieve PIN Information. Please try again.', "error");
    });
  }

  function getPEN(id) {
    $("#getPENButton").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Fetching PEN...");
    
    $.ajax({
        url: "{{ url('eclaims/getEmployer') }}",
        data: {
          "_token": "{{ csrf_token() }}",
          "pPEN": "123456789098",
          "pEmployerName": $('input[name=pEmployerName]').val()
        }, 
        type: "post",
        dataType: "json"
    })
    .done(function(data) {
        console.log(data);
        if(data && data.ISOK == "OK") {
          $("#getPENButton").hide();
          var pin = data.pPEN.replace(/-/g,"");
          var expin = pin.split("");
          expin.forEach(function(item, index) {
            $('#epen'+index).val(item);
          })
          $('input[name=pEmployerContact').val(data.pEmployerAddress);
          $('input[name=pEmployerName').val(data.pEmployerName);
        }
        if(data && data.error == 'validation_error') {
          $("#getPENButton").html("Get PEN");
          swal("Oops!", data.message[0], "error");
        }
    })
    .fail( function() {
      $("#getPENButton").html("Get PEN");
      swal("Oops!", 'Something went wrong. Cannot retrieve PEN Information. Please try again.', "error");
    });
  }
  $(document).ready(function() {
    $("input.box-input").attr("maxlength", 1)
  });
</script>
