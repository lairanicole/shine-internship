@extends('eclaims::layouts.master')

@section('heads')
{!! HTML::style('modules/Eclaims/Assets/eclaims.css') !!}
<style>
    .tab-pane .box {
        background: #FFF !important;
    }
    .modal-header {
        border-radius: 6px 6px 0 0 !important;
    }
    .dl-horizontal dt {
        width: 200px;
        padding-right: 15px;
        text-align: left;
    }
    dl, hr {
        margin: 0;
    }
    h5 {
        font-weight: 900;
        margin:7px 0;
        padding:0;
    }
    hr.sign {
        margin-top: 120px;
        border: 1px solid #222;
    }
    .pad10 {
        padding: 10px;
    }
    .bordered {
        background-color:#999;
        height:1px;
    }
    table, tr, td {
        padding:0;
    }
    table th {
        width: auto !important;
    }

    textarea {
        resize: none;
    }

    @media print {
        #printForm hr {
            margin:0;
            padding:0;
            height:3px;
        }
        #printForm td h5 {
            margin:0;
            padding:0;
            font-size:8px;
        }
        #printForm td, #printForm td p {
            font-size: 8px;
            line-height:1;
            margin:0;
            padding:0;
        }
        #printForm .bordered {
            border-bottom: 2px solid #333;
        }
        #printForm small, #printForm .small {
            font-size: 6px;
        }
    }
</style>
@stop

@section('eclaims-content')
<!-- Main content -->
<section class="content">
    @if(isset($debug_result) AND Config::get('eclaims.debug') == 1 )
    <!-- if debug if on, display submitted values and results -->
        <div>
            <?php
            // echo "DATA POSTED";
            // print "<pre>";
            // print_r($debug_post);
            // print "</pre>";
            // echo "DATA SUBMITTED";
            // print "<pre>";
            // print_r($debug_data);
            // print "</pre>";
            echo "<h4>API RESPONSE</h4>";
            print "<pre>";
            if(isset($debug_result['error']) AND $debug_result['error'] == 'validation_error')
            {
                echo "VALIDATION ERROR<br><br>";
                echo "Requirements:<br>";
                foreach($debug_result['message'] as $key => $message)
                {
                    echo $key+1 . ". " . $message . "<br>";
                }
            }
            elseif(isset($debug_result['ISOK']) AND $debug_result['ISOK'] == 'error')
            {
                echo "ERROR<br><br>";
                echo $debug_result['MESSAGE']."<br>";   
            }
            else
            {
                echo $debug_result."<br>";   
            }
            print "</pre>";
            ?>            
        </div>
    @endif
    @if(isset($flash_message))
    <!-- display notification when available -->
      <div class="alert {{ $flash_type }}">{{ $flash_message }}</div>
    @endif

    <div class="row result" id="frame"></div>

    <br />
    <div class="row text-center" id="buttonGroup">
      @if($page == 'eligible')
      <p><strong>IMPORTANT</strong>: Print this form using US Folio size paper (8.5 x 13 inches) and 100% scaling using default margins.</p>
      @endif
      @if($page == 'csf')
      <p><strong>IMPORTANT</strong>: Print this form using US Folio size paper (8.5 x 13 inches) and 80% scaling using zero margins.</p>
      @endif
      @if($page == 'cf1')
      <p><strong>IMPORTANT</strong>: Print this form using US Folio size paper (8.5 x 13 inches) and 100% scaling using default margins.</p>
      @endif
      @if($page == 'cf2')
      <p><strong>IMPORTANT</strong>: Print this form using US Folio size paper (8.5 x 13 inches) and 100% scaling using zero margins.</p>
      @endif
      @if($page == 'cf3')
      <p><strong>IMPORTANT</strong>: Print this form using US Folio size paper (8.5 x 13 inches) and 78% scaling using default margins.</p>
      @endif
      @if($page == 'cf4')
      <p><strong>IMPORTANT</strong>: Print this form using Legal size paper (8.5 x 14 inches) and 74% scaling using default margins.</p>
      @endif
      <p>Make sure you turn on Print Background Graphics.</p>
      <br />
      <a href="{{ url('eclaims') }}" type="button" class="btn btn-default" >Close</a>
      @if($page != 'cf4')
      <button class="btn btn-success" onclick="$('.result').printThis();">{{ $print_button }}</button>
      @endif
      @if(isset($submit_button) AND $submit_button!="")
      <button onclick="$('#form_eclaims').submit();" class="btn btn-warning" >{{ $submit_button }}</button>
      @endif
      @if($page == 'cf4')
      <button onmousedown="$('input[name=submitCF4]').val(1);" onclick="gencf4();" class="btn btn-success genButton hidden">Generate CF4 XML</button>
      <button onmousedown="$('input[name=submitCF4]').val(1);" onclick="gencf4();" class="btn btn-success reGenButton hidden">Regenerate CF4 XML</button>
      @endif
    </div>
</section><!-- /.content -->

<div id="img-out"></div>
@stop

@section('scripts')
{!! HTML::script('modules/Eclaims/Assets/plugins/html2canvas.js') !!}
{!! HTML::script('modules/Eclaims/Assets/plugins/base64js.min.js') !!}
{!! HTML::script('modules/Eclaims/Assets/plugins/canvas2image.js') !!}

<script>
    function topdf() {
        html2canvas($("#frame"), {
            onrendered: function(canvas) {
                theCanvas = canvas;
                document.body.appendChild(canvas);

                // Convert and download as image 
                Canvas2Image.convertToPNG(canvas); 
                $("#img-out").append(canvas);
                // Clean up 
                //document.body.removeChild(canvas);
            }
        });
    }
    function gencf4() {
        $('#form_eclaims').on('submit', function(e){
            e.stopPropagation();
            swal({
                title: 'EClaims Processing',
                text: 'Generating CF4 XML. Please wait...',
                type: 'info',
                showLoaderOnConfirm: true,
                showConfirmButton: false,
                showCancelButton: false,
                onBeforeOpen: () => {
                    Swal.showLoading();
                }
            });
        }).submit();
    }
    $(document).ready(function() {
        @if(isset($eclaims_id))
            $('#frame').load("{{ url('/eclaims/loadclaim/'.$page.'/'.$eclaims_id) }}");
        @else
            @if(isset($careid) AND isset($patid))
                $('#frame').load("{{ url('/eclaims/load/'.$page.'/'.$patid.'/'.$careid) }}");
            @elseif(!isset($careid) AND isset($patid))
                $('#frame').load("{{ url('/eclaims/load/'.$page.'/'.$patid) }}");
            @else
                $('#frame').load("{{ url('/eclaims/load/'.$page) }}");
            @endif
        @endif
    });
</script>
@stop
