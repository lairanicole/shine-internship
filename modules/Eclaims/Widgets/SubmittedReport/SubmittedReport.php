<?php
namespace Modules\Eclaims\Widgets\SubmittedReport;

use Arrilot\Widgets\AbstractWidget;
use Shine\Repositories\Eloquent\UserRepository as BaseRepository;
use Shine\Repositories\Contracts\UserRepositoryInterface;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\UserHelper;
use Modules\Eclaims\Entities\Eclaims;
use View, Config, Session;

/**
 * Widget for the total number of records per module
 */
class SubmittedReport extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */

    protected $config = [];

    public function placeholder()
    {
        $loading = '<div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-cog fa-spin fa-fw"></i> Loading Stats widget...</h3>
                </div>
            </div>';


        return $loading;
    }

    /**
     * The repository object.
     *
     * @var object
     */
    private $baseRepository;

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(BaseRepository $baseRepository)
    {
        $facility = Session::get('facility_details');
        $eclaims_facility = Session::get('eclaims_facility');
        $hospital_code = $eclaims_facility ? $eclaims_facility->hci_hospitalcode : NULL;

        $claims = Eclaims::where('facility_id',$facility->facility_id)->where('HOSPITAL_CODE',$hospital_code)->get();
        $data['process'] = $claims->where('eclaim_status','IN PROCESS')->count();
        $data['denied'] = $claims->where('eclaim_status','DENIED')->count();
        $data['return'] = $claims->where('eclaim_status','RETURN')->count();
        $voucher = $claims->where('eclaim_status','WITH VOUCHER')->count();
        $cheque = $claims->where('eclaim_status','WITH CHEQUE')->count();
        $data['voucher'] = $voucher + $cheque;

        View::addNamespace('submittedreport', 'modules/Eclaims/Widgets/SubmittedReport');
        return view("submittedreport::index")->with($data);
    }
}
