<div class="box box-primary" id="content_overview">
    <div class="box-body">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <!-- small box -->
            <div class="overview-box small-box bg-blue">
                <div class="inner">
                    <div class="icon"><i class="fa fa-cog"></i></div>
                    <h3>{{$process ?? 0}}</h3>
                    <p>IN PROCESS</p>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <!-- small box -->
            <div class="overview-box small-box bg-red">
                <div class="inner">
                    <div class="icon"><i class="fa fa-exclamation-circle"></i></div>
                    <h3>{{$denied ?? 0}}</h3>
                    <p>DENIED</p>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <!-- small box -->
            <div class="overview-box small-box bg-orange">
                <div class="inner">
                    <div class="icon"><i class="fa fa-arrow-circle-left"></i></div>
                    <h3>{{$return ?? 0}}</h3>
                    <p>RETURN</p>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <!-- small box -->
            <div class="overview-box small-box bg-green">
                <div class="inner">
                    <div class="icon"><i class="fa fa-check-circle"></i></div>
                    <h3>{{$voucher ?? 0}}</h3>
                    <p>VOUCHER/<br>CHEQUE</p>
                </div>
            </div>
        </div><!-- ./col -->
    </div>
</div><!--./box-->
