<?php namespace Modules\Eclaims\Http\Controllers;

use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientContacts;
use Plugins\Philhealth\PhilhealthModel;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Healthcareservices\Entities\Disposition;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderPrescription;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderLabExam;

use ShineOS\Core\Users\Entities\Users;
use ShineOS\Core\Users\Entities\MDUsers;

use Pingpong\Modules\Routing\Controller;
use Modules\Eclaims\Entities\Eclaims; //model
use Modules\Eclaims\Entities\EclaimsFacility;
use Shine\Libraries\IdGenerator;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\UserHelper;
use Shine\Libraries\Utils\Lovs;

use Modules\Eclaims\Assets\libraries\PhilHealthEClaimsEncryptor as Encrypt; //model

use View, Response, Input, Datetime, DB, Redirect, Session, Config;

class EclaimsController extends Controller {

    private $facility_id;

    public function __construct() {
        $this->middleware('auth');
        $this->user = UserHelper::getUserInfo();
        $this->facility = FacilityHelper::facilityInfo();

        $this->ECLAIMS_URL = getenv('ECLAIMS_URL');
        $this->SOFTWARECERTID = getenv('ECLAIMS_SOFTWARECERTID');
        // $this->ECLAIMS_URL = 'http://host.docker.internal:8282';
        // $this->ECLAIMS_URL = 'http://202.125.102.51/demo';

        $this->eclaims_facility = Session::has('eclaims_facility') ? Session::get('eclaims_facility') : NULL;

        // Get following details from facility information
        $this->USERNAME = $this->eclaims_facility ? $this->eclaims_facility->hci_userid.':'.$this->SOFTWARECERTID : NULL;
        $this->USER_PASSWORD = $this->eclaims_facility ? $this->eclaims_facility->hci_password : NULL;
        $this->HOSPITAL_CODE = $this->eclaims_facility ? $this->eclaims_facility->hci_hospitalcode : NULL;
        $this->PAN = $this->eclaims_facility ? $this->eclaims_facility->hci_ph_inst_code : NULL;
        $this->CIPHER_KEY = $this->eclaims_facility ? $this->eclaims_facility->hci_cipherkey : NULL;
    }

    public function index()
    {
        $data['user'] = $this->user;
        $data['facilityInfo'] = $this->facility;

        $data['claims'] = Eclaims::collectQualifiers($this->facility->facility_id); 
        $data['eligibilitycheck'] = Eclaims::whereNull('eclaim_file_date')->where('HOSPITAL_CODE',$this->HOSPITAL_CODE)->get();  
        $data['eligibles'] = Eclaims::where('pbef_status', 'YES')->whereNull('eclaim_file_date')->where('HOSPITAL_CODE',$this->HOSPITAL_CODE)->get(); 
        $data['noneligibles'] = Eclaims::where('pbef_status', 'NO')->where('HOSPITAL_CODE',$this->HOSPITAL_CODE)->get();
        $data['submitted'] = Eclaims::whereNotNULL('eclaim_file_date')->where('HOSPITAL_CODE',$this->HOSPITAL_CODE)->get();
        
        $temp_eclaimsfacility = EclaimsFacility::where('facility_id',$this->facility->facility_id)->select('hci_hospitalcode',DB::raw("CONCAT(hci_accrename,' - ',hci_hospitalcode) AS hci_accrename"))->get()->keyBy('hci_hospitalcode')->toArray();
        $data['eclaims_facility'] = [];
        foreach ($temp_eclaimsfacility as $key => $value) {
            $data['eclaims_facility'][$key] = $value['hci_accrename'];
        }

        $data['accreditation_types'] = array(
            'H1'=>'Level 1 Hospital',
            'H2'=>'Level 2 Hospital',
            'H3'=>'Level 3 Hospital',
            'ASC'=>'Ambulatory Surgical Clinic',
            'PCF'=>'Primary Care Facility',
            'MAT'=>'Maternity Care Package Provider',
            'FSDC'=>'Freestanding Dialysis Clinics',
        );

        $selected = $data['selected'] = Session::has('eclaims_facility') ? Session::get('eclaims_facility') : NULL;
        $data['selected_facility'] = $selected ? $selected->hci_hospitalcode : NULL;
        $data['selected_accreditation'] = $selected ? $selected->hci_accreditation : NULL;

        return view('eclaims::index')->with($data);
    }

    public function saveaccreditation() {
        $facility = Session::get('facility_details');

        $hci_hospitalcode = Input::get('hci_hospitalcode');
        $hci_accreditation = Input::get('hci_accreditation');

        $eclaims_facility = EclaimsFacility::where('hci_hospitalcode',$hci_hospitalcode)->where('facility_id',$facility->facility_id)->first();
        $eclaims_facility->hci_accreditation = $hci_accreditation;   
        $eclaims_facility->save();

        $eclaims_facility = Session::put('eclaims_facility',$eclaims_facility);
        return Redirect::to('eclaims');
    }

    /**
     * Fill Form
     * Param $patid String - Patient ID
     * Param $careid String - Healthcare ID
     * Param $page String Optional - Form Name
     * 
     * Controller to display the form
     * 
     */
    function fillform($eclaims_id, $page=NULL)
    {
        $claim = Eclaims::where('eclaims_id', $eclaims_id)->first();
        $data['cf1'] = json_decode($claim->cf1_details);
        $data['patient'] = Patients::where("patient_id", $claim->patient_id)->first();
        $data['contact'] = PatientContacts::where("patient_id", $claim->patient_id)->first();
        $data['philhealth'] = PhilhealthModel::where('patient_id','=',$claim->patient_id)->first();
        $data['patid'] = $claim->patient_id;
        $data['careid'] = $claim->healthcareservice_id;
        $data['eclaims_id'] = $eclaims_id;

        if($page) {
            if($page == 'receipts')
            {
                $data['claim'] = $claim;
                return view('eclaims::receipt.receipts')->with($data);
            }

            $data['page'] = $page;
            $data['title'] = strtoupper($page);
            $data['submit_button'] = "Save ".strtoupper($page);
            $data['print_button'] = "Print ".strtoupper($page);
            return view('eclaims::page', $data);
        } else {
            return view('eclaims::forms.cf2', $data);
        }
    }

    /**
     * Save Form
     * Param $form String - Form Name
     * 
     * Controller for save the form
     * 
     */
    function saveform()
    {
        $form = $_POST['form'];
        $eclaims_id = $_POST['eclaims_id'];

        if($form == 'csf') { //this is for updating only
            $csfdata = array(
                "csf" => IdGenerator::generateSmallId()."-csf",
                "csf_details" => json_encode($_POST),
                "csf_date" => date('Y-m-d H:i:s')
            );

            Eclaims::where('eclaims_id', $eclaims_id)->update($csfdata);
            $message = "CF1 saved successfully.";
        }

        if($form == 'cf1') { //this is for updating only
            if(isset($_POST['cf1'])) {
                $eclaimData = $this->getCF1Form();
                $cf1data = array(
                    "cf1_sdata" => json_encode($_POST),
                    "cf1_details" => json_encode($eclaimData)
                );
            } else {
                $cf1data = array(
                    "cf1" => IdGenerator::generateSmallId()."-cf1",
                    "cf1_sdata" => json_encode($_POST),
                    "cf1_date" => date('Y-m-d H:i:s')
                );
            }

            Eclaims::where('eclaims_id', $eclaims_id)->update($cf1data);
            $message = "CF1 saved successfully.";
        }
        if($form == 'cf2') {
            $cf2data = array(
                "cf2" => IdGenerator::generateSmallId()."-cf2",
                "cf2_details" => json_encode($_POST),
                "cf2_date" => date('Y-m-d H:i:s')
            );

            Eclaims::where('eclaims_id', $eclaims_id)->update($cf2data);
            $message = "CF2 saved successfully.";
        }
        if($form == 'cf3') {
            $cf3data = array(
                "cf3" => IdGenerator::generateSmallId()."-cf3",
                "cf3_details" => json_encode($_POST),
                "cf3_date" => date('Y-m-d H:i:s')
            );

            Eclaims::where('eclaims_id', $eclaims_id)->update($cf3data);
            $message = "CF3 saved successfully.";
        }
        if($form == 'cf4') {
            $cf4data = array(
                "cf4" => IdGenerator::generateSmallId()."-cf4",
                "cf4_details" => json_encode($_POST),
                "cf4_date" => date('Y-m-d H:i:s')
            );

            Eclaims::where('eclaims_id', $eclaims_id)->update($cf4data);
            $message = "CF4 saved successfully.";

            if(isset($_POST['submitCF4']) AND $_POST['submitCF4'] == 1) {
                return redirect()->route('eclaims.generateCF4', $eclaims_id);
            }
        }
        return Redirect::to( "eclaims#eligibilitycheck" )->with('message', $message);
    }

    /**
     * Show Form
     * Param $form String - name of form to load
     * Param $eclaimid String - eclaim ID
     * 
     * This is the controller called by the route to
     * show submitted forms for viewing
     * 
     */
    function showform($form, $eclaimid)
    {
        $claim = Eclaims::where('eclaims_id', $eclaimid)->first();
        $data['eclaims_id'] = $eclaimid;
        $data['patient'] = Patients::where("patient_id", $claim->patient_id)->first();
        $data['contact'] = PatientContacts::where("patient_id", $claim->patient_id)->first();
        $data['philhealth'] = PhilhealthModel::where('patient_id','=',$claim->patient_id)->first();
        $data['patid'] = $claim->patient_id;
        $data['careid'] = $claim->healthcareservice_id;
        $data['page'] = $form;
        $data['title'] = ($form == 'eligible') ?  "Claims Eligibility" : "Claims ".strtoupper($form);
        $data['pdf_button'] = "Save to PDF";
        $data['print_button'] = ($form == 'eligible') ? "Print PBEF" : "Print ".strtoupper($form);

        if(in_array($claim->pbef_status,['ERROR','NO']) AND $form == 'cf1')
        {
            $data['submit_button'] = "Check Eligibility";
        }
        else
        {
            $data['submit_button'] = ($form == 'eligible') ? "" : "Update ".strtoupper($form);
        }

        if($claim->cf4_details) {
            $data['cf4on'] = 1;
        }

        return view('eclaims::page', $data);
    }

    function searchCaseRate($pICD=NULL, $pRVS=NULL)
    {
        $eclaimData['cipherKey'] = $this->CIPHER_KEY;
        $eclaimData['pUserName'] = $this->USERNAME;
        $eclaimData['pUserPassword'] = $this->USER_PASSWORD;
        $eclaimData['pHospitalCode'] = $this->HOSPITAL_CODE;

        if($pICD OR $pRVS) // For eClaims submission
        {
            $eclaimData['pICD'] = strtoupper($pICD);
            $eclaimData['pRVS'] = strtoupper($pRVS);
        } 
        elseif(isset($_POST['pICD']) AND isset($_POST['pRVS'])) // For eClaims management
        {
            $eclaimData['pICD'] = strtoupper($_POST['pICD']);
            $eclaimData['pRVS'] = strtoupper($_POST['pRVS']);
        } 
        else 
        {
            $eclaimData['pICD'] = NULL;
            $eclaimData['pRVS'] = NULL;
        }

        $url = $this->ECLAIMS_URL."/api/v1/SearchCaseRate";
        $result = NULL;
        $result = $this->_sendCurl($url, $eclaimData);
        
        if($result) {
            if($pICD OR $pRVS) {
                return json_encode($result);
            }
            if(isset($_POST['pICD']) AND isset($_POST['pRVS']))
            {

                $decoded = is_array($result) ? $result : json_decode($result,TRUE);
                if(isset($decoded['CASERATES']))
                {
                    $data['caserates'] = $decoded['CASERATES'];
                    $view = View::make('eclaims::responses.caserate')->with($data)->render();
                    $response = array('ISOK'=>'success','MESSAGE'=>$view);                    
                }
                elseif(isset($decoded['ISOK']) && $decoded['ISOK']=='error' && isset($decoded['MESSAGE']))
                {
                    $response = array('ISOK'=>'error','MESSAGE'=>$decoded['MESSAGE']);
                }
                else
                {
                    $response = array('ISOK'=>'error','MESSAGE'=>"Sorry, something went wrong. Please try again later.");                      
                }
                echo json_encode($response);
            }
        }
        //print("<h4>Voucher Details</h4><br /><pre>".print_r($result,true)."</pre>");
    }
    
    function searchHospital()
    {
        $eclaimData['cipherKey'] = $this->CIPHER_KEY;
        $eclaimData['pUserName'] = $this->USERNAME;
        $eclaimData['pUserPassword'] = $this->USER_PASSWORD;
        $eclaimData['pHospitalCode'] = $this->HOSPITAL_CODE;
        $eclaimData['pHospitalName'] = $_POST['pHospitalName'];

        $url = $this->ECLAIMS_URL."/api/v1/SearchHospital";
        $result = NULL;
        $result = $this->_sendCurl($url, $eclaimData);

        $decoded = is_array($result) ? $result : json_decode($result,TRUE);
        if(isset($decoded['ISOK']) && $decoded['ISOK']=='error')
        {
            $response = array(
                'ISOK'=>'error',
                'MESSAGE'=>$decoded['MESSAGE']
            );
        }
        elseif(isset($decoded['HOSPITAL']))
        {
            $data['hospitals'] = $decoded['HOSPITAL'];
            $view = View::make('eclaims::responses.hospitals')->with($data)->render();
            $response = array(
                'ISOK'=>'success',
                'MESSAGE'=>$view
            );
        }
        elseif(isset($decoded['error']) AND $decoded['error'] == 'validation_error')
        {
            $response = $decoded;
        }
        else
        {
            $response = array(
                'ISOK'=>'error',
                'MESSAGE'=>$result
            );            
        }
        
        echo json_encode($response);
        //print("<h4>Voucher Details</h4><br /><pre>".print_r($result,true)."</pre>");
    }

    function getPIN($patid=NULL) {
        if($patid) {
            $patient = Patients::where("patient_id", $patid)->first();
        }
        if($patid == NULL OR !$patient) {
            if(isset($_POST['pMemberSuffix'])) {
                $suffix = $_POST['pMemberSuffix'];
            } else {
                $suffix = "";
            }
            $patient = new \stdClass();
            $patient->first_name = $_POST['pMemberFirstName'];
            $patient->last_name = $_POST['pMemberLastName'];
            $patient->middle_name = $_POST['pMemberMiddleName'];
            $patient->name_suffix = $suffix;
            $patient->birthdate = $_POST['pMemberBirthDate'];
        }
        if($patient) {
            $data['cipherKey'] = $this->CIPHER_KEY;
            $data['pUserName'] = $this->USERNAME;
            $data['pUserPassword'] = $this->USER_PASSWORD;
            $data['pHospitalCode'] = $this->HOSPITAL_CODE;
            $data['pMemberLastName'] = $patient->last_name;
            $data['pMemberFirstName'] = $patient->first_name;
            $data['pMemberMiddleName'] = $patient->middle_name;
            $data['pMemberSuffix'] = $patient->name_suffix;
            $data['pMemberBirthDate'] = date("m-d-Y", strtotime($patient->birthdate));
            $url = $this->ECLAIMS_URL."/api/v1/GetMemberPIN";
        $result = NULL;
        $result = $this->_sendCurl($url, $data);

        } else {
            $result['error'] = "Member not found";
        }
        // dd($result);
        echo json_encode($result);
    }

    function searchEmployer() {
        $data['cipherKey'] = $this->CIPHER_KEY;
        $data['pUserName'] = $this->USERNAME;
        $data['pUserPassword'] = $this->USER_PASSWORD;
        $data['pHospitalCode'] = $this->HOSPITAL_CODE;
        $data['pPEN'] = $_POST['empPen'];
        $data['pEmployerName'] = $_POST['empName'];

        $url = $this->ECLAIMS_URL."/api/v1/employer/SearchEmployer";
        $result = NULL;
        $result = $this->_sendCurl($url, $data);

        $fixedjson = json_encode($result);

        $fixedjson = str_replace("@attributes", "node", $fixedjson);

        echo $fixedjson;
    }
    
    function getDepPIN() {
        $data['cipherKey'] = $this->CIPHER_KEY;
        $data['pUserName'] = $this->USERNAME;
        $data['pUserPassword'] = $this->USER_PASSWORD;
        $data['pHospitalCode'] = $this->HOSPITAL_CODE;
        $data['pMemberLastName'] = $_POST['last_name'];
        $data['pMemberFirstName'] = $_POST['first_name'];
        $data['pMemberMiddleName'] = $_POST['middle_name'];
        $data['pMemberSuffix'] = $_POST['suffix'];
        $data['pMemberBirthDate'] = date("m-d-Y", strtotime($_POST['birthdate']));


        $url = $this->ECLAIMS_URL."/api/v1/GetMemberPIN";
        $result = NULL;
        $result = $this->_sendCurl($url, $data);

        echo json_encode($result);
    }

    function getEmployer() {

        $data['cipherKey'] = $this->CIPHER_KEY;
        $data['pUserName'] = $this->USERNAME;
        $data['pUserPassword'] = $this->USER_PASSWORD;
        $data['pHospitalCode'] = $this->HOSPITAL_CODE;
        $data['pPEN'] = $_POST['pPEN'];
        $data['pEmployerName'] = $_POST['pEmployerName'];

        $url = $this->ECLAIMS_URL."/api/v1/employer/SearchEmployer";
        $result = NULL;
        $result = $this->_sendCurl($url, $data);

        $fixed_result = str_replace("@attributes", "node", json_encode($result));

        echo $fixed_result;
    }

    function getPAN($eclaims_id=NULL) {
        if($_POST) {
            if(!isset($_POST['pDoctorTIN'])) {
                
            } else {
                $prof = new \stdClass();
                $doctor = new \stdClass();
                $prof->tin = $_POST['pDoctorTIN'];
                $doctor->last_name = $_POST['pDoctorLastName'];
                $doctor->first_name = $_POST['pDoctorFirstName'];
                $doctor->middle_name = $_POST['pDoctorMiddleName'];
                $doctor->suffix = $_POST['pDoctorSufix'];
                $doctor->birth_date = $_POST['pDoctorBirthDate'];
            }
        }
        if($eclaims_id) {
            $claim = Eclaims::where('eclaims_id', $eclaims_id)->first();
            $careid = $claim->healthcareservice_id;
            $healthcare = Healthcareservices::where("healthcareservice_id", $careid)->first();
            $doctor_id = $healthcare->seen_by;
            $doctor = Users::where("user_id", $doctor_id)->first();
            $prof = MDUsers::where("user_id", $doctor_id)->first();
        }
        
        if($doctor AND $prof) {
            $data['cipherKey'] = $this->CIPHER_KEY;
            $data['pUserName'] = $this->USERNAME;
            $data['pUserPassword'] = $this->USER_PASSWORD;
            $data['pHospitalCode'] = $this->HOSPITAL_CODE;
            $data['pDoctorTIN'] = $prof->tin;
            $data['pDoctorLastName'] = $doctor->last_name;
            $data['pDoctorFirstName'] = $doctor->first_name;
            $data['pDoctorMiddleName'] = $doctor->middle_name;
            $data['pDoctorSufix'] = $doctor->suffix;
            $data['pDoctorBirthDate'] = date("m-d-Y", strtotime($doctor->birth_date));
            
            $url = $this->ECLAIMS_URL."/api/v1/doctor/GetDoctorPAN";
            $result = NULL;
            $result = $this->_sendCurl($url, $data);
        } else {
            $result['ISOK'] = "NODOC";
        }
        echo json_encode($result);
    }

    function checkAccr($eclaims_id=NULL) {
        if($eclaims_id) {
            $claim = Eclaims::where('eclaims_id', $eclaims_id)->first();
            $careid = $claim->healthcareservice_id;
            $healthcare = Healthcareservices::where("healthcareservice_id", $careid)->first();
            $discharge = Disposition::where("healthcareservice_id", $careid)->first();
        }
        if(!$eclaims_id AND !$_POST) {
            $result['ISOK'] = "NODOC";
        } else {
            //let us check doctor accreditation
            $data['cipherKey'] = $this->CIPHER_KEY;
            $data['pUserName'] = $this->USERNAME;
            $data['pUserPassword'] = $this->USER_PASSWORD;
            $data['pHospitalCode'] = $this->HOSPITAL_CODE;
            $data['pDoctorAccreCode'] = $_POST['pDoctorAccreCode'];

            if($_POST['pAdmissionDate']) {
                $date = new DateTime(str_replace('/','-',$_POST['pAdmissionDate']));
                $data['pAdmissionDate'] = $date->format('m-d-Y');
            }
            
            if($_POST['pDischargeDate']) {
                $date = new DateTime(str_replace('/','-',$_POST['pDischargeDate']));
                $data['pDischargeDate'] = $date->format('m-d-Y');
            }
            $url = $this->ECLAIMS_URL."/api/v1/doctor/isDoctorAccredited";
            $result = NULL;
            $result = $this->_sendCurl($url, $data);
        }
        $fixed_result = str_replace("@attributes", "node", json_encode($result));

        return $fixed_result;
    }

    function uploadDocument($eclaimid, $form = NULL)
    {
        $claim = Eclaims::where('eclaims_id', $eclaimid)->first();
        $old_attachments = $claim->attachments;

        $destinationPath = 'public/uploads/eclaims/'.$eclaimid; // upload path
        $urlPath = upload_base_path().'eclaims/'.$eclaimid;
        
        $doc_type = Input::get('send_doc_type');
        $extension = Input::file('send_doc')->getClientOriginalExtension();

        // Set new filename
        if($form) {
            $newFileName = $form.'_'.$doc_type.'.'.$extension.'.'.'enc';
        } else {
            $newFileName = $doc_type.'.'.$extension.'.'.'enc';
        }

        // Prepare old file for encryption
        $originalName = Input::file('send_doc')->getClientOriginalName();
        $source = $urlPath.'/'.$originalName;

        Input::file('send_doc')->move($destinationPath, $originalName);

        //encrypt file
        $encryptor = new Encrypt;

        $publicKeyFileName = base_path().'/modules/Eclaims/Assets/libraries/pnpki_philhealth_eclaims_auth_cert.pem';
        $uxPassword1 = '';
        $uxPassword2 = '';
        $uxIV = '';

        $encryptor->setPublicKeyFileName($publicKeyFileName);
        $encryptor->setLoggingEnabled(TRUE);
        $encryptor->setPassword1UsingHexStr($uxPassword1);
        $encryptor->setPassword2UsingHexStr($uxPassword2);
        $encryptor->setIVUsingHexStr($uxIV);

        $enc = $encryptor->encryptImageFile($source, 'application/pdf', $urlPath.'/'.$newFileName);

        $result['status'] = "OK";

        $claim->attachments =  $old_attachments.$newFileName."|";
        $claim->save();

        echo json_encode($result);
    }

    function addDocument($eclaimid)
    {
        $claim = Eclaims::where('eclaims_id', $eclaimid)->first();
        $old_attachments = $claim->attachments;

        $pClaimSeriesLhios = json_decode($claim->pClaimSeriesLhio, TRUE);
        $pClaimSeriesLhio = $pClaimSeriesLhios[0]['pClaimSeriesLhio'];
        $DOCS = array(
            'pSeriesLhioNo' => $pClaimSeriesLhio,
            'pUserName' => $this->USERNAME,
            'pHospitalCode' => $this->HOSPITAL_CODE,
            'cipherKey' => $this->CIPHER_KEY
        );

        if(Input::has('add_doc_type'))
        {
            $destinationPath = 'public/uploads/eclaims/'.$eclaimid; // upload path
            $urlPath = upload_base_path().'eclaims/'.$eclaimid;
            
            $doc_type = Input::get('add_doc_type');
            $extension = Input::file('add_doc')->getClientOriginalExtension();

            // Set new filename
            $newFileName = $doc_type.'.'.$extension.'.'.'enc';

            // Prepare old file for encryption
            $originalName = Input::file('add_doc')->getClientOriginalName();
            $source = $urlPath.'/'.$originalName;

            Input::file('add_doc')->move($destinationPath, $originalName);

            //encrypt file
            $encryptor = new Encrypt;

            $publicKeyFileName = base_path().'/modules/Eclaims/Assets/libraries/pnpki_philhealth_eclaims_auth_cert.pem';
            $uxPassword1 = '';
            $uxPassword2 = '';
            $uxIV = '';

            $encryptor->setPublicKeyFileName($publicKeyFileName);
            $encryptor->setLoggingEnabled(TRUE);
            $encryptor->setPassword1UsingHexStr($uxPassword1);
            $encryptor->setPassword2UsingHexStr($uxPassword2);
            $encryptor->setIVUsingHexStr($uxIV);

            $enc = $encryptor->encryptImageFile($source, 'application/pdf', $urlPath.'/'.$newFileName);

            $DOCS['DOCUMENTS'][] = ['pDocType'=>$doc_type,'pDocumentURL'=>url().'/'.$destinationPath.'/'.$newFileName];
        }

        // CURL
        $data_string = json_encode($DOCS);
        $ch = curl_init($this->ECLAIMS_URL.'/api/v1/addRequiredDocument');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");           
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        $result = json_decode( $response, true );

        $claim->attachments =  $old_attachments.$newFileName."|";
        $claim->save();

        echo json_encode($result);
    }

    function attachments($eclaimid)
    {
        $claim = Eclaims::where('eclaims_id', $eclaimid)->first();

        $docsDir = upload_base_path().'eclaims/'.$eclaimid."/";
        $list = "<h3>Claim Document Attachments</h3><p>These are the files attached to this eClaim.</p><ol>";
        if(!file_exists($docsDir))
        {
            echo "<h3>Claim Document Attachments</h3>No attachments found for this claim!";
        }
        else
        {
            $docs = directoryFiles($docsDir);
            if($docs) {
                foreach($docs as $i=>$doc) {
                    if($doc[0] != '.') // to remove hidden files
                    {
                        $list .= "<li>".$doc."</li>";
                    }
                }
            }
            $list .= "</ol>";

            echo $list;
        }
    }

    /**
     * Send CURL
     * Param $url String - EClaims API URL
     * Param $data Array - Data Array from form
     * 
     * Controller that send the form data to the
     * EClaims API for submission to Philhealth
     * 
     * Returns the response of the Philhealth WebService
     * 
     */
    function _sendCurl($url, $data)
    {
        if(!$data) {
            return NULL;
        }

        $curl = curl_init();

        $postvars = '';
        foreach($data as $key=>$value) {
            if(is_array($value)) {
                $postvars .= $key . "=" . http_build_query($value) . "&";
            } else {
                $postvars .= $key . "=" . $value . "&";
            }
        }

        curl_setopt($curl,CURLOPT_URL,$url);
        curl_setopt($curl,CURLOPT_POST, 1);                //0 for a get request
        curl_setopt($curl,CURLOPT_POSTFIELDS,$postvars);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($curl,CURLOPT_TIMEOUT, 20);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            return $err;
        } else {
            return json_decode($response, true);
        }
    }

    function addReceipt($eclaimid)
    {
        // dd($_POST);

        $claim = Eclaims::where('eclaims_id', $eclaimid)->first();

        $RECEIPTS = ['RECEIPTS'=>[]];
        if(Input::has('RECEIPT'))
        {
            $input_receipts = Input::get('RECEIPT');
            foreach ($input_receipts as $receipt_key => $receipt_value) {

                // Change date format
                $receipt_value['pReceiptDate'] = date("m-d-Y", strtotime($receipt_value['pReceiptDate']));
                if(isset($receipt_value['ITEM']))
                {
                    $item_labels = $receipt_value['ITEM'];

                    $temp_items = [];
                    foreach ($item_labels as $item_label_key => $item_label_value) {
                        foreach ($item_label_value as $item_key => $item_value) {
                            $temp_items[$item_key][$item_label_key] = $item_value; 
                        }
                    }

                    $receipt_value['ITEM'] = $temp_items;
                }

                $RECEIPTS['RECEIPTS'][] = $receipt_value;
            }
        }

        $PARTICULARS = ['PARTICULARS'=>['DRGMED'=>[],'XLSO'=>[]]];
        if(Input::has('PARTICULARS'))
        {
            $particular_inputs = Input::get('PARTICULARS');
            if(isset($particular_inputs['DRGMED']))
            {
                foreach ($particular_inputs['DRGMED'] as $key => $drug) {
                    $drug['pPurchaseDate'] = date("m-d-Y", strtotime($drug['pPurchaseDate']));
                    $PARTICULARS['PARTICULARS']['DRGMED'][] = $drug;
                }
            }
            if(isset($particular_inputs['XLSO']))
            {
                foreach ($particular_inputs['XLSO'] as $key => $lab) {
                    $lab['pDiagnosticDate'] = date("m-d-Y", strtotime($lab['pDiagnosticDate']));
                    $PARTICULARS['PARTICULARS']['XLSO'][] = $lab;
                }
            }
        }

        $claim->particulars = json_encode($PARTICULARS);
        $claim->receipts = json_encode($RECEIPTS);
        $claim->save();

        return Redirect::to("eclaims/fillform/".$eclaimid."/receipts");
    }

    function buildTime($hh_am, $hh_pm)
    {
        if($hh_am != NULL AND $hh_am != '')
        {
            return $hh_am."AM";
        }
        elseif($hh_pm != NULL AND $hh_pm != '')
        {
            return $hh_pm."PM";
        }
        else
        {
            return "";        
        }

    }

    function getCF1Form()
    {
        $eclaimData['cipherKey'] = $this->CIPHER_KEY;
        $eclaimData['pUserName'] = $this->USERNAME;
        $eclaimData['pUserPassword'] = $this->USER_PASSWORD;
        $eclaimData['pHospitalCode'] = $this->HOSPITAL_CODE;
        $eclaimData['pPIN'] = $_POST['pin'];
        $eclaimData['pMemberLastName'] = strtoupper($_POST['pMemberLastName']);
        $eclaimData['pMemberFirstName'] = strtoupper($_POST['pMemberFirstName']);
        $eclaimData['pMemberMiddleName'] = strtoupper($_POST['pMemberMiddleName']);
        $eclaimData['pMemberSuffix'] = strtoupper($_POST['pMemberSuffix']);
        $eclaimData['pMemberBirthDate'] = $_POST['mon']."-".$_POST['dd']."-".$_POST['yy'];
        $eclaimData['pMemberSex'] = $_POST['pMemberSex'] ?? NULL;
        
        $unit = ($_POST['unit'] != '') ? $_POST['unit']." " : NULL;
        $building = ($_POST['building'] != '') ? $_POST['building']." " : NULL;
        $housenumber = ($_POST['housenumber'] != '') ? $_POST['housenumber']." " : NULL;
        $street = ($_POST['street'] != '') ? $_POST['street']." " : NULL;
        $village = ($_POST['village'] != '') ? $_POST['village']." " : NULL;

        $eclaimData['pMailingAddress'] = $unit.$building.$housenumber.$street.$village.$_POST['barangay']." ".$_POST['city']." ".$_POST['province']." ".$_POST['country'];            
        $eclaimData['pZipCode'] = $_POST['pZipCode'];
        $eclaimData['pAdmissionDate'] = date("m-d-Y", strtotime($_POST['pAdmissionDate']));
        $eclaimData['pDischargeDate'] = date("m-d-Y", strtotime($_POST['pDischargeDate']));
        
        $membership = "M";
        if(isset($_POST['pPatientIs']) AND $_POST['pPatientIs'] == "MM") {
            $membership = "M";
            $eclaimData['pPatientIs'] = $membership;
        }

        if(isset($_POST['pPatientIs']) AND $_POST['pPatientIs'] == 'DD') {
            $eclaimData['pPatientIs'] = isset($_POST['dep_relationship']) ? $_POST['dep_relationship'] : '';
            $eclaimData['pPatientLastName'] = strtoupper($_POST['pPatientLastName']);
            $eclaimData['pPatientFirstName'] = strtoupper($_POST['pPatientFirstName']);
            $eclaimData['pPatientMiddleName'] = strtoupper($_POST['pPatientMiddleName']);
            $eclaimData['pPatientSuffix'] = strtoupper($_POST['pPatientSuffix']);
            $eclaimData['pPatientBirthDate'] = $_POST['dep_mon']."-".$_POST['dep_dd']."-".$_POST['dep_yy'];
            $eclaimData['pPatientSex'] = $_POST['pPatientSex'] ?? NULL;
        } else {
            $eclaimData['pPatientLastName'] = strtoupper($_POST['pMemberLastName']);
            $eclaimData['pPatientFirstName'] = strtoupper($_POST['pMemberFirstName']);
            $eclaimData['pPatientMiddleName'] = strtoupper($_POST['pMemberMiddleName']);
            $eclaimData['pPatientSuffix'] = strtoupper($_POST['pMemberSuffix']);
            $eclaimData['pPatientBirthDate'] = $_POST['mon']."-".$_POST['dd']."-".$_POST['yy'];
            $eclaimData['pPatientSex'] = $_POST['pMemberSex'] ?? NULL;
        }
        
        $eclaimData['pMemberShipType'] = $_POST['pMemberShipType'] ?? NULL;
        $eclaimData['pPEN'] = $_POST['epen'];
        $eclaimData['pEmployerContact'] = $_POST['pEmployerContact'];
        $eclaimData['pEmployerName'] = $_POST['pEmployerName'];
        $eclaimData['pRVS'] = $_POST['pRVS'];
        $eclaimData['pTotalAmountActual'] = $_POST['pTotalAmountActual'];
        $eclaimData['pTotalAmountClaimed'] = $_POST['pTotalAmountClaimed'];
        $eclaimData['pIsFinal'] = isset($_POST['pIsFinal']) ? $_POST['pIsFinal'] : '0';

        return $eclaimData;
    }

}


