<?php namespace Modules\Eclaims\Http\Controllers;

use Modules\Eclaims\Http\Controllers\EclaimsController;

use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientContacts;
use Plugins\Philhealth\PhilhealthModel;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Healthcareservices\Entities\Disposition;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderPrescription;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderLabExam;

use ShineOS\Core\Users\Entities\Users;
use ShineOS\Core\Users\Entities\MDUsers;

use Pingpong\Modules\Routing\Controller;
use Modules\Eclaims\Entities\Eclaims; //model
use Modules\Eclaims\Entities\EclaimsFacility;
use Shine\Libraries\IdGenerator;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\UserHelper;
use Shine\Libraries\Utils\Lovs;

use Shine\Libraries\ArrayToXML;

use Modules\Eclaims\Assets\libraries\PhilHealthEClaimsEncryptor as Encrypt; //model

use View, Response, Input, Datetime, DB, Redirect, Session, Config, Exception;
use File;

class ClaimController extends EclaimsController {

    /**
     * Check Eligibility
     * Param $patid String - Patient ID
     * Param $page String - Form Name
     * 
     * Controller that sends the CF1 Form data
     * to the EClaims API to submit data to 
     * Philhealth WebService
     * 
     * Returns the response of the Philhealth Web Service
     * for Claims Eligibility of a patient
     * 
     */
    function checkEligibility($patid=NULL, $careid=NULL, $page=NULL)
    {
        if($_POST) {

            if(Input::has('eclaims_id'))
            {
                $eclaims_id = Input::get('eclaims_id');
            }
            $pin = $_POST['pin'];
            $eclaimData = $this->getCF1Form();

            $url = $this->ECLAIMS_URL."/api/v1/isClaimEligible";
            $result = NULL;
            $result = $this->_sendCurl($url, $eclaimData);
            
            //save submitted data to DB
            if(isset($eclaims_id))
            {
                $ec = Eclaims::where('eclaims_id',$eclaims_id)->first();
            }
            else
            {
                $ec = new Eclaims;
                $ec->eclaims_id = $eclaims_id = IdGenerator::generateSmallId();
                $ec->cf1 = IdGenerator::generateSmallId()."-cf1";
            }
            $ec->pbef_date = date('Y-m-d H:i:s');
            $ec->patient_id = $_POST['patid'];
            $ec->healthcareservice_id = $_POST['careid'];
            $ec->HOSPITAL_CODE = $this->HOSPITAL_CODE;
            $ec->patient_pin = $pin;
            $ec->cf1_details = json_encode($eclaimData);
            $ec->cf1_sdata = json_encode($_POST);
            $ec->cf1_date = date('Y-m-d H:i:s');
            $facility = FacilityHelper::facilityInfo();
            $ec->facility_id = $facility->facility_id;
            $this->user = UserHelper::getUserInfo();
            $ec->user_id = $this->user->user_id;
            $ec->save();

            if(isset($result['@attributes']['ISOK'])) {

                //display eligible certificate
                $dat['page'] = 'eligible';
                $dat['title'] = "Claim Eligible";
                $dat['submit_button'] = "";
                $dat['print_button'] = "Print CF1";
                $dat['patient'] = Patients::where("patient_id", $patid)->first();
                $dat['patid'] = $patid;
                $dat['careid'] = $careid;
                $dat['eclaims_id'] = $eclaims_id;
                
                $pbef = array(
                    "pbef" => IdGenerator::generateSmallId()."-pbef",
                    "pbef_details" => json_encode($result),
                    "pbef_status" => $result['@attributes']['ISOK'],
                    "pTrackingNumber" => $result['@attributes']['TRACKING_NUMBER']
                );

                Eclaims::where('eclaims_id', $eclaims_id)->update($pbef);

                return view('eclaims::page', $dat);
            } else {
                
                //return to form and load submitted data and error notice
                $data['page'] = 'cf1';
                $data['eclaims_id'] = $eclaims_id;
                $data['title'] = "Claims Eligibility Check";
                $data['submit_button'] = "Check Eligibility";
                $data['print_button'] = "Print";
                $data['patient'] = Patients::where("patient_id", $patid)->first();
                $data['postdata'] = $_POST;
                if(isset($result['error']) AND $result['error'] == 'validation_error') {
                    $data['flash_message'] = 'Oops! A required field is missing. Please check your form again.';
                    $data['flash_type'] = 'alert-success alert-dismissible alert-absolute';
                } else {
                    $data['flash_message'] = 'Oops! Something went wrong. '.print_r($result);
                    $data['flash_type'] = 'alert-success alert-dismissible alert-absolute';
                }
                //return some data for debugging
                $data['debug_result'] = $result;
                $data['debug_post'] = $_POST;
                $data['debug_data'] = $eclaimData;
                $data['patid'] = $patid;
                $data['careid'] = $careid;

                 Eclaims::where('eclaims_id', $eclaims_id)->update(['pbef_status'=>'ERROR']);
                
                return view('eclaims::page', $data);
            }
            
            
        } else {
            $data['patid'] = NULL;
            $data['careid'] = NULL;
            if($patid AND $careid) {
                $data['patid'] = $patid;
                $data['careid'] = $careid;
            }
            $data['title'] = "Claims Eligibility Check";
            $data['submit_button'] = "Check Eligibility";
            $data['print_button'] = "Print";

            if($page) {
                $data['page'] = $page;   
            } else {
                $data['page'] = 'cf1';
                $data['eclaims_id'] = NULL;
            }

            return view('eclaims::page', $data);
        }
    }

    function viewSubmitted($eclaimid)
    {
        $claim = Eclaims::where('eclaims_id', $eclaimid)->first();
        $result = json_decode($claim, false);
        $cf1 = json_decode($claim->cf1_sdata, false);
        $cf2 = json_decode($claim->cf2_details, false);
        
        //dd($result->pTransmissionControlNumber, $result->pReceiptTicketNumber, $result->eclaim_file_date);
        $view = '
        <table width="100%">
            <tr><td colspan="2"><h3>E-Claim Submission</h3></td></tr>
            <tr>
                <td width="40%">Full Name : </td>
                <td>'.$cf2->pPatientFirstName." ".$cf2->pPatientLastName.'</td>
            </tr>
            <tr>
                <td width="40%">Claim File Date/Time : </td>
                <td>'.$result->eclaim_file_date.'</td>
            </tr>
            <tr>
                <td width="40%">Transmission Control Number : </td>
                <td>'.$result->pTransmissionControlNumber.'</td>
            </tr>
            <tr>
                <td width="40%">Receipt Ticket Number : </td>
                <td>'.$result->pReceiptTicketNumber.'</td>
            </tr>
            <tr>
                <td colspan=2><hr /></td>
            </tr>
            <tr>
                <td width="40%">Hospital Claim ID : </td>
                <td>'.$result->pClaimNumber.'</td>
            </tr>
        </table>
        ';
        echo $view;
    }

    function getClaimsMap($eclaimid,$afterSubmit=false)
    {
        $claim = Eclaims::where('eclaims_id', $eclaimid)->first();
        $cf1 = json_decode($claim->cf1_details, false);

        $eclaimData['cipherKey'] = $this->CIPHER_KEY;
        $eclaimData['pUserName'] = $this->USERNAME;
        $eclaimData['pUserPassword'] = $this->USER_PASSWORD;
        $eclaimData['pHospitalCode'] = $claim->HOSPITAL_CODE;
        $eclaimData['pReceiptTicketNumber'] = $claim->pReceiptTicketNumber;

        $url = $this->ECLAIMS_URL."/api/v1/GetUploadedClaimsMap";
        $result = NULL;
        $result = $this->_sendCurl($url, $eclaimData);
        $result = json_decode(str_replace('@attributes', 'attributes', json_encode($result)), TRUE);
        $data['dat'] = $result;
        if($result) {
            $data['maps'] = array();
            if(isset($result['MAPPING']))
            {
                if(count($result['MAPPING']) > 1)
                {
                    foreach ($result['MAPPING'] as $key => $mapping) {
                        $data['maps'][$key] = array();
                        $data['maps'][$key]['pClaimNumber'] = $mapping['attributes']['pClaimNumber'];
                        $data['maps'][$key]['pClaimSeriesLhio'] = $mapping['attributes']['pClaimSeriesLhio'];
                    }
                }
                else
                {
                    $data['maps'][0]['pClaimNumber'] = $result['MAPPING']['attributes']['pClaimNumber'];
                    $data['maps'][0]['pClaimSeriesLhio'] = $result['MAPPING']['attributes']['pClaimSeriesLhio'];
                }
                $claim->pClaimSeriesLhio = json_encode($data['maps']); 
                $claim->save();

                if($afterSubmit == false)
                {    
                    return view('eclaims::forms.mapping', $data);
                }
                else
                {
                    return $afterSubmit;
                }
            }
            else
            {
                dd($result);
            }
        }
    }

    /**
     * Get Claim Status
     * Param $eclaimid String - EClaim ID
     * 
     * This is the controller called by the route to
     * get the status of an eclaim
     * 
     * Returns an array of history status
     * 
     */
    function getClaimsStatus($eclaimid,$console=false)
    {
        $claim = Eclaims::where('eclaims_id', $eclaimid)->first();
        $old_status = $claim->eclaim_status;
        $cf1 = json_decode($claim->cf1_details, false);

        if($console)
        {
            $eclaims_facility = EclaimsFacility::where('hci_hospitalcode',$claim->HOSPITAL_CODE)->first();
        }
        else
        {
            $facility = Session::get('facility_details');
            $eclaims_facility = EclaimsFacility::where('hci_hospitalcode',$claim->HOSPITAL_CODE)->where('facility_id',$facility->facility_id)->first();
        }

        $eclaimData['pHospitalCode'] = $claim->HOSPITAL_CODE;
        $eclaimData['cipherKey'] = $eclaims_facility->hci_cipherkey;
        $eclaimData['pUserName'] = $eclaims_facility->hci_userid.":".getenv('ECLAIMS_SOFTWARECERTID');
        $eclaimData['pUserPassword'] = $eclaims_facility->hci_password;  
             
        $maps = json_decode($claim->pClaimSeriesLhio,TRUE);
        if(count($maps) >= 1) 
        {
            $eclaimData['pSeriesLhioNos'] = $maps[count($maps)-1]['pClaimSeriesLhio'];

            $url = $this->ECLAIMS_URL."/api/v1/GetClaimStatus";
            $result = NULL;
            $result = $this->_sendCurl($url, $eclaimData);
            $result = json_decode(str_replace('@attributes', 'attributes', json_encode($result)), TRUE);

            $claim->eclaim_status = $result['CLAIM']['attributes']['pStatus'] ?? $old_status;
            $claim->save();
            if(!$console)
            {
                try 
                {
                    if(isset($result['attributes'])) 
                    {

                        //return view('eclaims::forms.mapping', $data);
                        $view = '
                        <table width="100%">
                            <tr><td colspan="2"><h3>E-Claim Status Inquiry</h3></td></tr>
                            <tr>
                                <td width="40%">Status as of : </td>
                                <td>'.$result['attributes']['pAsOf'].'</td>
                            </tr>';

                        if(isset($result['CLAIM']))
                        {
                            $view .= '<tr>
                                <td width="40%">Claim LHIO Series : </td>
                                <td>'.$result['CLAIM']['attributes']['pClaimSeriesLhio'].'</td></tr>
                                <td width="40%">Patient Name : </td>
                                <td>'.$result['CLAIM']['attributes']['pPatientFirstName'].' '.$result['CLAIM']['attributes']['pPatientLastName'].'</td></tr>
                                <td width="40%">Claim Date Received : </td>
                                <td>'.$result['CLAIM']['attributes']['pClaimDateReceived'].'</td></tr>
                                <td width="40%">Claim Date Refile : </td>
                                <td>'.$result['CLAIM']['attributes']['pClaimDateRefile'].'</td></tr>
                                <td width="40%">Claim Status : </td>
                                <td>'.$result['CLAIM']['attributes']['pStatus'].'</td>
                                </tr>
                                <tr>
                                    <td colspan=2><hr /></td>
                                </tr>';
                            if(isset($result['CLAIM']['attributes']['pStatus']) AND $result['CLAIM']['attributes']['pStatus'] == 'DENIED') {
                                if(count($result['CLAIM']['DENIED']['REASON']) > 1)
                                {
                                    foreach ($result['CLAIM']['DENIED']['REASON'] as $key => $value) {
                                        if($key == 0)
                                        {
                                            $view .= '<tr>
                                                <td width="40%">Reasons: </td>
                                                <td>'.$result['CLAIM']['DENIED']['REASON'][$key]['attributes']['pReason'].'</td>
                                                </tr>';
                                        }
                                        else
                                        {
                                            $view .= '<tr>
                                                <td width="40%"></td>
                                                <td>'.$result['CLAIM']['DENIED']['REASON'][$key]['attributes']['pReason'].'</td>
                                                </tr>';
                                        }
                                    }
                                }
                                else
                                {
                                    $view .= '<tr>
                                    <td width="40%">Reason: </td>
                                    <td>'.$result['CLAIM']['DENIED']['REASON']['attributes']['pReason'].'</td>
                                    </tr>';   
                                }
                            }
                            elseif(isset($result['CLAIM']['attributes']['pStatus']) AND $result['CLAIM']['attributes']['pStatus'] == 'IN PROCESS') {
                                $view .= '<tr>
                                    <td width="40%">Process Stages: </td>
                                    <td></td>
                                    </tr>';
                                if(count($result['CLAIM']['TRAIL']['PROCESS']) > 1)
                                {
                                    foreach ($result['CLAIM']['TRAIL']['PROCESS'] as $key => $value) {
                                        $view .= '<tr>
                                            <td width="40%">['.$result['CLAIM']['TRAIL']['PROCESS'][$key]['attributes']['pProcessDate'].'] </td>
                                            <td>'.$result['CLAIM']['TRAIL']['PROCESS'][$key]['attributes']['pProcessStage'].'</td>
                                            </tr>';
                                    }
                                }
                                else
                                {
                                    $view .= '<tr>
                                    <td width="40%">['.$result['CLAIM']['TRAIL']['PROCESS']['attributes']['pProcessDate'].'] </td>
                                    <td>'.$result['CLAIM']['TRAIL']['PROCESS']['attributes']['pProcessStage'].'</td>
                                    </tr>';   
                                }
                            }
                            elseif(isset($result['CLAIM']['attributes']['pStatus']) AND $result['CLAIM']['attributes']['pStatus'] == 'RETURN') {
                                if(isset($result['CLAIM']['RETURN']['DEFECTS']['attributes']['pDeficiency']))
                                {
                                    $view .= '<tr>
                                    <td width="40%">Deficiency: </td>
                                    <td>'.$result['CLAIM']['RETURN']['DEFECTS']['attributes']['pDeficiency'].'</td>
                                    </tr>';
                                }
                                
                                if(isset($result['CLAIM']['RETURN']['DEFECTS']['attributes']) AND isset($result['CLAIM']['RETURN']['DEFECTS']['REQUIREMENT']))
                                {
                                    $reqArray = [];
                                    if(count($result['CLAIM']['RETURN']['DEFECTS']['REQUIREMENT']) > 1)
                                    {
                                        $reqArray = $result['CLAIM']['RETURN']['DEFECTS']['REQUIREMENT'];
                                    }
                                    else
                                    {
                                        $reqArray[] = $result['CLAIM']['RETURN']['DEFECTS']['REQUIREMENT'];
                                    }
                                    foreach ($reqArray as $key => $reqVal) {
                                        if($key == 0)
                                        {
                                            $view .= '<tr><td width="40%">Requirements:</td><td>'.$reqVal['attributes']['pRequirement'].'</td></tr>';
                                        }
                                        else
                                        {
                                            $view .= '<tr><td width="40%"></td><td>'.$reqVal['attributes']['pRequirement'].'</td></tr>';    
                                        }
                                    }
                                }
                                elseif((count($result['CLAIM']['RETURN']['DEFECTS']) > 1) AND !isset($result['CLAIM']['RETURN']['DEFECTS']['REQUIREMENT']))
                                {
                                    foreach ($result['CLAIM']['RETURN']['DEFECTS'] as $key => $value) {
                                        if($key == 0)
                                        {
                                            $view .= '<tr>
                                                <td width="40%">Requirements: </td>
                                                <td>'.$result['CLAIM']['RETURN']['DEFECTS'][$key]['attributes']['pDeficiency'].'</td>
                                                </tr>';    
                                        }
                                        else
                                        {
                                            $view .= '<tr>
                                                <td width="40%"></td>
                                                <td>'.$result['CLAIM']['RETURN']['DEFECTS'][$key]['attributes']['pDeficiency'].'</td>
                                                </tr>';                                        
                                        }
                                    }
                                }
                            }
                            elseif(isset($result['CLAIM']['attributes']['pStatus']) AND ($result['CLAIM']['attributes']['pStatus'] == 'VOUCHERING' OR $result['CLAIM']['attributes']['pStatus'] == 'WITH VOUCHER' OR $result['CLAIM']['attributes']['pStatus'] == 'WITH CHEQUE')) {
                                if(count($result['CLAIM']['PAYMENT']['PAYEE']) > 1)
                                {
                                    foreach ($result['CLAIM']['PAYMENT']['PAYEE'] as $key => $value) {
                                        $view .= '<tr><td width="40%">Payee '.($key+1).': </td><td></td></tr>';
                                        $view .= '<tr><td width="40%">Voucher No : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE'][$key]['attributes']['pVoucherNo'].'</td></tr>'; 
                                        $view .= '<tr><td width="40%">Voucher Date : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE'][$key]['attributes']['pVoucherDate'].'</td></tr>';
                                        $view .= '<tr><td width="40%">Check No : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE'][$key]['attributes']['pCheckNo'].'</td></tr>'; 
                                        $view .= '<tr><td width="40%">Check Date : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE'][$key]['attributes']['pCheckDate'].'</td></tr>'; 
                                        $view .= '<tr><td width="40%">Check Amount : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE'][$key]['attributes']['pCheckAmount'].'</td></tr>'; 
                                        $view .= '<tr><td width="40%">Claim Amount : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE'][$key]['attributes']['pClaimAmount'].'</td></tr>'; 
                                        $view .= '<tr><td width="40%">Claim Payee Name : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE'][$key]['attributes']['pClaimPayeeName'].'</td></tr>';    
                                    }
                                }
                                else
                                { 
                                    $view .= '<tr><td width="40%">Payee : </td><td></td></tr>';
                                    $view .= '<tr><td width="40%">Voucher No : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE']['attributes']['pVoucherNo'].'</td></tr>'; 
                                    $view .= '<tr><td width="40%">Voucher Date : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE']['attributes']['pVoucherDate'].'</td></tr>';
                                    $view .= '<tr><td width="40%">Check No : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE']['attributes']['pCheckNo'].'</td></tr>'; 
                                    $view .= '<tr><td width="40%">Check Date : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE']['attributes']['pCheckDate'].'</td></tr>'; 
                                    $view .= '<tr><td width="40%">Check Amount : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE']['attributes']['pCheckAmount'].'</td></tr>'; 
                                    $view .= '<tr><td width="40%">Claim Amount : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE']['attributes']['pClaimAmount'].'</td></tr>'; 
                                    $view .= '<tr><td width="40%">Claim Payee Name : </td><td>'.$result['CLAIM']['PAYMENT']['PAYEE']['attributes']['pClaimPayeeName'].'</td></tr>';  
                                }
                            }
                            else
                            {
                                dd($result);
                            }
                        }
                        $view .= '</table>';
                        echo $view;
                    }
                    else
                    {
                        dd($result);
                    }                    
                } catch (Exception $e) {
                    dd($result);
                }

                if(isset($result) AND $result AND isset($result->error)) {
                    echo $result->message[0];
                }
            }
        }
        else
        {
            if(!$console)
            {
                echo "<h3>No Claim Series LHIO Number found!</h3><br><p>Please run Get Claims Map first to claim your Claim Series LHIO Number.</p>";
            }
        }

    }

    function inquireVoucher($eclaimid=NULL)
    {
        $eclaimData['cipherKey'] = $this->CIPHER_KEY;
        $eclaimData['pUserName'] = $this->USERNAME;
        $eclaimData['pUserPassword'] = $this->USER_PASSWORD;
        $eclaimData['pHospitalCode'] = $this->HOSPITAL_CODE;
        $eclaimData['pVoucherNo'] = $_POST['pVoucherNo'];

        // Test Voucher: 201-110001-18I12
        $url = $this->ECLAIMS_URL."/api/v1/GetVoucherDetails";
        $result = NULL;
        $result = $this->_sendCurl($url, $eclaimData);
        
        $decoded = is_array($result) ? $result : json_decode($result,TRUE);
        if(isset($decoded['@attributes']) && isset($decoded['SUMMARY']))
        {
            $data['voucher'] = $decoded;
            $view = View::make('eclaims::responses.voucher')->with($data)->render();
            $response = array(
                'ISOK'=>'success',
                'MESSAGE'=>$view
            );            
        }
        elseif(isset($decoded['ISOK']) && $decoded['ISOK']=='error' && isset($decoded['MESSAGE']))
        {
            $response = array(
                'ISOK'=>'error',
                'MESSAGE'=>$decoded['MESSAGE']
            );            
        }
        else
        {
            $response = array(
                'ISOK'=>'error',
                'MESSAGE'=>"Sorry, something went wrong. Please try again later."
            );             
        }

        echo json_encode($response);
        //print("<h4>Voucher Details</h4><br /><pre>".print_r($result,true)."</pre>");
    }

    /**
     * Submit Claim
     * Param $eclaimid String - EClaim ID
     * 
     * This is the controller called by the route to
     * show submitted forms for viewing
     * 
     */
    function submitClaim($eclaimid)
    {
        try
        {
            $error_check = '';

            //$url = "http://104.236.3.75/api/v1/eClaimsUpload";
            //collect all data from Forms CF1, CF2, CF3 and format for submission
            $claim = Eclaims::where('eclaims_id', $eclaimid)->first();
            
            $pbef = json_decode($claim->pbef_details, true);
            $cf1dForm = json_decode($claim->cf1_details, true);
            $cf1Form = json_decode($claim->cf1_sdata, true);
            $cf2Form = json_decode($claim->cf2_details, true);

            if(!isset($cf2Form['pEnoughBenefits'])){
                $cf2Form['pEnoughBenefits'] = 0;
            }

            if(!isset($cf2Form['pWithCoPay_1'])){
                $cf2Form['pWithCoPay_1'] = 0;
            }

            $data['care'] = $care =  Healthcareservices::with('GeneralConsultation')->with('VitalsPhysical')->with('Diagnosis')->with('Disposition')->with('MedicalOrder')->where("healthcareservice_id", $claim->healthcareservice_id)->first();

            $facility = Facilities::getCurrentFacility($this->facility->facility_id);
            $pHTN = ($claim->pHospitalTransmittalNo == NULL) ? IdGenerator::generateSmallId() : $claim->pHospitalTransmittalNo;
            if($claim->pClaimSeriesLhio) {
                $pClaimSeriesLhios = json_decode($claim->pClaimSeriesLhio,TRUE); 
                $pCN = $pClaimSeriesLhios[count($pClaimSeriesLhios)-1]['pClaimNumber'];
            }
            else {
                $pCN = IdGenerator::generateSmallId(0);
            }

            // Check admission date and time in CF2
            if(isset($cf2Form['ad_ampm']) && isset($cf2Form['dc_ampm']))
            {
                $ad_ampm = $cf2Form['ad_ampm'];
                $dc_ampm = $cf2Form['dc_ampm'];
            }
            else
            {
                $ad_ampm = $dc_ampm = NULL;
                $error_check = 'ERROR';
                echo "<h3>Incomplete Data!</h3><br>".nl2br( htmlentities("Please complete admission date/time and disharge date/time in CF2."));
            }

            $eCLAIMS = [
                'cipherKey' => $this->CIPHER_KEY,
                'pUserName' => $this->USERNAME,
                'pUserPassword' => $this->USER_PASSWORD,
                'pHospitalCode' => $this->HOSPITAL_CODE,
                'pHospitalEmail' => $facility->facilityContact->email_address,
                'pServiceProvider' => $facility->facility_name,
                'pCertificateId' => $this->SOFTWARECERTID, //software validation certificate number
                'pXML' => [
                    'pHospitalTransmittalNo' => $pHTN,
                    'pTotalClaims' => 1,
                    'CLAIM' => [
                        'pClaimNumber' => $pCN,
                        'pTrackingNumber' => str_replace("-", "",$pbef['@attributes']['TRACKING_NUMBER']),
                        //'pCataractPreAuth' => "",
                	        'pPhilhealthClaimType' => "ALL-CASE-RATE",
                        // 'pPatientType' => $cf1Form['pPatientType'],
                        'pPatientType' => "O",
                        'pIsEmergency' => "N",
                        'CF1' => [
                            'pMemberPIN' => $cf1Form['pin'],
                            'pMemberLastName' => isset($cf1dForm['pMemberLastName']) ? strtoupper($cf1dForm['pMemberLastName']) : strtoupper($cf1Form['pMemberLastName']),
                            'pMemberFirstName' => isset($cf1dForm['pMemberFirstName']) ? strtoupper($cf1dForm['pMemberFirstName']) : strtoupper($cf1Form['pMemberFirstName']),
                            'pMemberMiddleName' => isset($cf1dForm['pMemberMiddleName']) ? strtoupper($cf1dForm['pMemberMiddleName']) : strtoupper($cf1Form['pMemberMiddleName']),
                            'pMemberSuffix' => isset($cf1dForm['pMemberSuffix']) ? strtoupper($cf1dForm['pMemberSuffix']) : strtoupper($cf1Form['pMemberSuffix']),
                            'pMemberBirthDate' => isset($cf1dForm['pMemberBirthDate']) ? $cf1dForm['pMemberBirthDate'] : $cf1Form['mon']."-".$cf1Form['dd']."-".$cf1Form['yy'],
                            'pMemberShipType' => isset($cf1dForm['pMemberShipType']) ? $cf1dForm['pMemberShipType'] : $cf1Form['pMemberShipType'],
                            'pMailingAddress' => isset($cf1dForm['pMailingAddress']) ? $cf1dForm['pMailingAddress'] : $cf1Form['unit']." ".$cf1Form['building']." ".$cf1Form['housenumber']." ".$cf1Form['street'].", ".$cf1Form['village'].", ".$cf1Form['barangay']." ".$cf1Form['city']." ".$cf1Form['province']." ".$cf1Form['country'],
                            'pZipCode' => isset($cf1dForm['pZipCode']) ? $cf1dForm['pZipCode'] : $cf1Form['pZipCode'],
                            'pMemberSex' => isset($cf1dForm['pMemberSex']) ? $cf1dForm['pMemberSex'] : $cf1Form['pMemberSex'],
                            'pLandlineNo' => $cf1Form['pLandlineNo'],
                            'pMobileNo' => $cf1Form['pMobileNo'],
                            'pEmailAddress' => $cf1Form['pEmailAddress'],
                            'pPatientIs' => $cf1dForm['pPatientIs'],
                            'pPatientPIN' => isset($cf1Form['pPatientPIN']) ? $cf1Form['pPatientPIN'] : $cf1Form['pin'],
                            'pPatientLastName' => isset($cf1dForm['pPatientLastName']) ? strtoupper($cf1Form['pPatientLastName']) : strtoupper($cf1dForm['pMemberLastName']),
                            'pPatientFirstName' => isset($cf1dForm['pPatientFirstName']) ? strtoupper($cf1Form['pPatientFirstName']) : strtoupper($cf1dForm['pMemberFirstName']),
                            'pPatientMiddleName' => isset($cf1dForm['pPatientMiddleName']) ? strtoupper($cf1Form['pPatientMiddleName']) : strtoupper($cf1dForm['pMemberMiddleName']),
                            'pPatientSuffix' => isset($cf1dForm['pPatientSuffix']) ? strtoupper($cf1Form['pPatientSuffix']) : strtoupper($cf1dForm['pMemberSuffix']),
                            'pPatientBirthDate' => $cf1dForm['pPatientBirthDate'],
                            'pPatientSex' => isset($cf1dForm['pPatientSex']) ? $cf1dForm['pPatientSex'] : isset($cf1dForm['pMemberSex']) ? $cf1dForm['pMemberSex'] : $cf1Form['pMemberSex']
                            //Employer below
                        ],
                        'CF2' => [
                            'pPatientReferred' => isset($cf2Form['pPatientReferred']) ? $cf2Form['pPatientReferred'] : "",
                	            'pReferredIHCPAccreCode' => isset($cf2Form['pReferredIHCPAccreCode']) ? $cf2Form['pReferredIHCPAccreCode'] : "",
                	            'pAdmissionDate' => $cf2Form['ad_mon']."-".$cf2Form['ad_dd']."-".$cf2Form['ad_yy'],
                            'pDischargeDate' => $cf2Form['dc_mon']."-".$cf2Form['dc_dd']."-".$cf2Form['dc_yy'],
                            'pAdmissionTime' => $cf2Form['ad_hh'].":".$cf2Form['ad_min'].":00 ".$ad_ampm,
                            'pDischargeTime' => $cf2Form['dc_hh'].":".$cf2Form['dc_min'].":00 ".$dc_ampm,
                            'pDisposition' => $cf2Form['pDisposition'],
                            'pExpiredDate' => (isset($cf2Form['expr_mon']) AND $cf2Form['expr_mon']!="") ? $cf2Form['expr_mon']."-".$cf2Form['expr_dd']."-".$cf2Form['expr_yy'] : "",
                            'pExpiredTime' => (isset($cf2Form['expr_hh']) AND $cf2Form['expr_hh']!="") ? $cf2Form['expr_hh'].":".$cf2Form['expr_min'].":"."00 ".$cf2Form['expr_ampm'] : "",
                            'pReferralIHCPAccreCode' => isset($cf2Form['pReferralIHCPAccreCode']) ? $cf2Form['pReferralIHCPAccreCode'] : "",
                            'pReferralReasons' => isset($cf2Form['pReferralReasons']) ? $cf2Form['pReferralReasons'] : "",
                            'pAccommodationType' => isset($cf2Form['pAccommodationType']) ? $cf2Form['pAccommodationType'] : "P",
                            'pHasAttachedSOA' => isset($cf2Form['pHasAttachedSOA']) ? $cf2Form['pHasAttachedSOA'] : "N",
                            'DIAGNOSIS' => [
                                'pAdmissionDiagnosis' => $cf2Form['pAdmissionDiagnosis']
                                //discharge diagnosis loop here - see below
                            ],
                            'SPECIAL' => [],
                            'CONSUMPTION' => [
                                'pEnoughBenefits' => $cf2Form['pEnoughBenefits']
                            ],
                            'APR' => [
                                'APRBYPATREPSIG' => [
                                    'pDateSigned' => $cf2Form['apr_signed_mon']."-".$cf2Form['apr_signed_dd']."-".$cf2Form['apr_signed_yy']
                                ]
                            ]
                        ],
                        'PARTICULARS' => [
                        ],
                        'RECEIPTS' => [
                        ],
                        'DOCUMENTS' => [
                        ]
                    ]
                ]
            ];
            // PARTICULARS
            $particulars = json_decode($claim->particulars,TRUE)['PARTICULARS'];
            $eCLAIMS['pXML']['CLAIM']['PARTICULARS'] = $particulars ? $particulars : [];

            // RECEIPTS
            $receipts = json_decode($claim->receipts,TRUE)['RECEIPTS'];
            $eCLAIMS['pXML']['CLAIM']['RECEIPTS']['RECEIPT'] = $receipts ? $receipts : [];


            $dischargeVal = "";
            $discharge_arr = [];
            for($x = 0; $x <= 5; $x++) {
                $space = in_array($x,[1,2,4,5]) ? " " : "";
                $letter = $x <= 2 ? 'a' : 'b';
                $dischargeVal = $x == 3 ? "" : $dischargeVal;
                if(strlen($cf2Form['pDischargeDiagnosis'][$x]) >= 1) {
                    $dischargeVal .= $space.$cf2Form['pDischargeDiagnosis'][$x];
                    $discharge[$letter]['pDischargeDiagnosis'] = $dischargeVal;
                }
                if($cf2Form['pICDCode'][$x]!="")
                {
                    $discharge[$letter]['ICDCODE'][] = $cf2Form['pICDCode'][$x];
                }
                if($cf2Form['pRVSCode'][$x]!="")
                {
                    $discharge[$letter]['RVSCODES'][] = [
                        'pRelatedProcedure' => $cf2Form['pRelatedProcedure'][$x],
                        'pRVSCode' => $cf2Form['pRVSCode'][$x],
                        'pProcedureDate' =>  date("m-d-Y", strtotime($cf2Form['pProcedureDate'][$x])),
                        'pLaterality' => isset($cf2Form['pLaterality'][$x]) ? $cf2Form['pLaterality'][$x] : "N"
                    ];
                }
            }
            if(isset($discharge['a']))
            {
                if((isset($discharge['a']['ICDCODE']) OR isset($discharge['a']['RVSCODES'])) AND !isset($discharge['a']['pDischargeDiagnosis']))
                {
                    $discharge['a']['pDischargeDiagnosis'] = "";
                }
                $eCLAIMS['pXML']['CLAIM']['CF2']['DIAGNOSIS']['DISCHARGE'][] = $discharge['a'];
            }
            if(isset($discharge['b']))
            {
                if((isset($discharge['b']['ICDCODE']) OR isset($discharge['b']['RVSCODES'])) AND !isset($discharge['b']['pDischargeDiagnosis']))
                {
                    $discharge['b']['pDischargeDiagnosis'] = "";
                }
                $eCLAIMS['pXML']['CLAIM']['CF2']['DIAGNOSIS']['DISCHARGE'][] = $discharge['b'];
            }

            //EMPLOYER
            if(isset($cf1Form['epen']) AND $cf1Form['epen'] != "") {
                $eCLAIMS['pXML']['CLAIM']['CF1']['pPEN'] = isset($cf1Form['epen']) ? $cf1Form['epen'] : "";
                $eCLAIMS['pXML']['CLAIM']['CF1']['pEmployerName'] = isset($cf1Form['pEmployerName']) ? $cf1Form['pEmployerName'] : "";
            }
            
            //PROCEDURES
            if(isset($cf2Form['HEMODIALYSIS_pSessionDate']) AND $cf2Form['HEMODIALYSIS_pSessionDate']) {
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['PROCEDURES']['HEMODIALYSIS']['SESSIONS']['pSessionDate'] = date("m-d-Y", strtotime($cf2Form['HEMODIALYSIS_pSessionDate']));
            }
            if(isset($cf2Form['CHEMOTHERAPY_pSessionDate']) AND $cf2Form['CHEMOTHERAPY_pSessionDate']) {
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['PROCEDURES']['CHEMOTHERAPY']['SESSIONS']['pSessionDate'] = date("m-d-Y", strtotime($cf2Form['CHEMOTHERAPY_pSessionDate']));
            }
            if(isset($cf2Form['BLOODTRANSFUSION_pSessionDate']) AND $cf2Form['BLOODTRANSFUSION_pSessionDate']) {
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['PROCEDURES']['BLOODTRANSFUSION']['SESSIONS']['pSessionDate'] = date("m-d-Y", strtotime($cf2Form['BLOODTRANSFUSION_pSessionDate']));
            }
            if(isset($cf2Form['PERITONEALDIALYSIS_pSessionDate']) AND $cf2Form['PERITONEALDIALYSIS_pSessionDate']) {
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['PROCEDURES']['PERITONEALDIALYSIS']['SESSIONS']['pSessionDate'] = date("m-d-Y", strtotime($cf2Form['PERITONEALDIALYSIS_pSessionDate']));
            }
            if(isset($cf2Form['BRACHYTHERAPY_pSessionDate']) AND $cf2Form['BRACHYTHERAPY_pSessionDate']) {
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['PROCEDURES']['BRACHYTHERAPY']['SESSIONS']['pSessionDate'] = date("m-d-Y", strtotime($cf2Form['BRACHYTHERAPY_pSessionDate']));
            }
            if(isset($cf2Form['LINAC_pSessionDate']) AND $cf2Form['LINAC_pSessionDate']) {
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['PROCEDURES']['LINAC']['SESSIONS']['pSessionDate'] = date("m-d-Y", strtotime($cf2Form['LINAC_pSessionDate']));
            }
            if(isset($cf2Form['COBALT_pSessionDate']) AND $cf2Form['COBALT_pSessionDate']) {
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['PROCEDURES']['COBALT']['SESSIONS']['pSessionDate'] = date("m-d-Y", strtotime($cf2Form['COBALT_pSessionDate']));
            }
            if(isset($cf2Form['SIMPLEDEBRIDEMENT_pSessionDate']) AND $cf2Form['SIMPLEDEBRIDEMENT_pSessionDate']) {
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['PROCEDURES']['SIMPLEDEBRIDEMENT']['SESSIONS']['pSessionDate'] = date("m-d-Y", strtotime($cf2Form['SIMPLEDEBRIDEMENT_pSessionDate']));
            }

            //MCP Package
            $maternalclaim = false;
            $mcpFields = ["pCheckUpDate1","pCheckUpDate2","pCheckUpDate3","pCheckUpDate4"];
            foreach($mcpFields as $mcpField)
            {
                if($cf2Form[$mcpField])
                {
                    $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['MCP'][] = date("m-d-Y", strtotime($cf2Form[$mcpField]));            
                    $maternalclaim = true;            
                }
            }


            //TBDOTS
            if(isset($cf2Form['pTBType']) AND $cf2Form['pTBType']) {
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['TBDOTS'] = [
                    'pTBType' => $cf2Form['pTBType'],
                    'pNTPCardNo' => $cf2Form['pNTPCardNo']
                ];
            }

            //ANIMAL BITE
            if(isset($cf2Form['pDay0ARV']) AND $cf2Form['pDay0ARV']){
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['ABP'] = [
                    'pDay0ARV' => date("m-d-Y", strtotime($cf2Form['pDay0ARV'])),
                    'pDay3ARV' => date("m-d-Y", strtotime($cf2Form['pDay3ARV'])),
                    'pDay7ARV' => date("m-d-Y", strtotime($cf2Form['pDay7ARV'])),
                    'pRIG' => date("m-d-Y", strtotime($cf2Form['pRIG'])),
                    'pABPOthers' => date("m-d-Y", strtotime($cf2Form['pABPOthers'])),
                    'pABPSpecify' => "pAPBSpecify"
                ];
            }

            //NCP
            if(isset($cf2Form['pEssentialNewbornCare']) OR isset($cf2Form['pNewbornHearingScreeningTest']) OR isset($cf2Form['pNewbornScreeningTest'])) 
            {
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['NCP']['pEssentialNewbornCare'] = $cf2Form['pEssentialNewbornCare'] ?? "N";
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['NCP']['pNewbornHearingScreeningTest'] = $cf2Form['pNewbornHearingScreeningTest'] ?? "N";
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['NCP']['pNewbornScreeningTest'] = $cf2Form['pNewbornScreeningTest'] ?? "N";
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['NCP']['pFilterCardNo'] = $cf2Form['pFilterCardNo'] ?? "";
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['NCP']['ESSENTIAL'] = [
                    'pDrying' => isset($cf2Form['pDrying']) ? $cf2Form['pDrying'] : "N",
            	    'pSkinToSkin' => isset($cf2Form['pSkinToSkin']) ? $cf2Form['pSkinToSkin'] : "N",
            	    'pCordClamping' => isset($cf2Form['pCordClamping']) ? $cf2Form['pCordClamping'] : "N",
            	    'pProphylaxis' => isset($cf2Form['pProphylaxis']) ? $cf2Form['pProphylaxis'] : "N",
            	    'pWeighing' => isset($cf2Form['pWeighing']) ? $cf2Form['pWeighing'] : "N",
            	    'pVitaminK' => isset($cf2Form['pVitaminK']) ? $cf2Form['pVitaminK'] : "N",
            	    'pBCG' => isset($cf2Form['pBCG']) ? $cf2Form['pBCG'] : "N",
            	    'pNonSeparation' => isset($cf2Form['pNonSeparation']) ? $cf2Form['pNonSeparation'] : "N",
            	    'pHepatitisB' => isset($cf2Form['pHepatitisB']) ? $cf2Form['pHepatitisB'] : "N"
                ];
            }

            //HIV
            if(isset($cf2Form['pLaboratoryNumber']) AND $cf2Form['pLaboratoryNumber']){
                $eCLAIMS['pXML']['CLAIM']['CF2']['SPECIAL']['HIVAIDS'] = [
                    'pLaboratoryNumber' => $cf2Form['pLaboratoryNumber']
                ];
            }

            //PROFESSIONALS
            $eCLAIMS['pXML']['CLAIM']['CF2']['PROFESSIONALS'] = array();
            for ($i=1; $i <= 3; $i++) { 
                if(isset($cf2Form['pWithCoPay_'.$i]))
                {
                   $eCLAIMS['pXML']['CLAIM']['CF2']['PROFESSIONALS'][] = [
                        'pDoctorAccreCode' => $cf2Form['doc_accr_'.$i],
                        'pDoctorLastName' => $cf2Form['doctor_lastname_'.$i],
                        'pDoctorFirstName' => $cf2Form['doctor_firstname_'.$i],
                        'pDoctorMiddleName' => $cf2Form['doctor_middlename_'.$i],
                        'pDoctorSuffix' => $cf2Form['doctor_suffix_'.$i],
                        'pWithCoPay' => $cf2Form['pWithCoPay_'.$i],
                        'pDoctorCoPay' => $cf2Form['pDoctorCoPay_'.$i],
                        'pDoctorSignDate' => $cf2Form['signed_mon_'.$i]."-".$cf2Form['signed_dd_'.$i]."-".$cf2Form['signed_yy_'.$i]
                    ];
                }            # code...
            }

            //BENFITS
            if( isset($cf2Form['pEnoughBenefits']) AND $cf2Form['pEnoughBenefits'] == 'Y' ) {
                $eCLAIMS['pXML']['CLAIM']['CF2']['CONSUMPTION']['BENEFITS'] = [
                    'pTotalHCIFees' => $cf2Form['pTotalHCIFees'],
                    'pTotalProfFees' => $cf2Form['pTotalProfFees'],
                    'pGrandTotal' => $cf2Form['pGrandTotal']
                ];
            }

            if( isset($cf2Form['pEnoughBenefits']) AND $cf2Form['pEnoughBenefits'] == 'N' ) {
                $eCLAIMS['pXML']['CLAIM']['CF2']['CONSUMPTION']['HCIFEES'] = [
                    'pTotalActualCharges' => isset($cf2Form['hci_pTotalActualCharges']) ? $cf2Form['hci_pTotalActualCharges'] : "",
                	    'pDiscount' => isset($cf2Form['hci_pDiscount']) ? $cf2Form['hci_pDiscount'] : "",
                	    'pPhilhealthBenefit' => isset($cf2Form['hci_pPhilhealthBenefit']) ? $cf2Form['hci_pPhilhealthBenefit'] : "",
                	    'pTotalAmount' => isset($cf2Form['hci_pTotalAmount']) ? $cf2Form['hci_pTotalAmount'] : "",
                	    'pMemberPatient' => isset($cf2Form['hci_pMemberPatient']) ? $cf2Form['hci_pMemberPatient'] : "",
                	    'pHMO' => isset($cf2Form['hci_pHMO']) ? $cf2Form['hci_pHMO'] : "",
                	    'pOthers' => isset($cf2Form['hci_pOthers']) ? $cf2Form['hci_pOthers'] : ""
                ];
                $eCLAIMS['pXML']['CLAIM']['CF2']['CONSUMPTION']['PROFFEES'] = [
                    'pTotalActualCharges' => isset($cf2Form['pf_pTotalActualCharges']) ? $cf2Form['pf_pTotalActualCharges'] : "",
                	    'pDiscount' => isset($cf2Form['pf_pDiscount']) ? $cf2Form['pf_pDiscount'] : "",
                	    'pPhilhealthBenefit' => isset($cf2Form['pf_pPhilhealthBenefit']) ? $cf2Form['pf_pPhilhealthBenefit'] : "",
                	    'pTotalAmount' => isset($cf2Form['pf_pTotalAmount']) ? $cf2Form['pf_pTotalAmount'] : "",
                	    'pMemberPatient' => isset($cf2Form['pf_pMemberPatient']) ? $cf2Form['pf_pMemberPatient'] : "",
                	    'pHMO' => isset($cf2Form['pf_pHMO']) ? $cf2Form['pf_pHMO'] : "",
                	    'pOthers' => isset($cf2Form['pf_pOthers']) ? $cf2Form['pf_pOthers'] : ""
                ];
                $eCLAIMS['pXML']['CLAIM']['CF2']['CONSUMPTION']['PURCHASES'] = [
                    'pDrugsMedicinesSupplies' => isset($cf2Form['pDrugsMedicinesSupplies']) ? $cf2Form['pDrugsMedicinesSupplies'] : "",
                	    'pDMSTotalAmount' => isset($cf2Form['pDMSTotalAmount']) ? $cf2Form['pDMSTotalAmount'] : "",
                	    'pExaminations' => isset($cf2Form['pExaminations']) ? $cf2Form['pExaminations'] : "",
                	    'pExamTotalAmount' => isset($cf2Form['pExamTotalAmount']) ? $cf2Form['pExamTotalAmount'] : ""
                ];
            }

            //APR
            $eCLAIMS['pXML']['CLAIM']['CF2']['APR']['APRBYPATREPSIG']['DEFINEDPATREPREL'] = [
                'pRelCode' => isset($cf2Form['pRelCode']) ? $cf2Form['pRelCode'] : "S"
            ];
            $eCLAIMS['pXML']['CLAIM']['CF2']['APR']['APRBYPATREPSIG']['OTHERREASONFORSIGNING'] = [
                'pReasonDesc' => isset($cf2Form['pReasonDesc']) ? $cf2Form['pReasonDesc'] : ""
            ];
            if( isset($cf2Form['pRelCode']) AND $cf2Form['pRelCode'] == 'O') {
                $eCLAIMS['pXML']['CLAIM']['CF2']['APR']['APRBYPATREPSIG']['OTHERREASONFORSIGNING'] = [
                    'pReasonDesc' => isset($cf2Form['pReasonDesc']) ? $cf2Form['pReasonDesc'] : ""
                ];
            }

            //CASERATE
            $caserate_return = ['STATUS'=>''];
            if($error_check != 'ERROR')
            {
                if(isset($cf2Form['FirstCaseRateCode']) AND $cf2Form['FirstCaseRateCode'] != '') {
                    //generate XML section for first case rate
                    $caserate_return['PRIMARY'] = $this->caseRateOnUpload($cf2Form['FirstCaseRateCode'],'PRIMARY');
                    if($caserate_return['PRIMARY']['ISOK'] == 'error')
                    {
                        $caserate_return['STATUS'] = 'ERROR';
                        echo "<h3>ERROR!</h3><br>".nl2br( htmlentities($caserate_return['PRIMARY']['MESSAGE'] ));           
                    }
                    else
                    {
                        $eCLAIMS['pXML']['CLAIM']['ALLCASERATE']['CASERATE'][0] = $caserate_return['PRIMARY']['data'];
                    }
                }
                if(isset($cf2Form['SecondCaseRateCode']) AND $cf2Form['SecondCaseRateCode'] != '') {
                    $caserate_return['SECONDARY'] = $this->caseRateOnUpload($cf2Form['SecondCaseRateCode'],'SECONDARY');
                    if($caserate_return['SECONDARY']['ISOK'] == 'error')
                    {
                        $caserate_return['STATUS'] = 'ERROR';
                        echo "<h3>ERROR!</h3><br>".nl2br( htmlentities($caserate_return['SECONDARY']['MESSAGE'] ));           
                    }
                    else
                    {
                        $eCLAIMS['pXML']['CLAIM']['ALLCASERATE']['CASERATE'][1] = $caserate_return['SECONDARY']['data'];
                    }
                }
            }
            
            // FOR CF3
            if($maternalclaim == true)
            {
                $getCf3Details = $claim->cf3_details;
                if($getCf3Details)
                {
                    $cf3Form = json_decode($getCf3Details, true);
                    $eCLAIMS['pXML']['CLAIM']['CF3'] = 
                        [
                            'CF3_OLD' => [
                                'pChiefComplaint' => isset($cf3Form['pChiefComplaint']) ? $cf3Form['pChiefComplaint'] : "",
                    	            'pBriefHistory' => isset($cf3Form['pBriefHistory']) ? preg_replace( "/\r|\n/", " ", $cf3Form['pBriefHistory']) : "",
                    	            'pCourseWard' => isset($cf3Form['pCourseWard']) ? $cf3Form['pCourseWard'] : "",
                    	            'pPertinentFindings' => isset($cf3Form['pPertinentFindings']) ? $cf3Form['pPertinentFindings'] : "",
                    	            'PHEX' => [
                                    'pBP' => isset($cf3Form['pBP']) ? $cf3Form['pBP'] : "",
                    	                'pCR' => isset($cf3Form['pCR']) ? $cf3Form['pCR'] : "",
                    	                'pRR' => isset($cf3Form['pRR']) ? $cf3Form['pRR'] : "",
                    	                'pTemp' => isset($cf3Form['pTemp']) ? $cf3Form['pTemp'] : "",
                    	                'pHEENT' => isset($cf3Form['pHEENT']) ? $cf3Form['pHEENT'] : "",
                    	                'pChestLungs' => isset($cf3Form['pChestLungs']) ? $cf3Form['pChestLungs'] : "",
                    	                'pCVS' => isset($cf3Form['pCVS']) ? $cf3Form['pCVS'] : "",
                    	                'pAbdomen' => isset($cf3Form['pAbdomen']) ? $cf3Form['pAbdomen'] : "",
                    	                'pGUIE' => isset($cf3Form['pGUIE']) ? $cf3Form['pGUIE'] : "",
                    	                'pSkinExtremities' => isset($cf3Form['pSkinExtremities']) ? $cf3Form['pSkinExtremities'] : "",
                    	                'pNeuroExam' => isset($cf3Form['pNeuroExam']) ? $cf3Form['pNeuroExam'] : ""
                                ],
                                'MATERNITY' => [
                                    'PRENATAL' => [
                                        'pPrenatalConsultation' => isset($cf3Form['pPrenatalConsultation']) ? $cf3Form['pPrenatalConsultation'] : "",
                    	                    'pMCPOrientation' => isset($cf3Form['pMCPOrientation']) ? $cf3Form['pMCPOrientation'] : "N",
                                        'pExpectedDeliveryDate' => (isset($cf3Form['edate_delv_mon']) AND isset($cf3Form['edate_delv_dd']) AND isset($cf3Form['edate_delv_yy'])) ? implode("-", [$cf3Form['edate_delv_mon'],$cf3Form['edate_delv_dd'],$cf3Form['edate_delv_yy']]) : "",
                    	                    'CLINICALHIST' => [
                                            'pVitalSigns' => isset($cf3Form['pVitalSigns']) ? $cf3Form['pVitalSigns'] : "N",
                                            'pPregnancyLowRisk' => isset($cf3Form['pPregnancyLowRisk']) ? $cf3Form['pPregnancyLowRisk'] : "N",
                                            'pLMP' => (isset($cf3Form['lmp_mon']) AND isset($cf3Form['lmp_dd']) AND isset($cf3Form['lmp_yy'])) ? implode("-", [$cf3Form['lmp_mon'],$cf3Form['lmp_dd'],$cf3Form['lmp_yy']]) : "",
                    	                        'pMenarcheAge' => isset($cf3Form['pMenarcheAge']) ? $cf3Form['pMenarcheAge'] : "",
                    	                        'pObstetricG' => isset($cf3Form['pObstetricG']) ? $cf3Form['pObstetricG'] : "",
                    	                        'pObstetricP' => isset($cf3Form['pObstetricP']) ? $cf3Form['pObstetricP'] : "",
                    	                        'pObstetric_T' => isset($cf3Form['pObstetric_T']) ? $cf3Form['pObstetric_T'] : "",
                    	                        'pObstetric_P' => isset($cf3Form['pObstetric_P']) ? $cf3Form['pObstetric_P'] : "",
                    	                        'pObstetric_A' => isset($cf3Form['pObstetric_A']) ? $cf3Form['pObstetric_A'] : "",
                    	                        'pObstetric_L' => isset($cf3Form['pObstetric_L']) ? $cf3Form['pObstetric_L'] : ""
                                        ],
                                        'OBSTETRIC' => [
                                            'pMultiplePregnancy' => isset($cf3Form['pMultiplePregnancy']) ? $cf3Form['pMultiplePregnancy'] : "N",
                                            'pOvarianCyst' => isset($cf3Form['pOvarianCyst']) ? $cf3Form['pOvarianCyst'] : "N",
                                            'pMyomaUteri' => isset($cf3Form['pMyomaUteri']) ? $cf3Form['pMyomaUteri'] : "N",
                                            'pPlacentaPrevia' => isset($cf3Form['pPlacentaPrevia']) ? $cf3Form['pPlacentaPrevia'] : "N",
                                            'pMiscarriages' => isset($cf3Form['pMiscarriages']) ? $cf3Form['pMiscarriages'] : "N",
                                            'pStillBirth' => isset($cf3Form['pStillBirth']) ? $cf3Form['pStillBirth'] : "N",
                                            'pPreEclampsia' => isset($cf3Form['pPreEclampsia']) ? $cf3Form['pPreEclampsia'] : "N",
                                            'pEclampsia' => isset($cf3Form['pEclampsia']) ? $cf3Form['pEclampsia'] : "N",
                                            'pPrematureContraction' => isset($cf3Form['pPrematureContraction']) ? $cf3Form['pPrematureContraction'] : "N"
                                        ],
                                        'MEDISURG' => [
                                            'pHypertension' => isset($cf3Form['pHypertension']) ? $cf3Form['pHypertension'] : "N",
                                            'pHeartDisease' => isset($cf3Form['pHeartDisease']) ? $cf3Form['pHeartDisease'] : "N",
                                            'pDiabetes' => isset($cf3Form['pDiabetes']) ? $cf3Form['pDiabetes'] : "N",
                                            'pThyroidDisaster' => isset($cf3Form['pThyroidDisaster']) ? $cf3Form['pThyroidDisaster'] : "N",
                                            'pObesity' => isset($cf3Form['pObesity']) ? $cf3Form['pObesity'] : "N",
                                            'pAsthma' => isset($cf3Form['pAsthma']) ? $cf3Form['pAsthma'] : "N",
                                            'pEpilepsy' => isset($cf3Form['pEpilepsy']) ? $cf3Form['pEpilepsy'] : "N",
                                            'pRenalDisease' => isset($cf3Form['pRenalDisease']) ? $cf3Form['pRenalDisease'] : "N",
                                            'pBleedingDisorders' => isset($cf3Form['pBleedingDisorders']) ? $cf3Form['pBleedingDisorders'] : "N",
                                            'pPreviousCS' => isset($cf3Form['pPreviousCS']) ? $cf3Form['pPreviousCS'] : "N",
                                            'pUterineMyomectomy' => isset($cf3Form['pUrineMyomectomy']) ? $cf3Form['pUrineMyomectomy'] : "N"
                                        ]
                                    ],
                                    'DELIVERY' => [
                                        'pDeliveryDate' => (isset($cf3Form['deliv_dc_mon']) AND isset($cf3Form['deliv_dc_dd']) AND isset($cf3Form['deliv_dc_yy'])) ? implode("-",[$cf3Form['deliv_dc_mon'],$cf3Form['deliv_dc_dd'],$cf3Form['deliv_dc_yy']]) : "",
                                        'pDeliveryTime' => $this->buildtime($cf3Form['deliv_hh_am'],$cf3Form['deliv_hh_pm']),
                                        'pObstetricIndex' => isset($cf3Form['pObstetricIndex']) ? $cf3Form['pObstetricIndex'] : "",
                    	                    'pAOGLMP' => isset($cf3Form['pAOGLMP']) ? $cf3Form['pAOGLMP'] : "",
                    	                    'pDeliveryManner' => isset($cf3Form['pDeliveryManner']) ? $cf3Form['pDeliveryManner'] : "",
                    	                    'pPresentation' => isset($cf3Form['pPresentation']) ? $cf3Form['pPresentation'] : "",
                    	                    'pFetalOutcome' => isset($cf3Form['pFetalOutcome']) ? $cf3Form['pFetalOutcome'] : "",
                    	                    'pSex' => isset($cf3Form['pSex']) ? $cf3Form['pSex'] : "",
                    	                    'pBirthWeight' => isset($cf3Form['pBirthWeight']) ? $cf3Form['pBirthWeight'] : "",
                    	                    'pAPGARScore' => isset($cf3Form['pAPGARScore']) ? $cf3Form['pAPGARScore'] : "",
                    	                    'pPostpartum' => isset($cf3Form['pPostpartum']) ? $cf3Form['pPostpartum'] : "",
                    	                ],
                                    'POSTPARTUM' => [
                                        'pPerinealWoundCare' => isset($cf3Form['pPerinealWoundCare']) ? $cf3Form['pPerinealWoundCare'] : "N",
                                        'pPerinealRemarks' => isset($cf3Form['pPerinealRemarks']) ? $cf3Form['pPerinealRemarks'] : "",
                    	                    'pMaternalComplications' => isset($cf3Form['pMaternalComplications']) ? $cf3Form['pMaternalComplications'] : "N",
                                        'pMaternalRemarks' => isset($cf3Form['pMaternalRemarks']) ? $cf3Form['pMaternalRemarks'] : "",
                    	                    'pBreastFeeding' => isset($cf3Form['pBreastFeeding']) ? $cf3Form['pBreastFeeding'] : "N",
                                        'pBreastFeedingRemarks' => isset($cf3Form['pBreastFeedingRemarks']) ? $cf3Form['pBreastFeedingRemarks'] : "",
                    	                    'pFamilyPlanning' => isset($cf3Form['pFamilyPlanning']) ? $cf3Form['pFamilyPlanning'] : "N",
                                        'pFamilyPlanningRemarks' => isset($cf3Form['pFamilyPlanningRemarks']) ? $cf3Form['pFamilyPlanningRemarks'] : "",
                    	                    'pPlanningService' => isset($cf3Form['pPlanningService']) ? $cf3Form['pPlanningService'] : "N",
                                        'pPlanningServiceRemarks' => isset($cf3Form['pPlanningServiceRemarks']) ? $cf3Form['pPlanningServiceRemarks'] : "",
                    	                    'pSurgicalSterilization' => isset($cf3Form['pSurgicalSterilization']) ? $cf3Form['pSurgicalSterilization'] : "N",
                                        'pSterilizationRemarks' => isset($cf3Form['pSterilizationRemarks']) ? $cf3Form['pSterilizationRemarks'] : "",
                    	                    'pFollowupSchedule' => isset($cf3Form['pFollowupSchedule']) ? $cf3Form['pFollowupSchedule'] : "N",
                                        'pFollowupScheduleRemarks' => isset($cf3Form['pFollowupScheduleRemarks']) ? $cf3Form['pFollowupScheduleRemarks'] : ""
                                    ]
                                ]
                            ]
                        ];
                    //CF3 Consultation
                    if(isset($cf3Form['pVisitDate_mon']) AND $cf3Form['pVisitDate_mon']) {
                        foreach($cf3Form['pVisitDate_mon'] as $k=>$visitdate_mon) {
                            if($visitdate_mon != "") {
                                $eCLAIMS['pXML']['CLAIM']['CF3']['CF3_OLD']['MATERNITY']['PRENATAL']['CONSULTATION'][$k] = [
                                    'pVisitDate' => $visitdate_mon."-".$cf3Form['pVisitDate_dd'][$k]."-".$cf3Form['pVisitDate_yy'][$k],
                                    'pAOGWeeks' => $cf3Form['pAOGWeeks'][$k],
                                    'pWeight' => $cf3Form['pWeight'][$k],
                                    'pCardiacRate' => $cf3Form['pCardiacRate'][$k],
                                    'pRespiratoryRate' => $cf3Form['pRespiratoryRate'][$k],
                                    'pBloodPressure' => $cf3Form['pBloodPressure'][$k],
                                    'pTemperature' => $cf3Form['pTemperature'][$k]
                                ];
                            }
                        }
                    } else {
                        $eCLAIMS['pXML']['CLAIM']['CF3']['CF3_OLD']['MATERNITY']['PRENATAL']['CONSULTATION'][$k] = [
                            'pVisitDate' => "",
                    	        'pAOGWeeks' => "",
                    	        'pWeight' => "",
                    	        'pCardiacRate' => "",
                    	        'pRespiratoryRate' => "",
                    	        'pBloodPressure' => "",
                    	        'pTemperature' => ""
                        ];
                    }
                }
            }

            $url = url('/').'/public/uploads/eclaims/'.$eclaimid."/";
            $docsDir = upload_base_path().'eclaims/'.$eclaimid."/";
            if(File::exists($docsDir)) {
                $attachments_array = [];
                $docs = directoryFiles($docsDir);
                if($docs) {
                    $attachments = "";
                    foreach($docs as $i=>$doc) {
                        if($doc[0] != '.') // to remove hidden files
                        {
                            $base_name = explode('.', $doc);
                            $eCLAIMS['pXML']['CLAIM']['DOCUMENTS']['DOCUMENT'][$i] = [
                                'pDocumentType' => $base_name[0],
                                'pDocumentURL' => $url.$doc
                            ];
                            $attachments .= $doc."|";

                            $attachments_array[] = $base_name[0];
                        }
                    }
                    if(!in_array('CSF',$attachments_array))
                    {
                        $error_check = 'ERROR';
                        echo "<h3>Missing file!</h3><br>".nl2br("CSF not found in attachments! Please upload to prevent RTH.");
                    }
                } else {
                    $eCLAIMS['pXML']['CLAIM']['DOCUMENTS']['DOCUMENT'][0] = [
                        'pDocumentType' => '',
                        'pDocumentURL' => ''
                    ];
                }
            }
            else
            {
                $error_check = 'ERROR';
                echo "<h3>Missing file!</h3><br>".nl2br("No attachments found! Please upload required attachments.");
            }

            if($caserate_return['STATUS'] != 'ERROR' AND $error_check != 'ERROR')
            {
                $data_string = json_encode($eCLAIMS,JSON_FORCE_OBJECT);
                $ch = curl_init($this->ECLAIMS_URL.'/api/v1/eClaimsUpload');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");           
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
                );
                $response = curl_exec($ch);
                $err = curl_error($ch);
                curl_close($ch);

                if($err) {
                    dd($err);
                }
                if($response) {
                    $result = json_decode( $response, true );
                }

                if (isset($result['ISOK']) AND $result['ISOK'] == 'error') {
                    echo "<h3>ERROR!</h3><br>".nl2br( htmlentities($result['MESSAGE'] ));
                }
                elseif(isset($result['@attributes'])) 
                {     
                    $esubmit = array(
                        "pClaimNumber" => $eclaimid,
                        "eclaim_response" => json_encode($result),
                        "eclaim_xmlarray" => json_encode($eCLAIMS),
                        "eclaim_submit_status" => "OK",
                        "eclaim_file_date" => date('Y-m-d H:i:s'),
                        "pHospitalTransmittalNo" => $result['@attributes']['pHospitalTransmittalNo'],
                        "pTransmissionControlNumber" => $result['@attributes']['pTransmissionControlNumber'],
                        "pReceiptTicketNumber" => $result['@attributes']['pReceiptTicketNumber'],
                        "attachments" => $attachments
                    );

                    Eclaims::where('eclaims_id', $eclaimid)->update($esubmit);
                    
                    $lhio = false;
                    $lhio = $this->getClaimsMap($eclaimid,true);
                    if($lhio)
                    {
                        echo "Claim is successfully submitted and LHIO claim series no. is claimed!";
                    }
                    else
                    {
                        echo "Claim is successfully submitted! Please run Get Claims Map on your submitted claim.";
                    }
                }
                else
                {
                    echo "<h3>ERROR!</h3><br>".nl2br( htmlentities($response));
                }
            }
        }
        catch (Exception $e)
        {
            echo "<h3>Missing data!</h3><br>".nl2br( htmlentities($e->getMessage()));
        }
        
    }

    function generateCF4($eclaimid) {

        $claim = Eclaims::where('eclaims_id', $eclaimid)->first();
        $pbef = json_decode($claim->pbef_details, true);
        $cf1arr = json_decode($claim->cf1_details, true);
        $cf1 = (object) $cf1arr;
        $cf1sarr = json_decode($claim->cf1_sdata, true);
        $cf1s = (object) $cf1sarr;
        $cf2arr = json_decode($claim->cf2_details, true);
        $cf2 = (object) $cf2arr;
        $cf3arr = json_decode($claim->cf3_details, true);
        $cf34 = (object) $cf3arr;
        $cf4arr = json_decode($claim->cf4_details, true);
        $cf4 = (object) $cf4arr;

        $patient = Patients::where("patient_id", $claim->patient_id)->with('patientMedicalHistory')->with('patientFamilyInfo')->with('patientMedicalHistory')->with('patientAllergies')->with('patientDisabilities')->with('patientPhilhealthInfo')->first();
        $care =  Healthcareservices::with('GeneralConsultation')->with('VitalsPhysical')->with('Examination')->with('Diagnosis')->with('MedicalOrder')->with('Disposition')->where("healthcareservice_id", $claim->healthcareservice_id)->first();
        $philhealth = PhilhealthModel::where('patient_id','=',$claim->patient_id)->first();
        $facility = Facilities::getCurrentFacility($this->facility->facility_id);


        //Submit only for Philhealth Members
        if(in_array($cf1->pPatientIs, ['M','C','P','S'])) 
        {

            if($cf1->pPatientIs == "M") { //PATIENT IS MEMBER
                $MemberPin = $PatPin = $cf1->pPIN ? $cf1->pPIN : NULL;
            }
            elseif($cf1->pPatientIs != "M") { //PATIENT IS DEPENDENT
                $MemberPin = $cf1->pPIN ? $cf1->pPIN : NULL;
                $PatPin = $cf1s->deppin ? $cf1->deppin : $MemberPin;
            }

            $cf4CLAIMS = [
                'pXML' => [
                    'EPCB' => [
                        '@pUsername' => $this->USERNAME,
                        '@pPassword' => $this->USER_PASSWORD,
                        '@pHciAccreNo' => $cf4->pHciAccreNo,
                        '@pEnlistTotalCnt' => isset($cf4->pEnlistTotalCnt) ? $cf4->pEnlistTotalCnt : "",
                        '@pProfileTotalCnt' => isset($cf4->pProfileTotalCnt) ? $cf4->pProfileTotalCnt : "",
                        '@pSoapTotalCnt' => isset($cf4->pSoapTotalCnt) ? $cf4->pSoapTotalCnt : "",
                        '@pEmrId' => isset($cf4->pEmrId) ? $cf4->pEmrId : "",
                        '@pCertificationId' => $this->SOFTWARECERTID,
                        '@pHciTransmittalNumber' => $claim->pHospitalTransmittalNo ? $claim->pHospitalTransmittalNo : $eclaimid,
                        'ENLISTMENTS' => [
                            // - Registration 
                            'ENLISTMENT' => [
                                '@pEClaimId' => $eclaimid,
                                '@pEClaimsTransmittalId' => $claim->pHospitalTransmittalNo ? $claim->pHospitalTransmittalNo : $eclaimid,
                                '@pHciCaseNo' => $cf4->pHciCaseNo,
                                '@pHciTransNo' => $cf4->pHciTransNo,
                                '@pEffYear' => date("Y"), //$cf4->pEffYear,
                                '@pEnlistStat' => isset($cf4->pEnlistStat) ? $cf4->pEnlistStat : 1,
                                '@pEnlistDate' => isset($cf4->pEnlistDate) ? $cf4->pEnlistDate : date("Y-m-d"),
                                '@pPackageType' => isset($cf4->pPackageType) ? $cf4->pPackageType : "A",
                                '@pMemPin' => $MemberPin,
                                '@pMemFname' => isset($cf4->pMemFname) ? $cf4->pMemFname : "",
                                '@pMemMname' => isset($cf4->pMemMname) ? $cf4->pMemMname : "",
                                '@pMemLname' => isset($cf4->pMemLname) ? $cf4->pMemLname : "",
                                '@pMemExtname' => isset($cf4->pMemExtname) ? $cf4->pMemExtname : "",
                                '@pMemDob' => isset($cf4->pMemDob) ? $cf4->pMemDob : "",
                                '@pMemCat' => isset($cf4->pMemCat) ? $cf4->pMemCat : "",
                                '@pMemNcat' => isset($cf4->pMemNcat) ? $cf4->pMemNcat : "",
                                '@pPatientPin' => $PatPin,
                                '@pPatientFname' => isset($cf4->pPatientFname) ? $cf4->pPatientFname : "",
                                '@pPatientMname' => isset($cf4->pPatientMname) ? $cf4->pPatientMname : "",
                                '@pPatientLname' => isset($cf4->pPatientLname) ? $cf4->pPatientLname : "",
                                '@pPatientExtname' => isset($cf4->pPatientExtname) ? $cf4->pPatientExtname : "",
                                '@pPatientType' => $cf1->pMemberShipType,
                                '@pPatientSex' => isset($cf4->pPatientSex) ? $cf4->pPatientSex : "",
                                '@pPatientContactno' => isset($cf4->pPatientContactno) ? $cf4->pPatientContactno : "",
                                '@pPatientDob' => $cf1->pMemberBirthDate,
                                '@pPatientAddbrgy' => isset($cf4->pPatientAddbrgy) ? $cf4->pPatientAddbrgy : "",
                                '@pPatientAddmun' => isset($cf4->pPatientAddmun) ? $cf4->pPatientAddmun : "",
                                '@pPatientAddprov' => isset($cf4->pPatientAddprov) ? $cf4->pPatientAddprov : "",
                                '@pPatientAddreg' => isset($cf4->pPatientAddreg) ? $cf4->pPatientAddreg : "",
                                '@pPatientAddzipcode' => isset($cf4->pPatientAddzipcode) ? $cf4->pPatientAddzipcode : "",
                                '@pCivilStatus' => isset($cf4->pCivilStatus) ? $cf4->pCivilStatus : "U",
                                '@pWithConsent' => isset($cf4->pWithConsent) ? $cf4->pWithConsent : "X",
                                '@pWithLoa' => isset($cf4->pWithLoa) ? $cf4->pWithLoa : "X",
                                '@pWithDisability' => isset($cf4->pWithDisability) ? $cf4->pWithDisability : "X",
                                '@pDependentType' => isset($cf4->pDependentType) ? $cf4->pDependentType : "X",
                                '@pTransDate' => date("Y-m-d", strtotime($claim->cf4_date)),
                                '@pCreatedBy' => $claim->user_id,
                                '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : "",
                                '@pAvailFreeService' => isset($cf4->pAvailFreeService) ? $cf4->pAvailFreeService : "X"
                            ]
                        ],
                        'PROFILING' => [
                            //Health Screening & Assessment 
                            'PROFILE' => [
                                '@pHciTransNo' => $cf4->pHciTransNo,
                                '@pHciCaseNo' => $cf4->pHciCaseNo,
                                '@pPatientPin' => $PatPin,
                                '@pPatientType' => $cf1->pMemberShipType,
                                '@pMemPin' => $MemberPin,
                                '@pProfDate' => $cf1->pAdmissionDate,
                                '@pRemarks' => isset($cf4->pRemarks) ? $cf4->pRemarks : "",
                                '@pEffYear' => date("Y"),
                                '@pProfileATC' => "CF4",
                                '@pReportStatus' => "U",
                                '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : "",
                            
                                //Other Information 
                                'OINFO' => [
                                    '@pPatientPob' => isset($patient->birthplace) ? $patient->birthplace : (isset($cf4->pPatientPob) ? $cf4->pPatientPob : ""),
                                    '@pPatientAge' => getAge( date("Y-m-d", strtotime($cf1->pMemberBirthDate))),
                                    '@pPatientOccupation' => isset($cf4->pPatientOccupation) ? $cf4->pPatientOccupation : "",
                                    '@pPatientEducation' => isset($cf4->pPatientEducation) ? $cf4->pPatientEducation : "",
                                    '@pPatientReligion' => isset($cf4->pPatientReligion) ? $cf4->pPatientReligion : "",
                                    '@pPatientMotherMnln' => isset($cf4->pPatientMotherMnln) ? $cf4->pPatientMotherMnln : "",
                                    '@pPatientMotherMnmi' => isset($cf4->pPatientMotherMnmi) ? $cf4->pPatientMotherMnmi : "",
                                    '@pPatientMotherBday' => isset($cf4->pPatientMotherBday) ? $cf4->pPatientMotherBday : "",
                                    '@pPatientFatherFn' => isset($cf4->pPatientFatherFn) ? $cf4->pPatientFatherFn : "",
                                    '@pPatientFatherExtn' => isset($cf4->pPatientFatherExtn) ? $cf4->pPatientFatherExtn : "",
                                    '@pPatientMotherFn' => isset($cf4->pPatientMotherFn) ? $cf4->pPatientMotherFn : "",
                                    '@pPatientFatherBday' => isset($cf4->pPatientFatherBday) ? $cf4->pPatientFatherBday : "",
                                    '@pPatientMotherExtn' => isset($cf4->pPatientMotherExtn) ? $cf4->pPatientMotherExtn : "",
                                    '@pPatientFatherLn' => isset($cf4->pPatientFatherLn) ? $cf4->pPatientFatherLn : "",
                                    '@pPatientFatherMi' => isset($cf4->pPatientFatherMi) ? $cf4->pPatientFatherMi : "",
                                    '@pReportStatus' => "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Patient Medical History 
                                'MEDHIST' => [
                                    '@pMdiseaseCode' =>  isset($cf4->pMdiseaseCode) ? $cf4->pMdiseaseCode : "",
                                    '@pReportStatus' =>  isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' =>  isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Patient Specific Disease Description in Medical History 
                                'MHSPECIFIC' => [
                                    '@pMdiseaseCode' => isset($cf4->pMdiseaseCode) ? $cf4->pMdiseaseCode : "",
                                    '@pSpecificDesc' => isset($cf4->pSpecificDesc) ? $cf4->pSpecificDesc : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Patient Surgical History 
                                'SURGHIST' => [
                                    '@pSurgDesc' => isset($cf4->pSurgDesc) ? $cf4->pSurgDesc : "",
                                    '@pSurgDate' => isset($cf4->pSurgDate) ? $cf4->pSurgDate : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Family Medical History 
                                'FAMHIST' => [
                                    '@pMdiseaseCode' => isset($cf4->pMdiseaseCode) ? $cf4->pMdiseaseCode : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Specific Disease Description in Patient Family Medical History 
                                'FHSPECIFIC' => [
                                    '@pMdiseaseCode' => isset($cf4->pMdiseaseCode) ? $cf4->pMdiseaseCode : "",
                                    '@pSpecificDesc' => isset($cf4->pSpecificDesc) ? $cf4->pSpecificDesc : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Patient Social/ Personal History 
                                'SOCHIST' => [
                                    '@pIsSmoker' => isset($cf4->pIsSmoker) ? $cf4->pIsSmoker : "",
                                    '@pNoCigpk' => isset($cf4->pNoCigpk) ? $cf4->pNoCigpk : "",
                                    '@pIsAdrinker' => isset($cf4->pIsAdrinker) ? $cf4->pIsAdrinker : "",
                                    '@pNoBottles' => isset($cf4->pNoBottles) ? $cf4->pNoBottles : "",
                                    '@pIllDrugUser' => isset($cf4->pIllDrugUser) ? $cf4->pIllDrugUser : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Patient Immunization Record 
                                'IMMUNIZATION' => [
                                    '@pChildImmcode' => isset($cf4->pChildImmcode) ? $cf4->pChildImmcode : "",
                                    '@pYoungwImmcode' => isset($cf4->pYoungwImmcode) ? $cf4->pYoungwImmcode : "",
                                    '@pPregwImmcode' => isset($cf4->pPregwImmcode) ? $cf4->pPregwImmcode : "",
                                    '@pElderlyImmcode' => isset($cf4->pElderlyImmcode) ? $cf4->pElderlyImmcode : "",
                                    '@pOtherImm' => isset($cf4->pOtherImm) ? $cf4->pOtherImm : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Patient Menstrual History (For Female Patient Only) 
                                'MENSHIST' => [
                                    '@pMenarchePeriod' => isset($cf4->pMenarchePeriod) ? $cf4->pMenarchePeriod : "",
                                    '@pLastMensPeriod' => isset($cf4->pObstetric_lmp) ? $cf4->pObstetric_lmp : "",
                                    '@pPeriodDuration' => isset($cf4->pPeriodDuration) ? $cf4->pPeriodDuration : "",
                                    '@pMensInterval' => isset($cf4->pMensInterval) ? $cf4->pMensInterval : "",
                                    '@pPadsPerDay' => isset($cf4->pPadsPerDay) ? $cf4->pPadsPerDay : "",
                                    '@pOnsetSexIc' => isset($cf4->pOnsetSexIc) ? $cf4->pOnsetSexIc : "",
                                    '@pBirthCtrlMethod' => isset($cf4->pBirthCtrlMethod) ? $cf4->pBirthCtrlMethod : "",
                                    '@pIsMenopause' => isset($cf4->pIsMenopause) ? $cf4->pIsMenopause : "",
                                    '@pMenopauseAge' => isset($cf4->pMenopauseAge) ? $cf4->pMenopauseAge : "",
                                    '@pIsApplicable' => isset($cf4->pObstetric_NA) ? $cf4->pObstetric_NA : "N",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Patient Pregnancy History (For Female Patient Only) 
                                'PREGHIST' => [
                                    '@pPregCnt' => isset($cf4->pObstetricG) ? $cf4->pObstetricG : 0,
                                    '@pDeliveryCnt' => isset($cf4->pObstetricP) ? $cf4->pObstetricP : 0,
                                    '@pDeliveryTyp' => isset($cf4->pDeliveryTyp) ? $cf4->pDeliveryTyp : "",
                                    '@pFullTermCnt' => isset($cf4->pObstetric_T) ? $cf4->pObstetric_T : 0,
                                    '@pPrematureCnt' => isset($cf4->pObstetric_P) ? $cf4->pObstetric_P : 0,
                                    '@pAbortionCnt' => isset($cf4->pObstetric_A) ? $cf4->pObstetric_A : 0,
                                    '@pLivChildrenCnt' => isset($cf4->pObstetric_L) ? $cf4->pObstetric_L : 0,
                                    '@pWPregIndhyp' => isset($cf4->pWPregIndhyp) ? $cf4->pWPregIndhyp : "",
                                    '@pWFamPlan' => isset($cf4->pWFamPlan) ? $cf4->pWFamPlan : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Physical Examination Findings for BP Measurements, Heart and Respiratory Rate, Body Measurements and Vision 
                                'PEPERT' => [
                                    '@pSystolic' => (isset($cf4->VSBP) AND count(explode("/", $cf4->VSBP)) == 2) ? explode("/", $cf4->VSBP)[0] : "",
                                    '@pDiastolic' => (isset($cf4->VSBP) AND count(explode("/", $cf4->VSBP)) == 2) ? explode("/", $cf4->VSBP)[1] : "",
                                    '@pHr' => isset($cf4->VSHR) ? $cf4->VSHR : "",
                                    '@pRr' => isset($cf4->VSRR) ? $cf4->VSRR : "",
                                    '@pTemp' => isset($cf4->VSTemp) ? $cf4->VSTemp : "",
                                    '@pHeight' => $care ? $care->VitalsPhysical->height : "",
                                    '@pWeight' => $care ? $care->VitalsPhysical->weight : "",
                                    '@pVision' => isset($cf4->pVision) ? $cf4->pVision : "",
                                    '@pLength' => isset($cf4->pLength) ? $cf4->pLength : "",
                                    '@pHeadCirc' => isset($cf4->pHeadCirc) ? $cf4->pHeadCirc : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Patient Blood type Details 
                                'BLOODTYPE' => [
                                    '@pBloodType' => isset($cf4->pBloodType) ? $cf4->pBloodType : "",
                                    '@pBloodRh' => isset($cf4->pBloodRh) ? $cf4->pBloodRh : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Physical Examination General Survey 
                                'PEGENSURVEY' => [
                                    '@pGenSurveyId' => isset($cf4->GS) ? $cf4->GS : "",
                                    '@pGenSurveyRem' => isset($cf4->GSAltered) ? $cf4->GSAltered : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Physical Examination Findings for Skin, Heent, Chest, Heart, Abdomen, Neuro, Rectal and Genitourinary 
                                //change to foreach -> see below:
                                'PEMISC' => [
                                    // '@pSkinId' => isset($cf4->pSkinId) ? $cf4->pSkinId : "",
                                    // '@pHeentId' => isset($cf4->pHeentId) ? $cf4->pHeentId : "",
                                    // '@pChestId' => isset($cf4->pChestId) ? $cf4->pChestId : "",
                                    // '@pHeartId' => isset($cf4->pHeartId) ? $cf4->pHeartId : "",
                                    // '@pAbdomenId' => isset($cf4->pAbdomenId) ? $cf4->pAbdomenId : "",
                                    // '@pNeuroId' => isset($cf4->pNeuroId) ? $cf4->pNeuroId : "",
                                    // '@pRectalId' => isset($cf4->pRectalId) ? $cf4->pRectalId : "",
                                    // '@pGuId' => isset($cf4->pGuId) ? $cf4->pGuId : "",
                                    // '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    // '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Physical Examination Specific Findings for Skin, Heent, Chest, Heart, Abdomen, Neuro, Rectal and Genitourinary 
                                //change to foreach -> see below:
                                'PESPECIFIC' => [
                                    '@pSkinRem' => isset($cf4->PeSkin['pSkinRem']) ? $cf4->PeSkin['pSkinRem'] : "",
                                    '@pHeentRem' => isset($cf4->HEENT['pHeentRem']) ? $cf4->HEENT['pHeentRem'] : "",
                                    '@pChestRem' => isset($cf4->PeChest['pChestRem']) ? $cf4->PeChest['pChestRem'] : "",
                                    '@pHeartRem' => isset($cf4->PeCVS['pHeartRem']) ? $cf4->PeCVS['pHeartRem'] : "",
                                    '@pAbdomenRem' => isset($cf4->PeAbdomen['pAbdomenRem']) ? $cf4->PeAbdomen['pAbdomenRem'] : "",
                                    '@pNeuroRem' => isset($cf4->PeNeuro['pNeuroRem']) ? $cf4->PeNeuro['pNeuroRem'] : "",
                                    '@pRectalRem' => isset($cf4->pRectalRem) ? $cf4->pRectalRem : "",
                                    '@pGuRem' => isset($cf4->PeGU['pGuRem']) ? $cf4->PeGU['pGuRem'] : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Prescribed Laboratory  to the Patient 
                                'DIAGNOSTIC' => [
                                    '@pDiagnosticId' => isset($cf4->pDiagnosticId) ? $cf4->pDiagnosticId : "0",
                                    '@pOthRemarks' => isset($cf4->pOthRemarks) ? $cf4->pOthRemarks : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Patient Plan/Management Record 
                                'MANAGEMENT' => [
                                    '@pManagementId' => isset($cf4->pManagementId) ? $cf4->pManagementId : "0",
                                    '@pOthRemarks' => isset($cf4->pOthRemarks) ? $cf4->pOthRemarks : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Doctor’s Advice to Patient 
                                'ADVICE' => [
                                    '@pRemarks' => isset($cf4->pRemarks) ? $cf4->pRemarks : "NA",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Patient Answer to NCD Questionnaires (For Patient Aged 25 Years Old) 
                                'NCDQANS' => [
                                    '@pQid2_Yn' => isset($cf4->pQid2_Yn) ? $cf4->pQid2_Yn : "",
                                    '@pQid1_Yn' => isset($cf4->pQid1_Yn) ? $cf4->pQid1_Yn : "",
                                    '@pQid3_Yn' => isset($cf4->pQid3_Yn) ? $cf4->pQid3_Yn : "",
                                    '@pQid4_Yn' => isset($cf4->pQid4_Yn) ? $cf4->pQid4_Yn : "",
                                    '@pQid23_Yn' => isset($cf4->pQid23_Yn) ? $cf4->pQid23_Yn : "",
                                    '@pQid18_Yn' => isset($cf4->pQid18_Yn) ? $cf4->pQid18_Yn : "",
                                    '@pQid20_Yn' => isset($cf4->pQid20_Yn) ? $cf4->pQid20_Yn : "",
                                    '@pQid19_Fbsdate' => isset($cf4->pQid19_Fbsdate) ? $cf4->pQid19_Fbsdate : "",
                                    '@pQid14_Yn' => isset($cf4->pQid14_Yn) ? $cf4->pQid14_Yn : "",
                                    '@pQid15_Yn' => isset($cf4->pQid15_Yn) ? $cf4->pQid15_Yn : "",
                                    '@pQid21_Yn' => isset($cf4->pQid21_Yn) ? $cf4->pQid21_Yn : "",
                                    '@pQid5_Ynx' => isset($cf4->pQid5_Ynx) ? $cf4->pQid5_Ynx : "",
                                    '@pQid13_Yn' => isset($cf4->pQid13_Yn) ? $cf4->pQid13_Yn : "",
                                    '@pQid16_Yn' => isset($cf4->pQid16_Yn) ? $cf4->pQid16_Yn : "",
                                    '@pQid24_Yn' => isset($cf4->pQid24_Yn) ? $cf4->pQid24_Yn : "",
                                    '@pQid22_Proteindate' => isset($cf4->pQid22_Proteindate) ? $cf4->pQid22_Proteindate : "",
                                    '@pQid6_Yn' => isset($cf4->pQid6_Yn) ? $cf4->pQid6_Yn : "",
                                    '@pQid7_Yn' => isset($cf4->pQid7_Yn) ? $cf4->pQid7_Yn : "",
                                    '@pQid19_Yn' => isset($cf4->pQid19_Yn) ? $cf4->pQid19_Yn : "",
                                    '@pQid19_Fbsmmol' => isset($cf4->pQid19_Fbsmmol) ? $cf4->pQid19_Fbsmmol : "",
                                    '@pQid21_Ketondate' => isset($cf4->pQid21_Ketondate) ? $cf4->pQid21_Ketondate : "",
                                    '@pQid8_Yn' => isset($cf4->pQid8_Yn) ? $cf4->pQid8_Yn : "",
                                    '@pQid22_Proteinval' => isset($cf4->pQid22_Proteinval) ? $cf4->pQid22_Proteinval : "",
                                    '@pQid17_Abcde' => isset($cf4->pQid17_Abcde) ? $cf4->pQid17_Abcde : "",
                                    '@pQid21_Ketonval' => isset($cf4->pQid21_Ketonval) ? $cf4->pQid21_Ketonval : "",
                                    '@pQid9_Yn' => isset($cf4->pQid9_Yn) ? $cf4->pQid9_Yn : "",
                                    '@pQid20_Choledate' => isset($cf4->pQid20_Choledate) ? $cf4->pQid20_Choledate : "",
                                    '@pQid19_Fbsmg' => isset($cf4->pQid19_Fbsmg) ? $cf4->pQid19_Fbsmg : "",
                                    '@pQid20_Choleval' => isset($cf4->pQid20_Choleval) ? $cf4->pQid20_Choleval : "",
                                    '@pQid10_Yn' => isset($cf4->pQid10_Yn) ? $cf4->pQid10_Yn : "",
                                    '@pQid22_Yn' => isset($cf4->pQid22_Yn) ? $cf4->pQid22_Yn : "",
                                    '@pQid11_Yn' => isset($cf4->pQid11_Yn) ? $cf4->pQid11_Yn : "",
                                    '@pQid12_Yn' => isset($cf4->pQid12_Yn) ? $cf4->pQid12_Yn : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ] 
                            ]
                        ],
                        'SOAPS' => [
                            //Consultation 
                            'SOAP' => [
                                '@pHciTransNo' => $cf4->pHciTransNo,
                                '@pHciCaseNo' => $cf4->pHciCaseNo,
                                '@pPatientPin' => $PatPin,
                                '@pPatientType' => $cf1->pMemberShipType,
                                '@pMemPin' => $MemberPin,
                                '@pSoapDate' => $care ? date("Y-m-d", strtotime($care->created_at)) : $cf1->pAdmissionDate,
                                '@pEffYear' => $cf4->pEffYear,
                                '@pSoapATC' => isset($cf4->pSoapATC) ? $cf4->pSoapATC : "CF4",
                                '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : "",
                                //Record of Patient Chief Complaint and History of Illness 
                                'SUBJECTIVE' => [
                                    '@pChiefComplaint' => isset($cf4->pChiefComplaint) ? $cf4->pChiefComplaint : "",
                                    '@pIllnessHistory' => isset($cf4->pIllnessHistory) ? $cf4->pIllnessHistory : "",
                                    '@pOtherComplaint' => isset($cf4->pOtherComplaint) ? $cf4->pOtherComplaint : "",
                                    '@pSignsSymptoms' => isset($cf4->pSignsSymptoms) ? implode(";", array_filter($cf4->pSignsSymptoms, 'strlen')) : "",
                                    '@pPainSite' => isset($cf4->pPainSite) ? $cf4->pPainSite : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Physical Examination Findings for BP Measurements, Heart and Respiratory Rate, Body Measurements and Vision 
                                'PEPERT' => [
                                    '@pSystolic' => (isset($cf4->VSBP) AND count(explode("/", $cf4->VSBP)) == 2) ? explode("/", $cf4->VSBP)[0] : "",
                                    '@pDiastolic' => (isset($cf4->VSBP) AND count(explode("/", $cf4->VSBP)) == 2) ? explode("/", $cf4->VSBP)[1] : "",
                                    '@pHr' => isset($cf4->VSHR) ? $cf4->VSHR : "",
                                    '@pRr' => isset($cf4->VSRR) ? $cf4->VSRR : "",
                                    '@pTemp' => isset($cf4->VSTemp) ? $cf4->VSTemp : "",
                                    '@pHeight' => $care ? $care->VitalsPhysical->height : "",
                                    '@pWeight' => $care ? $care->VitalsPhysical->weight : "",
                                    '@pVision' => isset($cf4->pVision) ? $cf4->pVision : "",
                                    '@pLength' => isset($cf4->pLength) ? $cf4->pLength : "",
                                    '@pHeadCirc' => isset($cf4->pHeadCirc) ? $cf4->pHeadCirc : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Physical Examination Findings for Skin, Heent, Chest, Heart, Abdomen, Neuro, Rectal and Genitourinary 
                                //change to foreach -> see below:
                                'PEMISC' => [
                                    '@pSkinId' => isset($cf4->pSkinId) ? $cf4->pSkinId : "",
                                    '@pHeentId' => isset($cf4->pHeentId) ? $cf4->pHeentId : "",
                                    '@pChestId' => isset($cf4->pChestId) ? $cf4->pChestId : "",
                                    '@pHeartId' => isset($cf4->pHeartId) ? $cf4->pHeartId : "",
                                    '@pAbdomenId' => isset($cf4->pAbdomenId) ? $cf4->pAbdomenId : "",
                                    '@pNeuroId' => isset($cf4->pNeuroId) ? $cf4->pNeuroId : "",
                                    '@pRectalId' => isset($cf4->pRectalId) ? $cf4->pRectalId : "",
                                    '@pGuId' => isset($cf4->pGuId) ? $cf4->pGuId : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Remarks  in Patient Physical Examination Findings for Skin, Heent, Chest, Heart, Abdomen, Neuro, Rectal and Genitourinary 
                                'PESPECIFIC' => [
                                    '@pSkinRem' => isset($cf4->PeSkin['pSkinRem']) ? $cf4->PeSkin['pSkinRem'] : "",
                                    '@pHeentRem' => isset($cf4->HEENT['pHeentRem']) ? $cf4->HEENT['pHeentRem'] : "",
                                    '@pChestRem' => isset($cf4->PeChest['pChestRem']) ? $cf4->PeChest['pChestRem'] : "",
                                    '@pHeartRem' => isset($cf4->PeCVS['pHeartRem']) ? $cf4->PeCVS['pHeartRem'] : "",
                                    '@pAbdomenRem' => isset($cf4->PeAbdomen['pAbdomenRem']) ? $cf4->PeAbdomen['pAbdomenRem'] : "",
                                    '@pNeuroRem' => isset($cf4->PeNeuro['pNeuroRem']) ? $cf4->PeNeuro['pNeuroRem'] : "",
                                    '@pRectalRem' => isset($cf4->pRectalRem) ? $cf4->pRectalRem : "",
                                    '@pGuRem' => isset($cf4->PeGU['pGuRem']) ? $cf4->PeGU['pGuRem'] : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //List of ICD10 Codes For Patient Diagnosis 
                                'ICDS' => [
                                    '@pIcdCode' => isset($cf4->pIcdCode) ? $cf4->pIcdCode : "000",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Prescribed Laboratory  to the Patient 
                                'DIAGNOSTIC' => [
                                    '@pDiagnosticId' => isset($cf4->pDiagnosticId) ? $cf4->pDiagnosticId : "0",
                                    '@pOthRemarks' => isset($cf4->pOthRemarks) ? $cf4->pOthRemarks : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Patient Plan/Management Record 
                                'MANAGEMENT' => [
                                    '@pManagementId' => isset($cf4->pManagementId) ? $cf4->pManagementId : "0",
                                    '@pOthRemarks' => isset($cf4->pOthRemarks) ? $cf4->pOthRemarks : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                //Doctor’s Advice to Patient 
                                'ADVICE' => [
                                    '@pRemarks' => isset($cf4->pRemarks) ? $cf4->pRemarks : "NA",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ]
                            ]
                        ],
                        'COURSEWARDS' => [
                            //List of Actions/Order by Doctor 
                            //change to foreach -> see below:
                            'COURSEWARD' => [
                                '@pHciCaseNo' => $cf4->pHciCaseNo,
                                '@pHciTransNo' => $cf4->pHciTransNo,
                                '@pDateAction' => isset($cf4->pDateAction) ? $cf4->pDateAction : "",
                                '@pDoctorsAction' => isset($cf4->pDoctorsAction) ? $cf4->pDoctorsAction : "",
                                '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                            ]
                        ],
                        'LABRESULTS' => [
                            'LABRESULT' => [
                                '@pHciCaseNo' => $cf4->pHciCaseNo,
                                '@pPatientPin' => $PatPin,
                                '@pPatientType' => $cf1->pMemberShipType,
                                '@pMemPin' => $MemberPin,
                                '@pEffYear' => isset($cf4->pEffYear) ? $cf4->pEffYear : "",

                                //COMPLETE BLOOD COUNT (CBC)
                                'CBC' => [
                                    '@pHciTransNo' => $cf4->pHciTransNo,
                                    '@pReferralFacility' => isset($cf4->pReferralFacility) ? $cf4->pReferralFacility: "",
                                    '@pLabDate' => isset($cf4->pLabDate) ? $cf4->pLabDate : "",
                                    '@pHematocrit' => isset($cf4->pHematocrit) ? $cf4->pHematocrit : "",
                                    '@pHemoglobinG' => isset($cf4->pHemoglobinG) ? $cf4->pHemoglobinG : "",
                                    '@pHemoglobinMmol' => isset($cf4->pHemoglobinMmol) ? $cf4->pHemoglobinMmol : "",
                                    '@pMhcPg' => isset($cf4->pMhcPg) ? $cf4->pMhcPg : "",
                                    '@pMhcFmol' => isset($cf4->pMhcFmol) ? $cf4->pMhcFmol : "",
                                    '@pMchcGhb' => isset($cf4->pMchcGhb) ? $cf4->pMchcGhb : "",
                                    '@pMchcMmol' => isset($cf4->pMchcMmol) ? $cf4->pMchcMmol : "",
                                    '@pMcvUm' => isset($cf4->pMcvUm) ? $cf4->pMcvUm : "",
                                    '@pMcvFl' => isset($cf4->pMcvFl) ? $cf4->pMcvFl : "",
                                    '@pWbc1000' => isset($cf4->pWbc1000) ? $cf4->pWbc1000 : "",
                                    '@pWbc10' => isset($cf4->pWbc10) ? $cf4->pWbc10 : "",
                                    '@pMyelocyte' => isset($cf4->pMyelocyte) ? $cf4->pMyelocyte : "",
                                    '@pNeutrophilsBnd' => isset($cf4->pNeutrophilsBnd) ? $cf4->pNeutrophilsBnd : "",
                                    '@pNeutrophilsSeg' => isset($cf4->pNeutrophilsSeg) ? $cf4->pNeutrophilsSeg : "",
                                    '@pLymphocytes' => isset($cf4->pLymphocytes) ? $cf4->pLymphocytes : "",
                                    '@pMonocytes' => isset($cf4->pMonocytes) ? $cf4->pMonocytes : "",
                                    '@pEosinophils' => isset($cf4->pEosinophils) ? $cf4->pEosinophils : "",
                                    '@pBasophils' => isset($cf4->pBasophils) ? $cf4->pBasophils : "",
                                    '@pPlatelet' => isset($cf4->pPlatelet) ? $cf4->pPlatelet : "",
                                    '@pDateAdded' => isset($cf4->pDateAdded) ? $cf4->pDateAdded : "",
                                    '@pIsApplicable' => isset($cf4->pIsApplicable) ? $cf4->pIsApplicable : "N",
                                    '@pModule' => isset($cf4->pModule) ? $cf4->pModule : "",
                                    '@pDiagnosticLabFee' => isset($cf4->pDiagnosticLabFee) ? $cf4->pDiagnosticLabFee : "",
                                    '@pCoPay' => isset($cf4->pCoPay) ? $cf4->pCoPay : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : ""
                                ],
                                'URINALYSIS' => [
                                    '@pHciTransNo' => $cf4->pHciTransNo,
                                    '@pReferralFacility' => isset($cf4->pReferralFacility) ? $cf4->pReferralFacility : "",
                                    '@pLabDate' => isset($cf4->pLabDate ) ? $cf4->pLabDate  : "",
                                    '@pGravity' => isset($cf4->pGravity ) ? $cf4->pGravity  : "",
                                    '@pAppearance' => isset($cf4->pAppearance ) ? $cf4->pAppearance  : "",
                                    '@pColor' => isset($cf4->pColor ) ? $cf4->pColor  : "",
                                    '@pGlucose' => isset($cf4->pGlucose ) ? $cf4->pGlucose  : "",
                                    '@pProteins' => isset($cf4->pProteins ) ? $cf4->pProteins  : "",
                                    '@pKetones' => isset($cf4->pKetones ) ? $cf4->pKetones  : "",
                                    '@pPh' => isset($cf4->pPh ) ? $cf4->pPh  : "",
                                    '@pRbCells' => isset($cf4->pRbCells ) ? $cf4->pRbCells  : "",
                                    '@pWbCells' => isset($cf4->pWbCells ) ? $cf4->pWbCells  : "",
                                    '@pBacteria' => isset($cf4->pBacteria ) ? $cf4->pBacteria  : "",
                                    '@pCrystals' => isset($cf4->pCrystals ) ? $cf4->pCrystals  : "",
                                    '@pBladderCell' => isset($cf4->pBladderCell ) ? $cf4->pBladderCell  : "",
                                    '@pSquamousCell' => isset($cf4->pSquamousCell ) ? $cf4->pSquamousCell  : "",
                                    '@pTubularCell' => isset($cf4->pTubularCell ) ? $cf4->pTubularCell  : "",
                                    '@pBroadCasts' => isset($cf4->pBroadCasts ) ? $cf4->pBroadCasts  : "",
                                    '@pEpithelialCast' => isset($cf4->pEpithelialCast ) ? $cf4->pEpithelialCast  : "",
                                    '@pGranularCast' => isset($cf4->pGranularCast ) ? $cf4->pGranularCast  : "",
                                    '@pHyalineCast' => isset($cf4->pHyalineCast ) ? $cf4->pHyalineCast  : "",
                                    '@pRbcCast' => isset($cf4->pRbcCast ) ? $cf4->pRbcCast  : "",
                                    '@pWaxyCast' => isset($cf4->pWaxyCast ) ? $cf4->pWaxyCast  : "",
                                    '@pWcCast' => isset($cf4->pWcCast ) ? $cf4->pWcCast  : "",
                                    '@pAlbumin' => isset($cf4->pAlbumin ) ? $cf4->pAlbumin  : "",
                                    '@pPusCells' => isset($cf4->pPusCells ) ? $cf4->pPusCells  : "",
                                    '@pDateAdded' => isset($cf4->pDateAdded ) ? $cf4->pDateAdded  : "",
                                    '@pIsApplicable' => isset($cf4->pIsApplicable) ? $cf4->pIsApplicable : "N",
                                    '@pModule' => isset($cf4->pModule) ? $cf4->pModule : "",
                                    '@pDiagnosticLabFee' => isset($cf4->pDiagnosticLabFee) ? $cf4->pDiagnosticLabFee : "",
                                    '@pCoPay' => isset($cf4->pCoPay) ? $cf4->pCoPay : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks ) ? $cf4->pDeficiencyRemarks  : ""
                                ],
                                //CHEST X-RAY
                                'CHESTXRAY' => [
                                    '@pHciTransNo' => $cf4->pHciTransNo,
                                    '@pReferralFacility' => isset($cf4->pReferralFacility) ? $cf4->pReferralFacility : "",
                                    '@pLabDate' => isset($cf4->pLabDate) ? $cf4->pLabDate : "",
                                    '@pFindings' => isset($cf4->pFindings ) ? $cf4->pFindings  : "",
                                    '@pRemarksFindings' => isset($cf4->pRemarksFindings ) ? $cf4->pRemarksFindings  : "",
                                    '@pObservation' => isset($cf4->pObservation ) ? $cf4->pObservation  : "",
                                    '@pRemarksObservation' => isset($cf4->pRemarksObservation ) ? $cf4->pRemarksObservation  : "",
                                    '@pDateAdded' => isset($cf4->pDateAdded ) ? $cf4->pDateAdded  : "",
                                    '@pIsApplicable' => isset($cf4->pIsApplicable) ? $cf4->pIsApplicable : "N",
                                    '@pModule' => isset($cf4->pModule) ? $cf4->pModule : "",
                                    '@pDiagnosticLabFee' => isset($cf4->pDiagnosticLabFee) ? $cf4->pDiagnosticLabFee : "",
                                    '@pCoPay' => isset($cf4->pCoPay) ? $cf4->pCoPay : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks ) ? $cf4->pDeficiencyRemarks  : ""
                                ],
                                'SPUTUM' => [
                                    '@pHciTransNo' => $cf4->pHciTransNo,
                                    '@pReferralFacility' => isset($cf4->pReferralFacility) ? $cf4->pReferralFacility : "",
                                    '@pLabDate' => isset($cf4->pLabDate ) ? $cf4->pLabDate  : "",
                                    '@pDataCollection' => isset($cf4->pDataCollection) ? $cf4->pDataCollection : "X",
                                    '@pFindings' => isset($cf4->pFindings ) ? $cf4->pFindings  : "",
                                    '@pRemarks' => isset($cf4->pRemarks ) ? $cf4->pRemarks  : "",
                                    '@pNoPlusses' => isset($cf4->pNoPlusses ) ? $cf4->pNoPlusses  : "",
                                    '@pDateAdded' => isset($cf4->pDateAdded ) ? $cf4->pDateAdded  : "",
                                    '@pIsApplicable' => isset($cf4->pIsApplicable) ? $cf4->pIsApplicable : "N",
                                    '@pModule' => isset($cf4->pModule) ? $cf4->pModule : "",
                                    '@pDiagnosticLabFee' => isset($cf4->pDiagnosticLabFee) ? $cf4->pDiagnosticLabFee : "",
                                    '@pCoPay' => isset($cf4->pCoPay) ? $cf4->pCoPay : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks ) ? $cf4->pDeficiencyRemarks  : ""
                                ],
                                //LIPID PROFILE
                                'LIPIDPROF' => [
                                    '@pHciTransNo' => $cf4->pHciTransNo,
                                    '@pReferralFacility' => isset($cf4->pReferralFacility) ? $cf4->pReferralFacility : "",
                                    '@pLabDate' => isset($cf4->pLabDate ) ? $cf4->pLabDate  : "",
                                    '@pLdl' => isset($cf4->pLdl ) ? $cf4->pLdl  : "",
                                    '@pHdl' => isset($cf4->pHdl ) ? $cf4->pHdl  : "",
                                    '@pTotal' => isset($cf4->pTotal ) ? $cf4->pTotal  : "",
                                    '@pCholesterol' => isset($cf4->pCholesterol ) ? $cf4->pCholesterol  : "",
                                    '@pTriglycerides' => isset($cf4->pTriglycerides ) ? $cf4->pTriglycerides  : "",
                                    '@pDateAdded' => isset($cf4->pDateAdded ) ? $cf4->pDateAdded  : "",
                                    '@pIsApplicable' => isset($cf4->pIsApplicable) ? $cf4->pIsApplicable : "N",
                                    '@pModule' => isset($cf4->pModule) ? $cf4->pModule : "",
                                    '@pDiagnosticLabFee' => isset($cf4->pDiagnosticLabFee) ? $cf4->pDiagnosticLabFee : "",
                                    '@pCoPay' => isset($cf4->pCoPay) ? $cf4->pCoPay : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks ) ? $cf4->pDeficiencyRemarks  : ""
                                ],
                                //Fasting Blood Sugar
                                'FBS' => [
                                    '@pHciTransNo' => $cf4->pHciTransNo,
                                    '@pReferralFacility' => isset($cf4->pReferralFacility) ? $cf4->pReferralFacility : "",
                                    '@pLabDate' => isset($cf4->pLabDate) ? $cf4->pLabDate : "",
                                    '@pGlucoseMg' => isset($cf4->pGlucoseMg ) ? $cf4->pGlucoseMg  : "",
                                    '@pGlucoseMmol' => isset($cf4->pGlucoseMmol ) ? $cf4->pGlucoseMmol  : "",
                                    '@pDateAdded' => isset($cf4->pDateAdded ) ? $cf4->pDateAdded  : "",
                                    '@pIsApplicable' => isset($cf4->pIsApplicable) ? $cf4->pIsApplicable : "N",
                                    '@pModule' => isset($cf4->pModule) ? $cf4->pModule : "",
                                    '@pDiagnosticLabFee' => isset($cf4->pDiagnosticLabFee) ? $cf4->pDiagnosticLabFee : "",
                                    '@pCoPay' => isset($cf4->pCoPay) ? $cf4->pCoPay : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks ) ? $cf4->pDeficiencyRemarks  : ""
                                ],
                                //Electrocardiogram
                                'ECG' => [
                                    '@pHciTransNo' => $cf4->pHciTransNo,
                                    '@pReferralFacility' => isset($cf4->pReferralFacility) ? $cf4->pReferralFacility : "",
                                    '@pLabDate' => isset($cf4->pLabDate ) ? $cf4->pLabDate  : "",
                                    '@pFindings' => isset($cf4->pFindings ) ? $cf4->pFindings  : "",
                                    '@pRemarks' => isset($cf4->pRemarks ) ? $cf4->pRemarks  : "",
                                    '@pDateAdded' => isset($cf4->pDateAdded ) ? $cf4->pDateAdded  : "",
                                    '@pIsApplicable' => isset($cf4->pIsApplicable) ? $cf4->pIsApplicable : "N",
                                    '@pModule' => isset($cf4->pModule) ? $cf4->pModule : "",
                                    '@pDiagnosticLabFee' => isset($cf4->pDiagnosticLabFee) ? $cf4->pDiagnosticLabFee : "",
                                    '@pCoPay' => isset($cf4->pCoPay) ? $cf4->pCoPay : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks ) ? $cf4->pDeficiencyRemarks  : ""
                                ],
                                //Stool exam
                                'FECALYSIS' => [
                                    '@pHciTransNo' => $cf4->pHciTransNo,
                                    '@pReferralFacility' => isset($cf4->pReferralFacility) ? $cf4->pReferralFacility : "",
                                    '@pLabDate' => isset($cf4->pLabDate ) ? $cf4->pLabDate  : "",
                                    '@pColor' => isset($cf4->pColor ) ? $cf4->pColor  : "",
                                    '@pConsistency' => isset($cf4->pConsistency ) ? $cf4->pConsistency  : "",
                                    '@pRbc' => isset($cf4->pRbc ) ? $cf4->pRbc  : "",
                                    '@pWbc' => isset($cf4->pWbc ) ? $cf4->pWbc  : "",
                                    '@pOva' => isset($cf4->pOva ) ? $cf4->pOva  : "",
                                    '@pParasite' => isset($cf4->pParasite ) ? $cf4->pParasite  : "",
                                    '@pBlood' => isset($cf4->pBlood ) ? $cf4->pBlood  : "",
                                    '@pOccultBlood' => isset($cf4->pOccultBlood ) ? $cf4->pOccultBlood  : "",
                                    '@pPusCells' => isset($cf4->pPusCells ) ? $cf4->pPusCells  : "",
                                    '@pDateAdded' => isset($cf4->pDateAdded ) ? $cf4->pDateAdded  : "",
                                    '@pIsApplicable' => isset($cf4->pIsApplicable) ? $cf4->pIsApplicable : "N",
                                    '@pModule' => isset($cf4->pModule) ? $cf4->pModule : "",
                                    '@pDiagnosticLabFee' => isset($cf4->pDiagnosticLabFee) ? $cf4->pDiagnosticLabFee : "",
                                    '@pCoPay' => isset($cf4->pCoPay) ? $cf4->pCoPay : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks ) ? $cf4->pDeficiencyRemarks  : ""
                                ],
                                'PAPSSMEAR' => [
                                    '@pHciTransNo' => $cf4->pHciTransNo,
                                    '@pReferralFacility' => isset($cf4->pReferralFacility) ? $cf4->pReferralFacility : "",
                                    '@pLabDate' => isset($cf4->pLabDate ) ? $cf4->pLabDate  : "",
                                    '@pFindings' => isset($cf4->pFindings ) ? $cf4->pFindings  : "",
                                    '@pImpression' => isset($cf4->pImpression ) ? $cf4->pImpression  : "",
                                    '@pDateAdded' => isset($cf4->pDateAdded ) ? $cf4->pDateAdded  : "",
                                    '@pIsApplicable' => isset($cf4->pIsApplicable) ? $cf4->pIsApplicable : "N",
                                    '@pModule' => isset($cf4->pModule) ? $cf4->pModule : "",
                                    '@pDiagnosticLabFee' => isset($cf4->pDiagnosticLabFee) ? $cf4->pDiagnosticLabFee : "",
                                    '@pCoPay' => isset($cf4->pCoPay) ? $cf4->pCoPay : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks ) ? $cf4->pDeficiencyRemarks  : ""
                                ],
                                //Oral Glucose Tolerance Test
                                'OGTT' => [
                                    '@pHciTransNo' => $cf4->pHciTransNo,
                                    '@pReferralFacility' => isset($cf4->pReferralFacility) ? $cf4->pReferralFacility : "",
                                    '@pLabDate' => isset($cf4->pLabDate ) ? $cf4->pLabDate  : "",
                                    '@pExamFastingMg' => isset($cf4->pExamFastingMg ) ? $cf4->pExamFastingMg  : "",
                                    '@pExamFastingMmol' => isset($cf4->pExamFastingMmol) ? $cf4->pExamFastingMmol : "",
                                    '@pExamOgttOneHrMg' => isset($cf4->pExamOgttOneHrMg ) ? $cf4->pExamOgttOneHrMg  : "",
                                    '@pExamOgttOneHrMmol' => isset($cf4->pExamOgttOneHrMmol) ? $cf4->pExamOgttOneHrMmol : "",
                                    '@pExamOgttTwoHrMg' => isset($cf4->pExamOgttTwoHrMg ) ? $cf4->pExamOgttTwoHrMg  : "",
                                    '@pExamOgttTwoHrMmol' => isset($cf4->pExamOgttTwoHrMmol ) ? $cf4->pExamOgttTwoHrMmol  : "",
                                    '@pDateAdded' => isset($cf4->pDateAdded ) ? $cf4->pDateAdded  : "",
                                    '@pIsApplicable' => isset($cf4->pIsApplicable) ? $cf4->pIsApplicable : "N",
                                    '@pModule' => isset($cf4->pModule) ? $cf4->pModule : "",
                                    '@pDiagnosticLabFee' => isset($cf4->pDiagnosticLabFee) ? $cf4->pDiagnosticLabFee : "",
                                    '@pCoPay' => isset($cf4->pCoPay) ? $cf4->pCoPay : "",
                                    '@pReportStatus' => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                    '@pDeficiencyRemarks' => isset($cf4->pDeficiencyRemarks ) ? $cf4->pDeficiencyRemarks  : ""
                                ]
                            ]
                        ],
                        'MEDICINES' => [
                            'MEDICINE' => [
                                "@pHciCaseNo" => isset($cf4->pHciCaseNo) ? $cf4->pHciCaseNo : "",
                                "@pHciTransNo" => isset($cf4->pHciTransNo) ? $cf4->pHciTransNo : "",
                                "@pDrugCode" => isset($cf4->pDrugCode) ? $cf4->pDrugCode : "",
                                "@pGenericName" => isset($cf4->pGenericName) ? $cf4->pGenericName : "",
                                "@pGenericCode" => isset($cf4->pGenericCode) ? $cf4->pGenericCode : "",
                                "@pSaltCode" => isset($cf4->pSaltCode) ? $cf4->pSaltCode : "",
                                "@pStrengthCode" => isset($cf4->pStrengthCode) ? $cf4->pStrengthCode : "",
                                "@pFormCode" => isset($cf4->pFormCode) ? $cf4->pFormCode : "",
                                "@pUnitCode" => isset($cf4->pUnitCode) ? $cf4->pUnitCode : "",
                                "@pPackageCode" => isset($cf4->pPackageCode) ? $cf4->pPackageCode : "",
                                "@pRoute" => isset($cf4->pRoute) ? $cf4->pRoute : "",
                                "@pQuantity" => isset($cf4->pQuantity) ? $cf4->pQuantity : "",
                                "@pActualUnitPrice" => isset($cf4->pActualUnitPrice) ? $cf4->pActualUnitPrice : "",
                                "@pCoPayment" => isset($cf4->pCoPayment) ? $cf4->pCoPayment : "",
                                "@pTotalAmtPrice" => isset($cf4->pTotalAmtPrice) ? $cf4->pTotalAmtPrice : "",
                                "@pInstructionQuantity" => isset($cf4->pInstructionQuantity) ? $cf4->pInstructionQuantity : "",
                                "@pInstructionStrength" => isset($cf4->pInstructionStrength) ? $cf4->pInstructionStrength : "",
                                "@pInstructionFrequency" => isset($cf4->pInstructionFrequency) ? $cf4->pInstructionFrequency : "",
                                "@pPrescPhysician" => isset($cf4->pPrescPhysician) ? $cf4->pPrescPhysician : "",
                                "@pIsApplicable" => isset($cf4->pIsApplicable) ? $cf4->pIsApplicable : "N",
                                "@pDateAdded" => isset($cf4->pDateAdded) ? $cf4->pDateAdded : "",
                                "@pModule" => isset($cf4->pModule) ? $cf4->pModule : "CF4",
                                "@pReportStatus" => isset($cf4->pReportStatus) ? $cf4->pReportStatus : "U",
                                "@pDeficiencyRemarks" => isset($cf4->pDeficiencyRemarks) ? $cf4->pDeficiencyRemarks : "",
                            ]
                        ]
                    ]
                ]
            ];
 
            //MEDHIST PROFILING
            $mDiseases = ["Allergy","Asthma","Cancer","Cerebrovascular Disease","Coronary Artery Disease","Diabetes Mellitus","Emphysema","Epilepsy/Seizure Disorder","Hepatitis","Hyperlipidemia","Hypertension","Peptic Ulcer","Pneumonia","Thyroid Disease","Pulmonary Tuberculosis","Extrapulmonary Tuberculosis","Urinary Tract Infection","Mental Illness"];
            if($patient AND $patient->patientMedicalHistory AND count($patient->patientMedicalHistory) > 0)
            {
                $p['XML']['EPCB']['PROFILING']['PROFILE']['MEDHIST'] = [];
                foreach($patient->patientMedicalHistory as $k=>$history) {
                    $dCode = '';
                    if(in_array($history->LovHistoryModel->disease_name, $mDiseases)) {
                        $dCode = array_search( $history->LovHistoryModel->disease_name, $mDiseases ) + 1;
                        $p['XML']['EPCB']['PROFILING']['PROFILE']['MEDHIST'][] = [
                            '@pMdiseaseCode' =>  $dCode,
                            '@pReportStatus' =>  "U",
                            '@pDeficiencyRemarks' =>  NULL
                        ];
                    }
                }
            }

            //PEMISC
            //HEENT
            $libs = ["HEENT","PeChest","PeCVS","PeAbdomen","PeGU","PeSkin","PeNeuro"];
            $checkpemisc = false;
            foreach($libs as $lib) {
                if($cf4->$lib)
                {
                    // dd($cf4->$lib);
                    foreach($cf4->$lib as $code=>$val){
                        if($val != "") {
                            $checkpemisc = true;
                            if(substr($code, -3) === "Rem") // For others fields then assign 99 as value
                            {
                                $PEMISC = [
                                    '@pSkinId' => ($lib == 'PeSkin' AND $val != "") ? "99" : "",
                                    '@pHeentId' => ($lib == 'HEENT' AND $val != "") ? "99" : "",
                                    '@pChestId' => ($lib == 'PeChest' AND $val != "") ? "99" : "",
                                    '@pHeartId' => ($lib == 'PeCVS' AND $val != "") ? "99" : "",
                                    '@pAbdomenId' => ($lib == 'PeAbdomen' AND $val != "") ? "99" : "",
                                    '@pNeuroId' => ($lib == 'PeNeuro' AND $val != "") ? "99" : "",
                                    '@pRectalId' => "",
                                    '@pGuId' => ($lib == 'PeGU' AND $val != "") ? "99" : "",
                                    '@pReportStatus' => "U",
                                    '@pDeficiencyRemarks' => ""
                                ];
                            }
                            else
                            {
                                $PEMISC = [
                                    '@pSkinId' => ($lib == 'PeSkin' AND $val != "") ? $val : "",
                                    '@pHeentId' => ($lib == 'HEENT' AND $val != "") ? $val : "",
                                    '@pChestId' => ($lib == 'PeChest' AND $val != "") ? $val : "",
                                    '@pHeartId' => ($lib == 'PeCVS' AND $val != "") ? $val : "",
                                    '@pAbdomenId' => ($lib == 'PeAbdomen' AND $val != "") ? $val : "",
                                    '@pNeuroId' => ($lib == 'PeNeuro' AND $val != "") ? $val : "",
                                    '@pRectalId' => "",
                                    '@pGuId' => ($lib == 'PeGU' AND $val != "") ? $val : "",
                                    '@pReportStatus' => "U",
                                    '@pDeficiencyRemarks' => ""
                                ];
                            }
                            $cf4CLAIMS['pXML']['EPCB']['PROFILING']['PROFILE']['PEMISC'][] = $PEMISC;
                        }
                    }
                }
            }
            if(!$checkpemisc) // EMPTY PEMIS
            {
                $cf4CLAIMS['pXML']['EPCB']['PROFILING']['PROFILE']['PEMISC'] = [
                    '@pSkinId' => "",
                    '@pHeentId' => "",
                    '@pChestId' => "",
                    '@pHeartId' => "",
                    '@pAbdomenId' => "",
                    '@pNeuroId' => "",
                    '@pRectalId' => "",
                    '@pGuId' => "",
                    '@pReportStatus' => "U",
                    '@pDeficiencyRemarks' => ""                    
                ];
            }

            //COURSEWARD
            if($cf4->CourseInWardDate AND $cf4->CourseInWardDate[0] != "") {
                $cf4CLAIMS['pXML']['EPCB']['COURSEWARDS']['COURSEWARD'] = [];
                foreach($cf4->CourseInWardDate as $o=>$cwdate){
                    if($cwdate){
                        $temp = [
                            '@pHciCaseNo' => $cf4->pHciCaseNo,
                            '@pHciTransNo' => $cf4->pHciTransNo,
                            '@pDateAction' => $cwdate,
                            '@pDoctorsAction' => $cf4->CourseInWordOrder[$o],
                            '@pReportStatus' => "U",
                            '@pDeficiencyRemarks' => ""
                        ];
                        $cf4CLAIMS['pXML']['EPCB']['COURSEWARDS']['COURSEWARD'][] = $temp;
                    }
                }
            }

            //MEDICINE
            if($cf4->generic AND $cf4->generic[0] != "") {
                $cf4CLAIMS['pXML']['EPCB']['MEDICINES']['MEDICINE'] = [];
                foreach($cf4->generic as $g=>$generic) {
                    $med = DB::table('lov_phic_medicines')->where('drug_description',$generic)->first();
                    if($med){
                        //dd($med, $cf4->generic, $cf4->qtyDose, $cf4->total,$care, $care->MedicalOrder[$g]->MedicalOrderPrescription[$g]->created_at->toDateString());
                        // $prescriptionDate = $care->MedicalOrder[$g]->MedicalOrderPrescription[$g]->created_at;
                        // $dateAdd = $prescriptionDate->toDateString();
                        $det = explode("/",$cf4->qtyDose[$g]);
                        $temp = [
                            '@pHciCaseNo' => $cf4->pHciCaseNo,
                            '@pHciTransNo' => $cf4->pHciTransNo,
                            '@pDrugCode' => $med->drug_code,
                            '@pGenericName' => $med->drug_description,
                            '@pGenericCode' => $med->gen_code,
                            '@pSaltCode' => $med->salt_code,
                            '@pStrengthCode' => $med->strength_code,
                            '@pFormCode' => $med->form_code,
                            '@pUnitCode' => $med->unit_code,
                            '@pPackageCode' => $med->package_code,
                            '@pRoute' => $det[2],
                            '@pQuantity' => $det[0],
                            '@pActualUnitPrice' => isset($cf4->pActualUnitPrice) ? $cf4->pActualUnitPrice : "",
                            '@pCoPayment' => isset($cf4->pCoPayment) ? $cf4->pCoPayment : "",
                            '@pTotalAmtPrice' => $cf4->total[$g],
                            '@pInstructionQuantity' => isset($cf4->pInstructionQuantity) ? $cf4->pInstructionQuantity : "",
                            '@pInstructionStrength' => isset($cf4->pInstructionStrength) ? $cf4->pInstructionStrength : "",
                            '@pInstructionFrequency' => $det[1],
                            '@pPrescPhysician' => "",
                            '@pIsApplicable' => "Y",
                            '@pDateAdded' => date("Y-m-d"),
                            '@pModule' => "CF4",
                            '@pReportStatus' => "U",
                            '@pDeficiencyRemarks' => ""
                        ];
                        $cf4CLAIMS['pXML']['EPCB']['MEDICINES']['MEDICINE'][] = $temp;
                    }
                }
            }

            $jxml = json_encode($cf4CLAIMS,JSON_FORCE_OBJECT);

            $cf4xml = [
                'cipherKey' => $this->CIPHER_KEY,
                'pUserName' => $this->USERNAME,
                'pUserPassword' => $this->USER_PASSWORD,
                'pHospitalCode' => $this->HOSPITAL_CODE,
                'pHospitalEmail' => $facility->facilityContact->email_address,
                'pServiceProvider' => $facility->facility_name,
                'pCertificateId' => $this->SOFTWARECERTID, //software validation certificate number
                'jxml' => $jxml
            ];


            $data_string = json_encode($cf4xml,JSON_FORCE_OBJECT);
            $ch = curl_init($this->ECLAIMS_URL.'/api/v1/generateCF4XML');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");           
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);


            if($err) {
                dd($err);
            }
            if($response) {
                $result = json_decode( $response, true );
            }

            if($result['ISOK'] == true) {
                //let us add this file to the folder of attachments
                $old_attachments = $claim->attachments;
                $destinationPath = 'public/uploads/eclaims/'.$eclaimid; // upload path
                $urlPath = upload_base_path().'eclaims/'.$eclaimid;

                if(!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777);
                }

                //store the file to folder
                $bytes_written = File::put($destinationPath.'/CF4.xml.enc', $result['file']);
                if ($bytes_written === false)
                {
                    die("Error writing to file");
                }

                //if cf4 xml is not yet listed, add it to the list of attachments
                if(strpos($old_attachments,"CF4.xml.enc|") === false) {
                    $claim->attachments =  $old_attachments."CF4.xml.enc|";
                    $claim->cf4_xmlarray = $jxml;
                    $claim->save();
                }

                $message = "CF4 XML successfully generated.";
                return Redirect::to( "eclaims#generated" )->with('isok', $message);
            } else {
                $message = "ERROR! ".$result['MESSAGE'];
                return Redirect::to( "eclaims#generated" )->with('notok', $message);
            }
            
        } 
        else 
        {
            //if not Philhealth Member respond in error
            echo "<h3>ERROR! Patient is not a member of Philhealth. Please verify membership record.</h3><br>";
        }
    }

    function caseRateOnUpload($code,$primary)
    {
        $accre_types = array(
            'H1'=>'pCheckFacilityH1',
            'H2'=>'pCheckFacilityH2',
            'H3'=>'pCheckFacilityH3',
            'ASC'=>'pCheckFacilityASC',
            'PCF'=>'pCheckFacilityPCF',
            'MAT'=>'pCheckFacilityMAT',
            'FSDC'=>'pCheckFacilityFSDC',
        );


        //let us search case rate for given code
        $rvsc = $this->searchCaseRate(NULL, $code);
        $icd10 = $this->searchCaseRate($code,NULL);

        $search = "";
        if($rvsc)
        {
            $search = $rvsc;
            $codetype = 'RVS';
        }
        elseif ($icd10) {
            $search = $icd10;
            $codetype = 'ICD';
        }
        $search = json_decode($search,TRUE);

        $eclaims_fac = $this->eclaims_facility ? $this->eclaims_facility : NULL;
        if(isset($search['CASERATES']) AND isset($search['CASERATES']['@attributes']) AND isset($search['CASERATES']['AMOUNT']))
        {
            $pCaseRateCode = '';
            $pICDCode = '';
            $pRVSCode = '';
            $pCaseRateAmount = '';
            if(!$eclaims_fac)
            {
                $MESSAGE = 'Accreditation not found. Please assign facility accreditation in the eClaims page.';
                $response = array('ISOK'=>'error','MESSAGE'=>$MESSAGE);                
            }
            else
            {
                $pCaseRateCode = $search['CASERATES']['@attributes']['pCaseRateCode'];
                if($codetype == 'ICD')
                {
                    $pICDCode = $search['CASERATES']['@attributes']['pItemCode'];
                }
                else
                {
                    $pRVSCode = $search['CASERATES']['@attributes']['pItemCode'];
                }

                $accre = $eclaims_fac->hci_accreditation;
                $search_result = count($search['CASERATES']['AMOUNT']) == 1 ? ['@attributes'=>$search['CASERATES']['AMOUNT']] : $search['CASERATES']['AMOUNT'];
                foreach ($search_result as $amount_key => $amount) {
                    if(isset($amount['@attributes']) AND $amount['@attributes'][$accre_types[$accre]] == 'T')
                    {
                        if($primary == 'PRIMARY')
                        {
                            $pCaseRateAmount = $amount['@attributes']['pPrimaryCaseRate'];
                        }
                        if($primary == 'SECONDARY')
                        {
                            $pCaseRateAmount = $amount['@attributes']['pSecondaryCaseRate'];
                        }
                    }
                }
                if($pCaseRateAmount == '')
                {
                    $primary_text = ($primary == 'PRIMARY') ? 'primary' : 'secondary';
                    $MESSAGE = 'No applicable '.$primary_text.' case rates for your selected accreditation. Please check if your accreditation matches your '.$primary_text.' case rate item code';
                    $response = array('ISOK'=>'error','MESSAGE'=>$MESSAGE);
                }
                else
                {
                    $response = array(
                        'ISOK' => 'success',
                        'data' => array(
                            'pCaseRateCode' => $pCaseRateCode,
                            'pICDCode' => $pICDCode,
                            'pRVSCode' => $pRVSCode,
                            'pCaseRateAmount' => $pCaseRateAmount
                        )
                    );                    
                }

            }
        }
        if(isset($search['CASERATES']) AND !isset($search['CASERATES']['@attributes']))
        {
            $primary_text = ($primary == 'PRIMARY') ? 'primary' : 'secondary';
            $MESSAGE = 'More than one '.$primary_text.' case rate details were found. Please check item #9 on CF2.';
            $response = array('ISOK'=>'error','MESSAGE'=>$MESSAGE);            
        }

        if(!isset($search['CASERATES']) AND !isset($search['CASERATES']['@attributes']))
        {
            $primary_text = ($primary == 'PRIMARY') ? 'primary' : 'secondary';
            $MESSAGE = 'No '.$primary_text.' case rate details were found. Please check item #9 on CF2.';
            $response = array('ISOK'=>'error','MESSAGE'=>$MESSAGE);            
        }

        // if($icd10) { //code is ICD10
        //     $response = json_decode(str_replace('@attributes', 'attributes',$icd10), true);
        //     if(isset($response['CASERATES'])) {
        //         $caseRateCode = $response['CASERATES']['attributes']['pCaseRateCode'];
        //         $caseICDCode = $cf2Form['FirstCaseRateCode'];
        //         $caseRVSCode = "";
        //         $caseRateAmount = $response['CASERATES']['AMOUNT'][0]['attributes']['pPrimaryCaseRate'];
        //     }
        // }
        // if($rvsc) { //code is RVS
        //     $response = json_decode(str_replace('@attributes', 'attributes',$rvsc), true);
        //     if(isset($response['CASERATES'])) {
        //         $caseRateCode = $response['CASERATES']['attributes']['pCaseRateCode'];
        //         $caseICDCode = "";
        //         $caseRVSCode = $cf2Form['FirstCaseRateCode'];
        //         $caseRateAmount = $response['CASERATES']['AMOUNT'][0]['attributes']['pPrimaryCaseRate'];
        //     }
        // }
        return $response;
    }

    function delete($eclaims_id)
    {
        $claim = Eclaims::where('eclaims_id',$eclaims_id)->delete();

        if ($claim) :
            Session::flash('alert-class', 'alert-success alert-dismissible');
            $message = "Successfully deleted a catient.";
        else:
            Session::flash('alert-class', 'alert-danger alert-dismissible');
            $message = "An error was encountered while deleting the user. Kindly try again.";
        endif;

        return Redirect::to('eclaims#eligibilitycheck')->with('message', $message);
    }

}