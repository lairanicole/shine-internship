<?php namespace Modules\Eclaims\Http\Controllers;

use Modules\Eclaims\Http\Controllers\EclaimsController;

use ShineOS\Core\Patients\Entities\Patients;
use ShineOS\Core\Patients\Entities\PatientContacts;
use Plugins\Philhealth\PhilhealthModel;
use ShineOS\Core\Healthcareservices\Entities\Healthcareservices;
use ShineOS\Core\Healthcareservices\Entities\Disposition;
use ShineOS\Core\Healthcareservices\Entities\Diagnosis;
use ShineOS\Core\Facilities\Entities\Facilities;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderPrescription;
use ShineOS\Core\Healthcareservices\Entities\MedicalOrderLabExam;

use ShineOS\Core\Referrals\Entities\Referrals;

use ShineOS\Core\Users\Entities\Users;
use ShineOS\Core\Users\Entities\MDUsers;

use Pingpong\Modules\Routing\Controller;
use Modules\Eclaims\Entities\Eclaims; //model
use Modules\Eclaims\Entities\EclaimsFacility;
use Shine\Libraries\IdGenerator;
use Shine\Libraries\FacilityHelper;
use Shine\Libraries\UserHelper;
use Shine\Libraries\Utils\Lovs;

use Modules\Eclaims\Assets\libraries\PhilHealthEClaimsEncryptor as Encrypt; //model

use View, Response, Input, Datetime, DB, Redirect, Session, Config;

class LoadClaimController extends EclaimsController {

    /**
     * Load Form
     * Param $form String - name of form to load
     * Param $patid String - patient ID or eclaim ID
     * Param $careid String Optional - healthcare ID
     * 
     * This displays the form inside a div in the page
     * for new input or view can populate form using 
     * sparse data from EMR or from stored eclaim data
     */
    function loadClaim($form,$eclaims_id)
    {
        $data['page'] = $form;

        $claim = Eclaims::where('eclaims_id', $eclaims_id)->first();
        $cf1 = json_decode($claim->cf1_details);

        if($form == 'eligible') {
            $result = Eclaims::where('eclaims_id', $eclaims_id)->first();
            $cf1details = json_decode($result->cf1_details);
            $pbef_details = json_decode($result->pbef_details);
            
            $data['TRACKING_NUMBER'] = $result->pTrackingNumber;
            $data['date_time'] = $result->pbef_date;
            $data['hci_portal_ref'] = $cf1details->pHospitalCode;
            $data['hci_name'] = $this->facility->facility_name;
            $data['hci_accr'] = $this->HOSPITAL_CODE;
            $data['pin'] = $result->patient_pin;
            $data['name'] = $cf1details->pMemberFirstName." ".$cf1details->pMemberMiddleName." ".$cf1details->pMemberLastName." ".$cf1details->pMemberSuffix;
            

            // $data['birthdate'] = $cf1details->pMemberBirthDate;
            $birthdate = DateTime::createFromFormat('m-d-Y', $cf1details->pMemberBirthDate);
            $data['birthdate'] = $birthdate->format('F d, Y');
                
            $data['sex'] = $cf1details->pMemberSex;
            $data['category'] = $cf1details->pMemberShipType;
            $data['patient'] = $cf1details->pPatientFirstName." ".$cf1details->pPatientMiddleName." ".$cf1details->pPatientLastName." ".$cf1details->pPatientSuffix;
            $admitted = DateTime::createFromFormat('m-d-Y', $cf1details->pAdmissionDate);
            $data['admitted'] = $admitted->format('F d, Y');
            $discharge = DateTime::createFromFormat('m-d-Y', $cf1details->pDischargeDate);
            $data['discharge'] = $discharge->format('F d, Y');
            $data['patsex'] = $cf1details->pPatientSex;
            $patbirthdate = DateTime::createFromFormat('m-d-Y', $cf1details->pPatientBirthDate);
            $data['patbirthdate'] = $patbirthdate->format('F d, Y');
            $data['isok'] = $result->pbef_status;
            $data['reasons'] = isset($pbef_details->DOCUMENTS) ? json_encode($pbef_details->DOCUMENTS) : "";

        }
        if($form == 'csf') {
            $data['eclaims_id'] = $eclaims_id;

            if($claim->csf_details) {
                $data['csfForm'] = json_decode($claim->csf_details, false);
                $data['pbef_details'] = json_decode($claim->pbef_details);
            } else {
                $csfForm = new \stdClass();

                $patient = Patients::where("patient_id", $claim->patient_id)->first();
                $contact = PatientContacts::where("patient_id", $claim->patient_id)->first();
                $philhealth = PhilhealthModel::where('patient_id','=',$claim->patient_id)->first();
                $care = Healthcareservices::where("healthcareservice_id", $claim->healthcareservice_id)->first();
                $discharge = Disposition::where("healthcareservice_id", $claim->healthcareservice_id)->first();
                
                //generate form values  
                $brgy = $contact ? getBrgyName($contact->barangay) : NULL;
                $city = $contact ? getCityNameReturn($contact->city) : NULL;
                $prov = $contact ? getProvinceNameReturn($contact->province) : NULL;

                $csfForm->pMemberShipType = isset($philhealth) ? $philhealth->philhealth_category : "";
                $admission = $care ? date("Y-m-d", strtotime($care->created_at)) : date("Y-m-d", strtotime($cf1->pAdmissionDate));
                if($admission) {
                    $b =  explode("-",$admission);
                    if($b) {
                        $byy = $b[0];
                        $bdd = $b[1];
                        $bmon = $b[2];
                    }
                }
                $csfForm->admission = $admission;
                $csfForm->admit_mon = isset($bmon) ? $bmon : NULL;
                $csfForm->admit_dd = isset($bdd) ? $bdd : NULL;
                $csfForm->admit_yy = isset($byy) ? $byy : NULL;

                $discharge = $discharge ? date("Y-m-d", strtotime($discharge->discharge_datetime)) : date("Y-m-d", strtotime($cf1->pDischargeDate));;
                if($discharge) {
                    $b =  explode("-",$discharge);
                    if($b) {
                        $byy = $b[0];
                        $bdd = $b[1];
                        $bmon = $b[2];
                    }
                }
                $csfForm->discharge = $discharge;
                $csfForm->disch_mon = isset($bmon) ? $bmon : NULL;
                $csfForm->disch_dd = isset($bdd) ? $bdd : NULL;
                $csfForm->disch_yy = isset($byy) ? $byy : NULL;
                
                $csfForm->patid = $claim->patient_id;
                $csfForm->careid = $claim->healthcareservice_id;

                $csfForm->pin = $philhealth ? $philhealth->philhealth_id : $cf1->pPIN;
                $bmon = NULL;
                $bdd = NULL;
                $byy = NULL;
                $b =  $patient ? explode("-",$patient->birthdate) : NULL;
                if($b) {
                    $bmon = $b[1];
                    $bdd = $b[2];
                    $byy = $b[0];
                }

                $csfForm->birthdate = $patient ? date("m-d-Y", strtotime($patient->birthdate)) : $cf1->pMemberBirthDate;
                $csfForm->mon = explode('-',$csfForm->birthdate)[0];
                $csfForm->dd = explode('-',$csfForm->birthdate)[1];
                $csfForm->yy = explode('-',$csfForm->birthdate)[2];
                $csfForm->pPatientType = $care ? $care->encounter_type : NULL;
                $csfForm->pMemberLastName = $patient ? $patient->last_name : $cf1->pMemberLastName;
                $csfForm->pMemberFirstname = $patient ? $patient->first_name : $cf1->pMemberFirstName;
                $csfForm->pMemberSuffix = $patient ? $patient->name_suffix : $cf1->pMemberMiddleName;
                $csfForm->pMemberMiddleName = $patient ? $patient->middle_name : $cf1->pMemberSuffix;
                $csfForm->pMemberSex = $patient ? $patient->gender : $cf1->pMemberSex;
                $csfForm->unit = "";
                $csfForm->building = "";
                $csfForm->housenumber = "";
                $csfForm->street = $contact ? $contact->street_name : NULL;
                $csfForm->village = "";
                $csfForm->barangay = $brgy;
                $csfForm->city = $city;
                $csfForm->province = $prov;
                $csfForm->country = $contact ? $contact->country : NULL;
                $csfForm->zipcode = $contact ? $contact->zip : NULL;
                //$csfForm->landline = $contact->phone;
                //$csfForm->mobile = $contact->mobile;
                //$csfForm->email = $contact->email;
                $csfForm->is_member = "";
                if(isset($philhealth)) {
                    $csfForm->is_member = $philhealth->member_type;
                }

                $csfForm->deppin = NULL;
                $csfForm->patbirth_mon = explode('-',$cf1->pPatientBirthDate)[0];
                $csfForm->patbirth_dd = explode('-',$cf1->pPatientBirthDate)[1];
                $csfForm->patbirth_yy = explode('-',$cf1->pPatientBirthDate)[2];
                $csfForm->pPatientLastName = $patient ? $patient->last_name : $cf1->pPatientLastName;
                $csfForm->pPatientFirstName = $patient ? $patient->first_name : $cf1->pPatientFirstName;
                $csfForm->pPatientSuffix = $patient ? $patient->name_suffix : $cf1->pPatientMiddleName;
                $csfForm->pPatientMiddleName = $patient ? $patient->middle_name : $cf1->pPatientSuffix;
                $csfForm->epen = $cf1->pPEN;
                $csfForm->pEmployerContact = $cf1->pEmployerContact;
                $csfForm->pEmployerName = $cf1->pEmployerName;
                $csfForm->FirstCaseRateCode = $cf1->pRVS;
                $data['csfForm'] = $csfForm;
            }
        }
        if($form == 'cf1') {
            $memcat = array(
                '01'=>'S',
                '02'=>'S',
                '03'=>'S',
                '04'=>'G',
                '05'=>'G',
                '06'=>'G',
                '18'=>'I',
                '13'=>'NS',
                '10'=>'NO',
                '11'=>'NO',
                '19'=>'PG',
                '22'=>'P',
                '23'=>'P'
            );
            //let us check if there is submitted data already
            //patid here is the eclaimid
            $data['title'] = "CF1";
            $data['submit_button'] = "Submit CF1";
            $data['print_button'] = "Print CF1";
			$data['eclaims_id'] = $eclaims_id;
            $claim = Eclaims::where('eclaims_id', $eclaims_id)->first();
            
            if(isset($claim->cf1_details)) {
                $data['cf1'] = json_decode($claim);
                $data['cf1Form'] = json_decode($claim->cf1_sdata, false);
            } else {
                $patient = NULL;
                $contact =NULL;
                $philhealth = NULL;
                $care = NULL;
                $discharge = NULL;

                $cf1 = new \stdClass();
                $cf1Form = new \stdClass();
                
                $cf1->patient_id = NULL;
                $cf1->heatlhcareservice_id = NULL;

                $cf1Form->pMemberShipType = NULL;
                $cf1Form->pin = NULL;
                $cf1Form->pAdmissionDate = NULL;
                $cf1Form->pDischargeDate = NULL;
                $cf1Form->pHospitalCode = $this->HOSPITAL_CODE;
                $cf1Form->pMemberBirthDate = NULL;
                $cf1Form->mon = NULL;
                $cf1Form->dd = NULL;
                $cf1Form->yy = NULL;
                $cf1Form->pMemberLastName = NULL;
                $cf1Form->pMemberFirstName = NULL;
                $cf1Form->pMemberSuffix = NULL;
                $cf1Form->pMemberMiddleName = NULL;
                $cf1Form->pMemberSex = NULL;
                $cf1Form->unit = "";
                $cf1Form->building = "";
                $cf1Form->housenumber = "";
                $cf1Form->street = NULL;
                $cf1Form->village = "";
                $cf1Form->barangay = NULL;
                $cf1Form->city = NULL;
                $cf1Form->province = NULL;
                $cf1Form->country = NULL;
                $cf1Form->pZipCode = NULL;
                $cf1Form->pLandlineNo = NULL;
                $cf1Form->pMobileNo = NULL;
                $cf1Form->pEmailAddress = NULL;
                $cf1Form->is_member = "";

                $cf1Form->deppin = NULL;
                $cf1Form->dep_mon = NULL;
                $cf1Form->dep_dd = NULL;
                $cf1Form->dep_yy = NULL;
                $cf1Form->pPatientLastName = NULL;
                $cf1Form->pPatientFirstName = NULL;
                $cf1Form->pPatientSuffix = NULL;
                $cf1Form->pPatientMiddleName = NULL;
                $cf1Form->pPatientType = NULL;
                $cf1Form->epen = NULL;
                $cf1Form->pEmployerContact = NULL;
                $cf1Form->pEmployerName = NULL;
                    
                $data['cf1Form'] = $cf1Form;
                $data['cf1'] = $cf1;
            }
        }
        if($form == 'cf2') {
            $facility = Facilities::getCurrentFacility($this->facility->facility_id);
            
            if($claim->cf2) {
                $data['cf2Form'] = json_decode($claim->cf2_details, false);
                $data['eclaims_id'] = $eclaims_id;
                $data['cf2'] = $claim;
            } else {
                $patient = Patients::where("patient_id", $claim->patient_id)->first();
                $care = Healthcareservices::where("healthcareservice_id", $claim->healthcareservice_id)->first();
                $discharge = Disposition::where("healthcareservice_id", $claim->healthcareservice_id)->first();
                $diagnosis = Diagnosis::getDiagnosis($claim->healthcareservice_id);
                $referral = Referrals::where("healthcareservice_id", $claim->healthcareservice_id)->first();

                $cf2Form = new \stdClass();

                $cf2Form->pPatientLastName = ($patient) ? $patient->last_name : $cf1->pPatientLastName;
                $cf2Form->pPatientFirstName = ($patient) ? $patient->first_name : $cf1->pPatientFirstName;
                $cf2Form->pPatientSuffix = ($patient) ? $patient->name_suffix : $cf1->pPatientSuffix;
                $cf2Form->pPatientMiddleName = ($patient) ? $patient->middle_name : $cf1->pPatientMiddleName;
                $cf2Form->pPatientReferred = "N";
                $ds = NULL;
                if($discharge AND $discharge->disposition) {
                    switch($discharge->disposition) {
                        case "HOME": 
                        case "HAMA":
                            $ds = "H"; break;
                        case "ABS": $ds = "A"; break;
                        case "ADMDX": $ds = "T"; break;
                    }
                }
                if($discharge AND $discharge->discharge_condition) {
                    switch($discharge->discharge_condition) {
                        case "IMPRO": $ds = "I"; break;
                        case "RECOV": $ds = "R"; break;
                        default: $ds = "U"; break;
                    }
                }
                $cf2Form->pDisposition = $ds;
                $cf2Form->pAdmissionDiagnosis = isset($diagnosis[0]) ? $diagnosis[0]->diagnosislist_id : NULL;
                $cf2Form->pan = $this->PAN;
                $cf2Form->hci_name = $facility->facility_name;
                $cf2Form->hci_building = $facility->facilityContact->building_name.' '.$facility->facilityContact->house_no.' '.$facility->facilityContact->street_name;
                $cf2Form->hci_city = getCityNameReturn($facility->facilityContact->city);
                $cf2Form->hci_province = getProvinceNameReturn($facility->facilityContact->province);
                if($referral) {
                    $cf2Form->pPatientReferred = "Y";
                    $cf2Form->referring_hci_street_name = "Y";
                }
                $admission = $care ? date("m-d-Y h:i:s A", strtotime($care->created_at)) : $cf1->pAdmissionDate;
                if($admission) {
                    $ad =  explode(" ",$admission);
                    $b = explode("-", $ad[0]);
                    if($b) {
                        $ad_mon = $b[0];
                        $ad_dd = $b[1];
                        $ad_yy = $b[2];
                    }
                    $at = isset($ad[1]) ? explode(":", $ad[1]) : NULL;
                    if($at) {
                        $ad_hh = $at[1];
                        $ad_min = $at[2];
                    }
                    $aa = isset($ad[2]) ? $ad[2] : NULL;
                }
                $cf2Form->admission = date("m-d-Y h:i:s A", strtotime($admission));
                $cf2Form->ad_mon = isset($ad_mon) ? $ad_mon : NULL;
                $cf2Form->ad_dd = isset($ad_dd) ? $ad_dd : NULL;
                $cf2Form->ad_yy = isset($ad_yy) ? $ad_yy : NULL;
                $cf2Form->ad_hh = isset($ad_hh) ? $ad_hh : NULL;
                $cf2Form->ad_min = isset($ad_min) ? $ad_min : NULL;
                $cf2Form->ad_ampm = isset($aa) ? $aa : NULL;
    
                $discharge = $care ? date("m-d-Y h:i:s A", strtotime($discharge->discharge_datetime)) : $cf1->pDischargeDate;
                if($discharge) {
                    $dd =  explode(" ",$discharge);
                    $b = explode("-", $dd[0]);
                    if($b) {
                        $dd_mon = $b[0];
                        $dd_dd = $b[1];
                        $dd_yy = $b[2];
                    }
                    $dt = isset($dd[1]) ? explode(":", $dd[1]) : NULL;
                    if($dt) {
                        $dd_hh = $dt[1];
                        $dd_min = $dt[2];
                    }
                    $da = isset($dd[2]) ? $dd[2] : NULL;
                }
                $cf2Form->discharge = date("m-d-Y h:i:s A", strtotime($discharge));
                $cf2Form->dc_mon = isset($dd_mon) ? $dd_mon : NULL;
                $cf2Form->dc_dd = isset($dd_dd) ? $dd_dd : NULL;
                $cf2Form->dc_yy = isset($dd_yy) ? $dd_yy : NULL;
                $cf2Form->dc_hh = isset($dd_hh) ? $dd_hh : NULL;
                $cf2Form->dc_min = isset($dd_min) ? $dd_min : NULL;
                $cf2Form->dc_ampm = isset($da) ? $da : NULL;

                $cf2Form->doctor_fullname_1 = $care ? getUserFullNameByUserID($care->seen_by) : NULL;
                $cf2Form->doctor_firstname_1 = $care ? getUserFirstNameByID($care->seen_by) : NULL;
                $cf2Form->doctor_lastname_1 = $care ? getUserLastNameByID($care->seen_by) : NULL;
                $cf2Form->doctor_middlename_1 = $care ? getUserMidNameByID($care->seen_by) : NULL;
                $cf2Form->doctor_suffix_1 = $care ? getUserSuffixByID($care->seen_by) : NULL;
                
                $data['cf2Form'] = $cf2Form;
            }
        }
        if($form == 'cf3') {
            $data['eclaims_id'] = $eclaims_id;
            $data['cf1Form'] = json_decode($claim->cf1_sdata, false);
            $data['cf2Form'] = json_decode($claim->cf2_details, false);
            if($claim->cf3_details) {
                $data['cf3Form'] = json_decode($claim->cf3_details, false);
                $data['cf3'] = $claim;
            } else {
                $patient = $data['patient'] = Patients::where("patient_id", $claim->patient_id)->first();
                $data['care'] = $care =  Healthcareservices::with('GeneralConsultation')->with('VitalsPhysical')->with('Diagnosis')->with('Disposition')->where("healthcareservice_id", $claim->healthcareservice_id)->first();
                $referral = $data['referral'] = Referrals::where("healthcareservice_id", $claim->healthcareservice_id)->first();

                $cf3Form = new \stdClass();
                $cf3Form->lastname = $patient ? $patient->last_name : $cf1->pPatientLastName;
                $cf3Form->firstname = $patient ? $patient->first_name : $cf1->pPatientFirstName;
                $cf3Form->suffix = $patient ? $patient->name_suffix : $cf1->pPatientSuffix;
                $cf3Form->middlename = $patient ? $patient->middle_name : $cf1->pPatientMiddleName;
                $cf3Form->was_referred = "N";
                if($referral) {
                    $cf3Form->was_referred = "Y";
                    $cf3Form->referring_hci_street_name = "Y";
                }
                $cf3Form->pChiefComplaint = $care ? $care->GeneralConsultation[0]->complaint : NULL;
                $cf3Form->pBriefHistory = $care ? $care->GeneralConsultation[0]->complaint_history : NULL;

                $cf3Form->pBP = '';
                $cf3Form->pCR = '';
                $cf3Form->pTemp = '';
                $cf3Form->pAbdomen = '';
                $cf3Form->pHEENT = '';
                $cf3Form->pChestLungs = '';
                $cf3Form->pSkinExtremities = '';              

                if(isset($care->VitalsPhysical))
                {
                    $cf3Form->pBP = $care->VitalsPhysical->bloodpressure_systolic."/".$care->VitalsPhysical->bloodpressure_diastolic;
                    $cf3Form->pCR = $care->VitalsPhysical->respiratory_rate;
                    $cf3Form->pTemp = $care->VitalsPhysical->temperature;
                    $cf3Form->pAbdomen = $care->VitalsPhysical->abdomen_abnormal;
                    $cf3Form->pHEENT = $care->VitalsPhysical->Head_abnormal." ".$care->VitalsPhysical->Eyes_abnormal." ".$care->VitalsPhysical->Ent_abnormal;
                    $cf3Form->pChestLungs = $care->VitalsPhysical->Chest_abnormal;
                    $cf3Form->pSkinExtremities = $care->VitalsPhysical->Skin_abnormal;
                }

                if($care AND $care->healthcareservicetype_id == "MaternalCare") {
                    $maternal = \Plugins\MaternalCare\MaternalCareModel::where("healthcareservice_id", $care->healthcareservice_id)->first();
                    $visits = \Plugins\MaternalCare\MaternalCareVisitsModel::where('maternalcase_id', $maternal->maternalcase_id)->get();
                    //get first actual_visit date of the collection
                    if(count($visits)>=1) {
                        $cf3Form->ini_mon = date("m", strtotime($visits[0]->actual_visit));
                        $cf3Form->ini_dd = date("d", strtotime($visits[0]->actual_visit));
                        $cf3Form->ini_yy = date("Y", strtotime($visits[0]->actual_visit));
                    }
                }
                $data['cf3Form'] = $cf3Form;
            }
        }
        if($form == 'cf4') {
            //define cf4 content as NULL
            $cf4 = NULL;
            //get facility information
            $facility = Facilities::getCurrentFacility($this->facility->facility_id);

            $patient = $data['patient'] = Patients::where("patient_id", $claim->patient_id)->with('patientMedicalHistory')->first();
            $data['care'] = $care =  Healthcareservices::with('GeneralConsultation')->with('VitalsPhysical')->with('Examination')->with('Diagnosis')->with('MedicalOrder')->with('Disposition')->where("healthcareservice_id", $claim->healthcareservice_id)->first();
            $discharge = Disposition::where("healthcareservice_id", $claim->healthcareservice_id)->first();
            $diagnosis = Diagnosis::getDiagnosis($claim->healthcareservice_id);
            $referral = Referrals::where("healthcareservice_id", $claim->healthcareservice_id)->first();
            $philhealth = PhilhealthModel::where('patient_id','=',$claim->patient_id)->first();
            //dd($patient, $care, $referral,$diagnosis,$philhealth);

            //get the claim data from the eclaims table using eclaims_id
            $claim = Eclaims::where('eclaims_id', $eclaims_id)->first();
            $data['eclaims_id'] = $eclaims_id;
            //get all other data for use in this form
            $data['pbef'] = $pbef = json_decode($claim->cf2_details, false);
            $data['cf1Form'] = $cf1Form = json_decode($claim->cf1_details, false);
            $data['cf1sForm'] = $cf1sForm = json_decode($claim->cf1_sdata, false);
            $data['cf2Form'] = $cf2Form = json_decode($claim->cf2_details, false);
            $data['cf3Form'] = $cf3Form = json_decode($claim->cf3_details, false);
            //if cf4data is not empty load them data
            if($claim->cf4_details) {
                $data['cf4Form'] = $cf4Form = json_decode($claim->cf4_details, false);
                $data['cf4'] = $claim;
            //else get the data from the EMR
            } else {
                $cf4Form = new \stdClass();
                $cf4Form->hci_name = $this->facility->facility_name;
                $cf4Form->pHciAccreNo = $this->PAN;
                $cf4Form->hci_bldg = $facility->facilityContact->building_name;
                $cf4Form->hci_street = $facility->facilityContact->house_no.' '.$facility->facilityContact->street_name;

                $stamper = $claim->HOSPITAL_CODE.date('Y').date('m').str_pad($claim->id,5,"0",STR_PAD_LEFT);
                $cf4Form->pHciTransNo = $stamper;
                $cf4Form->pHciCaseNo = "C".$stamper;
                $cf4Form->pEffYear = date('Y-m-d');

                $cf4Form->pPatientAddbrgy = getBrgyName($facility->facilityContact->brgy);
                $cf4Form->pPatientAddmun = getCityNameReturn($facility->facilityContact->city);
                $cf4Form->pPatientAddprov = getProvinceNameReturn($facility->facilityContact->province);
                $cf4Form->pPatientAddzipcode = $facility->facilityContact->zipcode;
                
                $cf4Form->pPatientLname = $cf1Form->pMemberLastName ? $cf1Form->pMemberLastName : NULL;
                $cf4Form->pPatientFname = $cf1Form->pMemberFirstName ? $cf1Form->pMemberFirstName : NULL;
                $cf4Form->pPatientExtname = $cf1Form->pMemberSuffix ? $cf1Form->pMemberSuffix : NULL;
                $cf4Form->pPatientMname = $cf1Form->pMemberMiddleName ? $cf1Form->pMemberMiddleName : NULL;

                $cf4Form->pPatientPin = $cf1Form->pPIN ? $cf1Form->pPIN : NULL;

                $cf4Form->pMemDob = $cf1Form->pMemberBirthDate ? $cf1Form->pMemberBirthDate : $patient->birthdate;
                $cf4Form->patAge = getAge(str_replace("-", "/",$cf4Form->pMemDob));
                $cf4Form->pPatientSex = $cf1Form->pPatientSex ? $cf1Form->pPatientSex : $patient->gender;
                $cf4Form->was_referred = "N";
                
                if($referral){
                    $cf4Form->was_referred = "Y";
                    $cf4Form->YesReason = $referral->ReferralReasons[0]->lovreferralreason_id;
                    $cf4Form->NameOrigHCI = $referral->facility_id;
                } else if($cf2Form AND $cf2Form->pPatientReferred == 'Y') {
                    $cf4Form->was_referred = "Y";
                    $cf4Form->YesReason = $cf2Form->pReferralReasons;
                    $cf4Form->NameOrigHCI = $cf2Form->referring_hci_name;
                }
                $cf4Form->pChiefComplaint = isset($cf3Form->pChiefComplaint) ? $cf3Form->pChiefComplaint : NULL;
                $cf4Form->pIllnessHistory = isset($cf3Form->pBriefHistory) ? $cf3Form->pBriefHistory : NULL;

                $cf4Form->pAdmissionDiagnosis = ($cf2Form AND $cf2Form->pAdmissionDiagnosis) ? $cf2Form->pAdmissionDiagnosis : ( isset($diagnosis[0]) ? $diagnosis[0]->icd10_code.': '.$diagnosis[0]->diagnosislist_id : NULL );

                $cf4Form->pIcdCode = ($cf2Form AND $cf2Form->pICDCode[0]) ? $cf2Form->pICDCode[0] : (isset($diagnosis[0]) ? $diagnosis[0]->icd10_code : NULL);

                $cf4Form->pFirstCaseRate = $cf2Form ? $cf2Form->FirstCaseRateCode : NULL;
                $cf4Form->pSecondCaseRate = $cf2Form ? $cf2Form->SecondCaseRateCode : NULL;

                $cf4Form->ad_mon = $cf2Form ? $cf2Form->ad_mon : NULL;
                $cf4Form->ad_dd = $cf2Form ? $cf2Form->ad_dd : NULL;
                $cf4Form->ad_yy = $cf2Form ? $cf2Form->ad_yy : NULL;
                $cf4Form->ad_hh = $cf2Form ? $cf2Form->ad_hh : NULL;
                $cf4Form->ad_min = $cf2Form ? $cf2Form->ad_min : NULL;
                $cf4Form->ad_ampm = $cf2Form AND isset($cf2Form->ad_ampm) ? $cf2Form->ad_ampm : NULL;

                $cf4Form->dc_mon = $cf2Form ? $cf2Form->dc_mon : NULL;
                $cf4Form->dc_dd = $cf2Form ? $cf2Form->dc_dd : NULL;
                $cf4Form->dc_yy = $cf2Form ? $cf2Form->dc_yy : NULL;
                $cf4Form->dc_hh = $cf2Form ? $cf2Form->dc_hh : NULL;
                $cf4Form->dc_min = $cf2Form ? $cf2Form->dc_min : NULL;
                $cf4Form->dc_ampm = $cf2Form AND isset($cf2Form->dc_ampm) ? $cf2Form->dc_ampm : NULL;

                $cf4Form->pObstetricG = isset($cf3Form->pObstetricG) ? $cf3Form->pObstetricG : '';
                $cf4Form->pObstetricP = isset($cf3Form->pObstetricP) ? $cf3Form->pObstetricP : '';
                $cf4Form->pObstetric_T = isset($cf3Form->pObstetric_T) ? $cf3Form->pObstetric_T : '';
                $cf4Form->pObstetric_P = isset($cf3Form->pObstetric_P) ? $cf3Form->pObstetric_P : '';
                $cf4Form->pObstetric_A = isset($cf3Form->pObstetric_A) ? $cf3Form->pObstetric_A : '';
                $cf4Form->pObstetric_L = isset($cf3Form->pObstetric_L) ? $cf3Form->pObstetric_L : '';
                $cf4Form->pObstetric_lmp = $cf4 ? $cf4->pObstetric_lmp : '';
                $cf4Form->pObstetric_NA = $cf4 ? $cf4->pObstetric_NA : 0;

                $cf4Form->pSignsSymptoms['Sensorium'] = $cf4 ? $cf4->pSignsSymptoms['Sensorium'] : NULL;
                $cf4Form->pSignsSymptoms['Diarrhea'] = $cf4 ? $cf4->pSignsSymptoms['Diarrhea'] : NULL;
                $cf4Form->pSignsSymptoms['Hematemesis'] = $cf4 ? $cf4->pSignsSymptoms['Hematemesis'] : NULL;
                $cf4Form->pSignsSymptoms['Palpitations'] = $cf4 ? $cf4->pSignsSymptoms['Palpitations'] : NULL;
                $cf4Form->pSignsSymptoms['AbCramps'] = $cf4 ? $cf4->pSignsSymptoms['AbCramps'] : NULL;
                $cf4Form->pSignsSymptoms['Dizziness'] = $cf4 ? $cf4->pSignsSymptoms['Dizziness'] : NULL;
                $cf4Form->pSignsSymptoms['Hemturia'] = $cf4 ? $cf4->pSignsSymptoms['Hemturia'] : NULL;
                $cf4Form->pSignsSymptoms['Seizures'] = $cf4 ? $cf4->pSignsSymptoms['Seizures'] : NULL;
                $cf4Form->pSignsSymptoms['Anorexia'] = $cf4 ? $cf4->pSignsSymptoms['Anorexia'] : NULL;
                $cf4Form->pSignsSymptoms['Dysphagia'] = $cf4 ? $cf4->pSignsSymptoms['Dysphagia'] : NULL;
                $cf4Form->pSignsSymptoms['Hemoptysis'] = $cf4 ? $cf4->pSignsSymptoms['Hemoptysis'] : NULL;
                $cf4Form->pSignsSymptoms['SkinRashes'] = $cf4 ? $cf4->pSignsSymptoms['SkinRashes'] : NULL;
                $cf4Form->pSignsSymptoms['BleedingGums'] = $cf4 ? $cf4->pSignsSymptoms['BleedingGums'] : NULL;
                $cf4Form->pSignsSymptoms['Dyspnea'] = $cf4 ? $cf4->pSignsSymptoms['Dyspnea'] : NULL;
                $cf4Form->pSignsSymptoms['Irritability'] = $cf4 ? $cf4->pSignsSymptoms['Irritability'] : NULL;
                $cf4Form->pSignsSymptoms['BloodyStool'] = $cf4 ? $cf4->pSignsSymptoms['BloodyStool'] : NULL;
                $cf4Form->pSignsSymptoms['BodyWeakness'] = $cf4 ? $cf4->pSignsSymptoms['BodyWeakness'] : NULL;
                $cf4Form->pSignsSymptoms['Dysuria'] = $cf4 ? $cf4->pSignsSymptoms['Dysuria'] : NULL;
                $cf4Form->pSignsSymptoms['Jaundice'] = $cf4 ? $cf4->pSignsSymptoms['Jaundice'] : NULL;
                $cf4Form->pSignsSymptoms['Sweating'] = $cf4 ? $cf4->pSignsSymptoms['Sweating'] : NULL;
                $cf4Form->pSignsSymptoms['BlurryVision'] = $cf4 ? $cf4->pSignsSymptoms['BlurryVision'] : NULL;
                $cf4Form->pSignsSymptoms['Epistaxis'] = $cf4 ? $cf4->pSignsSymptoms['Epistaxis'] : NULL;
                $cf4Form->pSignsSymptoms['LowerEdema'] = $cf4 ? $cf4->pSignsSymptoms['LowerEdema'] : NULL;
                $cf4Form->pSignsSymptoms['Urgency'] = $cf4 ? $cf4->pSignsSymptoms['Urgency'] : NULL;
                $cf4Form->pSignsSymptoms['ChestPain'] = $cf4 ? $cf4->pSignsSymptoms['ChestPain'] : NULL;
                $cf4Form->pSignsSymptoms['Fever'] = $cf4 ? $cf4->pSignsSymptoms['Fever'] : NULL;
                $cf4Form->pSignsSymptoms['Myaglia'] = $cf4 ? $cf4->pSignsSymptoms['Myaglia'] : NULL;
                $cf4Form->pSignsSymptoms['Vomiting'] = $cf4 ? $cf4->pSignsSymptoms['Vomiting'] : NULL;
                $cf4Form->pSignsSymptoms['Constipation'] = $cf4 ? $cf4->pSignsSymptoms['Constipation'] : NULL;
                $cf4Form->pSignsSymptoms['FrequentUrination'] = $cf4 ? $cf4->pSignsSymptoms['FrequentUrination'] : NULL;
                $cf4Form->pSignsSymptoms['Orthopnea'] = $cf4 ? $cf4->pSignsSymptoms['Orthopnea'] : NULL;
                $cf4Form->pSignsSymptoms['WeightLoss'] = $cf4 ? $cf4->pSignsSymptoms['WeightLoss'] : NULL;
                $cf4Form->pSignsSymptoms['Cough'] = $cf4 ? $cf4->pSignsSymptoms['Cough'] : NULL;
                $cf4Form->pSignsSymptoms['Headache'] = $cf4 ? $cf4->pSignsSymptoms['Headache'] : NULL;
                $cf4Form->pSignsSymptoms['Pain'] = $cf4 ? $cf4->pSignsSymptoms['Pain'] : NULL;
                $cf4Form->pPainSite = $cf4 ? $cf4->pPainSite : NULL;
                $cf4Form->pSignsSymptoms['Others'] = $cf4 ? $cf4->pSignsSymptoms['Others'] : NULL;
                $cf4Form->pOtherComplaint = $cf4 ? $cf4->pOtherComplaint : NULL;

                $cf4Form->GSAwake = $cf4 ? $cf4->GSAwake : NULL;
                $cf4Form->PeAdOthers = $cf4 ? $cf4->PeAdOthers : NULL;
                $cf4Form->GSAltered = $cf4 ? $cf4->GSAltered : NULL;

                $cf4Form->VSBP = $care ? $care->VitalsPhysical->bloodpressure_systolic."/".$care->VitalsPhysical->bloodpressure_diastolic : NULL;
                $cf4Form->VSHR = $care ? $care->VitalsPhysical->heart_rate : NULL;
                $cf4Form->VSRR = $care ? $care->VitalsPhysical->respiratory_rate : NULL;
                $cf4Form->VSTemp = $care ? $care->VitalsPhysical->temperature : NULL;

                $cf4Form->HEENT['Normal'] = $cf4 ? $cf4->HEENT['Normal'] : NULL;
                $cf4Form->HEENT['AbnPupillary'] = $cf4 ? $cf4->HEENT['AbnPupillary'] : NULL;
                $cf4Form->HEENT['Lymphade'] = $cf4 ? $cf4->HEENT['Lymphade'] : NULL;
                $cf4Form->HEENT['DryMucus'] = $cf4 ? $cf4->HEENT['DryMucus'] : NULL;
                $cf4Form->HEENT['Icteric']  = $cf4 ? $cf4->HEENT['Icteric'] : NULL;
                $cf4Form->HEENT['Pale'] = $cf4 ? $cf4->HEENT['Pale'] : NULL;
                $cf4Form->HEENT['SunkenEyeballs'] = $cf4 ? $cf4->HEENT['SunkenEyeballs'] : NULL;
                $cf4Form->HEENT['Fontanelle'] = $cf4 ? $cf4->HEENT['Fontanelle'] : NULL;
                $cf4Form->HEENT['pHeentRem'] = $care ? $care->VitalsPhysical->Ent_abnormal : NULL;

                $cf4Form->PeChest['EssentialNormal'] = $cf4 ? $cf4->PeChest['EssentialNormal'] : NULL;
                $cf4Form->PeChest['AsymmetricalChest'] = $cf4 ? $cf4->PeChest['AsymmetricalChest'] : NULL;
                $cf4Form->PeChest['DecreasedBreath'] = $cf4 ? $cf4->PeChest['DecreasedBreath'] : NULL;
                $cf4Form->PeChest['Wheezes'] = $cf4 ? $cf4->PeChest['Wheezes'] : NULL;
                $cf4Form->PeChest['LumpsOverBreast'] = $cf4 ? $cf4->PeChest['LumpsOverBreast'] : NULL;
                $cf4Form->PeChest['RalesCrackles'] = $cf4 ? $cf4->PeChest['RalesCrackles'] : NULL;
                $cf4Form->PeChest['IntercostalRibs'] = $cf4 ? $cf4->PeChest['IntercostalRibs'] : NULL;
                $cf4Form->PeChest['pChestRem'] = $care ? $care->VitalsPhysical->Chest_abnormal : NULL;

                $cf4Form->PeCVS['CVSEssentialNormal'] = $cf4 ? $cf4->PeCVS['CVSEssentialNormal'] : NULL;
                $cf4Form->PeCVS['DisplacedApex'] = $cf4 ? $cf4->PeCVS['DisplacedApex'] : NULL;
                $cf4Form->PeCVS['Heaves'] = $cf4 ? $cf4->PeCVS['Heaves'] : NULL;
                $cf4Form->PeCVS['PericardialBulge'] = $cf4 ? $cf4->PeCVS['PericardialBulge'] : NULL;
                $cf4Form->PeCVS['IrregularRhythm'] = $cf4 ? $cf4->PeCVS['IrregularRhythm'] : NULL;
                $cf4Form->PeCVS['MuffledHeart'] = $cf4 ? $cf4->PeCVS['MuffledHeart'] : NULL;
                $cf4Form->PeCVS['Murmur'] = $cf4 ? $cf4->PeCVS['Murmur'] : NULL;
                $cf4Form->PeCVS['pHeartRem'] = $care ? $care->VitalsPhysical->Cardiovascular_abnormal : NULL;

                $cf4Form->PeAbdomen['AbdomenEssentialNormal'] = $cf4 ? $cf4->PeAbdomen['AbdomenEssentialNormal'] : NULL;
                $cf4Form->PeAbdomen['AbdomenalRigid'] = $cf4 ? $cf4->PeAbdomen['AbdomenalRigid'] : NULL;
                $cf4Form->PeAbdomen['AbdomenalTender'] = $cf4 ? $cf4->PeAbdomen['AbdomenalTender'] : NULL;
                $cf4Form->PeAbdomen['HyperactiveBowel'] = $cf4 ? $cf4->PeAbdomen['HyperactiveBowel'] : NULL;
                $cf4Form->PeAbdomen['PalpableMass'] = $cf4 ? $cf4->PeAbdomen['PalpableMass'] : NULL;
                $cf4Form->PeAbdomen['Tympanitic'] = $cf4 ? $cf4->PeAbdomen['Tympanitic'] : NULL;
                $cf4Form->PeAbdomen['Uterine'] = $cf4 ? $cf4->PeAbdomen['Uterine'] : NULL;
                $cf4Form->PeAbdomen['pAbdomenRem'] = $care ? $care->VitalsPhysical->Abdomen_abnormal : NULL;

                $cf4Form->PeGU['GUEssentialNormal'] = $cf4 ? $cf4->PeGU['GUEssentialNormal'] : NULL;
                $cf4Form->PeGU['BloodStain'] = $cf4 ? $cf4->PeGU['BloodStain'] : NULL;
                $cf4Form->PeGU['CervicalDilation'] = $cf4 ? $cf4->PeGU['CervicalDilation'] : NULL;
                $cf4Form->PeGU['AbnormalDischarge'] = $cf4 ? $cf4->PeGU['AbnormalDischarge'] : NULL;
                $cf4Form->PeGU['pGuRem'] = $care ? $care->VitalsPhysical->Pelvis_abnormal : NULL;

                $cf4Form->PeSkin['SkinEssentialNormal'] = $cf4 ? $cf4->PeSkin['SkinEssentialNormal'] : NULL;
                $cf4Form->PeSkin['Clubbing'] = $cf4 ? $cf4->PeSkin['Clubbing'] : NULL;
                $cf4Form->PeSkin['ClammySkin'] = $cf4 ? $cf4->PeSkin['ClammySkin'] : NULL;
                $cf4Form->PeSkin['Cyanosis'] = $cf4 ? $cf4->PeSkin['Cyanosis'] : NULL;
                $cf4Form->PeSkin['Edima'] = $cf4 ? $cf4->PeSkin['Edima'] : NULL;
                $cf4Form->PeSkin['DecreaseMobility'] = $cf4 ? $cf4->PeSkin['DecreaseMobility'] : NULL;
                $cf4Form->PeSkin['PaleNailBeds'] = $cf4 ? $cf4->PeSkin['PaleNailBeds'] : NULL;
                $cf4Form->PeSkin['PoorSkinTurgor'] = $cf4 ? $cf4->PeSkin['PoorSkinTurgor'] : NULL;
                $cf4Form->PeSkin['Rashes'] = $cf4 ? $cf4->PeSkin['Rashes'] : NULL;
                $cf4Form->PeSkin['WeakPulses'] = $cf4 ? $cf4->PeSkin['WeakPulses'] : NULL;
                $cf4Form->PeSkin['pSkinRem'] = $care ? $care->VitalsPhysical->Skin_abnormal : NULL;

                $cf4Form->PeNeuro['NeuroEssentialNormal'] = $cf4 ? $cf4->PeNeuro['NeuroEssentialNormal'] : NULL;
                $cf4Form->PeNeuro['AbnormalGait'] = $cf4 ? $cf4->PeNeuro['AbnormalGait'] : NULL;
                $cf4Form->PeNeuro['AbnormalPosition'] = $cf4 ? $cf4->PeNeuro['AbnormalPosition'] : NULL;
                $cf4Form->PeNeuro['AbnormalSensation'] = $cf4 ? $cf4->PeNeuro['AbnormalSensation'] : NULL;
                $cf4Form->PeNeuro['AbnormalReflexes'] = $cf4 ? $cf4->PeNeuro['AbnormalReflexes'] : NULL;
                $cf4Form->PeNeuro['PoorMemory'] = $cf4 ? $cf4->PeNeuro['PoorMemory'] : NULL;
                $cf4Form->PeNeuro['PoorMuscleTone'] = $cf4 ? $cf4->PeNeuro['PoorMuscleTone'] : NULL;
                $cf4Form->PeNeuro['PoorCoordination'] = $cf4 ? $cf4->PeNeuro['PoorCoordination'] : NULL;
                $cf4Form->PeNeuro['pNeuroRem'] = $care ? $care->VitalsPhysical->Skin_abnormal : NULL;

                for($x = 0; $x <= 14; $x++) {
                    $cf4Form->CourseInWardDate[$x] = $cf4 ? $cf4->CourseInWardDate[$x] : NULL;
                    $cf4Form->CourseInWordOrder[$x] = $cf4 ? $cf4->CourseInWordOrder[$x] : NULL;
                }
                
                $cf4Form->SurgicalProcedureCode = $cf4 ? $cf4->SurgicalProcedureCode : NULL;

                if($care AND $care->MedicalOrder) {
                    foreach($care->MedicalOrder as $k=>$medOrder) {
                        if($medOrder->medicalorder_type == 'MO_MED_PRESCRIPTION') {
                            $presCount = count($medOrder->MedicalOrderPrescription);
                            foreach($medOrder->MedicalOrderPrescription as $a=>$pres) {
                                $cf4Form->generic[$a] = str_replace("  ", " ", $pres->generic_name);
                                $cf4Form->qtyDose[$a] = $pres->total_quantity.'/'.$pres->dose_quantity.'/'.$pres->dosage_regimen;
                                $cf4Form->total[$a] = NULL;
                            }
                            for($b = $presCount; $b <= 13; $b++) {
                                $cf4Form->generic[$b] = NULL;
                                $cf4Form->qtyDose[$b] = NULL;
                                $cf4Form->total[$b] = NULL;
                            }
                        }
                    }
                } else {
                    for($a = 0; $a <= 13; $a++) {
                        $cf4Form->generic[$a] = $cf4 ? $cf4->generic[$a] : NULL;
                        $cf4Form->qtyDose[$a] = $cf4 ? $cf4->qtyDose[$a] : NULL;
                        $cf4Form->total[$a] = $cf4 ? $cf4->total[$a] : NULL;
                    }
                }

                $cf4Form->OutcomeOpt = $care ? $care->Disposition->disposition : NULL;
                $cf4Form->OutcomeOptReason = $cf4 ? $cf4->OutcomeOptReason : NULL;

            }

            $data['cf4Form'] = $cf4Form;
            $data['medicines'] = DB::table('lov_phic_medicines')->lists('drug_description');

        }

        if($eclaims_id) {
            $data['eclaims_id'] = $eclaims_id;
            $data['claim'] = $claim;
        }
        
        return view('eclaims::forms.'.$form, $data);
    }

    /**
     * Load Form
     * Param $form String - name of form to load
     * Param $patid String - patient ID or eclaim ID
     * Param $careid String Optional - healthcare ID
     * 
     * This displays the form inside a div in the page
     * for new input or view can populate form using 
     * sparse data from EMR or from stored eclaim data
     */
    function loadform($form, $patid=NULL, $careid=NULL)
    {
        $data['page'] = $form;
        if($form == 'cf1') 
        {
            $memcat = array(
                '01'=>'S',
                '02'=>'S',
                '03'=>'S',
                '04'=>'G',
                '05'=>'G',
                '06'=>'G',
                '18'=>'I',
                '13'=>'NS',
                '10'=>'NO',
                '11'=>'NO',
                '19'=>'PG',
                '22'=>'P',
                '23'=>'P'
            );
            //let us check if there is submitted data already
            //patid here is the eclaimid
            $data['title'] = "CF1";
            $data['submit_button'] = "Submit CF1";
            $data['print_button'] = "Print CF1";
            $data['eclaims_id'] = NULL;
            if(isset($cf1->cf1_details)) {
                $data['cf1'] = $cf1->cf1_details;
                $data['cf1Form'] = json_decode($cf1->cf1_sdata, false);
            } else {
                if($patid AND $careid){
                    $patient = Patients::where("patient_id", $patid)->first();
                    $contact = PatientContacts::where("patient_id", $patid)->first();
                    $philhealth = PhilhealthModel::where('patient_id','=',$patid)->first();
                    $care = Healthcareservices::where("healthcareservice_id", $careid)->first();
                    $discharge = Disposition::where("healthcareservice_id", $careid)->first();
                    
                    //generate form values  
                    $brgy = getBrgyName($contact->barangay);
                    $city = getCityNameReturn($contact->city);
                    $prov = getProvinceNameReturn($contact->province);

                    $cf1 = new \stdClass();
                    $cf1Form = new \stdClass();
                    
                    $cf1->patient_id = $patid;
                    $cf1->heatlhcareservice_id = $careid;

                    $cf1Form->pMemberShipType = NULL;
                    if(isset($philhealth->philhealth_category) AND $philhealth->philhealth_category){
                        $cf1Form->pMemberShipType = $memcat[$philhealth->philhealth_category];
                    }
                    $cf1Form->pAdmissionDate = date("m-d-Y", strtotime($care->created_at));
                    $cf1Form->pDischargeDate = date("m-d-Y", strtotime($discharge->discharge_datetime));
                    $cf1Form->pHospitalCode = $this->HOSPITAL_CODE;

                    $pin = NULL;
                    if(isset($philhealth)) {
                        $pin = $philhealth->philhealth_id;
                    }
                    $cf1Form->pin = $pin;
                    $bmon = NULL;
                    $bdd = NULL;
                    $byy = NULL;
                    $b =  explode("-",$patient->birthdate);
                    if($b) {
                        /*$bmon = str_split($b[1]);
                        $bdd = str_split($b[2]);
                        $byy = str_split($b[0]);*/
                        $bmon = $b[1];
                        $bdd = $b[2];
                        $byy = $b[0];
                    }
                    $cf1Form->pMemberBirthDate = date("m-d-Y", strtotime($patient->birthdate));
                    $cf1Form->mon = $bmon;
                    $cf1Form->dd = $bdd;
                    $cf1Form->yy = $byy;
                    $cf1Form->pMemberLastName = $patient->last_name;
                    $cf1Form->pMemberFirstName = $patient->first_name;
                    $cf1Form->pMemberSuffix = $patient->name_suffix;
                    $cf1Form->pMemberMiddleName = $patient->middle_name;
                    $cf1Form->pMemberSex = $patient->gender;
                    $cf1Form->unit = "";
                    $cf1Form->building = "";
                    $cf1Form->housenumber = "";
                    $cf1Form->street = $contact->street_name;
                    $cf1Form->village = "";
                    $cf1Form->barangay = $brgy;
                    $cf1Form->city = $city;
                    $cf1Form->province = $prov;
                    $cf1Form->country = $contact->country;
                    $cf1Form->pZipCode = $contact->zip;
                    $cf1Form->pLandlineNo = $contact->phone;
                    $cf1Form->pMobileNo = $contact->mobile;
                    $cf1Form->pEmailAddress = $contact->email;
                    $cf1Form->is_member = "";
                    if(isset($philhealth)) {
                        $cf1Form->pPatientIs = $philhealth->member_type;
                    }
                    $cf1Form->deppin = NULL;
                    $cf1Form->dep_mon = NULL;
                    $cf1Form->dep_dd = NULL;
                    $cf1Form->dep_yy = NULL;
                    $cf1Form->pPatientLastName = $patient->last_name;
                    $cf1Form->pPatientFirstName = $patient->first_name;
                    $cf1Form->pPatientSuffix = $patient->name_suffix;
                    $cf1Form->pPatientMiddleName = $patient->middle_name;
                    $cf1Form->pPatientType = $care->encounter_type;
                    $cf1Form->epen = NULL;
                    $cf1Form->pEmployerContact = NULL;
                    $cf1Form->pEmployerName = NULL;
                    
                } else {
                    $patient = NULL;
                    $contact =NULL;
                    $philhealth = NULL;
                    $care = NULL;
                    $discharge = NULL;

                    $cf1 = new \stdClass();
                    $cf1Form = new \stdClass();

                    $cf1->patient_id = NULL;
                    $cf1->heatlhcareservice_id = NULL;

                    $cf1Form->pMemberShipType = NULL;
                    $cf1Form->pin = NULL;
                    $cf1Form->pAdmissionDate = NULL;
                    $cf1Form->pDischargeDate = NULL;
                    $cf1Form->pHospitalCode = $this->HOSPITAL_CODE;
                    $cf1Form->pMemberBirthDate = NULL;
                    $cf1Form->mon = NULL;
                    $cf1Form->dd = NULL;
                    $cf1Form->yy = NULL;
                    $cf1Form->pMemberLastName = NULL;
                    $cf1Form->pMemberFirstName = NULL;
                    $cf1Form->pMemberSuffix = NULL;
                    $cf1Form->pMemberMiddleName = NULL;
                    $cf1Form->pMemberSex = NULL;
                    $cf1Form->unit = "";
                    $cf1Form->building = "";
                    $cf1Form->housenumber = "";
                    $cf1Form->street = NULL;
                    $cf1Form->village = "";
                    $cf1Form->barangay = NULL;
                    $cf1Form->city = NULL;
                    $cf1Form->province = NULL;
                    $cf1Form->country = NULL;
                    $cf1Form->pZipCode = NULL;
                    $cf1Form->pLandlineNo = NULL;
                    $cf1Form->pMobileNo = NULL;
                    $cf1Form->pEmailAddress = NULL;
                    $cf1Form->is_member = "";

                    $cf1Form->deppin = NULL;
                    $cf1Form->dep_mon = NULL;
                    $cf1Form->dep_dd = NULL;
                    $cf1Form->dep_yy = NULL;
                    $cf1Form->pPatientLastName = NULL;
                    $cf1Form->pPatientFirstName = NULL;
                    $cf1Form->pPatientSuffix = NULL;
                    $cf1Form->pPatientMiddleName = NULL;
                    $cf1Form->pPatientType = NULL;
                    $cf1Form->epen = NULL;
                    $cf1Form->pEmployerContact = NULL;
                    $cf1Form->pEmployerName = NULL;
                }
                $data['cf1Form'] = $cf1Form;
                $data['cf1'] = $cf1;
            }
        }
        
        return view('eclaims::forms.'.$form, $data);
    }

}