<?php

Route::group(['prefix' => 'eclaims', 'namespace' => 'Modules\Eclaims\Http\Controllers', 'middleware' => 'auth.access:eclaims'], function()
{
    Route::get('/', 'EclaimsController@index');
        
    Route::get('/fillform/{eclaimid}/{page}', 'EclaimsController@fillform');
    Route::get('/show/{form}/{eclaimid}', 'EclaimsController@showform');
    Route::post('/saveform', 'EclaimsController@saveform');
    Route::post('/getPIN', 'EclaimsController@getPIN');
    Route::post('/getPIN/{patid}', 'EclaimsController@getPIN');
    Route::post('/getDepPIN', 'EclaimsController@getDepPIN');
    Route::post('/getEmployer', 'EclaimsController@getEmployer');
    Route::post('/searchEmployer', 'EclaimsController@searchEmployer');
    Route::post('/getPAN/{careid}', 'EclaimsController@getPAN');
    Route::post('/getPAN', 'EclaimsController@getPAN');
    Route::post('/checkAccr/{careid}', 'EclaimsController@checkAccr');
    Route::post('/checkAccr', 'EclaimsController@checkAccr');
    Route::post('/searchCaseRate', 'EclaimsController@searchCaseRate');
    Route::post('/searchHospital', 'EclaimsController@searchHospital');
    Route::post('/uploadDocument/{eclaimid}', 'EclaimsController@uploadDocument');
    Route::post('/addDocument/{eclaimid}', 'EclaimsController@addDocument');
    Route::get('/addDocument/{eclaimid}', 'EclaimsController@addDocument');
    Route::get('/attachments/{eclaimid}', 'EclaimsController@attachments');
    Route::post('/addreceipt/{eclaimid}', 'EclaimsController@addreceipt');
    Route::post('/saveaccreditation', 'EclaimsController@saveaccreditation');

    Route::get('/loadclaim/{form}/{eclaimid}', 'LoadClaimController@loadclaim');
    Route::get('/load/{form}', 'LoadClaimController@loadform');
    Route::get('/load/{form}/{patid}', 'LoadClaimController@loadform');
    Route::get('/load/{form}/{patid}/{careid}', 'LoadClaimController@loadform');

    Route::get('/checkEligibility', 'ClaimController@checkEligibility');
    Route::post('/checkEligibility', 'ClaimController@checkEligibility');
    Route::get('/checkEligibility/{patid}/{careid}', 'ClaimController@checkEligibility');
    Route::post('/checkEligibility/{patid}/{careid}/{page}', 'ClaimController@checkEligibility');
    Route::get('/submitClaim/{eclaimid}', 'ClaimController@submitClaim');
    Route::get('/inquireClaim/{eclaimid}', 'ClaimController@inquireClaim');
    Route::get('/resubmitClaim/{eclaimid}', 'ClaimController@submitClaim');
    Route::get('/getClaimsStatus/{eclaimid}', 'ClaimController@getClaimsStatus');
    Route::post('/inquireVoucher', 'ClaimController@inquireVoucher');
    Route::get('/getClaimsMap/{eclaimid}', 'ClaimController@getClaimsMap');
    Route::get('/viewSubmitted/{eclaimid}', 'ClaimController@viewSubmitted');
    Route::get('/delete/{eclaimid}', 'ClaimController@delete');
    Route::get('/generateCF4/{eclaimid}', ['as' => 'eclaims.generateCF4', 'uses' => 'ClaimController@generateCF4']);
});
