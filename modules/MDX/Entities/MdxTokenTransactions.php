<?php namespace Modules\MDX\Entities;
   
use Illuminate\Database\Eloquent\Model;

class MdxTokenTransactions extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mdx_token_transaction';
    protected static $table_name = 'mdx_token_transaction';

    /**
     * Override primary key used by model
     *
     * @var int
     */
    protected $primaryKey = 'mdxtokentransactions_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['*'];

    public function medix()
    {
        return $this->belongsTo('Modules\Portal\Entities\Medix','medix_id', 'medix_id');
    }

}