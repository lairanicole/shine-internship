<?php namespace Modules\MDX\Entities;
   
use Illuminate\Database\Eloquent\Model;

use Shine\Libraries\IdGenerator;
use Modules\MDX\Entities\MdxTokenTransactions;

use DB;

class Medix extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'medix';
    protected static $table_name = 'medix';

    /**
     * Override primary key used by model
     *
     * @var int
     */
    protected $primaryKey = 'medix_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['*'];

    public function medix()
    {
        return $this->hasMany('Modules\Portal\Entities\MdxTokenTransactions','medix_id', 'medix_id');
    }

    public static function award($pid,$country_code='0608',$app_id = '0100')
    {
        $check = self::where('patient_id',$pid)->get();
        if(count($check) == 0)
        {
            $medix = new self;
            $medix->medix_id = $medix_id = IdGenerator::generateMedixId($country_code,$app_id);
            $medix->patient_id = $pid;
            $medix->tokens = 400.0;
            $medix->mdx_coin = 0.0;
            $medix->save();

            $new_transaction = new MdxTokenTransactions;
            $new_transaction->mdxtokentransaction_id = IdGenerator::generateId();
            $new_transaction->medix_id = $medix_id;
            $new_transaction->currency_updated = 'token';
            $new_transaction->transaction = 'Initial awarding of tokens';
            $new_transaction->transaction_amount = 400.00;
            $new_transaction->operation = 'credit';
            $new_transaction->old_value = 0.00;
            $new_transaction->new_value = 400.00;
            $new_transaction->save();
        }
    }

}