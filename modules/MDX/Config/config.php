<?php

return [
    'name' => 'MDX',
    'version' => '1.0',
    'title' => 'Shine MDX Module',
    'folder' => 'MDX',
    'table' => 'medix',
    'description' => 'MDX',
    'developer' => 'MediXServe',
    'copy' => '2018',
    'url' => 'www.shine.ph'
];
