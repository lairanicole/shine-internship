<?php

Route::group(['prefix' => 'mdx', 'namespace' => 'Modules\MDX\Http\Controllers'], function()
{
	Route::get('/generate', 'MDXController@generateMdxExisting');
	Route::get('/generate/{facid}', 'MDXController@generateMdxExisting');
});