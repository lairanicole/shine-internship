<?php 

namespace Modules\MDX\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use ShineOS\Core\Facilities\Entities\Facilities;

use Modules\MDX\Entities\Medix;
use Modules\MDX\Entities\MdxTokenTransactions;
use Shine\Libraries\IdGenerator;

use View, Form, Response, Validator, Input, Mail, Session, Redirect, Hash, Auth, DB, Datetime, Request, Storage, Schema, Image, Config;


class MDXController extends Controller {

	public function generateMdxExisting($facid = NULL){
		if($facid)
		{
			$facility_ids = array($facid);
		}
		else
		{
			$private = array('4645294151509091313054116','5003868161608081818012019');
			$facility_ids = Facilities::whereNotNull('wskey')
										->whereNotNull('ekey')
										->orWhereIn('facility_id',$private)
										->orderBy('facility_name')
										->lists('facility_id');
		}

		foreach ($facility_ids as $facility_id) {
			$temp_fac_id = array($facility_id);

			DB::table('patients_view')
				->whereIn('patients_view.facility_id',$temp_fac_id)
				->whereNull('deadid')
				->whereNull('deleted_at')
		        ->leftJoin('medix', function ($join) {
		            $join->on('patients_view.patient_id', '=', 'medix.patient_id')
		                 ->whereNull('medix.medix_id');
		        })
				->orderBy('patients_view.created_at')
				->select('patients_view.patient_id','medix.medix_id')
				->chunk(100, function ($patients) {
				    foreach ($patients as $patient) {

				    	$medix_existing = Medix::where('patient_id',$patient->patient_id)->first();
				    	if(!$medix_existing)
				    	{
					        $medix = new Medix;

					        $medix->medix_id = $medix_id = IdGenerator::generateMedixId('0608','0100');
					        $medix->patient_id = $patient->patient_id;
					        $medix->tokens = 400.00;
					        $medix->mdx_coin = 0.0;
					        $medix->save();

				            $new_transaction = new MdxTokenTransactions;
				            $new_transaction->mdxtokentransaction_id = IdGenerator::generateId();
				            $new_transaction->medix_id = $medix_id;
				            $new_transaction->currency_updated = 'token';
				            $new_transaction->transaction = 'Initial awarding of tokens';
				            $new_transaction->transaction_amount = 400.00;
				            $new_transaction->operation = 'credit';
				            $new_transaction->old_value = 0.00;
				            $new_transaction->new_value = 400.00;
				            $new_transaction->save();
					    }

				    }
				});
		}

		echo "Done!";
	}
	
}