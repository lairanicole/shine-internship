<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class lov_icd10_diagnosis extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // lov_icd10
        $icds = csv_to_array(base_path( '/database/seeds/libraries/icd10_diagnosis.csv' ),',');

        if(is_array($icds) || is_object($icds))
        {
            foreach ($icds as $key => $icd)
            {
                DB::table('lov_icd10_diagnosis')->insert([
                    'id' => $icd[0], 
                    'code' => $icd[1],
                    'diagnosis_name' => $icd[2],
                ]);
            }
        }

        Model::reguard();
    }
}
