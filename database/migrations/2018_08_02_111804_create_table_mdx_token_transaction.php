<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMdxTokenTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mdx_token_transaction', function (Blueprint $table) {
            $table->increments('id');

            $table->string('mdxtokentransaction_id',60);
            $table->string('medix_id', 16);
            $table->string('currency_updated');
            $table->double('old_value');
            $table->text('transaction');
            $table->double('transaction_amount');
            $table->string('operation',30);
            $table->double('new_value');

            $table->timestamps();
            $table->unique('mdxtokentransaction_id');
        });         //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mdx_token_transaction');
    }
}
