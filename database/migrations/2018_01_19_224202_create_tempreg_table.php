<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempregTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tempreg')!=TRUE) {
            Schema::create('tempreg', function(Blueprint $table)
            {
                $table->increments('id');
                $table->text('regjson');

                $table->timestamps();
            });
        }        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tempreg');        //
    }
}
