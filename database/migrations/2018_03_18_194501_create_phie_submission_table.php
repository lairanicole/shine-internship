<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhieSubmissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phie_submitted_records', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('phiesubmittedrecord_id', 60);
            $table->string('submission_id', 60);
            $table->string('facility_id', 60);
            $table->string('param_type', 60)->nullable();
            $table->string('success',10)->nullable();
            $table->string('response_code',10)->nullable();
            $table->string('time',10)->nullable();
            $table->string('timeval',10)->nullable();
            $table->string('year',10)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phie_submitted_records');
    }
}
