<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateConsultationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('examination', 'Hypertrophic_Congestion')!=TRUE) {
            Schema::table('examination', function (Blueprint $table) { 
                $table->string('Hypertrophic_Congestion', 5)->nullable()->after('Tonsillopharyngeal_Congestion');
                $table->string('Retractions', 5)->nullable()->after('Clear_Breathsounds');
                $table->string('Gross_Deformity', 5)->nullable()->after('abdomen_others');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
} 