<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateCatchmentArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('facility_catchment_area')!=TRUE) { 
                Schema::create('facility_catchment_area', function (Blueprint $table) {
                $table->increments('id');
                $table->string('ca_id',60);
                $table->string('facility_id',60);
                $table->string('bhs_name',60);
                $table->string('bhs_brgy_code',60);
                $table->string('createdby_userid',60);

                $table->softDeletes();
                $table->timestamps();
                $table->unique('ca_id');
            }); 
        }

        if (Schema::hasColumn('facility_user', 'catchment_area_id')!=TRUE) {
            Schema::table('facility_user', function (Blueprint $table) {
                $table->string('catchment_area_id',60)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('facility_catchment_area'); 
    }
}
