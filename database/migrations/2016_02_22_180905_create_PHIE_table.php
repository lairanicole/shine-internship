<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePHIETable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('phie_sync')!=TRUE) {
            Schema::create('phie_sync', function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('sync_id',60);
                $table->string('facility_id',60);
                $table->string('user_id',60);
                
                $table->string('param_type',60)->nullable();
                $table->string('param_type_id',60)->nullable();
                $table->string('response_code',60)->nullable();
                $table->longText('response')->nullable();

                $table->softDeletes();
                $table->timestamps();
                $table->unique('sync_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phie_sync');
    }

}
