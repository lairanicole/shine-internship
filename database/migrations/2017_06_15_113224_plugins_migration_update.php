<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PluginsMigrationUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dir    = base_path().'/plugins';
        $files = scandir($dir);
        
        if(is_null($files)==FALSE) {
            foreach ($files as $key => $value) {
                Artisan::call('migrate', ["--path"=>'/plugins/'.$value, "--force"=> true ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
