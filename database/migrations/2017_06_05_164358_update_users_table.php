<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //  
        if (Schema::hasTable('user_md')==TRUE) {          
            Schema::table('user_md', function (Blueprint $table) {  
                $conn = Schema::getConnection();
                $dbSchemaManager = $conn->getDoctrineSchemaManager();
                $doctrineTable = $dbSchemaManager->listTableDetails('user_md');
                if (! $doctrineTable->hasIndex('user_md_user_id_unique')) {
                    $table->unique(['user_id']);
                } 
            });
        }

        if (Schema::hasTable('user_contact')==TRUE) {          
            Schema::table('user_contact', function (Blueprint $table) {  
                $conn = Schema::getConnection();
                $dbSchemaManager = $conn->getDoctrineSchemaManager();
                $doctrineTable = $dbSchemaManager->listTableDetails('user_contact');
                if (! $doctrineTable->hasIndex('user_contact_user_id_unique')) {
                    $table->unique(['user_id']);
                }
            });
        }
        
        if (Schema::hasTable('roles_access')==TRUE) {          
            Schema::table('roles_access', function (Blueprint $table) {  
                $conn = Schema::getConnection();
                $dbSchemaManager = $conn->getDoctrineSchemaManager();
                $doctrineTable = $dbSchemaManager->listTableDetails('roles_access');
                if (! $doctrineTable->hasIndex('roles_access_facilityuser_id_unique')) {
                    $table->unique(['facilityuser_id']);
                }
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
